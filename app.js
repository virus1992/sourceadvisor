var compression = require('compression');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var cors = require('cors');
var exphbs = require('express-handlebars');
var mailer = require('express-mailer');
var session = require('express-session');
var flash = require('connect-flash');
var useragent = require('express-useragent');
//var basicauth = require('basicauth-middleware');

var constants = require('./modules/constants');
var handleBarHelpers = require('./modules/handlebarhelpers');
var uuid = require('./modules/uuid');

// creating routes.
var routes = require('./routes/index');
var users = require('./routes/users');
var mentor = require('./routes/mentor');
var messages = require('./routes/messages');
var miscRoutes = require('./routes/miscroutes');
var paymentRoutes = require('./routes/payment');
var meetingLog = require('./routes/meetinglog');
var mail = require('./routes/mail');
var automatedTesting = require('./routes/automatedtesting');
var admin = require('./routes/admin/index');
var adminMisc = require('./routes/admin/misc');

var cron = require('node-cron');
var moment = require('moment');
var userModel = require('./model/users_model');
var cronModel = require('./model/cron_model');
var app = express();

// Authenticator
//app.use(basicauth('sanchit', 'sahu'));

// Create `ExpressHandlebars` instance with a default layout.
var hbs = exphbs.create({
    defaultLayout: 'blank',
    // Uses multiple partials dirs, templates in "shared/templates/" are shared
    // with the client-side of the app (see below).
    partialsDir: [
        'views/partials/'
    ],
    helpers: handleBarHelpers.helperFunction
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // res.header('Cache-Control', 'no-store, must-revalidate, max-age=0');
    // res.header('Pragma', 'no-cache');
    // res.header('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT');

    if ('OPTIONS' === req.method) {
        res.send(200);
    } else {
        next();
    }
};

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(compression());
app.use(allowCrossDomain);
app.use(useragent.express());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/keyNoteVideo', express.static(path.join(__dirname, 'uploads/keynotes')));

app.use('/workfolioVideo/*', function (req, res, next) {
    if (req.query.dl !== undefined)
        res.attachment();
    next();
});
app.use('/workfolioVideo', express.static(path.join(__dirname, 'uploads/workfolio')));

app.use('/messageDocs/*', function (req, res, next) {
    if (req.query.dl !== undefined)
        res.attachment();
    next();
});
app.use('/messageDocs', express.static(path.join(__dirname, 'uploads/messagedocs')));

app.use('/crypto', express.static(path.join(__dirname, 'bower_components/crypto-js')));

app.use('/.well-known/pki-validation', express.static(path.join(__dirname, 'well-known/pki-validation')));

app.use('/slotbookingdocs/*', function (req, res, next) {
    if (req.query.dl !== undefined)
        res.attachment();
    next();
});
app.use('/slotbookingdocs', express.static(path.join(__dirname, 'uploads/slotbookingdocs')));

app.use('/bc', express.static(path.join(__dirname, '/bower_components/')));

app.use(function (req, res, next) {
    if (!req.session) {
        //res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');

        // working code for cache control
        // res.header('Cache-Control', 'no-store, must-revalidate, max-age=0');
        // res.header('Pragma', 'no-cache');
        // res.header('Expires', 'Sat, 26 Jul 1997 05:00:00 GMT');
    }
    next();
});

app.use(session({
    secret: 'ssshhhhh',
    resave: false,
    saveUninitialized: true,
    cookie: {
        path: '/',
        httpOnly: false,
        secure: false,
        maxAge: 31536000000
    }
}));
app.use(flash());

// express validator
//app.use(express.bodyParser());
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
                , root = namespace.shift()
                , formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return msg;
//        return {
//            param: formParam,
//            msg: msg,
//            value: value
//        };
    }
}));


// 
app.use(function (req, res, next) {
    req.session.userIpAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    if (typeof req.session.userId == "undefined") {
        req.session.timezone = 'Asia/Kolkata';
        req.session.USERTZBROWSER = '+05:30';
        req.session.save();
        next();
    } else {
        next();
    }
});

app.use('/', routes);
app.use('/users', users);
app.use('/mentor', mentor);
app.use('/messages', messages);
app.use('/miscRoutes', miscRoutes);
app.use('/payment', paymentRoutes);
app.use('/meetinglog', meetingLog);
app.use('/mail', mail);
app.use('/admin', admin);
app.use('/admin/misc', adminMisc);
app.use('/automatedTesting', automatedTesting);

mailer.extend(app, {
    from: constants.MAIL_FROM,
    host: constants.MAIL_HOST, // hostname 
    secureConnection: constants.MAIL_SECURE, // use SSL 
    port: constants.MAIL_PORT, // port for secure SMTP 
    transportMethod: constants.MAIL_METHOD, // default is SMTP. Accepts anything that nodemailer accepts 
    auth: {
        user: constants.MAIL_FROM_AUTH,
        pass: constants.MAIL_PASSWORD
    }
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// Cron Tab to send meeting notification mail before 30 minutes of meeting scheduled time

/*
cron.schedule('* * * * *', function (req, res, err) {
    var req = {};
    req.app = app;
    console.log('running a task every minute\n');
    console.log('cron run time : ' + moment.utc().format());
    //  userModel.getPendingMeetingNotificationUsers(req, res, function (req, res, results) {
    //      var pendingUsers = results;
    //      pendingUsers.forEach(function (pendingUsers) {
    //          userModel.getUserInformation(req, res, pendingUsers.userId, pendingUsers, userModel.sendMeetingInvitationRemainderToUser);
    //      });
    //  });

    // fucntion call to expire normal meeting invites.
    var dataToPass = {
        flagNormalInvite: true
    }
    userModel.timedoutCronMeetingSelect(req, res, dataToPass);

    // fucntion call to expire normal meeting invites.
    var dataToPass = {
        flagNormalInvite: false
    }
    userModel.timedoutCronMeetingSelect(req, res, dataToPass);

    var objDataParams = {
        day: 3
    }
    cronModel.getPenginReminder(req, res, objDataParams, function (req, res) {
    });

    var objDataParams = {
        day: 7
    }
    cronModel.getPenginReminder(req, res, objDataParams, function (req, res) {
    });


    // cron function for sending meeting reminders 30 mins prior to the meeting
    cronModel.getMeetingInfoForReminder(req, res, {}, function (req, res, objResponseData) {
        console.log("MEETING REMINDER MAIL SEND FOR TIME - " + objResponseData.currentUTC);
    });


    //userModel.timedoutCronPrivateInvite(req, res);
    //userModel.timedoutCronMeetingSelect(req, res);
});

cron.schedule('* * * * *', function (req, res, err) {
    var req = {};
    req.app = app;
    console.log("Cron for meeting status check.");
    cronModel.finalStatusUpdateOfMeeting(req, res, function (req, res, objData) {

    });
});


cron.schedule('* * * * *', function (req, res, err) {
    var req = {};
    req.app = app;
    console.log("Cron for meeting end check status check.");
    cronModel.finalStatusMeetingEndStatusUpdateOfMeeting(req, res, function (req, res, objData) {

    });
});


cron.schedule('* * * * *', function (req, res, err) {
    var req = {};
    req.app = app;
    console.log("Cron for wallet update balance");
    cronModel.doPaymentAfterMeetingCompletion(req, res, {}, function (req, res, objTest) {
    });
});


cron.schedule('* * * * *', function (req, res, err) {
    var req = {};
    req.app = app;
    console.log("Cron for wallet update balance for AUTOMATED TESTING");
    cronModel.doPaymentAfterMeetingCompletionAUTOMATEDTESTING(req, res, {}, function (req, res, objTest) {
    });
});

*/
module.exports = app;