var exports = module.exports = {};
var dateFormat = require('dateformat');


var moment = require('moment');
var dbconnect = require('./../../modules/dbconnect');
var constants = require('./../../modules/constants');
var mailer = require('./../../modules/mailer');
var fs = require('fs');
var async = require("async");
var handlebars = require('handlebars');

var miscFunction = require('./../../model/misc_model');
var userModel = require('./../../model/users_model');
var mentorModel = require('./../../model/mentor_model');
var messagesModel = require('./../../model/messages_model');
var meetingLogModel = require('./../../model/meetinglog_model');

var frontConstant = require('./../../modules/front_constant');
var sendResponse = require('./../../modules/sendresponse');



var checkAdminUserCredentials = function(req, res, objDataParams, callback){
    var userName = objDataParams.userName;

    var queryParam = [userName];
    var strQuery = '    SELECT au.recid adminUserId, au.username adminUserName, au.password adminPassword, au.timezone_id adminTimezoneId, z.zone_name adminTimeZoneLabel ' + 
                    '   FROM adminusers au, zone z ' + 
                    '   WHERE au.username = ? ' + 
                    '   AND au.timezone_id = z.zone_id';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if(results.length > 0) {
            callback(req, res, results[0]);
        }
        else {
            callback(req, res, []);
        }
    });
};
exports.checkAdminUserCredentials = checkAdminUserCredentials;

var saveAdminSession = function(req, res, objDataParams, callback) {

    req.session.adminActiveSession = {};
    req.session.adminActiveSession.adminUserId = objDataParams.adminUserId;
    req.session.adminActiveSession.adminUserName = objDataParams.adminUserName;
    req.session.adminActiveSession.adminTimezoneId = objDataParams.adminTimezoneId;
    req.session.adminActiveSession.adminTimeZoneLabel = objDataParams.adminTimeZoneLabel;

    req.session.USERTZBROWSER = moment().tz(objDataParams.adminTimeZoneLabel).format('Z');

    req.session.save(function (err) {
        callback(req, res, true);
    });
};

exports.saveAdminSession = saveAdminSession;


var logoutAdminSession = function(req, res, callback){
    req.session.adminActiveSession = {};
    req.session.save(function (err) {
        callback(req, res);
    });
}
exports.logoutAdminSession = logoutAdminSession;


var checkAdminLoggedInWeb = function(req, res, callback){
    // req.session.adminActiveSession = {};
    // req.session.adminActiveSession.adminUserId = 1;
    // req.session.adminActiveSession.adminTimezoneId = 192;
    // req.session.adminActiveSession.adminTimeZoneLabel = "Asia/Kolkata";
    
    if (typeof req.session.adminActiveSession != 'undefined') {
        if(typeof req.session.adminActiveSession.adminUserId != 'undefined') {
            callback(req, res);
        }
        else {
            req.flash('messages', 'Please login to continue.')
            res.redirect('/admin');
        }
    }
    else {
        req.flash('messages', 'Please login to continue.')
        res.redirect('/admin');
    }
}
exports.checkAdminLoggedInWeb = checkAdminLoggedInWeb;


var checkAdminLoggedInAPI = function(req, res, callback){

    // req.session.adminActiveSession = {};
    // req.session.adminActiveSession.adminUserId = 1;
    // req.session.adminActiveSession.adminTimezoneId = 192;
    // req.session.adminActiveSession.adminTimeZoneLabel = "Asia/Kolkata";
    
    if (typeof req.session.adminActiveSession != 'undefined') {
        if(typeof req.session.adminActiveSession.adminUserId != 'undefined') {
            callback(req, res);
        }
        else {
            sendResponse.sendJsonResponse(req, res, 200, {}, "1", "ADMNOTLOG");
        }
    }
    else {
        sendResponse.sendJsonResponse(req, res, 200, {}, "1", "ADMNOTLOG");
    }
}
exports.checkAdminLoggedInAPI = checkAdminLoggedInAPI;