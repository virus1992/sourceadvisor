var exports = module.exports = {};
var dateFormat = require('dateformat');


var moment = require('moment');
var dbconnect = require('./../../modules/dbconnect');
var constants = require('./../../modules/constants');
var mailer = require('./../../modules/mailer');
var fs = require('fs');
var async = require("async");
var handlebars = require('handlebars');

var miscFunction = require('./../../model/misc_model');
var userModel = require('./../../model/users_model');
var mentorModel = require('./../../model/mentor_model');
var messagesModel = require('./../../model/messages_model');
var meetingLogModel = require('./../../model/meetinglog_model');
var paymentModel = require('./../../model/payment_model');
var paypalFunction = require('./../../model/paypal_model');

var frontConstant = require('./../../modules/front_constant');
var sendResponse = require('./../../modules/sendresponse');




var getCountryListForAdmin = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var regionId = objParams.regionId;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';


    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var countryId = '0';
    if (typeof objParams.countryId != 'undefined') {
        countryId = objParams.countryId;
    }

    var queryParam = {};

    var strSelect = ' c.region_id regionId, c.country_id countryId, c.country_name countryName, c.country_code countryCode, c.phone_code countryPhoneCode, c.wc_mentor_percentage wissenxMentorPercentage, c.wc_user_percentage wissenxUserPercentage, c.status status, mr.region_name regionName, mr.region_id regionId,c.minpayout,c.maxpayout,c.maxpayoutperweek ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    //var strWhereQuery = ' c.status <> 0 ';
    var strWhereQuery = ' (c.status = 0 || c.status = 1) ';
    if (searchKey != '') {
        strWhereQuery += '   AND ( ' +
                '       c.country_name LIKE "%' + searchKey + '%" OR ' +
                '       c.country_code LIKE "%' + searchKey + '%" OR ' +
                '       c.phone_code LIKE "%' + searchKey + '%" OR ' +
                '       c.wc_mentor_percentage LIKE "%' + searchKey + '%" OR ' +
                '       c.wc_user_percentage LIKE "%' + searchKey + '%" OR ' +
                '       mr.region_name LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (countryId > 0) {
        strWhereQuery += '   AND c.country_id = ' + countryId;
    }

    if (regionId > 0) {
        strWhereQuery += '   AND c.region_id = ' + regionId;
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM countries c ' +
            '   LEFT JOIN master_region mr ' +
            '   ON c.region_id = mr.region_id ' +
            ' WHERE ' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " c.country_name ";
                break;
            case '1':
                strOrderBy = " c.country_code ";
                break;
            case '2':
                strOrderBy = " mr.region_name ";
                break;
            case '3':
                strOrderBy = " c.phone_code ";
                break;
            case '4':
                strOrderBy = " c.wc_user_percentage ";
                break;
            case '5':
                strOrderBy = " c.wc_mentor_percentage ";
                break;
            case '6':
                strOrderBy = " c.country_name ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getCountryListForAdmin = getCountryListForAdmin;


var updateWissenxCharges = function (req, res, objDataParams, callback) {
    var txtWissenxUserCharge = objDataParams.txtWissenxUserCharge;
    var txtWissenxeMentorCharge = objDataParams.txtWissenxeMentorCharge;

    var strSetQuery = '';

    if (txtWissenxUserCharge != "") {
        strSetQuery += 'c.wc_user_percentage = ' + txtWissenxUserCharge + ', ';
    }
    if (txtWissenxeMentorCharge != "") {
        strSetQuery += 'c.wc_mentor_percentage = ' + txtWissenxeMentorCharge + ', ';
    }

    var queryParam = [];
    var strQuery = 'UPDATE countries c SET ' + strSetQuery;
    strQuery = strQuery.replace(/, +$/, '');

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateWissenxCharges = updateWissenxCharges;


var deleteCountry = function (req, res, objDataParams, callback) {
    var countryId = objDataParams.countryId;

    var queryParam = [countryId];
    var strQuery = 'UPDATE countries c SET c.status = 2 WHERE c.country_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteCountry = deleteCountry;


var addCountry = function (req, res, objDataParams, callback) {
    var lstRegion = objDataParams.lstRegion;
    var txtCountryCode = objDataParams.txtCountryCode;
    var txtCountryName = objDataParams.txtCountryName;
    var txtPhoneCode = objDataParams.txtPhoneCode;
    var txtWissenxMentorChargeAddEdit = objDataParams.txtWissenxMentorChargeAddEdit;
    var txtWissenxUserChargeAddEdit = objDataParams.txtWissenxUserChargeAddEdit;
    var minpayout = objDataParams.minpayout;
    var maxpayout = objDataParams.maxpayout;
    var maxpayoutperweek = objDataParams.maxpayoutperweek;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        region_id: lstRegion,
        country_code: txtCountryCode,
        country_name: txtCountryName,
        phone_code: txtPhoneCode,
        wc_mentor_percentage: txtWissenxMentorChargeAddEdit,
        wc_user_percentage: txtWissenxUserChargeAddEdit,
        minpayout: minpayout,
        maxpayout: maxpayout,
        maxpayoutperweek: maxpayoutperweek,
        status: "1",
        created_date: currentUTC
    };

    var strQuery = 'INSERT INTO countries SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.addCountry = addCountry;



var updateCountry = function (req, res, objDataParams, callback) {
    var lstRegion = objDataParams.lstRegion;
    var txtCountryCode = objDataParams.txtCountryCode;
    var txtCountryName = objDataParams.txtCountryName;
    var txtPhoneCode = objDataParams.txtPhoneCode;
    var txtWissenxMentorChargeAddEdit = objDataParams.txtWissenxMentorChargeAddEdit;
    var txtWissenxUserChargeAddEdit = objDataParams.txtWissenxUserChargeAddEdit;
    var minpayout = objDataParams.minpayout;
    var maxpayout = objDataParams.maxpayout;
    var maxpayoutperweek = objDataParams.maxpayoutperweek;
    var countryId = objDataParams.countryId;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        region_id: lstRegion,
        country_code: txtCountryCode,
        country_name: txtCountryName,
        phone_code: txtPhoneCode,
        wc_mentor_percentage: txtWissenxMentorChargeAddEdit,
        wc_user_percentage: txtWissenxUserChargeAddEdit,
        minpayout: minpayout,
        maxpayout: maxpayout,
        maxpayoutperweek: maxpayoutperweek,
        status: "1",
        updated_date: currentUTC
    };

    var queryParam = [params, countryId];
    var strQuery = " UPDATE countries SET ? WHERE country_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateCountry = updateCountry;



var getSiteUsersList = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';

    var filterUserType = objParams.filterUserType;
    var filterUserStatus = objParams.filterUserStatus;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var countryId = '0';
    if (typeof objParams.countryId != 'undefined') {
        countryId = objParams.countryId;
    }

    var queryParam = {};

    var strSelect = ' CONCAT_WS(" ", ud.first_name, ud.last_name) fullName, u.user_type userType, u.slug slug, u.email email, u.phone_number, u.status, u.user_id';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = ' ud.user_id = u.user_id ';
    if (searchKey != '') {
        strWhereQuery += '   AND ( ' +
                '       ud.first_name LIKE "%' + searchKey + '%" OR ' +
                '       ud.last_name LIKE "%' + searchKey + '%" OR ' +
                '       u.user_type LIKE "%' + searchKey + '%" OR ' +
                '       u.email LIKE "%' + searchKey + '%" OR ' +
                '       u.phone_number LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (countryId > 0) {
        strWhereQuery += '   AND u.user_id = ' + countryId;
    }

    if (filterUserType != "") {
        switch (filterUserType) {
            case "M":
                strWhereQuery += '   AND u.user_type = "mentor" ';
                break;
            case "U":
                strWhereQuery += '   AND u.user_type = "user" ';
                break;
        }
    }

    if (filterUserStatus != "") {
        switch (filterUserStatus) {
            case "A":
                strWhereQuery += '   AND u.status = "1" ';
                break;
            case "S":
                strWhereQuery += '   AND u.status = "2" ';
                break;
        }
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM users u, user_details ud ' +
            ' WHERE ' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " ud.first_name ";
                break;
            case '1':
                strOrderBy = " u.user_type ";
                break;
            case '2':
                strOrderBy = " u.email ";
                break;
            case '3':
                strOrderBy = " u.phone_number ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getSiteUsersList = getSiteUsersList;


var getCitiesList = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';

    var countryId = objParams.countryId;
    var stateId = objParams.stateId;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    var strSelect = ' c.city_id cityId, c.country_id countryId, c.state_id stateId, c.city_name cityName, c.status status, c.created_date createdDate, c.updated_date updatedDate ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = ' 1 ';
    if (searchKey != '') {
        strWhereQuery += '   AND c.city_name LIKE "%' + searchKey + '%" ';
    }

    if (countryId > 0) {
        strWhereQuery += '   AND c.country_id = ' + countryId;
    }
    if (stateId > 0) {
        strWhereQuery += '   AND c.state_id = ' + stateId;
    }

    strWhereQuery += '   AND ( c.status = 1 OR c.status = 0) ';


    var strQuery = ' SELECT ' + strSelect +
            ' FROM cities c ' +
            ' WHERE ' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " c.city_name ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getCitiesList = getCitiesList;



var addCity = function (req, res, objDataParams, callback) {
    var txtCityName = objDataParams.txtCityName;
    var countryId = objDataParams.countryId;
    var stateId = objDataParams.stateId;

    var params = {
        country_id: countryId,
        state_id: stateId,
        city_name: txtCityName,
        status: "1"
    };

    var strQuery = 'INSERT INTO cities SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.addCity = addCity;


var updateCity = function (req, res, objDataParams, callback) {
    var txtCityName = objDataParams.txtCityName;
    var countryId = objDataParams.countryId;
    var stateId = objDataParams.stateId;
    var cityId = objDataParams.cityId;

    var params = {
        country_id: countryId,
        state_id: stateId,
        city_name: txtCityName,
    };

    var queryParam = [params, cityId];
    var strQuery = " UPDATE cities SET ? WHERE city_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateCity = updateCity;

var deleteCity = function (req, res, objDataParams, callback) {
    var cityId = objDataParams.cityId;

    var queryParam = [cityId];
    var strQuery = 'UPDATE cities mr SET mr.status = 2 WHERE mr.city_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteCity = deleteCity;


var changeStatusForCity = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var cityId = objDataParams.cityId;
    var params = {
        status: statusToChange
    };
    var queryParam = [params, cityId];
    var strQuery = " UPDATE cities SET ? WHERE city_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.changeStatusForCity = changeStatusForCity;


var getCityDetailInfo = function (req, res, objDataParams, callback) {
    var cityId = objDataParams.cityId;

    var queryParam = [cityId];
    var strQuery = " SELECT * FROM cities c where c.city_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, {});
            }
        }
    });
};
exports.getCityDetailInfo = getCityDetailInfo;

//==========================================================================


var getRegionList = function (req, res, objDataParams, callback) {

    var regionId = 0;

    if (typeof objDataParams.regionId != 'undefined') {
        regionId = objDataParams.regionId;
    }

    var queryParam = {};
    var strQuery = '    SELECT mr.region_id regionId, mr.region_name regionName, mr.status status ' +
            '   FROM master_region mr ' +
            //'   WHERE mr.status <> 0 ';
            '   WHERE (mr.status = "0" OR  mr.status = "1") ';
    if (regionId > 0) {
        strQuery += '   AND mr.region_id = ' + regionId;
    }

    strQuery += '   ORDER BY region_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}
exports.getRegionList = getRegionList;


var addRegion = function (req, res, objDataParams, callback) {
    var txtRegionName = objDataParams.txtRegionName;

    var params = {
        region_name: txtRegionName,
        status: "1"
    };

    var strQuery = 'INSERT INTO master_region SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.addRegion = addRegion;


var updateRegion = function (req, res, objDataParams, callback) {
    var txtRegionName = objDataParams.txtRegionName;
    var regionId = objDataParams.regionId;

    var params = {
        region_name: txtRegionName
    };

    var queryParam = [params, regionId];
    var strQuery = " UPDATE master_region SET ? WHERE region_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateRegion = updateRegion;

var deleteRegion = function (req, res, objDataParams, callback) {
    var regionId = objDataParams.regionId;

    var queryParam = [regionId];
    var strQuery = 'UPDATE master_region mr SET mr.status = 2 WHERE mr.region_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteRegion = deleteRegion;


var changeStatusForRegion = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var regionId = objDataParams.regionId;
    var params = {
        status: statusToChange
    };

    async.parallel([
        function (updateStatus) {
            var queryParam = [params, regionId];
            var strQuery = " UPDATE master_region SET ? WHERE region_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [params, regionId];
            var strQuery = " UPDATE countries SET ? WHERE region_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [statusToChange, regionId];
            var strQuery = " UPDATE states s,countries c SET s.status = ? WHERE s.country_id = c.country_id AND c.region_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [statusToChange, regionId];
            var strQuery = " UPDATE cities ct,countries c SET ct.status = ? WHERE ct.country_id = c.country_id AND c.region_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        }
    ], function (err, result) {
        var resgionUpdate = result[0];
        var countryUpdate = result[1];
        var stateUpdate = result[2];
        var cityUpdate = result[3];

        if (!resgionUpdate || !countryUpdate || !stateUpdate || !cityUpdate) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }

    });
};
exports.changeStatusForRegion = changeStatusForRegion;


var getIndustriesList = function (req, res, objDataParams, callback) {
    var industryId = 0;

    if (typeof objDataParams.industryId != 'undefined') {
        industryId = objDataParams.industryId;
    }

    var queryParam = {};
    var strQuery = '    SELECT mi.industry_id industryId, mi.industry_name industryName, mi.status status ' +
            '   FROM master_industry mi ' +
            //'   WHERE mi.status <> 0 ';
            '   WHERE (mi.status = "0" OR  mi.status = "1") ';
    if (industryId > 0) {
        strQuery += '   AND mi.industry_id = ' + industryId;
    }

    strQuery += '   ORDER BY mi.industry_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getIndustriesList = getIndustriesList;


var addIndustry = function (req, res, objDataParams, callback) {
    var txtIndustrynName = objDataParams.txtIndustrynName;

    var params = {
        industry_name: txtIndustrynName,
        status: "1"
    };

    var strQuery = 'INSERT INTO master_industry SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.addIndustry = addIndustry;


var updateIndustry = function (req, res, objDataParams, callback) {
    var txtIndustrynName = objDataParams.txtIndustrynName;
    var industryId = objDataParams.industryId;

    var params = {
        industry_name: txtIndustrynName
    };

    var queryParam = [params, industryId];
    var strQuery = " UPDATE master_industry SET ? WHERE industry_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateIndustry = updateIndustry;


var deleteIndustry = function (req, res, objDataParams, callback) {
    var industryId = objDataParams.industryId;

    var queryParam = [industryId];
    var strQuery = 'UPDATE master_industry mr SET mr.status = 2 WHERE mr.industry_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteIndustry = deleteIndustry;


var changeStatusForIndustry = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var industryId = objDataParams.industryId;

    var params = {
        status: statusToChange
    };

    async.parallel([
        function (changeStatus) {
            var queryParam = [params, industryId];
            var strQuery = " UPDATE master_industry SET ? WHERE industry_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    changeStatus(null, false);
                    //callback(req, res, false);
                } else {
                    changeStatus(null, true);
                    //callback(req, res, true);
                }
            });
        },
        function (changeStatus) {
            var queryParam = [params, industryId];
            var strQuery = " UPDATE master_subindustry SET ? WHERE industry_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    changeStatus(null, false);
                    //callback(req, res, false);
                } else {
                    changeStatus(null, true);
                    //callback(req, res, true);
                }
            });
        }
    ], function (error, result) {
        var industryUpdate = result[0];
        var subIndustryUpdate = result[1];

        if (!industryUpdate || !subIndustryUpdate) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.changeStatusForIndustry = changeStatusForIndustry;


var getSubIndustriesDetails = function (req, res, objDataParams, callback) {

    if (typeof objDataParams.industryId != 'undefined') {
        var industryId = objDataParams.industryId;
        var subIndustryId = 0;

        if (typeof objDataParams.subIndustryId != 'undefined') {
            subIndustryId = objDataParams.subIndustryId;
        }

        var queryParam = {};
        var strQuery = '    SELECT msi.subindustry_id subIndustryId, msi.industry_id industryId, msi.subindustry_name subIndustryName, mi.industry_name industryName, msi.subindustry_desc subIndustryDescription, msi.subindustry_keyword subIndustryKeyWork, msi.status status ' +
                '   FROM master_subindustry msi, master_industry mi ' +
                //'   WHERE msi.status <> 0 ' +
                '   WHERE (msi.status = "0" OR  msi.status = "1") ' +
                '   AND msi.industry_id = mi.industry_id ' +
                '   AND msi.industry_id = ' + industryId;
        if (subIndustryId > 0) {
            strQuery += '   AND msi.subindustry_id = ' + subIndustryId;
        }

        strQuery += '   ORDER BY msi.subindustry_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            callback(req, res, results);
        });
    } else {
        callback(req, res, []);
    }
}
exports.getSubIndustriesDetails = getSubIndustriesDetails;



var addSubIndustry = function (req, res, objDataParams, callback) {
    var txtIndustrynName = objDataParams.txtIndustrynName;
    var taDescription = objDataParams.taDescription;
    var taKeywordsz = objDataParams.taKeywords;
    var industryId = objDataParams.industryId;

    var params = {
        industry_id: industryId,
        subindustry_name: txtIndustrynName,
        subindustry_desc: taDescription,
        subindustry_keyword: taKeywordsz,
        status: "1"
    };

    var strQuery = 'INSERT INTO master_subindustry SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.addSubIndustry = addSubIndustry;


var updateSubIndustry = function (req, res, objDataParams, callback) {
    var txtIndustrynName = objDataParams.txtIndustrynName;
    var taDescription = objDataParams.taDescription;
    var taKeywordsz = objDataParams.taKeywords;
    var industryId = objDataParams.industryId;
    var hidSubIndustryId = objDataParams.hidSubIndustryId;

    var params = {
        industry_id: industryId,
        subindustry_name: txtIndustrynName,
        subindustry_desc: taDescription,
        subindustry_keyword: taKeywordsz
    };

    var queryParam = [params, hidSubIndustryId];
    var strQuery = " UPDATE master_subindustry SET ? WHERE subindustry_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateSubIndustry = updateSubIndustry;


var deleteSubIndustry = function (req, res, objDataParams, callback) {
    var industryId = objDataParams.industryId;
    var subIndustryId = objDataParams.subIndustryId;

    var queryParam = [subIndustryId];
    var strQuery = 'UPDATE master_subindustry mr SET mr.status = 2 WHERE mr.subindustry_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
}
exports.deleteSubIndustry = deleteSubIndustry;


var changeStatusForSubIndustry = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var subIndustryId = objDataParams.subIndustryId;
    var industryId = objDataParams.industryId;

    var params = {
        status: statusToChange
    };

    if (statusToChange == "1") {
        var queryParam = [params, industryId];
        var strQuery = " UPDATE master_industry SET ? WHERE industry_id = ? ";
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                callback(req, res, false);
            } else {
                var queryParam = [params, subIndustryId];
                var strQuery = " UPDATE master_subindustry SET ? WHERE subindustry_id = ? ";
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        callback(req, res, false);
                    } else {
                        callback(req, res, true);
                    }
                });
            }
        });
    } else {
        var queryParam = [params, subIndustryId];
        var strQuery = " UPDATE master_subindustry SET ? WHERE subindustry_id = ? ";
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                callback(req, res, false);
            } else {
                callback(req, res, true);
            }
        });
    }
}
exports.changeStatusForSubIndustry = changeStatusForSubIndustry;


/**
 * function starting for subindustries keyword
 * @param {*} req 
 * @param {*} res 
 * @param {*} objDataParams 
 * @param {*} callback 
 */
var getSubIndustriesKeywords = function (req, res, objDataParams, callback) {

    if (typeof objDataParams.subIndustryId != 'undefined') {
        var subIndustryId = objDataParams.subIndustryId;
        var subIndustryKeywordId = 0;

        if (typeof objDataParams.subIndustryKeywordId != 'undefined') {
            subIndustryKeywordId = objDataParams.subIndustryKeywordId;
        }

        var queryParam = {};
        var strQuery = '    SELECT msk.master_subindustry_keyword_id subIndustryKeywordId, msk.subindustry_id subIndustryId, msk.keyword keyword, msk.status status, msk.created_date createdDate, msk.updated_date updatedDate ' +
                '   FROM master_subindustry_keyword msk, master_subindustry msi, master_industry mi ' +
                '   WHERE (msk.status = "0" OR  msk.status = "1") ' +
                '   AND msk.subindustry_id = msi.subindustry_id ' +
                '   AND msi.industry_id = mi.industry_id ' +
                '   AND msk.subindustry_id = ' + subIndustryId;
        if (subIndustryKeywordId > 0) {
            strQuery += '   AND msk.master_subindustry_keyword_id = ' + subIndustryKeywordId;
        }

        strQuery += '   ORDER BY msk.keyword ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            callback(req, res, results);
        });
    } else {
        callback(req, res, []);
    }
}
exports.getSubIndustriesKeywords = getSubIndustriesKeywords;



var addSubIndustryKeyword = function (req, res, objDataParams, callback) {

    var txtSubIndustryKeywordName = objDataParams.txtSubIndustryKeywordName;
    var subIndustryKeywordId = objDataParams.subIndustryKeywordId;
    var subIndustryId = objDataParams.subIndustryId;
    var industryId = objDataParams.industryId;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        subindustry_id: subIndustryId,
        keyword: txtSubIndustryKeywordName,
        status: "1",
        created_date: currentUTC
    };

    var strQuery = 'INSERT INTO master_subindustry_keyword SET ? ';

    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.addSubIndustryKeyword = addSubIndustryKeyword;


var updateSubIndustryKeyword = function (req, res, objDataParams, callback) {
    var txtSubIndustryKeywordName = objDataParams.txtSubIndustryKeywordName;
    var subIndustryKeywordId = objDataParams.subIndustryKeywordId;
    var subIndustryId = objDataParams.subIndustryId;
    var industryId = objDataParams.industryId;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        keyword: txtSubIndustryKeywordName,
        updated_date: currentUTC
    };

    var queryParam = [params, subIndustryKeywordId];
    var strQuery = " UPDATE master_subindustry_keyword SET ? WHERE master_subindustry_keyword_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateSubIndustryKeyword = updateSubIndustryKeyword;


var deleteSubIndustryKeyword = function (req, res, objDataParams, callback) {
    var subIndustryKeywordId = objDataParams.subIndustryKeywordId;

    var queryParam = [subIndustryKeywordId];
    var strQuery = 'UPDATE master_subindustry_keyword mr SET mr.status = 2 WHERE mr.master_subindustry_keyword_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
}
exports.deleteSubIndustryKeyword = deleteSubIndustryKeyword;


var changeStatusForSubIndustryKeyword = function (req, res, objDataParams, callback) {

    var subIndustryId = objDataParams.subIndustryId;
    var subIndustryKeywordId = objDataParams.subIndustryKeywordId;
    var statusToChange = objDataParams.statusToChange;


    var params = {
        status: statusToChange
    };

    var queryParam = [params, subIndustryKeywordId];
    var strQuery = " UPDATE master_subindustry_keyword SET ? WHERE master_subindustry_keyword_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });

}
exports.changeStatusForSubIndustryKeyword = changeStatusForSubIndustryKeyword;






var getDomainList = function (req, res, objDataParams, callback) {

    var domainId = 0;

    if (typeof objDataParams.domainId != 'undefined') {
        domainId = objDataParams.domainId;
    }

    var queryParam = {};
    var strQuery = '    SELECT md.master_domain_id domainId, md.domain_name domainName, md.status status ' +
            '   FROM master_domain md ' +
            '   WHERE md.status <> 0 ';
    if (domainId > 0) {
        strQuery += '   AND md.master_domain_id = ' + domainId;
    }

    strQuery += '   ORDER BY md.domain_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getDomainList = getDomainList;

var addDomain = function (req, res, objDataParams, callback) {
    var txtDomainName = objDataParams.txtDomainName;

    var params = {
        domain_name: txtDomainName,
        status: "1"
    };

    var strQuery = 'INSERT INTO master_domain SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.addDomain = addDomain;



var updateDomain = function (req, res, objDataParams, callback) {
    var txtDomainName = objDataParams.txtDomainName;
    var domainId = objDataParams.domainId;

    var params = {
        domain_name: txtDomainName
    };

    var queryParam = [params, domainId];
    var strQuery = " UPDATE master_domain SET ? WHERE master_domain_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateDomain = updateDomain;


var deleteDomain = function (req, res, objDataParams, callback) {
    var domainId = objDataParams.domainId;

    var queryParam = [domainId];
    var strQuery = 'UPDATE master_domain md SET md.status = 0 WHERE md.master_domain_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteDomain = deleteDomain;

var changeStatusForDomain = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var domainId = objDataParams.domainId;

    var params = {
        status: statusToChange
    };

    var queryParam = [params, domainId];
    var strQuery = " UPDATE master_domain SET ? WHERE master_domain_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.changeStatusForDomain = changeStatusForDomain;



var getSystemSettingVariables = function (req, res, objDataParams, callback) {
    var systemSettingVariableId = 0;

    if (typeof objDataParams.systemSettingVariableId != 'undefined') {
        systemSettingVariableId = objDataParams.systemSettingVariableId;
    }

    var queryParam = {};
    var strQuery = '    SELECT ss.system_settings_id systemSettingVariableId, ss.code code, ss.description description, ss.varused varused, ss.int_value intValue, ss.decimal_value decimalValue, ss.char_value charValue, ss.text_value textValue, ss.wysiwyg editorValue, ' +
            '   IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
            '   FROM system_settings ss ' +
            '   WHERE ss.status <> 0 ';
    if (systemSettingVariableId > 0) {
        strQuery += '   AND ss.system_settings_id = ' + systemSettingVariableId;
    }

    strQuery += '   ORDER BY ss.code ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getSystemSettingVariables = getSystemSettingVariables;


var addSystemVariable = function (req, res, objDataParams, callback) {

    var txtCode = objDataParams.txtCode;
    var taDescription = objDataParams.taDescription;
    var lstValueType = objDataParams.lstValueType;
    var txtIntValue = objDataParams.txtIntValue;
    var txtFloatValue = objDataParams.txtFloatValue;
    var txtCharValue = objDataParams.txtCharValue;
    var txtTextValue = objDataParams.txtTextValue;
    var taEditorValue = objDataParams.taEditorValue;

    var params = {
        code: txtCode,
        description: taDescription,
        varused: lstValueType,
        int_value: txtIntValue,
        decimal_value: txtFloatValue,
        char_value: txtCharValue,
        text_value: txtTextValue,
        wysiwyg: taEditorValue,
        status: "1"
    };

    var strQuery = 'INSERT INTO system_settings SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.addSystemVariable = addSystemVariable;


var updateSystemVariable = function (req, res, objDataParams, callback) {
    var txtCode = objDataParams.txtCode;
    var taDescription = objDataParams.taDescription;
    var lstValueType = objDataParams.lstValueType;
    var txtIntValue = objDataParams.txtIntValue;
    var txtFloatValue = objDataParams.txtFloatValue;
    var txtCharValue = objDataParams.txtCharValue;
    var txtTextValue = objDataParams.txtTextValue;
    var taEditorValue = objDataParams.taEditorValue;
    var systemVariableId = objDataParams.systemVariableId;

    var params = {
        code: txtCode,
        description: taDescription,
        varused: lstValueType,
        int_value: txtIntValue,
        decimal_value: txtFloatValue,
        char_value: txtCharValue,
        text_value: txtTextValue,
        wysiwyg: taEditorValue,
        status: "1"
    };

    var queryParam = [params, systemVariableId];
    var strQuery = " UPDATE system_settings SET ? WHERE system_settings_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateSystemVariable = updateSystemVariable;



var deleteSystemVariable = function (req, res, objDataParams, callback) {
    var systemSettingVariableId = objDataParams.systemSettingVariableId;

    var queryParam = [systemSettingVariableId];
    var strQuery = 'UPDATE system_settings ss SET ss.status = 0 WHERE ss.system_settings_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
}
exports.deleteSystemVariable = deleteSystemVariable;


var getPolicyInfo = function (req, res, objDataParams, callback) {
    var policyFor = "user";

    if (typeof objDataParams.policyFor != 'undefined') {
        policyFor = objDataParams.policyFor;
    }

    var queryParam = [policyFor];
    var strQuery = '    SELECT cp.cancellation_policy_id cancellationPolicyId, cpc.cancellation_policy_charges_id cancellationPolicyChargesId, cpt.cancellation_policy_type_id cancellationPolicyTypeId, cp.policy_for policyFor, cp.policy_type policyType, cp.policy_label policyLabel, cpc.policy_type, cpc.percentage cancellationPercentage, cpt.hours hoursDistribution ' +
            '   FROM cancellation_policy cp ' +
            '   	RIGHT JOIN cancellation_policy_charges cpc ON cpc.cancellation_policy_id = cp.cancellation_policy_id, ' +
            '   	cancellation_policy_type cpt ' +
            '   WHERE cpt.policy_type = cpc.policy_type ' +
            '   AND cp.policy_for = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, []);
        }
    });

};
exports.getPolicyInfo = getPolicyInfo;

var recursivePolicyBreak = function (req, res, resultSet, arrFinalResult, callback) {

    if (resultSet.length > 0) {
        var thisResultSet = resultSet[0];

        var policyType = thisResultSet.policyType;
        var policyLabel = thisResultSet.policyLabel;
        var policy_type = thisResultSet.policy_type;
        var cancellationPercentage = thisResultSet.cancellationPercentage;
        var hoursDistribution = thisResultSet.hoursDistribution;

        var cancellationPolicyId = thisResultSet.cancellationPolicyId;
        var cancellationPolicyChargesId = thisResultSet.cancellationPolicyChargesId;
        var cancellationPolicyTypeId = thisResultSet.cancellationPolicyTypeId;


        //if(arrFinalResult.indexOf(policyType) < 0) {
        if (typeof arrFinalResult[policyType] == 'undefined') {
            arrFinalResult[policyType] = {};
        }

        if (typeof arrFinalResult[policyType][policy_type] == 'undefined') {
            //if(arrFinalResult[policyType].indexOf(policy_type) < 0) {
            arrFinalResult[policyType][policy_type] = {};

            var arrHourDistribution = [];

            if (policy_type == "second") {
                arrHourDistribution = hoursDistribution.split(',');
            }

            arrFinalResult[policyType][policy_type]['cancellationPolicyId'] = cancellationPolicyId;
            arrFinalResult[policyType][policy_type]['cancellationPolicyChargesId'] = cancellationPolicyChargesId;
            arrFinalResult[policyType][policy_type]['cancellationPolicyTypeId'] = cancellationPolicyTypeId;

            arrFinalResult[policyType][policy_type]['policyType'] = policyType;
            arrFinalResult[policyType][policy_type]['policyLabel'] = policyLabel;
            arrFinalResult[policyType][policy_type]['policy_type'] = policy_type;
            arrFinalResult[policyType][policy_type]['cancellationPercentage'] = cancellationPercentage;
            arrFinalResult[policyType][policy_type]['hoursDistribution'] = hoursDistribution;
            if (policy_type == "second") {
                arrFinalResult[policyType]['arrHours'] = {
                    "firstSlotTime": arrHourDistribution[0],
                    "secondSlotTime": arrHourDistribution[1]
                };
            }
        }

        resultSet.splice(0, 1);
        recursivePolicyBreak(req, res, resultSet, arrFinalResult, callback);
    } else {
        callback(req, res, arrFinalResult);
    }


    // var test = {
    //     ACCESSURL               : 	"BASE_URL",
    //     ACCESSURL_ADMIN         :   "BASE_URL_ADMIN",
    //     COMPLIANCE_TEST_URL     : 	"COMPLIANCE_TEST_URL",
    //     LISTINGPERPAGE          : 	"LISTINGPERPAGE",
    //     GOOGLE_API_KEY          :   "GOOGLE_API_KEY",
    //     LINKEDIN_API_KEY        :   "LINKEDIN_API_KEY",
    //     HELP_EMAIL              :   "HELP_EMAIL",
    //     INFO_EMAIL              :   "INFO_EMAIL",
    //     CONNECT_EMAIL           :   "CONNECT_EMAIL",
    //     REVIEWCOUNT             :   "REVIEWCOUNT",
    //     MEETINGPERPAGE          :   "MEETINGPERPAGE",
    //     INVITEPERPAGE           :   "INVITEPERPAGE",
    //     MAILCONTEXTBG           :   "MAILCONTEXTBG",
    //     MESSAGECONTEXTBG        :   "MESSAGECONTEXTBG"
    // };
    // callback(req ,res, test);
};
exports.recursivePolicyBreak = recursivePolicyBreak;

var getMentorTransection = function (req, res, mentorId, callback) {
    var mentorId = mentorId;

    var queryParam = [mentorId, mentorId];
    var strQuery = 'Select ins.pay_to_mentor,DATE_FORMAT(ins.created_date, "%d %b %Y") as created_date ,mi.meeting_purpose ' +
            ' From meeting_invitations mi' +
            ' LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id ' +
            ' where (mi.user_id = ? AND mi.invitation_type = 0) OR (mi.requester_id = ? AND mi.invitation_type = 1) ORDER BY ins.created_date DESC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results);
        }
    });
}
exports.getMentorTransection = getMentorTransection;

var getMentorEarning = function (req, res, mentorId, amount, callback) {
    var mentorId = mentorId;
    var amount = amount;

    if (amount == 'total') {
        var condition = "";
    } else {
        var condition = " AND wallet_transection.created_date > YEAR(CURDATE())-01-01 AND wallet_transection.created_date < CURDATE() ";
    }

//    var queryParam = [mentorId, mentorId];
//    var strQuery = 'Select SUM(ins.pay_to_mentor) as total_earning,YEAR(CURDATE()) as year ' +
//            ' From meeting_invitations mi' +
//            ' LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id ' +
//            ' where (mi.user_id = ? AND mi.invitation_type = 0) OR (mi.requester_id = ? AND mi.invitation_type = 1)' +
//            condition +
//            ' ORDER BY ins.created_date DESC';

    var queryParam = [mentorId];
    var strQuery = 'Select FORMAT(SUM(amount),2) total_earning ,YEAR(CURDATE()) as year from wallet_transection WHERE user_id = ? AND transection_type != "PAYOUT"' + condition;

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
}
exports.getMentorEarning = getMentorEarning;

var getUserTransection = function (req, res, userId, callback) {
    var userId = userId;

    var queryParam = [userId, userId];
    var strQuery = 'Select ins.total_payable_amount,DATE_FORMAT(ins.created_date, "%d %b %Y") as created_date ,mi.meeting_purpose ' +
            ' From meeting_invitations mi' +
            ' LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id ' +
            ' where (mi.user_id = ? AND mi.invitation_type = 1) OR (mi.requester_id = ? AND mi.invitation_type = 0) ORDER BY ins.created_date DESC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results);
        }
    });
}
exports.getUserTransection = getUserTransection;

var getUserEarning = function (req, res, mentorId, amount, callback) {
    var mentorId = mentorId;
    var amount = amount;

    if (amount == 'total') {
        var condition = "";
    } else {
        var condition = " AND wallet_transection.created_date > YEAR(CURDATE())-01-01 AND wallet_transection.created_date < CURDATE() ";
    }

//    var queryParam = [mentorId, mentorId];
//    var strQuery = 'Select SUM(ins.total_payable_amount) as total_earning,YEAR(CURDATE()) as year ' +
//            ' From meeting_invitations mi' +
//            ' LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id ' +
//            ' where (mi.user_id = ? AND mi.invitation_type = 1) OR (mi.requester_id = ? AND mi.invitation_type = 0)' +
//            condition +
//            ' ORDER BY ins.created_date DESC';

    var queryParam = [mentorId, mentorId, mentorId];
    var strQuery = ' SELECT FORMAT(( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection ' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "DEBITED"' + condition +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "CREDITED" ' + condition +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "REFUNDED" ' + condition +
            ' ),2) total_earning , YEAR(CURDATE()) as year ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
}
exports.getUserEarning = getUserEarning;



var updateMentorCancellationPolicy = function (req, res, objDataParams, callback) {
    var cancellationPercetage = objDataParams.cancellationPercetage;
    var cancellationPolicyId = objDataParams.cancellationPolicyId;
    var cancellationPolicyChargesId = objDataParams.cancellationPolicyChargesId;
    var cancellationPolicyTypeId = objDataParams.cancellationPolicyTypeId;

    var params = {
        percentage: cancellationPercetage,
    };

    var queryParam = [params, cancellationPolicyChargesId];

    var strQuery = " UPDATE cancellation_policy_charges SET ? WHERE cancellation_policy_charges_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateMentorCancellationPolicy = updateMentorCancellationPolicy;


var updateCancellationPolicyTypeHour = function (req, res, objDataParams, callback) {
    var policyTypeId = objDataParams.policyTypeId;
    var slotToUpdate = objDataParams.slotToUpdate;

    var params = {
        hours: slotToUpdate,
    };

    var queryParam = [params, policyTypeId];

    var strQuery = " UPDATE cancellation_policy_type SET ? WHERE cancellation_policy_type_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateCancellationPolicyTypeHour = updateCancellationPolicyTypeHour;



var getRefundRequestListInfo = function (req, res, objData, callback) {

    var refundRequestId = 0;

    if (typeof objData.refundRequestId != 'undefined') {
        refundRequestId = objData.refundRequestId
    }


    var queryParam = {};
    var strQuery = '    SELECT rq.refund_request_id refundRequestId, rq.refund_request_unq_id refundRequestUnqId, rq.invitation_id invitationId, rq.meeting_detail_id meetingDetailId, rq.requester_id meetingUserId, rq.user_id meetingMentorId, rq.reason reason, rq.reason_description reasonDescription, rq.status refundRequestStatus, rq.final_action refundRequestFinalActionTaken, rq.created_date refundRequestedDate, CONCAT_WS(" ", udu.first_name, udu.last_name) userFullName, CONCAT_WS(" ", udm.first_name, udm.last_name) mentorFullName, uu.slug userSlug, um.slug mentorSlug, mi.meeting_type meetingType, mi.duration meetingDuration, mi.meeting_purpose meetingPurpose, mi.meeting_purpose_indetail meetingPurposeDetail, mi.created_date meetingInvitationCreatedDate, udu.profile_image userProfileImage, udm.profile_image mentorProfileImage, md.scheduled_start_time meetingStartDateTime, i.total_payable_amount totalPayAmount,md.meeting_url_key, ' +
            '   DATE_FORMAT(CONVERT_TZ(rq.created_date, "' + constants.UTCTZ + '", "' + req.session.adminActiveSession.adminTimeZoneLabel + '"), "' + constants.MYSQL_DB_DATETIME + '") refundRequestedDateDisplay ' +
            '   FROM refund_request rq, users uu, user_details udu, users um, user_details udm, meeting_invitations mi, meeting_details md, invoice i ' +
            '   WHERE rq.requester_id = udu.user_id ' +
            '   AND rq.user_id = udm.user_id ' +
            '   AND rq.requester_id = uu.user_id ' +
            '   AND rq.user_id = um.user_id ' +
            '   AND rq.invitation_id = mi.invitation_id ' +
            '   AND rq.invitation_id = md.invitation_id ' +
            '   AND rq.invitation_id = i.invitation_id ';

    if (refundRequestId > 0) {
        strQuery += '    AND rq.refund_request_id = ' + refundRequestId;
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            var resultCounter = 0;
            results.forEach(function (element) {
                var meetingTimeConvertedDisplayFormat = moment.tz(element.meetingStartDateTime, "UTC").tz(req.session.adminActiveSession.adminTimeZoneLabel).format('Do MMMM, Y hh:mm A');
                var meetingTimeConvertedDefaultFormat = moment.tz(element.meetingStartDateTime, "UTC").tz(req.session.adminActiveSession.adminTimeZoneLabel).format('YYYY-MM-DD HH:mm:ss');

                var endtext = moment(meetingTimeConvertedDefaultFormat).add(element.meetingDuration, 'm').format('hh:mm A');

                var meetingBookedDateTime = element.meetingInvitationCreatedDate;
                var meetingBookedDateTimeConverted = moment.tz(meetingBookedDateTime, "UTC").tz(req.session.adminActiveSession.adminTimeZoneLabel).format('YYYY-MM-DD HH:mm:ss');
                var pastTimeMeetingBooked = moment(meetingBookedDateTimeConverted).fromNow();

                var totalPayAmountDisplay = element.totalPayAmount.toFixed(2);
                results[resultCounter].displayMeetingTime = meetingTimeConvertedDisplayFormat + ' To ' + endtext;
                results[resultCounter].totalPayAmountDisplay = totalPayAmountDisplay;
                results[resultCounter].pastTimeMeetingBooked = pastTimeMeetingBooked;
                resultCounter++;
            });
            callback(req, res, results);
        }
    });
}
exports.getRefundRequestListInfo = getRefundRequestListInfo;

var processPartialRefund = function (req, res, objDataParams, callback) {

    var chkRefundType = objDataParams.chkRefundType;
    var invitationId = objDataParams.invitationId;
    var meetingCost = objDataParams.meetingCost;
    var meetingDetailId = objDataParams.sfasmeetingDetailIddf;
    var meetingMentorId = objDataParams.meetingMentorId;
    var meetingUserId = objDataParams.meetingUserId;
    var refundRequestId = objDataParams.refundRequestId;
    var refundRequestUnqId = objDataParams.refundRequestUnqId;
    var taRefundMessage = objDataParams.taRefundMessage;
    var txtAmountToRefund = objDataParams.txtAmountToRefund;
    var meetingDuration = objDataParams.meetingDuration;
    var meeting_url_key = objDataParams.meeting_url_key;

    async.parallel([
        // function call to get invoice detail
        function (refundRequestCallback) {
            getInvoiceDetailForInvitation(req, res, objDataParams, function (req, res, objInvoiceDetail) {
                refundRequestCallback(null, objInvoiceDetail);
            });
        },
        function (refundRequestCallback) {

            var objDataParams = {
                systemSettingVariables: ['MINWCP', 'TAXCRG']
            }
            miscFunction.getSystemSettingVariable(req, res, objDataParams, function (req, res, objSystemSettingResponse) {
                refundRequestCallback(null, objSystemSettingResponse);
            });
        }
    ], function (error, resultSet) {

        var objInvoiceDetail = resultSet[0];
        var objSystemSettingsVariable = resultSet[1];

        var wissenxMinimumUserCharge = "0";
        var taxChargePercentage = "0";

        for (var i = 0; i < objSystemSettingsVariable.length; i++) {
            if (objSystemSettingsVariable[i]['systemVariableCode'] == 'MINWCP') {
                wissenxMinimumUserCharge = objSystemSettingsVariable[i]['settingValue'];
            }

            if (objSystemSettingsVariable[i]['systemVariableCode'] == 'TAXCRG') {
                taxChargePercentage = objSystemSettingsVariable[i]['settingValue'];
            }
        }


        if (typeof objInvoiceDetail.invoiceId != 'undefined') {

            var invoiceId = objInvoiceDetail.invoiceId;
            var userId = objInvoiceDetail.userId;
            //var invitationId = objInvoiceDetail.invitationId;
            //var meetingDetailId = objInvoiceDetail.meetingDetailId;
            var mentorHourlyCharge = objInvoiceDetail.mentorHourlyCharge;
            var mentorCharge = objInvoiceDetail.mentorCharge;
            var wissenxUserPercentage = objInvoiceDetail.wissenxUserPercentage;
            var wissenxUserCharge = objInvoiceDetail.wissenxUserCharge;
            var wissenxMentorPercentage = objInvoiceDetail.wissenxMentorPercentage;
            var wissenxMentorCharge = objInvoiceDetail.wissenxMentorCharge;
            var promocodeId = objInvoiceDetail.promocodeId;
            var promocodeDiscount = objInvoiceDetail.promocodeDiscount;
            var wissenxTaxPercentage = objInvoiceDetail.wissenxTaxPercentage;
            var wissenxTaxCharge = objInvoiceDetail.wissenxTaxCharge;
            var totalPayableAmount = objInvoiceDetail.totalPayableAmount;
            var payToMentor = objInvoiceDetail.payToMentor;
            var payToWissenx = objInvoiceDetail.payToWissenx;
            //var refundRequestId = objInvoiceDetail.refundRequestId;
            var refundType = objInvoiceDetail.refundType;
            var refundPaidToUser = objInvoiceDetail.refundPaidToUser;
            var afterRefundWissenxAmount = objInvoiceDetail.afterRefundWissenxAmount;
            var afterRefundMentorAmount = objInvoiceDetail.afterRefundMentorAmount;
            var finalPayToWissenx = objInvoiceDetail.finalPayToWissenx;
            var finalPayToMentor = objInvoiceDetail.finalPayToMentor;
            var paidToMentor = objInvoiceDetail.paidToMentor;
            var status = objInvoiceDetail.status;
            var createdDate = objInvoiceDetail.createdDate;
            var updatedTime = objInvoiceDetail.updatedTime;

            var captureId = objInvoiceDetail.captureId;



            var afterDiscountMeetingCost = "0";

            var wissenxUserChargeFactor = "0";

            var afterRefundTotalAmountGoesToMentor = "0";

            var afterRefundWissenxUserCharges = "0";

            var afterRefundWissenxMentorCharge = "0";

            var afterRefundAmountRemainingForMentor = "0";

            var afterRefundTotalAmountGoesToWissenx = "0";

            var refundType = "PARTIALREF";

            if (chkRefundType == "PARTIAL") {

                // edge case, all the calculation will get reverted.
                // discussed with client.
                if (mentorCharge <= wissenxMinimumUserCharge) {

                    afterDiscountMeetingCost = parseFloat(parseFloat(totalPayableAmount) - parseFloat(txtAmountToRefund));

                    if (parseFloat(txtAmountToRefund) <= parseFloat(wissenxMinimumUserCharge)) {
                        afterRefundWissenxUserCharges = parseFloat(parseFloat(wissenxMinimumUserCharge) - parseFloat(txtAmountToRefund));
                    }

                    afterRefundTotalAmountGoesToMentor = parseFloat(parseFloat(afterDiscountMeetingCost) - parseFloat(afterRefundWissenxUserCharges));

                    afterRefundWissenxMentorCharge = parseFloat(parseFloat(afterRefundTotalAmountGoesToMentor) * parseFloat(parseFloat(wissenxMentorPercentage) / parseFloat(100)));

                    afterRefundAmountRemainingForMentor = parseFloat(parseFloat(afterRefundTotalAmountGoesToMentor) - parseFloat(afterRefundWissenxMentorCharge));

                    afterRefundTotalAmountGoesToWissenx = parseFloat(parseFloat(afterRefundWissenxUserCharges) + parseFloat(afterRefundWissenxMentorCharge));
                }
                // ideal scenario.
                else {
                    afterDiscountMeetingCost = parseFloat(parseFloat(totalPayableAmount) - parseFloat(txtAmountToRefund));

                    wissenxUserChargeFactor = parseFloat(parseFloat(parseFloat(100) + parseFloat(wissenxUserPercentage)) / parseFloat(100));

                    afterRefundTotalAmountGoesToMentor = parseFloat(parseFloat(afterDiscountMeetingCost) / parseFloat(wissenxUserChargeFactor));

                    afterRefundWissenxUserCharges = parseFloat(parseFloat(afterDiscountMeetingCost) - parseFloat(afterRefundTotalAmountGoesToMentor));

                    afterRefundWissenxMentorCharge = parseFloat(parseFloat(afterRefundTotalAmountGoesToMentor) * parseFloat(parseFloat(wissenxMentorPercentage) / parseFloat(100)));

                    afterRefundAmountRemainingForMentor = parseFloat(parseFloat(afterRefundTotalAmountGoesToMentor) - parseFloat(afterRefundWissenxMentorCharge));

                    afterRefundTotalAmountGoesToWissenx = parseFloat(parseFloat(afterRefundWissenxUserCharges) + parseFloat(afterRefundWissenxMentorCharge));
                }
            } else {

                refundType = "FULLREF";

            }



            afterRefundTotalAmountGoesToWissenx = parseFloat(afterRefundTotalAmountGoesToWissenx).toFixed(2);
            afterRefundAmountRemainingForMentor = parseFloat(afterRefundAmountRemainingForMentor).toFixed(2);
            afterRefundWissenxUserCharges = parseFloat(afterRefundWissenxUserCharges).toFixed(2);
            afterRefundWissenxMentorCharge = parseFloat(afterRefundWissenxMentorCharge).toFixed(2);


            var params = {
                status: 3,
                final_action: refundType
            };

            var queryParam = [params, refundRequestId];

            var strQuery = " UPDATE refund_request SET ? WHERE refund_request_id = ? ";

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    callback(req, res, false);
                } else {

                    var params = {
                        refund_request_id: refundRequestId,
                        /*refund_type: chkRefundType,*/
                        refund_type: refundType,
                        refund_paid_to_user: txtAmountToRefund,
                        after_refund_wissenx_user_charge: afterRefundWissenxUserCharges,
                        after_refund_wissenx_mentor_charge: afterRefundWissenxMentorCharge,
                        after_refund_wissenx_amount: afterRefundTotalAmountGoesToWissenx,
                        after_refund_mentor_amount: afterRefundAmountRemainingForMentor,
                        final_pay_to_wissenx: afterRefundTotalAmountGoesToWissenx,
                        final_pay_to_mentor: afterRefundAmountRemainingForMentor,
                    };

                    var queryParam = [params, invoiceId];

                    var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            callback(req, res, false);
                        } else {

                            /************************* Refund ***********************/

                            var dataParam = {
                                amount: txtAmountToRefund,
                                captureId: captureId,
                                invitation_id: invitationId,
                                invoiceid: invoiceId,
                                paymentAction: "credited",
                                reason: "REQREFPAY",
                                user_id: meetingUserId,
                                user_type: "user"
                            };
                            paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {

                                var data = {
                                    invoice_id: invoiceId,
                                    user_id: meetingMentorId,
                                    user_type: "mentor",
                                    reason: "REQREFPAY",
                                    transection_type: "PAYREF",
                                    payment_type: "credited",
                                    payment_id: null,
                                    transection_id: null,
                                    capture_id: null,
                                    refund_id: null,
                                    amount: afterRefundAmountRemainingForMentor,
                                    status: "completed",
                                    created_date: constants.CURR_UTC_DATETIME()
                                };
                                paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                                    var objParams = {
                                        user_id: meetingMentorId,
                                        amount: afterRefundAmountRemainingForMentor
                                    };
                                    paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                        var data = {
                                            invoice_id: invoiceId,
                                            user_id: "1",
                                            user_type: "admin",
                                            reason: "REQREFPAY",
                                            transection_type: "PAYREF",
                                            payment_type: "credited",
                                            payment_id: null,
                                            transection_id: null,
                                            capture_id: null,
                                            refund_id: null,
                                            amount: afterRefundTotalAmountGoesToWissenx,
                                            status: 'completed',
                                            created_date: constants.CURR_UTC_DATETIME()
                                        };
                                        paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                            var objParams = {
                                                user_id: "1",
                                                amount: afterRefundTotalAmountGoesToWissenx
                                            };
                                            paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                            });
                                        });
                                    });
                                });
                            });


                            /************************* Refund ***********************/


                            var invitationData = {
                                invitation_id: invitationId,
                                duration: meetingDuration,
                                newRandomUnqRefundRequest: refundRequestUnqId,
                                txtAmountToRefund: txtAmountToRefund,
                                afterRefundAmountRemainingForMentor: afterRefundAmountRemainingForMentor,
                                meeting_url_key: meeting_url_key
                            };

                            userModel.getUserInformation(req, res, meetingMentorId, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                                userModel.getUserInformation(req, res, meetingUserId, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                                    sendRefundRequestAcceptMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    callback(req, res, true);
                                });
                            });
                        }
                    });
                }
            });
        } else {
            callback(req, res, false);
        }
    });

}
exports.processPartialRefund = processPartialRefund;

var sendRefundRequestAcceptMail = function (req, res, arrMentorData, arrUserData, invitationData) {

    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    userModel.getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var requestNumber = invitationData.newRandomUnqRefundRequest;
            var txtAmountToRefund = invitationData.txtAmountToRefund;
            var afterRefundAmountRemainingForMentor = invitationData.afterRefundAmountRemainingForMentor;
            var meeting_url_key = invitationData.meeting_url_key;

            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'acceptedmeetingcancel') {
                var slotsMentorObj = {};
                var slotsUserObj = {};

                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/refundrequestaccepteduser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                requestNumber: requestNumber,
                txtAmountToRefund: txtAmountToRefund,
                afterRefundAmountRemainingForMentor: afterRefundAmountRemainingForMentor,
                meeting_url_key: meeting_url_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ",  your refund request has been approved. Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/refundrequestacceptedusermsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                requestNumber: requestNumber,
                txtAmountToRefund: txtAmountToRefund,
                afterRefundAmountRemainingForMentor: afterRefundAmountRemainingForMentor,
                meeting_url_key: meeting_url_key
            };

            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/refundrequestacceptedmetor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                requestNumber: requestNumber,
                txtAmountToRefund: txtAmountToRefund,
                afterRefundAmountRemainingForMentor: afterRefundAmountRemainingForMentor,
                meeting_url_key: meeting_url_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", Refund request raised by " + arrUserData.firstName + " " + arrUserData.lastName + " has been approved. Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/refundrequestacceptedmetormsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                requestNumber: requestNumber,
                txtAmountToRefund: txtAmountToRefund,
                afterRefundAmountRemainingForMentor: afterRefundAmountRemainingForMentor,
                meeting_url_key: meeting_url_key
            };

            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);


        });
    });
};
exports.sendRefundRequestAcceptMail = sendRefundRequestAcceptMail;


var declineRefundRequest = function (req, res, objDataParams, callback) {

    var taRefundDeclineRequest = objDataParams.taRefundDeclineRequest;
    var refundRequestId = objDataParams.refundRequestId;
    var refundRequestUnqId = objDataParams.refundRequestUnqId;
    var meetingDetailId = objDataParams.meetingDetailId;
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;
    var meetingDuration = objDataParams.meetingDuration;

    getInvoiceDetailForInvitation(req, res, objDataParams, function (req, res, objInvoiceDetail) {

        if (typeof objInvoiceDetail.invoiceId != 'undefined') {

            var invoiceId = objInvoiceDetail.invoiceId;
            var userId = objInvoiceDetail.userId;
            //var invitationId = objInvoiceDetail.invitationId;
            //var meetingDetailId = objInvoiceDetail.meetingDetailId;
            var mentorHourlyCharge = objInvoiceDetail.mentorHourlyCharge;
            var mentorCharge = objInvoiceDetail.mentorCharge;
            var wissenxUserPercentage = objInvoiceDetail.wissenxUserPercentage;
            var wissenxUserCharge = objInvoiceDetail.wissenxUserCharge;
            var wissenxMentorPercentage = objInvoiceDetail.wissenxMentorPercentage;
            var wissenxMentorCharge = objInvoiceDetail.wissenxMentorCharge;
            var promocodeId = objInvoiceDetail.promocodeId;
            var promocodeDiscount = objInvoiceDetail.promocodeDiscount;
            var wissenxTaxPercentage = objInvoiceDetail.wissenxTaxPercentage;
            var wissenxTaxCharge = objInvoiceDetail.wissenxTaxCharge;
            var totalPayableAmount = objInvoiceDetail.totalPayableAmount;
            var payToMentor = objInvoiceDetail.payToMentor;
            var payToWissenx = objInvoiceDetail.payToWissenx;
            //var refundRequestId = objInvoiceDetail.refundRequestId;
            var refundType = objInvoiceDetail.refundType;
            var refundPaidToUser = objInvoiceDetail.refundPaidToUser;
            var afterRefundWissenxAmount = objInvoiceDetail.afterRefundWissenxAmount;
            var afterRefundMentorAmount = objInvoiceDetail.afterRefundMentorAmount;
            var finalPayToWissenx = objInvoiceDetail.finalPayToWissenx;
            var finalPayToMentor = objInvoiceDetail.finalPayToMentor;
            var paidToMentor = objInvoiceDetail.paidToMentor;
            var status = objInvoiceDetail.status;
            var createdDate = objInvoiceDetail.createdDate;
            var updatedTime = objInvoiceDetail.updatedTime;


            var refundType = "CANREF";
            var params = {
                status: 3,
                final_action: refundType
            };

            var queryParam = [params, refundRequestId];

            var strQuery = " UPDATE refund_request SET ? WHERE refund_request_id = ? ";

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    callback(req, res, false);
                } else {

                    var params = {
                        refund_request_id: refundRequestId,
                        refund_type: refundType,
                        refund_paid_to_user: 0.0000,
                        after_refund_wissenx_amount: payToWissenx,
                        after_refund_mentor_amount: payToMentor,
                        final_pay_to_wissenx: payToWissenx,
                        final_pay_to_mentor: payToMentor,
                    };

                    var queryParam = [params, invoiceId];

                    var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            callback(req, res, false);
                        } else {

                            /************************* Refund ***********************/

                            var data = {
                                invoice_id: invoiceId,
                                user_id: meetingMentorId,
                                user_type: "mentor",
                                reason: "DECREQREFPAY",
                                transection_type: "PAYREF",
                                payment_type: "credited",
                                payment_id: null,
                                transection_id: null,
                                capture_id: null,
                                refund_id: null,
                                amount: payToMentor,
                                status: "completed",
                                created_date: constants.CURR_UTC_DATETIME()
                            };
                            paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                                var objParams = {
                                    user_id: meetingMentorId,
                                    amount: payToMentor
                                };
                                paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                    var data = {
                                        invoice_id: invoiceId,
                                        user_id: "1",
                                        user_type: "admin",
                                        reason: "DECREQREFPAY",
                                        transection_type: "PAYREF",
                                        payment_type: "credited",
                                        payment_id: null,
                                        transection_id: null,
                                        capture_id: null,
                                        refund_id: null,
                                        amount: payToWissenx,
                                        status: 'completed',
                                        created_date: constants.CURR_UTC_DATETIME()
                                    };
                                    paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                        var objParams = {
                                            user_id: "1",
                                            amount: payToWissenx
                                        };
                                        paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                        });
                                    });
                                });
                            });



                            /************************* Refund ***********************/

                            var invitationData = {
                                invitation_id: invitationId,
                                duration: meetingDuration,
                                newRandomUnqRefundRequest: refundRequestUnqId,
                                decline_reason: taRefundDeclineRequest
                            };

                            userModel.getUserInformation(req, res, meetingMentorId, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                                userModel.getUserInformation(req, res, meetingUserId, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                                    sendRefundRequestDeclineMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    callback(req, res, true);
                                });
                            });
                        }
                    });
                }
            });
        } else {
            callback(req, res, false);
        }
    });

};
exports.declineRefundRequest = declineRefundRequest;

var sendRefundRequestDeclineMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    userModel.getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var requestNumber = invitationData.newRandomUnqRefundRequest;
            var decline_reason = invitationData.decline_reason;

            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'acceptedmeetingcancel') {
                var slotsMentorObj = {};
                var slotsUserObj = {};

                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/refundrequestdeclined",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                requestNumber: requestNumber,
                declineReason: decline_reason
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", Your refund request has been reviewed. Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/refundrequestdeclinedmsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                requestNumber: requestNumber,
                declineReason: decline_reason
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendRefundRequestDeclineMail = sendRefundRequestDeclineMail;


var getAdminMessagesInfoForReundRequest = function (req, res, objDataParams, callback) {
    var refundRequestId = objDataParams.refundRequestId;

    var queryParam = [refundRequestId];

    var strQuery = '    SELECT armm.admin_refund_messages_master_id adminRefundMessageMasterId, armm.refund_request_id refundRequstId, armm.refund_request_unq_id refundRequestUnqId, armm.created_date createdDate ' +
            '   FROM admin_refund_messages_master armm' +
            '   WHERE armm.refund_request_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getAdminMessagesInfoForReundRequest = getAdminMessagesInfoForReundRequest;


var getAdminMessagesForRefundRequest = function (req, res, objDataParams, callback) {
    var refundRequestId = objDataParams.refundRequestId;
    var userType = objDataParams.userType;

    var queryParam = [refundRequestId, userType];

    var strQuery = '    SELECT armm.admin_refund_messages_master_id adminRefundMessageMasterId, armm.refund_request_id refundRequestId, armm.refund_request_unq_id refundRequestUniqId, armt.admin_refund_messages_thread_id adminRefundMessageThreadId, armt.user_id userId, armt.typex userType, arm.admin_refund_messages_id adminRefundMessageId, arm.message_from messageFrom, arm.message message, arm.created_date messageSendDate ' +
            '   FROM admin_refund_messages_master armm, admin_refund_messages_thread armt, admin_refund_messages arm ' +
            '   WHERE armm.refund_request_id = ? ' +
            '   AND armt.admin_refund_messages_master_id = armm.admin_refund_messages_master_id ' +
            '   AND armt.typex = ? ' +
            '   AND arm.admin_refund_messages_thread_id = armt.admin_refund_messages_thread_id';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            var i = 0;
            results.forEach(function (element) {
                results[i].displayDateTime = moment.tz(element.messageSendDate, 'UTC').tz(req.session.adminActiveSession.adminTimeZoneLabel).format('YYYY-MM-DD HH:MM:ss');
                i++;
            });

            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });

}
exports.getAdminMessagesForRefundRequest = getAdminMessagesForRefundRequest;



var sendRefundRequestMessageToUsers = function (req, res, objDataParams, callback) {
    var adminRefundMessageMasterId = objDataParams.adminRefundMessageMasterId;
    var message = objDataParams.message;
    var sendMessageFor = objDataParams.sendMessageFor;
    var refundRequestId = objDataParams.refundRequestId;
    var refundRequestUnqId = objDataParams.refundRequestUnqId;
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objDataToPass = {
        adminRefundMessageMasterId: adminRefundMessageMasterId,
        sendMessageFor: sendMessageFor
    }

    getAdminRefundMessageThreadDetail(req, res, objDataToPass, function (req, res, objAdminRefundMessageThreadInfo) {
        console.log("objAdminRefundMessageThreadInfo :: ", objAdminRefundMessageThreadInfo);
        if (typeof objAdminRefundMessageThreadInfo.adminRefundMessageThreadId != 'undefined') {
            var adminRefundMessageThreadId = objAdminRefundMessageThreadInfo.adminRefundMessageThreadId;
            var adminRefundMessageMasterId = objAdminRefundMessageThreadInfo.adminRefundMessageMasterId;
            var userId = objAdminRefundMessageThreadInfo.userId;
            var userType = objAdminRefundMessageThreadInfo.userType;
            var createdDate = objAdminRefundMessageThreadInfo.createdDate;

            var currentUTC = constants.CURR_UTC_DATETIME();

            var objMessageToUpdateUnreadCount = {
                adminRefundMessageThreadId: adminRefundMessageThreadId
            };
            updateMessageThreadUnreadCount(req, res, objMessageToUpdateUnreadCount, function (req, res, objResponse) {

                var params = {
                    admin_refund_messages_thread_id: adminRefundMessageThreadId,
                    message_from: "A",
                    message: message,
                    created_date: currentUTC
                };

                var strQuery = 'INSERT INTO admin_refund_messages SET ? ';

                dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
                    if (error) {
                        callback(req, res, {status: false, currentUTCTime: currentUTC});
                    } else {
                        callback(req, res, {status: true, currentUTCTime: currentUTC});
                    }
                });

                var objParamsToMailSend = {
                    receiver_id: userId,
                    sendername: "Wissenx Admin",
                    message: message
                };
                messagesModel.sendUserMessagesEmailAdmin(req, res, objParamsToMailSend);
            });
        } else {
            callback(req, res, {status: false, currentUTCTime: currentUTC});
        }
    });

};
exports.sendRefundRequestMessageToUsers = sendRefundRequestMessageToUsers;


var updateMessageThreadUnreadCount = function (req, res, objDataParams, callback) {

    var adminRefundMessageThreadId = objDataParams.adminRefundMessageThreadId;
    var params = {};

    var queryParam = [params, adminRefundMessageThreadId];
    var strQuery = " UPDATE admin_refund_messages_thread SET unread_count = unread_count + 1 WHERE admin_refund_messages_thread_id = " + adminRefundMessageThreadId;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateMessageThreadUnreadCount = updateMessageThreadUnreadCount;



var getAdminRefundMessageThreadDetail = function (req, res, objDataParams, callback) {
    var adminRefundMessageMasterId = objDataParams.adminRefundMessageMasterId;
    var sendMessageFor = objDataParams.sendMessageFor;

    var queryParam = [adminRefundMessageMasterId, sendMessageFor];

    var strQuery = '    SELECT armt.admin_refund_messages_thread_id adminRefundMessageThreadId, armt.admin_refund_messages_master_id adminRefundMessageMasterId, armt.user_id userId, armt.typex userType, armt.created_date createdDate ' +
            '   FROM admin_refund_messages_thread armt ' +
            '   WHERE armt.admin_refund_messages_master_id = ? ' +
            '   AND armt.typex = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getAdminRefundMessageThreadDetail = getAdminRefundMessageThreadDetail;




var getInvoiceDetailForInvitation = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;

    var queryParam = [invitationId];
    var strQuery = '    Select i.invoice_id invoiceId, i.user_id userId, i.invitation_id invitationId, i.meeting_id meetingDetailId, i.mentor_hourly_charge mentorHourlyCharge, i.mentor_charge mentorCharge, i.wissenx_user_percentage wissenxUserPercentage, i.wissenx_user_charge wissenxUserCharge, i.wissenx_mentor_percentage wissenxMentorPercentage, i.wissenx_mentor_charge wissenxMentorCharge, i.promocode_id promocodeId, i.promocode_discount promocodeDiscount, i.wissenx_tax_percentage wissenxTaxPercentage, i.wissenx_tax_charge wissenxTaxCharge, i.total_payable_amount totalPayableAmount, i.pay_to_mentor payToMentor, i.pay_to_wissenx payToWissenx, i.refund_request_id refundRequestId, i.refund_type refundType, i.refund_paid_to_user refundPaidToUser, i.after_refund_wissenx_amount afterRefundWissenxAmount, i.after_refund_mentor_amount afterRefundMentorAmount, i.final_pay_to_wissenx finalPayToWissenx, i.final_pay_to_mentor finalPayToMentor, i.paid_to_mentor paidToMentor, i.paid_to_wissenx paidToWissenx, i.status status, i.created_date createdDate, i.updated_time updatedTime, ' +
            '   pp.capture_id captureId  ' +
            '   From invoice i' +
            '   LEFT JOIN payments_paypal pp ON i.invoice_id = pp.invoice_id ' +
            '   WHERE i.invitation_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, []);
            }
        }
    });
};
exports.getInvoiceDetailForInvitation = getInvoiceDetailForInvitation;


var suspendUser = function (req, res, callback) {
    var reqBody = req.body;
    var suspendReason = reqBody.suspendReason;
    var suspendReasonDesc = reqBody.suspendReasonDesc;
    var suspendUser = reqBody.suspendUser;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var data = {
        user_id: suspendUser,
        reason: suspendReason,
        description: suspendReasonDesc,
        created_date: currentUTC
    }

    var queryParam = [data];
    var strQuery = 'insert into user_suspended set ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            var queryParam = [results.insertId, suspendUser];
            var strQuery = 'update users set status = 3 , suspend_reason_id = ? where user_id = ?';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                } else {
                    var queryParam = [suspendUser];
                    var strQuery = 'select * from users u LEFT JOIN user_details ud ON u.user_id = ud.user_id where u.user_id = ?';

                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            var email = results[0].email;
                            var first_name = results[0].first_name;
                            var last_name = results[0].last_name;

                            // Send mail to user
                            var templateVariable = {
                                templateURL: "mailtemplate/accountsuspended",
                                frontConstant: frontConstant,
                                first_name: first_name,
                                last_name: last_name
                            };

                            var mailParamsObject = {
                                templateVariable: templateVariable,
                                to: email,
                                subject: "Your account has been temporarily suspended"
                            };
                            mailer.sendMail(req, res, mailParamsObject, function (err) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    callback(req, res, "success");
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
exports.suspendUser = suspendUser;

var continueUser = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.suspendUser;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, suspendUser];
    var strQuery = 'update users set status = 1 , suspend_reason_id = 0 , updated_date = ? where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}

exports.continueUser = continueUser;

var makeMentorCelebrity = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.mentorId;

    var queryParam = [suspendUser];
    var strQuery = 'update mentor_details set is_featured = 1 where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            callback(req, res, "success");
        }
    });

}
exports.makeMentorCelebrity = makeMentorCelebrity;

var removeMentorCelebrity = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.mentorId;

    var queryParam = [suspendUser];
    var strQuery = 'update mentor_details set is_featured = 0 where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            callback(req, res, "success");
        }
    });

}
exports.removeMentorCelebrity = removeMentorCelebrity;
var removeMentorCelebrity = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.mentorId;

    var queryParam = [suspendUser];
    var strQuery = 'update mentor_details set is_featured = 0 where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            callback(req, res, "success");
        }
    });

}
exports.removeMentorCelebrity = removeMentorCelebrity;

var hideReviewRating = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.mentorId;

    var queryParam = [suspendUser];
    var strQuery = 'update mentor_details set hide_review_rating = 1 where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            callback(req, res, "success");
        }
    });

}
exports.hideReviewRating = hideReviewRating;

var showReviewRating = function (req, res, callback) {
    var reqBody = req.body;
    var suspendUser = reqBody.mentorId;

    var queryParam = [suspendUser];
    var strQuery = 'update mentor_details set hide_review_rating = 0 where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            callback(req, res, "success");
        }
    });

}
exports.showReviewRating = showReviewRating;

var changeStatusForCountry = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var countryId = objDataParams.countryId;
    var regionId = objDataParams.regionId;
    var params = {
        status: statusToChange
    };

    async.parallel([
        function (updateStatus) {
            if (statusToChange == "1") {
                var queryParam = [params, regionId];
                var strQuery = " UPDATE master_region SET ? WHERE region_id = ? ";
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        updateStatus(null, false);
                    } else {
                        updateStatus(null, true);
                    }
                });
            } else {
                updateStatus(null, true);
            }
        },
        function (updateStatus) {
            var queryParam = [params, countryId];
            var strQuery = " UPDATE countries SET ? WHERE country_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [statusToChange, countryId];
            var strQuery = " UPDATE states SET status = ? WHERE country_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [statusToChange, countryId];
            var strQuery = " UPDATE cities SET status = ? WHERE country_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        }
    ], function (error, result) {
        var regionUpdate = result[0];
        var countryUpdate = result[1];
        var stateUpdate = result[2];
        var cityUpdate = result[3];

        if (!regionUpdate || !countryUpdate || !stateUpdate || !cityUpdate) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }

    });
};
exports.changeStatusForCountry = changeStatusForCountry;

var getStatesListForAdmin = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var countryId = objParams.countryId;
    var stateid = objParams.stateid;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';


    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

//    var countryId = '0';
//    if (typeof objParams.countryId != 'undefined') {
//        countryId = objParams.countryId;
//    }

    var queryParam = {};

    var strSelect = 'state_id ,country_id ,state_name,status ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    //var strWhereQuery = ' c.status <> 0 ';
    var strWhereQuery = ' (status = 0 || status = 1) ';
    if (searchKey != '') {
        strWhereQuery += '   AND ( ' +
                '       state_name LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (countryId > 0) {
        strWhereQuery += '   AND country_id = ' + countryId;
    }

    if (stateid > 0) {
        strWhereQuery += '   AND state_id = ' + stateid;
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM states ' +
            ' WHERE ' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " state_name ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getStatesListForAdmin = getStatesListForAdmin;

var deleteState = function (req, res, objDataParams, callback) {
    var stateid = objDataParams.stateid;

    var queryParam = [stateid];
    var strQuery = 'UPDATE states s SET s.status = 2 WHERE s.state_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.deleteState = deleteState;

var changeStatusForState = function (req, res, objDataParams, callback) {
    var statusToChange = objDataParams.statusToChange;
    var stateId = objDataParams.stateId;
    var params = {
        status: statusToChange
    };

    async.parallel([
        function (updateStatus) {
            var queryParam = [statusToChange, stateId];
            var strQuery = " UPDATE states SET status = ? WHERE state_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        },
        function (updateStatus) {
            var queryParam = [statusToChange, stateId];
            var strQuery = " UPDATE cities SET status = ? WHERE state_id = ? ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    updateStatus(null, false);
                } else {
                    updateStatus(null, true);
                }
            });
        }
    ], function (error, result) {
        var stateUpdate = result[0];
        var cityUpdate = result[1];

        if (!stateUpdate || !cityUpdate) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }

    });
};
exports.changeStatusForState = changeStatusForState;

var addState = function (req, res, objDataParams, callback) {
    var lstCountry = objDataParams.lstCountry;
    var txtStateName = objDataParams.txtStateName;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        country_id: lstCountry,
        state_name: txtStateName,
        status: "1",
        created_date: currentUTC
    };

    var strQuery = 'INSERT INTO states SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.addState = addState;

var updateState = function (req, res, objDataParams, callback) {

    var lstCountry = objDataParams.lstCountry;
    var txtStateName = objDataParams.txtStateName;
    var hidStateId = objDataParams.hidStateId;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        country_id: lstCountry,
        state_name: txtStateName,
        status: "1",
        updated_date: currentUTC
    };

    var queryParam = [params, hidStateId];
    var strQuery = " UPDATE states SET ? WHERE state_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateState = updateState;


var checkStatesUrl = function (req, res, objDataParams, callback) {
    var stateId = objDataParams.stateId;
    var countryId = objDataParams.countryId;

    var queryParam = [countryId, stateId];
    var strQuery = '    SELECT count(*) totalCount ' +
            '   FROM states s, countries c ' +
            '   WHERE (s.status = "0" OR  s.status = "1") ' +
            '   AND (c.status = "0" OR  c.status = "1") ' +
            '   AND s.country_id = c.country_id ' +
            '   AND s.country_id = ? ' +
            '   AND s.state_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, []);
        }
    });
}
exports.checkStatesUrl = checkStatesUrl;


var getReportData = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';

    var user_type = objParams.user_type;
    var transTypefilter = objParams.transTypefilter;
    var filterSD = objParams.filterSD;
    var filterED = objParams.filterED;
    var timezone = req.session.USERTZBROWSER;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    //var strSelect = 'ABS(wt.amount) amount,md.meeting_url_key ,DATE_FORMAT(wt.created_date,"%m/%d/%Y %h:%i:%s") created_date, COALESCE(wt.refund_id , wt.transection_id ) transection_id,CONCAT(ud.first_name," ",ud.last_name) name,wt.reason,UPPER(wt.payment_type) payment_type ';
    var strSelect = 'ABS(wt.amount) amount,md.meeting_url_key ,DATE_FORMAT(CONVERT_TZ(wt.created_date, "' + constants.UTCTZ + '", "' + timezone + '"),"%m/%d/%Y %h:%i:%s") created_date, COALESCE(wt.refund_id , wt.transection_id ) transection_id,CONCAT(ud.first_name," ",ud.last_name) name,wt.reason,UPPER(wt.payment_type) payment_type ';
    if (!paging) {
        strSelect = ' COUNT(wt.id) totalCount ';
    }


    var strWhereQuery = ' 1 ';
    if (searchKey != '') {
        strWhereQuery += '   AND ud.first_name LIKE "%' + searchKey + '%" ';
    }

    if (user_type != "0") {
        strWhereQuery += '   AND wt.user_type = "' + user_type + '"';
    }

    if (transTypefilter == 'credited') {
        strWhereQuery += '   AND (wt.payment_type = "credited" OR wt.payment_type = "CREDITED")';
    }

    if (transTypefilter == 'debited') {
        strWhereQuery += '   AND (wt.payment_type = "debited" OR wt.payment_type = "DEBIT")';
    }

    if (filterSD != '' && filterED != '') {
        strWhereQuery += '   AND wt.created_date >= " ' + filterSD + ' 00:00:00" AND wt.created_date <= " ' + filterED + ' 23:00:00"';
    }



    var strQuery = ' SELECT ' + strSelect +
            ' FROM wallet_transection wt ' +
            ' LEFT JOIN user_details ud ON wt.user_id = ud.user_id ' +
            ' LEFT JOIN invoice ins ON wt.invoice_id = ins.invoice_id ' +
            ' LEFT JOIN meeting_details md ON ins.invitation_id = md.invitation_id ' +
            ' WHERE ' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " wt.created_date ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        mentorModel.getMsgForShortCode(req, res, results, 0, [], function (req, res, objFinalData) {
            callback(req, res, objFinalData);
        });
    });

};
exports.getReportData = getReportData;


var getCurrentMonthMeetings = function (req, res, callback) {

    var strQuery = 'Select COUNT(invitation_id) meetingCount,DATE_FORMAT(NOW(),"%M %Y") currentDate from meeting_invitations WHERE  status = "completed" AND updated_date  >= DATE_FORMAT(NOW() ,"%Y-%m-01") AND updated_date <=  now()';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getCurrentMonthMeetings = getCurrentMonthMeetings;

var getCurrentYearMeetings = function (req, res, callback) {

    var strQuery = 'Select COUNT(invitation_id) meetingCount,DATE_FORMAT(NOW(),"%Y") currentDate from meeting_invitations WHERE  status = "completed" AND updated_date  >= DATE_FORMAT(NOW() ,"%Y-01-01") AND updated_date <=  now()';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getCurrentYearMeetings = getCurrentYearMeetings;

var getAllMeetings = function (req, res, callback) {

    var strQuery = 'Select COUNT(invitation_id) meetingCount from meeting_invitations WHERE  status = "completed"';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getAllMeetings = getAllMeetings;

var getCurrentMonthEarning = function (req, res, callback) {

    var strQuery = 'Select ROUND(SUM(amount),2) amount,DATE_FORMAT(NOW(),"%M %Y") currentDate from wallet_transection WHERE user_id = 1 AND created_date  >= DATE_FORMAT(NOW() ,"%Y-%m-01") AND created_date <=  now()';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getCurrentMonthEarning = getCurrentMonthEarning;

var getCurrentYearEarning = function (req, res, callback) {

    var strQuery = 'Select ROUND(SUM(amount),2) amount,DATE_FORMAT(NOW(),"%Y") currentDate from wallet_transection WHERE user_id = 1 AND created_date  >= DATE_FORMAT(NOW() ,"%Y-01-01") AND created_date <=  now()';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getCurrentYearEarning = getCurrentYearEarning;

var getAllEarning = function (req, res, callback) {

    var strQuery = 'Select ROUND(SUM(amount),2) amount from wallet_transection WHERE user_id = 1';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getAllEarning = getAllEarning;

var getRecentMentor = function (req, res, callback) {

    var strQuery = 'Select urs.email,CONCAT(ud.first_name," ",ud.last_name) name, IF(urs.status = 1,"Active","InActive")  STATUS ,IF(md.company_name = "","Self Employed",md.company_name) company_name ' +
            ' from users  urs ' +
            ' LEFT JOIN mentor_details md ON md.user_id = urs.user_id , user_details ud ' +
            ' WHERE urs.user_type = "mentor" and urs.user_id = ud.user_id ORDER BY urs.created_date DESC LIMIT 0 , 5';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results);
        }
    });
};
exports.getRecentMentor = getRecentMentor;

var getRecentUser = function (req, res, callback) {

    var strQuery = 'Select urs.email,CONCAT(ud.first_name," ",ud.last_name) name, IF(urs.status = 1,"Active","InActive")  STATUS ,urs.phone_number ' +
            ' from users  urs , user_details ud ' +
            ' WHERE urs.user_type = "user" and urs.user_id = ud.user_id ORDER BY urs.created_date DESC LIMIT 0 , 5';
    dbconnect.executeQuery(req, res, strQuery, "", function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results);
        }
    });
};
exports.getRecentUser = getRecentUser;

var getMentorList = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';
    var filterUserStatus = objParams.filterUserStatus;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    var strSelect = ' CONCAT_WS(" ", ud.first_name, ud.last_name) fullName, u.user_type userType, u.slug slug, u.email email, u.phone_number, u.status, u.user_id , md.wallet_balance,ct.minpayout,ct.maxpayout,ct.maxpayoutperweek ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = ' ud.user_id = u.user_id ';
    if (searchKey != '') {
        strWhereQuery += ' AND ( ' +
                '       ud.first_name LIKE "%' + searchKey + '%" OR ' +
                '       ud.last_name LIKE "%' + searchKey + '%" OR ' +
                '       u.user_type LIKE "%' + searchKey + '%" OR ' +
                '       u.email LIKE "%' + searchKey + '%" OR ' +
                '       u.phone_number LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (filterUserStatus != "") {
        switch (filterUserStatus) {
            case "A":
                strWhereQuery += '   AND u.status = "1" ';
                break;
            case "S":
                strWhereQuery += '   AND u.status = "2" ';
                break;
        }
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM users u, user_details ud ' +
            ' LEFT JOIN mentor_details md ON ud.user_id = md.user_id ' +
            ' LEFT JOIN countries ct ON ud.country_id = ct.country_id ' +
            ' WHERE  ' + strWhereQuery + ' AND u.user_type = "mentor" AND u.user_id != "1"';

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " ud.first_name ";
                break;
            case '1':
                strOrderBy = " u.user_type ";
                break;
            case '2':
                strOrderBy = " u.email ";
                break;
            case '3':
                strOrderBy = " u.phone_number ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getMentorList = getMentorList;

var payoutMentorWallet = function (req, res, objDataParams, callback) {
    var payoutAmount = objDataParams.payoutAmount;
    var id = objDataParams.id;
    var user_id = objDataParams.user_id;
    var mentorName = objDataParams.mentorName;
    var mentorEmail = objDataParams.mentorEmail;
    var mentorpayaplEmail = objDataParams.mentorpaypalEmail;
    var walBal = objDataParams.walBal;
    var dataParam = {};
    var maxpayout;
    dataParam = {
        txtEmail: mentorpayaplEmail,
        txtAmount: payoutAmount,
        user_type: "mentor",
        user_id: user_id,
        id: id
    };

    paypalFunction.paypalPayout(req, res, dataParam, function (req, res, payment) {
        if (payment == 'error') {
            console.log('error');
        } else {

            async.parallel([
                function (updateStatus) {
                    var queryParam = [payoutAmount, user_id];
                    var strQuery = 'UPDATE mentor_details SET wallet_balance = wallet_balance - ? where user_id = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            updateStatus(null, false);
                        } else {
                            updateStatus(null, true);
                        }
                    });
                },
                function (updateStatus) {
//                    var queryParam = {
//                        mentor_id: user_id,
//                        amount: payoutAmount,
//                        created_date: constants.CURR_UTC_DATETIME()
//                    };
//                    var strQuery = "insert into mentor_payout_history set ?";
//                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
//                        if (error) {
//                            updateStatus(null, false);
//                        } else {
//                            updateStatus(null, true);
//                        }
//                    });

                updateStatus(null, true);
                },function(updateStatus){
                    var queryParam = [user_id]
                    var strQuery = ' SELECT ud.country_id ,ct.maxpayout' +
                                     ' FROM user_details ud ' +
                                     ' LEFT JOIN countries ct ON ud.country_id = ct.country_id ' +
                                     ' WHERE ud.user_id = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            updateStatus(null, false);
                        } else {
                            console.log('resultss',results)
                            updateStatus(null, results);
                        }
                    });
                }
            ], function (error, result) {
                var walletUpdate = result[0];
                var historyUpdate = result[1];

                var maxpayoutamt = JSON.parse(JSON.stringify(result[2]));
                    maxpayout= maxpayoutamt[0].maxpayout
                if (!walletUpdate || !historyUpdate) {
                    callback(req, res, "");
                } else {
                  var objParams = {
                        payoutAmount: payoutAmount,
                        name: mentorName,
                        email: mentorEmail,
                        walBal: walBal,
                        maxpayout:maxpayout
                    };

                    sendPayoutMailMentor(req, res, user_id, objParams, "success", function (req, res, returnedObj) {
                        if (returnedObj != "error") {
                            sendPayoutMailAdmin(req, res, user_id, objParams, "success", function (req, res, returnedObjA) {                                
                                callback(req, res, "success");
                            });
                        } else {
                            returnedObj.msg = "error";
                            callback(req, res, returnedObj);
                        }
                    });
                    //callback(req, res, finalObj);
                }

            });
        }
    });
};
exports.payoutMentorWallet = payoutMentorWallet;

var sendPayoutMailMentor = function (req, res, userId, objParams, finalObj, callback) {
    // fetched user data.
    console.log(objParams);
    if (objParams.mentorEmail !== 'undefined') {
        var email = objParams.email;
        var userName = objParams.name;        
        var walBal = parseFloat(objParams.walBal).toFixed(2);
        var amount = parseFloat(objParams.payoutAmount).toFixed(2);
        var sumamount = parseFloat(walBal) - parseFloat(amount);
        var finalamount = parseFloat(sumamount).toFixed(2);
        var maxpayout=parseFloat(objParams.maxpayout).toFixed(2);

        var templateFile = fs.readFileSync('./views/mailtemplate/mentorpayoutmsg.handlebars', 'utf8');
        var context = {
            userName: userName,
            amount: amount,
            walBal: walBal,
            finalamount: finalamount,
            frontConstant: frontConstant            
        };
        var template = handlebars.compile(templateFile);
        var html = template(context);
        req.body.lstUserList = userId;
        req.body.msgNewBody = html;
        req.body.sender_id = 1;
        req.body.sendNoResponse = 1;
        messagesModel.sendUserMessages(req, res);

        var templateVariable = {
            templateURL: "mailtemplate/mentorpayoutmail",
            userName: userName,
            amount: amount,
            walBal: walBal,
            finalamount: finalamount,
            frontConstant: frontConstant,
            maxDayPayout:maxpayout            
        };
        console.log(userName);
        var mailParamsObject = {
            templateVariable: templateVariable,
            to: email,
            subject: 'Hi ' + userName + ',SourceAdvisor has transferred funds to your PayPal account.'
        };
        mailer.sendMail(req, res, mailParamsObject, function (err) {
            if (err!==null) {
                callback(req, res, "error");
            } else {
                callback(req, res, finalObj);
            }
        });
    } else {
        callback(req, res, "error");
    }
};
exports.sendPayoutMailMentor = sendPayoutMailMentor;

var sendPayoutMailAdmin = function (req, res, userId, objParams, finalObj, callback) {
    // fetched user data.

    if (objParams.mentorEmail !== 'undefined') {
        var email = objParams.email;
        var userName = objParams.name;
        var walBal = parseFloat(objParams.walBal).toFixed(2);
        var amount = parseFloat(objParams.payoutAmount).toFixed(2);
        var sumamount = parseFloat(walBal) - parseFloat(amount);
        var finalamount = parseFloat(sumamount).toFixed(2);
        var maxpayout=parseFloat(objParams.maxpayout).toFixed(2);
        var templateVariable = {
            templateURL: "mailtemplate/adminpayoutmail",
            userName: userName,
            amount: amount,
            walBal: walBal,
            finalamount: finalamount,
            frontConstant: frontConstant,
            maxDayPayout:maxpayout

        };
        var mailParamsObject = {
            templateVariable: templateVariable,
            to: frontConstant.ADMIN_EMAIL,
            subject: 'Hi Admin, We have transferred funds to ' + userName
        };
        mailer.sendMail(req, res, mailParamsObject, function (err) {
            if (err) {
                console.log(err);
                callback(req, res, "error");
            } else {
                callback(req, res, finalObj);
            }
        });
    }
    // no user data found
    else {
        callback(req, res, "error");
    }
};
exports.sendPayoutMailAdmin = sendPayoutMailAdmin;

var chkWeekAmt = function (req, res, objParams, callback) {

    var payoutAmount = objParams.payoutAmount;
    var user_id = objParams.user_id;
    var weekPayout = objParams.weekPayout;
    var param = [user_id];
    var finalObj = {};
    var strQuery = 'Select  SUM(amount) as amount,COUNT(payout_id) as maxCount ' +
            ' from mentor_payout_history ' +
            ' WHERE mentor_id = ? and YEARWEEK(created_date) = YEARWEEK(NOW())';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var maxCount = results[0].maxCount;
                var objDataParams = {
                    systemSettingVariables: ['MWPYT']
                };
                miscFunction.getSystemSettingVariable(req, res, objDataParams, function (req, res, objSystemSettingResponse) {
                    if (objSystemSettingResponse != undefined) {
                        var maxAllPay = objSystemSettingResponse[0].settingValue;
                        if (maxCount < maxAllPay) {
                            var tot = results[0].amount;
                            var totAmt = parseFloat(tot) + parseFloat(payoutAmount);
                            var totsend = parseFloat(weekPayout) - parseFloat(totAmt);
                            if (totAmt >= weekPayout) {
                                finalObj = {
                                    msg: "stop",
                                    amt: totsend,
                                    wAmt: weekPayout,
                                    pcnt: 0,
                                    acnt: 0
                                };
                                callback(req, res, finalObj);
                            } else {
                                finalObj = {
                                    msg: "continue",
                                    amt: 0,
                                    wAmt: weekPayout,
                                    pcnt: 0,
                                    acnt: 0
                                };
                                callback(req, res, finalObj);
                            }
                        } else {
                            finalObj = {
                                msg: "stop",
                                amt: totsend,
                                wAmt: weekPayout,
                                pcnt: maxCount,
                                acnt: maxAllPay
                            };
                            callback(req, res, finalObj);
                        }
                    }
                });
            } else {
                finalObj = {
                    msg: "continue",
                    amt: 0,
                    wAmt: weekPayout,
                    pcnt: 0,
                    acnt: 0
                };
                callback(req, res, finalObj);
            }
        }
    });
};
exports.chkWeekAmt = chkWeekAmt;


var getMentorPayoutDetails = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';
    var mentorId = objParams.mentorId;
    var adminTZ = req.session.adminActiveSession.adminTimeZoneLabel;
    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    var strSelect = ' CONCAT_WS(" ", ud.first_name, ud.last_name) fullName, u.user_type userType, u.slug slug, u.email email, u.phone_number, u.status, u.user_id , md.wallet_balance,mph.amount,DATE_FORMAT(CONVERT_TZ(mph.created_date, "UTC", "' + adminTZ + '"), "%m/%d/%Y %h:%i:%s") cdate ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = ' ud.user_id = u.user_id ';
    if (searchKey != '') {
        strWhereQuery += ' AND ( ' +
                '       ud.first_name LIKE "%' + searchKey + '%" OR ' +
                '       ud.last_name LIKE "%' + searchKey + '%" OR ' +
                '       u.user_type LIKE "%' + searchKey + '%" OR ' +
                '       u.email LIKE "%' + searchKey + '%" OR ' +
                '       u.phone_number LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (mentorId != "") {
        strWhereQuery += '   AND mph.mentor_id = "' + mentorId + '" ';
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM users u, user_details ud ' +
            ' LEFT JOIN mentor_details md ON ud.user_id = md.user_id ' +
            ' LEFT JOIN mentor_payout_history mph ON ud.user_id = mph.mentor_id ' +
            ' WHERE  ' + strWhereQuery + ' AND u.user_type = "mentor" AND u.user_id != "1"';

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " ud.first_name ";
                break;
            case '1':
                strOrderBy = " u.user_type ";
                break;
            case '2':
                strOrderBy = " u.email ";
                break;
            case '3':
                strOrderBy = " u.phone_number ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getMentorPayoutDetails = getMentorPayoutDetails;

var getBroadCastData = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';
    var filterUserStatus = objParams.filterUserStatus;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    var strSelect = ' *,DATE_FORMAT(start_date, "%m/%d/%Y") as start_date ,  DATE_FORMAT(end_date, "%m/%d/%Y") as end_date ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = '';
    if (searchKey != '') {
        strWhereQuery += ' WHERE ( bs.msg LIKE "%' + searchKey + '%" ) ';
    }

//    if (filterUserStatus != "") {
//        switch (filterUserStatus) {
//            case "A":
//                strWhereQuery += '   AND u.status = "1" ';
//                break;
//            case "S":
//                strWhereQuery += '   AND u.status = "2" ';
//                break;
//        }
//    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM broadcast_messages bs' + strWhereQuery;

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " bs.msg ";
                break;
            case '1':
                strOrderBy = " bs.start_date ";
                break;
            case '2':
                strOrderBy = " bs.end_date ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getBroadCastData = getBroadCastData;

var addBroadCastMsg = function (req, res, objDataParams, callback) {
    var msg = objDataParams.msg;
    var startDate = objDataParams.startDate;
    var endDate = objDataParams.endDate;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var params = {
        msg: msg,
        start_date: startDate,
        end_date: endDate,
        created_date: currentUTC
    };

    var strQuery = 'INSERT INTO broadcast_messages SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
}
exports.addBroadCastMsg = addBroadCastMsg;

var delBroadCastMsg = function (req, res, objDataParams, callback) {
    var id = objDataParams.id;
    var queryParam = [id];
    var strQuery = 'DELETE from broadcast_messages WHERE id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.delBroadCastMsg = delBroadCastMsg;

var getMsgData = function (req, res, objDataParams, callback) {
    var id = objDataParams.id;
    var queryParam = [id];
    var strQuery = 'Select *,DATE_FORMAT(start_date, "%Y-%m-%d") as start_date ,  DATE_FORMAT(end_date, "%Y-%m-%d") as end_date from broadcast_messages WHERE id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results);
        }
    });
};
exports.getMsgData = getMsgData;

var editBroadCastMsg = function (req, res, objDataParams, callback) {
    var id = objDataParams.editid;
    var editmsg = objDataParams.editmsg;
    var editstartDate = objDataParams.editstartDate;
    var editendDate = objDataParams.editendDate;
    var queryParam = [editmsg, editstartDate, editendDate, id];
    var strQuery = 'UPDATE broadcast_messages SET msg = ? , start_date = ? ,end_date = ?  where id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.editBroadCastMsg = editBroadCastMsg;

var getPayoutRequest = function (req, res, objParams, callback) {

    var paging = false;
    if (typeof objParams.paging != 'undefined') {
        paging = objParams.paging;
    }

    var start = objParams.start;
    var length = objParams.length;
    var orderByColumn = -1;
    var orderByColumnType = objParams.orderByColumnType;
    var searchKey = '';
    var filterUserStatus = objParams.filterUserStatus;

    if (typeof objParams.orderByColumn != 'undefined') {
        orderByColumn = objParams.orderByColumn;
    }

    if (typeof objParams.searchKey != 'undefined') {
        searchKey = objParams.searchKey;
    }

    var queryParam = {};

    var strSelect = 'pr.wallet_amount as walamt, pr.id as id, pr.request_amount,DATE_FORMAT(pr.created_date, "%m/%d/%Y") as requested_date, DATE_FORMAT(pr.updated_date, "%m/%d/%Y") as paid_date , pr.status as payStatus,pr.amount_paid  , CONCAT_WS(" ", ud.first_name, ud.last_name) fullName, u.user_type userType, u.slug slug, u.email email, u.phone_number, u.status, u.user_id , md.wallet_balance,md.paypal_email , md.paypal_phone ';
    if (!paging) {
        strSelect = ' COUNT(*) totalCount ';
    }


    var strWhereQuery = ' ud.user_id = u.user_id ';
    if (searchKey != '') {
        strWhereQuery += ' AND ( ' +
                '       ud.first_name LIKE "%' + searchKey + '%" OR ' +
                '       ud.last_name LIKE "%' + searchKey + '%" OR ' +
                '       u.user_type LIKE "%' + searchKey + '%" OR ' +
                '       u.email LIKE "%' + searchKey + '%" OR ' +
                '       u.phone_number LIKE "%' + searchKey + '%" ' +
                '   ) ';
    }

    if (filterUserStatus != "") {
        switch (filterUserStatus) {
            case "A":
                strWhereQuery += '   AND u.status = "1" ';
                break;
            case "S":
                strWhereQuery += '   AND u.status = "2" ';
                break;
        }
    }

    var strQuery = ' SELECT ' + strSelect +
            ' FROM payout_request pr ' +
            ' LEFT JOIN mentor_details md ON pr.mentor_id = md.user_id ' +
            ' LEFT JOIN user_details ud ON ud.user_id = pr.mentor_id ' +
            ' LEFT JOIN users u ON u.user_id = pr.mentor_id ' +
            //' LEFT JOIN countries ct ON ud.country_id = ct.country_id ' +
            ' WHERE  ' + strWhereQuery + ' AND u.user_type = "mentor" AND u.user_id != "1"';

    var strOrderBy = "";

    if (orderByColumn > -1) {
        switch (orderByColumn) {
            case '0':
                strOrderBy = " ud.first_name ";
                break;
            case '1':
                strOrderBy = " u.user_type ";
                break;
            case '2':
                strOrderBy = " u.email ";
                break;
            case '3':
                strOrderBy = " u.phone_number ";
                break;
        }
    }

    if (strOrderBy != "") {
        strOrderBy = " ORDER BY " + strOrderBy + orderByColumnType;
    }
    if (paging) {
        if (start > -1) {
            strQuery += strOrderBy + ' LIMIT ' + start + ', ' + length;
        }
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getPayoutRequest = getPayoutRequest;