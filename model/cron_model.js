var exports = module.exports = {};
var dateFormat = require('dateformat');


var moment = require('moment');
var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var fs = require('fs');
var async = require("async");
var handlebars = require('handlebars');

var miscFunction = require('./../model/misc_model');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var messagesModel = require('./../model/messages_model');
var meetingLogModel = require('./../model/meetinglog_model');
var paymentModel = require('./../model/payment_model');
var paypalFunction = require('./../model/paypal_model');

var frontConstant = require('./../modules/front_constant');
var sendResponse = require('./../modules/sendresponse');




var finalStatusMeetingEndStatusUpdateOfMeeting = function (req, res, callback) {
    getMeetingInfoForEndedMeeting(req, res, {}, function (req, res, arrMeetingData) {
        recursiveMethodForUpdatingMeetingStatusAfterMeetingComplete(req, res, arrMeetingData, 0, [], function (req, res, objFinalData) {

        });
    });
};
exports.finalStatusMeetingEndStatusUpdateOfMeeting = finalStatusMeetingEndStatusUpdateOfMeeting;


var recursiveMethodForUpdatingMeetingStatusAfterMeetingComplete = function (req, res, arrMeetingData, counter, arrFinalDataSet, callback) {
    if (arrMeetingData.length > 0) {
        var thisResultSet = arrMeetingData[0];
        var participantMeetingLogMasterId = thisResultSet.participantMeetingLogMasterId;
        var invitationId = thisResultSet.invitationId;
        var meetingDetailId = thisResultSet.meetingDetailId;
        var meetingDetailStatus = thisResultSet.meetingDetailStatus;
        var meetingInviteStatus = thisResultSet.meetingInviteStatus;

        if (participantMeetingLogMasterId != null) {
            if (meetingDetailStatus == 'pending') {

                var objToUpdateInvitationTable = {
                    invitationId: invitationId,
                    status: 'completed'
                };

                meetingLogModel.updateInvitationTableForMeetingEnd(req, res, objToUpdateInvitationTable, function (req, res, objReturnParams) {
                    arrMeetingData.splice(0, 1);
                    arrFinalDataSet.push(thisResultSet);
                    counter++;
                    recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
                });
            } else {
                arrMeetingData.splice(0, 1);
                arrFinalDataSet.push(thisResultSet);
                counter++;
                recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
            }
        } else {
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        }
    } else {
        callback(req, res, {message: "no logs found"});
    }
};
exports.recursiveMethodForUpdatingMeetingStatusAfterMeetingComplete = recursiveMethodForUpdatingMeetingStatusAfterMeetingComplete;

var getMeetingInfoForEndedMeeting = function (req, res, objDataParams, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC];
    var strQuery = '    SELECT mi.invitation_id invitationId, md.meeting_id meetingDetailId, md.status meetingDetailStatus, md.meeting_url_key, mi.status meetingInviteStatus, pmlm.participant_meeting_log_master_id participantMeetingLogMasterId, ' +
            '       IF(mi.invitation_type = 0, "normalInvite", "privateInvite") meetingType, ' +
            '       IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) userId, ' +
            '       IF(mi.invitation_type = 1, mi.requester_id, mi.user_id) mentorId ' +
            '   FROM meeting_details md  ' +
            '       LEFT JOIN participant_meeting_log_master pmlm ON pmlm.meeting_urk_key = md.meeting_url_key, ' +
            '       meeting_invitations mi ' +
            '   WHERE md.invitation_id = mi.invitation_id ' +
            '   AND ADDTIME(md.scheduled_end_time, "00:15:00") < ? ' +
            '   AND mi.flag_cron_meeting_end_check = "N"' +
            '   AND mi.status = "accepted" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        callback(req, res, results);
    });
};
exports.getMeetingInfoForEndedMeeting = getMeetingInfoForEndedMeeting;



var finalStatusUpdateOfMeeting = function (req, res, callback) {

    // function call to get meetings, whoes start time start 12 minutes prior.
    getMeetingsInfoForNoShowCheck(req, res, {}, function (req, res, arrMeetingData) {
        recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, 0, [], function (req, res, objFinalData) {

        });
    });
};
exports.finalStatusUpdateOfMeeting = finalStatusUpdateOfMeeting;


var recursiveMethodForCheckingCheckingPendingMeetingForStatus = function (req, res, arrMeetingData, counter, arrFinalDataSet, callback) {

    if (arrMeetingData.length > 0) {

        var thisResultSet = arrMeetingData[0];
        var participantMeetingLogMasterId = thisResultSet.participantMeetingLogMasterId;
        var userTimeZone = thisResultSet.userTimeZone;
        var mentorTimeZone = thisResultSet.mentorTimeZone;

        // one party no show case.
        if (participantMeetingLogMasterId != null) {
            checkParticipationLogAndMakeAppropriateEntriesInDB(req, res, thisResultSet, function (req, res, objResponseData) {
                arrMeetingData.splice(0, 1);
                arrFinalDataSet.push(thisResultSet);
                counter++;
                recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
            });
        }
        // no show by both parties case.
        else {
            updateNoShowEntryForBothParticipants(req, res, thisResultSet, function (req, res, objResponseData) {
                arrMeetingData.splice(0, 1);
                arrFinalDataSet.push(objResponseData);
                counter++;
                recursiveMethodForCheckingCheckingPendingMeetingForStatus(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
            });
        }
    } else {
        callback(req, res, {message: "no logs found"});
    }

};
exports.recursiveMethodForCheckingCheckingPendingMeetingForStatus = recursiveMethodForCheckingCheckingPendingMeetingForStatus;



var checkParticipationLogAndMakeAppropriateEntriesInDB = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;
    var captureId = objDataParams.capture_id;
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meeting_url_key;
    var meetingInviteStatus = objDataParams.meetingInviteStatus;
    var participantMeetingLogMasterId = objDataParams.participantMeetingLogMasterId;
    var meetingType = objDataParams.meetingType;
    var userId = objDataParams.userId;
    var mentorId = objDataParams.mentorId;
    var userTimeZone = objDataParams.userTimeZone;
    var mentorTimeZone = objDataParams.mentorTimeZone;

    async.parallel([
        // function call to check user no show for perticular participation
        function (checkUserNoShowCallback) {
            var objDataForUser = {
                meetingUrlKey: meetingUrlKey,
                participantId: userId,
                participantMeetingLogMasterId: participantMeetingLogMasterId
            };
            checkForUserParticipationMasterLog(req, res, objDataForUser, function (req, res, objResponseData) {
                checkUserNoShowCallback(null, objResponseData);
            });
        },
        // function call to mentor no show for perticular participation
        function (checkUserNoShowCallback) {
            var objDataForUser = {
                meetingUrlKey: meetingUrlKey,
                participantId: mentorId,
                participantMeetingLogMasterId: participantMeetingLogMasterId
            };
            checkForUserParticipationMasterLog(req, res, objDataForUser, function (req, res, objResponseData) {
                checkUserNoShowCallback(null, objResponseData);
            });
        }
    ], function (error, resultSet) {
        var flagUserEntryPresent = resultSet[0].flagEntryPresent;
        var flagMentorEntryPresent = resultSet[1].flagEntryPresent;

        var currentUTC = constants.CURR_UTC_DATETIME();

        if (!flagMentorEntryPresent || !flagUserEntryPresent) {
            async.parallel([
                // updating meeting_invitation table.
                function (lastUpdateCallback) {
                    var objToUpdateInvitationTable = {};

                    if (!flagUserEntryPresent && flagMentorEntryPresent) {
                        objToUpdateInvitationTable = {
                            invitationId: invitationId,
                            noShowUserId: userId,
                            status: 'no_show_user'
                        };

                        meetingLogModel.updateInvitationTableForNoShow(req, res, objToUpdateInvitationTable, function (req, res, objReturnParams) {
                            lastUpdateCallback(req, res, objReturnParams);
                        });
                    } else if (!flagMentorEntryPresent && flagUserEntryPresent) {
                        objToUpdateInvitationTable = {
                            invitationId: invitationId,
                            noShowUserId: mentorId,
                            status: 'no_show_mentor'
                        };

                        meetingLogModel.updateInvitationTableForNoShow(req, res, objToUpdateInvitationTable, function (req, res, objReturnParams) {
                            lastUpdateCallback(req, res, objReturnParams);
                        });
                    } else if (!flagMentorEntryPresent && !flagUserEntryPresent) {
                        objToUpdateInvitationTable = {
                            invitationId: invitationId,
                            noShowUserId: null,
                            status: 'no_show_both'
                        };

                        meetingLogModel.updateInvitationTableForNoShow(req, res, objToUpdateInvitationTable, function (req, res, objReturnParams) {
                            lastUpdateCallback(req, res, objReturnParams);
                        });
                    }
                },
                // function call to update user related no show case.
                function (lastUpdateCallback) {
                    // if user no show
                    if (!flagUserEntryPresent) {
                        var objDataToCreateParticipantLogEntry = {
                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                            participantId: userId,
                            expressSessionId: '',
                            connectTime: currentUTC,
                            userType: 'user'
                        };
                        meetingLogModel.createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function (req, res, objMeetingLogInfo) {
                            var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;


                            /**
                             * Mail to user for no show START.
                             */
                            var objParamsForNoShowMailSend = {
                                invitationId: invitationId,
                                meetingDetailId: meetingDetailId,
                                meetingUrlKey: meetingUrlKey,
                                userId: userId,
                                mentorId: mentorId,
                                timeZone: userTimeZone
                            };

                            sendNoShowMailUser(req, res, objParamsForNoShowMailSend, function (req, res, objResponseData) {
                            });


                            /**
                             * funtion call to update invoice tabel for user no show.
                             */
                            var objParams = {
                                invitationId: invitationId,
                                meetingUserId: userId,
                                meetingMentorId: mentorId
                            };
                            paymentModel.updateInvoiceForNoShowByUser(req, res, objParams, function (req, res, objResponse) {

                                var dataParam = {
                                    amount: objResponse.amountBackToUser,
                                    captureId: captureId,
                                    invitation_id: invitationId,
                                    invoiceid: objResponse.invoiceId,
                                    paymentAction: "credited",
                                    reason: "NOSHOWBYUSR",
                                    user_id: userId,
                                    user_type: "user"
                                };
                                paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {

                                    var data = {
                                        invoice_id: objResponse.invoiceId,
                                        user_id: mentorId,
                                        user_type: "mentor",
                                        reason: "NOSHOWBYUSR",
                                        transection_type: "PAYREF",
                                        payment_type: "CREDITED",
                                        payment_id: null,
                                        transection_id: null,
                                        capture_id: null,
                                        refund_id: null,
                                        amount: objResponse.mentorsCut,
                                        status: "completed",
                                        created_date: constants.CURR_UTC_DATETIME()
                                    };
                                    paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                                        var objParams = {
                                            user_id: mentorId,
                                            amount: objResponse.mentorsCut
                                        };
                                        paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                            var data = {
                                                invoice_id: objResponse.invoiceId,
                                                user_id: "1",
                                                user_type: "admin",
                                                reason: "NOSHOWBYUSR",
                                                transection_type: "PAYREF",
                                                payment_type: "CREDITED",
                                                payment_id: null,
                                                transection_id: null,
                                                capture_id: null,
                                                refund_id: null,
                                                amount: objResponse.wissenxCut,
                                                status: 'completed',
                                                created_date: constants.CURR_UTC_DATETIME()
                                            };
                                            paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                                var objParams = {
                                                    user_id: "1",
                                                    amount: objResponse.wissenxCut
                                                };
                                                paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                                });
                                            });
                                        });
                                    });
                                });
                            });

                            var objDataToPass = {
                                participantMeetingLogMasterId: participantMeetingLogMasterId,
                                participantMeetingLogId: participantMeetingLogId
                            };
                            lastUpdateCallback(null, objDataToPass);
                        });
                    } else {
                        lastUpdateCallback(null, {})
                    }
                },
                // if mentor no show
                function (lastUpdateCallback) {
                    //if mentor no show
                    if (!flagMentorEntryPresent) {
                        var objDataToCreateParticipantLogEntry = {
                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                            participantId: mentorId,
                            expressSessionId: '',
                            connectTime: currentUTC,
                            userType: 'mentor'
                        };
                        meetingLogModel.createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function (req, res, objMeetingLogInfo) {
                            var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;


                            /**
                             * Mail to mentor for no show.
                             */
                            var objParamsForNoShowMailSend = {
                                invitationId: invitationId,
                                meetingDetailId: meetingDetailId,
                                meetingUrlKey: meetingUrlKey,
                                userId: userId,
                                mentorId: mentorId,
                                timeZone: mentorTimeZone
                            };

                            /**
                             * function call to send mentor no show mail.
                             */
                            sendNoShowMailMentor(req, res, objParamsForNoShowMailSend, function (req, res, objResponseData) {
                            });

                            /**
                             * funtion call to update invoice tabel for mentor no show.
                             */
                            var objParams = {
                                invitationId: invitationId,
                                meetingUserId: userId,
                                meetingMentorId: mentorId
                            };
                            paymentModel.updateInvoiceForNoShowByMentor(req, res, objParams, function (req, res, objResponse) {

                                var dataParam = {
                                    amount: objResponse.amountBackToUser,
                                    captureId: captureId,
                                    invitation_id: invitationId,
                                    invoiceid: objResponse.invoiceId,
                                    paymentAction: "CREDITED",
                                    reason: "NOSHOWBYMEN",
                                    user_id: userId,
                                    user_type: "user"
                                };
                                paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {

                                    var data = {
                                        invoice_id: objResponse.invoiceId,
                                        user_id: mentorId,
                                        user_type: "mentor",
                                        reason: "NOSHOWBYMEN",
                                        transection_type: "PAYREF",
                                        payment_type: "DEBITED",
                                        payment_id: null,
                                        transection_id: null,
                                        capture_id: null,
                                        refund_id: null,
                                        amount: objResponse.mentorsCut,
                                        status: "completed",
                                        created_date: constants.CURR_UTC_DATETIME()
                                    };
                                    paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                                        var objParams = {
                                            user_id: mentorId,
                                            amount: objResponse.mentorsCut
                                        };
                                        paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                            var data = {
                                                invoice_id: objResponse.invoiceId,
                                                user_id: "1",
                                                user_type: "admin",
                                                reason: "NOSHOWBYMEN",
                                                transection_type: "PAYREF",
                                                payment_type: "CREDITED",
                                                payment_id: null,
                                                transection_id: null,
                                                capture_id: null,
                                                refund_id: null,
                                                amount: objResponse.wissenxCut,
                                                status: 'completed',
                                                created_date: constants.CURR_UTC_DATETIME()
                                            };
                                            paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                                var objParams = {
                                                    user_id: "1",
                                                    amount: objResponse.wissenxCut
                                                };
                                                paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                                });
                                            });
                                        });
                                    });
                                });
                            });

                            var objDataToPass = {
                                participantMeetingLogMasterId: participantMeetingLogMasterId,
                                participantMeetingLogId: participantMeetingLogId
                            };
                            lastUpdateCallback(null, objDataToPass);
                        });
                    } else {
                        lastUpdateCallback(null, {})
                    }
                }
            ], function (lastUpdateError, lastUpdateResultSet) {
                callback(req, res, {});
            });
        } else {
            callback(req, res, {});
        }
    });
};
exports.checkParticipationLogAndMakeAppropriateEntriesInDB = checkParticipationLogAndMakeAppropriateEntriesInDB;


var checkForUserParticipationMasterLog = function (req, res, objDataParams, callback) {
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var participantId = objDataParams.participantId;
    var participantMeetingLogMasterId = objDataParams.participantMeetingLogMasterId;

    var queryParam = [participantMeetingLogMasterId, participantId];
    var strQuery = '    SELECT COUNT(*) totalCnt ' +
            '   FROM participant_meeting_log pml ' +
            '   WHERE pml.participant_meeting_log_master_id = ? ' +
            '   AND pml.participant_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            var resultSet = results[0];

            if (resultSet.totalCnt > 0) {
                var objResponseToCallback = {
                    flagEntryPresent: true
                }
                callback(req, res, objResponseToCallback);
            } else {
                var objResponseToCallback = {
                    flagEntryPresent: false
                }
                callback(req, res, objResponseToCallback);
            }
        } else {
            var objResponseToCallback = {
                flagEntryPresent: false
            }
            callback(req, res, objResponseToCallback);
        }
    });
};
exports.checkForUserParticipationMasterLog = checkForUserParticipationMasterLog;


var updateNoShowEntryForBothParticipants = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;
    var captureId = objDataParams.capture_id;
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meeting_url_key;
    var meetingInviteStatus = objDataParams.meetingInviteStatus;
    var participantMeetingLogMasterId = objDataParams.participantMeetingLogMasterId;
    var meetingType = objDataParams.meetingType;
    var userId = objDataParams.userId;
    var mentorId = objDataParams.mentorId;

    var userTimeZone = objDataParams.userTimeZone;
    var mentorTimeZone = objDataParams.mentorTimeZone;

    var dataForCreatingParticipationMasterLog = {
        meetingUrlKey: meetingUrlKey
    };
    meetingLogModel.createMasterLogEntry(req, res, dataForCreatingParticipationMasterLog, function (req, res, objParticipationMaserLogResponse) {
        var participantMeetingLogMasterId = objParticipationMaserLogResponse.participantMeetingLogMasterId;

        var currentUTC = constants.CURR_UTC_DATETIME();

        async.parallel([
            // creating no show entry in invitation table for both no show both
            function (logCallback) {
                var objToUpdateInvitationTable = {
                    invitationId: invitationId,
                    status: 'no_show_both'
                };

                meetingLogModel.updateInvitationTableForNoShow(req, res, objToUpdateInvitationTable, function (req, res, objReturnParams) {
                    logCallback(req, res, objReturnParams);
                });
            },
            // creating no show entry for user
            function (logCallback) {

                var objDataToCreateParticipantLogEntry = {
                    participantMeetingLogMasterId: participantMeetingLogMasterId,
                    participantId: userId,
                    expressSessionId: req.sessionID,
                    connectTime: currentUTC,
                    userType: 'user'
                };

                meetingLogModel.createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function (req, res, objMeetingLogInfo) {
                    var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                    var objParamsForNoShowMailSend = {
                        invitationId: invitationId,
                        meetingDetailId: meetingDetailId,
                        meetingUrlKey: meetingUrlKey,
                        userId: userId,
                        mentorId: mentorId,
                        timeZone: userTimeZone
                    };

                    sendNoShowMailUser(req, res, objParamsForNoShowMailSend, function (req, res, objResponseData) {
                    });

                    var objDataToPass = {
                        participantMeetingLogMasterId: participantMeetingLogMasterId,
                        participantMeetingLogId: participantMeetingLogId
                    };
                    logCallback(null, objDataToPass);
                });
            },
            // creating no show entry for mentor
            function (logCallback) {

                var objDataToCreateParticipantLogEntry = {
                    participantMeetingLogMasterId: participantMeetingLogMasterId,
                    participantId: mentorId,
                    expressSessionId: req.sessionID,
                    connectTime: currentUTC,
                    userType: 'mentor'
                };

                meetingLogModel.createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function (req, res, objMeetingLogInfo) {
                    var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                    var objParamsForNoShowMailSend = {
                        invitationId: invitationId,
                        meetingDetailId: meetingDetailId,
                        meetingUrlKey: meetingUrlKey,
                        userId: userId,
                        mentorId: mentorId,
                        timeZone: mentorTimeZone
                    };

                    sendNoShowMailMentor(req, res, objParamsForNoShowMailSend, function (req, res, objResponseData) {
                    });

                    var objDataToPass = {
                        participantMeetingLogMasterId: participantMeetingLogMasterId,
                        participantMeetingLogId: participantMeetingLogId
                    };
                    logCallback(null, objDataToPass);
                });
            },
            // function call to update invoice for no show by both
            function (updateInvoiceNoShowBothCallback) {

                var objParams = {
                    invitationId: invitationId,
                    meetingUserId: userId,
                    meetingMentorId: mentorId
                };

                paymentModel.updateInvoiceForNoShowBothParties(req, res, objParams, function (req, res, objResponse) {

                    var dataParam = {
                        amount: objResponse.amountBackToUser,
                        captureId: captureId,
                        invitation_id: invitationId,
                        invoiceid: objResponse.invoiceId,
                        paymentAction: "CREDITED",
                        reason: "NOSHOWBYBOTH",
                        user_id: userId,
                        user_type: "user"
                    };
                    paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {
                        updateInvoiceNoShowBothCallback(null, true);
                        /* var data = {
                         invoice_id: objResponse.invoiceId,
                         user_id: mentorId,
                         user_type: "mentor",
                         reason: "NOSHOWBYBOTH",
                         transection_type: "PAYREF",
                         payment_type: "DEBITED",
                         payment_id: null,
                         transection_id: null,
                         capture_id: null,
                         refund_id: null,
                         amount: objResponse.mentorsCut,
                         status: "completed",
                         created_date: constants.CURR_UTC_DATETIME()
                         };
                         paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                         var objParams = {
                         user_id: mentorId,
                         amount: objResponse.mentorsCut
                         };
                         paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                         var data = {
                         invoice_id: objResponse.invoiceId,
                         user_id: "1",
                         user_type: "admin",
                         reason: "NOSHOWBYBOTH",
                         transection_type: "PAYREF",
                         payment_type: "CREDITED",
                         payment_id: null,
                         transection_id: null,
                         capture_id: null,
                         refund_id: null,
                         amount: objResponse.wissenxCut,
                         status: 'completed',
                         created_date: constants.CURR_UTC_DATETIME()
                         };
                         paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                         
                         var objParams = {
                         user_id: "1",
                         amount: objResponse.wissenxCut
                         };
                         paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                         updateInvoiceNoShowBothCallback(null, true);
                         });
                         });
                         });
                         });*/
                    });
                });
            }
        ],
                // callback function. 
                        function (error, resultSet) {
                            callback(req, res, true);
                        });
            });
};
exports.updateNoShowEntryForBothParticipants = updateNoShowEntryForBothParticipants;


var getMeetingsInfoForNoShowCheck = function (req, res, objDataParams, callback) {

    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC];

    var strQuery = '    SELECT mi.invitation_id invitationId, md.meeting_id meetingDetailId, md.meeting_url_key, mi.status meetingInviteStatus, pmlm.participant_meeting_log_master_id participantMeetingLogMasterId, zu.zone_name userTimeZone, zm.zone_name mentorTimeZone,wt.capture_id, ' +
            '       IF(mi.invitation_type = 0, "normalInvite", "privateInvite") meetingType, ' +
            '       IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) userId, ' +
            '       IF(mi.invitation_type = 1, mi.requester_id, mi.user_id) mentorId ' +
            '   FROM meeting_details md  ' +
            '       LEFT JOIN participant_meeting_log_master pmlm ON pmlm.meeting_urk_key = md.meeting_url_key, ' +
            '       meeting_invitations mi ' +
            '       LEFT JOIN invoice ins ON mi.invitation_id = ins.invitation_id ' +
            '       LEFT JOIN wallet_transection wt ON wt.invoice_id = ins.invoice_id AND wt.transection_type = "PAYCAP" ' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   WHERE md.invitation_id = mi.invitation_id ' +
            '   AND ADDTIME(md.scheduled_start_time, "00:10:00") < ? ' +
            '   AND mi.flag_cron_no_show_check = "N"' +
            '   AND mi.status = "accepted" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getMeetingsInfoForNoShowCheck = getMeetingsInfoForNoShowCheck;



var sendNoShowMailMentor = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var userId = objDataParams.userId;
    var mentorId = objDataParams.mentorId;
    var timeZone = objDataParams.timeZone;

    async.parallel([
        // fucntion call to get user information.
        function (sendNoShowMailMentorParallelCallback) {
            userModel.getUserInformation(req, res, userId, {timeZone: timeZone}, function (req, res, userId, arrUserData, objParameterData) {
                sendNoShowMailMentorParallelCallback(null, arrUserData);
            });
        },
        // fucntion call to get mentor information.
        function (sendNoShowMailMentorParallelCallback) {
            mentorModel.getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
                var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
                arrMentorFullData.mentorCancellationPolicy = mentorCancellationPolicy;
                sendNoShowMailMentorParallelCallback(null, arrMentorFullData);
            });
        },
        //function call to get meeting information for no show case.
        function (sendNoShowMailMentorParallelCallback) {
            var objDataParamsForAcceptedSlot = {
                invitationId: invitationId,
            };
            miscFunction.getAcceptedInviteSlotForMeeting(req, res, objDataParamsForAcceptedSlot, function (req, res, objDataResponse) {
                sendNoShowMailMentorParallelCallback(null, objDataResponse);
            });
        }
    ], function (error, resultSet) {

        var arrUserData = resultSet[0];
        var arrMentorData = resultSet[1];
        var arrInvitationSlotData = resultSet[2];

        var dateOfMeeting = arrInvitationSlotData.date;
        var timeOfMeeting = arrInvitationSlotData.time;
        var duration = arrInvitationSlotData.duration;
        var dateTimeOfMeeting = dateOfMeeting + ' ' + timeOfMeeting;

        var mentorTimeZone = arrMentorData.timeZoneName;

        var startTimeSlotInMentorTimeZone = moment.tz(dateTimeOfMeeting, 'UTC').tz(mentorTimeZone).format('YYYY-MM-DD HH:mm:ss');
        var endTimeSlotInMentorTimeZone = moment(startTimeSlotInMentorTimeZone).add(duration, 'minutes').format('YYYY-MM-DD HH:mm:ss');


        var dateToDisplay = moment(startTimeSlotInMentorTimeZone).format('MMMM DD, YYYY');
        var startTimeToDisplay = moment(startTimeSlotInMentorTimeZone).format('hh:mm a');
        var endTimeToDisplay = moment(endTimeSlotInMentorTimeZone).format('hh:mm a');
        var strTimeToDisplay = startTimeToDisplay + ' to ' + endTimeToDisplay + ', UTC ' + moment.tz(mentorTimeZone).format('Z');


        var objInvitationData = {
            dateToDisplay: dateToDisplay,
            strTimeToDisplay: strTimeToDisplay
        };

        var templateVariable = {
            templateURL: "mailtemplate/noshow/mentor",
            frontConstant: frontConstant,
            arrMentorData: arrMentorData,
            arrUserData: arrUserData,
            objInvitationData: objInvitationData,
            mentorCancellationPolicy: arrMentorData.mentorCancellationPolicy,
            mentorCancellationPolicyLink: arrMentorData.cancellation_policy_link
        };

        var mailParamsObject = {
            templateVariable: templateVariable,
            to: arrMentorData.mentorMail,
            subject: 'Hi ' + arrMentorData.first_name + ', you missed your confirmed meeting  with ' + arrUserData.name + ' ' + arrUserData.last_name
        };

        mailer.sendMail(req, res, mailParamsObject, function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log("mail send no show mentor");
            }
        });
    });
};
exports.sendNoShowMailMentor = sendNoShowMailMentor;



var sendNoShowMailUser = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var userId = objDataParams.userId;
    var mentorId = objDataParams.mentorId;

    var dataForInvitationUserMentorData = {
        invitationId: invitationId
    };

    getInvitationUserMentorData(req, res, dataForInvitationUserMentorData, function (req, res, objDataResponse) {

        //var invitationId = objDataResponse.invitationId;
        var meetingDuration = objDataResponse.meetingDuration;
        var inviteType = objDataResponse.inviteType;
        var meetingUserId = objDataResponse.meetingUserId;
        var meetingMentorId = objDataResponse.meetingMentorId;
        var userTimeZone = objDataResponse.userTimeZone;
        var mentorTimeZone = objDataResponse.mentorTimeZone;

        if (typeof objDataResponse.invitationId != 'undefined') {
            async.parallel([
                // fucntion call to get user information.
                function (sendNoShowMailUserParallelCallback) {
                    var dataForUser = {
                        timeZone: userTimeZone
                    };
                    userModel.getUserInformation(req, res, userId, dataForUser, function (req, res, userId, arrUserData, objParameterData) {
                        sendNoShowMailUserParallelCallback(null, arrUserData);
                    });
                },
                // fucntion call to get mentor information.
                function (sendNoShowMailUserParallelCallback) {
                    mentorModel.getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
                        var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });
                        arrMentorFullData.mentorCancellationPolicy = mentorCancellationPolicy;
                        sendNoShowMailUserParallelCallback(null, arrMentorFullData);
                    });
                },
                //function call to get meeting information for no show case.
                function (sendNoShowMailUserParallelCallback) {
                    var objDataParamsForAcceptedSlot = {
                        invitationId: invitationId,
                    };
                    miscFunction.getAcceptedInviteSlotForMeeting(req, res, objDataParamsForAcceptedSlot, function (req, res, objDataResponse) {
                        sendNoShowMailUserParallelCallback(null, objDataResponse);
                    });
                }
            ], function (error, resultSet) {
                var arrUserData = resultSet[0];
                var arrMentorData = resultSet[1];
                var arrInvitationSlotData = resultSet[2];

                var dateOfMeeting = arrInvitationSlotData.date;
                var timeOfMeeting = arrInvitationSlotData.time;
                var duration = arrInvitationSlotData.duration;
                var dateTimeOfMeeting = dateOfMeeting + ' ' + timeOfMeeting;

                var userTimeZone = arrUserData.zone_name;

                var startTimeSlotInUserTimeZone = moment.tz(dateTimeOfMeeting, 'UTC').tz(userTimeZone).format('YYYY-MM-DD HH:mm:ss');
                var endTimeSlotInUserTimeZone = moment(startTimeSlotInUserTimeZone).add(duration, 'minutes').format('YYYY-MM-DD HH:mm:ss');


                var dateToDisplay = moment(startTimeSlotInUserTimeZone).format('MMMM DD, YYYY');
                var startTimeToDisplay = moment(startTimeSlotInUserTimeZone).format('hh:mm a');
                var endTimeToDisplay = moment(endTimeSlotInUserTimeZone).format('hh:mm a');
                var strTimeToDisplay = startTimeToDisplay + ' to ' + endTimeToDisplay + ', UTC ' + moment.tz(userTimeZone).format('Z');


                var objInvitationData = {
                    dateToDisplay: dateToDisplay,
                    strTimeToDisplay: strTimeToDisplay
                };

                var templateVariable = {
                    templateURL: "mailtemplate/noshow/user",
                    frontConstant: frontConstant,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    objInvitationData: objInvitationData,
                    mentorCancellationPolicy: arrMentorData.mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorData.cancellation_policy_link
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrUserData.email,
                    subject: 'Hi ' + arrUserData.name + ', you missed your confirmed meeting  with ' + arrMentorData.first_name + ' ' + arrMentorData.last_name
                };

                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("mail send no show mentor");
                    }
                });
            });
        }
    });
};
exports.sendNoShowMailUser = sendNoShowMailUser;


var getInvitationsThatPassedExpieryTime = function (req, res, objDataParams, callback) {

    var flagNormalInvite = objDataParams.flagNormalInvite;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var meetingType = 0;
    var systemSettingVariable = "EXPOVR";

    if (!flagNormalInvite) {
        meetingType = 1;
        systemSettingVariable = "EXPOPT";
    }

    var queryParam = [meetingType, currentUTC, systemSettingVariable];

    var strQuery = '    SELECT mi.invitation_id invitationId, mi.duration meetingDuration, zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            '   IF(mi.invitation_type = 0, "normalInvite", "PrivateInvite") inviteType, ' +
            '   IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) meetingUserId, ' +
            '   IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) meetingMentorId ' +
            '   FROM  meeting_invitations mi' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   WHERE mi.invitation_type = ? ' +
            '   AND mi.status = "pending" ' +
            '   AND (mi.flag_payment_done = "1" AND mi.invitation_type = "0" OR mi.flag_payment_done = "0" AND mi.invitation_type = "1" ) ' +
            '   AND ( ' +
            '       TIMESTAMPDIFF(HOUR, mi.created_date, ? ) > (' +
            '                                                       SELECT IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value * 24, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
            '                                                       FROM system_settings ss ' +
            '                                                       WHERE ss.code = ? ' +
            '                                                   )' +
            '       ) ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getInvitationsThatPassedExpieryTime = getInvitationsThatPassedExpieryTime;


var getInvitationsThatAboutToExpireFromCurrentTime = function (req, res, objDataParams, callback) {
    var flagNormalInvite = objDataParams.flagNormalInvite;

    var meetingType = 0;
    var systemSettingVariable = "EXPMIN";

    if (!flagNormalInvite) {
        meetingType = 1;
        systemSettingVariable = "EXPMPT";
    }


    var queryParam = [meetingType, systemSettingVariable];

    var strQuery = '    SELECT mip.invitation_id invitationId, mip.duration meetingDuration, zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            '   IF(mip.invitation_type = 0, "normalInvite", "PrivateInvite") inviteType, ' +
            '   IF(mip.invitation_type = 0, mip.requester_id, mip.user_id) meetingUserId, ' +
            '   IF(mip.invitation_type = 0, mip.user_id, mip.requester_id) meetingMentorId ' +
            '   FROM meeting_invitations mip ' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mip.invitation_type = 0, mip.requester_id, mip.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mip.invitation_type = 0, mip.user_id, mip.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   WHERE mip.status = "pending" ' +
            '   AND mip.invitation_type = ? ' +
            '   AND (mip.flag_payment_done = "1" AND mip.invitation_type = "0" OR mip.flag_payment_done = "0" AND mip.invitation_type = "1" ) ' +
            '   AND ( ' +
            '   		SELECT COUNT(mism.invitation_id) ' +
            '   		FROM meeting_invitation_slot_mapping mism, meeting_invitations mi ' +
            '   		WHERE mism.invitation_id = mip.invitation_id ' +
            '   		AND mip.status = "pending" ' +
            '   		AND mi.invitation_id = mip.invitation_id ' +
            '   		AND CONCAT_WS(" ", mism.date, mism.time) <= ( DATE_ADD( NOW(), INTERVAL ( ' +
            '   												SELECT IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value * 60, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
            '   												FROM system_settings ss ' +
            '   												WHERE ss.code = ? ' +
            '   											) MINUTE ' +
            '   									 ) ' +
            '	    							) ' +
            '		    GROUP BY mip.invitation_id ' +
            '	    ) = ( ' +
            '		    SELECT COUNT(mism2.invitation_id) ' +
            '	    	FROM meeting_invitation_slot_mapping mism2 ' +
            '	    	WHERE mism2.invitation_id = mip.invitation_id ' +
            '	    	GROUP BY mism2.invitation_id ' +
            '	    )';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getInvitationsThatAboutToExpireFromCurrentTime = getInvitationsThatAboutToExpireFromCurrentTime;



var getInvitationUserMentorData = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;

    var queryParam = [invitationId];

    var strQuery = '    SELECT mi.invitation_id invitationId, mi.duration meetingDuration, zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            '   IF(mi.invitation_type = 0, "normalInvite", "PrivateInvite") inviteType, ' +
            '   IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) meetingUserId, ' +
            '   IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) meetingMentorId ' +
            '   FROM  meeting_invitations mi' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   WHERE mi.invitation_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getInvitationUserMentorData = getInvitationUserMentorData;

var getMeetingInfoForReminder = function (req, res, objDataParams, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC];
    var strQuery = 'SELECT mi.duration ,mi.invitation_id invitationId, md.meeting_id meetingDetailId, md.status meetingDetailStatus, md.meeting_url_key, mi.status meetingInviteStatus, pmlm.participant_meeting_log_master_id participantMeetingLogMasterId,zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            ' IF(mi.invitation_type = 0, "normalInvite", "privateInvite") meetingType, ' +
            ' IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) userId, ' +
            ' IF(mi.invitation_type = 1, mi.requester_id, mi.user_id) mentorId ' +
            ' FROM meeting_details md  ' +
            ' LEFT JOIN participant_meeting_log_master pmlm ON pmlm.meeting_urk_key = md.meeting_url_key, ' +
            ' meeting_invitations mi ' +
            ' LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            ' LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            ' LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            ' LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            ' WHERE md.invitation_id = mi.invitation_id ' +
            ' AND ADDTIME(?, "00:30:00") = md.scheduled_start_time ' +
            ' AND mi.status = "accepted" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            //callback(req, res, results);
            recursiveMethodForMeetingReminder(req, res, results, 0, [], function (req, res, objFinalData) {
                callback(req, res, {currentUTC: currentUTC});
            });
        }
    });
};
exports.getMeetingInfoForReminder = getMeetingInfoForReminder;

var recursiveMethodForMeetingReminder = function (req, res, arrResultSet, counter, arrFinalData, callback) {
    if (arrResultSet.length > 0) {
        var thisResultSet = arrResultSet[0];
        var invitationId = thisResultSet.invitationId;
        var meetingDuration = thisResultSet.duration;
        var inviteType = thisResultSet.meetingType;
        var meetingUserId = thisResultSet.userId;
        var meetingMentorId = thisResultSet.mentorId;
        var userTimeZone = thisResultSet.userTimeZone;
        var mentorTimeZone = thisResultSet.mentorTimeZone;
        var meeting_url_key = thisResultSet.meeting_url_key;


        var invitationDataMentor = {
            invitation_id: invitationId,
            duration: meetingDuration,
            timeZone: mentorTimeZone
        };

        userModel.getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
            var invitationDataUser = {
                invitation_id: invitationId,
                duration: meetingDuration,
                timeZone: userTimeZone
            };
            userModel.getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                var invitationData = {
                    invitation_id: invitationId,
                    duration: meetingDuration,
                    meeting_url_key: meeting_url_key
                }
                sendMeetingReminderMail(req, res, arrMentorData, arrUserData, invitationData, 0);
            });
        });

        arrResultSet.splice(0, 1);
        arrFinalData.push(thisResultSet);
        counter++;
        recursiveMethodForMeetingReminder(req, res, arrResultSet, counter, arrFinalData, callback);
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.recursiveMethodForMeetingReminder = recursiveMethodForMeetingReminder;

var sendMeetingReminderMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });


    var mentorIdPolicy = 0;
    mentorIdPolicy = arrMentorData.user_id;

    userModel.getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, mentorIdPolicy, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var meeting_url_key = invitationData.meeting_url_key;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
            });

            // Send mail to user           

            var templateVariable = {
                templateURL: "mailtemplate/meetingremindermailuser",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                meeting_url_key: meeting_url_key,
                userType: "user"
            };


            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + userFullName + ",you have a scheduled meeting coming up in next 30 minutes with " + arrMentorData.firstName + " " + arrMentorData.lastName + "."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingremindermsguser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                meeting_url_key: meeting_url_key
            };
            req.body = {};
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/meetingremindermailmentor",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                meeting_url_key: meeting_url_key,
                userType: "mentor"
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", you have a scheduled meeting coming up in next 30 minutes with " + userFullName + "."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to mentor inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingremindermsgmentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                meeting_url_key: meeting_url_key
            };
            req.body = {};
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendMeetingReminderMail = sendMeetingReminderMail;

var getPenginReminder = function (req, res, objDataParams, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();
    var day = objDataParams.day;

    var queryParam = [currentUTC];

    if (day == 3) {
        var strQuery = 'SELECT md.meeting_id meetingDetailId, md.invitation_id invitationId ' +
                '   FROM meeting_details md , meeting_invitations mi  ' +
                '   WHERE md.invitation_id = mi.invitation_id  ' +
                '   AND DATE_ADD(md.scheduled_start_time, INTERVAL 3 DAY) <= ? ' +
                '   AND md.flag_cron_review_3_day = 0 AND mi.status = "completed"';
    } else {
        var strQuery = 'SELECT md.meeting_id meetingDetailId, md.invitation_id invitationId ' +
                '   FROM meeting_details md , meeting_invitations mi  ' +
                '   WHERE md.invitation_id = mi.invitation_id  ' +
                '   AND DATE_ADD(md.scheduled_start_time, INTERVAL 7 DAY) <= ? ' +
                '   AND md.flag_cron_review_7_day = 0 AND mi.status = "completed"';
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                //callback(req, res, results);
                recursiveCheckMeetingReview(req, res, day, results, 0, [], function (req, res, objDataResponse) {

                });
            } else {
                callback(req, res, {});
            }
        }
    });

};
exports.getPenginReminder = getPenginReminder;


var recursiveCheckMeetingReview = function (req, res, day, arrMeetingData, counter, arrFinalDataSet, callback) {
    if (arrMeetingData.length > 0) {
        var thisResultSet = arrMeetingData[0];
        var meetingDetailId = thisResultSet.meetingDetailId;
        var invitationId = thisResultSet.invitationId;

        var objDataToPass = {
            meetingDetailId: meetingDetailId,
            invitationId: invitationId
        };

        getInvitationUserMentorData(req, res, objDataToPass, function (req, res, objUserMentorData) {
            var invitationId = objUserMentorData.invitationId;
            var inviteType = objUserMentorData.inviteType;
            var meetingUserId = objUserMentorData.meetingUserId;
            var meetingMentorId = objUserMentorData.meetingMentorId;

            async.parallel([
                // function call to check user had given feedback
                function (feedbackCallback) {
                    var objDataForFeedbackCheck = {
                        requesterId: meetingUserId,
                        meetingDetailId: meetingDetailId
                    }
                    checkUserHadGivenFeedBack(req, res, objDataForFeedbackCheck, function (req, res, objResponse) {
                        var dataToBack = objUserMentorData;
                        dataToBack.reviewSubmitted = objResponse.status;

                        feedbackCallback(null, dataToBack);
                    });
                },
                // functio call to check mentor had given feedback
                function (feedbackCallback) {
                    var objDataForFeedbackCheck = {
                        requesterId: meetingMentorId,
                        meetingDetailId: meetingDetailId
                    }
                    checkUserHadGivenFeedBack(req, res, objDataForFeedbackCheck, function (req, res, objResponse) {
                        var dataToBack = objUserMentorData;
                        dataToBack.reviewSubmitted = objResponse.status;

                        feedbackCallback(null, dataToBack);
                    });
                }
            ], function (error, resultSet) {

                var objUserFeedbackSubmit = resultSet[0];
                var objMentorFeedbackSubmit = resultSet[1];

                // mail to user
                if (!objUserFeedbackSubmit.reviewSubmitted) {


                    var queryParam = [meetingDetailId, "user", "0"];
                    var strQuery = 'Select * from leave_feedback where meeting_detail_id = ? AND user_type = ? AND is_used = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            console.log(error);
                        } else {
                            if (results.length > 0) {
                                var invitationDataMentor = {
                                    invitation_id: objUserFeedbackSubmit.invitationId,
                                    duration: objUserFeedbackSubmit.meetingDuration,
                                    timeZone: objUserFeedbackSubmit.mentorTimeZone
                                };

                                userModel.getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                                    var invitationDataUser = {
                                        invitation_id: objUserFeedbackSubmit.invitationId,
                                        duration: objUserFeedbackSubmit.meetingDuration,
                                        timeZone: objUserFeedbackSubmit.userTimeZone
                                    };
                                    userModel.getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                                        var invitationData = {
                                            invitation_id: objUserFeedbackSubmit.invitationId,
                                            duration: objUserFeedbackSubmit.meetingDuration,
                                            meetingDetailId: meetingDetailId,
                                            day: day,
                                            url_key: results[0].url_key
                                        };
                                        sendMeetingFeedbackUserMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    });
                                });
                            } else {
                                var randomString = "";
                                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                                for (var i = 0; i < 16; i++) {
                                    randomString += possible.charAt(Math.floor(Math.random() * possible.length));
                                }

                                var queryParam = {
                                    url_key: randomString + meetingUserId,
                                    meeting_detail_id: meetingDetailId,
                                    user_type: "user",
                                    requester_id: meetingUserId,
                                    user_id: meetingMentorId,
                                    is_used: "0",
                                    created_date: moment.utc().format()
                                };
                                var strQuery = 'insert into leave_feedback set ?';
                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                    if (error) {
                                        console.log(error);
                                    } else {

                                        var invitationDataMentor = {
                                            invitation_id: objUserFeedbackSubmit.invitationId,
                                            duration: objUserFeedbackSubmit.meetingDuration,
                                            timeZone: objUserFeedbackSubmit.mentorTimeZone
                                        };

                                        userModel.getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                                            var invitationDataUser = {
                                                invitation_id: objUserFeedbackSubmit.invitationId,
                                                duration: objUserFeedbackSubmit.meetingDuration,
                                                timeZone: objUserFeedbackSubmit.userTimeZone
                                            };
                                            userModel.getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                                                var invitationData = {
                                                    invitation_id: objUserFeedbackSubmit.invitationId,
                                                    duration: objUserFeedbackSubmit.meetingDuration,
                                                    meetingDetailId: meetingDetailId,
                                                    day: day,
                                                    url_key: randomString + meetingUserId,
                                                };
                                                sendMeetingFeedbackUserMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                            });
                                        });
                                    }
                                });
                            }
                        }
                    });
                }

                // mail to mentor
                if (!objMentorFeedbackSubmit.reviewSubmitted) {

                    var queryParam = [meetingDetailId, "mentor", "0"];
                    var strQuery = 'Select * from leave_feedback where meeting_detail_id = ? AND user_type = ? AND is_used = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            console.log(error);
                        } else {
                            if (results.length > 0) {
                                var invitationDataMentor = {
                                    invitation_id: objUserFeedbackSubmit.invitationId,
                                    duration: objUserFeedbackSubmit.meetingDuration,
                                    timeZone: objUserFeedbackSubmit.mentorTimeZone
                                };

                                userModel.getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                                    var invitationDataUser = {
                                        invitation_id: objUserFeedbackSubmit.invitationId,
                                        duration: objUserFeedbackSubmit.meetingDuration,
                                        timeZone: objUserFeedbackSubmit.userTimeZone
                                    };
                                    userModel.getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                                        var invitationData = {
                                            invitation_id: objUserFeedbackSubmit.invitationId,
                                            duration: objUserFeedbackSubmit.meetingDuration,
                                            meetingDetailId: meetingDetailId,
                                            day: day,
                                            url_key: results[0].url_key
                                        };
                                        sendMeetingFeedbackMentorMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    });
                                });
                            } else {
                                var randomString = "";
                                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                                for (var i = 0; i < 16; i++) {
                                    randomString += possible.charAt(Math.floor(Math.random() * possible.length));
                                }

                                var queryParam = {
                                    url_key: randomString + meetingMentorId,
                                    meeting_detail_id: meetingDetailId,
                                    user_type: "mentor",
                                    requester_id: meetingMentorId,
                                    user_id: meetingUserId,
                                    is_used: "0",
                                    created_date: moment.utc().format()
                                };
                                var strQuery = 'insert into leave_feedback set ?';
                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        var invitationDataMentor = {
                                            invitation_id: objUserFeedbackSubmit.invitationId,
                                            duration: objUserFeedbackSubmit.meetingDuration,
                                            timeZone: objUserFeedbackSubmit.mentorTimeZone
                                        };

                                        userModel.getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                                            var invitationDataUser = {
                                                invitation_id: objUserFeedbackSubmit.invitationId,
                                                duration: objUserFeedbackSubmit.meetingDuration,
                                                timeZone: objUserFeedbackSubmit.userTimeZone
                                            };
                                            userModel.getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                                                var invitationData = {
                                                    invitation_id: objUserFeedbackSubmit.invitationId,
                                                    duration: objUserFeedbackSubmit.meetingDuration,
                                                    meetingDetailId: meetingDetailId,
                                                    day: day,
                                                    url_key: randomString + meetingMentorId
                                                };
                                                sendMeetingFeedbackMentorMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                            });
                                        });
                                    }
                                });
                            }
                        }
                    });
                }

            });
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            recursiveCheckMeetingReview(req, res, day, arrMeetingData, counter, arrFinalDataSet, callback);
        });

    } else {
        callback(req, res, {message: "no logs found"});
    }
};
exports.recursiveCheckMeetingReview = recursiveCheckMeetingReview;


var checkUserHadGivenFeedBack = function (req, res, objDataParams, callback) {

    var requesterId = objDataParams.requesterId;
    var meetingDetailId = objDataParams.meetingDetailId;
    var objDataSend = {};


    var queryParam = [requesterId, meetingDetailId];
    var strQuery = 'Select COUNT(*) as count from user_reviews WHERE reviewer_id = ? AND meeting_detail_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
        } else {
            if (results[0].count > 0) {
                objDataSend = {
                    status: true
                };
            } else {
                objDataSend = {
                    status: false
                };
            }
        }
        callback(req, res, objDataSend);
    });
};
exports.checkUserHadGivenFeedBack = checkUserHadGivenFeedBack;

var sendMeetingFeedbackUserMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    var mentorIdPolicy = 0;
    mentorIdPolicy = arrMentorData.user_id;
    var day = invitationData.day;
    var key = invitationData.url_key;
    var meetingDetailId = invitationData.meetingDetailId;

    userModel.getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, mentorIdPolicy, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
            });

            // Send mail to user           

            var templateVariable = {
                templateURL: "mailtemplate/meetingreviewremindermailuser",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                userType: "user",
                key: key
            };


            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + userFullName + ",Request Review of your recent meeting with " + arrMentorData.firstName + " " + arrMentorData.lastName + "."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    var queryParam = [meetingDetailId];
                    var strQuery = "";
                    if (day == 3) {
                        strQuery = 'UPDATE meeting_details set flag_cron_review_3_day = 1 where meeting_id = ?';
                    } else {
                        strQuery = 'UPDATE meeting_details set flag_cron_review_7_day = 1 where meeting_id = ?';
                    }
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("record updated");
                        }
                    });
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingreviewremindermsguser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                key: key
            };
            req.body = {};
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendMeetingFeedbackUserMail = sendMeetingFeedbackUserMail;

var sendMeetingFeedbackMentorMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });


    var mentorIdPolicy = 0;
    mentorIdPolicy = arrMentorData.user_id;
    var day = invitationData.day;
    var key = invitationData.url_key;
    var meetingDetailId = invitationData.meetingDetailId;

    userModel.getCompletedMeetingData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, mentorIdPolicy, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
            });

            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/meetingreviewremindermailmentor",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                userType: "mentor",
                key: key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", Request Review of your recent meeting with " + userFullName + "."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    var queryParam = [meetingDetailId];
                    var strQuery = "";
                    if (day == 3) {
                        strQuery = 'UPDATE meeting_details set flag_cron_review_3_day = 1 where meeting_id = ?';
                    } else {
                        strQuery = 'UPDATE meeting_details set flag_cron_review_7_day = 1 where meeting_id = ?';
                    }
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log("record updated");
                        }
                    });

                }
            });
            // Send message to mentor inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingreviewremindermsgmentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                key: key
            };
            req.body = {};
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendMeetingFeedbackMentorMail = sendMeetingFeedbackMentorMail;


var classMarkerTestResponse = function (req, res, objDataParams, callback) {

    var reqBody = req.body;
    var reqBodyStr = JSON.stringify(reqBody);

    var result = reqBody.result;

    var userId = parseInt(result.cm_user_id);

    if (userId > 0) {
        var objDataForComplianceTest = {
            userId: userId
        }
        checkUserPassedComplianceTest(req, res, objDataForComplianceTest, function (req, res, objResponseData) {

            var complianceTestResultId = 0;

            if (typeof objResponseData.compliance_test_result_id != 'undefined') {
                // update compliance test
                var objParamsForInsertCopliance = {
                    coplianceData: reqBody,
                    complianceTestGivenData: objResponseData
                }
                updateIntoComplianceTest(req, res, objParamsForInsertCopliance, function (req, res, objResponseFromInsertIntoComplianceTest) {
                    var objParamsForUpdateMentorCompliancePassStatus = {
                        userId: userId,
                        coplianceData: reqBody
                    }

                    updateMentorCompliancePassStatus(req, res, objParamsForUpdateMentorCompliancePassStatus, function (req, res, objResponse) {
                        callback(req, res, {data: true});
                    });
                });
            } else {
                // insert into compliance test
                var objParamsForInsertCopliance = {
                    coplianceData: reqBody
                }
                insertIntoComplianceTest(req, res, objParamsForInsertCopliance, function (req, res, objResponseFromInsertIntoComplianceTest) {
                    var objParamsForUpdateMentorCompliancePassStatus = {
                        userId: userId,
                        coplianceData: reqBody
                    }
                    updateMentorCompliancePassStatus(req, res, objParamsForUpdateMentorCompliancePassStatus, function (req, res, objResponse) {
                        callback(req, res, {data: true});
                    });
                });
            }
        });
    } else {
        var templateVariable = {
            templateURL: "mailtemplate/blanktemplate.handlebars",
            blankData: reqBodyStr
        };

        var mailParamsObject = {
            templateVariable: templateVariable,
            to: 'milpas999@gmail.com',
            subject: 'Class marker GET'
        };
        mailer.sendMail(req, res, mailParamsObject, function (error) {
            if (error) {
                console.log("classmarker mail not send", err);
            } else {
                console.log("classmarker mail send");
            }
        });
        callback(req, res, {data: true});
    }
}
exports.classMarkerTestResponse = classMarkerTestResponse;

var checkUserPassedComplianceTest = function (req, res, objDataParams, callback) {
    var userId = objDataParams.userId;

    var queryParam = [userId];
    var strQuery = ' SELECT * ' +
            ' FROM compliance_test_results c ' +
            ' WHERE c.cm_user_id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, []);
        }
    });

}
exports.checkUserPassedComplianceTest = checkUserPassedComplianceTest;

var updateIntoComplianceTest = function (req, res, objDataParams, callback) {

    var currentUTC = constants.CURR_UTC_DATETIME();
    var complianceTestResultId = objDataParams.complianceTestGivenData.compliance_test_result_id;

    var params = {
        link_result_id: objDataParams.coplianceData.result.link_result_id,
        test_id: objDataParams.coplianceData.test.test_id,
        link_id: objDataParams.coplianceData.link.link_id,
        first: objDataParams.coplianceData.result.first,
        last: objDataParams.coplianceData.result.last,
        email: objDataParams.coplianceData.result.email,
        percentage: objDataParams.coplianceData.result.percentage,
        points_scored: objDataParams.coplianceData.result.points_scored,
        points_available: objDataParams.coplianceData.result.points_available,
        time_started: objDataParams.coplianceData.result.time_started,
        time_finished: objDataParams.coplianceData.result.time_finished,
        duration: objDataParams.coplianceData.result.duration,
        status: objDataParams.coplianceData.result.passed,
        requires_grading: objDataParams.coplianceData.result.requires_grading,
        cm_user_id: objDataParams.coplianceData.result.cm_user_id,
        access_code: objDataParams.coplianceData.result.access_code,
        extra_info: objDataParams.coplianceData.result.extra_info,
        extra_info2: objDataParams.coplianceData.result.extra_info2,
        extra_info3: objDataParams.coplianceData.result.extra_info3,
        extra_info4: objDataParams.coplianceData.result.extra_info4,
        extra_info5: objDataParams.coplianceData.result.extra_info5,
        ip_address: objDataParams.coplianceData.result.ip_address,
        datex: currentUTC
    };

    var queryParam = [params, complianceTestResultId];
    var strQuery = " UPDATE compliance_test_results SET ? WHERE compliance_test_result_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, {date: true});
    });
}
exports.updateIntoComplianceTest = updateIntoComplianceTest;

var insertIntoComplianceTest = function (req, res, objDataParams, callback) {

    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = {
        link_result_id: objDataParams.coplianceData.result.link_result_id,
        test_id: objDataParams.coplianceData.test.test_id,
        link_id: objDataParams.coplianceData.link.link_id,
        first: objDataParams.coplianceData.result.first,
        last: objDataParams.coplianceData.result.last,
        email: objDataParams.coplianceData.result.email,
        percentage: objDataParams.coplianceData.result.percentage,
        points_scored: objDataParams.coplianceData.result.points_scored,
        points_available: objDataParams.coplianceData.result.points_available,
        time_started: objDataParams.coplianceData.result.time_started,
        time_finished: objDataParams.coplianceData.result.time_finished,
        duration: objDataParams.coplianceData.result.duration,
        status: objDataParams.coplianceData.result.passed,
        requires_grading: objDataParams.coplianceData.result.requires_grading,
        cm_user_id: objDataParams.coplianceData.result.cm_user_id,
        access_code: objDataParams.coplianceData.result.access_code,
        extra_info: objDataParams.coplianceData.result.extra_info,
        extra_info2: objDataParams.coplianceData.result.extra_info2,
        extra_info3: objDataParams.coplianceData.result.extra_info3,
        extra_info4: objDataParams.coplianceData.result.extra_info4,
        extra_info5: objDataParams.coplianceData.result.extra_info5,
        ip_address: objDataParams.coplianceData.result.ip_address,
        datex: currentUTC
    };

    var strQuery = 'insert into compliance_test_results SET ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        callback(req, res, {data: true});
    });
}
exports.insertIntoComplianceTest = insertIntoComplianceTest;

var updateMentorCompliancePassStatus = function (req, res, objDataParams, callback) {

    var userId = objDataParams.coplianceData.result.cm_user_id;
    var complianceStatus = 0;

    if (objDataParams.coplianceData.result.passed) {
        complianceStatus = 1;
    }

    var params = {
        is_compliance_passed: complianceStatus
    };

    var queryParam = [params, userId];
    var strQuery = " UPDATE mentor_details SET ? WHERE user_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, {data: true});
    });
}
exports.updateMentorCompliancePassStatus = updateMentorCompliancePassStatus;

var doPaymentAfterMeetingCompletion = function (req, res, objParam, callback) {
    getCompletedMeetingInfo(req, res, {}, function (req, res, arrMeetingData) {
        recursiveMethodForCompletedMeetingPayment(req, res, arrMeetingData, 0, [], function (req, res, objFinalData) {
            callback(req, res, "");
        });
    });
};
exports.doPaymentAfterMeetingCompletion = doPaymentAfterMeetingCompletion;


var getCompletedMeetingInfo = function (req, res, objDataParams, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC];
    var strQuery = 'SELECT rr.refund_request_id,ins.paid_to_mentor,ins.paid_to_wissenx,mi.status meetingDetailStatus,mi.duration, zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            '   mi.invitation_id invitationId,ins.invoice_id invoiceId,ins.final_pay_to_mentor,ins.final_pay_to_wissenx, ' +
            '   IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) userId, ' +
            '   IF(mi.invitation_type = 1, mi.requester_id, mi.user_id) mentorId ' +
            '   FROM meeting_invitations mi  ' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   LEFT JOIN refund_request rr ON mi.invitation_id = rr.invitation_id AND rr.refund_request_id IS NULL ' +
            '   LEFT JOIN invoice ins ON mi.invitation_id = ins.invitation_id ' +
            '   WHERE mi.status = "completed" AND (ins.paid_to_mentor = "N" OR ins.paid_to_wissenx = "N") AND TIMESTAMPDIFF(HOUR,mi.updated_date,NOW())  >= 72 ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        callback(req, res, results);
    });
};
exports.getCompletedMeetingInfo = getCompletedMeetingInfo;




var doPaymentAfterMeetingCompletionAUTOMATEDTESTING = function (req, res, objParam, callback) {
    getCompletedMeetingInfoFORAUTOMATEDTESTING(req, res, {}, function (req, res, arrMeetingData) {
        recursiveMethodForCompletedMeetingPayment(req, res, arrMeetingData, 0, [], function (req, res, objFinalData) {
            callback(req, res, "");
        });
    });
}
exports.doPaymentAfterMeetingCompletionAUTOMATEDTESTING = doPaymentAfterMeetingCompletionAUTOMATEDTESTING;


var getCompletedMeetingInfoFORAUTOMATEDTESTING = function (req, res, objDataParams, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC];
    var strQuery = 'SELECT rr.refund_request_id,ins.paid_to_mentor,ins.paid_to_wissenx,mi.status meetingDetailStatus,mi.duration,zu.zone_name userTimeZone, zm.zone_name mentorTimeZone, ' +
            '   mi.invitation_id invitationId,ins.invoice_id invoiceId,ins.final_pay_to_mentor,ins.final_pay_to_wissenx, ' +
            '   IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) userId, ' +
            '   IF(mi.invitation_type = 1, mi.requester_id, mi.user_id) mentorId ' +
            '   FROM meeting_invitations mi  ' +
            '   LEFT JOIN user_details udu ON udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   LEFT JOIN zone zu ON zu.zone_id = udu.timezone_id ' +
            '   LEFT JOIN user_details udm ON udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   LEFT JOIN zone zm ON zm.zone_id = udm.timezone_id ' +
            '   LEFT JOIN refund_request rr ON mi.invitation_id = rr.invitation_id AND rr.refund_request_id IS NULL ' +
            '   LEFT JOIN invoice ins ON mi.invitation_id = ins.invitation_id ' +
            '   WHERE mi.status = "completed" AND (ins.paid_to_mentor = "N" OR ins.paid_to_wissenx = "N") AND TIMESTAMPDIFF(MINUTE,mi.updated_date,NOW())  >= 1 ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}
exports.getCompletedMeetingInfoFORAUTOMATEDTESTING = getCompletedMeetingInfoFORAUTOMATEDTESTING;



var recursiveMethodForCompletedMeetingPayment = function (req, res, arrMeetingData, counter, arrFinalDataSet, callback) {
    if (arrMeetingData.length > 0) {
        var thisResultSet = arrMeetingData[0];
        var refund_request_id = thisResultSet.refund_request_id;
        var invitationId = thisResultSet.invitationId;
        var paid_to_mentor = thisResultSet.paid_to_mentor;
        var paid_to_wissenx = thisResultSet.paid_to_wissenx;
        var invoiceId = thisResultSet.invoiceId;
        var final_pay_to_mentor = thisResultSet.final_pay_to_mentor;
        var final_pay_to_wissenx = thisResultSet.final_pay_to_wissenx;
        var userId = thisResultSet.userId;
        var mentorId = thisResultSet.mentorId;
        var duration = thisResultSet.duration;
        var meetingDetailStatus = thisResultSet.meetingDetailStatus;
        var userTimeZone = thisResultSet.userTimeZone;
        var mentorTimeZone = thisResultSet.mentorTimeZone;

        if (refund_request_id == null) {
            if (meetingDetailStatus == 'completed') {
                if (paid_to_mentor == 'N') {
                    var objParams = {
                        user_id: mentorId,
                        amount: final_pay_to_mentor
                    };
                    paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                        var queryParam = [invoiceId];
                        var strQuery = 'UPDATE invoice SET paid_to_mentor = "Y" where invoice_id = ?';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSlotRequestedData, fields) {
                            if (error) {
                                callback(req, res, 'error');
                            } else {
                                var data = {
                                    invoice_id: invoiceId,
                                    user_id: mentorId,
                                    user_type: "mentor",
                                    reason: "WALLBALUPD",
                                    transection_type: "PAYWALLET",
                                    payment_type: "CREDITED",
                                    payment_id: null,
                                    transection_id: null,
                                    capture_id: null,
                                    refund_id: null,
                                    amount: final_pay_to_mentor,
                                    status: 'completed',
                                    created_date: constants.CURR_UTC_DATETIME()
                                };
                                paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                                    arrMeetingData.splice(0, 1);
                                    arrFinalDataSet.push(thisResultSet);
                                    counter++;
                                    recursiveMethodForCompletedMeetingPayment(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
                                });
                            }
                        });
                    });

                    var invitationDataUser = {
                        invitation_id: invitationId,
                        duration: duration,
                        amount: final_pay_to_mentor,
                        timeZone: userTimeZone
                    };

                    var invitationDataMentor = {
                        invitation_id: invitationId,
                        duration: duration,
                        amount: final_pay_to_mentor,
                        timeZone: mentorTimeZone
                    };

                    var invitationData = {
                        invitation_id: invitationId,
                        duration: duration,
                        amount: final_pay_to_mentor,
                        timeZone: mentorTimeZone
                    };

                    userModel.getUserInformation(req, res, mentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                        userModel.getUserInformation(req, res, userId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                            sendMentorWalletUpdMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                        });
                    });

                }
                if (paid_to_wissenx == 'N') {
                    var objParams = {
                        user_id: "1",
                        amount: final_pay_to_wissenx
                    };
                    paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                        var queryParam = [invoiceId];
                        var strQuery = 'UPDATE invoice SET paid_to_wissenx = "Y" where invoice_id = ?';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSlotRequestedData, fields) {
                            if (error) {
                                callback(req, res, 'error');
                            } else {
//                                var data = {
//                                    invoice_id: invoiceId,
//                                    user_id: mentorId,
//                                    user_type: "mentor",
//                                    reason: "WALLBALUPD",
//                                    transection_type: "PAYWALLET",
//                                    payment_type: "CREDITED",
//                                    payment_id: null,
//                                    transection_id: null,
//                                    capture_id: null,
//                                    refund_id: null,
//                                    amount: final_pay_to_mentor,
//                                    status: 'completed',
//                                    created_date: constants.CURR_UTC_DATETIME()
//                                };
//                                paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
//                                    arrMeetingData.splice(0, 1);
//                                    arrFinalDataSet.push(thisResultSet);
//                                    counter++;
//                                    recursiveMethodForCompletedMeetingPayment(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
//                                });
                            }
                        });
                    });
                }
            }
        } else {
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            recursiveMethodForCompletedMeetingPayment(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        }
    } else {
        callback(req, res, {message: "no logs found"});
    }
};
exports.recursiveMethodForCompletedMeetingPayment = recursiveMethodForCompletedMeetingPayment;

var sendMentorWalletUpdMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    var mentorIdPolicy = 0;
    mentorIdPolicy = arrMentorData.user_id;
    var amount = invitationData.amount;

    userModel.getCompletedMeetingData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, mentorIdPolicy, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
            });

            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/meetingcompletewalletmail",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                userType: "mentor",
                amount: amount
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", Thank you for conducting a  meeting with " + userFullName + ". We are crediting your account for your mentoring charges for this meeting."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("success");
                }
            });
        });
    });
};
exports.sendMentorWalletUpdMail = sendMentorWalletUpdMail;

var chkMeeting = function (req, res, callback) {
    var queryParam = [];
    var strQuery = "SELECT md.meeting_url_key,mi.invitation_id,mi.requester_id,mi.user_id,mi.slot_date,mi.slot_time ,uz.zone_name as userZone ,mz.zone_name as metorZone from meeting_invitations mi " +
            " LEFT JOIN user_details ud ON ud.user_id = mi.requester_id " +
            " LEFT JOIN user_details uds ON uds.user_id = mi.user_id " +
            " LEFT JOIN zone mz ON mz.zone_id = ud.timezone_id " +
            " LEFT JOIN zone uz ON uz.zone_id = uds.timezone_id " +            
            " LEFT JOIN meeting_details md ON md.invitation_id = mi.invitation_id " +
            " WHERE mi.status = 'accepted'";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        
        if (results.length > 0) {


//            results.forEach(function (eachdata) {
//                var tmpObj = {
//                    requester_id: eachdata.requester_id,
//                    user_id: eachdata.user_id,
//                    time: moment.tz(eachdata.slot_date + " " + eachdata.slot_time, 'UTC').tz("Asia/Kolkata").format('YYYY-MM-DD HH:mm:ss')
//                        };
//                arrReturnData.push(tmpObj);
//            });






            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
};
exports.chkMeeting = chkMeeting;
