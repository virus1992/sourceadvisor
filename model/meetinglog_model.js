var exports = module.exports = {};
var dateFormat = require('dateformat');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var fs = require('fs');
var async = require("async");
var handlebars = require('handlebars');

var miscFunction = require('./../model/misc_model');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var messagesModel = require('./../model/messages_model');
var frontConstant = require('./../modules/front_constant');
var sendResponse = require('./../modules/sendresponse');


var getLogsEntry = function(req, res, objData, callback) {
    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var fetchFor = objData.fetchFor;

    var queryParam = [meetingUrlKey];

    var strQuery = ' SELECT pmlm.participant_meeting_log_master_id participantMeetingLogMasterId, pmlm.meeting_id meetingId, pmlm.meeting_urk_key meetingUrlKey ' +
            ' FROM participant_meeting_log_master pmlm ' +
            ' WHERE pmlm.meeting_urk_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            callback(req, res, results[0]);
        }
        else {
            callback(req, res, {});
        }
    });
};
exports.getLogsEntry = getLogsEntry;

var updateOarticipantDisconnectedLog = function(req, res, callback) {
    var currentParticipant = req.session.userId;
    var meetingUrlKey = req.body.meeting_key;

    var dataToPass = {
        currentParticipant: currentParticipant,
        meetingUrlKey: meetingUrlKey
    };
    getOtherPatricipantOfMeeting(req, res, dataToPass, function(req, res, objOtherParticipantData){
        if(typeof objOtherParticipantData.otherUserId != 'undefined') {
            var otherUserId = objOtherParticipantData.otherUserId;

            var dataToPass = {
                participantId: otherUserId,
                meetingUrlKey: meetingUrlKey
            };
            updateUsersMeetingDisconnectLogForOtherParticipant(req, res, dataToPass, function(req, res, flagInfo) {
                if(flagInfo) {
                    callback(req, res, true);
                }
            });
        }
    });

}
exports.updateOarticipantDisconnectedLog = updateOarticipantDisconnectedLog;


var updateUsersMeetingDisconnectLogForOtherParticipant = function (req, res, objData, callback) {
    var participantId = objData.participantId;
    var meetingUrlKey = objData.meetingUrlKey;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, meetingUrlKey, participantId];

    var strQuery =  '   UPDATE participant_meeting_log pml, participant_meeting_log_master pmlm ' + 
                    '   SET pml.log_out_datex = ?, ' + 
                    '       pml.flag_active = "N", ' + 
                    '       pml.disconnect_type = "" ' + 
                    '   WHERE pml.participant_meeting_log_master_id = pmlm.participant_meeting_log_master_id ' + 
                    '   AND pmlm.meeting_urk_key = ? ' + 
                    '   AND pml.participant_id = ? ' + 
                    '   AND flag_active = "Y"';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, true);
    });
};
exports.updateUsersMeetingDisconnectLogForOtherParticipant = updateUsersMeetingDisconnectLogForOtherParticipant;




var updateCurrentUserLeavingMeetingWindow = function(req, res, objDataParams, callback) {
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var participantMeetingLogMasterId = objDataParams.participantMeetingLogMasterId;
    var participantMeetingLogId = objDataParams.participantMeetingLogId;
    var disconnect_reason = objDataParams.disconnect_reason;

    var dataToPass = {
        participantMeetingLogMasterId: participantMeetingLogMasterId,
        participantMeetingLogId: participantMeetingLogId,
        meetingUrlKey: meetingUrlKey,
        disconnect_reason: disconnect_reason
    };
    updateUsersMeetingDisconnectLog(req, res, dataToPass, function(req, res, flagInfo) {
        if(flagInfo) {
            callback(req, res, true);
        }
        else {
            callback(req, res, false);
        }
    });
};
exports.updateCurrentUserLeavingMeetingWindow = updateCurrentUserLeavingMeetingWindow;

var updateUsersMeetingDisconnectLog = function (req, res, objData, callback) {
    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var participantMeetingLogId = objData.participantMeetingLogId;
    var meetingUrlKey = objData.meetingUrlKey;
    var disconnect_reason = objData.disconnect_reason;
    
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, disconnect_reason, participantMeetingLogId];

    var strQuery =  '   UPDATE participant_meeting_log pml ' + 
                    '   SET pml.log_out_datex = ?, ' + 
                    '       pml.disconnect_type = ?, ' + 
                    '       pml.flag_active = "N" ' + 
                    '   WHERE pml.participant_meeting_log_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            ///throw error;
            callback(req, res, false);
        }
        callback(req, res, true);
    });
};
exports.updateUsersMeetingDisconnectLog = updateUsersMeetingDisconnectLog;



var updateCurrentUserNOTLeavingMeetingWindow = function(req, res, objDataParams, callback) {
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var participantMeetingLogMasterId = objDataParams.participantMeetingLogMasterId;
    var participantMeetingLogId = objDataParams.participantMeetingLogId;
    var disconnect_reason = objDataParams.disconnect_reason;

    var dataToPass = {
        participantMeetingLogMasterId: participantMeetingLogMasterId,
        participantMeetingLogId: participantMeetingLogId,
        meetingUrlKey: meetingUrlKey,
        disconnect_reason: disconnect_reason
    };
    updateUsersMeetingConnectAgainLog(req, res, dataToPass, function(req, res, flagInfo) {
        if(flagInfo) {
            callback(req, res, true);
        }
        else {
            callback(req, res, false);
        }
    });
};
exports.updateCurrentUserNOTLeavingMeetingWindow = updateCurrentUserNOTLeavingMeetingWindow;


var updateUsersMeetingConnectAgainLog = function(req, res, objData, callback) {
    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var participantMeetingLogId = objData.participantMeetingLogId;
    var meetingUrlKey = objData.meetingUrlKey;
    var disconnect_reason = objData.disconnect_reason;

    var queryParam = [disconnect_reason, participantMeetingLogId];

    var strQuery =  '   UPDATE participant_meeting_log pml ' + 
                    '   SET pml.log_out_datex = NULL, ' + 
                    '       pml.disconnect_type = ?, ' + 
                    '       pml.flag_active = "Y" ' + 
                    '   WHERE pml.participant_meeting_log_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            ///throw error;
            callback(req, res, false);
        }
        callback(req, res, true);
    });
};

var getOtherPatricipantOfMeeting = function(req ,res, objData, callback) {
    var currentParticipant = objData.currentParticipant;
    var meetingUrlKey = objData.meetingUrlKey;

    var queryParam = [currentParticipant, meetingUrlKey];

    var strQuery =  '   SELECT IF(mi.requester_id = ?, mi.user_id, mi.requester_id) otherUser ' + 
                    '   FROM meeting_details md, meeting_invitations mi ' + 
                    '   WHERE md.invitation_id = mi.invitation_id ' + 
                    '   AND md.meeting_url_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        
        if(results.length > 0) {
            var dataToPass = {
                otherUserId: results[0].otherUser
            };
            callback(req, res, dataToPass);
        }
        else {
            callback(req, res, {});
        }
    });

}
exports.getOtherPatricipantOfMeeting = getOtherPatricipantOfMeeting;

var authorizeFromMeetingLog = function(req, res, objData, callback) {
    var participantId = objData.userId;
    var meetingUrlKey = objData.meetingUrlKey;

    var objDataParams = {
        userId : participantId,
        meetingUrlKey: meetingUrlKey
    }

    checkMeetingTimeConstraintForMeeting(req, res, objDataParams, function(req, res, arrMeetingTimeConstrainParams){
        var meetingCanStart = arrMeetingTimeConstrainParams[0].flagMeetingCanStart;

        if(meetingCanStart) {
            checkUserTypeForThisPerticularMeeting(req, res, objDataParams, function(req, res, objUserInfo) {

                var loggedInUserTypeForLoggedInMeeting = objUserInfo.loggedInUserTypeForLoggedInMeeting;
                
                checkMeetingLogExists(req, res, objData, function(req, res, objMasterLogData) {

                    // if master log is already present
                    if(typeof objMasterLogData.participantMeetingLogMasterId != 'undefined') {

                        var participantMeetingLogMasterId = objMasterLogData.participantMeetingLogMasterId;
                        var meetingId = objMasterLogData.meetingId;
                        var meetingUrlKey = objMasterLogData.meetingUrlKey;

                        var dataToPass = {
                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                            participantId: participantId,
                            expressSessionId: req.sessionID
                        };

                        checkParticipantActiveEntryIsPresentOrNot(req, res, dataToPass, function(req, res, objCheckData) {


                            //participantMeetingLogId, participantMeetingLogMasterId
                            //if entry is present
                            if(typeof objCheckData.participantMeetingLogId != 'undefined') {
                                var objDataToPass = {
                                    allowAccess: false,
                                    flagNoShow: false,
                                    participantMeetingLogMasterId: objCheckData.participantMeetingLogMasterId,
                                    participantMeetingLogId: objCheckData.participantMeetingLogId
                                };
                                callback(req, res, objDataToPass);
                            }
                            // if entry is not present 
                            else {


                                checkIfNoShowEntryAlreadyPresent(req, res, objDataParams, function(req, res, flagNoShowAlreadyApplied) {

                                    // if no show is already applied
                                    if(flagNoShowAlreadyApplied) {
                                        var objDataToPass = {
                                            allowAccess: false,
                                            flagNoShow: true,
                                            participantMeetingLogMasterId: objCheckData.participantMeetingLogMasterId,
                                            participantMeetingLogId: objCheckData.participantMeetingLogId
                                        };
                                        callback(req, res, objDataToPass);
                                    }
                                    // if no show entry is not present, that means user/mentor can join the meeting
                                    else {

                                        var currentUTC = constants.CURR_UTC_DATETIME();

                                        var objDataToCreateParticipantLogEntry = {
                                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                                            participantId: participantId,
                                            expressSessionId: req.sessionID,
                                            connectTime: currentUTC,
                                            userType: loggedInUserTypeForLoggedInMeeting
                                        };
                                            

                                        /// check for no show case.
                                        checkForNoShowCaseKicksIn(req, res, objData, function(req, res, flagNoShowResponse) {
                                            // if for loggedin user, no show policy kicks in, then made no show entry in log table.
                                            if(flagNoShowResponse) {
                                                createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function(req, res, objMeetingLogInfo){
                                                    var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                                                    var objDataToPass = {
                                                        allowAccess: false,
                                                        flagNoShow: true,
                                                        participantMeetingLogMasterId: participantMeetingLogMasterId,
                                                        participantMeetingLogId: participantMeetingLogId
                                                    };
                                                    callback(req, res, objDataToPass);
                                                });
                                            }
                                            // else make all the meeting realted entries in databases.
                                            else {

                                                createParticipantMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function(req, res, objMeetingLogInfo) {

                                                    var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                                                    var objDataToPass = {
                                                        allowAccess: true,
                                                        flagNoShow: false,
                                                        participantMeetingLogMasterId: participantMeetingLogMasterId,
                                                        participantMeetingLogId: participantMeetingLogId
                                                    };
                                                    callback(req, res, objDataToPass);
                                                });    
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    // if master log is not present
                    else {

                        createMasterLogEntry(req, res, objData, function(req, res, objMasterLogDataCreated) {
                            var currentUTC = constants.CURR_UTC_DATETIME();
                            var participantMeetingLogMasterId = objMasterLogDataCreated.participantMeetingLogMasterId;

                            var objDataToCreateParticipantLogEntry = {
                                participantMeetingLogMasterId: participantMeetingLogMasterId,
                                participantId: participantId,
                                expressSessionId: req.sessionID,
                                connectTime: currentUTC,
                                userType: loggedInUserTypeForLoggedInMeeting
                            };


                            /// check for no show case.
                            checkForNoShowCaseKicksIn(req, res, objData, function(req, res, flagNoShowResponse) {

                                // if for loggedin user, no show policy kicks in, then made no show entry in log table.
                                if(flagNoShowResponse) {
                                    createParticipantNoShowMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function(req, res, objMeetingLogInfo){
                                        var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                                        var objDataToPass = {
                                            allowAccess: false,
                                            flagNoShow: true,
                                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                                            participantMeetingLogId: participantMeetingLogId
                                        };
                                        callback(req, res, objDataToPass);
                                    });
                                }
                                // else make all the meeting realted entries in databases.
                                else {
                                    createParticipantMeetingLogEntry(req, res, objDataToCreateParticipantLogEntry, function(req, res, objMeetingLogInfo) {

                                        var participantMeetingLogId = objMeetingLogInfo.participantMeetingLogId;

                                        var objDataToPass = {
                                            allowAccess: true,
                                            flagNoShow: false,
                                            participantMeetingLogMasterId: participantMeetingLogMasterId,
                                            participantMeetingLogId: participantMeetingLogId
                                        };
                                        callback(req, res, objDataToPass);
                                    });
                                }
                            });
                        });
                    }
                });
            });
        }
        else {
            var objDataToPass = {
                allowAccess: false,
                flagNoShow: false,
                participantMeetingLogMasterId: 0,
                participantMeetingLogId: 0
            };
            callback(req, res, objDataToPass);
        }
    });
};
exports.authorizeFromMeetingLog = authorizeFromMeetingLog;


var checkIfNoShowEntryAlreadyPresent = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var queryParam = [userId, meetingUrlKey];
    var strQuery = '    SELECT COUNT(*) totalCnt ' + 
                    '   FROM participant_meeting_log_master pmlm, participant_meeting_log pml ' + 
                    '   WHERE pmlm.participant_meeting_log_master_id = pml.participant_meeting_log_master_id ' + 
                    '   AND pml.participant_id = ? ' + 
                    '   AND pmlm.meeting_urk_key = ? ' + 
                    '   AND pml.disconnect_type = "no_show" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            var resultSet = results[0];

            if(resultSet.totalCnt > 0) {
                callback(req, res, true);
            }
            else {
                callback(req, res, false);
            }
        }
        else {
            callback(req, res, false);
        }
    });
};
exports.checkIfNoShowEntryAlreadyPresent = checkIfNoShowEntryAlreadyPresent;

var checkForNoShowCaseKicksIn = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [currentUTC, userId, meetingUrlKey];

    var strQuery = '    SELECT ' + 
                    '	    IF(pml.participant_meeting_log_id IS NULL, IF(TIMESTAMPDIFF(MINUTE, md.scheduled_start_time, ?) > 10, 1, 0), 0) userNotShowKicksIn ' + 
                    '   FROM meeting_details md  ' + 
                    '	    LEFT JOIN participant_meeting_log_master pmlm  ' + 
                    '	        ON md.meeting_url_key = pmlm.meeting_urk_key ' + 
                    '	    LEFT JOIN participant_meeting_log pml ' + 
                    '	        ON pml.participant_meeting_log_master_id = pmlm.participant_meeting_log_master_id AND pml.participant_id = ? ' + 
                    '   WHERE md.meeting_url_key = ? ' + 
                    '   GROUP BY pml.participant_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            var resultSet = results[0];

            if(resultSet.userNotShowKicksIn == "1") {
                callback(req, res, true);
            }
            else {
                callback(req, res, false);
            }
        }
        else {
            callback(req, res, false);
        }
    });
};
exports.checkForNoShowCaseKicksIn = checkForNoShowCaseKicksIn;

var checkUserTypeForThisPerticularMeeting = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var queryParam = [userId, userId, meetingUrlKey];
    
    var strQuery = '    SELECT IF(mi.invitation_type = 0, IF(mi.requester_id = ?, "user", "mentor"), IF(mi.requester_id = ?, "mentor", "user"))  loggedInUserTypeForLoggedInMeeting' + 
                    '   FROM meeting_details md, meeting_invitations mi ' + 
                    '   WHERE md.invitation_id = mi.invitation_id ' + 
                    '   AND md.meeting_url_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if(results.length > 0) {
            callback(req, res, results[0]);
        }
        else {
            callback(req, res, {});
        }
    });
    
};
exports.checkUserTypeForThisPerticularMeeting = checkUserTypeForThisPerticularMeeting;


var checkParticipantActiveEntryIsPresentOrNot = function(req, res, objData, callback) {

    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var participantId = objData.participantId;
    //var expressSessionId = objData.expressSessionId;

    var queryParam = [participantMeetingLogMasterId, participantId];
    var strQuery = ' SELECT pml.participant_meeting_log_id participantMeetingLogId, pml.participant_meeting_log_master_id participantMeetingLogMasterId, pml.participant_id participantId, pml.session_id sessionId, pml.log_in_datex logINDatex, pml.log_out_datex logOutDateX, pml.flag_active flagActive ' +
            ' FROM participant_meeting_log pml ' +
            ' WHERE pml.participant_meeting_log_master_id = ? ' + 
            ' AND pml.participant_id = ? ' + 
            ' AND pml.flag_active = "Y" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            callback(req, res, results[0]);
        }
        else {
            callback(req, res, {});
        }
    });

};
exports.checkParticipantActiveEntryIsPresentOrNot = checkParticipantActiveEntryIsPresentOrNot;

var createParticipantMeetingLogEntry = function(req, res, objData, callback) {
    
    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var participantId = objData.participantId;
    var expressSessionId = objData.expressSessionId;
    var connectTime = objData.connectTime;
    var userType = objData.userType;

    if(typeof expressSessionId == 'undefined') {
        expressSessionId = "";
    }

    var insertParams = {
        participant_meeting_log_master_id: participantMeetingLogMasterId, 
        participant_id: participantId, 
        user_type: userType, 
        session_id: expressSessionId, 
        log_in_datex: connectTime
    };

    dbconnect.executeQuery(req, res, 'INSERT INTO participant_meeting_log SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var participantMeetingLogId = results.insertId;

        var dataToPass = {
            participantMeetingLogId: participantMeetingLogId
        };

        callback(req, res, dataToPass);
    });
};
exports.createParticipantMeetingLogEntry = createParticipantMeetingLogEntry;


var createParticipantNoShowMeetingLogEntry = function(req, res, objData, callback) {
    
    var participantMeetingLogMasterId = objData.participantMeetingLogMasterId;
    var participantId = objData.participantId;
    var expressSessionId = objData.expressSessionId;
    var connectTime = objData.connectTime;
    var userType = objData.userType;

    if(typeof expressSessionId == 'undefined') {
        expressSessionId = "";
    }

    var insertParams = {
        participant_meeting_log_master_id: participantMeetingLogMasterId, 
        participant_id: participantId, 
        user_type: userType, 
        session_id: expressSessionId, 
        log_in_datex: connectTime,
        log_out_datex: connectTime,
        disconnect_type: 'no_show',
        flag_video_capabilities: 'N',
        flag_audio_capabilities: 'N',
        flag_active: 'N'
    };

    dbconnect.executeQuery(req, res, 'INSERT INTO participant_meeting_log SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var participantMeetingLogId = results.insertId;

        var dataToPass = {
            participantMeetingLogId: participantMeetingLogId
        };

        callback(req, res, dataToPass);
    });
};
exports.createParticipantNoShowMeetingLogEntry = createParticipantNoShowMeetingLogEntry;

var createMasterLogEntry = function(req, res, objData, callback) {
    var meetingUrlKey = objData.meetingUrlKey;
    var insertParams = {
        meeting_id: "0",
        meeting_urk_key: meetingUrlKey
    };
    dbconnect.executeQuery(req, res, 'INSERT INTO participant_meeting_log_master SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var participantMeetingLogMasterId = results.insertId;

        var dataToPass = {
            participantMeetingLogMasterId: participantMeetingLogMasterId
        };

        callback(req, res, dataToPass);
    });
};
exports.createMasterLogEntry = createMasterLogEntry;


var checkMeetingLogExists = function(req, res, objData, callback) {
    var meetingUrlKey = objData.meetingUrlKey;

    var queryParam = [meetingUrlKey];

    var strQuery = ' SELECT pmlm.participant_meeting_log_master_id participantMeetingLogMasterId, pmlm.meeting_id meetingId, pmlm.meeting_urk_key meetingUrlKey ' +
            ' FROM participant_meeting_log_master pmlm ' +
            ' WHERE pmlm.meeting_urk_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            callback(req, res, results[0]);
        }
        else {
            callback(req, res, {});
        }
    });
};
exports.checkMeetingLogExists = checkMeetingLogExists;



var getMeetingLogFromMeetingUrlKey = function(req, res, objData, callback) {
    var meetingUrlKey = objData.meetingUrlKey;

    getMeetingLogsDataForSessionFromMeetingUrlKey(req, res, objData, function(req, res, objMeetingLog) {
        callback(req, res, objMeetingLog);
    });
};
exports.getMeetingLogFromMeetingUrlKey = getMeetingLogFromMeetingUrlKey;


var getMeetingLogsDataForSessionFromMeetingUrlKey = function(req, res, objData, callback) {
    var meetingUrlKey = objData.meetingUrlKey;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [meetingUrlKey, meetingUrlKey];

    var strQuery =  '   SELECT id, dateX, participantId, firstName, lastName, logType, convertedDateX , pid ' + 
                    '   FROM ( ' + 
                    '   SELECT pml.participant_meeting_log_id id, pml.participant_meeting_log_master_id pid, pml.log_in_datex dateX, pml.participant_id participantId, ud.first_name firstName, ud.last_name lastName, "LOGIN" logType, DATE_FORMAT(CONVERT_TZ(pml.log_in_datex, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME + '") convertedDateX ' + 
                    '   FROM participant_meeting_log pml, participant_meeting_log_master pmlm, user_details ud ' + 
                    '   WHERE pml.participant_meeting_log_master_id = pmlm.participant_meeting_log_master_id ' + 
                    '   AND pml.participant_id = ud.user_id ' + 
                    '   AND pmlm.meeting_urk_key = ? ' + 
                    '   AND pml.log_in_datex IS NOT NULL ' + 
                    '   UNION ' + 
                    '   SELECT pml.participant_meeting_log_id id, pml.participant_meeting_log_master_id pid ,pml.log_out_datex dateX, pml.participant_id participantId, ud.first_name firstName, ud.last_name lastName, "LOGOUT" logType, DATE_FORMAT(CONVERT_TZ(pml.log_out_datex, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME + '") convertedDateX ' + 
                    '   FROM participant_meeting_log pml, participant_meeting_log_master pmlm, user_details ud ' + 
                    '   WHERE pml.participant_meeting_log_master_id = pmlm.participant_meeting_log_master_id ' + 
                    '   AND pml.participant_id = ud.user_id ' + 
                    '   AND pmlm.meeting_urk_key = ? ' + 
                    '   AND pml.log_out_datex IS NOT NULL) ' + 
                    '   AS meetingLog ORDER BY dateX DESC ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            var dataToSend = {
                logData: results,
                currentServerTimeStamp: currentUTC
            }
            callback(req, res, dataToSend);
        }
        else {
            var dataToSend = {
                logData: {},
                currentServerTimeStamp: currentUTC
            }
            callback(req, res, dataToSend);
        }
    });
};
exports.getMeetingLogsDataForSessionFromMeetingUrlKey = getMeetingLogsDataForSessionFromMeetingUrlKey;

var checkMeetingTimeConstraintForMeeting = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var currentUTC = constants.CURR_UTC_DATETIME();
    
    var queryParam = [currentUTC, currentUTC, meetingUrlKey];
    
    var strQuery =  '   SELECT ' + 
                    '       IF(TIMESTAMPDIFF(MINUTE, ?, scheduled_start_time) <= 15, 1, 0) flagMeetingCanStart,  ' + 
                    '       IF(TIMESTAMPDIFF(MINUTE, ?, TIMESTAMPADD(MINUTE, 15, scheduled_end_time)) > 0, 0, 1) flagMeetingTotalEnd ' + 
                    '   FROM meeting_details  ' + 
                    '   WHERE meeting_url_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            callback(req, res, results);
        }
        else {
            callback(req, res, {});
        }
    });
};
exports.checkMeetingTimeConstraintForMeeting = checkMeetingTimeConstraintForMeeting;

var checkAnyTechnicalIssueForMeetingForSourceAdvisor = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var queryParam = [meetingUrlKey];

    var strQuery =  '   SELECT COUNT(*) totalCnt ' + 
                    '   FROM participant_meeting_log_master pmlm, participant_meeting_log pml ' + 
                    '   WHERE pmlm.meeting_urk_key = ? ' + 
                    '   AND pml.participant_meeting_log_master_id = pmlm.participant_meeting_log_master_id ' + 
                    '   AND pml.log_out_datex IS NULL ' + 
                    '   AND pml.flag_active = "Y" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if(results.length > 0) {
            var resultSet = results[0];
            if(resultSet.totalCnt == "2") {
                callback(req, res, true);
            }
            else {
                callback(req, res, false);
            }
        }
        else {
            callback(req, res, false);
        }
    });

};
exports.checkAnyTechnicalIssueForMeetingForSourceAdvisor = checkAnyTechnicalIssueForMeetingForSourceAdvisor;


var getMeetingTimerInfo = function(req, res, objParams, callback) {
    var meetingUrlKey = objParams.meetingUrlKey;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, currentUTC, currentUTC, meetingUrlKey];

    var strQuery =  '   SELECT ' + 
                    '       TIMESTAMPDIFF(SECOND, ?, md.scheduled_start_time) startTimerAfterTheseSecond, ' + 
                    '       IF(TIMESTAMPDIFF(SECOND, ?, md.scheduled_start_time) <= 0, 1, 0) timerRunning, ' + 
                    '       TIMESTAMPDIFF(SECOND, md.scheduled_start_time, ?) secondsElapsed, ' + 
                    '       mi.duration duration, ' + 
                    '       md.scheduled_start_time scheduledStartTime, ' + 
                    '       md.scheduled_end_time scheduledEndTime ' + 
                    '   FROM meeting_details md, meeting_invitations mi ' + 
                    '   WHERE meeting_url_key = ? ' + 
                    '   AND md.invitation_id = mi.invitation_id ' ;

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if(results.length > 0) {
            var resultSet = results[0];
            resultSet.userTimeZone = req.session.timezone,
            resultSet.currentServerTimeStamp = currentUTC
            callback(req, res, resultSet);
        }
        else {
            callback(req, res, {});
        }
    });
};
exports.getMeetingTimerInfo = getMeetingTimerInfo;

var getOppositeUserIdForMeeting = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var queryParam = [userId, meetingUrlKey];

    var strQuery =  '   SELECT IF(mi.requester_id = ?, mi.user_id, mi.requester_id) userId ' + 
                    '   FROM meeting_details md, meeting_invitations mi ' + 
                    '   WHERE md.invitation_id = mi.invitation_id ' + 
                    '   AND md.meeting_url_key = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if(results.length > 0) {
            var resultSet = results[0];

            var objDataParams = {
                userId : resultSet.userId,
                meetingUrlKey: meetingUrlKey
            }
            checkUserTypeForThisPerticularMeeting(req, res, objDataParams, function(req, res, objUserInfo) {
                var userTypeForLoggedInMeeting = objUserInfo.loggedInUserTypeForLoggedInMeeting;

                resultSet.userTypeForLoggedInMeeting = userTypeForLoggedInMeeting;
                callback(req, res, resultSet);
            });
        }
        else {
            callback(req, res, {});
        }
    });
    
};
exports.getOppositeUserIdForMeeting = getOppositeUserIdForMeeting;



var updateInvitationTableForNoShow = function(req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var status = objDataParams.status;
    var noShowUserId = objDataParams.noShowUserId;

    var params = [status, 'ended', noShowUserId, invitationId];
    var sqlQuery = ' UPDATE meeting_invitations mi, meeting_details md SET mi.flag_cron_no_show_check = "Y", mi.status = ?, md.status = ?, mi.declined_by = ? WHERE mi.invitation_id = ? AND mi.invitation_id = md.invitation_id';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateInvitationTableForNoShow = updateInvitationTableForNoShow;

var updateInvitationTableForMeetingEnd = function(req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var status = objDataParams.status;

    var params = [status, 'ended', invitationId];
    var sqlQuery = ' UPDATE meeting_invitations mi, meeting_details md SET mi.flag_cron_meeting_end_check = "Y", mi.status = ?, md.status = ? WHERE mi.invitation_id = ? AND mi.invitation_id = md.invitation_id';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateInvitationTableForMeetingEnd = updateInvitationTableForMeetingEnd;


/** CHAT FUNCTIONS START */

var getChatMasterLogInfoForMeeting = function(req, res, objDataParams, callback) {
    // function call to check if chat master log is already present
    getChatMasterLogInfo(req, res, objDataParams,function(req, res, objChatMasterLogInfo) {
        // if chat master Log is present for that meeting url
        if(typeof objChatMasterLogInfo.meetingChatMasterId != 'undefined') {
            callback(req, res, objChatMasterLogInfo);
        }
        // if chat master log is not present for the meeting url
        else {
            createChatMasterLogForMeeting(req, res, objDataParams, function(req, res, objNewCharMasterLogCreated) {
                callback(req, res, objNewCharMasterLogCreated);
            });
        }
    });
};
exports.getChatMasterLogInfoForMeeting = getChatMasterLogInfoForMeeting;


var createChatMasterLogForMeeting = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    getMeetingDetailsFromMeetingUrlKey(req, res, meetingUrlKey, function(req, res, objMeetingData) {
        // if meeting Data Present
        if(typeof objMeetingData.meetingDetailId != 'undefined') {
            var objCreateChatMasterParams = {
                meetingDetailId: objMeetingData.meetingDetailId,
                meetingUrlKey: meetingUrlKey
            };
            createChatMasterLogEntry(req, res, objCreateChatMasterParams, function(req, res, objChatMasterLogInfo) {
                var meetingChatMasterId = objChatMasterLogInfo.meetingChatMasterId;

                getChatMasterLogInfo(req ,res, { meetingUrlKey: meetingUrlKey }, function(req, res, objCreatedMeetingData) {
                    callback(req, res, objCreatedMeetingData);
                });
            });
        }
        else {
            callback(req, res, objMeetingData);
        }
    })
};
exports.createChatMasterLogForMeeting = createChatMasterLogForMeeting;

var createChatMasterLogEntry = function(req, res, objDataParams, callback) {
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meetingUrlKey;
    var currentUTC = constants.CURR_UTC_DATETIME();
    
    var insertParams = {
        meeting_detail_id: meetingDetailId, 
        meeting_url_key: meetingUrlKey, 
        created_date: currentUTC
    };

    dbconnect.executeQuery(req, res, 'INSERT INTO meeting_chat_master SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var meetingChatMasterId = results.insertId;

        var dataToPass = {
            meetingChatMasterId: meetingChatMasterId
        };

        callback(req, res, dataToPass);
    });
};
exports.createChatMasterLogEntry = createChatMasterLogEntry;

var getMeetingDetailsFromMeetingUrlKey = function(req, res, meetingUrlKey, callback) {

    var params = [meetingUrlKey];
    var sqlQuery = '    SELECT md.meeting_id meetingDetailId, md.invitation_id meetingInvitationId, md.meeting_url_key meetingUrlKey, md.scheduled_start_time scheduledStartTime, md.scheduled_end_time scheduledEndTime, md.status meetingDetailStatus, mi.requester_id requesterId, mi.user_id userId, mi.invoice_id invoiceId, mi.duration meetingDuration, mi.notified_to_user flagNotifiedToUser, mi.notified_to_mentor flagNotifiedToMentor, mi.flag_payment_done flagPaymentDone, mi.status meetingInvitationStatus, mi.invitation_type invitationType, mi.declined_by declinedById, mi.flag_cron_no_show_check flagCronNoShowChedk, mi.flag_cron_meeting_end_check flagCronMeetingEndCheck ' + 
                    '   FROM meeting_details md, meeting_invitations mi ' + 
                    '   WHERE md.invitation_id = mi.invitation_id ' + 
                    '   AND md.meeting_url_key = ? ';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if(results.length > 0) {
                callback(req, res, results[0]);
            }
            else {
                callback(req, res, {});
            }
        }
    });
};
exports.getMeetingDetailsFromMeetingUrlKey = getMeetingDetailsFromMeetingUrlKey;

var getChatMasterLogInfo = function(req, res, objDataParams, callback) {
    var userId = objDataParams.userId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var params = [meetingUrlKey];
    var sqlQuery = '    SELECT mcm.meeting_chat_master_id meetingChatMasterId, mcm.meeting_detail_id meetingDetailId, mi.invitation_id invitationId ' + 
                    '   FROM meeting_chat_master mcm, meeting_details md, meeting_invitations mi ' + 
                    '   WHERE mcm.meeting_detail_id = md.meeting_id ' + 
                    '   AND md.invitation_id = mi.invitation_id ' + 
                    '   AND md.meeting_url_key = ? ';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if(results.length > 0) {
                callback(req, res, results[0]);
            }
            else {
                callback(req, res, {});
            }
        }
    });
};
exports.getChatMasterLogInfo = getChatMasterLogInfo;


var saveChatMessageToDB = function(req, res, objDataParams, callback) {
    var message = objDataParams.message;
    var dateTimeStamp = objDataParams.dateTimeStamp;
    var chatMasterId = objDataParams.chatMasterId;
    var meetingDetailId = objDataParams.meetingDetailId;
    var meetingUrlKey = objDataParams.meetingUrlKey;

    var currentUTC = constants.CURR_UTC_DATETIME();


    var objDataParamsForUserTypeCheck = {
        userId: req.session.userId,
        meetingUrlKey: meetingUrlKey
    };
    checkUserTypeForThisPerticularMeeting(req, res, objDataParamsForUserTypeCheck, function(req, res, objReturnData) {

        var loggedInUserTypeForLoggedInMeeting = objReturnData.loggedInUserTypeForLoggedInMeeting;


        var insertParams = {
            meeting_chat_master_id: chatMasterId, 
            sender_id: req.session.userId, 
            sender_type: loggedInUserTypeForLoggedInMeeting,
            message: message,
            send_time: dateTimeStamp,
            created_date: currentUTC
        };

        dbconnect.executeQuery(req, res, 'INSERT INTO meeting_chat_records SET ?', insertParams, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var meetingChatRecordId = results.insertId;

            var dataToPass = {
                meetingChatRecordId: meetingChatRecordId
            };

            callback(req, res, dataToPass);
        });
    });
};
exports.saveChatMessageToDB = saveChatMessageToDB;

var getChatConversations = function(req, res, objDataParams, callback) {
    var meetingChatMasterId = objDataParams.meetingChatMasterId;
    var loggedInUserId = req.session.userId;

    var params = [loggedInUserId, meetingChatMasterId];
    var sqlQuery = '    SELECT mcr.meeting_chat_record_id meetingChatRecordId, mcr.meeting_chat_master_id meetingCharMasterId, mcr.sender_id senderId, mcr.sender_type senderType, mcr.message message, mcr.send_time chatMessageTime, ud.first_name firstName, ud.last_name lastName, ud.profile_image profileImage, IF(mcr.sender_id = ?, 1, 0) flagMyMessage, ' + 
                    '   DATE_FORMAT(CONVERT_TZ(mcr.send_time, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME + '") chatMessageDisplayTime ' + 
                    '   FROM meeting_chat_records mcr, user_details ud ' + 
                    '   WHERE mcr.sender_id = ud.user_id ' + 
                    '   AND mcr.meeting_chat_master_id = ? ' + 
                    '   ORDER BY mcr.send_time ASC ';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if(results.length > 0) {
                mentorModel.setImagePathOfAllUsers(req, res, results, 0, [], function(req, res, objData) {
                    callback(req, res, objData);
                });
            }
            else {
                callback(req, res, {});
            }
        }
    });

};
exports.getChatConversations = getChatConversations;