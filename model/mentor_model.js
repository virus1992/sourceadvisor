var exports = module.exports = {};
var dateFormat = require('dateformat');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var fs = require('fs');
var async = require("async");
var handlebars = require('handlebars');

var OpenTok = require('opentok');

var miscFunction = require('./../model/misc_model');
var paypalFunction = require('./../model/paypal_model');
var userModel = require('./../model/users_model');
var messagesModel = require('./../model/messages_model');
var meetingLogModel = require('./../model/meetinglog_model');
var paymentModel = require('./../model/payment_model');
var frontConstant = require('./../modules/front_constant');
var sendResponse = require('./../modules/sendresponse');

var opentok = new OpenTok(constants.TOKBOX_API_KEY, constants.TOKBOX_SECRET_KEY);

var getMeetingPendingInvites_model = function (req, res, invitationId, status, callback) {

    async.parallel([
        function (meetingsDataCallback) {

            var extraParam = {
                meeting_type: "received"
            };
            getMeetingPendingInvitesDetail_model(req, res, invitationId, status, extraParam, function (req, res, arrData) {
                meetingsDataCallback(null, arrData);
            });
        },
        function (meetingsDataCallback) {

            var extraParam = {
                meeting_type: "sent"
            };
            getMeetingPendingInvitesDetail_model(req, res, invitationId, status, extraParam, function (req, res, arrData) {
                meetingsDataCallback(null, arrData);
            });
        }
    ], function (err, results) {
        receivedData = results[0];
        sentData = results[1];

        var meetingDataRes = {
            receivedData: receivedData,
            sentData: sentData
        };

        callback(req, res, meetingDataRes);
    });
};
exports.getMeetingPendingInvites_model = getMeetingPendingInvites_model;

var inviteDataPaging = function (req, res, callback) {

    var page = req.body.page;
    var datatype = req.body.datatype;

    var extraParam = {
        meeting_type: datatype,
        page: page
    };

    getMeetingPendingInvitesDetail_model(req, res, '', 'pending', extraParam, function (req, res, arrData) {
        if (datatype == "sent") {
            var inviteDataRes = {
                receivedData: [],
                sentData: arrData
            };
        } else {
            var inviteDataRes = {
                receivedData: arrData,
                sentData: []
            };
        }
        callback(req, res, inviteDataRes);
    });
};
exports.inviteDataPaging = inviteDataPaging;

var getMeetingPendingInvitesDetail_model = function (req, res, invitationId, status, extraParam, callback) {

    var whereCond = '';
    var cond = '';
    var join = '';
    var payment = '';
    var page = 0;
    var queryParam = [status, req.session.userId];
    var pageLimit = frontConstant.INVITEPERPAGE;

    if (typeof extraParam.page != 'undefined' && !isNaN(extraParam.page) && extraParam.page > 0) {
        page = extraParam.page;
    }


    if (invitationId != '') {
        whereCond = ' AND mi.invitation_id = ' + invitationId;
        join = ' LEFT JOIN user_details ud ON ud.user_id = mi.user_id ';
    } else {

        if (extraParam.meeting_type == "sent") {
            queryParam = [status, req.session.userId];
            cond = ' AND mi.requester_id=? ';
            join = ' LEFT JOIN user_details ud ON ud.user_id = mi.user_id ';
            payment = " AND (mi.flag_payment_done = '1' AND mi.invitation_type = '0' OR mi.flag_payment_done = '0' AND mi.invitation_type = '1' )";
        } else {
            queryParam = [status, req.session.userId, req.session.userId];
            cond = ' AND mi.user_id=?';
            join = ' LEFT JOIN user_details ud ON ud.user_id = mi.requester_id ';
            payment = " AND (mi.flag_payment_done = '1' AND mi.invitation_type = '0' OR mi.flag_payment_done = '0' AND mi.invitation_type = '1' )";
        }
    }
    var strQuery = "SELECT  mi.invitation_type,mi.requester_id, mi.user_id, " +
            " mi.invitation_type,ud.profile_image profileImage, ud.first_name, ud.last_name, " +
            " DATE_FORMAT(mi.created_date, '" + constants.MYSQL_DB_DATETIME_FORMAT + "') created_date, " +
            " mi.invitation_id, mi.meeting_type, mi.meeting_purpose, mi.meeting_purpose_indetail, mi.duration, " +
            " mi.document ,us.slug,usm.slug mslug,ins.invoice_id,md.user_id as mentorId,pp.transaction_id,pp.amount " +
            " FROM meeting_invitations mi " +
            " LEFT JOIN users us ON us.user_id = mi.requester_id  " +
            " LEFT JOIN users usm ON usm.user_id = mi.user_id  " +
            " LEFT JOIN mentor_details md ON md.user_id = mi.requester_id " +
            " LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " + join +
            " LEFT JOIN payments_paypal pp ON ins.invoice_id = pp.invoice_id " +
            " WHERE mi.status=? " + whereCond + cond + payment +
            "  ORDER BY mi.invitation_id DESC";

    strQuery += ' LIMIT ' + parseInt(page) * parseInt(pageLimit) + ' ,' + parseInt(pageLimit);

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {

        if (error) {
            //throw error;
            console.log(error);
            callback(req, res, error);
        }
        /* getMentorPendingInvitationsData(req, res, results, 0, [], function (req, res, arrMentorData) {
         setImagePathOfAllUsers(req, res, arrMentorData, 0, [], callback, '');
         });*/
        getMentorPendingInvitationsData(req, res, results, 0, [], function (req, res, arrMentorData) {
            setImagePathOfAllUsers(req, res, arrMentorData, 0, [], function (req, res, MeetingData) {
                callback(req, res, MeetingData);
            });
        });

    });
};
exports.getMeetingPendingInvitesDetail_model = getMeetingPendingInvitesDetail_model;

var getMentorPendingInvitationsData = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];

        async.parallel([
            function (invitationCallback) {
                var document = thisObj.document;
                if (document != '') {
                    var queryParam = [];
                    var sqlQuery = ' SELECT document_id, document_name, document_real_name, document_size, document_path FROM documents ' +
                            ' WHERE document_id IN (' + document + ') ' +
                            ' AND is_deleted=0';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            invitationCallback(null, []);
                        } else {
                            miscFunction.checkDocumentExistOnPath(req, res, results, 0, [], function (req, res, finalDocument) {
                                invitationCallback(null, finalDocument);
                            });
                        }
                    });
                } else {
                    invitationCallback(null, []);
                }
            },
            function (invitationCallback) {
                var invitation_id = thisObj.invitation_id;
                var queryParam = [invitation_id];
                var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                        ' FROM meeting_invitation_slot_mapping mis ' +
                        ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                        ' WHERE mis.invitation_id= ?';
                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        invitationCallback(null, []);
                    } else {
                        invitationCallback(null, results);
                    }
                });
            }
        ], function (err, results) {
            thisObj.invitation_documents = results[0];
            thisObj.invitation_slots = results[1];

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            getMentorPendingInvitationsData(req, res, resultSet, currentIndex, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.getMentorPendingInvitationsData = getMentorPendingInvitationsData;


var createOpenTokTokenAndUpdate = function (req, res, objDataParams, callback) {

    var invitation_data = objDataParams.invitation_data;
    var invitation_slot_data = objDataParams.invitation_slot_data;
    var amount = objDataParams.amount;
    var private_invite = "";
    var mentorId = "";

    if (typeof objDataParams.private_invite != "undefined" && objDataParams.private_invite != "") {
        private_invite = objDataParams.private_invite;
    }
    if (typeof objDataParams.mentorId != "undefined" && objDataParams.mentorId != "") {
        mentorId = objDataParams.mentorId;
    }


    var meeting_key = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 12; i++) {
        meeting_key += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    opentok.createSession(function (err, session) {
        if (err) {
            throw err;
        }
        var data = {
            meeting_key: meeting_key,
            sessionId: session.sessionId,
            invitation_data: invitation_data,
            invitation_slot_data: invitation_slot_data[0]
        };


        insertSessionForMeeting(req, res, data, function (req, res, response) {
            if (response == 'error') {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
            } else if ('success') {
                var invitationData = {
                    invitation_id: data.invitation_data[0].invitation_id,
                    invitation_slot_id: data.invitation_slot_data[2],
                    duration: data.invitation_data[0].duration,
                    meeting_key: meeting_key,
                    amount: amount
                };


                if (private_invite != "1") {
                    userModel.getUserInformation(req, res, data.invitation_data[0].user_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        userModel.getUserInformation(req, res, data.invitation_data[0].requester_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendAcceptedInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                            callback(req, res, true);
                        });
                    });
                } else {
                    userModel.getUserInformation(req, res, data.invitation_data[0].requester_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        userModel.getUserInformation(req, res, data.invitation_data[0].user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendAcceptedPrivateInvitationMail(req, res, arrMentorData, arrUserData, invitationData);
                            callback(req, res, true);
                        });
                    });
                }
            }
        });
    });

};
exports.createOpenTokTokenAndUpdate = createOpenTokTokenAndUpdate;

var acceptInvitation = function (req, res, invitation_id, invitation_slot_id, transactionId, amount, invoiceid, mentorId, inviteType, callback) {

    var reason = "";

    if (inviteType == 0) {
        reason = "NORMEETCAP";
    } else {
        reason = "PRIMEETCAP";
    }

    var dataParam = {
        amount: amount,
        transactionId: transactionId,
        invitation_id: invitation_id,
        invoiceid: invoiceid,
        user_id: mentorId,
        paymentAction: "caputre",
        reason: reason,
        userType: "user"
    };

    paypalFunction.paypalCapturePayment(req, res, dataParam, function (req, res, payment) {
        if (payment == 'error') {
            callback(req, res, "error", "error");
        } else {

            async.parallel([
                function (acceptCallback) {

                    var data = {
                        updated_date: constants.CURR_UTC_DATETIME(),
                        status: 'accepted'
                    };
                    var queryParam = [data, invitation_id];
                    var sqlQuery = ' UPDATE meeting_invitations SET ? WHERE invitation_id=? ';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            acceptCallback(null, 'error');
                        } else {
                            var queryParam = [invitation_id];
                            var sqlQuery = ' SELECT * , (SELECT email FROM users WHERE user_id=mi.requester_id) AS user_email, ' +
                                    ' (SELECT email FROM users WHERE user_id=mi.user_id) AS mentor_email ' +
                                    ' FROM meeting_invitations mi ' +
                                    ' WHERE mi.invitation_id=? ';
                            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    acceptCallback(null, 'error');
                                } else {
                                    acceptCallback(null, results);
                                }
                            });
                        }
                    });
                },
                function (acceptCallback) {

                    var queryParam = [invitation_id];

                    var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="declined" WHERE invitation_id=? ';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            acceptCallback(null, 'error');
                        } else {
                            var queryParam = [invitation_id, invitation_slot_id];

                            var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="accepted" WHERE invitation_id=? AND id=?';
                            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    acceptCallback(null, 'error');
                                } else {
                                    var queryParam = [invitation_id, invitation_slot_id];
                                    var sqlQuery = ' SELECT *, CONCAT(DATE_FORMAT(date, "%Y-%m-%d")," ",DATE_FORMAT(time, "%H:%i:%s")) AS slot_datetime FROM meeting_invitation_slot_mapping WHERE invitation_id=? AND id=? AND status="accepted"';
                                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                                        if (error) {
                                            acceptCallback(null, 'error');
                                        } else {
                                            acceptCallback(null, results, invitation_id, invitation_slot_id);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            ], function (err, results) {
                invitation_data = results[0];
                invitation_slot_data = results[1];
                callback(req, res, invitation_data, invitation_slot_data);
            });
        }
    });
};
exports.acceptInvitation = acceptInvitation;

var chkAcceptedSlots = function (req, res, invitation_id, invitation_slot_id, mentorId, callback) {

    var queryParam = [invitation_id, invitation_slot_id];
    var sqlQuery = ' Select mi.duration,mis.date,mis.time from meeting_invitations mi ' +
            ' LEFT JOIN meeting_invitation_slot_mapping mis ON mi.invitation_id = mis.invitation_id' +
            ' WHERE mi.invitation_id=? AND mis.id = ?';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, invitation_id, invitation_slot_id, "error");
        } else {

            var resultRecord = results[0];
            var mentorTimezone = req.session.timezone;
            var startDate = resultRecord.date + " " + resultRecord.time;
            var objConvertedDateTime = moment.tz(startDate, 'UTC').tz(mentorTimezone).format('YYYY-MM-DD HH:mm:ss');
            var convertedDateTime = moment.tz(startDate, 'UTC').tz(mentorTimezone).format('YYYY-MM-DD HH:mm:ss');

            var convertedStartTime = moment(convertedDateTime).format('HH:mm:ss');
            var convertedDate = moment(convertedDateTime).format('YYYY-MM-DD');

            var endTimeStamp = moment(convertedDateTime).add(resultRecord.duration, "minutes").format('YYYY-MM-DD HH:mm:ss');
            var convertedEndTime = moment(endTimeStamp).format('HH:mm:ss');

            var dataToSend = {
                slot: convertedDate,
                startTime: convertedStartTime,
                endTime: convertedEndTime,
                mentorTimezone: req.session.timezone,
                slotcount: 1,
                mentorId: mentorId
            };
            checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                callback(req, res, invitation_id, invitation_slot_id, arrSlotData.msg);
            });

        }
    });
};
exports.chkAcceptedSlots = chkAcceptedSlots;

var declineInvitation = function (req, res, callback) {
    if (typeof req.body.invitation_id == "undefined" || req.body.invitation_id == "") {
        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        return false;
    }

    async.parallel([
        function (declineCallback) {
            var invitation_id = req.body.invitation_id;
            var data = {
                updated_date: constants.CURR_UTC_DATETIME(),
                status: 'declined'
            };
            var queryParam = [data, invitation_id];
            var sqlQuery = ' UPDATE meeting_invitations SET ? WHERE invitation_id = ? ';
            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    declineCallback(null, 'error');
                } else {
                    declineCallback(null, 'success');
                }
            });
        },
        function (declineCallback) {
            var invitation_id = req.body.invitation_id;
            var queryParam = [invitation_id];
            var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="declined" WHERE invitation_id=? ';
            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    declineCallback(null, 'error');
                } else {
                    declineCallback(null, 'success');
                }
            });
        }
    ], function (err, results) {
        invitation_update = results[0];
        invitation_slot_update = results[1];
        callback(req, res, invitation_update, invitation_slot_update);
    });
};
exports.declineInvitation = declineInvitation;

var getMeetingInformation = function (req, res, meeting_key, callback) {
    var queryParam = [meeting_key];
    var strQuery = " SELECT mts.meeting_id, mts.session_id, med.meeting_id meetingDetailId, med.meeting_url_key, med.status, med.scheduled_start_time, med.scheduled_end_time, med.start_time, med.end_time, med.elapsed_time, mi.duration " +
            " FROM meeting_tokbox_sessions mts " +
            " LEFT JOIN meeting_details med ON med.meeting_id=mts.meeting_id " +
            " LEFT JOIN meeting_invitations mi ON mi.invitation_id=mts.invitation_id " +
            " WHERE med.meeting_url_key=?";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        callback(req, res, results[0]);
    });
};
exports.getMeetingInformation = getMeetingInformation;

var startMeetingSession_model = function (req, res, callback) {
    var data = {
        start_time: constants.CURR_UTC_DATETIME(),
        updated_date: constants.CURR_UTC_DATETIME(),
        status: 'started'
    };
    var queryParam = [data, req.body.meeting_id];
    var strQuery = " UPDATE meeting_details SET ? WHERE meeting_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }
        sendResponse.sendJsonResponse(req, res, 200, {}, {}, 'SUCCESS');
    });
};
exports.startMeetingSession_model = startMeetingSession_model;

var endMeetingSession_model = function (req, res, callback) {
    var data = {
        end_time: constants.CURR_UTC_DATETIME(),
        updated_date: constants.CURR_UTC_DATETIME(),
        status: 'ended',
        elapsed_time: req.body.elapsed_time
    };
    var queryParam = [data, req.body.meeting_id];
    var strQuery = " UPDATE meeting_details SET ? WHERE meeting_id = ? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }

        var queryParamMeetingInvite = [req.body.meeting_id];
        var strQueryMeetingInvite = " UPDATE meeting_invitations mi, meeting_details md SET mi.status = 'completed', mi.flag_cron_no_show_check = 'Y', mi.flag_cron_meeting_end_check = 'Y' WHERE mi.invitation_id = md.invitation_id AND md.meeting_id = ? ";

        dbconnect.executeQuery(req, res, strQueryMeetingInvite, queryParamMeetingInvite, function (req, res, error, results) {
            if (error) {
                sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
            }

            var meetingUrlKey = req.body.meeting_key;
            var participantMeetingLogMasterId = req.body.participantMeetingLogMasterId;
            var participantMeetingLogId = req.body.participantMeetingLogId;
            var disconnect_reason = req.body.disconnect_reason;

            var objParams = {
                meetingUrlKey: meetingUrlKey,
                participantMeetingLogMasterId: participantMeetingLogMasterId,
                participantMeetingLogId: participantMeetingLogId,
                disconnect_reason: disconnect_reason
            };
            meetingLogModel.updateCurrentUserLeavingMeetingWindow(req, res, objParams, function (req, res, objResponse) {
                var dataToSend = {
                    updated: objResponse
                }
                //sendResponse.sendJsonResponse(req, res, 200, dataToSend, '', 'DONE');
                sendResponse.sendJsonResponse(req, res, 200, {}, {}, 'SUCCESS');
            });
        });
    });
};
exports.endMeetingSession_model = endMeetingSession_model;

var updateVideoSessionElapsedTime_model = function (req, res, elapsed_time, meeting_key, callback) {
    var queryParam = [meeting_key];
    var strQuery = " SELECT * FROM meeting_details WHERE meeting_url_key=?";
    var data = '';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            results = results[0];
            if (results.status == 'pending') {
                data = {
                    start_time: constants.CURR_UTC_DATETIME(),
                    status: 'started',
                    updated_date: constants.CURR_UTC_DATETIME()
                };
            } else if (results.status == 'started') {
                data = {
                    elapsed_time: elapsed_time,
                    updated_date: constants.CURR_UTC_DATETIME()
                };
            }
            if (data != '') {
                var queryParam1 = [data, meeting_key];
                var strQuery1 = " UPDATE meeting_details SET ? WHERE meeting_url_key= ?";
                dbconnect.executeQuery(req, res, strQuery1, queryParam1, function (req, res, error, results) {
                    if (error) {
                        throw error;
                    }
                    sendResponse.sendJsonResponse(req, res, 200, {}, {}, 'SUCCESS');
                });
            }
        } else {
            sendResponse.sendJsonResponse(req, res, 200, {}, {}, 'USRNLOGIN');
        }
    });
};
exports.updateVideoSessionElapsedTime_model = updateVideoSessionElapsedTime_model;

var getMeetingUserDetails_model = function (req, res, meeting_key, callback) {
    var queryParam = [meeting_key];
    var strQuery = " SELECT mi.user_id as mentorID,mi.requester_id as userID,ud.user_id,ud.first_name, ud.last_name, ud.profile_image profileImage, md.profession, md.company_name, md.hourly_rate, cy.city_name, c.country_name " +
            " FROM meeting_details med " +
            " LEFT JOIN meeting_tokbox_sessions mts ON mts.meeting_id=med.meeting_id " +
            " LEFT JOIN meeting_invitations mi ON mi.invitation_id=mts.invitation_id " +
            " LEFT JOIN user_details ud ON ud.user_id=mi.user_id " +
            " LEFT JOIN mentor_details md ON md.user_id=mi.user_id " +
            " LEFT JOIN countries c ON c.country_id = ud.country_id " +
            " LEFT JOIN cities cy ON cy.city_id = ud.city_id " +
            " WHERE med.meeting_url_key=? ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        setImagePathOfAllUsers(req, res, results, 0, [], callback, '');
    });
};
exports.getMeetingUserDetails_model = getMeetingUserDetails_model;

var checkMeetingSession_model = function (req, res, meetingId, callback) {
    var queryParam = [meetingId];
    meetingData = {};
    var strQuery = " SELECT * FROM meeting_details med WHERE med.meeting_id=? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            if (results[0].status == 'ended') {
                meetingData = {
                    status: results[0].status,
                    end_time: results[0].end_time, elapsed_time: results[0].elapsed_time
                };
                callback(req, res, meetingData);
            } else {
                callback(req, res, 0);
            }
        } else {
            callback(req, res, 0);
        }
    });
};
exports.checkMeetingSession_model = checkMeetingSession_model;

var checkSessionForMeeting = function (req, res, meetingId, meetingKey, callback) {
    var queryParam = '';
    var whrCond = '';
    if (meetingId != "") {
        queryParam = [meetingId];
        whrCond = ' WHERE mts.meeting_id = ?';
    } else if (meetingKey != "") {
        queryParam = [meetingKey];
        whrCond = ' WHERE med.meeting_url_key = ?';
    } else {
        console.log('ERROR: Returned FALSE')
        return false;
    }

    var strQuery = " SELECT mts.session_id, med.meeting_url_key, TIMESTAMPDIFF(MINUTE, NOW(), med.scheduled_start_time) AS time_remaining " +
            " FROM meeting_details med " +
            " LEFT JOIN meeting_tokbox_sessions mts ON mts.meeting_id=med.meeting_id " + whrCond +
            " /*AND med.status NOT IN ('ended','missed')*/ ";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            meetingSessionData = {
                session_id: results[0].session_id,
                meeting_url_key: results[0].meeting_url_key,
                time_remaining: results[0].time_remaining
            };
            callback(req, res, meetingSessionData);
        } else {
            callback(req, res, 0);
        }
    });
};
exports.checkSessionForMeeting = checkSessionForMeeting;

var checkUserAuthenticationForMeeting = function (req, res, objDataParam, callback) {

    var userId = objDataParam.userId;
    var meetingUrlKey = objDataParam.meetingUrlKey;

    var queryParam = [userId, userId, meetingUrlKey];
    var strQuery = ' SELECT COUNT(*) totalCnt ' +
            ' FROM meeting_invitations mi, meeting_details md ' +
            ' WHERE (mi.requester_id = ? OR mi.user_id = ?) ' +
            ' AND mi.status = "accepted" ' +
            ' AND md.invitation_id = mi.invitation_id ' +
            ' AND md.meeting_url_key = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
            callback(req, res, false);
        } else {
            var flagUserAuthForMeeting = false;
            var objRslt = results[0];
            if (objRslt.totalCnt > 0) {
                flagUserAuthForMeeting = true;
            }
            callback(req, res, flagUserAuthForMeeting);
        }
    });
}
exports.checkUserAuthenticationForMeeting = checkUserAuthenticationForMeeting;

var insertSessionForMeeting = function (req, res, data, callback) {

    var params = {
        invitation_id: data.invitation_data[0].invitation_id,
        meeting_url_key: data.meeting_key,
        scheduled_start_time: data.invitation_slot_data[0].slot_datetime,
        scheduled_end_time: moment(data.invitation_slot_data[0].slot_datetime, 'YYYY-MM-DD H:m:s').add(data.invitation_data[0].duration, 'minutes').format('YYYY-MM-DD HH:mm:ss'),
        elapsed_time: 0,
        status: 'pending',
        created_date: constants.CURR_UTC_DATETIME()
    };
    var strQuery = 'INSERT INTO meeting_details SET ? ';
    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        var queryParam = {
            invitation_id: data.invitation_data[0].invitation_id, meeting_id: results.insertId,
            session_id: data.sessionId,
            created_date: constants.CURR_UTC_DATETIME()
        };
        var strQuery = 'INSERT INTO meeting_tokbox_sessions SET ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                callback(req, res, 'error');
            } else {
                callback(req, res, 'success');
            }
        });
    });
};
exports.insertSessionForMeeting = insertSessionForMeeting;

var update_become_a_mentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var bcm_mentor = reqBody.bcm_mentor;
    req.checkBody('bcm_mentor', 'Please select one of the options').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/mentor/become_a_mentor');
    } else {

        var currentUTC = constants.CURR_UTC_DATETIME();
        // creating db connection

        var queryParam = [bcm_mentor, currentUTC, req.session.userId];
        var strQuery = ' UPDATE users ' +
                ' SET temp_user_type = ? , updated_date = ? ' + ' WHERE user_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            if (bcm_mentor != "mentor") {
                var queryParam = [req.session.userId];
                var strQuery = ' SELECT COUNT(*) AS totalCnt FROM mentor_details WHERE user_id = ?';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }
                    if (results[0].totalCnt > 0) {
                        var queryParam = [reqBody.organisation_name, req.session.userId];
                        var strQuery = ' UPDATE mentor_details ' +
                                ' SET organization_id = ? ' +
                                ' WHERE user_id = ? ';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                //callback(req, res, false);
                                throw error;
                            }
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "data": {},
                                "error": "0",
                                "message": "success"
                            }));
                            res.end();
                        });
                    } else {
                        var queryParam = [req.session.userId, reqBody.organisation_name];
                        var strQuery = ' INSERT INTO mentor_details ' +
                                ' (user_id, organization_id) ' +
                                ' VALUES ( ?, ? )';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            }

                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "data": {},
                                "error": "0",
                                "message": "success"
                            }));
                            res.end();
                        });
                    }
                });
            } else {
                //req.flash('messages', "Please continue with Step 1");
                callback(req, res);
            }
        });
    }
};
exports.update_become_a_mentor = update_become_a_mentor;

var update_become_mentor_step_1 = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var hourly_rate = reqBody.hourly_rate;
    var currency = reqBody.currency;
    var cancellation_policy_type = reqBody.cancellation_policy_type;
    var slug = reqBody.slug;
    req.checkBody('hourly_rate', 'Please enter Hourly Rate').notEmpty();
    req.checkBody('currency', 'Please Select Currency').notEmpty();
    req.checkBody('cancellation_policy_type', 'Please enter Cancellation Policy Type').notEmpty();
    req.checkBody('slug', 'Please enter slug').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/mentor/step1');
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [slug, req.session.userId];
        var strQuery = ' UPDATE users ' + ' SET slug = ? ' +
                ' WHERE user_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
        });
        var queryParam = [req.session.userId];
        var strQuery = ' SELECT COUNT(*) AS totalCnt FROM mentor_details WHERE user_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            if (results[0].totalCnt > 0) {
                var queryParam = [hourly_rate, currency, cancellation_policy_type, req.session.userId];
                var strQuery = ' UPDATE mentor_details ' +
                        ' SET hourly_rate = ?, ' +
                        ' currency = ?, ' +
                        ' cancellation_policy_type = ? ' +
                        ' WHERE user_id = ? ';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }
                    req.flash('messages', "Step 1 Completed Successfully.. Please continue with Step 2");
                    callback(req, res);
                });
            } else {
                var queryParam = [hourly_rate, currency, cancellation_policy_type, req.session.userId];
                var strQuery = ' INSERT INTO mentor_details ' +
                        ' (hourly_rate, currency, cancellation_policy_type, user_id) ' +
                        ' VALUES ( ?, ?, ?, ? )';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }
                    req.flash('messages', "Step 1 Completed Successfully.. Please continue with Step 2");
                    callback(req, res);
                });
            }
        });
    }
};
exports.update_become_mentor_step_1 = update_become_mentor_step_1;

var update_become_mentor_step_2 = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var profession = reqBody.profession;
    var job_description = reqBody.job_description;
    var employment_type = reqBody.employment_type;
    var company_name = reqBody.company_name;
    var punchline = reqBody.punchline;
    var overview = reqBody.overview;
    //    req.checkBody('profession', 'Please enter Role/Designation').notEmpty();
    //    req.checkBody('job_description', 'Please enter Job Description').notEmpty();
    //    req.checkBody('employment_type', 'Please select Employment Type').notEmpty();
    //    req.checkBody('company_name', 'Please enter Company Name').notEmpty();
    //    req.checkBody('punchline', 'Please enter Punchline').notEmpty();
    //    req.checkBody('overview', 'Please enter Overview').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/mentor/step2');
    } else {
        // var currentUTC = constants.CURR_UTC_DATETIME();
        // var queryParam = [currentUTC, req.session.userId];
        // var strQuery = ' UPDATE users SET user_type = temp_user_type, updated_date=? WHERE user_id = ?';

        // dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        //     if (error) {
        //         throw error;
        //     }
        var queryParam = [req.session.userId];
        var strQuery = ' SELECT COUNT(*) AS totalCnt FROM mentor_details WHERE user_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            if (results[0].totalCnt > 0) {
                var queryParam = [profession, job_description, employment_type, company_name, punchline, overview, req.session.userId];
                var strQuery = ' UPDATE mentor_details ' +
                        ' SET profession = ?, ' +
                        ' job_description = ?, ' +
                        ' employment_type = ?, ' +
                        ' company_name = ?, ' +
                        ' punchline = ?, ' +
                        ' overview = ? ' +
                        ' WHERE user_id = ? ';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }
                    req.flash('messages', "Step 2 Completed Successfully.. Please continue with Step 3");
                    callback(req, res);
                });
            } else {
                var queryParam = [profession, job_description, employment_type, company_name, punchline, overview, req.session.userId];
                var strQuery = ' INSERT INTO mentor_details ' +
                        ' (profession, job_description, employment_type, company_name, punchline, overview, user_id) ' +
                        ' VALUES ( ?, ?, ?, ? )';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }
                    req.flash('messages', "Step 2 Completed Successfully.. Please continue with Step 3");
                    callback(req, res);
                });
            }
        });
        //});
    }
};
exports.update_become_mentor_step_2 = update_become_mentor_step_2;

var get_mentor_linkedin_data_model = function (req, res, callback) {
    var queryParam = [req.session.userId];
    var strQuery = ' SELECT * FROM social_login WHERE user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (typeof results[0] != "undefined" && results[0].linkedin_user_data != "") {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results[0].linkedin_user_data,
                "error": "0",
                "message": "success"
            }));
        } else {
            res.write(JSON.stringify({
                "data": {},
                "error": "0",
                "message": "no_data_fround"
            }));
            res.end();
        }
    });
};
exports.get_mentor_linkedin_data_model = get_mentor_linkedin_data_model;

var save_employment_history_model = function (req, res, callback) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var params = {
        user_id: req.session.userId,
        role: reqBody.emp_his_role,
        job_function: reqBody.emp_his_job_func, employment_status: reqBody.emp_his_self_emp,
        company_name: (typeof reqBody.emp_his_comp_name != "undefined") ? reqBody.emp_his_comp_name : '',
        duration_from: moment(reqBody.emp_his_dur_from, "MM/YYYY").format(constants.JS_DB_SHORT_DATE + '-01'),
        duration_to: moment(reqBody.emp_his_dur_to, "MM/YYYY").format(constants.JS_DB_SHORT_DATE + '-30'),
        is_current: (reqBody.emp_his_dur_present == "1" ? "1" : "0"),
        job_description: reqBody.emp_his_job_desc,
        updated_date: currentUTC
    };
    if (reqBody.employment_id == 0) {
        params.created_date = currentUTC;
        var queryParam = params;
        var strQuery = ' INSERT INTO mentor_employment_history SET ?';
    } else if (reqBody.employment_id > 0) {
        var queryParam = [params, reqBody.employment_id]
        var strQuery = ' UPDATE mentor_employment_history SET ? WHERE mentor_employment_history_id = ?';
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (reqBody.employment_id == 0) {
            var queryParam = [results.insertId];
            var strQuery = ' SELECT *,DATE_FORMAT(duration_from, "%m/%Y") AS durationFrom, DATE_FORMAT(duration_to, "%m/%Y") AS durationTo FROM mentor_employment_history WHERE mentor_employment_history_id = ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results[0],
                    "error": "0",
                    "message": "add_success"
                }));
                res.end();
            });
        } else if (reqBody.employment_id > 0) {
            var queryParam = [reqBody.employment_id];
            var strQuery = ' SELECT *,DATE_FORMAT(duration_from, "%m/%Y") AS durationFrom, DATE_FORMAT(duration_to, "%m/%Y") AS durationTo FROM mentor_employment_history WHERE mentor_employment_history_id = ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results[0],
                    "error": "0",
                    "message": "update_success"
                }));
                res.end();
            });
        }
    });
};
exports.save_employment_history_model = save_employment_history_model;

var save_mentor_expertise_model = function (req, res, callback) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var params = {
        user_id: req.session.userId,
        industry: reqBody.expertise_industry,
        sub_industry: reqBody.expertise_sub_industry,
        domain: reqBody.expertise_domain,
        region: reqBody.expertise_region,
        sub_region: reqBody.expertise_sub_region,
        updated_date: currentUTC
    };
    if (reqBody.expertise_id == 0) {
        params.created_date = currentUTC;
        var queryParam = params;
        var strQuery = ' INSERT INTO mentor_expertise SET ?';
    } else if (reqBody.expertise_id > 0) {
        var queryParam = [params, reqBody.expertise_id]
        var strQuery = ' UPDATE mentor_expertise SET ? WHERE mentor_expertise_id = ?';
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (reqBody.expertise_id == 0) {
            var queryParam = [results.insertId];
            strQuery = 'SELECT me.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                    ' FROM mentor_expertise me ' +
                    ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry' +
                    ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry' +
                    ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain' +
                    ' LEFT JOIN master_region mr ON mr.region_id = me.region' +
                    ' LEFT JOIN countries mc ON mc.country_id = me.sub_region' +
                    ' WHERE me.mentor_expertise_id = ?' +
                    ' ORDER BY me.mentor_expertise_id DESC';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results[0],
                    "error": "0",
                    "message": "add_success"
                }));
                res.end();
            });
        } else if (reqBody.expertise_id > 0) {
            var queryParam = [reqBody.expertise_id];
            strQuery = 'SELECT me.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                    ' FROM mentor_expertise me ' +
                    ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry' +
                    ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry' +
                    ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain' +
                    ' LEFT JOIN master_region mr ON mr.region_id = me.region' +
                    ' LEFT JOIN countries mc ON mc.country_id = me.sub_region' +
                    ' WHERE me.mentor_expertise_id = ?' +
                    ' ORDER BY me.mentor_expertise_id DESC';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results[0],
                    "error": "0",
                    "message": "update_success"
                }));
                res.end();
            });
        }
    });
};
exports.save_mentor_expertise_model = save_mentor_expertise_model;

var addKeynoteVideo_model = function (req, res) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var params = {
        user_id: req.session.userId,
        category: '', document_name: '',
        document_type: 'video',
        document_size: '',
        document_path: '',
        document_url: '',
        document_mime: '',
        created_date: currentUTC,
        updated_date: currentUTC
    };
    if (reqBody.keynote_type == 'youtube') {
        params.document_name = reqBody.youtube_embed;
        params.category = 'keynote_youtube';
        params.description = reqBody.keynote_title;
    } else if (reqBody.keynote_type == 'upload') {
        params.document_name = req.file.filename;
        params.document_size = req.file.size;
        params.document_path = req.file.path;
        params.document_mime = req.file.mimetype;
        params.category = 'keynote_upload';
        params.description = reqBody.keynote_title;
    } else if (reqBody.keynote_type == 'record') {
        params.document_name = req.file.filename;
        params.document_size = req.file.size;
        params.document_path = req.file.path;
        params.document_mime = req.file.mimetype;
        params.category = 'keynote_record';
    }
    var queryParam = [params];
    var strQuery = ' INSERT INTO documents SET ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            var queryParam = [results.insertId, req.session.userId];
            var strQuery = ' UPDATE mentor_details SET keynote_id=? WHERE user_id = ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        data: params,
                        message: "success"
                    }));
                    res.end();
                }
            });
        }
    });
};
exports.addKeynoteVideo_model = addKeynoteVideo_model;


var checkSlotForPricateInvite = function (req, res, objParams, callback) {
    var slot = objParams.slot;
    var startTime = objParams.startTime;
    var endTime = objParams.endTime;
    var mentorTimezone = objParams.mentorTimezone;
    var slotcount = objParams.slotcount;
    var mentorId = objParams.mentorId;
    //var privateInvite = objParams.privateInvite;

    if (mentorId == "") {
        mentorId = req.session.userId;
    } else {
        mentorId = mentorId
    }

    if (typeof slot != 'undefined' && slot != "") {

        meetingstarttimeUTC = moment.tz(slot + " " + startTime, mentorTimezone).tz('UTC').format('HH:mm:00');
        meetingendtimeUTC = moment.tz(slot + " " + endTime, mentorTimezone).tz('UTC').format('HH:mm:00');
        callDate11 = moment.tz(slot + " " + startTime, mentorTimezone).tz('UTC').format('YYYY-MM-DD');

        async.parallel([
            function (slotChkCallback) {
                var queryParam = [callDate11, req.session.userId, meetingstarttimeUTC, meetingendtimeUTC, meetingstarttimeUTC];
                var strQuery = "SELECT * FROM meeting_invitation_slot_mapping psm, meeting_invitations mip " +
                        "WHERE  psm.status = 'accepted' " +
                        "AND psm.date = ? " +
                        "AND mip.invitation_id = psm.invitation_id " +
                        "AND mip.status = 'accepted' " +
                        " AND IF(mip.invitation_type = 0 , mip.user_id, mip.requester_id) = ? " +
                        "AND psm.id NOT IN (  " +
                        "SELECT id  " +
                        "FROM meeting_invitation_slot_mapping subpsm  " +
                        "WHERE subpsm.mentor_id = psm.mentor_id " +
                        "AND subpsm.date = psm.date " +
                        "AND (((? <= CONCAT(subpsm.time, ':00')) AND (? < CONCAT(subpsm.time, ':00')))  " +
                        "OR (? >= (SELECT DATE_FORMAT(DATE_ADD(CONCAT('2000-01-01 ', subpsm.time), INTERVAL mi.duration MINUTE), '%H:%i:00') " +
                        "FROM meeting_invitations mi  " +
                        "WHERE subpsm.invitation_id = mi.invitation_id) " +
                        "))) ";

                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    if (results.length > 0) {
                        slotChkCallback(null, results);
                    } else {
                        slotChkCallback(null, {});
                    }
                });
            },
            function (slotChkCallback) {
                var queryParam = [mentorId, callDate11, meetingstarttimeUTC, meetingendtimeUTC, meetingstarttimeUTC];
                var strQuery = "SELECT * FROM meeting_invitation_slot_mapping psm, meeting_invitations mip " +
                        "WHERE psm.mentor_id = ? AND psm.status = 'accepted' AND mip.status = 'accepted' " +
                        "AND psm.date = ? " +
                        "AND mip.invitation_id = psm.invitation_id " +
                        "AND psm.id NOT IN (  " +
                        "SELECT id  " +
                        "FROM meeting_invitation_slot_mapping subpsm  " +
                        "WHERE subpsm.mentor_id = psm.mentor_id " +
                        "AND subpsm.date = psm.date " +
                        "AND (((? <= CONCAT(subpsm.time, ':00')) AND (? < CONCAT(subpsm.time, ':00')))  " +
                        "OR (? >= (SELECT DATE_FORMAT(DATE_ADD(CONCAT('2000-01-01 ', subpsm.time), INTERVAL mi.duration MINUTE), '%H:%i:00') " +
                        "FROM meeting_invitations mi  " +
                        "WHERE subpsm.invitation_id = mi.invitation_id) " +
                        "))) ";

                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    if (results.length > 0) {
                        slotChkCallback(null, results);
                    } else {
                        slotChkCallback(null, {});
                    }
                });
            }
        ], function (error, resultSet) {
            var userChk = resultSet[0];
            var mentorChk = resultSet[1];

            if (mentorChk.length > 0 || userChk.length > 0) {
                callback(req, res, {msg: "already", slotcount: slotcount});
            } else {
                callback(req, res, {msg: "continue"});
            }
        });

    } else {
        callback(req, res, {msg: "continue"});
    }
};
exports.checkSlotForPricateInvite = checkSlotForPricateInvite;

var send_user_private_invite = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var user_id = reqBody.lstUserList;
    var mentor_id = req.session.userId;
    var callDate1 = reqBody.callDate1;
    var callDuration = reqBody.call_duration;
    var privateInviteMsg = reqBody.privateInviteMsg;
    var mentorTimezone = reqBody.zone;
    var timepicker1 = reqBody.privateInviteTimePicker1;
    var timepicker1parts = timepicker1.split(':');
    var meetingstarttime1 = ('0' + timepicker1parts[0]).slice(-2) + ':' + ('0' + timepicker1parts[1]).slice(-2);
    var meetingendtime1 = moment(callDate1 + " " + meetingstarttime1).add(callDuration, "minutes").format('HH:mm');
    /* var timepicker2 = reqBody.privateInviteTimePicker2;                    
     var timepicker2parts = timepicker2.split(':');
     var meetingendtime1 = ('0' + timepicker2parts[0]).slice(-2) + ':' + ('0' + timepicker2parts[1]).slice(-2);
     */

    var callDate2 = reqBody.callDate2;
    var timepicker3 = reqBody.privateInviteTimePicker3;
    var timepicker3parts = timepicker3.split(':');
    var meetingstarttime2 = ('0' + timepicker3parts[0]).slice(-2) + ':' + ('0' + timepicker3parts[1]).slice(-2);
    var meetingendtime2 = moment(callDate2 + " " + meetingstarttime2).add(callDuration, "minutes").format('HH:mm');
    /*var timepicker4 = reqBody.privateInviteTimePicker4;
     var timepicker4parts = timepicker4.split(':');
     var meetingendtime2 = ('0' + timepicker4parts[0]).slice(-2) + ':' + ('0' + timepicker4parts[1]).slice(-2);*/

    var callDate3 = reqBody.callDate3;
    var timepicker5 = reqBody.privateInviteTimePicker5;
    var timepicker5parts = timepicker5.split(':');
    var meetingstarttime3 = ('0' + timepicker5parts[0]).slice(-2) + ':' + ('0' + timepicker5parts[1]).slice(-2);
    var meetingendtime3 = moment(callDate3 + " " + meetingstarttime3).add(callDuration, "minutes").format('HH:mm');
    /*var timepicker6 = reqBody.privateInviteTimePicker6;
     var timepicker6parts = timepicker6.split(':');
     var meetingendtime3 = ('0' + timepicker6parts[0]).slice(-2) + ':' + ('0' + timepicker6parts[1]).slice(-2);*/


    var count = [];
    var mailStartDate1 = "";
    var mailEndDate1 = "";
    var mailStartDate2 = "";
    var mailEndDate2 = "";
    var mailStartDate3 = "";
    var mailEndDate3 = "";
    var already = 0;
    req.checkBody('lstUserList', 'Please Select User').notEmpty();
    req.checkBody('callDate1', 'Please Select Date').notEmpty();
    //req.checkBody('privateInviteTimePicker1', 'Please Select Start Time').notEmpty();
    //req.checkBody('privateInviteTimePicker2', 'Please Select End Time').notEmpty();
    var objDataParamsForMinimunDuration = {
        startDateTime: meetingstarttime1,
        slotCount: 1
    };


    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"
        }));
        res.end();
    } else {

        async.parallel([
            // function call for slot 1 to check minimuin duration check
            function (checkMeetingInvitationSlotMinimumDuration) {
                var objDataParamsForMinimunDuration = {
                    startDateTime: moment.tz(callDate1 + " " + meetingstarttime1 + ":00", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss'),
                    slotCount: 1,
                    privateInvite: 1
                };

                userModel.checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                    checkMeetingInvitationSlotMinimumDuration(null, objResponse);
                });
            },
            // function call for slot 2 to check minimuin duration check
            function (checkMeetingInvitationSlotMinimumDuration) {
                var objDataParamsForMinimunDuration = {
                    startDateTime: moment.tz(callDate2 + " " + meetingstarttime2 + ":00", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss'),
                    slotCount: 2,
                    privateInvite: 1
                };

                userModel.checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                    checkMeetingInvitationSlotMinimumDuration(null, objResponse);
                });
            },
            // function call for slot 3 to check minimuin duration check
            function (checkMeetingInvitationSlotMinimumDuration) {
                var objDataParamsForMinimunDuration = {
                    startDateTime: moment.tz(callDate3 + " " + meetingstarttime3 + ":00", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss'),
                    slotCount: 3,
                    privateInvite: 1
                };

                userModel.checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                    checkMeetingInvitationSlotMinimumDuration(null, objResponse);
                });
            },
            function (callbackForSlot) {
                var dataToSend = {
                    slot: callDate1,
                    startTime: meetingstarttime1,
                    endTime: meetingendtime1,
                    mentorTimezone: mentorTimezone,
                    slotcount: 1
                };

                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                    callbackForSlot(null, arrSlotData);
                });
            },
            function (callbackForSlot) {
                var dataToSend = {
                    slot: callDate2,
                    startTime: meetingstarttime2,
                    endTime: meetingendtime2,
                    mentorTimezone: mentorTimezone,
                    slotcount: 2
                };

                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                    callbackForSlot(null, arrSlotData);
                });
            },
            function (callbackForSlot) {

                var dataToSend = {
                    slot: callDate3,
                    startTime: meetingstarttime3,
                    endTime: meetingendtime3,
                    mentorTimezone: mentorTimezone,
                    slotcount: 3
                };
                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                    callbackForSlot(null, arrSlotData);
                });
            }
        ],
                function (err, results) {
                    console.log(results);
                    var toBeContinue = true;

                    var flagAlreadyError = false;
                    var flagMinimumSlotTimeError = false;

                    var conflictedSlot = [];
                    var minimumSlotTimeError = [];
                    var minimumExpectedSlot = "";

                    results.forEach(function (reachResult) {
                        if (typeof reachResult.msg != 'undefined') {
                            if (reachResult.msg == "already") {
                                toBeContinue = false;
                                flagAlreadyError = true;
                                conflictedSlot.push(reachResult.slotcount);
                            }
                        }

                        if (typeof reachResult.flagSlotAllowed != 'undefined') {
                            if (!reachResult.flagSlotAllowed && reachResult.flagConsider) {

                                var meetingExpectedTimeFormat = moment.tz(reachResult.minimumExpectedTime, "UTC").tz(req.session.timezone).format('YYYY-MM-DD hh:mm a');

                                toBeContinue = false;
                                flagMinimumSlotTimeError = true;
                                minimumSlotTimeError.push(reachResult.slotCount);
                                minimumExpectedSlot = meetingExpectedTimeFormat;
                            }
                        }
                    });

                    if (!toBeContinue) {
                        if (flagMinimumSlotTimeError == true) {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "minimumSlotDiff",
                                "data": minimumSlotTimeError,
                                "minimumExpectedSlot": minimumExpectedSlot
                            }));
                            res.end();
                        } else if (flagAlreadyError == true) {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "already",
                                "data": conflictedSlot
                            }));
                            res.end();
                        }
                    } else {


                        if (callDate1 != "") {

                            meetingstarttimeUTC = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('HH:mm');
                            meetingendtimeUTC = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('HH:mm');
                            callDate11 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                            meetingstarttimeUTC = meetingstarttimeUTC;
                            meetingendtimeUTC = meetingendtimeUTC;

                            var queryParam = {
                                requester_id: req.session.userId,
                                user_id: user_id,
                                invoice_id: 0,
                                meeting_type: 'virtual',
                                meeting_purpose: privateInviteMsg,
                                meeting_purpose_indetail: privateInviteMsg,
                                duration: callDuration,
                                status: 'pending',
                                invitation_type: 1,
                                created_date: constants.CURR_UTC_DATETIME()
                            };


                            var strQuery = 'INSERT INTO meeting_invitations SET ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"
                                    }));
                                    res.end();
                                } else {
                                    var invitation_id = results.insertId;
                                    var queryParam = {
                                        invitation_id: invitation_id,
                                        mentor_id: req.session.userId,
                                        date: callDate11,
                                        time: meetingstarttimeUTC,
                                        status: 'pending',
                                        //created_date: constants.CURR_UTC_DATETIME()
                                    };
                                    var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                        if (error) {
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "error"
                                            }));
                                            res.end();
                                        } else {
                                            var repeat_type = 0;
                                            startDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                            endDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                            var queryParam = {
                                                parent_id: invitation_id,
                                                mentor_id: req.session.userId,
                                                start_date_time: startDate1,
                                                end_date_time: endDate1,
                                                end_type: 1,
                                                title: "Private Invitation",
                                                description: privateInviteMsg,
                                                repeat_type: repeat_type,
                                                repeat_when: "",
                                                type: 2,
                                                created_date: constants.CURR_UTC_DATETIME()
                                            };
                                            var strQuery = 'INSERT INTO mentor_availability SET ?';
                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                if (error) {
                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                    res.write(JSON.stringify({
                                                        "message": "error"
                                                    }));
                                                    res.end();
                                                } else {

                                                    var strQuery2 = 'insert into invoice SET ?';
                                                    // Insert into invoice table
                                                    var queryParam2 = {
                                                        user_id: user_id,
                                                        invitation_id: invitation_id,
                                                        meeting_id: "0",
                                                        status: 'unpaid',
                                                        created_date: constants.CURR_UTC_DATETIME()
                                                    };


                                                    dbconnect.executeQuery(req, res, strQuery2, queryParam2, function (req, res, error, results, fields) {
                                                        if (error) {
                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                            res.write(JSON.stringify({
                                                                "message": "error"
                                                            }));
                                                            res.end();
                                                        } else {

                                                            if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                meetingstarttimeUTC = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('HH:mm');
                                                                meetingendtimeUTC = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('HH:mm');
                                                                callDate22 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                                                                meetingstarttimeUTC = meetingstarttimeUTC;
                                                                meetingendtimeUTC = meetingendtimeUTC;

                                                                var queryParam = {
                                                                    id: "DEFAULT",
                                                                    invitation_id: invitation_id,
                                                                    mentor_id: req.session.userId,
                                                                    date: callDate22,
                                                                    time: meetingstarttimeUTC,
                                                                    status: 'pending',
                                                                    //created_date: constants.CURR_UTC_DATETIME()
                                                                };
                                                                var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                    if (error) {
                                                                        res.writeHead(400, {'Content-Type': 'application/json'});
                                                                        res.write(JSON.stringify({
                                                                            "message": "error"
                                                                        }));
                                                                        res.end();
                                                                    } else {
                                                                        var repeat_type = 0;
                                                                        startDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                        endDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                        var queryParam = {
                                                                            id: "DEFAULT",
                                                                            parent_id: invitation_id,
                                                                            mentor_id: req.session.userId,
                                                                            start_date_time: startDate2,
                                                                            end_date_time: endDate2,
                                                                            end_type: 1,
                                                                            title: "Private Invitation",
                                                                            description: privateInviteMsg,
                                                                            repeat_type: repeat_type,
                                                                            repeat_when: "",
                                                                            type: 2,
                                                                            created_date: constants.CURR_UTC_DATETIME()
                                                                        };
                                                                        var strQuery = 'INSERT INTO mentor_availability SET ?';
                                                                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                            if (error) {
                                                                                res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                res.write(JSON.stringify({
                                                                                    "message": "error"
                                                                                }));
                                                                                res.end();
                                                                            } else {
                                                                                if (typeof callDate3 != "undefined" && callDate3 != "") {

                                                                                    meetingstarttimeUTC = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                    meetingendtimeUTC = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                    callDate33 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                                                                                    meetingstarttimeUTC = meetingstarttimeUTC;
                                                                                    meetingendtimeUTC = meetingendtimeUTC;
                                                                                    var queryParam = {
                                                                                        id: "DEFAULT",
                                                                                        invitation_id: invitation_id,
                                                                                        mentor_id: req.session.userId,
                                                                                        date: callDate33,
                                                                                        time: meetingstarttimeUTC,
                                                                                        status: 'pending',
                                                                                        //created_date: constants.CURR_UTC_DATETIME()
                                                                                    };
                                                                                    var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                                                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                        if (error) {
                                                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                            res.write(JSON.stringify({
                                                                                                "message": "error"
                                                                                            }));
                                                                                            res.end();
                                                                                        } else {
                                                                                            var repeat_type = 0;
                                                                                            var repeat_type = 0;
                                                                                            startDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                            endDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                            var queryParam = {
                                                                                                id: "DEFAULT",
                                                                                                parent_id: invitation_id,
                                                                                                mentor_id: req.session.userId,
                                                                                                start_date_time: startDate3,
                                                                                                end_date_time: endDate3,
                                                                                                end_type: 1,
                                                                                                title: "Private Invitation",
                                                                                                description: privateInviteMsg,
                                                                                                repeat_type: repeat_type,
                                                                                                repeat_when: "",
                                                                                                type: 2,
                                                                                                created_date: constants.CURR_UTC_DATETIME()
                                                                                            };
                                                                                            var strQuery = 'INSERT INTO mentor_availability SET ?';
                                                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                                if (error) {
                                                                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                                    res.write(JSON.stringify({
                                                                                                        "message": "error"
                                                                                                    }));
                                                                                                    res.end();
                                                                                                } else {
                                                                                                    /************ For mail and msg ****************/

                                                                                                    if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                                                        mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                        mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    }


                                                                                                    if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                                        mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                        mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    }

                                                                                                    if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                                                        mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                        mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    }


                                                                                                    privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", callback);
                                                                                                    privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", callback);


                                                                                                    var objParamsCaptureInvitationDetails = {
                                                                                                        invitationId: invitation_id
                                                                                                    };

                                                                                                    userModel.captureMeetingInvitationDetailsForBookiing(req, res, objParamsCaptureInvitationDetails, function (req, res, objDataResponse) {
                                                                                                    });

                                                                                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                                    res.write(JSON.stringify({
                                                                                                        "message": "success"
                                                                                                    }));
                                                                                                    res.end();
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    /************ For mail and msg ****************/

                                                                                    if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                                        mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                        mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                    }


                                                                                    if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                        mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                        mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                    }

                                                                                    if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                                        mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                        mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                    }


                                                                                    privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", callback);
                                                                                    privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", callback);

                                                                                    var objParamsCaptureInvitationDetails = {
                                                                                        invitationId: invitation_id
                                                                                    };

                                                                                    userModel.captureMeetingInvitationDetailsForBookiing(req, res, objParamsCaptureInvitationDetails, function (req, res, objDataResponse) {
                                                                                    });

                                                                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                    res.write(JSON.stringify({
                                                                                        "message": "success"
                                                                                    }));
                                                                                    res.end();
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            } else {
                                                                /************ For mail and msg ****************/

                                                                if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                    mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                    mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                }


                                                                if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                    mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                    mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                }

                                                                if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                    mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                    mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                }
                                                                privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", callback);
                                                                privateInvitationMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", callback);

                                                                var objParamsCaptureInvitationDetails = {
                                                                    invitationId: invitation_id
                                                                };

                                                                userModel.captureMeetingInvitationDetailsForBookiing(req, res, objParamsCaptureInvitationDetails, function (req, res, objDataResponse) {
                                                                });

                                                                res.writeHead(200, {'Content-Type': 'application/json'});
                                                                res.write(JSON.stringify({
                                                                    "message": "success"
                                                                }));
                                                                res.end();
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
    }
};
exports.send_user_private_invite = send_user_private_invite;


var privateInvitationMailToMentor = function (req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, userType, callback) {

    var mentorId = mentor_id;
    var userId = user_id;
    var slotsInMail = [];
    var slotsInMailObj1 = "";
    var slotsInMailObj2 = "";
    var slotsInMailObj3 = "";

    userModel.getUserInformation(req, res, userId, {}, function (req, res, userId, arrUserData, objParamsData) {
        userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
            getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
                var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
                if (mailStartDate1 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate1, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate1, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate1, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate1, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate1, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate1, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));
                    }
                }

                if (mailStartDate2 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate2, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate2, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate2, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate2, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate2, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate2, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));

                    }
                }

                if (mailStartDate3 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate3, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate3, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate3, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate3, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate3, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate3, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));

                    }
                }

                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
                var templete = "";
                var subject = "";
                if (userType == "user") {
                    templete = "mailtemplate/privateinviteuser";
                    subject = "Hi " + arrUserData.firstName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has sent you a private meeting slot";
                    var email = arrUserData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateInvitemessagetouser.handlebars', 'utf8');
                    req.body.lstUserList = arrUserData.user_id;
                } else {
                    templete = "mailtemplate/privateinvitementor";
                    subject = "Hi " + arrMentorData.firstName + ", you have sent a private invite slot to " + userFullName;
                    var email = arrMentorData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateInvitemessagetomentor.handlebars', 'utf8');
                    req.body.lstUserList = arrMentorData.user_id;
                }

                var templateVariable = {
                    templateURL: templete,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slotsInMail: slotsInMail,
                    userType: userType
                };
                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: subject
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttomentor");
                    }
                });
                // Send message to mentor's inbox

                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slotsInMail: slotsInMail

                };
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res, callback);
            });
        });
    });
};
exports.privateInvitationMailToMentor = privateInvitationMailToMentor;

var getUserListApi = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);

    async.parallel([
        // function call to get list of users who have communicated with mentor over messages.
        function (userListCallback) {
            if (req.session.userType == "user") {
                var param = [req.session.userId];
                var strQuery = 'Select ud.user_id,CONCAT_WS(" ",ud.first_name,ud.last_name) as username from users u LEFT JOIN user_details ud ON u.user_id = ud.user_id WHERE u.user_id != ? AND u.user_id != "1"';
            } else {
                var param = [req.session.userId, req.session.userId, req.session.userId, req.session.userId];
                var strQuery = 'Select ud.user_id,CONCAT_WS(" ",ud.first_name,ud.last_name) as username from users u LEFT JOIN user_details ud ON u.user_id = ud.user_id ' +
                        ' LEFT JOIN msg_conversation mc ON u.user_id = IF(mc.message_sender_id = ' + req.session.userId + ', mc.message_receiver_id, mc.message_sender_id) ' +
                        ' WHERE (mc.message_sender_id = ? OR mc.message_receiver_id = ?) AND u.user_id != ? AND u.user_id != "1"';
            }
            dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    console.log(error);
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {
                    userListCallback(null, results);
                }
            });
        },
        // function call to get users list who have tried to conduct a meeting with that mentor.
        function (userListCallback) {
            var queryParam = [req.session.userId];
            var strQuery = '    SELECT ud.user_id user_id, CONCAT_WS(" ",ud.first_name,ud.last_name) AS username ' +
                    '   FROM meeting_invitations mi ' +
                    '	    LEFT JOIN user_details ud ON IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) = ud.user_id ' +
                    '   WHERE IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) = ? ' +
                    '   GROUP BY ud.user_id ';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }

                if (results.length > 0) {
                    userListCallback(null, results);
                } else {
                    userListCallback(null, {});
                }
            });
        }
    ], function (error, resultSet) {
        //res.send(resultSet);
        var objUsersCommunicatedWithMentor = resultSet[0];
        var objUsersThatHaveConductedMeetingWithMentor = resultSet[1];

        var arrUnqUsers = [];
        var arrFinalResponse = [];

        if (objUsersCommunicatedWithMentor.length > 0) {
            objUsersCommunicatedWithMentor.forEach(function (element) {
                // not present in array
                if (arrUnqUsers.indexOf(element.user_id) == -1) {
                    arrFinalResponse.push(element);
                    arrUnqUsers.push(element.user_id);
                }
            });
        }

        if (objUsersThatHaveConductedMeetingWithMentor.length > 0) {
            objUsersThatHaveConductedMeetingWithMentor.forEach(function (element) {
                // not present in array
                if (arrUnqUsers.indexOf(element.user_id) == -1) {
                    arrFinalResponse.push(element);
                    arrUnqUsers.push(element.user_id);
                }
            });
        }

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": arrFinalResponse,
            "message": "success"
        }));
        res.end();

    });
};
exports.getUserListApi = getUserListApi;
var updateOrganizationTag_model = function (req, res, callback) {
    var reqBody = req.body;
    if (reqBody.action == 'add') {
        var queryParam = {
            user_id: req.session.userId, tag_name: reqBody.tagName
        };
        var strQuery = 'INSERT INTO mentor_organisation_tags SET ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "status": "error",
                    "message": "Error Occured. Please Try again."
                }));
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "status": "success",
                    "message": ""
                }));
                res.end();
            }
        });
    } else if (reqBody.action == 'delete') {
        var queryParam = [req.session.userId, reqBody.tagName];
        var strQuery = 'DELETE FROM mentor_organisation_tags WHERE user_id=? AND tag_name=?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "status": "error",
                    "message": "Error Occured. Please Try again."
                }));
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "status": "success",
                    "message": ""
                }));
                res.end();
            }
        });
    }
};
exports.updateOrganizationTag_model = updateOrganizationTag_model;
var getMentorDetails = function (req, res, userId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }

    var reqBody = req.body;
    var queryParam = [userId];
    var strQuery = ' SELECT md.*,u.email mentorMail, u.slug slug, z.zone_name timeZoneName, ud.first_name first_name, ud.last_name last_name, ROUND(md.rating,2) as rating,ROUND(md.rating) as round_rating, md.job_description jobDescription, cp.policy_label policyLabel, cp.policy_description  policyDescription, cp.cancellation_policy_link, d.*, ud.profile_image profileImage,md.is_featured,md.hide_review_rating, ' +
            ' IF(LENGTH(md.job_description) > 18, CONCAT(SUBSTRING(md.job_description,1, 18), "..."), md.job_description) jobDescriptionToDisplay,md.job_description, ' +
            ' IF(LENGTH(md.profession) > 18, CONCAT(SUBSTRING(md.profession,1, 18), "..."), md.profession) professtionToDisplay,md.profession, ' +
            ' IF(LENGTH(md.company_name) > 18, CONCAT(SUBSTRING(md.company_name,1, 18), "..."), md.company_name) companyToDisplay, md.company_name, ' +
            ' c.country_name, cit.city_name ' +
            ' FROM mentor_details md ' +
            ' LEFT JOIN documents d ON md.keynote_id=d.document_id, cancellation_policy cp, users u, user_details ud LEFT JOIN countries c ON ud.country_id = c.country_id LEFT JOIN cities cit ON ud.city_id = cit.city_id LEFT JOIN zone z ON ud.timezone_id = z.zone_id ' +
            ' WHERE md.user_id = ud.user_id AND u.user_id = ud.user_id AND cp.policy_type = md.cancellation_policy_type AND md.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results[0]);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorDetails = getMentorDetails;
// Get mentor details by slug
var getMentorDetailsBySlug = function (req, res, mentorSlug, callback) {
    var queryParam = [mentorSlug];
    var strQuery = '   SELECT  u.user_id as userId,ud.`first_name` as firstName,ud.`last_name` as lastName, u.slug as slug,md.hourly_rate as hourlyRate, cp.policy_label policyLabel, cp.policy_description policyDescription, cp.cancellation_policy_link ' +
            '   FROM users u ' +
            '   LEFT JOIN user_details ud ON u.user_id=ud.user_id ' +
            '   LEFT JOIN mentor_details md ON u.user_id=md.user_id, ' +
            '   cancellation_policy cp' +
            '   WHERE u.slug=? AND md.cancellation_policy_type = cp.policy_type';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getMentorDetailsBySlug = getMentorDetailsBySlug;
var getMentorWorkfolio = function (req, res, userId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }

    var reqBody = req.body;
    var queryParam = [userId];
    var strQuery = ' SELECT mw.media_title, IF(LENGTH(mw.media_title) > 27, CONCAT(SUBSTRING(mw.media_title,1, 25), ".."), mw.media_title) mediaTitleToDisplay ,md.industry_id,md.industry_name,mw.media_type,mw.media_value,mw.media_desc,SUBSTRING_INDEX(mw.media_mime,"/",2) as extension ,SUBSTRING_INDEX(mw.media_mime,"/",1) as media_mime ' +
            ' FROM mentor_workfolio mw ' +
            ' LEFT JOIN master_industry md ON md.industry_id = mw.industry_id ' +
            ' WHERE mw.user_id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            setMediaPathOfAllUsers(req, res, results, 0, [], function (req, res, arrDataWithPath) {
                //setWorkFolioContentTypeForFrontDisplay(req, res, arrDataWithPath, function(req, res, arrFinalData){
                callback(req, res, userId, arrDataWithPath);
                //});
            });
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorWorkfolio = getMentorWorkfolio;
//var setWorkFolioContentTypeForFrontDisplay = function (req, res, arrData, callback) {
//
//}

var getMentorReview = function (req, res, userId, page, callback) {

    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }

    if (typeof page == "undefined") {
        page = 0;
    }

    var reqBody = req.body;
    var queryParam = [userId, userId];
    var strQuery = ' SELECT ur.*,DATE_FORMAT(ur.created_date, "%M %d,%Y") AS reviewdate,DATEDIFF(CURDATE(),STR_TO_DATE(ur.created_date, "%Y-%m-%d")) AS DAYS,CONCAT(ud.first_name," ",ud.last_name) as name,ud.profile_image as profileImage ' +
            ' FROM user_reviews ur ' +
            ' LEFT JOIN user_details ud ON ur.reviewer_id = ud.user_id ' +
            ' WHERE ur.user_id = ? AND ur.userType = "mentor"' +
            ' ORDER BY ur.review_id DESC ';
    var perPageLimit = frontConstant.REVIEWCOUNT;
    var currentPage = page;
    var strLimit = "";
    strLimit = " LIMIT 0, " + perPageLimit;
    if (!isNaN(currentPage) && currentPage > 0) {
        strLimit = " LIMIT " + parseInt(parseInt(perPageLimit) * parseInt(currentPage)) + ", " + perPageLimit;
    }

    strQuery += strLimit;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {

        if (error) {
            throw error;
        }
        if (results.length > 0) {
            setImagePathOfAllUsers(req, res, results, 0, [], function (req, res, arrFinalData) {
                callback(req, res, userId, arrFinalData);
            });
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorReview = getMentorReview;
var getMentorStarCount = function (req, res, userId, expertiseId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }
    var reqBody = req.body;
    var strQuery = '';
    var queryParam = [userId, userId, userId, userId, userId, userId];
    strQuery = 'select (select COUNT(review_rating) from user_reviews where user_id=? and review_rating = 1) as Count1,(select COUNT(review_rating) from user_reviews where user_id=? and review_rating = 2) as Count2,(select COUNT(review_rating) from user_reviews where user_id=? and review_rating = 3) as Count3,(select COUNT(review_rating) from user_reviews where user_id=? and review_rating = 4) as Count4,(select COUNT(review_rating) from user_reviews where user_id=? and review_rating = 5) as Count5, (select COUNT(review_rating) from user_reviews where user_id=?)/100 as total';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorStarCount = getMentorStarCount;
var setMediaPathOfAllUsers = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var profilePicture = thisObj.media_value;
        //var finalProfileImageUrl = constants.ACCESSURL + "images/noimg.png";
        var finalProfileImageUrl = "";
        if (profilePicture != "" && thisObj.media_type == "upload") {
            fs.stat("./uploads/workfolio/" + profilePicture, function (err, stat) {
                if (err == null) {
                    finalProfileImageUrl = profilePicture;
                }

                thisObj.media_value = finalProfileImageUrl;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);
                currentIndex++;
                setMediaPathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callback);
            });
        } else {

            thisObj.finalProfileImageUrl = finalProfileImageUrl;
            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            currentIndex++;
            setMediaPathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callback);
        }
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.setMediaPathOfAllUsers = setMediaPathOfAllUsers;
var deleteMentorExpertise_model = function (req, res, expertiseId) {
    var reqBody = req.body;
    var queryParam = [expertiseId];
    var strQuery = 'DELETE FROM mentor_expertise where mentor_expertise_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            var queryParam = [req.session.userId, reqBody.industry_id];
            var strQuery = 'SELECT * FROM mentor_expertise WHERE user_id= ? AND industry = ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {
                    if (results.length == 0) {
                        var queryParam = [reqBody.industry_id];
                        var strQuery = 'DELETE FROM mentor_workfolio where industry_id = ?';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "success"
                                }));
                                res.end();
                            }
                        });
                    } else {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "success"
                        }));
                        res.end();
                    }
                }
            });
        }
    });
};
exports.deleteMentorExpertise_model = deleteMentorExpertise_model;
var deleteMentorEmpHistory_model = function (req, res, expertiseId) {
    var reqBody = req.body;
    var queryParam = [expertiseId];
    var strQuery = 'DELETE FROM mentor_employment_history where mentor_employment_history_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "success"
            }));
            res.end();
        }
    });
};
exports.deleteMentorEmpHistory_model = deleteMentorEmpHistory_model;
var removeKeynoteVideo_model = function (req, res, expertiseId) {
    var reqBody = req.body;
    var queryParam = [req.session.userId];
    var strQuery = 'SELECT keynote_id FROM mentor_details WHERE user_id=? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            var queryParam = [results[0].keynote_id];
            var strQuery = 'UPDATE documents SET is_deleted = 1 WHERE document_id=? ';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {
                    var queryParam = [req.session.userId];
                    var strQuery = 'UPDATE mentor_details SET keynote_id = 0 WHERE user_id=? ';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"
                            }));
                            res.end();
                        } else {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "success"
                            }));
                            res.end();
                        }
                    });
                }
            });
        }
    });
};
exports.removeKeynoteVideo_model = removeKeynoteVideo_model;
var getMentorExpertise = function (req, res, userId, expertiseId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }
    var reqBody = req.body;
    var strQuery = '';
    if (expertiseId == '') {
        var queryParam = [userId];
        strQuery = 'SELECT me.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_expertise me ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE me.user_id = ? AND mi.status = "1" AND msi.status = "1" AND md.status = "1" AND mr.status = "1" AND mc.status = "1"' +
                ' ORDER BY me.mentor_expertise_id DESC';
    } else {
        var queryParam = [userId, expertiseId];
        strQuery = 'SELECT me.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_expertise me ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry  ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE me.user_id = ? AND me.mentor_expertise_id = ? AND mi.status = "1" AND msi.status = "1" AND md.status = "1" AND mr.status = "1" AND mc.status = "1"' +
                ' ORDER BY me.mentor_expertise_id DESC';
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorExpertise = getMentorExpertise;
var getMentorWorkfolioDetail = function (req, res, userId, industry_id, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }
    var reqBody = req.body;
    var strQuery = '';
    if (typeof industry_id == "undefined" || industry_id == '') {
        var queryParam = [userId];
        strQuery = 'SELECT mw.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_workfolio mw ' +
                ' LEFT JOIN mentor_expertise me ON mw.industry_id = me.industry AND mw.user_id = me.user_id ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE mw.user_id = ? ' +
                ' GROUP BY mw.mentor_workfolio_id ' +
                ' ORDER BY mw.mentor_workfolio_id DESC';
    } else {
        var queryParam = [userId, industry_id];
        strQuery = 'SELECT mw.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_workfolio mw ' +
                ' LEFT JOIN mentor_expertise me ON mw.industry_id = me.industry AND mw.user_id = me.user_id ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE mw.user_id = ? AND mw.industry_id = ? ' +
                ' GROUP BY mw.mentor_workfolio_id ' +
                ' ORDER BY mw.mentor_workfolio_id DESC ';
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorWorkfolioDetail = getMentorWorkfolioDetail;
var getMentorExpertiseWorkfolio = function (req, res, userId, workfolioId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }
    var reqBody = req.body;
    var strQuery = '';
    if (workfolioId == '') {
        var queryParam = [userId];
        strQuery = 'SELECT mw.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_workfolio mw ' +
                ' LEFT JOIN mentor_expertise me ON mw.industry_id = me.industry AND mw.user_id = me.user_id ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE mw.user_id = ? ' +
                ' GROUP BY mw.mentor_workfolio_id ' +
                ' ORDER BY mw.mentor_workfolio_id ASC';
    } else {
        var queryParam = [userId, workfolioId];
        strQuery = 'SELECT mw.*, mi.industry_name, msi.subindustry_name, md.domain_name, mr.region_name, mc.country_name sub_region_name ' +
                ' FROM mentor_workfolio mw ' +
                ' LEFT JOIN mentor_expertise me ON mw.industry_id = me.industry AND mw.user_id = me.user_id ' +
                ' LEFT JOIN master_industry mi ON mi.industry_id = me.industry ' +
                ' LEFT JOIN master_subindustry msi ON msi.subindustry_id = me.sub_industry ' +
                ' LEFT JOIN master_domain md ON md.master_domain_id = me.domain ' +
                ' LEFT JOIN master_region mr ON mr.region_id = me.region ' +
                ' LEFT JOIN countries mc ON mc.country_id = me.sub_region ' +
                ' WHERE mw.user_id = ? AND mw.expertise_id = ? ' +
                ' GROUP BY mw.mentor_workfolio_id ' +
                ' ORDER BY mw.mentor_workfolio_id ASC ';
    }
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorExpertiseWorkfolio = getMentorExpertiseWorkfolio;
var getMentorOrganizationTag = function (req, res, userId, expertiseId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }
    var queryParam = [userId];
    var strQuery = 'SELECT tag_name FROM mentor_organisation_tags WHERE user_id=?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var arr = [];
        for (i = 0; i < results.length; i++) {
            arr.push(results[i].tag_name);
        }

        if (results.length > 0) {
            callback(req, res, userId, expertiseId, arr);
        } else {
            callback(req, res, userId, expertiseId, null);
        }
    });
};
exports.getMentorOrganizationTag = getMentorOrganizationTag;
var getMentorEmploymenthistory = function (req, res, userId, empHisId, callback) {
    if (typeof userId == "undefined") {
        userId = req.session.userId;
    }

    var reqBody = req.body;
    var strQuery = '';
    if (empHisId == '') {
        var queryParam = [userId];
        strQuery = 'SELECT *,DATE_FORMAT(duration_from, "%m/%Y") AS durationFrom, DATE_FORMAT(duration_to, "%m/%Y") AS durationTo FROM mentor_employment_history where user_id = ? ORDER BY duration_from DESC';
    } else {
        var queryParam = [userId, empHisId];
        strQuery = 'SELECT *,DATE_FORMAT(duration_from, "%m/%Y") AS durationFrom, DATE_FORMAT(duration_to, "%m/%Y") AS durationTo FROM mentor_employment_history WHERE user_id = ? AND mentor_employment_history_id = ? ORDER BY duration_from DESC';
    }

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, userId, results);
        } else {
            callback(req, res, userId, {});
        }
    });
};
exports.getMentorEmploymenthistory = getMentorEmploymenthistory;
var gcalSyncStep1Api = function (req, res, callback) {
    var mentor_id = req.session.userId;
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var currentUTC = constants.CURR_UTC_DATETIME();
    var strQuery = 'DELETE FROM mentor_availability where type = 1 AND mentor_id = ?';
    dbconnect.executeQuery(req, res, strQuery, [mentor_id], function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"
            }));
            res.end();
        }
    });
};
exports.gcalSyncStep1Api = gcalSyncStep1Api;
var gcalSyncApi = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var gcal_id = reqBody.id;
    var title = reqBody.title;
    var meetingstarttime = reqBody.start;
    var meetingendtime = reqBody.end;
    var url = reqBody.url;
    var description = reqBody.description;
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"
        }));
        res.end();
    } else {
        var mentor_id = req.session.userId;
        var queryParam = {
            id: "DEFAULT",
            mentor_id: mentor_id,
            google_cal_id: gcal_id,
            start_date_time: meetingstarttime,
            end_date_time: meetingendtime,
            title: title,
            description: description,
            end_type: 1, repeat_type: 0,
            repeat_when: 0,
            url: url,
            type: 1,
            created_date: moment.utc().format()
        };
        var qQuery = 'insert into mentor_availability SET ?';
        dbconnect.executeQuery(req, res, qQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"
                }));
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results,
                    "message": "success"
                }));
                res.end();
            }
        });
    }
};
exports.gcalSyncApi = gcalSyncApi;
var addAvailabilityApi = function (req, res, callback) {
    req.checkBody('startDate', 'Please Select Start Date').notEmpty();
    req.checkBody('timepicker1', 'Please Select Start Time').notEmpty();
    req.checkBody('timepicker2', 'Please Enter End Time').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"
        }));
        res.end();
    } else {
        var reqBody = req.body;
        console.log("req.body", req.body);
        var arrWeek = (typeof reqBody.week != undefined) ? reqBody.week : [];
        var arrWeekUTC = [];
        var availType = reqBody.availType;

        var startDate = reqBody.startDate;
        var endDate = reqBody.endDate;

        var endType = reqBody.endType;

        var timepicker1 = reqBody.timepicker1;
        var timepicker2 = reqBody.timepicker2;

        var mentorTimezone = reqBody.updateTz;

        var currentUTC = constants.CURR_UTC_DATETIME();
        var endDateTime, startDateTime;

        startDateTime = moment.tz(startDate + " " + timepicker1, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
        // Check if never ends selected in case of recurring event
        if (endType == "recurring") {
            endDateTime = moment.tz(endDate + " " + timepicker2, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
            var end_type = 1;
        } else {
            endDateTime = moment.tz(endDate + " " + timepicker2, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').add(5, 'years').format('YYYY-MM-DD HH:mm');
            var end_type = 0;
        }

        if (availType == "recurring") {
            for (i = moment.tz(startDate + " " + timepicker1, "YYYY-MM-DD HH:mm", mentorTimezone); i < moment.tz(endDate + " " + timepicker2, "YYYY-MM-DD HH:mm", mentorTimezone); i.add(1, "day")) {
                j = i.clone();
                if (arrWeek.indexOf(j.format("e")) >= 0) {
                    arrWeekUTC.push(j.tz("UTC").format("e"));
                }
            }

            var hoursDiff = (moment.tz(startDate + " " + timepicker2, "YYYY-MM-DD HH:mm", mentorTimezone) - moment.tz(startDate + " " + timepicker1, "YYYY-MM-DD HH:mm", mentorTimezone)) / (1000 * 60 * 60);

            var bulkQueryParams = [];
            for (i = moment.tz(startDateTime, "YYYY-MM-DD HH:mm", "UTC"), j = 0; i <= moment.tz(endDateTime, "YYYY-MM-DD HH:mm", "UTC"); i.add(1, "day"), j++) {
                k = i.clone();
                if (arrWeekUTC.indexOf(moment(k).format("e")) >= 0) {
                    var queryArr = [
                        0,
                        req.session.userId,
                        end_type,
                        1,
                        JSON.stringify(arrWeekUTC.filter((v, i, a) => a.indexOf(v) === i)),
                        0,
                        currentUTC,
                        currentUTC,
                        moment(startDateTime).format("YYYY-MM-DD HH:mm"),
                        moment(endDateTime).format("YYYY-MM-DD HH:mm"),
                        moment(k).format("YYYY-MM-DD HH:mm"),
                        moment(k).add(hoursDiff, "hours").format("YYYY-MM-DD HH:mm")
                    ];
                    bulkQueryParams.push(queryArr);
                }
            }
            console.log("bulkQueryParams", bulkQueryParams);
            var queryParam = bulkQueryParams[0];

            var strQuery = "INSERT INTO mentor_availability (parent_id,mentor_id,end_type,repeat_type,repeat_when,type,created_date,updated_date,recc_start,recc_end,start_date_time,end_date_time) VALUES "
                    + " (?,?,?,?,?,?,?,?,?,?,?,?) ";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                }
                bulkQueryParams.splice(0, 1);
                if (bulkQueryParams.length > 0) {
                    bulkQueryParams.forEach(element => {
                        element[0] = results.insertId;
                    });
                    var sql = "INSERT INTO mentor_availability (parent_id,mentor_id,end_type,repeat_type,repeat_when,type,created_date,updated_date,recc_start,recc_end,start_date_time,end_date_time) VALUES ? ";
                    dbconnect.executeQuery(req, res, sql, [bulkQueryParams], function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"
                            }));
                            res.end();
                        }
                        if (req.body.parent_id) {
                            var select_params = [req.session.userId, req.body.parent_id];
                            var select_deletedSlotQuery = 'SELECT * FROM deletedSlot WHERE mentor_id = ? AND parent_id = ?';
                            dbconnect.executeQuery(req, res, select_deletedSlotQuery, select_params, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"
                                    }));
                                    res.end();
                                }
                                if (results.length > 0) {
                                    var newDate = new Date(results[0].start_date_time);
                                    var day = newDate.getDate();
                                    newDate = startDate;
                                    var newDate1 = new Date(newDate);
                                    newDate1.setDate(day);
                                    var update_params = [newDate1, req.body.parent_id];
                                    var update_deletedSlotQuery = 'UPDATE deletedSlot set start_date_time = ? WHERE parent_id = ?';
                                    dbconnect.executeQuery(req, res, update_deletedSlotQuery, update_params, function (req, res, error, results, fields) {
                                        if (error) {
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "error"
                                            }));
                                            res.end();
                                        }
                                    });
                                }
                            });
                        } else {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "success"
                            }));
                            res.end();
                        }
                    });
                }
            });
        } else {
            var queryParam = {
                mentor_id: req.session.userId,
                start_date_time: startDateTime,
                end_date_time: endDateTime,
                end_type: 0,
                repeat_type: 0,
                repeat_when: "",
                type: 0,
                created_date: currentUTC,
                updated_date: currentUTC
            };
            var deleteCheckQueryParam = [
                startDateTime,
                endDateTime
            ];
            var deletedData = "SELECT count(*) as total, id FROM `deletedSlot` where `start_date_time` = ? and `end_date_time` = ?";
            dbconnect.executeQuery(req, res, deletedData, deleteCheckQueryParam, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {
                    if (results[0].total >= 1) {
                        // Delete "Deleted Slot for particular event datetime"
                        var deleteQuery = "DELETE FROM `deletedSlot` where id= ?";
                        var deleteParam = [results[0].id];
                        dbconnect.executeQuery(req, res, deleteQuery, deleteParam, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(400, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "success"
                                }));
                                res.end();
                            }
                        });
                    } else {
                        var strQuery = 'insert into mentor_availability SET ?';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(400, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "success"
                                }));
                                res.end();
                            }
                        });
                    }
                }
            });
        }
    }
};
exports.addAvailabilityApi = addAvailabilityApi;
// Update Mentor Availabilty Api
var updateAvailabilityApi = function (req, res) {

    var reqBody = req.body;
    console.log("reqBody", reqBody);
    var eventId = reqBody.eventId;
    var arrWeek = (typeof reqBody.week != undefined) ? reqBody.week : [];

    var availTypeUpdate = reqBody.availTypeUpdate;
    var startDate = reqBody.startDateUpdate;
    var endDate = reqBody.endDateUpdate;

    var endType = "1";
    var fromTime = reqBody.fromTimeUpdate;
    var toTime = reqBody.toTimeUpdate;
    var parentId = reqBody.parentId;
    var arrWeekUTC = [];
    var timepicker1parts = fromTime.split(':');
    fromTime = ('0' + timepicker1parts[0]).slice(-2) + ':' + ('0' + timepicker1parts[1]).slice(-2);

    var timepicker2parts = toTime.split(':');
    toTime = ('0' + timepicker2parts[0]).slice(-2) + ':' + ('0' + timepicker2parts[1]).slice(-2);
    var mentorTimezone = reqBody.updateTz;

    req.checkBody('startDateUpdate', 'Please Select Start Date').notEmpty();

    req.checkBody('fromTimeUpdate', 'Please Select Start Time').notEmpty();
    req.checkBody('toTimeUpdate', 'Please Enter End Time').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({"message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        // startDate = moment.tz(startDate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
        // if (availTypeUpdate == "recurring") {
        //     endDate = moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
        //     var end_type = 1;
        // } else {
        //     endDate = moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').add(5, 'years').format('YYYY-MM-DD HH:mm');
        //     var end_type = 0;
        // }
        startDateTime = moment.tz(startDate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
        // Check if never ends selected in case of recurring event
        if (availTypeUpdate == "recurring") {
            endDateTime = moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm');
            var end_type = 1;
        } else {
            endDateTime = moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').add(5, 'years').format('YYYY-MM-DD HH:mm');
            var end_type = 0;
        }
        //Mitul Code Start
        var strQuery = 'SELECT * FROM mentor_availability where id=?';
        dbconnect.executeQuery(req, res, strQuery, [reqBody.eventId], function (req, res, error, results, fields) {
            if (error) {
                console.log(error);
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"
                }));
                res.end();
            }
            // console.log('resultss', results[0])
            if (results[0].repeat_type === 1) {
                deleteAllRecuringByIdCallback(req, res, eventId, function (req, res, message) {
                    for (i = moment.tz(startDate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone); i < moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone); i.add(1, "day")) {
                        j = i.clone();
                        if (arrWeek.indexOf(j.format("e")) >= 0) {
                            arrWeekUTC.push(j.tz("UTC").format("e"));
                        }
                    }

                    var hoursDiff = (moment.tz(startDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone) - moment.tz(startDate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone)) / (1000 * 60 * 60);

                    var bulkQueryParams = [];
                    for (i = moment.tz(startDateTime, "YYYY-MM-DD HH:mm", "UTC"), j = 0; i <= moment.tz(endDateTime, "YYYY-MM-DD HH:mm", "UTC"); i.add(1, "day"), j++) {
                        k = i.clone();
                        if (arrWeekUTC.indexOf(moment(k).format("e")) >= 0) {
                            var queryArr = [
                                0,
                                req.session.userId,
                                end_type,
                                1,
                                JSON.stringify(arrWeekUTC.filter((v, i, a) => a.indexOf(v) === i)),
                                0,
                                currentUTC,
                                currentUTC,
                                moment(startDateTime).format("YYYY-MM-DD HH:mm"),
                                moment(endDateTime).format("YYYY-MM-DD HH:mm"),
                                moment(k).format("YYYY-MM-DD HH:mm"),
                                moment(k).add(hoursDiff, "hours").format("YYYY-MM-DD HH:mm")
                            ];
                            bulkQueryParams.push(queryArr);
                        }
                    }

                    var queryParam = bulkQueryParams[0];

                    var strQuery = "INSERT INTO mentor_availability (parent_id,mentor_id,end_type,repeat_type,repeat_when,type,created_date,updated_date,recc_start,recc_end,start_date_time,end_date_time) VALUES "
                            + " (?,?,?,?,?,?,?,?,?,?,?,?) ";
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"
                            }));
                            res.end();
                        }
                        bulkQueryParams.splice(0, 1);
                        if (bulkQueryParams.length > 0) {
                            bulkQueryParams.forEach(element => {
                                element[0] = results.insertId;
                            });
                            var sql = "INSERT INTO mentor_availability (parent_id,mentor_id,end_type,repeat_type,repeat_when,type,created_date,updated_date,recc_start,recc_end,start_date_time,end_date_time) VALUES ? ";
                            dbconnect.executeQuery(req, res, sql, [bulkQueryParams], function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"
                                    }));
                                    res.end();
                                }
                                if (req.body.parent_id) {
                                    var select_params = [req.session.userId, req.body.parent_id];
                                    var select_deletedSlotQuery = 'SELECT * FROM deletedSlot WHERE mentor_id = ? AND parent_id = ?';
                                    dbconnect.executeQuery(req, res, select_deletedSlotQuery, select_params, function (req, res, error, results, fields) {
                                        if (error) {
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "error"
                                            }));
                                            res.end();
                                        }
                                        if (results.length > 0) {
                                            var newDate = new Date(results[0].start_date_time);
                                            var day = newDate.getDate();
                                            newDate = startDate;
                                            var newDate1 = new Date(newDate);
                                            newDate1.setDate(day);
                                            var update_params = [newDate1, req.body.parent_id];
                                            var update_deletedSlotQuery = 'UPDATE deletedSlot set start_date_time = ? WHERE parent_id = ?';
                                            dbconnect.executeQuery(req, res, update_deletedSlotQuery, update_params, function (req, res, error, results, fields) {
                                                if (error) {
                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                    res.write(JSON.stringify({
                                                        "message": "error"
                                                    }));
                                                    res.end();
                                                }
                                            });
                                        } else {
                                            res.writeHead(200, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "success"
                                            }));
                                            res.end();
                                        }
                                    });
                                } else {
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "success"
                                    }));
                                    res.end();
                                }
                            });
                        }
                    });
                });
                // var arrWeekUTC = [];
                // for (i = moment.tz(reqBody.startDateUpdate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone); i < moment.tz(reqBody.endDateUpdate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone); i.add(1, "day")) {
                //     j = i.clone();
                //     if (arrWeek.indexOf(j.format("e")) >= 0) {
                //         arrWeekUTC.push(j.tz("UTC").format("e"));
                //     }
                // }

                // var queryParams = [
                //     {
                //         start_date_time: startDate,
                //         end_date_time: endDate,
                //         recc_start: moment(startDate, "YYYY-MM-DD HH:mm").format("HH:mm"),
                //         recc_end: moment(endDate, "YYYY-MM-DD HH:mm").format("HH:mm"),
                //         updated_date: currentUTC,
                //         repeat_when: JSON.stringify(arrWeekUTC.filter((v, i, a) => a.indexOf(v) === i))
                //     },
                //     {
                //         id: reqBody.eventId
                //     }
                // ];
                // var strQuery = 'UPDATE mentor_availability SET ? WHERE ?';
                // dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results, fields) {
                //     if (error) {
                //         res.writeHead(400, { 'Content-Type': 'application/json' });
                //         res.write(JSON.stringify({
                //             "message": "error"
                //         }));
                //         res.end();
                //     } else {
                //         console.log('Update Success')
                //         res.writeHead(200, { 'Content-Type': 'application/json' });
                //         res.write(JSON.stringify({
                //             "message": "success"
                //         }));
                //         res.end();
                //     }
                // });
            } else {
                var queryParam = [
                    moment.tz(startDate + " " + reqBody.fromTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm'),
                    moment.tz(endDate + " " + reqBody.toTimeUpdate, "YYYY-MM-DD HH:mm", mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm'),
                    moment().format("YYYY-MM-DD HH:mm:ss"),
                    eventId
                ];
                var deleteCheckQueryParam = [
                    startDate,
                    endDate
                ];
                var deletedData = "SELECT count(*) as total, id FROM `deletedSlot` where `start_date_time` = ? and `end_date_time` = ?";
                dbconnect.executeQuery(req, res, deletedData, deleteCheckQueryParam, function (req, res, error, results, fields) {
                    if (error) {
                        res.writeHead(400, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "error"
                        }));
                        res.end();
                    } else {
                        if (results[0].total >= 1) {
                            // Check and delete if same timeslot available in deleted slot

                            var deleteQuery = "DELETE FROM `deletedSlot` where id= ?";
                            var deleteParam = [results[0].id];
                            dbconnect.executeQuery(req, res, deleteQuery, deleteParam, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"
                                    }));
                                    res.end();
                                } else {
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "success"
                                    }));
                                    res.end();
                                }
                            });
                        } else {
                            var strQuery = 'UPDATE mentor_availability SET start_date_time= ? , end_date_time = ? , updated_date=? WHERE id = ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"
                                    }));
                                    res.end();
                                } else {
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "success"
                                    }));
                                    res.end();
                                }
                            });
                        }
                    }
                });
            }
        });
        //Mitul Code End





        // if (availType == "recurring") {
        //     var repeat_type = 1;
        //     var parent_id = 0;
        //     var strQuery = 'SELECT id FROM mentor_availability ORDER BY id DESC limit 1';
        //     dbconnect.executeQuery(req, res, strQuery, [], function (req, res, error, results, fields) {
        //         if (error) {
        //             console.log(error);
        //             res.writeHead(400, {'Content-Type': 'application/json'});
        //             res.write(JSON.stringify({
        //                 "message": "error"}));
        //             res.end();
        //         }

        //         var params = [];
        //         var parent_id = (typeof results[0] == "undefined") ? 1 : results[0].id + 1;
        //         //sessoin mentor id req.session.userId;    
        //         for (i = 0; i < arrWeek.length; i++) {
        //             var param = [parent_id, req.session.userId, startDate, endDate, end_type, repeat_type, arrWeek[i], 0, currentUTC];
        //             params.push(param);
        //         }
        //         var queryParam = [params];
        //         var strQuery = 'INSERT INTO mentor_availability (parent_id, mentor_id, start_date_time, end_date_time, end_type, repeat_type, repeat_when, type, created_date) VALUES ?';
        //         dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        //             if (error) {
        //                 res.writeHead(400, {'Content-Type': 'application/json'});
        //                 res.write(JSON.stringify({
        //                     "message": "error"}));
        //                 res.end();
        //             }
        //             if (req.body.parent_id) {
        //                 var select_params = [req.session.userId, req.body.parent_id];
        //                 var select_deletedSlotQuery = 'select * from deletedSlot where mentor_id = ? AND parent_id = ?';
        //                 dbconnect.executeQuery(req, res, select_deletedSlotQuery, select_params, function (req, res, error, results, fields) {
        //                     if (error) {
        //                         res.writeHead(400, {'Content-Type': 'application/json'});
        //                         res.write(JSON.stringify({
        //                             "message": "error"}));
        //                         res.end();
        //                     }
        //                     if (results.length > 0) {
        //                         var newDate = new Date(results[0].start_date_time);
        //                         var day = newDate.getDate();
        //                         newDate = startDate;
        //                         var newDate1 = new Date(newDate);
        //                         newDate1.setDate(day);
        //                         var update_params = [newDate1, req.body.parent_id];
        //                         var update_deletedSlotQuery = 'UPDATE deletedSlot set start_date_time = ? WHERE parent_id = ?';
        //                         dbconnect.executeQuery(req, res, update_deletedSlotQuery, update_params, function (req, res, error, results, fields) {
        //                             if (error) {
        //                                 console.log(error);
        //                                 res.writeHead(400, {'Content-Type': 'application/json'});
        //                                 res.write(JSON.stringify({
        //                                     "message": "error"}));
        //                                 res.end();
        //                             }
        //                         });
        //                     }
        //                 });
        //             }
        //             res.writeHead(200, {'Content-Type': 'application/json'});
        //             res.write(JSON.stringify({
        //                 "message": "success"}));
        //             res.end();
        //         });
        //     });
        // } else {
        //     var queryParam = [
        //         startDate,
        //         endDate,
        //         moment().format("YYYY-MM-DD HH:mm:ss"),
        //         eventId
        //     ];
        //     var deleteCheckQueryParam = [
        //         startDate,
        //         endDate
        //     ];
        //     var deletedData = "SELECT count(*) as total, id FROM `deletedSlot` where `start_date_time` = ? and `end_date_time` = ?";
        //     dbconnect.executeQuery(req, res, deletedData, deleteCheckQueryParam, function (req, res, error, results, fields) {
        //         if (error) {
        //             res.writeHead(400, {'Content-Type': 'application/json'});
        //             res.write(JSON.stringify({
        //                 "message": "error"}));
        //             res.end();
        //         } else {
        //             if (results[0].total >= 1)
        //             {
        //                 // Check and delete if same timeslot available in deleted slot

        //                 var deleteQuery = "DELETE FROM `deletedSlot` where id= ?";
        //                 var deleteParam = [results[0].id];
        //                 dbconnect.executeQuery(req, res, deleteQuery, deleteParam, function (req, res, error, results, fields) {
        //                     if (error) {
        //                         res.writeHead(400, {'Content-Type': 'application/json'});
        //                         res.write(JSON.stringify({
        //                             "message": "error"}));
        //                         res.end();
        //                     } else {
        //                         res.writeHead(200, {'Content-Type': 'application/json'});
        //                         res.write(JSON.stringify({
        //                             "message": "success"}));
        //                         res.end();
        //                     }
        //                 });
        //             } else
        //             {
        //                 var strQuery = 'UPDATE mentor_availability SET start_date_time= ? , end_date_time = ? , updated_date=? WHERE id = ?';
        //                 dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        //                     if (error) {
        //                         res.writeHead(400, {'Content-Type': 'application/json'});
        //                         res.write(JSON.stringify({
        //                             "message": "error"}));
        //                         res.end();
        //                     } else {
        //                         res.writeHead(200, {'Content-Type': 'application/json'});
        //                         res.write(JSON.stringify({
        //                             "message": "success"}));
        //                         res.end();
        //                     }
        //                 });
        //             }
        //         }
        //     });
        // }
    }
};
exports.updateAvailabilityApi = updateAvailabilityApi;
// Update Mentor Availabilty Api ends
var verifyMentorSlug = function (req, res, mentorSlug, nextFunction, callback) {

    var queryParam = [mentorSlug];
    var strQuery = '   SELECT u.user_id mentorId, u.username userName, u.user_type userType, u.slug slug ' +
            '   FROM users u ' +
            '   WHERE u.slug = ? ' +
            '   AND u.status = 1 ' +
            '   AND u.user_type = "mentor" ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            req.flash('errors', 'Mentor not found in system.');
            res.redirect('/');
        } else {
            var mentorId = results[0]['mentorId'];
            callback(req, res, mentorId, nextFunction);
        }
    });
}
exports.verifyMentorSlug = verifyMentorSlug;
var getMentorAllData = function (req, res, mentorId, callback) {
    var data = {};
    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }

    if (isNaN(mentorId) && mentorId <= 0) {
        callback(req, res, data);
    } else {

        async.parallel([
            function (callback) {
                userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, userData, userParams) {
                    callback(null, userData);
                });
            },
            function (callback) {
                getMentorDetails(req, res, mentorId, function (req, res, mentorId, mentorDetails) {
                    callback(null, mentorDetails);
                });
            },
            function (callback) {
                getMentorExpertise(req, res, mentorId, '', function (req, res, mentorId, mentorExpertise) {
                    callback(null, mentorExpertise);
                });
            },
            function (callback) {
                getMentorEmploymenthistory(req, res, mentorId, '', function (req, res, mentorId, mentorEmpHistory) {
                    callback(null, mentorEmpHistory);
                });
            },
            function (callback) {
                getMentorWorkfolio(req, res, mentorId, function (req, res, mentorId, mentorWorkfolio) {
                    callback(null, mentorWorkfolio);
                });
            },
            function (callback) {
                getMentorOrganizationTag(req, res, mentorId, '', function (req, res, mentorId, expertiseId, mentorTags) {
                    callback(null, mentorTags);
                });
            },
            function (callback) {
                getMentorReview(req, res, mentorId, 0, function (req, res, mentorId, mentorReview) {
                    callback(null, mentorReview);
                });
            },
            function (callback) {
                getMentorStarCount(req, res, mentorId, '', function (req, res, mentorId, mentorStarCount) {
                    callback(null, mentorStarCount);
                });
            },
            function (callback) {
                getMentorBookmarkedAndLikedData(req, res, loggedInUserId, mentorId, function (req, res, arrMentorBookmarkAndLikedData) {
                    callback(null, arrMentorBookmarkAndLikedData);
                });
            },
            function (callback) {
                getMentorWorkFolioIndustries(req, res, mentorId, function (req, res, arrMentorWorkFolioIndustries) {
                    callback(null, arrMentorWorkFolioIndustries);
                });
            }
        ],
                function (err, results) {
                    data.userData = results[0];
                    data.mentor_details = results[1];
                    data.mentorExpertise = results[2];
                    data.mentorEmpHistory = results[3];
                    data.mentorWorkfolio = results[4];
                    data.mentor_tags = results[5];
                    data.mentorReview = results[6];
                    data.mentorStarCount = results[7];
                    data.arrMentorBookmarkAndLikedData = results[8];
                    data.arrMentorWorkFolioIndustries = results[9];
                    callback(req, res, data);
                });
        // userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, userData, userParams) {
        //     getMentorDetails(req, res, mentorId, function (req, res, mentorId, mentorDetails) {
        //         getMentorExpertise(req, res, mentorId, '', function (req, res, mentorId, mentorExpertise) {
        //             getMentorEmploymenthistory(req, res, mentorId, '', function (req, res, mentorId, mentorEmpHistory) {
        //                 getMentorWorkfolio(req, res, mentorId, function (req, res, mentorId, mentorWorkfolio) {
        //                     getMentorOrganizationTag(req, res, mentorId, '', function (req, res, mentorId, expertiseId, mentorTags) {
        //                         getMentorReview(req, res, mentorId, function (req, res, mentorId, mentorReview) {
        //                             getMentorStarCount(req, res, mentorId, '', function (req, res, mentorId, mentorStarCount) {
        //                                 getMentorBookmarkedAndLikedData(req, res, loggedInUserId, mentorId, function (req, res, arrMentorBookmarkAndLikedData) {
        //                                     data.userData = userData;
        //                                     data.mentor_details = mentorDetails;
        //                                     data.mentor_tags = mentorTags;
        //                                     data.mentorExpertise = mentorExpertise;
        //                                     data.mentorEmpHistory = mentorEmpHistory;
        //                                     data.mentorWorkfolio = mentorWorkfolio;
        //                                     data.mentorReview = mentorReview;
        //                                     data.mentorStarCount = mentorStarCount;
        //                                     data.arrMentorBookmarkAndLikedData = arrMentorBookmarkAndLikedData;
        //                                     callback(req, res, data);
        //                                 });
        //                             });
        //                         });
        //                     });

        //                 });
        //             });
        //         });
        //     });
        // });
    }
};
exports.getMentorAllData = getMentorAllData;
var getMentorWorkFolioIndustries = function (req, res, mentorId, callback) {

    var queryParam = {};
    var strQuery = '    SELECT mi.industry_id industryId, mi.industry_name industryName ' +
            '   FROM mentor_expertise me, master_industry mi ' +
            '   WHERE me.user_id = ' + mentorId +
            '   AND me.industry = mi.industry_id ' +
            '   AND mi.status = 1 ' +
            '   GROUP BY me.industry ' +
            '   HAVING (SELECT COUNT(*) FROM mentor_workfolio mw WHERE mw.industry_id = mi.industry_id AND  mw.user_id = ' + mentorId + ' ) > 0 ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}

var getMentorBookmarkedAndLikedData = function (req, res, loggedInUserId, mentorId, callback) {

    if ((typeof loggedInUserId != 'undefined' && !isNaN(loggedInUserId) && loggedInUserId > 0) &&
            (typeof loggedInUserId != 'undefined' && !isNaN(loggedInUserId) && loggedInUserId > 0)) {
        var queryParam = [mentorId, loggedInUserId, mentorId, loggedInUserId, loggedInUserId];
        var strQuery = '   SELECT IF(mlm.id > 0, 1, 0) flagMentorLiked, IF(bm.id > 0, 1, 0) flagMentorBookmarked' +
                '   FROM users u, user_details ud ' +
                '   LEFT JOIN mentor_like_mapping mlm ' +
                '       ON mlm.mentor_id = ? ' +
                '       AND mlm.user_id = ? ' +
                '   LEFT JOIN bookmarked_mentor bm ' +
                '       ON bm.mentor_id = ? ' +
                '       AND bm.user_id = ? ' +
                '   WHERE ud.user_id = u.user_id ' +
                '   AND u.user_id = ?  ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorBookmarkAndLikedData, fields) {
            if (arrMentorBookmarkAndLikedData.length > 0) {
                callback(req, res, arrMentorBookmarkAndLikedData[0]);
            } else {
                var objToSend = {
                    flagMentorLiked: 0,
                    flagMentorBookmarked: 0
                };
                callback(req, res, objToSend);
            }
        });
    } else {
        var objToSend = {
            flagMentorLiked: 0,
            flagMentorBookmarked: 0
        };
        callback(req, res, objToSend);
    }
}

var getMentorSearhInformation = function (req, res, dataParams, callback) {
    var queryParam = [];
    var queryForAllData = "0";
    if (typeof dataParams.queryForAllData != 'undefined') {
        queryForAllData = "1";
    }

    var priceSideBar = "0";
    if (typeof dataParams.priceSideBar != 'undefined') {
        priceSideBar = "1";
    }

    var accessUrl = frontConstant.ACCESSURL;
    var strSelect = "*";
    if (queryForAllData == "0") {
        strSelect = '   u.user_id,CONCAT(ud.first_name," ",ud.last_name) name, u.email email,u.phone_country phoneCountry, u.phone_number phoneNumber, u.country_code countryCode, u.user_type userType, u.registered_from registeredFrom, u.slug slug, CONCAT("' + accessUrl + '", "mentorprofile/", u.slug) mentorProfileURL, u.email_verify_code emailVerifyCode, u.status status, ud.profile_image profileImage, ud.linkedin_link linkedInLink, c.country_name countryName, cit.city_name cityName, md.overview, md.overview  overViewListing, md.punchline punchline, md.punchline  punchLineListing, md.profession ,md.company_name ,md.hourly_rate ,md.currency ,md.like_count , ROUND(md.rating) as rating ,md.job_description jobDescription, cit.city_name, ud.country_id countryId, ud.city_id cityId, md.cancellation_policy_type cancellationType, cp.policy_label policyLabel, cp.policy_description policyDescription, cp.cancellation_policy_link cancellationPolicyLink,md.hide_calendar, ' +
                '   DATE_FORMAT(u.dob, "' + constants.MYSQL_DB_DATE + '") DOB, ' +
                '   DATE_FORMAT(CONVERT_TZ(u.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
                '   DATE_FORMAT(CONVERT_TZ(u.updated_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") updatedDate, ';
        if (typeof dataParams.loggedInUserId != 'undefined' && !isNaN(dataParams.loggedInUserId) && dataParams.loggedInUserId > 0) {
            strSelect += ' IF(mlm.id > 0, 1,0) flagMentorLiked, IF(bm.id > 0, 1,0) flagMentorBookmarked ';
        } else {
            strSelect += ' 0 flagMentorLiked, 0 flagMentorBookmarked ';
        }

    } else {
        if (priceSideBar == "0") {
            strSelect = '   u.user_id ';
        } else if (priceSideBar == "1") {
            strSelect = " MAX(md.hourly_rate) maxPrice, MIN(md.hourly_rate) minPrice ";
        }
    }

    var strFrom = '   users u, user_details ud ' +
            '   LEFT JOIN countries c  ' +
            '   ON ud.country_id = c.country_id  ' +
            '   LEFT JOIN cities cit  ' +
            '   ON ud.city_id = cit.city_id  ' +
            '   LEFT JOIN mentor_details md  ' +
            '   ON md.user_id = ud.user_id  ' +
            '   LEFT JOIN mentor_expertise me ' +
            '   ON me.user_id = ud.user_id ' +
            '   LEFT JOIN cancellation_policy cp ' +
            '   ON md.cancellation_policy_type = cp.policy_type ';
    if (typeof dataParams.loggedInUserId != 'undefined' && !isNaN(dataParams.loggedInUserId) && dataParams.loggedInUserId > 0) {
        strFrom += '   LEFT JOIN mentor_like_mapping mlm  ' +
                '   ON mlm.mentor_id = ud.user_id ' +
                '   AND mlm.user_id = "' + dataParams.loggedInUserId + '" ' +
                '   LEFT JOIN bookmarked_mentor bm  ' +
                '   ON bm.mentor_id = ud.user_id  ' +
                '   AND bm.user_id = "' + dataParams.loggedInUserId + '" ';
        if (typeof dataParams.flagBookmarkedMentor != 'undefined' && !isNaN(dataParams.flagBookmarkedMentor) && dataParams.flagBookmarkedMentor > 0) {
            strFrom += '   , bookmarked_mentor bmc  ';
        }
    }

    var strWhere = '   u.user_type IN ("mentor", "organisation")' +
            '   AND u.user_id = ud.user_id ' +
            '   AND u.status = 1 ' +
            '   AND md.hide_from_search = 0 ';
    /*========================================= BOOKMARKED MENTOR WHERE START =========================================*/
    if ((typeof dataParams.loggedInUserId != 'undefined' && !isNaN(dataParams.loggedInUserId) && dataParams.loggedInUserId > 0) &&
            (typeof dataParams.flagBookmarkedMentor != 'undefined' && !isNaN(dataParams.flagBookmarkedMentor) && dataParams.flagBookmarkedMentor > 0)) {
        strWhere += ' AND bmc.mentor_id = ud.user_id ' +
                ' AND bmc.user_id = "' + dataParams.loggedInUserId + '" ';
    }
    /*========================================= BOOKMARKED MENTOR WHERE END  =========================================*/

    /*========================================= PRICE FILTER START =========================================*/
    var hidMinPrice = parseInt(dataParams.minPriceSelected);
    var hidMaxPrice = parseInt(dataParams.maxPriceSelected);
    if ((!isNaN(hidMinPrice) && hidMinPrice > -1) &&
            (!isNaN(hidMaxPrice) && hidMaxPrice > -1)) {
        strWhere += ' AND md.hourly_rate >= "' + hidMinPrice + '" ' +
                ' AND md.hourly_rate <= "' + hidMaxPrice + '" ';
    }
    /*========================================= PRICE FILTER END ===========================================*/

    /*============================================ LOCATION SEARCH START =============================================*/
    var locationType = dataParams.hidLocationType;
    var locationId = dataParams.hidLocationId;
    var sortBy = dataParams.sortBy;
    if (locationType == "R" || locationType == "C") {
        if (locationType == "R") {
            strWhere += '   AND (  me.region = "' + locationId + '") ';
        } else if (locationType == "C") {
            strWhere += '   AND (  me.sub_region = "' + locationId + '") ';
        }
        //strWhere += '   OR me.region = "1") ';
    }
    /*============================================ LOCATION SEARCH END =============================================*/

    var flagMentorExpertiseCheck = false;
    /*============================================ INDUSTRTY SEARCH START =============================================*/
    if (typeof dataParams.industryId != 'undefined' && !isNaN(dataParams.industryId) && dataParams.industryId > 0) {

        var industryId = dataParams.industryId;
        strWhere += '   AND me.industry = "' + industryId + '" ';
    }
    /*============================================ INDUSTRTY SEARCH END ===============================================*/

    /*============================================ SUB INDUSTRTY SEARCH END ===============================================*/
    if (typeof dataParams.subIndustryIds != 'undefined' && dataParams.subIndustryIds.length > 0) {
        var strSubIndustryIds = dataParams.subIndustryIds.join();
        strWhere += '   AND me.sub_industry IN (' + strSubIndustryIds + ') ';
    }
    /*============================================ SUB INDUSTRTY SEARCH END ===============================================*/



    /*============================================ DOMAIN SEARCH START =============================================*/
    if (typeof dataParams.domain != 'undefined' && dataParams.domain.length > 0) {

        var strDomain = dataParams.domain.join();
        strWhere += '   AND me.domain = "' + strDomain + '" ';
    }
    /*============================================ DOMAIN SEARCH END ===============================================*/


    /*============================================ REGION SEARCH START =============================================*/
    if (typeof dataParams.regionId != 'undefined' && !isNaN(dataParams.regionId) && dataParams.regionId > 0) {
        var regionId = dataParams.regionId;
        strWhere += '   AND (  me.region = "' + regionId + '") ';
    }
    /*============================================ REGION SEARCH END ===============================================*/

    /*============================================ SUB REGION SEARCH END ===============================================*/
    if (typeof dataParams.subRegion != 'undefined' && dataParams.subRegion.length > 0) {
        var arrSubRegionIds = dataParams.subRegion.join();
        strWhere += '   AND (  me.sub_region IN (' + arrSubRegionIds + ')) ';
    }
    /*============================================ SUB REGION SEARCH END ===============================================*/


    /*============================================ MENTOR PROFILE SEARCH START =============================================*/


    /*============================================ MENTOR PROFILE PROFESSION SEARCH START =============================================*/
    if (typeof dataParams.designation != 'undefined' && dataParams.designation != "") {
        var mentorDesignation = dataParams.designation;
        strFrom += ' LEFT JOIN mentor_employment_history meh ON meh.user_id = ud.user_id';
        strWhere += ' AND ( md.profession LIKE "%' + decodeURIComponent(mentorDesignation) + '%" OR meh.role LIKE "%' + decodeURIComponent(mentorDesignation) + '%" ) ';
    }
    /*============================================ MENTOR PROFILE PROFESSION SEARCH END  =============================================*/


    /*============================================ MENTOR PROFILE COMPANY NAME SEARCH START =============================================*/
    if (typeof dataParams.company != 'undefined' && dataParams.company != "") {
        var mentoryCompany = dataParams.company;
        strFrom += ' LEFT JOIN mentor_employment_history meh ON meh.user_id = ud.user_id';
        strWhere += ' AND ( md.company_name LIKE "%' + decodeURIComponent(mentoryCompany) + '%" OR meh.company_name LIKE "%' + decodeURIComponent(mentoryCompany) + '%" ) ';
    }
    /*============================================ MENTOR PROFILE COMPANY NAME SEARCH END ===============================================*/


    /*============================================ MENTOR PROFILE COMPANY NAME SEARCH START =============================================*/
    if (typeof dataParams.jobDescription != 'undefined' && dataParams.jobDescription != "") {
        var mentoryJobDescription = dataParams.jobDescription;
        strFrom += ' LEFT JOIN mentor_employment_history meh ON meh.user_id = ud.user_id';
        strWhere += ' AND ( md.job_description LIKE "%' + decodeURIComponent(mentoryJobDescription) + '%" OR meh.job_function LIKE "%' + decodeURIComponent(mentoryJobDescription) + '%" ) ';
    }
    /*============================================ MENTOR PROFILE COMPANY NAME SEARCH END ===============================================*/


    /*============================================ MENTOR PROFILE COUNTRY SEARCH START =============================================*/
    if (typeof dataParams.countryId != 'undefined' && !isNaN(dataParams.countryId) && dataParams.countryId > 0) {
        var mentorCountryId = dataParams.countryId;
        strWhere += '   AND ( c.country_id = "' + mentorCountryId + '" ) ';
    }
    /*============================================ MENTOR PROFILE COUNTRY SEARCH END  =============================================*/


    /*============================================ MENTOR PROFILE CITY SEARCH START =============================================*/
    if (typeof dataParams.cityId != 'undefined' && !isNaN(dataParams.cityId) && dataParams.cityId > 0) {
        var mentorCityId = dataParams.cityId;
        strWhere += ' AND ud.city_id = "' + mentorCityId + '" ';
    }
    /*============================================ MENTOR PROFILE CITY SEARCH END  =============================================*/


    /*============================================ MENTOR PROFILE FIRSTNAME SEARCH START =============================================*/
    if (typeof dataParams.firstName != 'undefined' && dataParams.firstName != "") {
        var firstName = dataParams.firstName;
        strWhere += ' AND ud.first_name LIKE "%' + firstName + '%" ';
    }
    /*============================================ MENTOR PROFILE FIRSTNAME SEARCH END  =============================================*/

    /*============================================ MENTOR PROFILE LASTNAME SEARCH START =============================================*/
    if (typeof dataParams.lastName != 'undefined' && dataParams.lastName != "") {
        var lastName = dataParams.lastName;
        strWhere += ' AND ud.last_name = "' + lastName + '" ';
    }
    /*============================================ MENTOR PROFILE LASTNAME SEARCH END  =============================================*/


    /*============================================ MENTOR PROFILE LASTNAME SEARCH START =============================================*/
    if (typeof dataParams.fullName != 'undefined' && dataParams.fullName != "") {
        var fullName = dataParams.fullName;
        strWhere += ' AND CONCAT_WS(" ", ud.first_name, ud.last_name) = "' + fullName + '" ';
    }
    /*============================================ MENTOR PROFILE LASTNAME SEARCH END  =============================================*/


    /*============================================ MENTOR PROFILE EXPERTISE SEARCH START =============================================*/
    if (typeof dataParams.mentorExpertise != 'undefined' && dataParams.mentorExpertise != "") {
        var mentorExpertise = dataParams.mentorExpertise;
        var arrMentorExpertise = [mentorExpertise];
        var strForExpertiseSearch = "";
        if (arrMentorExpertise.length > 0) {
            arrMentorExpertise.forEach(function (mentorExpertise) {
                strForExpertiseSearch += '"' + mentorExpertise + '", ';
            });
            strForExpertiseSearch = strForExpertiseSearch.replace(/, $/, '');
            strFrom += ' , mentor_organisation_tags mot ';
            strWhere += '   AND mot.user_id = u.user_id ';
            strWhere += '   AND mot.tag_name IN (' + strForExpertiseSearch + ') ';
        }
    }
    /*============================================ MENTOR PROFILE EXPERTISE SEARCH END  =============================================*/




    /*============================================ MENTOR PROFILE SEARCH END  ==============================================*/

    var strOrderBy = "";
    switch (sortBy) {
        case 'SORTBY_RATING':
            strOrderBy = " md.rating DESC, md.hourly_rate DESC ";
            break;
        case 'SORTBY_PHL':
            strOrderBy = " md.hourly_rate DESC ";
            break;
        case 'SORTBY_PLH':
        default:
            strOrderBy = " md.hourly_rate ASC ";
            break;
    }

    var groupBy = "u.user_id";
    if (priceSideBar == "1") {
        groupBy = "";
    }
    if (groupBy.length > 0) {
        groupBy = " GROUP BY " + groupBy;
    }

    var strQuery = '    SELECT ' + strSelect + ' FROM ' + strFrom + ' WHERE ' + strWhere + ' ' + groupBy + ' ORDER BY ' + strOrderBy;
    var perPageLimit = frontConstant.LISTINGPERPAGE;
    var currentPage = dataParams.pageNumber;
    var strLimit = "";
    if (queryForAllData == "0") {
        strLimit = " LIMIT 0, " + perPageLimit;
        if (!isNaN(currentPage) && currentPage > 0) {
            strLimit = " LIMIT " + parseInt(parseInt(perPageLimit) * parseInt(currentPage)) + ", " + perPageLimit;
        }
    }

    strQuery += strLimit;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, []);
        } else {
            if (queryForAllData == "0") {
                getMentorTagsForListing(req, res, results, 0, [], function (req, res, arrResultSet) {
                    setImagePathOfAllUsers(req, res, arrResultSet, 0, [], callback);
                });
            } else {
                callback(req, res, results);
            }
        }
    });
}
exports.getMentorSearhInformation = getMentorSearhInformation;
var getQueryStringJsonForTokenInput = function (req, res, arrDataParams, callback) {
    var txtQueryString = arrDataParams.txtQueryString;
    var arrQryComponent = txtQueryString.split(",");
    var flagMentorExpertiseCheck = false;
    var arrTokenComponents = [];
    arrQryComponent.forEach(function (individualTokenInput) {

        var arrTokenInput = individualTokenInput.split("_");
        var keyWord = arrTokenInput[0].toUpperCase();
        var keyId = arrTokenInput[1];
        if (typeof arrTokenComponents[keyWord] == 'undefined') {
            arrTokenComponents[keyWord] = [];
            arrTokenComponents[keyWord].push(keyId);
        } else {
            arrTokenComponents[keyWord].push(keyId);
        }
    });
    var tagSelected = arrDataParams.hidTagSelected;
    //return false;    
    async.parallel([
        function (callback) {
            if (typeof arrTokenComponents.DOMAIN != 'undefined') {
                var domainKeys = arrTokenComponents.DOMAIN.join();
                miscFunction.getDomainInformationFromIds(req, res, domainKeys, callback);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.INDUSTRY != 'undefined') {
                var industryKeys = arrTokenComponents.INDUSTRY.join();
                miscFunction.getIndustryInformationFromIds(req, res, industryKeys, callback);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.SUBINDUSTRY != 'undefined') {
                var subIndustryKeys = arrTokenComponents.SUBINDUSTRY.join();
                miscFunction.getSubIndustryInformationFromIds(req, res, subIndustryKeys, callback);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.MENTOR != 'undefined') {
                var mentorKeys = arrTokenComponents.MENTOR.join();
                miscFunction.getMentorAndOrganizationInformationFromIds(req, res, mentorKeys, callback);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.MENTOREXPERTISE != 'undefined') {

                var arrMentorExpertiseKeys = arrTokenComponents.MENTOREXPERTISE;
                var arrReturnData = [];
                if (arrMentorExpertiseKeys.length > 0) {
                    arrMentorExpertiseKeys.forEach(function (eachMentorExpertise) {
                        var tmpObj = {
                            id: "MENTOREXPERTISE_" + eachMentorExpertise,
                            value: eachMentorExpertise,
                            typeX: "MENTOREXPERTISE"
                        };
                        arrReturnData.push(tmpObj);
                    });
                }
                callback(null, arrReturnData);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.MENTORFIRSTNAME != 'undefined') {

                var arrMentorFirstNameKeys = arrTokenComponents.MENTORFIRSTNAME;
                var arrReturnData = [];
                if (arrMentorFirstNameKeys.length > 0) {
                    arrMentorFirstNameKeys.forEach(function (eachMentorFirstName) {
                        var tmpObj = {
                            id: "MENTORFIRSTNAME_" + eachMentorFirstName,
                            value: eachMentorFirstName,
                            typeX: "MENTORFIRSTNAME"
                        };
                        arrReturnData.push(tmpObj);
                    });
                }
                callback(null, arrReturnData);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (typeof arrTokenComponents.MENTORLASTNAME != 'undefined') {

                var arrMentorLastNameKeys = arrTokenComponents.MENTORLASTNAME;
                var arrReturnData = [];
                if (arrMentorLastNameKeys.length > 0) {
                    arrMentorLastNameKeys.forEach(function (eachMentorLastName) {
                        var tmpObj = {
                            id: "MENTORLASTNAME_" + eachMentorLastName, value: eachMentorLastName,
                            typeX: "MENTORLASTNAME"
                        };
                        arrReturnData.push(tmpObj);
                    });
                }
                callback(null, arrReturnData);
            } else {
                callback(null, []);
            }
        },
        function (callback) {
            if (tagSelected != "" && typeof tagSelected != 'undefined') {
                var arrTagsComponent = tagSelected.split('_');
                var tagType = arrTagsComponent[0];
                var tagId = arrTagsComponent[1];
                switch (tagType) {
                    case 'CITY':
                        miscFunction.getCityTagsInformationFromIds(req, res, tagId, callback);
                        break;
                    case 'COUNTRY':
                        miscFunction.getCountryTagsInformationFromIds(req, res, tagId, callback);
                        break;
                    case 'COMPANY':
                        var tmpObj = {
                            id: "COMPANY_" + tagId,
                            value: decodeURIComponent(tagId),
                            typeX: "COMPANY"
                        };
                        var arrReturn = [];
                        arrReturn.push(tmpObj);
                        callback(null, arrReturn);
                        break;
                    case 'PROFESSION':
                        var tmpObj = {
                            id: "PROFESSION_" + tagId,
                            value: decodeURIComponent(tagId),
                            typeX: "PROFESSION"
                        };
                        var arrReturn = [];
                        arrReturn.push(tmpObj);
                        callback(null, arrReturn);
                        break;
                }
            } else {
                callback(null, []);
            }
        }
    ], // optional callback
            function (err, results) {
                var arrFinalArray = [];
                results.forEach(function (arrEachItem) {
                    arrEachItem.forEach(function (individualElement) {
                        arrFinalArray.push(individualElement);
                    });
                });
                callback(req, res, arrFinalArray);
            });
}
exports.getQueryStringJsonForTokenInput = getQueryStringJsonForTokenInput;
var getMentorTagsForListing = function (req, res, resultSet, currentIndex, arrFinalData, callback) {
    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var mentorId = thisObj.user_id;
        var queryParam = [mentorId];
        var sqlQuery = '   SELECT mt.mentor_org_tag_id mentorTagId, mt.tag_name tagName ' +
                '   FROM mentor_organisation_tags mt ' +
                '   WHERE mt.user_id = ?';
        dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            thisObj.arrMentorTags = results;
            if (typeof thisObj.company_name != 'undefined') {
                thisObj.URLEncodedCompanyName = encodeURIComponent(thisObj.company_name);
            }
            if (typeof thisObj.profession != 'undefined') {
                thisObj.URLEncodedProffession = encodeURIComponent(thisObj.profession);
            }

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            currentIndex++;
            getMentorTagsForListing(req, res, resultSet, currentIndex, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.getMentorTagsForListing = getMentorTagsForListing;
var setImagePathOfAllUsers = function (req, res, resultSet, currentIndex, arrFinalData, callbackOld, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var profilePicture = thisObj.profileImage;
        var finalProfileImageUrl = constants.ACCESSURL + "images/noimg.png";
        if (profilePicture != "") {
            fs.stat("./uploads/" + profilePicture, function (err, stat) {

                if (err == null) {
                    finalProfileImageUrl = constants.ACCESSURL + "uploads/" + profilePicture;
                }

                thisObj.finalProfileImageUrl = finalProfileImageUrl;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);
                currentIndex++;
                setImagePathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callbackOld, setImagePathOfAllUsers);
            });
        } else {

            thisObj.finalProfileImageUrl = finalProfileImageUrl;
            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            currentIndex++;
            setImagePathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callbackOld, setImagePathOfAllUsers);
        }
    } else {
        callbackOld(req, res, arrFinalData);
    }
};
exports.setImagePathOfAllUsers = setImagePathOfAllUsers;
var add_workfolio_items_model = function (req, res) {
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = {
        user_id: req.session.userId,
        industry_id: req.body.industry,
        created_date: currentUTC,
        media_desc: req.body.media_desc
    };
    if (req.body.workfolio_type == 'upload') {
        queryParam.media_type = 'upload';
        queryParam.media_value = req.file.filename;
        queryParam.media_mime = req.file.mimetype;
        queryParam.media_title = req.body.upload_media_title;
    } else if (req.body.workfolio_type == 'project') {
        queryParam.media_type = 'project';
        queryParam.media_value = req.body.project_link;
        queryParam.media_title = req.body.project_link_title;
    } else if (req.body.workfolio_type == 'media') {
        queryParam.media_type = 'media';
        queryParam.media_value = req.body.new_media_link;
        queryParam.media_title = req.body.media_link_title;
    }
    var strQuery = 'INSERT INTO mentor_workfolio SET ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var queryParam = [results.insertId];
        strQuery = 'SELECT * FROM mentor_workfolio mw ' +
                ' WHERE mw.mentor_workfolio_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "error": "0",
                "message": "success"
            }));
            res.end();
        });
    });
};
exports.add_workfolio_items_model = add_workfolio_items_model;
var delete_workfolio_items_model = function (req, res) {

    var queryParam = [req.session.userId, req.body.type, req.body.data_id];
    var strQuery = 'DELETE FROM mentor_workfolio WHERE user_id=? AND media_type=? AND mentor_workfolio_id=? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": {workfolio_id: req.body.data_id},
            "error": "0",
            "message": "success"
        }));
        res.end();
    });
};
exports.delete_workfolio_items_model = delete_workfolio_items_model;

var mentorEventData = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var currentUTC = constants.CURR_UTC_DATETIME();
    // Mentor Timezone
    var mentorTimezone = '';
    if (typeof req.body.tz != "undefined" && req.body.tz != '') {
        mentorTimezone = req.body.tz;
    } else if (mentorTimezone == '' && req.session.timezone != '') {
        mentorTimezone = req.session.timezone;
    } else {
        mentorTimezone = constants.DEFAULT_TIMEZONE;
    }

    var mentor_id = req.session.userId;
    //var param = [mentor_id, '0'];
    var param = [mentor_id];

    async.parallel([
        // for only availability
        function (availabilityCallback) {

            //var qQuery = 'Select id,DATE_FORMAT(start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(end_date_time, "%Y-%m-%d %H:%i:%s") AS end,title,description,url,repeat_when,repeat_type,type from mentor_availability where mentor_id = ? and repeat_type = ? AND type != 2';
            var qQuery = 'Select id,DATE_FORMAT(start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(end_date_time, "%Y-%m-%d %H:%i:%s") AS end,title,description,url,repeat_when,recc_start,recc_end,repeat_type,type from mentor_availability where mentor_id = ? AND type != 2';

            dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else if (results.length > 0) {
                    var data = [req.session.userId];
                    //var selectDelData = 'Select id,DATE_FORMAT(CONVERT_TZ(start_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(CONVERT_TZ(end_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS end ,repeat_when from deletedSlot where mentor_id = ?'
                    var selectDelData = 'Select id,DATE_FORMAT(start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(end_date_time, "%Y-%m-%d %H:%i:%s") AS end ,repeat_when,availability_id from deletedSlot where mentor_id = ?'
                    dbconnect.executeQuery(req, res, selectDelData, data, function (req, res, error, delResults, fields) {
                        if (error) {
                            res.status(400).send({
                                code: 400,
                                message: "error"
                            })
                        } else {
                            availabilityCallback(null, results, delResults);
                        }
                    })
                } else {
                    availabilityCallback(null, [], []);
                }
            });
        },
        // for private invite not accepted
        function (availabilityCallback) {
            //            var qQuery = 'Select ma.id,DATE_FORMAT(ma.start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,' +
            //                    ' DATE_FORMAT(ma.end_date_time, "%Y-%m-%d %H:%i:%s") AS end,ma.title,ma.description,ma.url, ' +
            //                    ' ma.repeat_when,ma.repeat_type,ma.type from mentor_availability ma , meeting_invitations mi' +
            //                    ' where ma.mentor_id = ? and ma.repeat_type = ? AND ma.parent_id = mi.invitation_id AND ma.type = "2" AND mi.status = "pending"';

            var qQuery = 'Select ma.id,DATE_FORMAT(ma.start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,' +
                    ' DATE_FORMAT(ma.end_date_time, "%Y-%m-%d %H:%i:%s") AS end,ma.title,ma.description,ma.url, ' +
                    ' ma.repeat_when,ma.recc_start,ma.recc_end,ma.repeat_type,ma.type from mentor_availability ma , meeting_invitations mi' +
                    ' where ma.mentor_id = ? AND ma.parent_id = mi.invitation_id AND ma.type = "2" AND mi.status = "pending"';

            dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else if (results.length > 0) {
                    availabilityCallback(null, results, []);
                } else {
                    availabilityCallback(null, [], []);
                }
            });
        }

    ], function (error, result, delResult) {

        var arrResultAvailability = result[0][0];
        var arrResultAvailabilityDel = result[0][1];

        var arrResultPrivateInvite = result[1][0];
        var arrResultPrivateInviteDel = result[1][1];

        var results = arrResultAvailability.concat(arrResultPrivateInvite);
        var delResults = arrResultAvailabilityDel.concat(arrResultPrivateInviteDel);

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": results,
            "delData": delResults,
            "message": "success"
        }));
        res.end();
    });

};
exports.mentorEventData = mentorEventData;

var mentorAvailableDate = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    mentorTimezone = reqBody.tz;
    mentor_id = reqBody.mentorId;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var param = [mentor_id];
    //    var qQuery = 'Select start_date_time AS start ,end_date_time AS end, repeat_type ,repeat_when from mentor_availability where mentor_id = ? AND type = "0"';
    var qQuery = 'Select DATE_FORMAT(mentor_availability.start_date_time, "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(mentor_availability.end_date_time, "%Y-%m-%d %H:%i:%s") AS end, repeat_type ,repeat_when from mentor_availability where mentor_id = ? AND type = "0"';
    dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            var arrFinal = [];
            arrFinal.push(results);
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"
            }));
            res.end();
        }
    });
};
exports.mentorAvailableDate = mentorAvailableDate;
var mentorDeletedDates = function (req, res, callback) {
    console.log('reqqq body', req.body)
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    mentorTimezone = reqBody.tz;
    mentor_id = reqBody.mentorId;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var param = [mentor_id];
    console.log(param)
    //    var qQuery = 'Select start_date_time AS start ,end_date_time AS end, repeat_type ,repeat_when from mentor_availability where mentor_id = ? AND type = "0"';
    var qQuery = 'Select * from deletedSlot where mentor_id = ?';
    dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            var arrFinal = [];
            arrFinal.push(results);
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"
            }));
            res.end();
        }
    });
};
exports.mentorDeletedDates = mentorDeletedDates;

var mentorAvailableTime = function (req, res, callback) {
    console.log('req.body', req.body)
    var reqBody = req.body;
    var date = reqBody.date;
    var day = reqBody.day;
    JSON.stringify(reqBody, null, 4);
    date = moment(date).format("YYYY-MM-DD");
    var dateTime = moment(reqBody.date).format("YYYY-MM-DD 00:00:00");
    //sessoin mentor id req.session.userId;                
    var mentor_id = reqBody.mentorId;
    var tz = reqBody.tz;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var param = [currentUTC, mentor_id, dateTime, date, date];
    console.log(param)
    //    var qQuery = 'Select DATE_FORMAT(start_date_time, "%H") AS start ,DATE_FORMAT(end_date_time, "%H") AS end, repeat_type ,repeat_when from mentor_availability where mentor_id = ? AND start_date_time like ? AND type != 1';
    //Original query Start
    // var qQuery = 'SELECT id,mentor_id,DATE_FORMAT(start_date_time,"%Y-%m-%d %H:%i:%s") as start,DATE_FORMAT( end_date_time ,"%Y-%m-%d %H:%i:%s") as end, ? currentServerTime  FROM `mentor_availability` WHERE `mentor_id` = ? and ((? between CONVERT_TZ(start_date_time ,"UTC","' + tz + '") and  CONVERT_TZ(end_date_time ,"UTC","' + tz + '")) or (DATE(CONVERT_TZ(start_date_time ,"UTC","' + tz + '"))=? OR DATE(CONVERT_TZ(end_date_time ,"UTC","' + tz + '"))=?)) and type="0" and repeat_type="0" ORDER BY start_date_time';
    //Original query End
    var qQuery = 'SELECT id,mentor_id,DATE_FORMAT(start_date_time,"%Y-%m-%d %H:%i:%s") as start,DATE_FORMAT( end_date_time ,"%Y-%m-%d %H:%i:%s") as end, ? currentServerTime , repeat_type FROM `mentor_availability` WHERE `mentor_id` = ? and ((? between CONVERT_TZ(start_date_time ,"UTC","' + tz + '") and  CONVERT_TZ(end_date_time ,"UTC","' + tz + '")) or (DATE(CONVERT_TZ(start_date_time ,"UTC","' + tz + '"))=? OR DATE(CONVERT_TZ(end_date_time ,"UTC","' + tz + '"))=?))  ORDER BY start_date_time';
    dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            if (results.length > 0) {
                var arrFinal = [];
                arrFinal.push(results);
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results,
                    "message": "success"
                }));
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": [],
                    "message": "success"
                }));
                res.end();
            }
        }
    });
};
exports.mentorAvailableTime = mentorAvailableTime;

var mentorAvailabilityData = function (req, res, callback) {
    var reqBody = req.body;
    var id = reqBody.id;
    var mentorTimezone = reqBody.tz;
    JSON.stringify(reqBody, null, 4);
    var currentUTC = constants.CURR_UTC_DATETIME();
    //sessoin mentor id req.session.userId;            

    var param = [id];
    var qQuery = 'Select parent_id from mentor_availability where id = ?';
    dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            // if (results.length != 0) {
            //     if (results[0].parent_id == 0) {
            //         var param = [id, id];
            //     } else {
            //         var param = [results[0].parent_id, results[0].parent_id];
            //     }
            // } else {
            var param = [id];
            // }


            // var qQuery = 'SELECT DATE_FORMAT(CONVERT_TZ(mentor_availability.start_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(CONVERT_TZ(mentor_availability.end_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS end,mentor_availability.*,GROUP_CONCAT(mentor_availability.repeat_when) as repeat_when from mentor_availability where id = ? OR parent_id = ? GROUP BY mentor_id';
            var qQuery = 'SELECT DATE_FORMAT(CONVERT_TZ(mentor_availability.start_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(CONVERT_TZ(mentor_availability.end_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS end,mentor_availability.*,repeat_when from mentor_availability where id = ?';// OR parent_id = ? GROUP BY mentor_id';
            dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    console.log(error);
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {

                    if (results.length == 0) {

                        var param = [id];
                        var qQuery = 'Select DATE_FORMAT(CONVERT_TZ(mentor_availability.start_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS start ,DATE_FORMAT(CONVERT_TZ(mentor_availability.end_date_time,"UTC","' + mentorTimezone + '"), "%Y-%m-%d %H:%i:%s") AS end,mentor_availability.* from mentor_availability where id = ?';
                        dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(400, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "data": results,
                                    "message": "success"
                                }));
                                res.end();
                            }
                        });
                    } else {
                        console.log(results);
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "data": results,
                            "message": "success"
                        }));
                        res.end();
                    }
                }
            });
        }
    });
};
exports.mentorAvailabilityData = mentorAvailabilityData;
var insertNewConversation_model = function (req, res, callback) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [reqBody.reply_sender_id, reqBody.reply_receiver_id, reqBody.reply_sender_id, reqBody.reply_receiver_id];
    var sqlQuery = ' SELECT * ' +
            ' FROM messages ' +
            ' WHERE (message_sender_id=? AND message_receiver_id=?) OR (message_receiver_id=? AND message_sender_id=?); ';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            if (results.length > 0) {
                reqBody.message_id = results[0].message_id;
                insertNewMessage_model(req, res, callback);
            }
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"
            }));
            res.end();
        }
    });
};
exports.insertNewConversation_model = insertNewConversation_model;
var insertNewMessage_model = function (req, res, callback) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [reqBody.message_id, reqBody.reply_sender_id, reqBody.message_id, reqBody.reply_receiver_id];
    var sqlQuery = ' SELECT ' +
            '(SELECT mus.status FROM message_user_status mus WHERE message_id=? AND user_id=?) AS senderStatus, ' +
            '(SELECT mus.status FROM message_user_status mus WHERE message_id=? AND user_id=?) AS receiverStatus;';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            if (results[0].senderStatus != "inbox") {
                updateUserMessageStatus(req, res, reqBody.reply_sender_id, currentUTC, function (req, res) {
                    if (results[0].receiverStatus != "inbox") {
                        updateUserMessageStatus(req, res, reqBody.reply_receiver_id, currentUTC, function (req, res) {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "data": results,
                                "message": "success"
                            }));
                            res.end();
                        });
                    } else {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "data": results,
                            "message": "success"
                        }));
                        res.end();
                    }
                });
            } else if (results[0].receiverStatus != "inbox") {
                updateUserMessageStatus(req, res, reqBody.reply_receiver_id, currentUTC, function (req, res) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "data": results,
                        "message": "success"
                    }));
                    res.end();
                });
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results,
                    "message": "success"
                }));
                res.end();
            }
        }
    });
};
exports.insertNewMessage_model = insertNewMessage_model;
var updateUserMessageStatus = function (req, res, user_id, currentUTC, callback) {
    var queryParam = {
        status: "inbox",
        updated_date: currentUTC
    };
    var params = [queryParam, user_id, req.body.message_id]
    var sqlQuery = ' UPDATE message_user_status SET ? WHERE user_id = ? AND message_id = ?;';
    dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res);
        }
    });
};
exports.updateUserMessageStatus = updateUserMessageStatus;
var checkMentorSlug = function (req, res, callback) {
    var reqBody = req.body;
    var slug = reqBody.slug;
    var queryParam = [slug, req.session.userId];
    var strQuery = 'select * from users where slug = ? AND user_id != ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            if (results.length > 0) {
                res.send(false);
            } else {
                res.send(true);
            }
        }
    });
};
exports.checkMentorSlug = checkMentorSlug;
var mentorDownloadFile = function (req, res, callback) {
    var reqBody = req.body;
    var filePath = reqBody.path;
    var file = "uploads/workfolio/" + filePath;
    res.download(file, filePath, function (err) {
        if (err) {
            console.log(err);
            // Handle error, but keep in mind the response may be partially-sent
            // so check res.headersSent
        } else {
        }
    });
};
exports.mentorDownloadFile = mentorDownloadFile;
var deleteCalEvent = function (req, res) {
    var id = parseInt(req.body.eventId);
    var queryParam = [id];
    var strQuery = 'delete from mentor_availability where id = ?';
    console.log(req.body);
    if (req.body.type == 'recurring') {
        var findQuery = 'select * from mentor_availability where id = ?';
        dbconnect.executeQuery(req, res, findQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.status(400).send({
                    code: 400,
                    message: "Event can not deleted"
                })
            } else {
                console.log(req.body)
                console.log(results[0])
                if (results.length > 0) {
                    var sdate = moment(req.body.delSDate).format('YYYY-MM-DD');
                    var edate = moment(req.body.delEDate).format('YYYY-MM-DD');
                    // var startDateTime = moment.tz(req.body.delSDate, req.body.tz).utcOffset('+0000').format("YYYY-MM-DD HH:mm:ss");
                    // var endDateTime = moment.tz(req.body.delEDate, req.body.tz).utcOffset('+0000').format("YYYY-MM-DD HH:mm:ss");
                    startDateTime = sdate + " " + results[0].recc_start;
                    endDateTime = edate + " " + results[0].recc_end;
                    var data = [results[0].parent_id, results[0].mentor_id, startDateTime, endDateTime, results[0].id, results[0].repeat_when];
                    var insertQuery = ' INSERT INTO deletedSlot ' +
                            ' (parent_id, mentor_id, start_date_time,end_date_time,availability_id,repeat_when) ' +
                            ' VALUES ( ?, ?, ?,?,?, ?)';
                    dbconnect.executeQuery(req, res, insertQuery, data, function (req, res, error, results, fields) {
                        if (error) {
                            console.log(error);
                            res.status(400).send({
                                code: 400,
                                message: "Event can not deleted"
                            })
                        } else {
                            if (results.affectedRows > 0) {
                                res.status(200).send({
                                    code: 200,
                                    message: 'Event deleted successfully'
                                })
                            } else {
                                res.status(400).send({
                                    code: 400,
                                    message: "Event can not deleted"
                                })
                            }
                        }
                    })
                }
            }
        })
    } else {
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                console.log(error);
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "Event can not deleted"
                }));
                res.end();
            } else {
                if (results.affectedRows > 0) {
                    res.status(200).send({
                        status: 200,
                        message: "Event deleted successfully"
                    })
                } else {
                    res.status(400).send({
                        status: 400,
                        message: "Event can not deleted"
                    })
                }
            }
        })
    }
}
exports.deleteCalEvent = deleteCalEvent;

var deleteAllRecuringById = function (req, res) {
    var id = parseInt(req.body.eventId);
    var queryParam = [id];

    var strQuery = 'SELECT * FROM mentor_availability where id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "Event can not deleted"
            }));
            res.end();
        } else {
            if (results.length > 0) {
                var parent_id = results[0].parent_id;
                var strQuery = 'DELETE FROM mentor_availability where id = ? ';
                var deleteParams = [id]
                dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                    if (error) {
                        console.log(error);
                        res.writeHead(400, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "Event can not deleted"
                        }));
                        res.end();
                    } else {
                        if (parent_id > 0) {
                            var strQuery = 'DELETE FROM mentor_availability where parent_id = ? OR id = ?';
                            var deleteParams = [parent_id, parent_id]
                            dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                if (error) {
                                    console.log(error);
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "Event can not deleted"
                                    }));
                                    res.end();
                                } else {
                                    var strQuery = 'DELETE FROM deletedSlot where parent_id = ? OR availability_id = ?';
                                    var deleteParams = [parent_id, id]
                                    dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                        if (error) {
                                            console.log(error);
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "Event can not deleted"
                                            }));
                                            res.end();
                                        } else {
                                            res.status(200).send({
                                                status: 200,
                                                message: "Event deleted successfully"
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            var strQuery = 'DELETE FROM mentor_availability where parent_id = ? ';
                            var deleteParams = [id]
                            dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                if (error) {
                                    console.log(error);
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "Event can not deleted"
                                    }));
                                    res.end();
                                } else {
                                    var strQuery = 'DELETE FROM deletedSlot where parent_id = ? OR availability_id = ?';
                                    var deleteParams = [id, id]
                                    dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                        if (error) {
                                            console.log(error);
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "Event can not deleted"
                                            }));
                                            res.end();
                                        } else {
                                            res.status(200).send({
                                                status: 200,
                                                message: "Event deleted successfully"
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                res.status(200).send({
                    status: 200,
                    message: "Event deleted successfully"
                });
            }
        }
    });
}
exports.deleteAllRecuringById = deleteAllRecuringById;

var deleteAllRecuringByIdCallback = function (req, res, eventId, callback) {
    var id = parseInt(eventId);
    var queryParam = [id];

    var strQuery = 'SELECT * FROM mentor_availability where id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, "error");
        } else {
            if (results.length > 0) {
                var parent_id = results[0].parent_id;
                var strQuery = 'DELETE FROM mentor_availability where id = ? ';
                var deleteParams = [id]
                dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                    if (error) {
                        console.log(error);
                        callback(req, res, "error");
                    } else {
                        if (parent_id > 0) {
                            var strQuery = 'DELETE FROM mentor_availability where parent_id = ? OR id = ?';
                            var deleteParams = [parent_id, parent_id]
                            dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                if (error) {
                                    console.log(error);
                                    callback(req, res, "error");
                                } else {
                                    var strQuery = 'DELETE FROM deletedSlot where parent_id = ? OR availability_id = ?';
                                    var deleteParams = [parent_id, id]
                                    dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                        if (error) {
                                            console.log(error);
                                            callback(req, res, "error");
                                        } else {
                                            callback(req, res, "success");
                                        }
                                    });
                                }
                            });
                        } else {
                            var strQuery = 'DELETE FROM mentor_availability where parent_id = ? ';
                            var deleteParams = [id]
                            dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                if (error) {
                                    console.log(error);
                                    callback(req, res, "error");
                                } else {
                                    var strQuery = 'DELETE FROM deletedSlot where parent_id = ? OR availability_id = ?';
                                    var deleteParams = [id, id]
                                    dbconnect.executeQuery(req, res, strQuery, deleteParams, function (req, res, error, results, fields) {
                                        if (error) {
                                            console.log(error);
                                            callback(req, res, "error");
                                        } else {
                                            callback(req, res, "success");
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                callback(req, res, "success");
            }
        }
    });
}
exports.deleteAllRecuringByIdCallback = deleteAllRecuringByIdCallback;


var getDeletedSlotsByMentorId = function (req, res) {

}

var featuredMentorProfile = function (req, res, loggedInUserId, limit, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var queryParam = [];
    var strQuery = 'SELECT u.user_id mentorId, u.slug, CONCAT(ud.first_name, " ", ud.last_name) as name, ud.profile_image AS profileImage, md.profession, md.job_description, md.company_name,ctry.country_name as country_name, ud.linkedin_link linkedInLink, ct.city_name,   ';
    if (typeof loggedInUserId != 'undefined' && !isNaN(loggedInUserId) && loggedInUserId > 0) {
        strQuery += ' IF(mlm.id > 0, 1,0) flagMentorLiked, IF(bm.id > 0, 1,0) flagMentorBookmarked ';
    } else {
        strQuery += ' 0 flagMentorLiked, 0 flagMentorBookmarked ';
    }

    strQuery += ' FROM users u ' +
            ' LEFT JOIN user_details ud ON ud.user_id=u.user_id' +
            ' LEFT JOIN mentor_details md ON md.user_id=u.user_id' +
            ' LEFT JOIN social_login sl ON sl.user_id=u.user_id' +
            ' LEFT JOIN countries ctry ON ctry.country_id = ud.country_id' +
            ' LEFT JOIN cities ct ON ct.city_id = ud.city_id';

    if (typeof loggedInUserId != 'undefined' && !isNaN(loggedInUserId) && loggedInUserId > 0) {
        strQuery += '   LEFT JOIN mentor_like_mapping mlm  ' +
                '   ON mlm.mentor_id = ud.user_id ' +
                '   AND mlm.user_id = "' + loggedInUserId + '" ' +
                '   LEFT JOIN bookmarked_mentor bm  ' +
                '   ON bm.mentor_id = ud.user_id  ' +
                '   AND bm.user_id = "' + loggedInUserId + '" ';
    }

    strQuery += '    WHERE u.status=1 AND u.user_type!="user" AND md.is_featured = 1  ORDER BY RAND() LIMIT ' + limit;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            //throw error;
        } else {

            if (results.length == 0) {
                callback(req, res, {});
            } else {
                setNextRowIndex(req, res, results, 1, [], function (req, res, arrResultsSetWithNextRowIndex) {
                    setImagePathOfAllUsers(req, res, arrResultsSetWithNextRowIndex, 0, [], callback, '');
                });
            }
        }
    });
};
exports.featuredMentorProfile = featuredMentorProfile;
var setNextRowIndex = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        if (currentIndex % 3 == 0) {
            thisObj.nextRow = "1";
        } else {
            thisObj.nextRow = "0";
        }

        arrFinalData.push(thisObj);
        resultSet.splice(0, 1);
        currentIndex++;
        setNextRowIndex(req, res, resultSet, currentIndex, arrFinalData, callback);
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.setNextRowIndex = setNextRowIndex;
var saveReview = function (req, res, sessionId, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();
    var reqBody = req.body;
    var reqPBody = JSON.parse(reqBody.data);
    var user_id = "";
    var reviewer_id = "";
    var star1 = 0;
    var star2 = 0;
    var star3 = 0;
    var star4 = 0;
    var star5 = 0;
    var userType = "";
    if (typeof reqPBody.starRatingData != 'undefined') {
        if (typeof reqPBody.starRatingData[1] != 'undefined') {
            star1 = reqPBody.starRatingData[1];
        }
        if (typeof reqPBody.starRatingData[2] != 'undefined') {
            star2 = reqPBody.starRatingData[2];
        }
        if (typeof reqPBody.starRatingData[3] != 'undefined') {
            star3 = reqPBody.starRatingData[3];
        }
        if (typeof reqPBody.starRatingData[4] != 'undefined') {
            star4 = reqPBody.starRatingData[4];
        }
        if (typeof reqPBody.starRatingData[5] != 'undefined') {
            star5 = reqPBody.starRatingData[5];
        }
    }

    var queryParam = [reqPBody.meetingDetailId];
    var strQuery = `SELECT mi.requester_id, mi.user_id
        FROM meeting_details md
        LEFT JOIN meeting_invitations mi ON mi.invitation_id = md.invitation_id
        WHERE md.meeting_id=?`;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMeetingInviteInfo, fields) {
        if (arrMeetingInviteInfo.length > 0) {

            if (reqPBody.reviewer_id == arrMeetingInviteInfo[0].requester_id) {
                reviewer_id = arrMeetingInviteInfo[0].requester_id;
                user_id = arrMeetingInviteInfo[0].user_id;
                userType = 'mentor';
            } else if (reqPBody.reviewer_id == arrMeetingInviteInfo[0].user_id) {
                reviewer_id = arrMeetingInviteInfo[0].user_id;
                user_id = arrMeetingInviteInfo[0].requester_id;
                userType = 'user';
            }

            var queryParam = {
                reviewer_id: reviewer_id,
                user_id: user_id,
                meeting_detail_id: reqPBody.meetingDetailId,
                communication: star1,
                call_quality: star2,
                system_stability: star3,
                dexpertise_or_pre_call: star4,
                star5: star5,
                review_rating: reqPBody.review_rating,
                review_desc: reqPBody.review_desc,
                userType: userType,
                created_date: currentUTC
            };
            console.log(queryParam)
            var strQuery = 'INSERT INTO user_reviews SET ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }
                if (results.insertId > 0) {
                    var queryParam = [reqPBody.user_id, reqPBody.user_id]
                    var strQuery = 'UPDATE mentor_details set rating = (SELECT AVG(urs.review_rating) FROM user_reviews urs  WHERE urs.user_id = ?) Where mentor_details.user_id = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            var queryParam = [reqPBody.user_id]

                            var strQuery = 'Select email from users where user_id = ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    throw error;
                                } else {
                                    var email = results[0].email;

                                    var queryParam = [reqPBody.reviewer_id]
                                    var strQuery = 'Select CONCAT(first_name," ",last_name) as name from user_details where user_id = ?';
                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                        if (error) {
                                            throw error;
                                        } else {
                                            var name = results[0].name;
                                            var Communication = "Communication " + star1 + " Stars";
                                            var call_quality_mail = "Call Quality " + star2 + " Stars";
                                            var system_stability_mail = "System Stability " + star3 + " Stars";

                                            if (reqBody.maillabel == "Domain Expertise") {
                                                var dexpertise_or_pre_call_mail = "Domain Expertise " + star4 + " Stars";
                                            } else {
                                                var dexpertise_or_pre_call_mail = "Pre Call Preparation" + star4 + " Stars";
                                            }
                                            var templateVariable = {
                                                templateURL: "mailtemplate/reviewmail",
                                                reviewer: name, communication: Communication,
                                                call_quality: call_quality_mail,
                                                system_stability: system_stability_mail,
                                                dexpertise_or_pre_call: dexpertise_or_pre_call_mail,
                                                review_desc: reqPBody.review_desc
                                            };
                                            var mailParamsObject = {
                                                templateVariable: templateVariable,
                                                to: email,
                                                subject: 'SourceAdvisor User Review.'
                                            };
                                            //                                            mailer.sendMail(req, res, mailParamsObject, function (err) {
                                            //                                                if (err) {
                                            //                                                    console.log(err);
                                            //                                                } else {
                                            //                                                    console.log(err);
                                            //                                                }
                                            //                                            });
                                        }
                                    });
                                }
                            });

                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "data": results,
                                "message": "success"
                            }));
                            res.end();
                        }
                    });
                } else {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "data": results,
                        "message": "error"
                    }));
                    res.end();
                }
            });
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": "",
                "message": "error"
            }));
            res.end();
        }
    });
};
exports.saveReview = saveReview;
var getMeetingInvitationInfo = function (req, res, meetingInvitationId, callback) {
    if (meetingInvitationId != 'undefined' && !isNaN(meetingInvitationId) && meetingInvitationId > 0) {

        var queryParam = [meetingInvitationId];
        var strQuery = '   SELECT mi.invitation_id, mi.requester_id, mi.user_id, mi.invoice_id, mi.meeting_type, mi.meeting_purpose, mi.meeting_purpose_indetail, mi.duration, mi.slot_date, mi.slot_time, mi.document, mi.status, mi.created_date, mi.updated_date ' +
                '   FROM meeting_invitations mi, invoice i ' +
                '   WHERE i.invitation_id = mi.invitation_id ' +
                '   AND i.invoice_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMeetingInviteInfo, fields) {
            if (arrMeetingInviteInfo.length > 0) {
                callback(req, res, arrMeetingInviteInfo[0]);
            } else {
                callback(req, res, {});
            }
        });
    } else {
        callback(req, res, {});
    }
}
exports.getMeetingInvitationInfo = getMeetingInvitationInfo;

var sendAcceptedInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });
    var amount = invitationData.amount;
    getMentorPendingInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var slotsMentorObj = {};
            var slotsUserObj = {};
            invitationSlots.forEach(function (slot) {
                if (slot.status == 'accepted') {
                    slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                    slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                    slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                    slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                    slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                    slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                    slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                    slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                    slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                    slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                    slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                    slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                }
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/acceptedInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserObj,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                invitationId: invitationId
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has accepted your meeting request"
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });


            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/acceptedInvitationMailUserCardChg",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserObj,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                invitationId: invitationId,
                amount: amount
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", your payment card has been charged for an amount of USD " + amount + " , for a meeting confirmed by " + arrMentorData.firstName + " " + arrMentorData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/acceptedInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserObj,
                meeting_url: invitationData.meeting_key,
                invitationId: invitationId
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/acceptedInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorObj,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                invitationId: invitationId
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", You have  accepted a meeting request from " + arrUserData.firstName + " " + arrUserData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/acceptedInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorObj,
                meeting_url: invitationData.meeting_key,
                invitationId: invitationId
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendAcceptedInvitationMail = sendAcceptedInvitationMail;

var sendAcceptedPrivateInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData, privateInvite) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });
    getMentorPendingInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var slotsMentorObj = {};
            var slotsUserObj = {};
            invitationSlots.forEach(function (slot) {
                if (slot.status == 'accepted') {
                    slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                    slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                    slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                    slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                    slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                    slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                    slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                    slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                    slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                    slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                    slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                    slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                }
            });
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/acceptedPrivateInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserObj,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", Thank you for accepting private invite sent by " + arrMentorData.firstName + " " + arrMentorData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/acceptedPrivateInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserObj,
                meeting_url: invitationData.meeting_key
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/acceptedPrivateInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorObj,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", " + arrUserData.firstName + " " + arrUserData.lastName + " has accepted your private invite. "
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/acceptedPrivateInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorObj,
                meeting_url: invitationData.meeting_key
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendAcceptedPrivateInvitationMail = sendAcceptedPrivateInvitationMail;

var mentorPrivateInvitesData = function (req, res, callback) {
    var reqBody = req.body;
    var id = reqBody.id;
    var mentorTimezone = reqBody.tz;
    JSON.stringify(reqBody, null, 4);
    var currentUTC = constants.CURR_UTC_DATETIME();
    //sessoin mentor id req.session.userId;            

    var param = [id];
    var qQuery = 'Select parent_id from mentor_availability where id = ?';
    dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            if (results.length != 0) {
                if (results[0].parent_id == 0) {
                    var param = [id, id];
                } else {
                    var param = [results[0].parent_id];
                }
            } else {
                var param = [id];
            }

            var qQuery = 'Select mi.invitation_id,mi.user_id, mi.duration,mi.meeting_purpose,DATE_FORMAT(CONVERT_TZ(CONCAT(mis.date," ",mis.time) ,"UTC","' + mentorTimezone + '"), "%Y-%m-%d") AS start , DATE_FORMAT(CONVERT_TZ(CONCAT(mis.date," ",mis.time) ,"UTC","' + mentorTimezone + '"), "%H:%i:%s") AS time  from meeting_invitations mi LEFT JOIN meeting_invitation_slot_mapping mis ON mi.invitation_id = mis.invitation_id where mi.invitation_id = ?';
            dbconnect.executeQuery(req, res, qQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    console.log(error);
                    res.writeHead(400, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "error"
                    }));
                    res.end();
                } else {

                    if (results.length == 0) {

                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "data": "",
                            "message": "data not found"
                        }));
                        res.end();
                    } else {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "data": results,
                            "message": "success"
                        }));
                        res.end();
                    }
                }
            });
        }
    });
};
exports.mentorPrivateInvitesData = mentorPrivateInvitesData;


var update_private_invitation = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var invitation_id = reqBody.update_invitation_id;

    var user_id = reqBody.update_lstUserList;
    var mentor_id = req.session.userId;
    var callDate1 = reqBody.update_callDate1;
    var callDuration = reqBody.update_call_duration;
    var privateInviteMsg = reqBody.update_privateInviteMsg;
    var mentorTimezone = reqBody.update_zone;
    var timepicker1 = reqBody.update_privateInviteTimePicker1;
    var timepicker1parts = timepicker1.split(':');
    var meetingstarttime1 = ('0' + timepicker1parts[0]).slice(-2) + ':' + ('0' + timepicker1parts[1]).slice(-2);
    var meetingendtime1 = moment(callDate1 + " " + meetingstarttime1).add(callDuration, "minutes").format('HH:mm');
    /* var timepicker2 = reqBody.privateInviteTimePicker2;                    
     var timepicker2parts = timepicker2.split(':');
     var meetingendtime1 = ('0' + timepicker2parts[0]).slice(-2) + ':' + ('0' + timepicker2parts[1]).slice(-2);
     */

    var callDate2 = reqBody.update_callDate2;
    var timepicker3 = reqBody.update_privateInviteTimePicker2;
    var timepicker3parts = timepicker3.split(':');
    var meetingstarttime2 = ('0' + timepicker3parts[0]).slice(-2) + ':' + ('0' + timepicker3parts[1]).slice(-2);
    var meetingendtime2 = moment(callDate2 + " " + meetingstarttime2).add(callDuration, "minutes").format('HH:mm');
    /*var timepicker4 = reqBody.privateInviteTimePicker4;
     var timepicker4parts = timepicker4.split(':');
     var meetingendtime2 = ('0' + timepicker4parts[0]).slice(-2) + ':' + ('0' + timepicker4parts[1]).slice(-2);*/

    var callDate3 = reqBody.update_callDate3;
    var timepicker5 = reqBody.update_privateInviteTimePicker3;
    var timepicker5parts = timepicker5.split(':');
    var meetingstarttime3 = ('0' + timepicker5parts[0]).slice(-2) + ':' + ('0' + timepicker5parts[1]).slice(-2);
    var meetingendtime3 = moment(callDate3 + " " + meetingstarttime3).add(callDuration, "minutes").format('HH:mm');
    /*var timepicker6 = reqBody.privateInviteTimePicker6;
     var timepicker6parts = timepicker6.split(':');
     var meetingendtime3 = ('0' + timepicker6parts[0]).slice(-2) + ':' + ('0' + timepicker6parts[1]).slice(-2);*/


    var count = [];
    var mailStartDate1 = "";
    var mailEndDate1 = "";
    var mailStartDate2 = "";
    var mailEndDate2 = "";
    var mailStartDate3 = "";
    var mailEndDate3 = "";
    var already = 0;
    //req.checkBody('lstUserList', 'Please Select User').notEmpty();
    //req.checkBody('callDate1', 'Please Select Date').notEmpty();
    //req.checkBody('privateInviteTimePicker1', 'Please Select Start Time').notEmpty();
    //req.checkBody('privateInviteTimePicker2', 'Please Select End Time').notEmpty();


    if (typeof invitation_id == "undefined" || invitation_id == "") {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"
        }));
        res.end();
    } else {

        var queryParam = [invitation_id];
        var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                ' FROM meeting_invitation_slot_mapping mis ' +
                ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                ' WHERE mis.invitation_id= ?';
        dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, resultsPreviousSlot, fields) {
            if (error) {
                console.log(error);
            } else {
                var queryParam = [invitation_id];
                var strQuery = 'Delete From meeting_invitations where invitation_id = ?';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        res.writeHead(400, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "error"
                        }));
                        res.end();
                    } else {
                        var queryParam = [invitation_id];
                        var strQuery = 'Delete From meeting_invitation_slot_mapping where invitation_id = ?';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(400, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                var queryParam = [invitation_id];
                                var strQuery = 'Delete From mentor_availability where parent_id = ?';
                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                    if (error) {
                                        res.writeHead(400, {'Content-Type': 'application/json'});
                                        res.write(JSON.stringify({
                                            "message": "error"
                                        }));
                                        res.end();
                                    } else {
                                        async.parallel([
                                            function (callbackForSlot) {
                                                var dataToSend = {
                                                    slot: callDate1,
                                                    startTime: meetingstarttime1,
                                                    endTime: meetingendtime1,
                                                    mentorTimezone: mentorTimezone,
                                                    slotcount: 1
                                                };

                                                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                                                    callbackForSlot(null, arrSlotData);
                                                });
                                            },
                                            function (callbackForSlot) {
                                                var dataToSend = {
                                                    slot: callDate2,
                                                    startTime: meetingstarttime2,
                                                    endTime: meetingendtime2,
                                                    mentorTimezone: mentorTimezone,
                                                    slotcount: 2
                                                };

                                                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                                                    callbackForSlot(null, arrSlotData);
                                                });
                                            },
                                            function (callbackForSlot) {

                                                var dataToSend = {
                                                    slot: callDate3,
                                                    startTime: meetingstarttime3,
                                                    endTime: meetingendtime3,
                                                    mentorTimezone: mentorTimezone,
                                                    slotcount: 3
                                                };
                                                checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                                                    callbackForSlot(null, arrSlotData);
                                                });
                                            }
                                        ],
                                                function (err, results) {
                                                    var toBeContinue = true;
                                                    var conflictedSlot = [];
                                                    results.forEach(function (reachResult) {
                                                        if (reachResult.msg == "already") {
                                                            toBeContinue = false;
                                                            conflictedSlot.push(reachResult.slotcount);
                                                        }
                                                    });

                                                    if (!toBeContinue) {
                                                        res.writeHead(200, {'Content-Type': 'application/json'});
                                                        res.write(JSON.stringify({
                                                            "message": "already",
                                                            "data": conflictedSlot
                                                        }));
                                                        res.end();
                                                    } else {


                                                        if (callDate1 != "") {

                                                            meetingstarttimeUTC = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('HH:mm');
                                                            meetingendtimeUTC = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('HH:mm');
                                                            callDate11 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                                                            meetingstarttimeUTC = meetingstarttimeUTC;
                                                            meetingendtimeUTC = meetingendtimeUTC;

                                                            var queryParam = {
                                                                invitation_id: "DEFAULT",
                                                                requester_id: req.session.userId,
                                                                user_id: user_id,
                                                                invoice_id: 0,
                                                                meeting_type: 'virtual',
                                                                meeting_purpose: privateInviteMsg,
                                                                meeting_purpose_indetail: privateInviteMsg,
                                                                duration: callDuration,
                                                                status: 'pending',
                                                                invitation_type: 1,
                                                                created_date: moment.utc().format()
                                                            };


                                                            var strQuery = 'INSERT INTO meeting_invitations SET ?';
                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                if (error) {
                                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                                    res.write(JSON.stringify({
                                                                        "message": "error"
                                                                    }));
                                                                    res.end();
                                                                } else {
                                                                    var invitation_id = results.insertId;
                                                                    var queryParam = {
                                                                        id: "DEFAULT",
                                                                        invitation_id: invitation_id,
                                                                        mentor_id: req.session.userId,
                                                                        date: callDate11,
                                                                        time: meetingstarttimeUTC,
                                                                        status: 'pending',
                                                                        //created_date: constants.CURR_UTC_DATETIME()
                                                                    };
                                                                    var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                        if (error) {
                                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                                            res.write(JSON.stringify({
                                                                                "message": "error"
                                                                            }));
                                                                            res.end();
                                                                        } else {
                                                                            var repeat_type = 0;
                                                                            startDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                            endDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                            var queryParam = {
                                                                                id: "DEFAULT",
                                                                                parent_id: invitation_id,
                                                                                mentor_id: req.session.userId,
                                                                                start_date_time: startDate1,
                                                                                end_date_time: endDate1,
                                                                                end_type: 1,
                                                                                title: "Private Invitation",
                                                                                description: privateInviteMsg,
                                                                                repeat_type: repeat_type,
                                                                                repeat_when: "",
                                                                                type: 2,
                                                                                created_date: constants.CURR_UTC_DATETIME()
                                                                            };
                                                                            var strQuery = 'INSERT INTO mentor_availability SET ?';
                                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                if (error) {
                                                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                    res.write(JSON.stringify({
                                                                                        "message": "error"
                                                                                    }));
                                                                                    res.end();
                                                                                } else {

                                                                                    var strQuery2 = 'insert into invoice SET ?';
                                                                                    // Insert into invoice table
                                                                                    var queryParam2 = {
                                                                                        user_id: user_id,
                                                                                        invitation_id: invitation_id,
                                                                                        meeting_id: "0",
                                                                                        status: 'unpaid',
                                                                                        created_date: constants.CURR_UTC_DATETIME()
                                                                                    };


                                                                                    dbconnect.executeQuery(req, res, strQuery2, queryParam2, function (req, res, error, results, fields) {
                                                                                        if (error) {
                                                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                            res.write(JSON.stringify({
                                                                                                "message": "error"
                                                                                            }));
                                                                                            res.end();
                                                                                        } else {

                                                                                            if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                                meetingstarttimeUTC = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                                meetingendtimeUTC = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                                callDate22 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                                                                                                meetingstarttimeUTC = meetingstarttimeUTC;
                                                                                                meetingendtimeUTC = meetingendtimeUTC;

                                                                                                var queryParam = {
                                                                                                    id: "DEFAULT",
                                                                                                    invitation_id: invitation_id,
                                                                                                    mentor_id: req.session.userId,
                                                                                                    date: callDate22,
                                                                                                    time: meetingstarttimeUTC,
                                                                                                    status: 'pending',
                                                                                                    //created_date: constants.CURR_UTC_DATETIME()
                                                                                                };
                                                                                                var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                                                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                                    if (error) {
                                                                                                        res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                                        res.write(JSON.stringify({
                                                                                                            "message": "error"
                                                                                                        }));
                                                                                                        res.end();
                                                                                                    } else {
                                                                                                        var repeat_type = 0;
                                                                                                        startDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                        endDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                        var queryParam = {
                                                                                                            id: "DEFAULT",
                                                                                                            parent_id: invitation_id,
                                                                                                            mentor_id: req.session.userId,
                                                                                                            start_date_time: startDate2,
                                                                                                            end_date_time: endDate2,
                                                                                                            end_type: 1,
                                                                                                            title: "Private Invitation",
                                                                                                            description: privateInviteMsg,
                                                                                                            repeat_type: repeat_type,
                                                                                                            repeat_when: "",
                                                                                                            type: 2,
                                                                                                            created_date: constants.CURR_UTC_DATETIME()
                                                                                                        };
                                                                                                        var strQuery = 'INSERT INTO mentor_availability SET ?';
                                                                                                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                                            if (error) {
                                                                                                                res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                                                res.write(JSON.stringify({
                                                                                                                    "message": "error"
                                                                                                                }));
                                                                                                                res.end();
                                                                                                            } else {
                                                                                                                if (typeof callDate3 != "undefined" && callDate3 != "") {

                                                                                                                    meetingstarttimeUTC = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                                                    meetingendtimeUTC = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('HH:mm');
                                                                                                                    callDate33 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD');
                                                                                                                    meetingstarttimeUTC = meetingstarttimeUTC;
                                                                                                                    meetingendtimeUTC = meetingendtimeUTC;
                                                                                                                    var queryParam = {
                                                                                                                        id: "DEFAULT",
                                                                                                                        invitation_id: invitation_id,
                                                                                                                        mentor_id: req.session.userId,
                                                                                                                        date: callDate33,
                                                                                                                        time: meetingstarttimeUTC,
                                                                                                                        status: 'pending',
                                                                                                                        //created_date: constants.CURR_UTC_DATETIME()
                                                                                                                    };
                                                                                                                    var strQuery = 'INSERT INTO meeting_invitation_slot_mapping SET ?';
                                                                                                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                                                        if (error) {
                                                                                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                                                            res.write(JSON.stringify({
                                                                                                                                "message": "error"
                                                                                                                            }));
                                                                                                                            res.end();
                                                                                                                        } else {
                                                                                                                            var repeat_type = 0;
                                                                                                                            var repeat_type = 0;
                                                                                                                            startDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                            endDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                            var queryParam = {
                                                                                                                                id: "DEFAULT",
                                                                                                                                parent_id: invitation_id,
                                                                                                                                mentor_id: req.session.userId,
                                                                                                                                start_date_time: startDate3,
                                                                                                                                end_date_time: endDate3,
                                                                                                                                end_type: 1,
                                                                                                                                title: "Private Invitation",
                                                                                                                                description: privateInviteMsg,
                                                                                                                                repeat_type: repeat_type,
                                                                                                                                repeat_when: "",
                                                                                                                                type: 2,
                                                                                                                                created_date: constants.CURR_UTC_DATETIME()
                                                                                                                            };
                                                                                                                            var strQuery = 'INSERT INTO mentor_availability SET ?';
                                                                                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                                                                                if (error) {
                                                                                                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                                                                                                    res.write(JSON.stringify({
                                                                                                                                        "message": "error"
                                                                                                                                    }));
                                                                                                                                    res.end();
                                                                                                                                } else {


                                                                                                                                    if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                                                                                        mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                        mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                    }


                                                                                                                                    if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                                                                        mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                        mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                    }

                                                                                                                                    if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                                                                                        mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                        mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                                    }


                                                                                                                                    privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", resultsPreviousSlot, callback);
                                                                                                                                    privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", resultsPreviousSlot, callback);
                                                                                                                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                                                                    res.write(JSON.stringify({
                                                                                                                                        "message": "success"
                                                                                                                                    }));
                                                                                                                                    res.end();
                                                                                                                                }
                                                                                                                            });
                                                                                                                        }
                                                                                                                    });
                                                                                                                } else {


                                                                                                                    if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                                                                        mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                        mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                    }


                                                                                                                    if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                                                        mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                        mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                    }

                                                                                                                    if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                                                                        mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                        mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                                    }


                                                                                                                    privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", resultsPreviousSlot, callback);
                                                                                                                    privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", resultsPreviousSlot, callback);
                                                                                                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                                                    res.write(JSON.stringify({
                                                                                                                        "message": "success"
                                                                                                                    }));
                                                                                                                    res.end();
                                                                                                                }
                                                                                                            }
                                                                                                        });
                                                                                                    }
                                                                                                });
                                                                                            } else {


                                                                                                if (typeof callDate1 != "undefined" && callDate1 != "") {
                                                                                                    mailStartDate1 = moment.tz(callDate1 + " " + meetingstarttime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    mailEndDate1 = moment.tz(callDate1 + " " + meetingendtime1, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                }


                                                                                                if (typeof callDate2 != "undefined" && callDate2 != "") {
                                                                                                    mailStartDate2 = moment.tz(callDate2 + " " + meetingstarttime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    mailEndDate2 = moment.tz(callDate2 + " " + meetingendtime2, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                }

                                                                                                if (typeof callDate3 != "undefined" && callDate3 != "") {
                                                                                                    mailStartDate3 = moment.tz(callDate3 + " " + meetingstarttime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                    mailEndDate3 = moment.tz(callDate3 + " " + meetingendtime3, mentorTimezone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                                                                                                }
                                                                                                privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "user", resultsPreviousSlot, callback);
                                                                                                privateInvitationUpdateMailToMentor(req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, "mentor", resultsPreviousSlot, callback);

                                                                                                res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                                res.write(JSON.stringify({
                                                                                                    "message": "success"
                                                                                                }));
                                                                                                res.end();
                                                                                            }
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};
exports.update_private_invitation = update_private_invitation;

var privateInvitationUpdateMailToMentor = function (req, res, mentor_id, user_id, mailStartDate1, mailStartDate2, mailStartDate3, mailEndDate1, mailEndDate2, mailEndDate3, userType, resultsPreviousSlot, callback) {

    var mentorId = mentor_id;
    var userId = user_id;
    var slotsInMail = [];
    var previousslotsInMailMentor = [];
    var previousslotsInMailUser = [];
    var slotsInMailObj1 = "";
    var slotsInMailObj2 = "";
    var slotsInMailObj3 = "";

    userModel.getUserInformation(req, res, userId, {}, function (req, res, userId, arrUserData, objParamsData) {
        userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
            getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
                var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
                if (mailStartDate1 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate1, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate1, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate1, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate1, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate1, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate1, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));
                    }
                }

                if (mailStartDate2 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate2, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate2, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate2, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate2, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate2, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate2, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));

                    }
                }

                if (mailStartDate3 != "") {
                    if (userType == "mentor") {
                        slotsInMail.push(moment.tz(mailStartDate3, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate3, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate3, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z'));
                    } else {
                        slotsInMail.push(moment.tz(mailStartDate3, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(mailStartDate3, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(mailEndDate3, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z'));

                    }
                }

                var invitationSlots = resultsPreviousSlot;

                resultsPreviousSlot.forEach(function (slot) {
                    var slotsMentorObj = {};
                    var slotsUserObj = {};
                    if (slot.status == 'pending') {
                        slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                        slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                        slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                        slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                        slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                        slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                        slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(slot.duration, "minutes").format('HH:mm');
                        slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(slot.duration, "minutes").format('HH:mm');
                        slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                        slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                        slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                        slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                        previousslotsInMailMentor.push(slotsMentorObj);
                        previousslotsInMailUser.push(slotsUserObj);
                    }
                });


                var previousSlots = "";
                var templateVariable = {};

                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
                var templete = "";
                var subject = "";
                if (userType == "user") {
                    templete = "mailtemplate/privateinviteupdateuser";
                    subject = "Hi " + userFullName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has updated a Private meeting slot sent to you earlier";
                    var email = arrUserData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateinviteupdateusermessage.handlebars', 'utf8');
                    req.body.lstUserList = arrUserData.user_id;
                } else {
                    templete = "mailtemplate/privateinviteupdatementor";
                    subject = "Hi " + arrMentorData.firstName + ", you have updated an earlier private invite slot sent to  " + userFullName;
                    var email = arrMentorData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateinviteupdatementormessage.handlebars', 'utf8');
                    req.body.lstUserList = arrMentorData.user_id;
                }

                if (userType == "mentor") {

                    templateVariable = {
                        templateURL: templete,
                        frontConstant: frontConstant,
                        arrSessionData: req.session,
                        arrMentorData: arrMentorData,
                        arrUserData: arrUserData,
                        mentorCancellationPolicy: mentorCancellationPolicy,
                        mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                        slotsInMail: slotsInMail,
                        userType: userType,
                        slots: previousslotsInMailMentor
                    };
                } else {

                    templateVariable = {
                        templateURL: templete,
                        frontConstant: frontConstant,
                        arrSessionData: req.session,
                        arrMentorData: arrMentorData,
                        arrUserData: arrUserData,
                        mentorCancellationPolicy: mentorCancellationPolicy,
                        mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                        slotsInMail: slotsInMail,
                        userType: userType,
                        slots: previousslotsInMailUser
                    };
                }

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: subject
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttomentor");
                    }
                });
                // Send message to mentor's inbox

                /* var context = {
                 frontConstant: frontConstant,
                 arrSessionData: req.session,
                 arrMentorData: arrMentorData,
                 arrUserData: arrUserData,
                 mentorCancellationPolicy: mentorCancellationPolicy,
                 slotsInMail: slotsInMail
                 
                 };*/
                var template = handlebars.compile(templateFile);
                var html = template(templateVariable);
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res, callback);
            });
        });
    });
};
exports.privateInvitationMailToMentor = privateInvitationMailToMentor;

var delete_private_invitation = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var invitation_id = reqBody.update_invitation_id;
    var user_id = reqBody.update_lstUserList;
    var mentor_id = req.session.userId;


    if (typeof invitation_id == "undefined" || invitation_id == "") {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"
        }));
        res.end();
    } else {
        var queryParam = [invitation_id];
        var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                ' FROM meeting_invitation_slot_mapping mis ' +
                ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                ' WHERE mis.invitation_id= ?';
        dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, resultsPreviousSlot, fields) {
            if (error) {
                console.log(error);
            } else {

                var queryParam = [invitation_id];
                // var strQuery = 'Delete From meeting_invitations where invitation_id = ?';
                var strQuery = 'UPDATE meeting_invitations SET status = "cancelled" ,updated_date = "' + constants.CURR_UTC_DATETIME() + '"  WHERE invitation_id = ?'
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        res.writeHead(400, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "error"
                        }));
                        res.end();
                    } else {
                        var queryParam = [invitation_id];
                        //var strQuery = 'Delete From meeting_invitation_slot_mapping where invitation_id = ?';
                        var strQuery = 'UPDATE meeting_invitation_slot_mapping SET status = "cancelled"  WHERE invitation_id = ?'
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                res.writeHead(400, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"
                                }));
                                res.end();
                            } else {
                                var queryParam = [invitation_id];
                                var strQuery = 'Delete From mentor_availability where parent_id = ?';
                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                    if (error) {
                                        res.writeHead(400, {'Content-Type': 'application/json'});
                                        res.write(JSON.stringify({
                                            "message": "error"
                                        }));
                                        res.end();
                                    } else {
                                        res.writeHead(200, {'Content-Type': 'application/json'});
                                        res.write(JSON.stringify({
                                            "message": "success"
                                        }));
                                        res.end();

                                        privateInvitationDeleteMailToMentor(req, res, mentor_id, user_id, "user", resultsPreviousSlot, callback);
                                        privateInvitationDeleteMailToMentor(req, res, mentor_id, user_id, "mentor", resultsPreviousSlot, callback);


                                        var objDataParamsForUpdateInvoice = {
                                            invitationId: invitation_id,
                                            meetingUserId: user_id,
                                            meetingMentorId: mentor_id
                                        };
                                        paymentModel.updateInvoiceForNormalInviteDeclineByMentor(req, res, objDataParamsForUpdateInvoice, function (req, res, objResponseData) {
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};
exports.delete_private_invitation = delete_private_invitation;

var privateInvitationDeleteMailToMentor = function (req, res, mentor_id, user_id, userType, resultsPreviousSlot, callback) {

    var mentorId = mentor_id;
    var userId = user_id;
    var slotsInMail = [];
    var previousslotsInMailMentor = [];
    var previousslotsInMailUser = [];
    var slotsInMailObj1 = "";
    var slotsInMailObj2 = "";
    var slotsInMailObj3 = "";

    userModel.getUserInformation(req, res, userId, {}, function (req, res, userId, arrUserData, objParamsData) {
        userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
            getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
                var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                });
                var invitationSlots = resultsPreviousSlot;

                resultsPreviousSlot.forEach(function (slot) {
                    var slotsMentorObj = {};
                    var slotsUserObj = {};
                    if (slot.status == 'pending') {
                        slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                        slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                        slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                        slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                        slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                        slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                        slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(slot.duration, "minutes").format('HH:mm');
                        slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(slot.duration, "minutes").format('HH:mm');
                        slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                        slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                        slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                        slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(slot.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                        previousslotsInMailMentor.push(slotsMentorObj);
                        previousslotsInMailUser.push(slotsUserObj);
                    }
                });


                var previousSlots = "";
                var templateVariable = {};

                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
                var templete = "";
                var subject = "";
                if (userType == "user") {
                    templete = "mailtemplate/privateinvitedeleteuser";
                    subject = "Hi " + userFullName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has cancelled a private invite slot sent to before your acceptance.";
                    var email = arrUserData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateinvitedeleteusermessage.handlebars', 'utf8');
                    req.body.lstUserList = arrUserData.user_id;
                } else {
                    templete = "mailtemplate/privateinvitedeletementor";
                    subject = "Hi " + arrMentorData.firstName + ", you have cancelled a private invite sent to   " + userFullName + " before acceptance";
                    var email = arrMentorData.email;
                    var templateFile = fs.readFileSync('./views/mailtemplate/privateinvitedeletementormessage.handlebars', 'utf8');
                    req.body.lstUserList = arrMentorData.user_id;
                }

                if (userType == "mentor") {

                    templateVariable = {
                        templateURL: templete,
                        frontConstant: frontConstant,
                        arrSessionData: req.session,
                        arrMentorData: arrMentorData,
                        arrUserData: arrUserData,
                        mentorCancellationPolicy: mentorCancellationPolicy,
                        mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                        slotsInMail: slotsInMail,
                        userType: userType,
                        slots: previousslotsInMailMentor
                    };
                } else {

                    templateVariable = {
                        templateURL: templete,
                        frontConstant: frontConstant,
                        arrSessionData: req.session,
                        arrMentorData: arrMentorData,
                        arrUserData: arrUserData,
                        mentorCancellationPolicy: mentorCancellationPolicy,
                        mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                        slotsInMail: slotsInMail,
                        userType: userType,
                        slots: previousslotsInMailUser
                    };
                }

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: subject
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttomentor");
                    }
                });
                // Send message to mentor's inbox

                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    slots: previousslotsInMailUser

                };
                var template = handlebars.compile(templateFile);
                var html = template(templateVariable);
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res, callback);
            });
        });
    });
};
exports.privateInvitationDeleteMailToMentor = privateInvitationDeleteMailToMentor;

var genratePromoCode_model = function (req, res, callback) {

    var promoCode = "";

    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 12; i++) {
        promoCode += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    checkRandomKeyGeneration(req, res, promoCode, function (req, res, objData) {
        if (objData) {
            genratePromoCode_model(req, res, callback);
        } else {
            var dataToSend = {
                flagfound: false,
                promoCode: promoCode
            }
            callback(req, res, dataToSend);
        }
    });
};
exports.genratePromoCode_model = genratePromoCode_model;

var checkRandomKeyGeneration = function (req, res, promoCode, callback) {

    var strQuery = 'SELECT * FROM promo_codes where promo_code = ?';
    dbconnect.executeQuery(req, res, strQuery, [promoCode], function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, '');
        } else {
            if (results.length > 0) {
                callback(req, res, true);
            } else {
                callback(req, res, false);
            }
        }
    });

};
exports.checkRandomKeyGeneration = checkRandomKeyGeneration;

var addPromoCode_model = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var user_id = reqBody.lstUserList;
    var mentor_id = req.session.userId;
    var promocode = reqBody.inputpromocode;
    var discount = parseFloat(reqBody.discount);
    var promoCodeMsg = reqBody.promoCodeMsg;


    var queryParam = {
        promo_code_id: "DEFAULT",
        mentor_id: mentor_id,
        user_id: user_id,
        promo_code: promocode,
        discount: discount,
        description: promoCodeMsg,
        promo_code_status: 1,
        created_date: constants.CURR_UTC_DATETIME()
    };


    var strQuery = 'INSERT INTO promo_codes SET ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            promocodeMailToUser(req, res, mentor_id, user_id, promocode, discount, promoCodeMsg, callback);
        }
    });

};
exports.addPromoCode_model = addPromoCode_model;

var promocodeMailToUser = function (req, res, mentor_id, user_id, promocode, discount, promoCodeMsg, callback) {

    var mentorId = mentor_id;
    var userId = user_id;

    userModel.getUserInformation(req, res, userId, {}, function (req, res, userId, arrUserData, objParamsData) {
        userModel.getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
            getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {

                var previousSlots = "";
                var templateVariable = {};

                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
                var templete = "";
                var subject = "";

                templete = "mailtemplate/promocodemailtouser";
                subject = "Hi " + userFullName + ", you have received a promo code from " + arrMentorData.firstName + " " + arrMentorData.lastName;
                var email = arrUserData.email;



                templateVariable = {
                    templateURL: templete,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    promocode: promocode,
                    discount: discount,
                    promoCodeMsg: promoCodeMsg
                };


                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: subject
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "success"
                        }));
                        res.end();
                    }
                });
                // Send message to mentor's inbox

                var msg = "";
                if (promoCodeMsg != "") {
                    msg = arrMentorData.firstName + ' mentions "' + promoCodeMsg + '"';
                }

                var templateFile = fs.readFileSync('./views/mailtemplate/promocodemsgtouser.handlebars', 'utf8');
                var context = {
                    templateURL: templete,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    promocode: promocode,
                    discount: discount,
                    promoCodeMsg: msg

                };
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.lstUserList = arrUserData.user_id;
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res, callback);
            });
        });
    });
};
exports.promocodeMailToUser = promocodeMailToUser;

var getPromoCodeData_model = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentor_id = req.session.userId;


    var queryParam = [mentor_id];


    var strQuery = ' SELECT pc.promo_code_id,pc.discount,pc.promo_code, ' +
            ' pc.promo_code_status,pc.is_used, ' +
            ' CONCAT(ud.first_name," ",ud.last_name) as name, ' +
            ' DATE_FORMAT(pc.created_date,"%d %M %Y") as created_date ' +
            ' FROM promo_codes pc ' +
            ' LEFT JOIN user_details ud ON pc.user_id = ud.user_id ' +
            ' WHERE pc.mentor_id = ? AND promo_code_status = 1';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"
            }));
            res.end();
        }
    });

};
exports.getPromoCodeData_model = getPromoCodeData_model;

var deletePromoCode_model = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var id = reqBody.id;

    var queryParam = [id];


    var strQuery = 'UPDATE promo_codes SET promo_code_status = 0 where promo_code_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"
            }));
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "success"
            }));
            res.end();
        }
    });

};
exports.deletePromoCode_model = deletePromoCode_model;

var updateUserToMentor = function (req, res, callback) {
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [currentUTC, req.session.userId];
    var strQuery = ' UPDATE users u, mentor_details md SET u.user_type = u.temp_user_type, u.updated_date=?, md.hide_from_search = 0, md.hide_calendar = 0 WHERE u.user_id = ? AND u.user_id = md.user_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, true);
        }
    });
}
exports.updateUserToMentor = updateUserToMentor;

var getTransectionData_model = function (req, res, callback) {

    var queryParam = [req.session.userId];
    var strQuery = ' Select *,FORMAT(ABS(amount),2) amount,UCASE(payment_type) payment_type,DATE_FORMAT(created_date, "' + constants.MYSQL_DB_DATETIME + '") created_date from wallet_transection WHERE user_id = ? ORDER BY created_date DESC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            getMsgForShortCode(req, res, results, 0, [], function (req, res, objData) {
                getTypeInPresent(req, res, objData, 0, [], function (req, res, objFinalData) {
                    callback(req, res, objFinalData);
                });
            });
        }
    });
};
exports.getTransectionData_model = getTransectionData_model;

var getTransectionDataAdmin_model = function (req, res, id, callback) {

    var queryParam = [id];
    var strQuery = ' Select *,FORMAT(ABS(amount),2) amount,UCASE(payment_type) payment_type,DATE_FORMAT(created_date, "%d %b %Y") created_date from wallet_transection WHERE user_id = ? ORDER BY created_date DESC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            getMsgForShortCode(req, res, results, 0, [], function (req, res, objData) {
                getTypeInPresent(req, res, objData, 0, [], function (req, res, objFinalData) {
                    callback(req, res, objFinalData);
                });
            });
        }
    });
};
exports.getTransectionDataAdmin_model = getTransectionDataAdmin_model;

var getTypeInPresent = function (req, res, arrMeetingData, counter, arrFinalDataSet, callback) {
    if (arrMeetingData.length > 0) {
        var thisResultSet = arrMeetingData[0];
        var reason = thisResultSet.payment_type;
        var str = "";

        if (reason != null) {
            switch (reason) {
                case "DEBITED":
                    str = "DEBIT";
                    break;

                case "REFUNDED":
                    str = "REFUND";
                    break;

                case "CREDITED":
                    str = "CREDIT";
                    break;
                case "VOIDED":
                    str = "VOID";
                    break;
                case "PAYOUT":
                    str = "PAYOUT";
                    break;
                case "":
                    str = "AUTH";
                    break;
                default:
                    console.log(reason);
                    break;
            }


            thisResultSet.payment_type = str;
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            getTypeInPresent(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        } else {
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            getTypeInPresent(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        }
    } else {
        callback(req, res, arrFinalDataSet);
    }
};
exports.getTypeInPresent = getTypeInPresent;

var getMsgForShortCode = function (req, res, arrMeetingData, counter, arrFinalDataSet, callback) {
    if (arrMeetingData.length > 0) {
        var thisResultSet = arrMeetingData[0];
        var reason = thisResultSet.reason;
        var created_date = thisResultSet.created_date;
        var str = "";

        if (reason != null) {
            switch (reason) {
                case "NOSHOWBYUSR":
                    str = "No Show By User";
                    break;

                case "NOSHOWBYBOTH":
                    str = "No Show By Both";
                    break;

                case "NOSHOWBYMEN":
                    str = "No Show By Mentor";
                    break;

                case "USRCANACCMEET":
                    str = "User Cancelled Accepted Meeting";
                    break;

                case "MENCANACCMEET":
                    str = "Mentor Cancelled Accepted Meeting";
                    break;

                case "NORMEETCAP":
                    str = "Mentor Accepted Meeting";
                    break;

                case "AUTHNORINVIMEET":
                    str = "Authorized Payment for Meeting";
                    break;

                case "AUTHPRIINVIMEET":
                    str = "Authorized Payment for Private Invite";
                    break;

                case "PRIMEETCAP":
                    str = "Private Invite Accepted";
                    break;

                case "MEETINVICANUSR":
                    str = "Sent Request Cancelled";
                    break;

                case "MEETINVIDECMTR":
                    str = "Sent Request Declined By Mentor";
                    break;

                case "MEETINVITIMEOUT":
                    str = "Meeting Invitation Timed-Out";
                    break;

                case "DECREQREFPAY":
                    str = "Refund Request Declined By Admin";
                    break;

                case "REQREFPAY":
                    str = "Refund Request Accepted By Admin";
                    break;
                case "WALLBALUPD":
                    str = "Wallet Balance Updated After Successful meeting";
                    break;
                case "PAYBYADMIN":
                    str = "Payout Done By Admin";
                    break;
                default:
                    console.log(reason);
                    break;
            }

            var mentorTimezone = req.session.timezone;
            var objConvertedDateTime = moment.tz(created_date, 'UTC').tz(mentorTimezone).format('MM-DD-YYYY HH:mm:ss');


            thisResultSet.reasonDisplay = str;
            thisResultSet.dateDisplay = objConvertedDateTime;
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            getMsgForShortCode(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        } else {
            arrMeetingData.splice(0, 1);
            arrFinalDataSet.push(thisResultSet);
            counter++;
            getMsgForShortCode(req, res, arrMeetingData, counter, arrFinalDataSet, callback);
        }
    } else {
        callback(req, res, arrFinalDataSet);
    }
};
exports.getMsgForShortCode = getMsgForShortCode;



var getWalletAmount_model = function (req, res, callback) {

    var queryParam = [req.session.userId];
    var strQuery = 'Select FORMAT(wallet_balance,2) as wallet_balance from mentor_details WHERE user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
}
exports.getWalletAmount_model = getWalletAmount_model;

var getYearlyIncome_model = function (req, res, callback) {

    var queryParam = [req.session.userId, req.session.userId, req.session.userId];
    //var strQuery = 'Select FORMAT(SUM(amount),2) amount from wallet_transection WHERE user_id = ? AND transection_type != "PAYAUTH" and created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 YEAR)';

    var strQuery = ' SELECT  ( SELECT FORMAT(( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection ' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "CREDITED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 YEAR)' +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "DEBITED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 YEAR) ' +
            ' ) + ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "REFUNDED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 YEAR) ' +
            ' ),2) diff ) as amount';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getYearlyIncome_model = getYearlyIncome_model;

var getMonthIncome_model = function (req, res, callback) {

    var queryParam = [req.session.userId, req.session.userId, req.session.userId];
    //var strQuery = 'Select FORMAT(SUM(amount),2) amount from wallet_transection WHERE user_id = ? AND transection_type != "PAYAUTH" and created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 MONTH)';

    var strQuery = ' SELECT  ( SELECT FORMAT(( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection ' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "CREDITED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 MONTH)' +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "DEBITED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 MONTH) ' +
            ' ) + ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "REFUNDED" AND created_date <  now() and created_date  > DATE_ADD(DATE(NOW()),INTERVAL -1 MONTH) ' +
            ' ),2) diff ) as amount';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
}
exports.getMonthIncome_model = getMonthIncome_model;

var getTotalIncome_model = function (req, res, callback) {

    var queryParam = [req.session.userId, req.session.userId, req.session.userId];
    // var strQuery = 'Select FORMAT(SUM(amount),2) amount from wallet_transection WHERE user_id = ? AND transection_type != "PAYAUTH" ';

    var strQuery = ' SELECT  ( SELECT FORMAT(( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection ' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "CREDITED"' +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "DEBITED" ' +
            ' ) + ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "REFUNDED" ' +
            ' ),2) diff ) as amount';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
}
exports.getTotalIncome_model = getTotalIncome_model;

var paypalEmail_model = function (req, res, callback) {

    var queryParam = [req.session.userId];
    var strQuery = 'Select paypal_email from mentor_details WHERE user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, "");
            }
        }
    });
};
exports.paypalEmail_model = paypalEmail_model;

var getTotalMoneySpent_model = function (req, res, callback) {

    var queryParam = [req.session.userId, req.session.userId, req.session.userId];
    //var strQuery = 'Select SUM(amount) amount from wallet_transection WHERE user_id = ? and payment_type = "DEBIT"';

    var strQuery = ' SELECT FORMAT(( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection ' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "DEBITED"' +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "CREDITED" ' +
            ' ) - ( ' +
            ' SELECT COALESCE(SUM(amount), 0 ) amount ' +
            ' FROM wallet_transection' +
            ' WHERE user_id = ? ' +
            ' AND payment_type = "REFUNDED" ' +
            ' ),2) diff';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.getTotalMoneySpent_model = getTotalMoneySpent_model;

var mentorContacted_model = function (req, res, callback) {

    var queryParam = [req.session.userId, req.session.userId];
    var strQuery = 'Select COUNT(*) as contacted from msg_conversation WHERE (message_sender_id = ? OR message_receiver_id = ?)';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.mentorContacted_model = mentorContacted_model;

var upComingMeeting_model = function (req, res, conType, callback) {

    var condition = "";

    if (conType == "month") {
        condition = " and  mis.date <  now() and mis.date  > DATE_ADD(DATE(NOW()),INTERVAL -1 MONTH) ";
    } else {
        condition = " and  YEARWEEK(mis.date, 1) = YEARWEEK(CURDATE(), 1) ";
    }

    var queryParam = [req.session.userId, req.session.userId, req.session.userId];
    var strQuery = "Select mi.*,ud.first_name, ud.last_name,ud.profile_image profileImage,DATE_FORMAT(mis.date,'%d') as meetDate,DATE_FORMAT(mis.date,'%b') as meetMonth " +
            " FROM meeting_invitations mi " +
            " LEFT JOIN user_details ud ON IF(mi.requester_id != ?,ud.user_id = mi.requester_id,ud.user_id = mi.user_id), " +
            " meeting_invitation_slot_mapping mis " +
            " WHERE mi.status = 'accepted' AND mis.status = 'accepted'" +
            " AND (mi.user_id = ?  OR mi.requester_id = ?) AND mis.invitation_id = mi.invitation_id " + condition + " ORDER BY CONCAT_WS(' ', mis.date, mis.time) ASC";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            setImagePathOfAllUsers(req, res, results, 0, [], function (req, res, MeetingData) {
                callback(req, res, MeetingData);
            });
        }
    });
};
exports.upComingMeeting_model = upComingMeeting_model;

var updatePaypalEmail_model = function (req, res, objParam, callback) {


    var paypalEmail = objParam.email;
    var paypalPhone = objParam.phone_number;

    var user_id = req.session.userId;
    console.log('user_id', user_id);
    var data = {
        paypal_email: paypalEmail,
        paypal_phone: paypalPhone
    };
    var queryParam = [data, user_id];
    var sqlQuery = ' UPDATE mentor_details SET ? WHERE user_id=? ';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, 'error');
        } else {
            callback(req, res, 'success');
        }
    });
};
exports.updatePaypalEmail_model = updatePaypalEmail_model;

var chkWeekAmt = function (req, res, objParams, callback) {

    var payoutAmount = objParams.payoutAmount;
    var user_id = objParams.user_id;
    var weekPayout = objParams.weekPayout;
    var param = [user_id];
    var finalObj = {};
    var strQuery = 'Select  SUM(amount) as amount,COUNT(payout_id) as maxCount ' +
            ' from mentor_payout_history ' +
            ' WHERE mentor_id = ? and YEARWEEK(created_date) = YEARWEEK(NOW())';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            if (results.length > 0) {
                var maxCount = results[0].maxCount;
                var objDataParams = {
                    systemSettingVariables: ['MWPYT']
                };
                miscFunction.getSystemSettingVariable(req, res, objDataParams, function (req, res, objSystemSettingResponse) {
                    if (objSystemSettingResponse != undefined) {
                        var maxAllPay = objSystemSettingResponse[0].settingValue;
                        if (maxCount < maxAllPay) {
                            var tot = results[0].amount;
                            var totAmt = parseFloat(tot) + parseFloat(payoutAmount);
                            var totsend = parseFloat(weekPayout) - parseFloat(totAmt);
                            if (totAmt >= weekPayout) {
                                finalObj = {
                                    msg: "stop",
                                    amt: totsend,
                                    wAmt: weekPayout,
                                    pcnt: 0,
                                    acnt: 0
                                };
                                callback(req, res, finalObj);
                            } else {
                                finalObj = {
                                    msg: "continue",
                                    amt: 0,
                                    wAmt: weekPayout,
                                    pcnt: 0,
                                    acnt: 0
                                };
                                callback(req, res, finalObj);
                            }
                        } else {
                            finalObj = {
                                msg: "stop",
                                amt: totsend,
                                wAmt: weekPayout,
                                pcnt: maxCount,
                                acnt: maxAllPay
                            };
                            callback(req, res, finalObj);
                        }
                    }
                });
            } else {
                finalObj = {
                    msg: "continue",
                    amt: 0,
                    wAmt: weekPayout,
                    pcnt: 0,
                    acnt: 0
                };
                callback(req, res, finalObj);
            }
        }
    });
};
exports.chkWeekAmt = chkWeekAmt;

var requestPayout_model = function (req, res, mentorDetails, callback) {
    var maxpayout;
    var queryParam = {
        mentor_id: req.session.userId,
        wallet_amount: mentorDetails.wallet_balance,
        request_amount: req.body.payoutAmount,
        created_date: constants.CURR_UTC_DATETIME()
    };

    var objParams = {};

    var objParam = {
        user_id: req.session.userId,
        payoutAmount: req.body.payoutAmount,
        weekPayout: req.body.weekPayout
    };

    chkWeekAmt(req, res, objParam, function (req, res, finalObj) {
        if (finalObj.msg == "continue") {
            //update insted of insert
            var strQuery = "insert into payout_request set ?";
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                } else {

                    var queryParam = {
                        mentor_id: req.session.userId,
                        amount: req.body.payoutAmount,
                        status: "1",
                        created_date: constants.CURR_UTC_DATETIME()
                    };

                    var strQuery = "insert into mentor_payout_history set ?";
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        } else {

                            var queryParam = [objParam.user_id]
                            var strQuery = ' SELECT ud.country_id ,ct.maxpayout' +
                                    ' FROM user_details ud ' +
                                    ' LEFT JOIN countries ct ON ud.country_id = ct.country_id ' +
                                    ' WHERE ud.user_id = ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                console.log('Hello user', results)
                                var maxpayoutamt = JSON.parse(JSON.stringify(results));
                                mentorDetails.maxpayout = maxpayoutamt[0].maxpayout
                                sendMailToMentor(req, res, mentorDetails, function () {
                                    sendMailToAdmin(req, res, mentorDetails, function () {
                                        var returnedObjA = {};
                                        returnedObjA.msg = "success";
                                        callback(req, res, returnedObjA);
                                    })
                                });
                            });
                        }

                    });
                }
            });
        } else {
            callback(req, res, finalObj);
        }
    });
};
exports.requestPayout_model = requestPayout_model;
var sendMailToMentor = function (req, res, mentorDetails, callback) {
    var full_name = mentorDetails.first_name + " " + mentorDetails.last_name;
    console.log('Hiii1')
    //function to send mail to mentor
    var templateVariable = {
        templateURL: "mailtemplate/paypalPayoutRequestMentor",
        // mentorpayoutmail
        userName: full_name,
        walBal: mentorDetails.wallet_balance,
        amount: req.body.payoutAmount,
        maxDayPayout: mentorDetails.maxpayout
    };

    console.log(mentorDetails.mentorMail);

    var mailParamsObject = {
        templateVariable: templateVariable,
        to: mentorDetails.mentorMail,
        subject: 'Hi ' + full_name + '  We have received your payout request.'
    };

    mailer.sendMail(req, res, mailParamsObject, function (err) {
        if (err) {
            console.log(err);
            console.log('errors', 'Error Occured while sending email.');
            //callback(req, res, 'error');

        } else {
            console.log('messages', 'sent');
            callback(true);

        }
    });
}
var sendMailToAdmin = function (req, res, mentorDetails, callback) {

    var full_name = mentorDetails.first_name + " " + mentorDetails.last_name;
    console.log('Hiii2')
    //function to send  mail to admin
    var templateVariable = {
        templateURL: "mailtemplate/payoutmailtoadmin",
        // mentorpayoutmail
        userName: full_name,
        walBal: mentorDetails.wallet_balance,
        amount: req.body.payoutAmount,
        maxDayPayout: req.body.payoutAmount
    };
    console.log(mentorDetails.mentorMail);
    var mailParamsObject = {
        templateVariable: templateVariable,
        to: frontConstant.ADMIN_EMAIL,
        subject: ' Hi Admin,  We have a payout request from ' + full_name + ' .'
    };

    mailer.sendMail(req, res, mailParamsObject, function (err) {
        if (err) {
            console.log(err);
            console.log('errors', 'Error Occured while sending email.');
            //callback(req, res, 'error');


        } else {
            console.log('messages', 'sent');
            callback(true)

        }
    });
}
var mentorDetailsforPayout = function (req, res, callback) {

    var queryParam = [req.session.userId];

    var strSelect = ' CONCAT_WS(" ", ud.first_name, ud.last_name) fullName, u.user_type userType, u.slug slug, u.email email, u.phone_number, u.status, u.user_id ,ct.minpayout,ct.maxpayout,ct.maxpayoutperweek ';

    var strQuery = ' SELECT ' + strSelect +
            ' FROM users u, user_details ud ' +
            ' LEFT JOIN mentor_details md ON ud.user_id = md.user_id ' +
            ' LEFT JOIN countries ct ON ud.country_id = ct.country_id ' +
            ' WHERE  u.user_id = ? AND u.user_id = ud.user_id AND  u.user_type = "mentor" AND u.user_id != "1"';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            callback(req, res, results[0]);
        }
    });

};
exports.mentorDetailsforPayout = mentorDetailsforPayout;

var removeEntery = function (req, res, callback) {

    var uid = req.body.uid;
    var pid = req.body.pid;
    var queryParam = [uid, pid];
    var sqlQuery = ' delete from participant_meeting_log where participant_id = ? AND participant_meeting_log_master_id = ?';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            callback(req, res, 'error');
        } else {
            callback(req, res, 'success');
        }
    });
};
exports.removeEntery = removeEntery;

var getMeetingUrl = function (req, res, id, callback) {
    var queryParam = [id];
    var strQuery = " SELECT meeting_url_key FROM meeting_details WHERE invitation_id=?";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        callback(req, res, results[0]);
    });
};
exports.getMeetingUrl = getMeetingUrl;