var exports = module.exports = {};
var dateFormat = require('dateformat');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var fs = require('fs');
var async = require("async");

var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var miscFunction = require('./../model/misc_model');

var frontConstant = require('./../modules/front_constant');


var getUserConversations_model = function (req, res, callback) {
    var userId = req.session.userId;
    var action = req.body.action;
    var searchKeyWord = req.body.searchKeyWord;
    var queryParams = [userId, userId, userId, userId, userId, userId, req.body.status];
    var sqlQuerySelect = 'u.slug,u.user_type,m.*,ud.user_id,mus.status, IFNULL(CONCAT(ud.first_name, " ", ud.last_name), "SourceAdvisor Admin") AS username, ud.profile_image AS profileImage, IF(m.message_sender_id = ?, m.message_receiver_id, m.message_sender_id) AS opponent_id, ' +
            ' (SELECT message_body FROM msg_replies WHERE conversation_id=m.conversation_id ORDER BY msg_replies_id DESC LIMIT 1) AS last_message, ' +
            //' (SELECT DATE_FORMAT(CONVERT_TZ(created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") created_date FROM msg_replies WHERE conversation_id=m.conversation_id ORDER BY msg_replies_id DESC LIMIT 1) AS last_message_datetime, ' +
            ' (SELECT DATE_FORMAT(created_date, "' + constants.MYSQL_DB_DATETIME_FORMAT + '") created_date FROM msg_replies WHERE conversation_id=m.conversation_id ORDER BY msg_replies_id DESC LIMIT 1) AS last_message_datetime, ' +
            ' (SELECT COUNT(msg_replies_id) FROM msg_replies WHERE status="unread" AND conversation_id=m.conversation_id AND reply_receiver_id=?) AS count_unread ';

    var sqlQueryFrom = ' msg_conversation m ' +
            ' LEFT JOIN msg_conv_user_status mus ON mus.conversation_id=m.conversation_id AND mus.user_id = ? ' +
            ' LEFT JOIN users u ON u.user_id = IF(m.message_sender_id = ?, m.message_receiver_id, m.message_sender_id) ' +
            ' LEFT JOIN user_details ud ON u.user_id = ud.user_id ' +
            ' LEFT JOIN msg_replies mr ON mr.conversation_id=m.conversation_id ';

    if (typeof searchKeyWord == 'undefined' || searchKeyWord == '') {
        var sqlQueryWHERE = '(m.message_sender_id=? OR m.message_receiver_id=?) AND mus.status=? ';
    } else {
        var sqlQueryWHERE = '(m.message_sender_id=? OR m.message_receiver_id=?) AND mus.status=? AND (ud.first_name like "%' + searchKeyWord + '%" OR ud.last_name like "%' + searchKeyWord + '%" OR mr.message_body like "%' + searchKeyWord + '%" ) ';
    }

    var sqlQueryGruopBy = ' GROUP BY mr.conversation_id ORDER BY last_message_datetime DESC';

    var sqlQuery = "SELECT " + sqlQuerySelect + " FROM " + sqlQueryFrom + " WHERE " + sqlQueryWHERE + sqlQueryGruopBy;

    if (typeof action != 'undefined') {
        sqlQuery += ' LIMIT 5';
    }


    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            mentorModel.setImagePathOfAllUsers(req, res, results, 0, [], callback);
        }
    });
};
exports.getUserConversations_model = getUserConversations_model;

var getConversationMessages_model = function (req, res, callback) {
    var userId = req.session.userId;
    var queryParams = [req.body.conversationId, userId];

    var sqlQuery = 'UPDATE msg_replies SET status = "read" WHERE conversation_id = ? AND reply_receiver_id = ?';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            var queryParams = [userId, req.body.conversationId, req.body.status];
            //var sqlQuery = ' SELECT mr.*, ma.file_name,DATE_FORMAT(CONVERT_TZ(mr.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") msg_sent_date' +
            var sqlQuery = ' SELECT mr.*, ma.file_name,mr.created_date msg_sent_date' +
                    ' FROM msg_replies mr ' +
                    ' LEFT JOIN msg_attachments ma ON mr.msg_replies_id=ma.msg_replies_id ' +
                    ' LEFT JOIN msg_conv_user_status mus ON mus.conversation_id=mr.conversation_id AND mus.user_id = ? ' +
                    ' WHERE mr.conversation_id = ? ' +
                    ' AND mus.status = ? ';

            if (req.body.status == 'inbox') {
                sqlQuery += ' AND mr.created_date >= mus.created_date ';
            } else if (req.body.status == 'deleted' || req.body.status == 'archived') {
                sqlQuery += ' AND mr.created_date BETWEEN mus.created_date AND mus.updated_date ';
            }
            sqlQuery += ' ORDER BY mr.msg_replies_id ASC';

            dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                } else {
                    mentorModel.setImagePathOfAllUsers(req, res, results, 0, [], callback);
                }
            });

        }
    });
};
exports.getConversationMessages_model = getConversationMessages_model;

var sendUserMessages = function (req, res, callback) {

    var reqBody = req.body;
    var fileInfo = "";
    var file_name = "";
    var file_type = "";
    var file_size = "";
    var msgBody = "";


    if (typeof req.files != 'undefined' && req.files != "") {
        fileInfo = req.files;
    } else {
        file_name = "";
    }
    var message_sender_id;
    // Check if sender id is set
    if (reqBody.sender_id != null) {
        message_sender_id = reqBody.sender_id;
    } else {
        message_sender_id = req.session.userId;
    }

    if (typeof reqBody.oppUserId == 'undefined' || reqBody.oppUserId == '') {
        var message_receiver_id = reqBody.lstUserList;
    } else {
        var message_receiver_id = reqBody.oppUserId;
    }

    if (typeof reqBody.msgBody == 'undefined' || reqBody.msgBody == '') {
        msgBody = reqBody.msgNewBody;
    } else {
        msgBody = reqBody.msgBody;
    }

    message_receiver_id = message_receiver_id;
    msgBody = msgBody;

    if (typeof msgBody == 'undefined' || msgBody == "" || msgBody == 'undefined') {
        if (fileInfo != "") {
            msgBody = "File Sent";
        }
    }


    if (message_sender_id != "1") {
        msgBody = msgBody.replace(/\n/g, '<br />');
    }


    //// req.checkBody('msgBody', 'Please enter Message').notEmpty();
    //req.checkBody('user_id', 'Please enter user id').notEmpty();
    //req.checkBody('mentor_id', 'Please enter mentorid').notEmpty();

    /* var errors = req.validationErrors();
     if (errors) {
     res.writeHead(400, {'Content-Type': 'application/json'});
     res.write(JSON.stringify({
     "message": "error"}));
     res.end();
     } else {*/
    var currentUTC = constants.CURR_UTC_DATETIME();
    //sessoin mentor id req.session.userId;

    var queryParam = [message_receiver_id, message_sender_id];

    var strQuery = 'SELECT * FROM msg_blocked_users WHERE user_id = ? and blocked_user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(error, {});
        } else {
            if (results.length > 0) {
                if (reqBody.sender_id == null) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "block"}));
                    res.end();
                } else {
                    console.log("user blocked");
                }

            } else {

                var queryParam = [message_sender_id, message_receiver_id];

                var strQuery = 'SELECT * FROM msg_blocked_users WHERE user_id = ? AND blocked_user_id = ?';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        callback(error, {});
                    } else {
                        if (results.length > 0) {
                            if (reqBody.sender_id == null) {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "blocked"}));
                                res.end();
                            } else {
                                console.log("user blocked");
                            }
                        } else {

                            var strQuery = 'SELECT * FROM msg_conversation WHERE message_sender_id =' + message_sender_id + ' AND message_receiver_id = ' + message_receiver_id + ' OR message_sender_id = ' + message_receiver_id + ' AND message_receiver_id =' + message_sender_id;
                            dbconnect.executeQuery(req, res, strQuery, '', function (req, res, error, results, fields) {
                                if (error) {
                                    console.log(error);
                                    if (reqBody.sender_id == null) {
                                        res.writeHead(400, {'Content-Type': 'application/json'});
                                        res.write(JSON.stringify({
                                            "message": "error"}));
                                        res.end();
                                    }
                                } else {

                                    if (results.length > 0) {
                                        var conversation_id = results[0].conversation_id;
                                        async.series([
                                            function (callback) {
                                                queryParam = [conversation_id, message_sender_id];
                                                var strQuery = 'SELECT * FROM msg_conv_user_status WHERE conversation_id = ? AND user_id = ? AND status = "inbox"';
                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        callback(error, {});
                                                    } else {

                                                        if (results.length == 0) {

                                                            var queryParam = {
                                                                conversation_id: conversation_id,
                                                                user_id: message_sender_id,
                                                                status: "inbox",
                                                                created_date: constants.CURR_UTC_DATETIME()
                                                            };
                                                            var strQuery = 'INSERT INTO msg_conv_user_status SET ?';
                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                if (error) {
                                                                    callback(error, {});
                                                                } else {
                                                                    callback(null, {});
                                                                }
                                                            });
                                                        } else {
                                                            callback(null, {});
                                                        }
                                                    }
                                                });


                                            },
                                            function (callback) {

                                                queryParam = [conversation_id, message_receiver_id];

                                                var strQuery = 'SELECT * FROM msg_conv_user_status WHERE conversation_id = ? AND user_id = ? AND status = "inbox"';
                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        callback(error, {});
                                                    } else {

                                                        if (results.length == 0) {

                                                            var queryParam = {
                                                                conversation_id: conversation_id,
                                                                user_id: message_receiver_id,
                                                                status: "inbox",
                                                                created_date: constants.CURR_UTC_DATETIME()
                                                            };
                                                            var strQuery = 'INSERT INTO msg_conv_user_status SET ?';
                                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                if (error) {
                                                                    callback(error, {});
                                                                } else {
                                                                    callback(null, {});
                                                                }
                                                            });

                                                        } else {
                                                            callback(null, {});
                                                        }
                                                    }
                                                });
                                            },
                                            function (callback) {

                                                var queryParam = {
                                                    conversation_id: conversation_id,
                                                    reply_receiver_id: message_receiver_id,
                                                    reply_sender_id: message_sender_id,
                                                    message_body: msgBody,
                                                    status: "unread",
                                                    created_date: constants.CURR_UTC_DATETIME()
                                                };
                                                var strQuery = 'INSERT INTO msg_replies SET ?';
                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        callback(error, {});
                                                    } else {
                                                        if (typeof req.files != 'undefined' && req.files != "") {
                                                            var looptime = req.files.length;
                                                            for (var i = 0; i < looptime; i++) {

                                                                file_name = req.files[i].filename;
                                                                file_type = req.files[i].mimetype;
                                                                file_size = req.files[i].size;

                                                                var queryParam = {
                                                                    msg_replies_id: results.insertId,
                                                                    file_name: file_name,
                                                                    file_type: file_type,
                                                                    file_size: file_size,
                                                                    is_deleted: "0",
                                                                    created_date: constants.CURR_UTC_DATETIME()
                                                                };
                                                                var strQuery = 'INSERT INTO msg_attachments SET ?';
                                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                    if (error) {
                                                                        callback(error, {});
                                                                    } else {
                                                                        // callback(null, {});
                                                                    }
                                                                });
                                                            }
                                                            callback(null, {});
                                                        } else {
                                                            callback(null, {});
                                                        }
                                                    }
                                                });
                                            }
                                        ], function (err, results) {
                                            if (err) {
                                                console.log(err);
                                                if (reqBody.sender_id == null)
                                                {
                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                    res.write(JSON.stringify({
                                                        "message": "error"}));
                                                    res.end();
                                                }
                                            } else {

                                                var queryParams = [message_sender_id];

                                                var sqlQuery = 'SELECT * FROM users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';

                                                dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        if (reqBody.sender_id == null)
                                                        {
                                                            throw error;
                                                        }
                                                    } else {

                                                        var sendername = "";

                                                        if (results.length > 0) {

                                                            sendername = results[0].first_name + " " + results[0].last_name;

                                                            if (msgBody == "File Sent") {
                                                                msgBody = "File Received";
                                                            }

                                                            var emailData = {
                                                                receiver_id: message_receiver_id,
                                                                message: msgBody,
                                                                sendername: sendername,
                                                                time: constants.CURR_UTC_DATETIME(),
                                                                user_brief: results[0].user_brief,
                                                                userSlug: results[0].slug
                                                            };

                                                            if (message_sender_id != "1") {
                                                                sendUserMessagesEmail(req, res, emailData);
                                                            }

                                                        }
                                                        if (reqBody.sender_id == null)
                                                        {
                                                            res.writeHead(200, {'Content-Type': 'application/json'});
                                                            res.write(JSON.stringify({
                                                                "message": "success"}));
                                                            res.end();
                                                        }

                                                    }
                                                });
                                            }
                                        });
                                    } else {

                                        var queryParam = {
                                            message_receiver_id: message_receiver_id,
                                            message_sender_id: message_sender_id,
                                            created_date: constants.CURR_UTC_DATETIME()
                                        };
                                        var strQuery = 'INSERT INTO msg_conversation SET ?';
                                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                            if (error) {
                                                if (reqBody.sender_id == null)
                                                {
                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                    res.write(JSON.stringify({
                                                        "message": "error"}));
                                                    res.end();
                                                } else
                                                {
                                                    console.log(error);
                                                }
                                            } else {
                                                var conversation_id = results.insertId;
                                                var queryParam = {
                                                    conversation_id: conversation_id,
                                                    reply_receiver_id: message_receiver_id,
                                                    reply_sender_id: message_sender_id,
                                                    message_body: msgBody,
                                                    status: "unread",
                                                    created_date: constants.CURR_UTC_DATETIME()
                                                            //created_date: moment.utc().add(1, 'm').format()
                                                };
                                                var strQuery = 'INSERT INTO msg_replies SET ?';
                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        if (reqBody.sender_id == null)
                                                        {
                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                            res.write(JSON.stringify({
                                                                "message": "error"}));
                                                            res.end();
                                                        } else
                                                        {
                                                            console.log(error);
                                                        }
                                                    } else {

                                                        var queryParam = {
                                                            conversation_id: conversation_id,
                                                            user_id: message_receiver_id,
                                                            status: "inbox",
                                                            created_date: constants.CURR_UTC_DATETIME()
                                                        };
                                                        var strQuery = 'INSERT INTO msg_conv_user_status SET ?';
                                                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                            if (error) {
                                                                if (reqBody.sender_id == null)
                                                                {
                                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                                    res.write(JSON.stringify({
                                                                        "message": "error"}));
                                                                    res.end();
                                                                } else
                                                                {
                                                                    console.log(error);
                                                                }
                                                            } else {

                                                                var queryParam = {
                                                                    conversation_id: conversation_id,
                                                                    user_id: message_sender_id,
                                                                    status: "inbox",
                                                                    created_date: constants.CURR_UTC_DATETIME()
                                                                };
                                                                var strQuery = 'INSERT INTO msg_conv_user_status SET ?';
                                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                                    if (error) {
                                                                        if (reqBody.sender_id == null)
                                                                        {
                                                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                                                            res.write(JSON.stringify({
                                                                                "message": "error"}));
                                                                            res.end();
                                                                        } else
                                                                        {
                                                                            console.log(error);
                                                                        }
                                                                    } else {

                                                                        var queryParams = [message_sender_id];

                                                                        var sqlQuery = 'SELECT * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';

                                                                        dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                                                                            if (error) {
                                                                                if (reqBody.sender_id == null)
                                                                                {
                                                                                    throw error;
                                                                                } else
                                                                                {
                                                                                    console.log(error);
                                                                                }
                                                                            } else {

                                                                                var sendername = "";

                                                                                if (results.length > 0) {

                                                                                    sendername = results[0].first_name + " " + results[0].last_name;
                                                                                    if (msgBody == "File Sent") {
                                                                                        msgBody = "File Received";
                                                                                    }
                                                                                    var emailData = {
                                                                                        receiver_id: message_receiver_id,
                                                                                        message: msgBody,
                                                                                        sendername: sendername,
                                                                                        time: constants.CURR_UTC_DATETIME(),
                                                                                        user_brief: results[0].user_brief,
                                                                                        userSlug: results[0].slug
                                                                                    };

                                                                                    if (message_sender_id != "1") {
                                                                                        sendUserMessagesEmail(req, res, emailData);
                                                                                    }

                                                                                }
                                                                                if (reqBody.sender_id == null)
                                                                                {
                                                                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                                                                    res.write(JSON.stringify({
                                                                                        "message": "success"}));
                                                                                    res.end();
                                                                                }

                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });

            }
        }
    });
    //}
};
exports.sendUserMessages = sendUserMessages;

var sendUserMessagesEmail = function (req, res, emailData) {
    console.log("emailData :: ", emailData);
    var queryParams = [emailData.receiver_id];

    var sqlQuery = 'Select * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {

        if (error) {
            throw error;
        } else {

            if (results.length > 0) {

                var email = results[0].email;
                //var fullname = results[0].first_name + " " + results[0].last_name;
                var fullname = results[0].first_name;
                var sendername = emailData.sendername;
                var msgBody = emailData.message;
                var userBreif = emailData.user_brief;
                var userSlug = emailData.userSlug;

                var templateVariable = {
                    templateURL: "mailtemplate/msgreviecedmail",
                    userName: fullname,
                    sendername: sendername,
                    msgBody: msgBody,
                    userBreif: userBreif,
                    frontConstant: frontConstant,
                    userSlug: userSlug
                };
                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: 'Hi ' + fullname + ', You have a message from ' + sendername
                };

                var mailerOptions = {
                    to: mailParamsObject.to,
                    subject: mailParamsObject.subject
                };

                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                        //req.flash('errors', 'Error Occured while sending email.');
                        // res.redirect('/login');
                    } else {
                        console.log("success");
                        // req.flash('messages', 'User registered successfully. Please check your email for verification.');
                        //res.redirect('/login');
                    }
                });
            }

        }
    });
};
exports.sendUserMessagesEmail = sendUserMessagesEmail;

var sendUserMessagesEmailAdmin = function (req, res, emailData) {
    console.log("emailData :: ", emailData);
    var queryParams = [emailData.receiver_id];

    var sqlQuery = 'Select * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {

        if (error) {
            throw error;
        } else {

            if (results.length > 0) {

                var email = results[0].email;
                //var fullname = results[0].first_name + " " + results[0].last_name;
                var fullname = results[0].first_name;
                var sendername = emailData.sendername;
                var msgBody = emailData.message;
                var userBreif = emailData.user_brief;
                var userSlug = emailData.userSlug;

                var templateVariable = {
                    templateURL: "mailtemplate/msgreviecedmailAdmin",
                    userName: fullname,
                    sendername: sendername,
                    msgBody: msgBody,
                    userBreif: userBreif,
                    frontConstant: frontConstant,
                    userSlug: userSlug
                };
                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: 'Hi ' + fullname + ', You have a message from ' + sendername
                };

                var mailerOptions = {
                    to: mailParamsObject.to,
                    subject: mailParamsObject.subject
                };

                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                        //req.flash('errors', 'Error Occured while sending email.');
                        // res.redirect('/login');
                    } else {
                        console.log("success");
                        // req.flash('messages', 'User registered successfully. Please check your email for verification.');
                        //res.redirect('/login');
                    }
                });
            }

        }
    });
};
exports.sendUserMessagesEmailAdmin = sendUserMessagesEmailAdmin;

var archiveMsg = function (req, res, callback) {
    var reqBody = req.body;

    var userId = req.session.userId;
    var conversationId = reqBody.id;
    var status = reqBody.status;
    if (status == "") {
        status = "archived";
    } else {
        status = "inbox";
    }
    var queryParams = [status, conversationId, userId];
    var updated_date = moment.utc().format("YYYY-MM-DD HH:mm:ss");

    var sqlQuery = 'Update msg_conv_user_status set status = ?,updated_date="' + updated_date + '" WHERE conversation_id = ? AND user_id = ? AND status != "deleted"';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "success"}));
            res.end();
        }
    });
};
exports.archiveMsg = archiveMsg;

var blockUser = function (req, res, callback) {
    var reqBody = req.body;

    var user_id = req.session.userId;
    var blocked_user_id = reqBody.id;
    var con_id = reqBody.con_id;
    var reason = reqBody.reason;

    var queryParam = [user_id, blocked_user_id];

    var strQuery = 'select * from msg_blocked_users where user_id = ? and blocked_user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(error, {});
        } else {
            if (results.length > 0) {

                var queryParams = [user_id, blocked_user_id];

                var sqlQuery = 'DELETE FROM msg_blocked_users WHERE user_id = ? AND blocked_user_id = ?';

                dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                    if (error) {
                        console.log(error);
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "error"}));
                        res.end();
                    } else {

                        var queryParams = [con_id, user_id];

                        var sqlQuery = 'Update msg_conv_user_status set status = "inbox" WHERE conversation_id = ? AND user_id = ? AND status != "deleted" ';

                        dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                            if (error) {
                                console.log(error);
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"}));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "success"}));
                                res.end();
                            }
                        });
                    }
                });

            } else {


                var queryParams = {
                    user_id: user_id,
                    blocked_user_id: blocked_user_id,
                    reason: reason,
                    created_date: moment.utc().format("YYYY-MM-DD HH:mm:ss")
                };

                var sqlQuery = 'INSERT INTO msg_blocked_users SET ?';

                dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                    if (error) {
                        console.log(error);
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "error"}));
                        res.end();
                    } else {

                        var queryParams = [con_id, user_id];
                        var updated_date = moment.utc().format("YYYY-MM-DD HH:mm:ss");

                        var sqlQuery = 'Update msg_conv_user_status set status = "blocked",updated_date="' + updated_date + '" WHERE conversation_id = ? AND user_id = ? AND status != "deleted"';

                        dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                            if (error) {
                                console.log(error);
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "error"}));
                                res.end();
                            } else {
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "message": "success"}));
                                res.end();
                            }
                        });
                    }
                });
            }
        }
    });
};
exports.blockUser = blockUser;

var deleteMsg = function (req, res, callback) {
    var reqBody = req.body;

    var userId = req.session.userId;
    var conversationId = reqBody.id;
    var status = reqBody.status;
    var queryParams = [status, conversationId, userId];
    var updated_date = moment.utc().format("YYYY-MM-DD HH:mm:ss");

    var sqlQuery = 'Update msg_conv_user_status set status = ?,updated_date="' + updated_date + '" WHERE conversation_id = ? AND user_id = ?';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "success"}));
            res.end();
        }
    });
};
exports.deleteMsg = deleteMsg;


var getAdminMessagesForLoggedInUser = function (req, res, callback) {
    var userId = req.session.userId;

    var queryParam = [userId];

    var strQuery = '    SELECT armm.admin_refund_messages_master_id adminRefundMessageMasterId, armm.refund_request_id refundRequestId, armm.refund_request_unq_id refundRequestUniqId, armt.admin_refund_messages_thread_id adminRefundMessageThreadId, armt.user_id userId, armt.typex userType, armt.unread_count unreadCount, arm.admin_refund_messages_id adminRefundMessageId, arm.message_from messageFrom, arm.message message,  ' +
            //'   (SELECT DATE_FORMAT(CONVERT_TZ(armSub.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") lastMessageSendDate FROM admin_refund_messages armSub WHERE armSub.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ORDER BY armSub.admin_refund_messages_id DESC LIMIT 1) AS lastMessageSendDate, ' +
            '   (SELECT DATE_FORMAT(armSub.created_date,"' + constants.MYSQL_DB_DATETIME_FORMAT + '") lastMessageSendDate FROM admin_refund_messages armSub WHERE armSub.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ORDER BY armSub.admin_refund_messages_id DESC LIMIT 1) AS lastMessageSendDate, ' +
            '   (SELECT armSub.message lastMessage FROM admin_refund_messages armSub WHERE armSub.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ORDER BY armSub.admin_refund_messages_id DESC LIMIT 1) AS lastMessage ' +
            '   FROM admin_refund_messages_master armm, admin_refund_messages_thread armt, admin_refund_messages arm ' +
            '   WHERE armt.user_id = ? ' +
            '   AND armt.admin_refund_messages_master_id = armm.admin_refund_messages_master_id ' +
            '   AND armt.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ' +
            '   GROUP BY armm.refund_request_id ' +
            '   ORDER BY lastMessageSendDate DESC ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getAdminMessagesForLoggedInUser = getAdminMessagesForLoggedInUser;

var getUsersRequestRefundAdminMessages = function (req, res, objDataParams, callback) {
    var adminRefundMessageId = objDataParams.adminRefundMessageId;
    var adminRefundMessageMasterId = objDataParams.adminRefundMessageMasterId;
    var adminRefundMessageThreadId = objDataParams.adminRefundMessageThreadId;
    var userId = objDataParams.userId;

    var queryParam = [userId, adminRefundMessageMasterId, adminRefundMessageThreadId];

    var strQuery = '    SELECT armm.admin_refund_messages_master_id adminRefundMessageMasterId, armm.refund_request_id refundRequestId, armm.refund_request_unq_id refundRequestUniqId, armt.admin_refund_messages_thread_id adminRefundMessageThreadId, armt.user_id userId, armt.typex userType, armt.unread_count unreadCount, arm.admin_refund_messages_id adminRefundMessageId, arm.message_from messageFrom, arm.message message, arm.created_date messageSendDate, ' +
            '   (SELECT DATE_FORMAT(CONVERT_TZ(armSub.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") lastMessageSendDate FROM admin_refund_messages armSub WHERE armSub.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ORDER BY armSub.admin_refund_messages_id DESC LIMIT 1) AS lastMessageSendDate, ' +
            '   (SELECT armSub.message lastMessage FROM admin_refund_messages armSub WHERE armSub.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ORDER BY armSub.admin_refund_messages_id DESC LIMIT 1) AS lastMessage ' +
            '   FROM admin_refund_messages_master armm, admin_refund_messages_thread armt, admin_refund_messages arm ' +
            '   WHERE armt.user_id = ? ' +
            '   AND armt.admin_refund_messages_master_id = armm.admin_refund_messages_master_id ' +
            '   AND armt.admin_refund_messages_master_id = ? ' +
            '   AND armt.admin_refund_messages_thread_id = arm.admin_refund_messages_thread_id ' +
            '   AND armt.admin_refund_messages_thread_id = ? ' +
            '   ORDER BY lastMessageSendDate ASC ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });

}
exports.getUsersRequestRefundAdminMessages = getUsersRequestRefundAdminMessages;


var updateMessageThreadCountToZero = function (req, res, objDataParams, callback) {
    var adminRefundMessageId = objDataParams.adminRefundMessageId;
    var adminRefundMessageMasterId = objDataParams.adminRefundMessageMasterId;
    var adminRefundMessageThreadId = objDataParams.adminRefundMessageThreadId;
    var userId = objDataParams.userId;

    var queryParam = [adminRefundMessageThreadId];
    var strQuery = " UPDATE admin_refund_messages_thread SET unread_count = 0 WHERE admin_refund_messages_thread_id = ? ";
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {
            callback(req, res, true);
        }
    });
};
exports.updateMessageThreadCountToZero = updateMessageThreadCountToZero;


var sendUserMessageToAdmin = function (req, res, callback) {

    var reqBody = req.body;

    var adminRefundMessageId = reqBody.adminRefundMessageId;
    var adminRefundMessageMasterId = reqBody.adminRefundMessageMasterId;
    var adminRefundMessageThreadId = reqBody.adminRefundMessageThreadId;
    var msgBody = reqBody.msgBody;

    var currentUTC = constants.CURR_UTC_DATETIME();

    msgBody = msgBody.replace(/\n/g, '<br />');

    var params = {
        admin_refund_messages_thread_id: adminRefundMessageThreadId,
        message_from: "U",
        message: msgBody,
        created_date: currentUTC
    };

    var strQuery = 'INSERT INTO admin_refund_messages SET ? ';

    dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, {status: false, currentUTCTime: currentUTC});
        } else {

            var emailData = {
                message: msgBody,
                time: constants.CURR_UTC_DATETIME(),
            };

            sendUserToAdminMessagesEmail(req, res, emailData, function (req, res, data) {
                callback(req, res, {status: true, currentUTCTime: currentUTC});
            });
        }
    });


}
exports.sendUserMessageToAdmin = sendUserMessageToAdmin;

var sendUserToAdminMessagesEmail = function (req, res, emailData, callback) {

    var queryParams = [req.session.userId];
    var sqlQuery = 'Select * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';

    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {

        if (error) {
            throw error;
        } else {

            if (results.length > 0) {

                var email = frontConstant.ADMIN_EMAIL;
                var sendername = results[0].first_name + " " + results[0].last_name;
                var fullname = "Admin";
                var msgBody = emailData.message;
                var userBreif = results[0].user_brief;
                var userSlug = results[0].userSlug;
                var template = "";
                if (req.session.userType == "user") {
                    template = "mailtemplate/msgreviecedmailusertoadmin";
                } else {
                    template = "mailtemplate/msgreviecedmailmentortoadmin";
                }

                var templateVariable = {
                    templateURL: template,
                    userName: fullname,
                    sendername: sendername,
                    msgBody: msgBody,
                    userBreif: userBreif,
                    frontConstant: frontConstant,
                    userSlug: userSlug
                };
                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: 'Hi Admin, You have a message from ' + sendername
                };

                var mailerOptions = {
                    to: mailParamsObject.to,
                    subject: mailParamsObject.subject
                };

                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                        callback(req, res, "err");
                        //req.flash('errors', 'Error Occured while sending email.');
                        // res.redirect('/login');
                    } else {
                        console.log("success");
                        callback(req, res, "success");
                        // req.flash('messages', 'User registered successfully. Please check your email for verification.');
                        //res.redirect('/login');
                    }
                });
            }

        }
    });
};
exports.sendUserToAdminMessagesEmail = sendUserToAdminMessagesEmail;



var sendContactMessages = function (req, res, callback) {
    var email = req.body.email;
    var sendername = req.body.name;
    var fullname = "Admin";
    var msgBody = req.body.comment;



    var templateVariable = {
        templateURL: "mailtemplate/sendcontactmsg",
        userName: fullname,
        sendername: sendername,
        frontConstant: frontConstant,
        comment: req.body.comment,
        email: req.body.email,
        number: req.body.number
    };
    
    var mailParamsObject = {
        templateVariable: templateVariable,
        to: email,
        subject: 'Hi Admin, You have a message from ' + sendername
    };

    var mailerOptions = {
        to: mailParamsObject.to,
        subject: mailParamsObject.subject
    };

    mailer.sendMail(req, res, mailParamsObject, function (err) {
        if (err) {
            console.log(err);
            callback(req, res, "err");
            //req.flash('errors', 'Error Occured while sending email.');
            // res.redirect('/login');
        } else {
            console.log("success");
            callback(req, res, "success");
            // req.flash('messages', 'User registered successfully. Please check your email for verification.');
            //res.redirect('/login');
        }
    });
};
exports.sendContactMessages = sendContactMessages;