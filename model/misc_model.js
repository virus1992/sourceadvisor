var dateFormat = require('dateformat');
var formidable = require('formidable');
var path = require('path');
var csv = require("fast-csv");
var Combinatorics = require('js-combinatorics');

var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var async = require("async");
var fs = require('fs');

var userModel = require('./../model/users_model');


var getCountryListFromDb = function (req, res, type, callback) {

    var conditon = "";

    if (type == "1") {
        conditon = "WHERE c.status != '2' ";
    } else {
        conditon = "WHERE c.status = '1' ";
    }

    var queryParam = {};
    var strQuery = ' SELECT c.region_id regionId, c.country_id countryId, c.country_name countryName, c.country_code countryCode, c.phone_code countryPhoneCode, c.wc_mentor_percentage wissenxMentorPercentage, c.wc_user_percentage wissenxUserPercentage' +
            ' FROM countries c ' +
            //' WHERE c.status = "1" ' +
            conditon +
            ' ORDER BY c.country_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getCountryList = getCountryListFromDb;

var getTimeZoneList = function (req, res, zone_id, callback) {

    var queryParam = {};
    var strQuery = ' SELECT z.zone_id zoneId, z.country_code countryCode, z.zone_name zoneName , z.display_name disaplayName,z.orderBy orderBy FROM zone z ';
    if (zone_id != '') {
        strQuery += ' WHERE z.zone_id = "' + zone_id + '" ';
    }
    strQuery += ' ORDER BY z.orderBy ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getTimeZoneList = getTimeZoneList;

var getAllLocationList = function (req, res, callback) {
    var qryString = req.param('term');

    var queryParam = {};
    var strQuery = ' SELECT c.country_id countryId, c.country_name countryName, c.country_code countryCode, c.phone_code countryPhoneCode, "C" typeX, "0" stateId, "0" stateCountryId, "" stateCode, "" stateName, c.country_id id, c.country_name value ' +
            ' FROM countries c ' +
            ' WHERE c.status = "1" ' +
            ' AND c.country_name LIKE "%' + qryString + '%" ' +
            ' ORDER BY c.country_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var arrCountryList = results;
        var queryParam = {};
        var strQuery = ' SELECT c.country_id countryId, c.country_name countryName, c.country_code countryCode, c.phone_code countryPhoneCode, "S" typeX, s.state_id stateId, s.country_id stateCountryId, s.state_name stateName, s.state_id id, s.state_name value ' +
                ' FROM states s, countries c ' +
                ' WHERE s.status = "1" ' +
                ' AND s.country_id = c.country_id ' +
                ' AND s.state_name LIKE "%' + qryString + '%" ' +
                ' ORDER BY c.state_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            var arrStateList = results;
            var arrFinalData = arrCountryList.concat(arrStateList);
            callback(req, res, arrFinalData);
        });
    });
};
exports.getAllLocationList = getAllLocationList;

var getLocationForSearch = function (req, res, callback) {
    var qryString = req.param('term');

    var queryParam = {};
    var strQuery = ' SELECT c.country_id locationId, c.country_name countryName, c.country_code countryCode, c.phone_code countryPhoneCode, "C" typeX, c.country_name value, c.country_id id ' +
            ' FROM countries c ' +
            ' WHERE c.status = "1" ' +
            ' AND c.country_name LIKE "%' + qryString + '%" '
    ' ORDER BY c.country_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrCountryResult, fields) {
        var arrCountryResult = arrCountryResult;

        var queryParam = {};
        var strQuery = ' SELECT mr.region_id locationId, "" countryName, "" countryCode, "" countryPhoneCode, "R" typeX, mr.region_name value, mr.region_id id ' +
                ' FROM master_region mr ' +
                ' WHERE mr.status = "1" ' +
                ' AND mr.region_name LIKE "%' + qryString + '%" '
        ' ORDER BY mr.region_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrZoneList, fields) {
            var arrZoneResult = arrZoneList;
            arrFinalData = arrCountryResult.concat(arrZoneResult);
            if (arrFinalData.length > 0) {
                callback(req, res, arrFinalData);
            } else {
                var noDataFound = {
                    "locationId": "",
                    "countryName": "",
                    "countryCode": "",
                    "countryPhoneCode": "",
                    "typeX": "NR",
                    "value": "No search result found, pls try again…",
                    "id": "0"
                };
                callback(req, res, [noDataFound]);
            }
        });
    });

}
exports.getLocationForSearch = getLocationForSearch;

var getCityListFromCountryId = function (req, res, countryId, callback) {
    if (typeof countryId == 'undefined' || !(countryId > 0) || isNaN(countryId)) {
        callback(req, res, {});
    }

    var queryParam = [countryId];
    var strQuery = ' SELECT c.city_id cityId, c.country_id countryId, c.state_id stateId, c.city_name cityName, c.status status ' +
            ' FROM cities c ' +
            ' WHERE c.status = "1" ' +
            ' AND c.country_id = ?' +
            ' ORDER BY c.city_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}
exports.getCityListFromCountryId = getCityListFromCountryId;



var searchCityListFromCountryId = function (req, res, countryId, searchStr, callback) {

    if (typeof countryId == 'undefined' || !(countryId > 0) || isNaN(countryId)) {
        callback(req, res, {});
    }

    var queryParam = [countryId];

    var strQuery = ' SELECT c.city_id cityId, c.country_id countryId, c.state_id stateId, c.city_name cityName, c.status status, c.city_id data, CONCAT_WS(", ", s.state_name, c.city_name) value, IF(c.city_name LIKE "' + searchStr + '", 1, 0) exactSearch, IF(c.city_name LIKE "' + searchStr + '%", 1, 0) firstLetter, IF(c.city_name LIKE "%' + searchStr + '%", 1, 0) inBetween, IF(c.city_name LIKE "%' + searchStr + '", 1, 0) lastSearch ' +
            ' FROM cities c, states s ' +
            ' WHERE c.status = "1" ' +
            ' AND s.status = "1" ' +
            ' AND c.state_id = s.state_id ' +
            ' AND s.country_id = c.country_id ' +
            ' AND c.country_id = ?' +
            ' AND c.city_name LIKE "%' + searchStr + '%" ' +
            ' ORDER BY exactSearch DESC, firstLetter DESC, inBetween DESC , lastSearch DESC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}
exports.searchCityListFromCountryId = searchCityListFromCountryId;



var getIndustryListFromDb = function (req, res, callback) {

    var queryParam = {};
    var strQuery = ' SELECT * ' +
            ' FROM master_industry ' +
            ' WHERE status = "1" ' +
            ' ORDER BY industry_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getIndustryListFromDb = getIndustryListFromDb;

var getRegionListFromDb = function (req, res, callback) {

    var queryParam = {};
    var strQuery = ' SELECT * ' +
            ' FROM master_region ' +
            ' WHERE status = "1" ' +
            ' ORDER BY region_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getRegionListFromDb = getRegionListFromDb;

var getSubIndustryListFromDb = function (req, res, callback) {

    var queryParam = {};
    var strQuery = ' SELECT * ' +
            ' FROM master_subindustry ' +
            ' WHERE status = "1" ' +
            ' ORDER BY subindustry_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getSubIndustryListFromDb = getSubIndustryListFromDb;

var getDomainListFromDb = function (req, res, callback) {

    var queryParam = {};
    var strQuery = ' SELECT * ' +
            ' FROM master_domain ' +
            ' WHERE status = "1" ' +
            ' ORDER BY domain_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
};
exports.getDomainListFromDb = getDomainListFromDb;

var getTimeZoneInfoFromZoneName = function (req, res, timeZoneLabel, callback) {

    if (typeof timeZoneLabel == 'undefined') {
        callback(req, res, {});
    } else {

        var queryParam = [timeZoneLabel];
        var strQuery = ' SELECT z.zone_id timezoneId, z.country_code timezoneCountryCode, z.zone_name timeZoneName ' +
                ' FROM zone z ' +
                ' WHERE z.zone_name = ? ';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, {});
            }
        });
    }
};
exports.getTimeZoneInfoFromZoneName = getTimeZoneInfoFromZoneName;

var getTimeZoneInfo = function (req, res, callback) {
    var queryParam = [];
    var strQuery = ' SELECT z.zone_id timezoneId, z.country_code timezoneCountryCode, z.zone_name timeZoneName, z.display_name timeZoneDisplayName,z.orderBy orderBy  ' +
            ' FROM zone z order by z.orderBy ASC';


    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getTimeZoneInfo = getTimeZoneInfo;



/*=================================================================================================================================*/
/*======================================================== KEY WORD SEARCH START ==================================================*/
/*=================================================================================================================================*/
var getSearchQueryStringData = function (req, res, callback) {
    var qryString = req.param('term');

    async.parallel([
        function (callback) {
            // getting industry list.
            getIndustryForSearch(req, res, qryString, callback);
        },
        function (callback) {
            // getting sub industry list.
            getSubIndustryForSearch(req, res, qryString, callback);
        },
        function (callback) {
            // getting sub industry list.
            getDomainForSearch(req, res, qryString, callback);
        },
        function (callback) {
            // getting list from mentors first name.
            getMentorAndOrganizationFromFirstNameForSearch(req, res, qryString, callback);
        },
        function (callback) {
            // getting list from mentors last name.
            getMentorAndOrganizationFromLastNameForSearch(req, res, qryString, callback);
        },
        function (callback) {
            //callback(null, []);
            // getting list from mentors expertise.
            getMentorAndOrganizationFromMentorExpertiseForSearch(req, res, qryString, callback);
        },
        function (callback) {
            // getting list from mentors last name.
            getMentorAndOrganizationFromFirstAndLastNameForSearch(req, res, qryString, callback);
        }
    ],
            // optional callback
                    function (err, results) {

                        var arrFinalArray = [];
                        if (results.length > 0) {
                            results.forEach(function (arrEachItem) {
                                if (arrEachItem.length > 0) {
                                    arrEachItem.forEach(function (individualElement) {
                                        arrFinalArray.push(individualElement);
                                    });
                                }
                            });

                            if (arrFinalArray.length > 0) {
                                callback(req, res, arrFinalArray);
                            } else {
                                var emptyObj = {
                                    "id": "No search result found, pls try again…",
                                    "value": "No search result found, pls try again…",
                                    "tokenTypeLabel": "",
                                    "tokenTypeX": "NR",
                                    "user_id": 0
                                };
                                callback(req, res, [emptyObj]);
                            }
                        } else {
                            var emptyObj = {
                                "id": "No search result found, pls try again…",
                                "value": "No search result found, pls try again…",
                                "tokenTypeLabel": "",
                                "tokenTypeX": "NR",
                                "user_id": 0
                            };
                            callback(req, res, [emptyObj]);
                        }
                    });
        }
exports.getSearchQueryStringData = getSearchQueryStringData;


var getMentorAndOrganizationFromFirstNameForSearch = function (req, res, qryString, callback) {
    var queryParam = {};
    var strQuery = ' SELECT ud.first_name id, ud.first_name value, "First Name" tokenTypeLabel, "FN" tokenTypeX, u.user_id  ' +
            ' FROM users u, user_details ud ' +
            ' WHERE u.user_id = ud.user_id ' +
            ' AND u.status = "1" ' +
            ' AND u.user_type IN ("mentor", "organization") ' +
            ' AND ud.first_name LIKE "%' + qryString + '%" ' +
            ' GROUP BY ud.first_name' +
            ' ORDER BY ud.first_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorAndOrganizationFirstResult, fields) {
        callback(null, arrMentorAndOrganizationFirstResult);
    });
}
exports.getMentorAndOrganizationFromFirstNameForSearch = getMentorAndOrganizationFromFirstNameForSearch;


var getMentorAndOrganizationFromFirstAndLastNameForSearch = function (req, res, qryString, callback) {

    var arrQueryString = qryString.split(" ");

    arrQueryString = arrQueryString.filter(function (word) {
        return word.trim() != '';
    });

    var objPermutationAndCombinations = Combinatorics.permutationCombination(arrQueryString);
    var arrPermutationAndCombinations = objPermutationAndCombinations.toArray();

    var newGeneratedString = "";
    var flagFirst = true;
    if (arrPermutationAndCombinations.length > 0) {

        async.each(arrPermutationAndCombinations, function (eachQueryArray, asyncCallback) {
            var eachPAndCStr = eachQueryArray.join(" ");

            if (flagFirst) {
                newGeneratedString += ' CONCAT_WS(" ", ud.first_name, ud.last_name) LIKE "%' + eachPAndCStr + '%"';
                flagFirst = false;
            } else {
                newGeneratedString += ' OR CONCAT_WS(" ", ud.first_name, ud.last_name) LIKE "%' + eachPAndCStr + '%"';
            }

            asyncCallback();

        }, function (error) {
            console.log("newGeneratedString :: ", newGeneratedString);
        });
    }

    if (newGeneratedString != "") {
        var queryParam = {};
        var strQuery = ' SELECT CONCAT_WS(" ", ud.first_name, ud.last_name) id, CONCAT_WS(" ", ud.first_name, ud.last_name) value, "Full Name" tokenTypeLabel, "FULLNAME" tokenTypeX, u.user_id  ' +
                ' FROM users u, user_details ud ' +
                ' WHERE u.user_id = ud.user_id ' +
                ' AND u.status = "1" ' +
                ' AND u.user_type IN ("mentor", "organization") ' +
                ' AND ( ' + newGeneratedString + ' ) ' +
                ' GROUP BY ud.first_name' +
                ' ORDER BY ud.first_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorAndOrganizationFirstResult, fields) {
            callback(null, arrMentorAndOrganizationFirstResult);
        });
    } else {
        callback(null, []);
    }
}
exports.getMentorAndOrganizationFromFirstAndLastNameForSearch = getMentorAndOrganizationFromFirstAndLastNameForSearch;


var getMentorAndOrganizationFromLastNameForSearch = function (req, res, qryString, callback) {
    var queryParam = {};
    var strQuery = ' SELECT ud.last_name id, ud.last_name value, "Last Name" tokenTypeLabel, "LN" tokenTypeX  ' +
            ' FROM users u, user_details ud ' +
            ' WHERE u.user_id = ud.user_id ' +
            ' AND u.status = "1" ' +
            ' AND u.user_type IN ("mentor", "organization") ' +
            ' AND ud.last_name LIKE "%' + qryString + '%" ' +
            ' GROUP BY ud.last_name' +
            ' ORDER BY ud.last_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorAndOrganizationLastNameResult, fields) {
        callback(null, arrMentorAndOrganizationLastNameResult);
    });
}
exports.getMentorAndOrganizationFromLastNameForSearch = getMentorAndOrganizationFromLastNameForSearch;


var getMentorAndOrganizationFromMentorExpertiseForSearch = function (req, res, qryString, callback) {
    var queryParam = {};
    var strQuery = ' SELECT mot.tag_name id, mot.tag_name value, "Expertise" tokenTypeLabel, "ME" tokenTypeX  ' +
            ' FROM mentor_organisation_tags mot, users u ' +
            ' WHERE mot.tag_name LIKE "%' + qryString + '%" ' +
            ' AND mot.user_id = u.user_id ' +
            ' AND u.status = 1 ' +
            ' GROUP BY mot.tag_name ' +
            ' ORDER BY mot.tag_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorAndOrganizationMentorExpertiseResult, fields) {
        callback(null, arrMentorAndOrganizationMentorExpertiseResult);
    });
}
exports.getMentorAndOrganizationFromMentorExpertiseForSearch = getMentorAndOrganizationFromMentorExpertiseForSearch;

var getIndustryForSearch = function (req, res, qryString, callback) {

    var queryParam = {};
    var strQuery = ' SELECT mi.industry_id id, mi.industry_name value, "Industry" tokenTypeLabel, "I" tokenTypeX ' +
            ' FROM master_industry mi ' +
            ' WHERE mi.status = "1" ' +
            ' AND mi.industry_name LIKE "%' + qryString + '%" ' +
            ' ORDER BY mi.industry_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrIndustryRecords, fields) {
        callback(null, arrIndustryRecords);
    });
}
exports.getIndustryForSearch = getIndustryForSearch;

var getDomainForSearch = function (req, res, qryString, callback) {

    var queryParam = {};
    var strQuery = ' SELECT md.master_domain_id id, md.domain_name value, "Domain" tokenTypeLabel, "D" tokenTypeX ' +
            ' FROM master_domain md ' +
            ' WHERE md.status = "1" ' +
            ' AND md.domain_name LIKE "%' + qryString + '%" ' +
            ' ORDER BY md.domain_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryRecords, fields) {
        callback(null, arrSubIndustryRecords);
    });
}
exports.getDomainForSearch = getDomainForSearch;


var getSubIndustryForSearch = function (req, res, qryString, callback) {

    var queryParam = {};
    var strQuery = ' SELECT msi.subindustry_id id, msi.subindustry_name value, "Sub Industry" tokenTypeLabel, "SI" tokenTypeX ' +
            ' FROM master_subindustry msi LEFT JOIN master_subindustry_keyword msk ON msk.subindustry_id = msi.subindustry_id AND msk.status = "1" ' +
            ' WHERE msi.status = "1" ' +
            ' AND ( msi.subindustry_name LIKE "%' + qryString + '%" OR msk.keyword LIKE "%' + qryString + '%") ' +
            ' GROUP BY msi.subindustry_id ' +
            ' ORDER BY msi.subindustry_name ASC ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(null, arrDomainRecords);
    });
}
exports.getSubIndustryForSearch = getSubIndustryForSearch;
/*=================================================================================================================================*/
/*========================================================= KEY WORD SEARCH END ===================================================*/
/*=================================================================================================================================*/





var getDomainInformationFromIds = function (req, res, domainKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("DOMAIN_",md.master_domain_id) id, md.domain_name value, "DOMAIN" typeX' +
            ' FROM master_domain md ' +
            ' WHERE md.status = "1" ' +
            ' AND md.master_domain_id IN (' + domainKeys + ') ';
    ' ORDER BY md.domain_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(null, arrDomainRecords);
    });
}
exports.getDomainInformationFromIds = getDomainInformationFromIds;


var getIndustryInformationFromIds = function (req, res, industryKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("INDUSTRY_", mi.industry_id) id, mi.industry_name value, "INDUSTRY" typeX ' +
            ' FROM master_industry mi ' +
            ' WHERE mi.status = "1" ' +
            ' AND mi.industry_id IN (' + industryKeys + ')' +
            ' ORDER BY mi.industry_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrIndustryRecords, fields) {
        callback(null, arrIndustryRecords);
    });
}
exports.getIndustryInformationFromIds = getIndustryInformationFromIds;


var getSubIndustryInformationFromIds = function (req, res, subIndustryKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("SUBINDUSTRY_", msi.subindustry_id) id, msi.subindustry_name value, "SUBINDUSTRY" typeX' +
            ' FROM master_subindustry msi ' +
            ' WHERE msi.status = "1" ' +
            ' AND msi.subindustry_id IN (' + subIndustryKeys + ') '
    ' ORDER BY msi.subindustry_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(null, arrDomainRecords);
    });
}
exports.getSubIndustryInformationFromIds = getSubIndustryInformationFromIds;



var getMentorAndOrganizationInformationFromIds = function (req, res, mentorKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("MENTOR_", u.user_id) id, CONCAT_WS(" ", ud.first_name, ud.last_name) value, "MENTOR" typeX' +
            ' FROM users u, user_details ud ' +
            ' WHERE u.user_id = ud.user_id ' +
            ' AND u.status = "1" ' +
            ' AND u.user_id IN (' + mentorKeys + ') '
    ' ORDER BY ud.first_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(null, arrDomainRecords);
    });
}
exports.getMentorAndOrganizationInformationFromIds = getMentorAndOrganizationInformationFromIds;


var getMentorTagsInformationFromIds = function (req, res, subIndustryKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("TAG_", mt.mentor_org_tag_id) id, mt.tag_name value, "TAG" typeX' +
            ' FROM mentor_organisation_tags mt ' +
            ' WHERE mt.mentor_org_tag_id IN (' + subIndustryKeys + ') '
    ' ORDER BY mt.subindustry_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(null, arrDomainRecords);
    });
}
exports.getMentorTagsInformationFromIds = getMentorTagsInformationFromIds;



var getCityTagsInformationFromIds = function (req, res, cityKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("CITY_", c.city_id) id, c.city_name value, "CITY" typeX' +
            ' FROM cities c ' +
            ' WHERE c.city_id IN (' + cityKeys + ') '
    ' ORDER BY c.city_id ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrCityRecords, fields) {
        callback(null, arrCityRecords);
    });
}
exports.getCityTagsInformationFromIds = getCityTagsInformationFromIds;


var getCountryTagsInformationFromIds = function (req, res, cityKeys, callback) {
    var queryParam = {};
    var strQuery = ' SELECT CONCAT("COUNTRY_", c.country_id) id, c.country_name value, "COUNTRY" typeX' +
            ' FROM countries c ' +
            ' WHERE c.country_id IN (' + cityKeys + ') '
    ' ORDER BY c.country_id ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrCityRecords, fields) {
        callback(null, arrCityRecords);
    });
}
exports.getCountryTagsInformationFromIds = getCountryTagsInformationFromIds;


var getDomainForSideBar = function (req, res, arrDataParams, callback) {

    /*getIdsListForDb(arrMentorIds, function(mentorIdList){
     
     if(mentorIdList.length > 0) {
     var queryParam = {};
     var strQuery = ' SELECT me.domain domainId, md.domain_name domainName, COUNT(DISTINCT(user_id)) userCount  '  +
     ' FROM mentor_expertise me, master_domain md  ' +
     ' WHERE me.domain = md.master_domain_id ' +
     ' AND user_id IN(' + mentorIdList + ') GROUP BY domain';
     console.log(strQuery);            
     dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainForSideBar, fields) {
     callback(null, arrDomainForSideBar);
     });
     }
     else {
     callback(null, []);
     }
     
     });*/
    callback(null, []);
}
exports.getDomainForSideBar = getDomainForSideBar;


var getIdsListForDb = function (arrData, callback) {

    var mentorIds = "";
    if (typeof arrData != 'undefined' && arrData.length > 0) {
        arrData.forEach(function (eachMentorData) {
            if (typeof eachMentorData.user_id == 'undefined') {
                callback(mentorIds);
            }
            mentorIds += eachMentorData.user_id + ",";
        });
    }
    mentorIds = mentorIds.replace(/,$/, '');
    callback(mentorIds);
}
exports.getIdsListForDb = getIdsListForDb;






var getIndustriesListForSideBar = function (req, res, dataParams, callback) {

    var queryParam = {};
    var strQuery = '   SELECT mi.industry_id industryId, mi.industry_name industryName ' +
            '   FROM master_industry mi ' +
            '   WHERE mi.status = 1 ' +
            '   ORDER BY mi.industry_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrIndustryRecords, fields) {

        if (arrIndustryRecords.length == 0) {
            callback(req, res, []);
        } else {
            getAllSubIndustry(req, res, dataParams, arrIndustryRecords, [], getAllSubIndustry, callback);
        }
    });
}
exports.getIndustriesListForSideBar = getIndustriesListForSideBar;


var getAllSubIndustry = function (req, res, dataParams, resultSet, arrFinalData, callbackOld, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var industryId = thisObj.industryId;

        var selectedIndustryId = "0";
        var strSubIndustryIds = "";
        if (typeof dataParams.industryId != 'undefined' && !isNaN(dataParams.industryId) && dataParams.industryId > 0) {
            selectedIndustryId = dataParams.industryId;
        }

        if (typeof dataParams.subIndustryIds != 'undefined' && dataParams.subIndustryIds.length > 0) {
            strSubIndustryIds += dataParams.subIndustryIds.join();
        }

        if (selectedIndustryId == industryId) {
            var queryParam = [industryId];
            var strQuery = '   SELECT msi.subindustry_id subIndustryId, msi.industry_id industryId, msi.subindustry_name subIndustryName, msi.subindustry_desc subIndustryDescription' +
                    '   FROM master_subindustry msi ' +
                    '   WHERE msi.status = 1 ' +
                    '   AND msi.industry_id = ? ' +
                    '   ORDER BY msi.subindustry_name ASC';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryRecords, fields) {
                thisObj.arrSubIndustry = arrSubIndustryRecords;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);

                callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubIndustry, callback);
            });
        } else if (strSubIndustryIds != "") {
            var queryParam = [];
            var strQuery = '   SELECT msi.subindustry_id subIndustryId, msi.industry_id industryId, msi.subindustry_name subIndustryName, msi.subindustry_desc subIndustryDescription' +
                    '   FROM master_subindustry msi ' +
                    '   WHERE msi.status = 1 ' +
                    '   AND msi.industry_id IN (SELECT mssi.industry_id from master_subindustry mssi where mssi.subindustry_id IN (' + strSubIndustryIds + ') AND mssi.industry_id = "' + industryId + '" GROUP BY mssi.industry_id) ' +
                    '   ORDER BY msi.subindustry_name ASC';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryRecords, fields) {
                thisObj.arrSubIndustry = arrSubIndustryRecords;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);

                callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubIndustry, callback);
            });
        } else {
            //thisObj.arrSubIndustry = arrSubIndustryRecords;
            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubIndustry, callback);
        }
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.getAllSubIndustry = getAllSubIndustry;





var getCompleteRegionInfo = function (req, res, dataParams, callback) {
    var queryParam = {};
    var strQuery = '   SELECT mr.region_id regionId, mr.region_name regionName ' +
            '   FROM master_region mr ' +
            '   WHERE mr.status = 1 '
    '   ORDER BY mr.region_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrRegionRecords, fields) {

        if (arrRegionRecords.length == 0) {
            callback(req, res, []);
        } else {
            getAllSubRegions(req, res, dataParams, arrRegionRecords, [], getAllSubRegions, callback);
        }
    });
}
exports.getCompleteRegionInfo = getCompleteRegionInfo;


var getAllSubRegions = function (req, res, dataParams, resultSet, arrFinalData, callbackOld, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var regionId = thisObj.regionId;

        var selectedRegionId = "0";
        var strSubRegionIds = "";
        if (typeof dataParams.hidLocationType != 'undefined' && dataParams.hidLocationType == "R") {
            if (typeof dataParams.hidLocationId != 'undefined' && !isNaN(dataParams.hidLocationId) && dataParams.hidLocationId > 0) {
                selectedRegionId = dataParams.hidLocationId;
            }
        } else if (typeof dataParams.hidLocationType != 'undefined' && dataParams.hidLocationType == "C") {
            if (typeof dataParams.hidLocationId != 'undefined' && !isNaN(dataParams.hidLocationId) && dataParams.hidLocationId > 0) {
                var tmpArrayLocation = [dataParams.hidLocationId];
                strSubRegionIds = tmpArrayLocation.join();
            }
        } else if (typeof dataParams.regionId != 'undefined' && !isNaN(dataParams.regionId) && dataParams.regionId > 0) {
            selectedRegionId = dataParams.regionId;
        }

        if (typeof dataParams.subRegion != 'undefined' && dataParams.subRegion.length > 0) {
            if (strSubRegionIds.length > 0) {
                strSubRegionIds += ",";
            }
            strSubRegionIds += dataParams.subRegion.join();
        }

        if (selectedRegionId == regionId) {

            var queryParam = [regionId];
            var strQuery = '   SELECT c.country_id countryId, c.region_id regionId, c.country_name countryName, c.phone_code phoneCode ' +
                    '   FROM countries c ' +
                    '   WHERE c.status = 1 ' +
                    '   AND c.region_id = ? ' +
                    '   ORDER BY c.country_name ASC';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryRecords, fields) {
                thisObj.arrSubRegion = arrSubIndustryRecords;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);

                callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubRegions, callback);
            });
        } else if (strSubRegionIds.length > 0) {
            var queryParam = [];
            var strQuery = '   SELECT c.country_id countryId, c.region_id regionId, c.country_name countryName, c.phone_code phoneCode ' +
                    '   FROM countries c ' +
                    '   WHERE c.status = 1 ' +
                    '   AND  c.region_id IN (SELECT cc.region_id FROM countries cc WHERE cc.country_id IN (' + strSubRegionIds + ') AND cc.region_id = "' + regionId + '" GROUP BY cc.region_id) ' +
                    '   ORDER BY c.country_name ASC';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryRecords, fields) {
                thisObj.arrSubRegion = arrSubIndustryRecords;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);

                callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubRegions, callback);
            });
        } else {

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);

            callbackOld(req, res, dataParams, resultSet, arrFinalData, getAllSubRegions, callback);
        }
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.getAllSubRegions = getAllSubRegions;

var getAllDomainInfo = function (req, res, callback) {
    var queryParam = {};
    var strQuery = '   SELECT md.master_domain_id domainId, md.domain_name domainName ' +
            '   FROM master_domain md ' +
            '   WHERE md.status = 1 ' +
            '   ORDER BY md.domain_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainRecords, fields) {
        callback(req, res, arrDomainRecords);
    });
}
exports.getAllDomainInfo = getAllDomainInfo;

/*=================================================================================================================================*/
/*===================================================== TOKEN TAGS START ==========================================================*/
/*=================================================================================================================================*/
var getRegionInfoFromIdForTokenTags = function (req, res, regionId, calledFor, callback) {

    var calledForValue = "TF";
    if (typeof calledFor != 'undefined') {
        calledForValue = calledFor;
    }

    var tagTypeText = "LOCATIONTEXTFIELD";

    switch (calledForValue) {
        case "TF":
            tagTypeText = "LOCATIONTEXTFIELD";
            break;
        case "SB":
            tagTypeText = "SIDEBARREGION";
            break;
    }

    var queryParam = [regionId];
    var strQuery = '   SELECT mr.region_id id, mr.region_name faceValue, "' + tagTypeText + '" tagType ' +
            '   FROM master_region mr ' +
            '   WHERE mr.status = 1 ' +
            '   AND mr.region_id = ? ' +
            '   ORDER BY mr.region_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrRegionInfo, fields) {
        callback(req, res, arrRegionInfo);
    });
}
exports.getRegionInfoFromIdForTokenTags = getRegionInfoFromIdForTokenTags;


var getSubRegionInfoFromIdForTokenTags = function (req, res, subRegionId, calledFor, callback) {

    var calledForValue = "TF";
    if (typeof calledFor != 'undefined') {
        calledForValue = calledFor;
    }
    var tagTypeText = "LOCATIONTEXTFIELD";

    switch (calledForValue) {
        case "TF":
            tagTypeText = "LOCATIONTEXTFIELD";
            break;
        case "MP":
            tagTypeText = "MENTORPRFILECOUNTRY";
            break;
        case "SB":
            tagTypeText = "SIDEBARSUBREGION";
            break;
    }

    var finalArray = [];
    if (typeof subRegionId == 'string') {
        finalArray.push(subRegionId);
    } else {
        finalArray = subRegionId;
    }
    var strSubRegionId = finalArray.join();
    var queryParam = [];
    var strQuery = '   SELECT c.country_id id, c.country_name faceValue, "' + tagTypeText + '" tagType ' +
            '   FROM countries c ' +
            '   WHERE /* c.status = 1*/ 1 ' +
            '   AND c.country_id IN (' + strSubRegionId + ') ' +
            '   ORDER BY c.country_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubRegionInfo, fields) {
        callback(req, res, arrSubRegionInfo);
    });
}
exports.getSubRegionInfoFromIdForTokenTags = getSubRegionInfoFromIdForTokenTags;



var getIndustryInfoFromIdForTokenTags = function (req, res, industryId, callback) {
    var queryParam = [industryId];
    var strQuery = '   SELECT mi.industry_id id, mi.industry_name faceValue, "INDUSTRY" tagType ' +
            '   FROM master_industry mi ' +
            '   WHERE mi.status = 1 ' +
            '   AND mi.industry_id = ? ' +
            '   ORDER BY mi.industry_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrIndustryInfo, fields) {
        callback(req, res, arrIndustryInfo);
    });
}
exports.getIndustryInfoFromIdForTokenTags = getIndustryInfoFromIdForTokenTags;


var getCityInfoFromIdForTokenTags = function (req, res, cityId, callback) {
    var queryParam = [cityId];
    var strQuery = '   SELECT c.city_id id, c.city_name faceValue, "MENTORPRFILECITY" tagType ' +
            '   FROM cities c ' +
            '   WHERE /*c.status = 1*/ 1 ' +
            '   AND c.city_id = ? ' +
            '   ORDER BY c.city_name ASC';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrCityInfo, fields) {
        callback(req, res, arrCityInfo);
    });
}
exports.getCityInfoFromIdForTokenTags = getCityInfoFromIdForTokenTags;


var getSubIndustryInfoFromIdForTokenTags = function (req, res, arrSubIndustryIds, callback) {
    if (typeof arrSubIndustryIds != 'undefined' && arrSubIndustryIds.length > 0) {
        var strSubIndustryId = arrSubIndustryIds.join();

        var queryParam = [];

        var strQuery = '   SELECT msi.subindustry_id id, msi.subindustry_name faceValue, "SUBINDUSTRY" tagType ' +
                '   FROM master_subindustry msi ' +
                '   WHERE msi.status = 1 ' +
                '   AND msi.subindustry_id IN (' + strSubIndustryId + ') ' +
                '   ORDER BY msi.subindustry_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSubIndustryInfo, fields) {
            callback(req, res, arrSubIndustryInfo);
        });
    } else {
        callback(req, res, []);
    }
}
exports.getSubIndustryInfoFromIdForTokenTags = getSubIndustryInfoFromIdForTokenTags;


var getDomainInfoFromIdForTokenTags = function (req, res, arrDomains, callback) {
    if (typeof arrDomains != 'undefined' && arrDomains.length > 0) {
        var strDomainIds = arrDomains.join();

        var queryParam = [];

        var strQuery = '   SELECT md.master_domain_id id, md.domain_name faceValue, "DOMAIN" tagType ' +
                '   FROM master_domain md ' +
                '   WHERE md.status = 1 ' +
                '   AND md.master_domain_id IN (' + strDomainIds + ') ' +
                '   ORDER BY md.domain_name ASC';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrDomainInfo, fields) {
            callback(req, res, arrDomainInfo);
        });
    } else {
        callback(req, res, []);
    }
}
exports.getDomainInfoFromIdForTokenTags = getDomainInfoFromIdForTokenTags;

var getAllTokenInputData = function (req, res, dataParams, callback) {

    // parallel function calling for getting token data
    async.parallel([
        // function call for getting tokeninfo for text location field
        function (callbackTokenTag) {
            if ((typeof dataParams.hidLocationType != 'undefined' && dataParams.hidLocationType != "") &&
                    (typeof dataParams.hidLocationId != 'undefined' && !isNaN(dataParams.hidLocationId) && dataParams.hidLocationId > 0)) {

                locationType = dataParams.hidLocationType;
                locationId = dataParams.hidLocationId;

                if (locationType == "R") {
                    getRegionInfoFromIdForTokenTags(req, res, locationId, "TF", function (req, res, arrRegionInfo) {
                        callbackTokenTag(null, arrRegionInfo);
                    });
                } else if (locationType == "C") {
                    getSubRegionInfoFromIdForTokenTags(req, res, [locationId], "TF", function (req, res, arrSubRegionInfo) {
                        callbackTokenTag(null, arrSubRegionInfo);
                    });
                }
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call for getting token info for industry
        function (callbackTokenTag) {
            // setting industry Id
            if (typeof dataParams.industryId != 'undefined' && !isNaN(dataParams.industryId) && dataParams.industryId > 0) {
                industryId = dataParams.industryId;
                getIndustryInfoFromIdForTokenTags(req, res, industryId, function (req, res, arrIndustryInfo) {
                    callbackTokenTag(null, arrIndustryInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call for getting token info for all sub industries.
        function (callbackTokenTag) {
            // setting array of subindustries
            if (typeof dataParams.subIndustryIds != 'undefined' && dataParams.subIndustryIds.length > 0) {
                var subIndustryIds = dataParams.subIndustryIds;

                getSubIndustryInfoFromIdForTokenTags(req, res, subIndustryIds, function (req, res, arrSubIndustryInfo) {
                    callbackTokenTag(null, arrSubIndustryInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call for getting token info for region
        function (callbackTokenTag) {
            // setting industry Id
            if (typeof dataParams.regionId != 'undefined' && !isNaN(dataParams.regionId) && dataParams.regionId > 0) {
                regionId = dataParams.regionId;
                getRegionInfoFromIdForTokenTags(req, res, regionId, "SB", function (req, res, arrRegionInfo) {
                    callbackTokenTag(null, arrRegionInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call for getting token info for all sub region.
        function (callbackTokenTag) {
            // setting array of subindustries
            if (typeof dataParams.subRegion != 'undefined' && dataParams.subRegion.length > 0) {
                var subRegion = dataParams.subRegion;
                getSubRegionInfoFromIdForTokenTags(req, res, subRegion, "SB", function (req, res, arrSubIndustryInfo) {
                    callbackTokenTag(null, arrSubIndustryInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get token info for all domains
        function (callbackTokenTag) {
            if (typeof dataParams.domain != 'undefined' && dataParams.domain.length > 0) {
                var domain = dataParams.domain;
                getDomainInfoFromIdForTokenTags(req, res, domain, function (req, res, arrDomainInfo) {
                    callbackTokenTag(null, arrDomainInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get countryname from mentor profile
        function (callbackTokenTag) {
            if (typeof dataParams.countryId != 'undefined' && !isNaN(dataParams.countryId) && dataParams.countryId > 0) {
                var countryId = dataParams.countryId;
                getSubRegionInfoFromIdForTokenTags(req, res, countryId, "MP", function (req, res, arrCountryInfo) {
                    callbackTokenTag(null, arrCountryInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get cityname from mentor profile
        function (callbackTokenTag) {
            if (typeof dataParams.cityId != 'undefined' && !isNaN(dataParams.cityId) && dataParams.cityId > 0) {
                var cityId = dataParams.cityId;
                getCityInfoFromIdForTokenTags(req, res, cityId, function (req, res, arrCityInfo) {
                    callbackTokenTag(null, arrCityInfo);
                });
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor expertise
        function (callbackTokenTag) {
            if (typeof dataParams.mentorExpertise != 'undefined' && dataParams.mentorExpertise != "") {
                var dataForToken = {
                    id: dataParams.mentorExpertise,
                    faceValue: dataParams.mentorExpertise,
                    tagType: "MENTOREXPERTISE"
                };

                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor designation
        function (callbackTokenTag) {
            if (typeof dataParams.designation != 'undefined' && dataParams.designation != "") {
                var dataForToken = {
                    id: dataParams.designation,
                    faceValue: dataParams.designation,
                    tagType: "MENTORDESIGNATION"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor company working
        function (callbackTokenTag) {
            if (typeof dataParams.company != 'undefined' && dataParams.company != "") {
                var dataForToken = {
                    id: dataParams.company,
                    faceValue: dataParams.company,
                    tagType: "MENTORCOMPANYWORKING"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor jobdescription working
        function (callbackTokenTag) {
            if (typeof dataParams.jobDescription != 'undefined' && dataParams.jobDescription != "") {
                var dataForToken = {
                    id: dataParams.jobDescription,
                    faceValue: dataParams.jobDescription,
                    tagType: "MENTORPROFILEJOBDESCRIPTION"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor first name
        function (callbackTokenTag) {
            if (typeof dataParams.firstName != 'undefined' && dataParams.firstName != "") {
                var dataForToken = {
                    id: dataParams.firstName,
                    faceValue: dataParams.firstName,
                    tagType: "MENTORFIRSTNAME"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor last name
        function (callbackTokenTag) {
            if (typeof dataParams.lastName != 'undefined' && dataParams.lastName != "") {
                var dataForToken = {
                    id: dataParams.lastName,
                    faceValue: dataParams.lastName,
                    tagType: "MENTORLASTNAME"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        },
        // function call to get mentor last name
        function (callbackTokenTag) {
            if (typeof dataParams.fullName != 'undefined' && dataParams.fullName != "") {
                var dataForToken = {
                    id: dataParams.fullName,
                    faceValue: dataParams.fullName,
                    tagType: "MENTORFULLNAME"
                };
                var arrDataToSend = [dataForToken];
                callbackTokenTag(null, arrDataToSend);
            } else {
                callbackTokenTag(null, []);
            }
        }
    ],
            // optional callback
                    function (err, results) {
                        var arrFinalArray = [];
                        if (results.length > 0) {
                            results.forEach(function (arrEachItem) {
                                if (arrEachItem.length > 0) {
                                    arrEachItem.forEach(function (individualElement) {
                                        arrFinalArray.push(individualElement);
                                    });
                                }
                            });
                            callback(req, res, arrFinalArray);
                        } else {
                            callback(req, res, []);
                        }
                    });
        }
exports.getAllTokenInputData = getAllTokenInputData;
/*=================================================================================================================================*/
/*===================================================== TOKEN TAGS END  ===========================================================*/
/*=================================================================================================================================*/




var checkDocumentExistOnPath = function (req, res, resultSet, currentIndex, arrFinalData, callback) {
    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var document_path = thisObj.document_path;
        var isFileExist = false;
        if (document_path != "") {
            fs.stat(document_path, function (err, stat) {
                if (err == null) {
                    isFileExist = true;
                }
                thisObj.isFileExist = isFileExist;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);
                currentIndex++;
                checkDocumentExistOnPath(req, res, resultSet, currentIndex, arrFinalData, callback);
            });
        } else {
            thisObj.isFileExist = isFileExist;
            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            currentIndex++;
            checkDocumentExistOnPath(req, res, resultSet, currentIndex, arrFinalData, callback);
        }
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.checkDocumentExistOnPath = checkDocumentExistOnPath;


var getCancellationPolicies = function (req, res, callback) {
    var queryParam = {};
    var strQuery = '   SELECT cp.policy_for ,cp.cancellation_policy_id cancellationPolicyId, cp.policy_type policyType, cp.policy_label policyLabel, cp.policy_description policyDescription, cp.cancellation_policy_link cancellationPolicyLink ' +
            '   FROM cancellation_policy cp ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrCancellationPolicyData, fields) {
        callback(req, res, arrCancellationPolicyData);
    });
}
exports.getCancellationPolicies = getCancellationPolicies;


var getCityListCountFromCountryId = function (req, res, countryId, callback) {
    if (typeof countryId == 'undefined' || !(countryId > 0) || isNaN(countryId)) {
        callback(req, res, {});
    }

    var queryParam = [countryId];
    var strQuery = ' SELECT COUNT(*) totalCount ' +
            ' FROM cities c ' +
            ' WHERE c.status = "1" ' +
            ' AND c.country_id = ?' +
            ' ORDER BY c.city_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        results = results[0];
        callback(req, res, results);
    });
}
exports.getCityListCountFromCountryId = getCityListCountFromCountryId;

var chkComplainceTest = function (req, res, user_id, callback) {

    var queryParam = [user_id];
    var strQuery = 'SELECT is_compliance_passed FROM mentor_details where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {

            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, {});
            }
        }
    });
};
exports.chkComplainceTest = chkComplainceTest;



var getPromoCodeInformation = function (req, res, objDataParams, callback) {
    var promoCodeId = objDataParams.promoCodeId;

    if (promoCodeId > 0) {
        var queryParam = [promoCodeId];
        var strQuery = '    SELECT pc.promo_code_id promoCodeId, pc.mentor_id mentorId, pc.user_id userId, pc.invitation_id invitationId, pc.promo_code prompCode, pc.discount discountPercentage, pc.description description, pc.promo_code_status promocodeStatus, pc.is_used isUsed ' +
                '   FROM promo_codes pc  ' +
                '   WHERE pc.promo_code_id = ? ';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length > 0) {
                callback(req, res, results[0]);
            } else {
                callback(req, res, {});
            }
        });
    } else {
        callback(req, res, {});
    }

};
exports.getPromoCodeInformation = getPromoCodeInformation;


var getAcceptedInviteSlotForMeeting = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;

    var queryParam = [invitationId];
    var strQuery = '    SELECT mism.id meetingInvitationSlotMappingId, mism.invitation_id invitationId, mism.mentor_id mentorId, mism.date date, mism.time time, mism.status status, mi.duration duration' +
            '   FROM meeting_invitation_slot_mapping mism, meeting_invitations mi  ' +
            '   WHERE mism.invitation_id = mi.invitation_id AND mism.invitation_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getAcceptedInviteSlotForMeeting = getAcceptedInviteSlotForMeeting;



var getCancellationPriceForUser = function (req, res, objDataParams, callback) {

    var userId = objDataParams.userId;
    var mentorId = objDataParams.mentorId;
    var invitationId = objDataParams.invitationId;
    var cancellationFor = objDataParams.cancellationFor;
    var getFullData = false;

    if (typeof objDataParams.getFullData != 'undefined') {
        getFullData = objDataParams.getFullData;
    }

    if (cancellationFor == "mentor") {

        async.parallel([
            // fucntion call to get meeting related data.
            function (meetingRelatedDataCallback) {
                var objParamsForMeetingDetailForCancellation = {
                    invitationId: invitationId
                };

                getMeetingDetailForCancellationPurpose(req, res, objParamsForMeetingDetailForCancellation, function (req, res, objMeetingDetailForCancellationPurpose) {
                    meetingRelatedDataCallback(null, objMeetingDetailForCancellationPurpose);
                });
            },
            // function call to get mentor cancellation charges,
            function (mentorCancellationChargesCallback) {
                var objDataForMentorCancellation = {
                    invitationId: invitationId
                };
                getMentorCancellationChargesForPerticularMeeting(req, res, objDataForMentorCancellation, function (req, res, objMentorCancellationData) {
                    mentorCancellationChargesCallback(null, objMentorCancellationData);
                });
            },
            // function call to get invoice info from invitationId
            function (mentorCancellationChargesCallback) {
                var objParamsForMeetingDetailForCancellation = {
                    invitationId: invitationId
                };
                getPrivateMeetingDetailForCancellationPurpose(req, res, objParamsForMeetingDetailForCancellation, function (req, res, objMeetingDetailForCancellationPurpose) {
                    mentorCancellationChargesCallback(null, objMeetingDetailForCancellationPurpose);
                });
            }
        ], function (error, resultSet) {

            var objMeetingDetailForCancellationPurpose = resultSet[0];
            console.log("objMeetingDetailForCancellationPurpose 111 :: ", objMeetingDetailForCancellationPurpose);
            var objMentorCancellationCharges = resultSet[1];
            var objPrivateMeetingDetailForCancellationPurpose = resultSet[2];


            var diffInHours = objMeetingDetailForCancellationPurpose.diffInHours;
            var hourLimit = objMentorCancellationCharges.hours;

            var mentorCancellationCharge = objMentorCancellationCharges.cancellationPercentage;
            if (diffInHours > hourLimit) {
                mentorCancellationCharge = 0;
            }

            if (!getFullData) {
                var objReturn = {
                    amountToRefund: parseFloat(mentorCancellationCharge).toFixed(2)
                };
                callback(req, res, objReturn);
            } else {
                console.log("objMeetingDetailForCancellationPurpose 2222 :: ", objMeetingDetailForCancellationPurpose);
                var objReturn = {
                    objMeetingDetailForCancellationPurpose: objMeetingDetailForCancellationPurpose,
                    objMentorCancellationCharges: objMentorCancellationCharges,
                    objPrivateMeetingDetailForCancellationPurpose: objPrivateMeetingDetailForCancellationPurpose,
                    amountToRefund: parseFloat(mentorCancellationCharge).toFixed(2)
                };
                callback(req, res, objReturn);
            }

        });
    } else {
        async.parallel([
            // fucntion call to get meeting related data.
            function (meetingRelatedDataCallback) {
                var objParamsForMeetingDetailForCancellation = {
                    invitationId: invitationId
                };
                getMeetingDetailForCancellationPurpose(req, res, objParamsForMeetingDetailForCancellation, function (req, res, objMeetingDetailForCancellationPurpose) {
                    meetingRelatedDataCallback(null, objMeetingDetailForCancellationPurpose);
                });
            },
            // fucntion call to get mentor cancellation policy.
            function (meetingRelatedDataCallback) {
                var objParamsForMentorCancellation = {
                    invitationId: invitationId
                };
                //getMentorCancellationPolicy(req, res, objParamsForMentorCancellation, function(req, res, objMentorCancellationPolicyInfo){
                getMentorCancellationPolicyForUserForPerticularMeeting(req, res, objParamsForMentorCancellation, function (req, res, objMentorCancellationPolicyInfo) {
                    meetingRelatedDataCallback(null, objMentorCancellationPolicyInfo);
                });
            },
            // function call to get invoice info from invitationId
            function (mentorCancellationChargesCallback) {
                var objParamsForMeetingDetailForCancellation = {
                    invitationId: invitationId
                };
                getPrivateMeetingDetailForCancellationPurpose(req, res, objParamsForMeetingDetailForCancellation, function (req, res, objMeetingDetailForCancellationPurpose) {
                    mentorCancellationChargesCallback(null, objMeetingDetailForCancellationPurpose);
                });
            }
        ], function (error, resultSet) {

            var objMeetingDetailForCancellationPurpose = resultSet[0];
            var objMentorCancellationPolicy = resultSet[1];
            var objPrivateMeetingDetailForCancellationPurpose = resultSet[2];

            var diffInHours = objMeetingDetailForCancellationPurpose.diffInHours;

            var totalAmountUserPaid = objMeetingDetailForCancellationPurpose.total_payable_amount;
            var correctConditionFound = false;

            for (var i = 0; i < objMentorCancellationPolicy.length; i++) {
                thisPolicyDetail = objMentorCancellationPolicy[i];

                var mentorPolicyAtTimeOfBooking = thisPolicyDetail.mentorPolicyAtTimeOfBooking;
                var cancellationPercentage = thisPolicyDetail.cancellationPercentage;

                if (!correctConditionFound) {
                    var hours = thisPolicyDetail.hours;
                    var arrayHours = hours.split(',');

                    if (arrayHours.length == 1) {
                        var onlyHour = arrayHours[0];

                        if (diffInHours <= onlyHour && i == 0) {
                            correctConditionFound = true;

                            var objDataParamsForCancellation = {
                                totalAmountUserPaid: totalAmountUserPaid,
                                cancellationPercentage: thisPolicyDetail.cancellationPercentage
                            };
                            getCancellationFeeCalculation(req, res, objDataParamsForCancellation, function (req, res, objResponse) {

                                if (!getFullData) {
                                    var objReturn = {
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking
                                    };
                                    callback(req, res, objReturn);
                                } else {
                                    var objReturn = {
                                        objMeetingDetailForCancellationPurpose: objMeetingDetailForCancellationPurpose,
                                        objMentorCancellationPolicy: objMentorCancellationPolicy,
                                        objPrivateMeetingDetailForCancellationPurpose: objPrivateMeetingDetailForCancellationPurpose,
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking,
                                        objCancellationPolicyForUser: thisPolicyDetail
                                    };
                                    callback(req, res, objReturn);
                                }
                            });

                        } else if (diffInHours > onlyHour && i == 2) {
                            correctConditionFound = true;

                            var objDataParamsForCancellation = {
                                totalAmountUserPaid: totalAmountUserPaid,
                                cancellationPercentage: thisPolicyDetail.cancellationPercentage
                            };
                            getCancellationFeeCalculation(req, res, objDataParamsForCancellation, function (req, res, objResponse) {
                                if (!getFullData) {
                                    var objReturn = {
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking
                                    };
                                    callback(req, res, objReturn);
                                } else {
                                    var objReturn = {
                                        objMeetingDetailForCancellationPurpose: objMeetingDetailForCancellationPurpose,
                                        objMentorCancellationPolicy: objMentorCancellationPolicy,
                                        objPrivateMeetingDetailForCancellationPurpose: objPrivateMeetingDetailForCancellationPurpose,
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking,
                                        objCancellationPolicyForUser: thisPolicyDetail
                                    };
                                    callback(req, res, objReturn);
                                }
                            });
                        }
                    } else if (arrayHours.length == 2) {
                        var startHourRage = arrayHours[0];
                        var endHourRage = arrayHours[1];

                        if (diffInHours > startHourRage && diffInHours <= endHourRage && i == 1) {
                            correctConditionFound = true;
                            var objDataParamsForCancellation = {
                                totalAmountUserPaid: totalAmountUserPaid,
                                cancellationPercentage: thisPolicyDetail.cancellationPercentage
                            };
                            getCancellationFeeCalculation(req, res, objDataParamsForCancellation, function (req, res, objResponse) {
                                if (!getFullData) {
                                    var objReturn = {
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking
                                    };
                                    callback(req, res, objReturn);
                                } else {
                                    var objReturn = {
                                        objMeetingDetailForCancellationPurpose: objMeetingDetailForCancellationPurpose,
                                        objPrivateMeetingDetailForCancellationPurpose: objPrivateMeetingDetailForCancellationPurpose,
                                        objMentorCancellationPolicy: objMentorCancellationPolicy,
                                        amountToRefund: objResponse.amountToRefund,
                                        mentorPolicyAtTimeOfBooking: mentorPolicyAtTimeOfBooking,
                                        objCancellationPolicyForUser: thisPolicyDetail
                                    };
                                    callback(req, res, objReturn);
                                }
                            });
                        }
                    }
                }
            }

            if (!correctConditionFound) {
                if (!getFullData) {
                    var objReturn = {
                        amountToRefund: "0",
                        mentorPolicyAtTimeOfBooking: ""
                    };
                    callback(req, res, objReturn);
                } else {
                    var objReturn = {
                        objMeetingDetailForCancellationPurpose: {},
                        objMentorCancellationPolicy: {},
                        objPrivateMeetingDetailForCancellationPurpose: {},
                        amountToRefund: "0",
                        mentorPolicyAtTimeOfBooking: "",
                        objCancellationPolicyForUser: {}
                    };
                    callback(req, res, objReturn);
                }
            }

        });

        //callback(req, res, { data: "asdfasdf"});
    }
};
exports.getCancellationPriceForUser = getCancellationPriceForUser;

var getCancellationFeeCalculation = function (req, res, objDataParams, callback) {
    console.log("objDataParams :: ", objDataParams);
    var totalAmountUserPaid = objDataParams.totalAmountUserPaid;
    var cancellationPercentage = objDataParams.cancellationPercentage;


    if (cancellationPercentage > 0 || true) {
        var cancellationFees = parseFloat(parseFloat(totalAmountUserPaid) * parseFloat(cancellationPercentage) / 100).toFixed(2);
        //var amountToRefund = parseFloat(parseFloat(totalAmountUserPaid) - parseFloat(cancellationFees)).toFixed(2);
        var objReturn = {
            amountToRefund: cancellationFees
        };
        callback(req, res, objReturn);
    } else {
        var objReturn = {
            amountToRefund: parseFloat(totalAmountUserPaid).toFixed(2)
        };
        callback(req, res, objReturn);
    }

};
exports.getCancellationFeeCalculation = getCancellationFeeCalculation;



var getMentorCancellationChargesForPerticularMeeting = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;

    var queryParam = [invitationId];
    var strQuery = '    SELECT cpci.policy_type policyType, cpci.hours hours, cpci.percentage cancellationPercentage ' +
            '   FROM meeting_price_image_master mpim, cancellation_policy_charges_image cpci' +
            '   WHERE cpci.meeting_price_image_master_id = mpim.meeting_price_image_master_id ' +
            '   AND mpim.invitation_id = ? ' +
            '   AND cpci.policy_type = "mentor" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getMentorCancellationChargesForPerticularMeeting = getMentorCancellationChargesForPerticularMeeting;


var getMentorCancellationCharges = function (req, res, objDataParams, callback) {

    var queryParam = [];
    var strQuery = '    SELECT cp.cancellation_policy_id cancellationPolicyId, cpc.policy_type policyType, cpt.hours, cpc.percentage cancellationPercentage ' +
            '   FROM cancellation_policy cp, cancellation_policy_charges cpc, cancellation_policy_type cpt ' +
            '   WHERE cp.policy_for = "mentor" ' +
            '   AND cpc.cancellation_policy_id = cp.cancellation_policy_id ' +
            '   AND cpt.policy_type = cpc.policy_type ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getMentorCancellationCharges = getMentorCancellationCharges;

var getMeetingDetailForCancellationPurpose = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, invitationId];

    var strQuery = '    SELECT TIMESTAMPDIFF(HOUR, ?, md.scheduled_start_time) diffInHours, i.* ' +
            '   FROM meeting_details md, meeting_invitations mi, invoice i  ' +
            '   WHERE md.invitation_id = ? ' +
            '   AND md.invitation_id = mi.invitation_id ' +
            '   AND i.invitation_id = mi.invitation_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getMeetingDetailForCancellationPurpose = getMeetingDetailForCancellationPurpose;


var getPrivateMeetingDetailForCancellationPurpose = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [currentUTC, invitationId];

    var strQuery = '    SELECT TIMESTAMPDIFF(HOUR, ?, mi.created_date) diffInHours, i.* ' +
            '   FROM meeting_invitations mi, invoice i  ' +
            '   WHERE mi.invitation_id = ? ' +
            '   AND i.invitation_id = mi.invitation_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getPrivateMeetingDetailForCancellationPurpose = getPrivateMeetingDetailForCancellationPurpose;


var getMentorCancellationPolicyForUserForPerticularMeeting = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;

    var queryParam = [invitationId];
    var strQuery = '    SELECT mpim.mentor_policy mentorPolicyAtTimeOfBooking, cpci.policy_type policyType, cpci.hours hours, cpci.percentage cancellationPercentage ' +
            '   FROM meeting_price_image_master mpim, cancellation_policy_charges_image cpci' +
            '   WHERE cpci.meeting_price_image_master_id = mpim.meeting_price_image_master_id ' +
            '   AND mpim.invitation_id = ? ' +
            '   AND cpci.policy_for = "user" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getMentorCancellationPolicyForUserForPerticularMeeting = getMentorCancellationPolicyForUserForPerticularMeeting;

var getMentorCancellationPolicy = function (req, res, objDataParams, callback) {
    var mentorId = objDataParams.mentorId;

    var queryParam = [mentorId];
    var strQuery = '    SELECT md.cancellation_policy_type cancellationPolicy, cp.cancellation_policy_id cancellationPolicyId, cpc.policy_type policyType, cpt.hours, cpc.percentage cancellationPercentage ' +
            '   FROM mentor_details md, cancellation_policy cp, cancellation_policy_charges cpc, cancellation_policy_type cpt ' +
            '   WHERE md.user_id = ? ' +
            '   AND md.cancellation_policy_type = cp.policy_type ' +
            '   AND cp.policy_for = "user" ' +
            '   AND cpc.cancellation_policy_id = cp.cancellation_policy_id ' +
            '   AND cpt.policy_type = cpc.policy_type ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getMentorCancellationPolicy = getMentorCancellationPolicy;

var getCancellationPolicyPercentage = function (req, res, objDataParams, callback) {

    var cancellationPolicyCase = objDataParams.cancellationPolicyCase;
    var cancellationPolicyId = objDataParams.cancellationPolicyId;

    var queryParam = [cancellationPolicyId, cancellationPolicyCase];
    var strQuery = '    SELECT cpc.cancellation_policy_id cancellationPolicyId, cpc.policy_type policyType, cpc.percentage cancellationPercentage ' +
            '   FROM cancellation_policy_charges cpc ' +
            '   WHERE cpc.cancellation_policy_id = ? ' +
            '   AND cpc.policy_type = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getCancellationPolicyPercentage = getCancellationPolicyPercentage;



var getMentorAndUserCountryPercentageForInvoice = function (req, res, objDataParams, callback) {
    var invoiceId = objDataParams.invoiceId;

    var queryParam = [invoiceId];
    var strQuery = '    SELECT cm.wc_mentor_percentage wissenxChargesMentorPercentage, cu.wc_user_percentage wissenxChargesUserPercentage ' +
            '   FROM invoice i, meeting_invitations mi, countries cm, user_details udm, countries cu, user_details udu ' +
            '   WHERE i.invoice_id = ? ' +
            '   AND i.invitation_id = mi.invitation_id ' +
            '   AND udu.user_id = IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) ' +
            '   AND udu.country_id = cu.country_id ' +
            '   AND udm.user_id = IF(mi.invitation_type = 0, mi.user_id, mi.requester_id) ' +
            '   AND udm.country_id = cm.country_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
            z
        }

        if (results.length > 0) {
            callback(req, res, results[0]);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getMentorAndUserCountryPercentageForInvoice = getMentorAndUserCountryPercentageForInvoice;


var getSystemSettingVariable = function (req, res, objDataParams, callback) {
    var systemSettingVariables = objDataParams.systemSettingVariables;

    var strForVariable = '';

    for (var i = 0; i < systemSettingVariables.length; i++) {
        strForVariable += '"' + systemSettingVariables[i] + '", ';
    }

    strForVariable = strForVariable.replace(/(^\s*,)|(,\s*$)/g, '');

    var queryParam = [];
    var strQuery = '    SELECT ss.code systemVariableCode, IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
            '   FROM system_settings ss ' +
            '   WHERE ss.code IN ( ' + strForVariable + ' ) ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
}
exports.getSystemSettingVariable = getSystemSettingVariable;


var getHomePageMentorExpertiseTags = function (req, res, objParams, callback) {
    var queryParam = {};
    var strQuery = ' SELECT mot.tag_name id, mot.tag_name value, "Expertise" tokenTypeLabel, "ME" tokenTypeX  ' +
            ' FROM mentor_organisation_tags mot, users u ' +
            ' WHERE mot.user_id = u.user_id ' +
            ' AND u.status = 1 ' +
            ' GROUP BY mot.tag_name ' +
            ' ORDER BY RAND() ' +
            ' LIMIT 0,9 ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrMentorAndOrganizationMentorExpertiseResult, fields) {
        callback(req, res, arrMentorAndOrganizationMentorExpertiseResult);
    });
};
exports.getHomePageMentorExpertiseTags = getHomePageMentorExpertiseTags;

var getFeedbackData_model = function (req, res, objParams, callback) {
    var feedbackKey = objParams.feedbackKey;
    var queryParam = [feedbackKey];
    var strQuery = 'Select lf.*,mi.invitation_id,zo.zone_name,mi.duration, ' +
            ' CONCAT(ud.first_name," ",ud.last_name) as name,ud.profile_image,mi.document,' +
            ' mi.meeting_purpose,mi.meeting_purpose_indetail,UPPER(mi.status) as status' +
            ' from leave_feedback lf ' +
            ' LEFT JOIN user_details ud ON ud.user_id = lf.user_id ' +
            ' LEFT JOIN zone zo ON zo.zone_id = ud.timezone_id ' +
            ' LEFT JOIN meeting_details md ON md.meeting_id = lf.meeting_detail_id ' +
            ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = md.invitation_id ' +
            ' Where url_key = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, feedbackData, fields) {
        if (feedbackData.length > 0) {
            userModel.getUserMeetingsData(req, res, "future", feedbackData, feedbackData.zone_name, 0, [], function (req, res, arrMentorData) {
                userModel.setImagePathOfAllUsers(req, res, arrMentorData, 0, [], function (req, res, MeetingData) {
                    callback(req, res, MeetingData);
                });
            });
        } else {
            callback(req, res, "");
        }
    });
};
exports.getFeedbackData_model = getFeedbackData_model;

var updateKey_model = function (req, res) {
    var reqBody = req.body;
    var key = reqBody.key;
    var queryParam = [key];
    var strQuery = 'UPDATE leave_feedback set is_used = 1  Where url_key = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": results,
                "message": "success"}));
            res.end();
        }
    });
};
exports.updateKey_model = updateKey_model;

var getCostBreakUp = function (req, res, callback) {
    var reqBody = req.body;
    var invitation_id = reqBody.invitation_id;
    var queryParam = [invitation_id];
    var strQuery = 'Select * from invoice where invitation_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, error);
        } else {
            callback(req, res, results);
        }
    });
};
exports.getCostBreakUp = getCostBreakUp;


var uploadCSVForMentorCreate = function (req, res, callback) {

    var form = new formidable.IncomingForm();
    form.multiples = true;
    form.uploadDir1 = path.join('uploads/miscfiles');
    form.uploadDir = path.join(__dirname, '../uploads/miscfiles');

    var fileData = {};

    form.on('file', function (field, file) {

        var uploadedFileName = 'mentorprofile' + Date.now() + path.extname(file.name);
        var uploadedPath = path.join(form.uploadDir1, uploadedFileName)
        fs.rename(file.path, uploadedPath);
        var fileSize = file.size;
        var fileName = file.name;
        var fileType = file.type;
        var filePath = file.path;
        var currentUTC = constants.CURR_UTC_DATETIME();
        var params = {
            document_name: uploadedFileName,
            document_real_name: fileName,
            document_type: fileType,
            document_size: fileSize,
            document_path: uploadedPath,
            document_mime: fileType,
            status: "FOUND"
        };

        fileData = params;

    });
    form.on('error', function (err) {
        callback(req, res, {status: "error"});
    });
    form.parse(req, function (err, fields, filesss) {
    });
    form.on('end', function () {
        callback(req, res, fileData);
    });

}
exports.uploadCSVForMentorCreate = uploadCSVForMentorCreate;



var processCsvForMentorCreate = function (req, res, objDataParams, callback) {

    var flagFirst = true;
    var arrHeader = [];
    csv
            .fromPath(objDataParams.document_path)
            .on("data", function (data) {
                if (flagFirst) {
                    console.log(data);

                    for (var i = 0; i < data.length; i++) {
                        arrHeader.push(data[i]);
                    }

                    flagFirst = false;
                } else {
                    console.log(data);
                    console.log(data.length);

                    var email = data[0];
                    var password = data[1];
                    var dob = data[2];
                    var phoneCountry = data[3];
                    var phoneNumber = data[4];
                    var firstName = data[5];
                    var lastName = data[6];
                    var profileImageURL = data[7];
                    var linkedInLink = data[8];
                    var address1 = data[9];
                    var address2 = data[10];
                    var profileCountryId = data[11];
                    var profileCityId = data[12];
                    var timeZoneId = data[13];
                    var hourlyRate = data[14];
                    var currency = data[15];
                    var cancellationPolicy = data[16];
                    var profession = data[17];
                    var jobDescription = data[18];
                    var employmentType = data[19];
                    var companyName = data[20];
                    var punchLine = data[21];
                    var overview = data[22];
                    var isFeatured = data[23];
                    var isCompliancePassed = data[24];
                    var hideFromSearch = data[25];
                    var hideCalendar = data[26];
                    var hideReviewRating = data[27];
                    var keyNoteVideo = data[28];
                }
            })
            .on("end", function () {
                console.log("done");
            });
}
exports.processCsvForMentorCreate = processCsvForMentorCreate;

var createMessageLog = function (req, res, data, callback) {

    var queryParam = {
        user_id: data.userId,
        user_type: data.user_type,
        purpose: data.purpose,
        number: data.number,
        message: data.message,
        status: data.status,
        created_date: constants.CURR_UTC_DATETIME()};
    var strQuery = 'INSERT INTO smslogs SET ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, 'error');
        } else {
            callback(req, res, 'success');
        }
    });

};
exports.createMessageLog = createMessageLog;



var checkCountryOrCityActiveOrNot = function (req, res, objDataParams, callback) {
    var countryId = objDataParams.countryId;
    var cityId = objDataParams.cityId;


    async.parallel([
        // fucntion call to check country active
        function (countryCityCheckCallback) {
            if (!isNaN(countryId) && countryId > 0) {
                var queryParam = [countryId];
                var strQuery = ' SELECT count(*) totalCnt ' +
                        ' FROM countries c ' +
                        ' WHERE c.status = "1" ' +
                        ' AND c.country_id = ? ' +
                        ' ORDER BY c.country_name ASC';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    if (results.length > 0) {
                        var thisResult = results[0];
                        if (thisResult['totalCnt'] > 0) {
                            countryCityCheckCallback(null, true);
                        } else {
                            countryCityCheckCallback(null, false);
                        }
                    } else {
                        countryCityCheckCallback(null, false);
                    }
                });
            } else {
                countryCityCheckCallback(null, true);
            }
        },
        // fucntion call to check country active
        function (countryCityCheckCallback) {
            if (!isNaN(cityId) && cityId > 0) {
                var queryParam = [cityId];
                var strQuery = ' SELECT count(*) totalCnt ' +
                        ' FROM cities c ' +
                        ' WHERE c.status = "1" ' +
                        ' AND c.city_id = ? ';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    if (results.length > 0) {
                        var thisResult = results[0];
                        if (thisResult['totalCnt'] > 0) {
                            countryCityCheckCallback(null, true);
                        } else {
                            countryCityCheckCallback(null, false);
                        }
                    } else {
                        countryCityCheckCallback(null, false);
                    }
                });
            } else {
                countryCityCheckCallback(null, true);
            }
        }
    ], function (error, resultSet) {
        console.log("resultSet :: ", resultSet);
        var countryValid = resultSet[0];
        var cityValid = resultSet[1];

        if (countryValid && cityValid) {
            callback(req, res, {valid: true});
        } else {
            callback(req, res, {valid: false});
        }
    });
}
exports.checkCountryOrCityActiveOrNot = checkCountryOrCityActiveOrNot;



var printAllPermutation = function (arrString, length, callback) {
    console.log("=============IN printAllPermutation ==================");
    console.log("arrString :: ", arrString);
//     printNextTest(arrString, "", length, length, function(stringRec){
// console.log("stringRec :: ", stringRec);
//         callback(stringRec);
//     });

    // var obj = permute(arrString);

    var testPandC = Combinatorics.permutationCombination(arrString);
    callback(testPandC.toArray());
}
exports.printAllPermutation = printAllPermutation;


var printNextTest = function (arrString, prefix, n, k, callback) {
    console.log("=============IN printNextTest ==================");
    if (k == 0) {
        console.log("prefix ::: ", prefix);
        callback(prefix);
    } else {

        for (i = 0; i < n; ++i) {
            console.log("i :: ", i);
            console.log("arrString[" + i + "] :: ", arrString[i]);
            var newPer = prefix + arrString[i];
            console.log("newPer :: ", newPer);
            var nk = k - 1;
            printNextTest(arrString, newPer, n, nk, callback);
        }
    }
}
exports.printNextTest = printNextTest;



var permArr = [],
        usedChars = [];
var permute = function (input) {
    var i, ch;
    for (i = 0; i < input.length; i++) {
        ch = input.splice(i, 1)[0];
        usedChars.push(ch);
        if (input.length == 0) {
            permArr.push(usedChars.slice());
        }
        permute(input);
        input.splice(i, 0, ch);
        usedChars.pop();
    }
    return permArr
}
exports.permute = permute;




var getCountryCityDataBase = function (req, res, index, callback) {

    var queryParam = {};
    var perPageCount = 50000;

    if (index <= 5 || true) {
        var startLimit = (perPageCount * index);
        // var endLimit = ((perPageCount * index) + perPageCount);
        var endLimit = (perPageCount);

        var limit = startLimit + ', ' + endLimit;

        var strQuery = '    SELECT co.country_name countryName, s.state_name stateName, c.city_name cityName ' +
                '   FROM cities c ' +
                '   	LEFT JOIN states s ON s.state_id = c.state_id ' +
                '   	LEFT JOIN countries co ON co.country_id = c.country_id ' +
                '   /* ORDER  BY co.country_name ASC, s.state_name ASC, c.city_name ASC */ ' +
                '   LIMIT ' + limit;

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length > 0) {
                insertIntoCsv(req, res, results, function (objResult) {
                    //callback(req, res, 'done');
                    getCountryCityDataBase(req, res, (index + 1), callback);
                });
            } else {
                callback(req, res, 'done');
            }
        });
    } else {
        callback(req, res, 'done');
    }


}
exports.getCountryCityDataBase = getCountryCityDataBase;

var insertIntoCsv = function (req, res, objResult, callback) {

    var ws = fs.createWriteStream("./../geodata/statesWithoutcountry.csv", {'flags': 'a'});
    csv.write(objResult, {
        headers: true
    })
            .pipe(ws)
            .on('finish', function () {
                callback(true)
            });
};


var getCountryListForAllDB = function (req, res, callback) {
    var queryParam = {};
    var strQuery = ' SELECT country_id, country_name ' +
            ' FROM countries ' +
            ' ORDER BY country_name ASC';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, results);
    });
}
exports.getCountryListForAllDB = getCountryListForAllDB;


var getStateWithoutCountry = function (req, res, callback) {
    var queryParam = {};
    var strQuery = ' SELECT * FROM cities WHERE country_id IS NULL OR country_id = "" OR country_id ="0" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length > 0) {
            insertIntoCsv(req, res, results, function (objResult) {
                callback(req, res, 'done');
                // getCountryCityDataBase(req, res, (index + 1), callback);
            });
        } else {
            callback(req, res, 'done');
        }
    });
}
exports.getStateWithoutCountry = getStateWithoutCountry;

var getLocationIds = function (req, res, objParams, callback) {

    console.log(objParams);

    var country = objParams.country;
    var state = objParams.state;
    var city_invalid = objParams.city;
    var iso2 = objParams.iso2;
    var countryId = "";
    var stateId = "";
    var cityId = "";
    var currentUTC = constants.CURR_UTC_DATETIME();
    var city = city_invalid.split(', ')[0];

    var strQuery = " INSERT INTO countries (country_code,country_name, status, created_date) " +
            " SELECT * FROM (SELECT ?, ?, ?, ?) AS tmp " +
            " WHERE NOT EXISTS ( " +
            " SELECT country_name FROM countries WHERE country_name = ? ) LIMIT 1 ";
    var param = [iso2, country, "1", currentUTC, country];
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, countryData, fields) {
        if (error) {
            console.log(error);
            callback(req, res, "err");
        } else {
            var strQuery = " INSERT INTO states (country_id,state_name, created_date) " +
                    " SELECT * FROM (SELECT (SELECT country_id FROM countries WHERE country_name = ?),?,?) AS tmp " +
                    " WHERE NOT EXISTS ( " +
                    " SELECT state_name FROM states WHERE state_name = ? ) LIMIT 1 ";
            var param = [country, state, currentUTC, state];
            dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, stateData, fields) {
                if (error) {
                    console.log(error);
                    callback(req, res, "err");
                } else {
                    var strQuery = " INSERT INTO cities (country_id,state_id,city_name, created_date) " +
                            " SELECT * FROM (SELECT (SELECT country_id FROM countries WHERE country_name = ?),(SELECT state_id FROM states WHERE state_name = ?),?,?) AS tmp " +
                            " WHERE NOT EXISTS ( " +
                            " SELECT city_name FROM cities WHERE city_name = ? ) LIMIT 1 ";
                    var param = [country, state, city, currentUTC, city];
                    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, cityData, fields) {
                        if (error) {
                            console.log(error);
                            callback(req, res, "err");
                        } else {
                            async.parallel([
                                function (getLocationId) {
                                    var strQuery = " SELECT country_id FROM countries WHERE country_name = ?";
                                    var param = [country];
                                    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, result, fields) {
                                        if (error) {
                                            getLocationId(null, "");
                                        } else {
                                            getLocationId(null, result[0].country_id);
                                        }
                                    });
                                },
                                function (getLocationId) {
                                    var strQuery = " SELECT state_id FROM states WHERE state_name = ?";
                                    var param = [state];
                                    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, result, fields) {
                                        if (error) {
                                            getLocationId(null, "");
                                        } else {
                                            getLocationId(null, result[0].state_id);
                                        }
                                    });
                                },
                                function (getLocationId) {
                                    var strQuery = " SELECT city_id FROM cities WHERE city_name = ?";
                                    var param = [city];
                                    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, result, fields) {
                                        if (error) {
                                            getLocationId(null, "");
                                        } else {
                                            getLocationId(null, result[0].city_id);
                                        }
                                    });
                                }
                            ], function (error, resultSet) {
                                countryId = resultSet[0];
                                stateId = resultSet[1];
                                cityId = resultSet[2];

                                var sendData = {
                                    countryId: countryId,
                                    stateId: stateId,
                                    cityId: cityId
                                };

                                callback(req, res, sendData);
                            });
                        }
                    });
                }
            });
        }
    });
};
exports.getLocationIds = getLocationIds;

var insertindb = function (req, res, objResult, callback) {

    var stream = fs.createReadStream("master_region_country.csv");
    var strdata = "";
    var csvStream = csv()
            .on("data", function (data) {
                strdata += '("' + data[0] + '", "' + data[1] + '"),';
            })
            .on("end", function () {

                var strQuery = 'insert into master_region_country_map values ' + strdata;
                console.log(strQuery);

//                var strQuery = " insert into master_region_country_map values (val1, val2), (val1, val2)";
//                var param = {
//                    region: data[0],
//                    country: data[1]
//                };
//                dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, result, fields) {
//                    if (error) {
//
//                    } else {
//
//                    }
//                });
            });

    stream.pipe(csvStream);
};
exports.insertindb = insertindb;



var getAllData = function (req, res, callback) {

    var queryParam = [];
    var strQuery = ' Select * from master_region_country_map';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length == 0) {
            callback(req, res, {});
        } else {
            insertDataInCountry(req, res, results, 0, [], callback);
        }
    });
};
exports.getAllData = getAllData;

var insertDataInCountry = function (req, res, resultSet, currentIndex, arrFinalData, callback) {
    console.log(currentIndex);
    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var region = thisObj.region;
        var country = thisObj.country;

        var queryParam = [region];
        var strQuery = ' Select region_id from master_region where region_name = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            if (results.length == 0) {
                callback(req, res, {});
            } else {
                var rid = results[0].region_id;
                var data = {
                    region_id: rid,
                    country_name: country,
                    created_date: constants.CURR_UTC_DATETIME()
                };
                var queryParam = [data];
                var strQuery = 'insert into countries set ?';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    } else {
                        resultSet.splice(0, 1);
                        currentIndex++;
                        insertDataInCountry(req, res, resultSet, currentIndex, arrFinalData, callback);
                    }
                });
            }
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.insertDataInCountry = insertDataInCountry;

var getBroadCastMsg = function (req, res, callback) {
    var queryParam = [];
    var strQuery = "SELECT * from broadcast_messages where start_date <= '" + constants.CURR_UTC_DATETIME() + "' AND end_date >= '"+ constants.CURR_UTC_DATETIME() + "'";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            callback(req, res, results);
        } else {
            callback(req, res, {});
        }
    });
};
exports.getBroadCastMsg = getBroadCastMsg;