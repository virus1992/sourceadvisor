var exports = module.exports = {};


var async = require('async');

var miscModel = require('./misc_model');

var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var sendResponse = require('./../modules/sendresponse');
var frontConstant = require('./../modules/front_constant');

var stripe = require('stripe')(constants.STRIPE_SERVER_SECRET_KEY);


var getMeetingInviteInvoiceAndUserDetail = function (req, res, dataParams, callback) {

    var invoiceId = dataParams.invoiceId;
    var queryParam = [invoiceId];
    var strQuery = '   SELECT i.invoice_id invoiceId, i.user_id userId, i.invitation_id invitationId, i.meeting_id meetingId, i.mentor_charge mentor_charge, i.wissenx_user_percentage wissenxUserPercentage, i.wissenx_user_charge wissenxUserCharge, i.promocode_id promoCodeId, i.promocode_discount promoCodeDiscount, i.wissenx_tax_percentage wissenxTaxPercentage, i.wissenx_tax_charge wissenxTaxCharge, i.total_payable_amount totalPayAbleAmount, i.pay_to_mentor payToMentor, i.pay_to_wissenx payToMentor, i.paid_to_mentor paidToMentor, i.paid_to_wissenx paidToWissenx, i.status invoiceStatus, i.created_date invoiceCreatedDate, i.updated_time invoiceUpdatedDate, mi.invitation_id meetingInvitationId, mi.requester_id meetingRequesterId, mi.user_id meetingUserId, mi.invoice_id meetingInvoiceId, mi.meeting_type meetingType, mi.meeting_purpose meetingPurpose, mi.meeting_purpose_indetail meetingPurposeInDetail, mi.duration duration, mi.notified_to_user meetingNotifiedToUser, mi.notified_to_mentor meetingNotifiedToMentor, mi.status meetingStatus, mi.created_date meetingCreatedDate, mi.updated_date meetingUpdatedDate, u.user_id usersId, u.email userEmail, u.slug userSlug, u.stripe_customer_id userStripeCustomerNumber ' +
            '   FROM invoice i, meeting_invitations mi, users u ' +
            '   WHERE i.invitation_id = mi.invitation_id ' +
            '   AND i.user_id = u.user_id ' +
            '   AND i.invoice_id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrInviteInvoiceAndUserData, fields) {

        if (arrInviteInvoiceAndUserData.length == 0) {
            callback(req, res, []);
        } else {
            callback(req, res, arrInviteInvoiceAndUserData[0]);
        }
    });
}
exports.getMeetingInviteInvoiceAndUserDetail = getMeetingInviteInvoiceAndUserDetail;


var generateInvoiceForMeetingInvite = function (req, res, objDataParamsForPayment, callback) {
    console.log("objDataParamsForPayment in func :: ", objDataParamsForPayment);
    return false;

    var queryParam = [statusToUpdate, userId];
    var strQuery = ' UPDATE users u ' +
            ' SET u.status = ? ' +
            ' WHERE u.user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, userId);
    });

}
exports.generateInvoiceForMeetingInvite = generateInvoiceForMeetingInvite;





var createStripeCustomer = function (req, res, objDataParams, callback) {

    var email = objDataParams.userEmail;
    var stripeToken = objDataParams.stripeToken;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var stripeObj = {
        email: email
    };

    stripe.customers.create({
        description: 'Customer created for ' + email,
        source: stripeToken // obtained with Stripe.js
    },
            function (err, customer) {

                if (err) {
                    callback(req, res, []);
                } else {
                    var strJsonCustomer = JSON.stringify(customer);
                    // Insert into invoice table
                    var queryParam = {
                        user_id: req.session.userId,
                        id: customer.id,
                        default_source: customer.default_source,
                        stripe_customer_JSON: strJsonCustomer,
                        datex: currentUTC
                    };

                    var strQuery = 'insert into invoice SET ?';

                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {

                        var queryParamUpdateUser = [customer.id, req.session.userId];

                        var strQueryUpdateUser = ' UPDATE users u ' +
                                ' SET u.stripe_customer_id = ? ' +
                                ' WHERE u.user_id = ?';

                        dbconnect.executeQuery(req, res, strQueryUpdateUser, queryParamUpdateUser, function (req, res, error, results, fields) {
                            if (error) {
                                callback(req, res, []);
                            } else {
                                callback(req, res, queryParam);
                            }
                        });
                    });
                }

            });
}
exports.createStripeCustomer = createStripeCustomer;

var updateInvoiceAndInsertPayment = function (req, res, dataParams, callback) {


    var dataParamsInvoice = dataParams.dataParamsInvoice;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [
        dataParamsInvoice.mentor_hourly_charge,
        dataParamsInvoice.mentor_charge,
        dataParamsInvoice.wissenx_user_percentage,
        dataParamsInvoice.wissenx_user_charge,
        dataParamsInvoice.wissenx_mentor_percentage,
        dataParamsInvoice.wissenx_mentor_charge,
        dataParamsInvoice.promocode_id,
        dataParamsInvoice.promocode_discount,
        dataParamsInvoice.wissenx_tax_percentage,
        dataParamsInvoice.wissenx_tax_charge,
        dataParamsInvoice.total_payable_amount,
        dataParamsInvoice.pay_to_mentor,
        dataParamsInvoice.pay_to_wissenx,
        dataParamsInvoice.refund_type,
        dataParamsInvoice.final_pay_to_wissenx,
        dataParamsInvoice.final_pay_to_mentor,
        dataParamsInvoice.final_user_paid_amount,
        'paid',
        currentUTC,
        '1',
        dataParams.requestBody.invoiceId
    ];

    var strQuery = ' UPDATE invoice i, meeting_invitations mi ' +
            ' SET i.mentor_hourly_charge = ?, ' +
            ' i.mentor_charge = ?, ' +
            ' i.wissenx_user_percentage = ?,  ' +
            ' i.wissenx_user_charge = ?,  ' +
            ' i.wissenx_mentor_percentage = ?,  ' +
            ' i.wissenx_mentor_charge = ?,  ' +
            ' i.promocode_id = ?,  ' +
            ' i.promocode_discount = ?,  ' +
            ' i.wissenx_tax_percentage = ?,  ' +
            ' i.wissenx_tax_charge = ?,  ' +
            ' i.total_payable_amount = ?,  ' +
            ' i.pay_to_mentor = ?,  ' +
            ' i.pay_to_wissenx = ?,  ' +
            ' i.refund_type = ?,  ' +
            ' i.final_pay_to_wissenx = ?,  ' +
            ' i.final_pay_to_mentor = ?,  ' +
            ' i.final_user_paid_amount = ?,  ' +
            ' i.status = ?,  ' +
            ' i.updated_time = ?, ' +
            ' mi.flag_payment_done = ? ' +
            ' WHERE i.invoice_id = ? ' +
            ' AND i.invitation_id = mi.invitation_id';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, false);
        } else {

            if (dataParamsInvoice.promocode_id > 0) {
                var queryParam = [
                    currentUTC,
                    dataParamsInvoice.promocode_id
                ];

                var strQuery = ' UPDATE promo_codes p ' +
                        ' SET p.is_used = 1, ' +
                        ' p.used_date = ? ' +
                        ' WHERE p.promo_code_id = ? ';

                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        callback(req, res, false);
                    } else {
                        var strJSONPayment = JSON.stringify(dataParams.dataParamsForChargeCreate);
                        var strJSONToken = JSON.stringify(dataParams.requestBody);
                        var paypal_id = dataParams.dataParamsForChargeCreate.chargeData.id;
                        var transection_id = dataParams.dataParamsForChargeCreate.chargeData.transactions[0].related_resources[0].authorization.id;

                        var params = {
                            invoice_id: dataParams.requestBody.invoiceId,
                            amount: dataParamsInvoice.total_payable_amount,
                            paypal_token: paypal_id,
                            order_id: "WIS-" + dataParams.requestBody.invoiceId,
                            transaction_id: transection_id,
                            payment_date: currentUTC,
                            tokenrequest: strJSONToken,
                            customerchargerequest: strJSONPayment,
                            created_date: currentUTC
                        };

                        var strQuery = 'INSERT INTO payments_paypal SET ? ';
                        dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
                            if (error) {
                                callback(req, res, false);
                            } else {
                                callback(req, res, true);
                            }
                        });
                    }
                });
            } else {
                var strJSONPayment = JSON.stringify(dataParams.dataParamsForChargeCreate);
                var strJSONToken = JSON.stringify(dataParams.requestBody);
                var paypal_id = dataParams.dataParamsForChargeCreate.chargeData.id;
                var transection_id = dataParams.dataParamsForChargeCreate.chargeData.transactions[0].related_resources[0].authorization.id;

                var params = {
                    invoice_id: dataParams.requestBody.invoiceId,
                    amount: dataParamsInvoice.total_payable_amount,
                    paypal_token: paypal_id,
                    order_id: "WIS-" + dataParams.requestBody.invoiceId,
                    transaction_id: transection_id,
                    payment_date: currentUTC,
                    tokenrequest: strJSONToken,
                    customerchargerequest: strJSONPayment,
                    created_date: currentUTC
                };

                var strQuery = 'INSERT INTO payments_paypal SET ? ';
                dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
                    if (error) {
                        callback(req, res, false);
                    } else {
                        callback(req, res, true);
                    }
                });
            }
        }
    });
}
exports.updateInvoiceAndInsertPayment = updateInvoiceAndInsertPayment;


var getMeetingInvitationInvoiceAllData = function (req, res, objDataParams, callback) {

    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;
    var cancellationFor = objDataParams.cancellationFor;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: cancellationFor,
        getFullData: true
    };

    miscModel.getCancellationPriceForUser(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {
        callback(req, res, objDataResponse);
    });
}
exports.getMeetingInvitationInvoiceAllData = getMeetingInvitationInvoiceAllData;


var updateInvoiceForPrivateMeetingTimeOut = function (req, res, objDataParams, callback) {
    /*console.log("---------------updateInvoiceForPrivateMeetingTimeOut START-----------------");
     console.log("objDataParams :: ", objDataParams);
     console.log("---------------updateInvoiceForPrivateMeetingTimeOut END-----------------");*/
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationCharges = objDataResponse.objMentorCancellationCharges;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "PVTINTO",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForPrivateMeetingTimeOut = updateInvoiceForPrivateMeetingTimeOut;


var updateInvoiceForNoShowBothParties = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNoShowBothParties START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNoShowBothParties END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "user"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationPolicy = objDataResponse.objMentorCancellationPolicy;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;
        var mentorPolicyAtTimeOfBooking = objDataResponse.mentorPolicyAtTimeOfBooking;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "NOSHOWBOTH",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            
            objDataResponse.amountBackToUser = amountBackToUser;
            objDataResponse.wissenxCut = "0.00";
            objDataResponse.mentorsCut = "0.00";
            objDataResponse.amountToRefund = "0.00";
            objDataResponse.invoiceId = invoiceId;
            
            
            callback(req, res, objDataResponse);
        });
    });

}
exports.updateInvoiceForNoShowBothParties = updateInvoiceForNoShowBothParties;



var updateInvoiceForNoShowByMentor = function (req, res, objDataParams, callback) {
    /*console.log("---------------updateInvoiceForNoShowByMentor START-----------------");
     console.log("objDataParams :: ", objDataParams);
     console.log("---------------updateInvoiceForNoShowByMentor END-----------------");*/
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;


    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "user"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationPolicy = objDataResponse.objMentorCancellationPolicy;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;
        var mentorPolicyAtTimeOfBooking = objDataResponse.mentorPolicyAtTimeOfBooking;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "NOSHOWMENT",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "30.00",
            final_pay_to_mentor: "-30.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            objDataResponse.amountBackToUser = amountBackToUser;
            objDataResponse.wissenxCut = "30.00";
            objDataResponse.mentorsCut = "-30.00";
            objDataResponse.amountToRefund = "0.00";
            objDataResponse.invoiceId = invoiceId;

            callback(req, res, objDataResponse);
        });

    });
}
exports.updateInvoiceForNoShowByMentor = updateInvoiceForNoShowByMentor;



var updateInvoiceForNoShowByUser = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNoShowByUser START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNoShowByUser END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;


    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "user"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationPolicy = objDataResponse.objMentorCancellationPolicy;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;
        var mentorPolicyAtTimeOfBooking = objDataResponse.mentorPolicyAtTimeOfBooking;
        var objCancellationPolicyForUser = objDataResponse.objCancellationPolicyForUser;



        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var cancellationPercentage = objCancellationPolicyForUser.cancellationPercentage;

        var wissenxUserCharge = objPrivateMeetingDetailForCancellationPurpose.wissenx_user_charge;
        var mentorCharge = objPrivateMeetingDetailForCancellationPurpose.mentor_charge;


        var mentorsCut = parseFloat(parseFloat(mentorCharge) * parseFloat(parseFloat(cancellationPercentage) / 100)).toFixed(2);
        var wissenxCut = parseFloat(parseFloat(wissenxUserCharge) * parseFloat(parseFloat(cancellationPercentage) / 100)).toFixed(2);
        amountBackToUser = parseFloat(parseFloat(amountBackToUser) - parseFloat(amountToRefund)).toFixed(2);

        var params = {
            refund_type: "NOSHOWUSR",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: wissenxCut,
            final_pay_to_mentor: mentorsCut,
            final_user_paid_amount: amountToRefund
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            objDataResponse.amountBackToUser = amountBackToUser;
            objDataResponse.wissenxCut = wissenxCut;
            objDataResponse.mentorsCut = mentorsCut;
            objDataResponse.amountToRefund = amountToRefund;
            objDataResponse.invoiceId = invoiceId;

            callback(req, res, objDataResponse);
        });

    });
};
exports.updateInvoiceForNoShowByUser = updateInvoiceForNoShowByUser;

var updateInvoiceForMentorCancellAcceptedInvite = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForMentorCancellAcceptedInvite START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForMentorCancellAcceptedInvite END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;


    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationPolicy = objDataResponse.objMentorCancellationPolicy;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;
        var mentorPolicyAtTimeOfBooking = objDataResponse.mentorPolicyAtTimeOfBooking;
        var objCancellationPolicyForUser = objDataResponse.objCancellationPolicyForUser;



        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var fineToMentor = parseFloat(parseFloat(-1) * parseFloat(amountToRefund)).toFixed(2);

        var params = {
            refund_type: "ACCMTCANMENT",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: amountToRefund,
            final_pay_to_mentor: fineToMentor,
            final_user_paid_amount: "0.00"
        };


        var queryParam = [params, invoiceId];

        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            objDataResponse.amountBackToUser = amountBackToUser;
            objDataResponse.wissenxCut = amountToRefund;
            objDataResponse.mentorsCut = fineToMentor;
            objDataResponse.amountToRefund = amountToRefund;
            objDataResponse.invoiceId = invoiceId;


            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForMentorCancellAcceptedInvite = updateInvoiceForMentorCancellAcceptedInvite;



var updateInvoiceForUserCancellAcceptedInvite = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForUserCancellAcceptedInvite START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForUserCancellAcceptedInvite END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;


    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "user"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationPolicy = objDataResponse.objMentorCancellationPolicy;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;
        var mentorPolicyAtTimeOfBooking = objDataResponse.mentorPolicyAtTimeOfBooking;
        var objCancellationPolicyForUser = objDataResponse.objCancellationPolicyForUser;



        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var cancellationPercentage = objCancellationPolicyForUser.cancellationPercentage;

        var wissenxUserCharge = objPrivateMeetingDetailForCancellationPurpose.wissenx_user_charge;
        var mentorCharge = objPrivateMeetingDetailForCancellationPurpose.mentor_charge;


        var mentorsCut = parseFloat(parseFloat(mentorCharge) * parseFloat(parseFloat(cancellationPercentage) / 100)).toFixed(2);
        var wissenxCut = parseFloat(parseFloat(wissenxUserCharge) * parseFloat(parseFloat(cancellationPercentage) / 100)).toFixed(2);
        amountBackToUser = parseFloat(parseFloat(amountBackToUser) - parseFloat(amountToRefund)).toFixed(2);

        var params = {
            refund_type: "ACCMTCANUSR",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: wissenxCut,
            final_pay_to_mentor: mentorsCut,
            final_user_paid_amount: amountToRefund
        };
        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            objDataResponse.amountBackToUser = amountBackToUser;
            objDataResponse.wissenxCut = wissenxCut;
            objDataResponse.mentorsCut = mentorsCut;
            objDataResponse.amountToRefund = amountToRefund;
            objDataResponse.invoiceId = invoiceId;

            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForUserCancellAcceptedInvite = updateInvoiceForUserCancellAcceptedInvite;


var updateInvoiceForNormalInviteDeclineByMentor = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNormalInviteDeclineByMentor START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNormalInviteDeclineByMentor END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationCharges = objDataResponse.objMentorCancellationCharges;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "NIDECBYMEN",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForNormalInviteDeclineByMentor = updateInvoiceForNormalInviteDeclineByMentor;



var updateInvoiceForNormalInviteDeclineByUser = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNormalInviteDeclineByUser START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNormalInviteDeclineByUser END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationCharges = objDataResponse.objMentorCancellationCharges;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "NIDECBYMEN",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForNormalInviteDeclineByUser = updateInvoiceForNormalInviteDeclineByUser;



var updateInvoiceForNormalInviteCancellByUser = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNormalInviteCancellByUser START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNormalInviteCancellByUser END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationCharges = objDataResponse.objMentorCancellationCharges;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "NICANBYUSR",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForNormalInviteCancellByUser = updateInvoiceForNormalInviteCancellByUser;



var updateInvoiceForNormalInviteCancellByMentor = function (req, res, objDataParams, callback) {
    console.log("---------------updateInvoiceForNormalInviteCancellByMentor START-----------------");
    console.log("objDataParams :: ", objDataParams);
    console.log("---------------updateInvoiceForNormalInviteCancellByMentor END-----------------");
    var invitationId = objDataParams.invitationId;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;

    var objToGetPaymentInfo = {
        userId: meetingUserId,
        mentorId: meetingMentorId,
        invitationId: invitationId,
        cancellationFor: "mentor"
    };

    getMeetingInvitationInvoiceAllData(req, res, objToGetPaymentInfo, function (req, res, objDataResponse) {

        var objMeetingDetailForCancellationPurpose = objDataResponse.objMeetingDetailForCancellationPurpose;
        var objMentorCancellationCharges = objDataResponse.objMentorCancellationCharges;
        var objPrivateMeetingDetailForCancellationPurpose = objDataResponse.objPrivateMeetingDetailForCancellationPurpose;
        var amountToRefund = objDataResponse.amountToRefund;

        var amountBackToUser = objPrivateMeetingDetailForCancellationPurpose.total_payable_amount;
        var invoiceId = objPrivateMeetingDetailForCancellationPurpose.invoice_id;

        var params = {
            refund_type: "PNTINVCANMENT",
            refund_paid_to_user: amountBackToUser,
            final_pay_to_wissenx: "0.00",
            final_pay_to_mentor: "0.00",
            final_user_paid_amount: "0.00"
        };

        var queryParam = [params, invoiceId];
        var strQuery = " UPDATE invoice SET ? WHERE invoice_id = ? ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, objDataResponse);
        });
    });
}
exports.updateInvoiceForNormalInviteCancellByMentor = updateInvoiceForNormalInviteCancellByMentor;




var getInvoiceAndPaymentData = function (req, res, invoiceId, callback) {

    async.parallel([
        function (paymentAsyncCallback) {
            getInvoiceAndPaymentDataFunction(req, res, invoiceId, function (req, res, arrData) {
                paymentAsyncCallback(null, arrData);
            });
        },
        function (paymentAsyncCallback) {
            getSlotsBookedForInvoice(req, res, invoiceId, function (req, res, arrSlotInformation) {
                paymentAsyncCallback(null, arrSlotInformation);
            });
        }
    ], function (err, results) {
        var finalResponse = {};
        finalResponse.arrInvoiceAndPaymentData = results[0];
        finalResponse.arrSlotsInformation = results[1];
        callback(req, res, finalResponse);
    });

}
exports.getInvoiceAndPaymentData = getInvoiceAndPaymentData;




var getInvoiceAndPaymentDataFunction = function (req, res, invoiceId, callback) {
    var queryParam = [invoiceId];
    var strQuery = '   SELECT i.invoice_id invoiceId, i.user_id userId, i.invitation_id invitationId, ' +
            '   i.meeting_id meetingId, i.mentor_charge mentor_charge, i.wissenx_user_percentage wissenxUserPercentage, ' +
            '   i.wissenx_user_charge wissenxUserCharge, i.promocode_id promoCodeId, i.promocode_discount promoCodeDiscount, ' +
            '   i.wissenx_tax_percentage wissenxTaxPercentage, i.wissenx_tax_charge wissenxTaxCharge, i.total_payable_amount totalPayAbleAmount, ' +
            '   i.pay_to_mentor payToMentor, i.pay_to_wissenx payToMentor, i.paid_to_mentor paidToMentor, i.paid_to_wissenx paidToWissenx, ' +
            '   i.status invoiceStatus, i.created_date invoiceCreatedDate, i.updated_time invoiceUpdatedDate,  p.payment_id paymentId, ' +
            '   p.invoice_id invoiceInPaymentId, p.amount paymentAmount, p.paypal_token paymentPaypalToken, p.order_id paymentOrderId, ' +
            '   p.transaction_id paymentTransactionId, p.payment_date paymentDate, p.created_date paymentCreated ' +
            '   FROM invoice i, payments_paypal p ' +
            '   WHERE i.invoice_id = p.invoice_id ' +
            '   AND i.invoice_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrInvoiceAndPaymentData, fields) {
        if (arrInvoiceAndPaymentData.length == 0) {
            callback(req, res, []);
        } else {
            callback(req, res, arrInvoiceAndPaymentData[0]);
        }
    });
}
exports.getInvoiceAndPaymentDataFunction = getInvoiceAndPaymentDataFunction;


var getSlotsBookedForInvoice = function (req, res, invoiceId, callback) {

    var queryParam = [invoiceId];
    var strQuery = '   SELECT  mi.duration, mism.*, ' +
            '   DATE_FORMAT(CONVERT_TZ(CONCAT_WS(" ", mism.date, mism.time), "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "%dth %M, %Y %h:%i %p") slotStart, ' +
            '   DATE_FORMAT(CONVERT_TZ(CONCAT_WS(" ", mism.date, ADDTIME(mism.time, SEC_TO_TIME(mi.duration * 60))), "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "%h:%i %p") slotEnd ' +
            '   FROM invoice i, meeting_invitations mi, meeting_invitation_slot_mapping mism ' +
            '   WHERE i.invitation_id = mi.invitation_id ' +
            '   AND mism.invitation_id = mi.invitation_id ' +
            '   AND i.invoice_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSlotRequestedData, fields) {
        if (arrSlotRequestedData.length == 0) {
            callback(req, res, []);
        } else {
            callback(req, res, arrSlotRequestedData);
        }
    });
}
exports.getSlotsBookedForInvoice = getSlotsBookedForInvoice;

var updateWalletBalance = function (req, res, objParams, callback) {

    var amount = objParams.amount;
    var id = objParams.user_id;
    var queryParam = [amount, id];
    var strQuery = 'UPDATE mentor_details SET wallet_balance = wallet_balance + ? where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSlotRequestedData, fields) {
        if (error) {
            callback(req, res, 'error');
        } else {
            callback(req, res, 'success');
        }
    });
};
exports.updateWalletBalance = updateWalletBalance;