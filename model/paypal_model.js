var exports = module.exports = {};


var async = require('async');

var miscModel = require('./misc_model');

var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var sendResponse = require('./../modules/sendresponse');
var frontConstant = require('./../modules/front_constant');

var axios = require('axios');
var curl = require("curljs");


var stripe = require('stripe')(constants.STRIPE_SERVER_SECRET_KEY);
var paypal_api = require('paypal-rest-sdk');

var config_opts = {
    'host': constants.HOST,
    'port': constants.PORT,
    'mode': constants.MODE,
    'client_id': constants.CLIENT_ID,
    'client_secret': constants.CLIENT_SECRET
};

var paypalAuthPayment = function (req, res, objParams, callback) {

    var curreny = constants.PAYPAL_CURRENCY;
    var amount = objParams.amount;
    var cardNumber = objParams.cardNumber;
    var cardExpiryMM = objParams.cardExpiryMM;
    var cardExpiryYY = objParams.cardExpiryYY;
    var cardCVC = objParams.cardCVC;
    var cardType = objParams.cardType;
    var reason = objParams.reason;
    var orderId = "WIS-" + objParams.orderId;
    var user_id = objParams.user_id;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var create_payment_json = {
        "intent": "authorize",
        "payer": {
            "payment_method": "credit_card",
            "funding_instruments": [{
                    "credit_card": {
                        "type": cardType,
                        "number": cardNumber,
                        "expire_month": cardExpiryMM,
                        "expire_year": cardExpiryYY,
                        "cvv2": cardCVC,
                        "first_name": req.session.firstName,
                        "last_name": req.session.lastName
                    }
                }]
        },
        "transactions": [{
                "amount": {
                    "total": amount,
                    "currency": curreny
                },
                "description": "Charges for meetings",
                "invoice_number": orderId + Math.random() * 10000000000000000,
                "payment_options":
                        {
                            "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
                        },
            }]
    };

    paypal_api.payment.create(create_payment_json, config_opts, function (err, payment) {
        if (err) {

            console.log(err);

            var data = {
                invoice_id: objParams.orderId,
                user_id: 0,
                user_type: "debug",
                reason: reason,
                payment_type: null,
                transection_type: "PAYAUTH_FAIL",
                payment_id: null,
                transection_id: null,
                extraData: JSON.stringify(err),
                capture_id: null,
                refund_id: null,
                amount: amount,
                status: "failed",
                created_date: currentUTC
            };
            insertWallet(req, res, data, function (req, res, result) { });

            if (err.response.name != "UNKNOWN_ERROR") {
                if (err.response.length > 0) {
                    callback(req, res, err.response.details[0].issue, {});
                } else {
                    callback(req, res, "Please enter proper card details", {});
                    // callback(req, res, JSON.stringify(err.response.details), {});
                }
            } else {
                callback(req, res, "Please enter proper card details.", {});
                // callback(req, res, JSON.stringify(err.response.details), {});
            }
        } else if (payment) {            

            if (payment.transactions[0].related_resources.length != 0) {

                var data = {
                    invoice_id: objParams.orderId,
                    user_id: user_id,
                    user_type: "user",
                    reason: reason,
                    payment_type: "",
                    transection_type: "PAYAUTH",
                    payment_id: payment.id,
                    transection_id: payment.transactions[0].related_resources[0].authorization.id,
                    capture_id: null,
                    refund_id: null,
                    amount: amount,
                    status: payment.state,
                    created_date: currentUTC
                };

                insertWallet(req, res, data, function (req, res, result) {
                    callback(req, res, null, payment);
                });
            } else {
                callback(req, res, "Please enter proper card details.", {});
            }
        } else {
            callback(req, res, "Please enter proper card details.", {});
        }
    });
};
exports.paypalAuthPayment = paypalAuthPayment;


var paypalCapturePayment = function (req, res, dataParams, callback) {

    var curreny = constants.PAYPAL_CURRENCY;
    var amount = dataParams.amount;
    var transactionId = dataParams.transactionId;
    var invoiceid = dataParams.invoiceid;
    var reason = dataParams.reason;
    var paymentAction = dataParams.paymentAction;
    var user_id = dataParams.user_id;
    //var user_id = req.session.userId;
    var user_type = dataParams.userType;
    var currentUTC = constants.CURR_UTC_DATETIME();


    var capture_details = {
        "amount": {
            "currency": curreny,
            "total": amount
        },
        "is_final_capture": true
    };

    paypal_api.authorization.capture(transactionId, capture_details, config_opts, function (error, payment) {
        if (error) {
            var request = {
                capture_details: capture_details,
                transactionId: transactionId
            };

            var requestData = JSON.stringify(request);
            var errorData = JSON.stringify(error);

            var data = {
                request: requestData,
                invoice_id: invoiceid,
                paymentaction: paymentAction,
                request_status: "OK",
                response: errorData,
                response_status: error.state
            };

            insertLog(req, res, data, function (req, res, result) {
                callback(req, res, "error");
            });
        } else {
            var capture_id = payment.id;
            var queryParam = [capture_id, transactionId];
            var sqlQuery = 'Update payments_paypal set capture_id = ? where transaction_id = ?';
            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    callback(req, res, "success");
                } else {

                    var data = {
                        invoice_id: invoiceid,
                        user_id: user_id,
                        user_type: user_type,
                        reason: reason,
                        transection_type: "PAYCAP",
                        payment_type: "DEBITED",
                        payment_id: payment.parent_payment,
                        transection_id: transactionId,
                        capture_id: capture_id,
                        refund_id: null,
                        amount: amount,
                        status: payment.state,
                        created_date: currentUTC
                    };

                    insertWallet(req, res, data, function (req, res, result) {
                        if (result == 'error') {
                            callback(req, res, "error");
                        } else {

                            var request = {
                                capture_details: capture_details,
                                transactionId: transactionId
                            };

                            var requestData = JSON.stringify(request);
                            var paymentData = JSON.stringify(payment);

                            var data = {
                                request: requestData,
                                invoice_id: invoiceid,
                                paymentaction: paymentAction,
                                request_status: "OK",
                                response: paymentData,
                                response_status: payment.state
                            };
                            insertLog(req, res, data, function (req, res, result) {
                                callback(req, res, "success");
                            });
                        }
                    });
                }
            });
        }
    });
};
exports.paypalCapturePayment = paypalCapturePayment;


var paypalVoidPayment = function (req, res, dataParams, callback) {

    var transactionId = dataParams.transactionId;
    var invoiceid = dataParams.invoiceid;
    var paymentAction = dataParams.paymentAction;
    var amount = dataParams.amount;
    var reason = dataParams.reason;
    var user_id = dataParams.user_id;
    var user_type = dataParams.user_type;
    //var user_id = req.session.userId;
    //var user_type = req.session.userType;
    var currentUTC = constants.CURR_UTC_DATETIME();


    if (reason != "MEETINVICANMTR" && reason != "MEETINVIDECUSR") {
        paypal_api.authorization.void(transactionId, config_opts, function (error, payment) {
            if (error) {
                var request = {
                    transactionId: transactionId
                };

                var requestData = JSON.stringify(request);
                var errorData = JSON.stringify(error);

                var data = {
                    request: requestData,
                    invoice_id: invoiceid,
                    paymentaction: paymentAction,
                    request_status: "OK",
                    response: errorData
                };

                insertLog(req, res, data, function (req, res, result) {
                    callback(req, res, "error");
                });
            } else {

                var data = {
                    invoice_id: invoiceid,
                    user_id: user_id,
                    user_type: user_type,
                    reason: reason,
                    transection_type: "PAYVOID",
                    payment_type: "VOIDED",
                    payment_id: payment.parent_payment,
                    transection_id: transactionId,
                    capture_id: null,
                    refund_id: payment.id,
                    amount: amount,
                    status: payment.state,
                    created_date: currentUTC
                };

                insertWallet(req, res, data, function (req, res, result) {
                    if (result == 'error') {
                        callback(req, res, "error");
                    } else {
                        var request = {
                            transactionId: transactionId
                        };

                        var requestData = JSON.stringify(request);
                        var paymentData = JSON.stringify(payment);

                        var data = {
                            request: requestData,
                            invoice_id: invoiceid,
                            paymentaction: paymentAction,
                            request_status: "OK",
                            response: paymentData,
                            response_status: payment.state
                        };
                        insertLog(req, res, data, function (req, res, result) {
                            callback(req, res, "success");
                        });
                    }
                });
            }
        });
    } else {
        callback(req, res, "success");
    }
};
exports.paypalVoidPayment = paypalVoidPayment;


var paypalRefundPayment = function (req, res, dataParams, callback) {

    var captureId = dataParams.captureId;
    var amount = dataParams.amount;
    var invoiceid = dataParams.invoiceid;
    var paymentAction = dataParams.paymentAction;
    var reason = dataParams.reason;
    var curreny = constants.PAYPAL_CURRENCY;
    var user_id = dataParams.user_id;
    var user_type = dataParams.user_type;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var refund_details = {
        "amount": {
            "currency": curreny,
            "total": amount
        }
    };

    paypal_api.capture.refund(captureId, refund_details, config_opts, function (error, payment) {
        if (error) {
            var request = {
                captureId: captureId,
                refund_details: refund_details
            };

            var requestData = JSON.stringify(request);
            var errorData = JSON.stringify(error);

            var data = {
                request: requestData,
                invoice_id: invoiceid,
                paymentaction: paymentAction,
                request_status: "OK",
                response: errorData
            };

            insertLog(req, res, data, function (req, res, result) {
                callback(req, res, "error");
            });
        } else {

            var data = {
                invoice_id: invoiceid,
                user_id: user_id,
                user_type: user_type,
                reason: reason,
                transection_type: "PAYREF",
                payment_type: "REFUNDED",
                payment_id: payment.parent_payment,
                transection_id: null,
                capture_id: payment.capture_id,
                refund_id: payment.id,
                amount: amount,
                status: payment.state,
                created_date: currentUTC
            };

            insertWallet(req, res, data, function (req, res, result) {
                if (result == 'error') {
                    callback(req, res, "error");
                } else {

                    var request = {
                        captureId: captureId,
                        refund_details: refund_details
                    };

                    var requestData = JSON.stringify(request);
                    var paymentData = JSON.stringify(payment);

                    var data = {
                        request: requestData,
                        invoice_id: invoiceid,
                        paymentaction: paymentAction,
                        request_status: "OK",
                        response: paymentData,
                        response_status: payment.state
                    };

                    var retParam = {
                        payment_id: payment.parent_payment,
                        capture_id: payment.capture_id,
                        refund_id: payment.id
                    };

                    insertLog(req, res, data, function (req, res, result) {
                        callback(req, res, retParam);
                    });
                }
            });
        }
    });
};
exports.paypalRefundPayment = paypalRefundPayment;


var paypalPayout = function (req, res, dataParams, callback) {

    var curreny = constants.PAYPAL_CURRENCY;
    var user_type = dataParams.userType;
    var user_id = dataParams.user_id;
    var id = dataParams.id;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var txtEmail = dataParams.txtEmail;
    var txtAmount = parseFloat(dataParams.txtAmount).toFixed(2);

    var sender_batch_id = Math.random().toString(36).substring(9);

    var create_payout_json = {
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": txtAmount,
                    "currency": curreny
                },
                "receiver": txtEmail,
                "note": "Thank you.",
                "sender_item_id": "item1"
            }
        ]
    };

    var sync_mode = 'false';

    paypal_api.payout.create(create_payout_json, sync_mode, config_opts, function (error, payout) {

        //paypal_api.payout.create(create_payout_json, config_opts, function (error, payout) {
        if (error) {
            console.log(error.response);
            var errorData = JSON.stringify(error);

            var data = {
                request: "",
                invoice_id: "",
                paymentaction: "Payout",
                request_status: "OK",
                response: errorData,
                response_status: error.state
            };

            insertLog(req, res, data, function (req, res, result) {
                callback(req, res, "error");
            });
        } else {
            if (payout.httpStatusCode == "201") {
                var queryParam = [txtAmount, currentUTC, id];
                var sqlQuery = 'Update payout_request set status = 1 , amount_paid = ? , updated_date = ? where id = ?';
                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        callback(req, res, "error");
                    } else {

                        var data = {
                            invoice_id: null,
                            user_id: user_id,
                            user_type: "mentor",
                            reason: "PAYBYADMIN",
                            transection_type: "PAYOUT",
                            //payment_type: "DEBITED",
                            payment_type: "PAYOUT",
                            payment_id: null,
                            transection_id: payout.batch_header.payout_batch_id,
                            capture_id: null,
                            refund_id: null,
                            amount: txtAmount,
                            status: "completed",
                            created_date: currentUTC
                        };

                        insertWallet(req, res, data, function (req, res, result) {
                            if (result == 'error') {
                                callback(req, res, "error");
                            } else {

                                var request = {
                                    capture_details: create_payout_json,
                                    transactionId: payout.batch_header.payout_batch_id
                                };

                                var requestData = JSON.stringify(request);

                                var data = {
                                    request: requestData,
                                    invoice_id: "",
                                    paymentaction: "Payout",
                                    request_status: "OK",
                                    response: "",
                                    response_status: ""
                                };
                                insertLog(req, res, data, function (req, res, result) {
                                    callback(req, res, "success");
                                });
                            }
                        });
                    }
                });
            } else {
                callback(req, res, "error");
            }
        }
    });
};
exports.paypalPayout = paypalPayout;

var insertWallet = function (req, res, data, callback) {

    var queryParam = [data];
    var sqlQuery = 'insert into wallet_transection set ?';
    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, "error");
        } else {
            callback(req, res, "success");
        }
    });
};
exports.insertWallet = insertWallet;
var insertLog = function (req, res, data, callback) {
    callback(req, res, "success");
//    var queryParam = [data];
//    var sqlQuery = 'insert into paymentlog set ?';
//    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
//        if (error) {
//            callback(req, res, "error");
//        } else {
//            callback(req, res, "success");
//        }
//    });
};
exports.insertLog = insertLog;
