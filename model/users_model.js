var exports = module.exports = {};
var dateFormat = require('dateformat');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var fs = require('fs');
var multer = require('multer');
var formidable = require('formidable');
var path = require('path');
var slug = require('slug');
var request = require('request').defaults({encoding: null});
slug.defaults.mode = 'pretty';
var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var sendResponse = require('./../modules/sendresponse');
var miscFunction = require('./../model/misc_model');
var messagesModel = require('./../model/messages_model');
var mentorModel = require('./../model/mentor_model');
var cronModel = require('./../model/cron_model');
var paymentModel = require('./../model/payment_model');
var paypalFunction = require('./../model/paypal_model');
var async = require("async");
var frontConstant = require('./../modules/front_constant');
var handlebars = require('handlebars');

var userNotAuthentication = function (req, res, callback) {
    if (req.session.userId) {
        res.redirect('/dashboard');
    } else {
        callback(req, res);
    }
};
exports.userNotAuthentication = userNotAuthentication;
var checkMentorPendingMeetings_model = function (req, res) {
    var queryParam = [req.session.userId, 'accepted'];
    var strQuery = 'SELECT COUNT(invitation_id) AS pendingMeetingCount FROM meeting_invitations WHERE user_id=? AND status = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'NO_DELETE_ERROR');
        } else {
            if (results[0].pendingMeetingCount > 0) {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'NO_DELETE');
            } else {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'DELETE');
            }
        }
    });
};
exports.checkMentorPendingMeetings_model = checkMentorPendingMeetings_model;
var userAuthentication = function (req, res, callback) {

    // req.session.timezone = 'Asia/Kolkata';
    // req.session.USERTZBROWSER = '+05:30';
    // req.session.userId = 245;
    // req.session.firstName = 'MILIND';
    // req.session.lastName = 'PASI';
    // req.session.userType = 'mentor';
    // req.session.finalProfileImageUrl = 'http://localhost:3038/images/noimg.png';

    if (req.session.userId) {
        callback(req, res);
    } else {
        req.flash('errors', 'User not logged in');
        res.redirect('/login');
    }
};
exports.userAuthentication = userAuthentication;
var userAPIAuthentication = function (req, res, callback) {

    // req.session.timezone = 'Asia/Kolkata';
    // req.session.USERTZBROWSER = '+05:30';
    // req.session.userId = 245;
    // req.session.firstName = 'MILIND';
    // req.session.lastName = 'PASI';
    // req.session.userType = 'mentor';
    // req.session.finalProfileImageUrl = 'http://localhost:3038/images/noimg.png';

    if (req.session.userId) {
        callback(req, res);
    } else {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": {},
            "error": "1",
            "flagMsg": "USRNLOGIN"
        }));
        res.end();
    }
};
exports.userAPIAuthentication = userAPIAuthentication;
var unsetUserSession = function (req, res, callback) {
    req.session.destroy(req, res, function (err) {
        if (err) {
            throw err;
        }
        callback(req, res);
    });
};
exports.unsetUserSession = unsetUserSession;
var checkUserByUserName = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var first_name = reqBody.first_name;
    var last_name = reqBody.last_name;
    var email = reqBody.email;
    var dob = reqBody.dob;
    var password = reqBody.password;
    var cnfpassword = reqBody.cnfpassword;
    req.checkBody('first_name', 'First name is empty.').notEmpty();
    req.checkBody('last_name', 'Last name is empty.').notEmpty();
    req.checkBody('email', 'Enter valid email.').isEmail();
    //req.checkBody('lstCountry', 'Please select country.').isInt();
    //req.checkBody('mobile', 'Please enter mobile.').isInt();
    req.checkBody('dob', 'Please enter valid date.').notEmpty();
    req.checkBody('password', 'Password is empty.').notEmpty();
    req.checkBody('cnfpassword', 'Confirm password is not matched with password.').equals(reqBody.password);
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/register');
    } else {
        var queryParam = [email];
        var strQuery = ' SELECT u.user_id userId, u.email email, u.password password, u.status status ' +
                ' FROM users u ' +
                ' WHERE u.email = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            // found user
            if (results[0]) {
                if (results[0].status == 2) {
                    //req.flash('errors', 'Account is currently Inactive. To Activate enter the e-mail associated with the Acct.');
                    req.flash('errors', 'Please check your email to complete registration');
                    res.redirect('/activateAccount');
                } else {

                    req.flash('errors', 'User already registered.');
                    res.redirect('/register');
                }
            } else {
                callback(req, res, null, null, null);
            }
        });
    }
};
exports.checkUserByUserName = checkUserByUserName;
var checkUserSlug = function (req, res, slugName, callback) {

    var slugName = slugName;
    var slugToCheck = slug(slugName);
    var queryParam = [slugToCheck];
    var strQuery = ' SELECT * ' +
            ' FROM users u ' +
            ' WHERE u.slug = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        // slug found.
        if (results[0]) {
            var min = 100;
            var max = 999;
            var num = Math.floor(Math.random() * (max - min + 1)) + min;
            var nextSlugToCheck = slugName + num;
            checkUserSlug(req, res, nextSlugToCheck, callback);
        } else {
            callback(req, res, slugToCheck);
        }
    });
};
exports.checkUserSlug = checkUserSlug;
var createNewUser = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var first_name = reqBody.first_name;
    var last_name = reqBody.last_name;
    var email = reqBody.email;
    var dob = reqBody.dob;
    var password = reqBody.password;
    var cnfpassword = reqBody.cnfpassword;
    var hidTimeZoneLabel = reqBody.hidTimeZoneLabel;
    var slugName = first_name + "-" + last_name;
    checkUserSlug(req, res, slugName, function (req, res, availableSlug) {

        var salt = bcrypt.genSaltSync(10);
        var emailVerifyCode = "";
        var hash = bcrypt.hashSync(password, salt);
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 16; i++) {
            emailVerifyCode += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        var currentUTC = constants.CURR_UTC_DATETIME();
        var dobFormated = moment(dob, 'DD/MM/YYYY');
        var insertParams = {
            email: email,
            password: hash,
            dob: dobFormated.format(constants.JS_DB_DATE),
            email_verify_code: emailVerifyCode,
            user_type: 'user',
            registered_from: 'email',
            slug: availableSlug,
            created_date: currentUTC,
            updated_date: currentUTC
        };
        dbconnect.executeQuery(req, res, 'INSERT INTO users SET ?', insertParams, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var userId = results.insertId;
            miscFunction.getTimeZoneInfoFromZoneName(req, res, hidTimeZoneLabel, function (req, res, arrTimeZoneData) {

                var timeZoneToDb = 0;
                if (typeof arrTimeZoneData.timezoneId != 'undefined') {
                    timeZoneToDb = arrTimeZoneData.timezoneId;
                }

                var insertUserDetailParams = {
                    user_id: userId,
                    first_name: first_name,
                    last_name: last_name,
                    timezone_id: timeZoneToDb
                };
                dbconnect.executeQuery(req, res, 'INSERT INTO user_details SET ?', insertUserDetailParams, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    var full_name = first_name + " " + last_name;
                    var templateVariable = {
                        templateURL: "mailtemplate/registration",
                        userName: full_name,
                        verifyURL: constants.ACCESSURL + "verifyUserEmail/" + emailVerifyCode
                    };
                    var mailParamsObject = {
                        templateVariable: templateVariable,
                        to: email,
                        subject: 'SourceAdvisor User verification mail.'
                    };
                    var mailerOptions = {
                        to: mailParamsObject.to,
                        subject: mailParamsObject.subject
                    };
                    mailer.sendMail(req, res, mailParamsObject, function (err) {
                        if (err) {
                            req.flash('errors', 'Error Occured while sending email.');
                            res.redirect('/login');
                        } else {
                            req.flash('messages', 'User registered successfully. Please check your email for verification.');
                            res.redirect('/login');
                        }
                    });
                });
            });
        });
    });
};
exports.createNewUser = createNewUser;
var verifyUserEmail = function (req, res, verficationCode, callback) {
    var queryParam = [verficationCode];
    var strQuery = '	SELECT u.user_id userId, u.status status ' +
            '	FROM users u ' +
            ' 	WHERE u.email_verify_code = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        if (results.length == "0") {
            req.flash('errors', 'Invalid Verification link.');
            res.redirect('/login');
        } else {
            var userInfo = results[0];
            callback(req, res, userInfo['userId'], '1', userEmailVerify);
        }
    });
};
exports.verifyUserEmail = verifyUserEmail;
var userEmailVerify = function (req, res, userId) {
    req.flash('messages', 'User verified successfully. Please login to continue.');
    res.redirect('/login');
};
exports.userEmailVerify = userEmailVerify;
var updateUserStatus = function (req, res, userId, statusToUpdate, callback) {

    var queryParam = [statusToUpdate, userId];
    var strQuery = ' UPDATE users u ' +
            ' SET u.status = ? ' +
            ' WHERE u.user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        callback(req, res, userId);
    });
};
exports.updateUserStatus = updateUserStatus;
var checkUserLogin = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    var password = reqBody.password;
    req.checkBody('email', 'Enter valid email.').isEmail();
    req.checkBody('password', 'Password is empty.').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/login');
    } else {

        var queryParam = [email];
        var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, u.status status, ud.timezone_id timezoneId, z.zone_name zoneName, ud.first_name firstName, ud.last_name lastName, u.user_type userType, ud.profile_image AS profileImage ' +
                '	FROM users u, user_details ud LEFT JOIN zone z ON ud.timezone_id = z.zone_id ' +
                ' 	WHERE u.email = ? ' +
                '   AND ud.user_id = u.user_id ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == "0") {
                req.flash('errors', 'User does not exists.');
                res.redirect('/login');
            } else {
                var status = results[0]['status'];
                var userId = results[0]['userId'];
                var passwordFromDB = results[0]['password'];
                var flagPasswordCheck = bcrypt.compareSync(password, passwordFromDB);
                if (!flagPasswordCheck) {
                    req.flash('errors', 'Invalid password.');
                    res.redirect('/login');
                } else {
                    // email not verified
                    if (status == "0") {
                        req.flash('errors', 'Please verify your email.');
                        res.redirect('/login');
                    }
                    // account deleted
                    else if (status == "2") {
                        req.flash('errors', 'Account is currently Inactive. To Activate enter the e-mail associated with the Acct.');
                        res.redirect('/activateAccount');
                    }
                    // account suspended
                    else if (status == "3") {
                        req.flash('errors', 'Your account has been suspended please contact to admin.');
                        res.redirect('/login');
                    }
                    // status OK
                    else if (status == "1") {
                        setImagePathOfAllUsers(req, res, results, 0, [], callback);
                        //callback(req, res, results[0]);
                    }
                }
            }
        });
    }
};
exports.checkUserLogin = checkUserLogin;
var setUserSession = function (req, res, arrObjParams) {
    arrObjParams = arrObjParams[0];
    req.session.userId = arrObjParams.userId;
    req.session.timezone = arrObjParams.zoneName;
    req.session.firstName = arrObjParams.firstName;
    req.session.lastName = arrObjParams.lastName;
    req.session.userType = arrObjParams.userType;
    req.session.USERTZBROWSER = moment().format('Z');
    req.session.finalProfileImageUrl = arrObjParams.finalProfileImageUrl;
    req.session.save(function (err) {
        res.redirect('/');
    });
};
exports.setUserSession = setUserSession;
var checkUserLoginPopUp = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    var password = reqBody.password;
    req.checkBody('email', 'Enter valid email.').isEmail();
    req.checkBody('password', 'Password is empty.').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, errors, "1", "INVPARAM");
    } else {

        var queryParam = [email];
        var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, u.status status, ud.timezone_id timezoneId, z.zone_name zoneName, ud.first_name firstName, ud.last_name lastName, u.user_type userType, ud.profile_image AS profileImage ' +
                '	FROM users u, user_details ud LEFT JOIN zone z ON ud.timezone_id = z.zone_id ' +
                ' 	WHERE u.email = ? ' +
                '   AND ud.user_id = u.user_id ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == "0") {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", "USRNTEXS");
            } else {
                var status = results[0]['status'];
                var userId = results[0]['userId'];
                var passwordFromDB = results[0]['password'];
                var flagPasswordCheck = bcrypt.compareSync(password, passwordFromDB);
                if (!flagPasswordCheck) {
                    sendResponse.sendJsonResponse(req, res, 200, [], "1", "INVP");
                } else {
                    // email not verified
                    if (status == "0") {
                        sendResponse.sendJsonResponse(req, res, 200, [], "1", "PLVFYEMAIL");
                    }
                    // account deleted
                    else if (status == "2") {
                        sendResponse.sendJsonResponse(req, res, 200, [], "1", "ACCNF");
                    }
                    // status OK
                    else if (status == "1") {
                        setImagePathOfAllUsers(req, res, results, 0, [], callback);
                    }
                }
            }
        });
    }
}
exports.checkUserLoginPopUp = checkUserLoginPopUp;
var setUserSessionPopUp = function (req, res, arrObjParams) {
    arrObjParams = arrObjParams[0];
    req.session.userId = arrObjParams.userId;
    req.session.timezone = arrObjParams.zoneName;
    req.session.firstName = arrObjParams.firstName;
    req.session.lastName = arrObjParams.lastName;
    req.session.userType = arrObjParams.userType;
    req.session.USERTZBROWSER = moment().tz(arrObjParams.zoneName).format('Z');
    req.session.finalProfileImageUrl = arrObjParams.finalProfileImageUrl;
    req.session.save(function (err) {
        sendResponse.sendJsonResponse(req, res, 200, arrObjParams.userId, "0", "LOGGEDIN");
    });
};
exports.setUserSessionPopUp = setUserSessionPopUp;
var checkUserLoginChk = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    var password = reqBody.password;
    req.checkBody('email', 'Enter valid email.').isEmail();
    req.checkBody('password', 'Password is empty.').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/login');
    } else {
        var queryParam = [email];
        var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, u.status status, ud.timezone_id timezoneId, z.zone_name zoneName, ud.first_name firstName, ud.last_name lastName, u.user_type userType, ud.profile_image AS profileImage ' +
                '	FROM users u, user_details ud LEFT JOIN zone z ON ud.timezone_id = z.zone_id ' +
                ' 	WHERE u.email = ? ' +
                '   AND ud.user_id = u.user_id ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == "0") {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "User does not exists."}));
                res.end();
            } else {
                var status = results[0]['status'];
                var userId = results[0]['userId'];
                var passwordFromDB = results[0]['password'];
                var flagPasswordCheck = bcrypt.compareSync(password, passwordFromDB);
                if (!flagPasswordCheck) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "Invalid password"}));
                    res.end();
                } else {
                    // email not verified
                    if (status == "0") {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "Please verify your email."}));
                        res.end();
                    }
                    // account deleted
                    else if (status == "2") {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "Invalid User."}));
                        res.end();
                    }
                    // status OK
                    else if (status == "1") {
                        setImagePathOfAllUsers(req, res, results, 0, [], callback);
                        //callback(req, res, results[0]);
                    }
                }
            }
        });
    }
};
exports.checkUserLoginChk = checkUserLoginChk;
var setUserSessionChk = function (req, res, arrObjParams) {
    arrObjParams = arrObjParams[0];
    req.session.userId = arrObjParams.userId;
    req.session.timezone = arrObjParams.zoneName;
    req.session.firstName = arrObjParams.firstName;
    req.session.lastName = arrObjParams.lastName;
    req.session.userType = arrObjParams.userType;
    req.session.finalProfileImageUrl = arrObjParams.finalProfileImageUrl;
    req.session.save(function (err) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "success"}));
        res.end();
    });
    /*if(reqBody.chkRememberMe && reqBody.chkRememberMe == "remember-me") {
     
     }*/

    //req.flash('messages', 'user loggedin successfully..');
};
exports.setUserSessionChk = setUserSessionChk;
var setUserSessionAndSendResponseToRedirectDashboard = function (req, res, dataForSession) {
    req.session.userId = dataForSession.userId;
    req.session.timezone = dataForSession.timeZone;
    req.session.firstName = dataForSession.firstName;
    req.session.lastName = dataForSession.lastName;
    req.session.userType = dataForSession.userType;
    req.session.USERTZBROWSER = moment().tz(dataForSession.timeZone).format('Z');
    req.session.finalProfileImageUrl = dataForSession.finalProfileImageUrl;
    req.session.save(function (err) {
        sendResponse.sendJsonResponse(req, res, 200, {}, "0", "RDTDASH");
    });
    /*    res.writeHead(200, {'Content-Type': 'application/json'});
     res.write(JSON.stringify({
     "data": {},
     "error": "0",
     "flagMsg": "RDTDASH"
     }));
     res.end();*/
};
var checkUserEmailValidForAccActivation = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    req.checkBody('email', 'Enter valid email.').isEmail();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/forgotPassword');
    } else {
        var queryParam = [email];
        var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, u.status status ' +
                '	FROM users u ' +
                ' 	WHERE u.email = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == "0") {
                req.flash('errors', 'User not found. Please enter valid email id.');
                res.redirect('/forgotPassword');
            } else {
                var userId = results[0].userId;
                //updateUserStatus(req, res, userId, "1", checkForOldForgotPasswordGeneratedTokensAndExpireThem);
                sendMailAccountActivationLink(req, res, userId);
            }
        });
    }
};
exports.checkUserEmailValidForAccActivation = checkUserEmailValidForAccActivation;
var sendMailAccountActivationLink = function (req, res, userId) {
    var emailVerifyCode = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 16; i++) {
        emailVerifyCode += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    var currentUTC = constants.CURR_UTC_DATETIME();
    var params = [emailVerifyCode, userId];
    var queryString = "UPDATE users set email_verify_code = ? where user_id = ?";
    dbconnect.executeQuery(req, res, queryString, params, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var params = [userId];
        var queryString = "SELECT * from users u LEFT JOIN user_details ud ON u.user_id = ud.user_id where u.user_id = ?";
        dbconnect.executeQuery(req, res, queryString, params, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }



            var full_name = results[0].first_name + " " + results[0].last_name;
            var templateVariable = {
                templateURL: "mailtemplate/registration",
                userName: full_name,
                verifyURL: constants.ACCESSURL + "verifyUserEmail/" + emailVerifyCode
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: results[0].email,
                subject: 'SourceAdvisor User Activation verification mail.'
            };
            var mailerOptions = {
                to: mailParamsObject.to,
                subject: mailParamsObject.subject
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    req.flash('errors', 'Error Occured while sending email.');
                    res.redirect('/login');
                } else {
                    req.flash('messages', 'Please check your email for activation link.');
                    res.redirect('/login');
                }
            });
        });
    });
};
exports.sendMailAccountActivationLink = sendMailAccountActivationLink;
var checkUserEmailValid = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    req.checkBody('email', 'Enter valid email.').isEmail();
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/forgotPassword');
    } else {
        var queryParam = [email];
        var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, u.status status ' +
                '	FROM users u ' +
                ' 	WHERE u.email = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == "0") {
                req.flash('errors', 'User not found. Please enter valid email id.');
                res.redirect('/forgotPassword');
            } else {
                var userId = results[0].userId;
                //updateUserStatus(req, res, userId, "1", checkForOldForgotPasswordGeneratedTokensAndExpireThem);
                checkForOldForgotPasswordGeneratedTokensAndExpireThem(req, res, userId);
            }
        });
    }
};
exports.checkUserEmailValid = checkUserEmailValid;
var checkForOldForgotPasswordGeneratedTokensAndExpireThem = function (req, res, userId, callback) {
    var now = new Date();
    var formatedNow = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
    var queryParam = ["1", userId];
    var strQuery = ' UPDATE tokens u ' +
            ' SET u.is_used = ? ' +
            ' WHERE u.user_id = ? ' +
            ' AND u.is_used = 0';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        generateForgotPasswordToken(req, res, userId, sendForgotPasswordMail);
    });
};
exports.checkForOldForgotPasswordGeneratedTokensAndExpireThem = checkForOldForgotPasswordGeneratedTokensAndExpireThem;
var generateForgotPasswordToken = function (req, res, userId, callback) {
    // generating forgot password token
    var forgotPasswordToken = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 12; i++) {
        forgotPasswordToken += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    var currentUTC = constants.CURR_UTC_DATETIME();
    var insertParams = {
        user_id: userId,
        token: forgotPasswordToken,
        token_type: "forgot_password",
        created_date: currentUTC,
        is_used: '0'
    };
    dbconnect.executeQuery(req, res, 'INSERT INTO tokens SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var objParameterData = {
            forgotPasswordToken: forgotPasswordToken
        };
        getUserInformation(req, res, userId, objParameterData, sendForgotPasswordMail);
    });
};
exports.generateForgotPasswordToken = generateForgotPasswordToken;
var sendForgotPasswordMail = function (req, res, userId, arrUserData, objParams, callback) {
    // fetched user data.
    if (arrUserData.name !== 'undefined') {
        var email = arrUserData.email;
        var userName = arrUserData.name;
        var forgotasswordToken = encodeURIComponent(objParams.forgotPasswordToken);
        var templateVariable = {
            templateURL: "mailtemplate/forgotpassword",
            userName: userName,
            verifyURL: constants.ACCESSURL + "generateNewPassword/" + forgotasswordToken
        };
        var mailParamsObject = {
            templateVariable: templateVariable,
            to: email,
            subject: 'SourceAdvisor Forgot password.'
        };
        mailer.sendMail(req, res, mailParamsObject, function (err) {
            if (err) {
                req.flash('errors', 'Error Occured while sending email.');
                res.redirect('/forgotPassword');
            } else {
                req.flash('messages', 'Forgot password token generated successfully. Please check your mail for reset password url.');
                res.redirect('/forgotPassword');
            }
        });
    }
    // no user data found
    else {
        req.flash('errors', 'No user found.');
        res.redirect('/forgotPassword');
    }
};
exports.sendForgotPasswordMail = sendForgotPasswordMail;
var getUserInformation = function (req, res, userId, objParameterData, callback) {

    var timeZone = "";
    if (typeof objParameterData.timeZone != 'undefined') {
        timeZone = objParameterData.timeZone;
    } else {
        timeZone = req.session.USERTZBROWSER;
    }

    var queryParam = [userId];
    var strQuery = '	SELECT c.wc_mentor_percentage mentorPercentage, u.user_id,ud.first_name name,ud.last_name,u.slug,c.country_id,ct.city_id ,z.zone_name,u.email email, u.phone_country phoneCountry, u.phone_number phoneNumber, u.country_code countryCode, u.user_type userType, u.registered_from registeredFrom, u.slug slug, u.email_verify_code emailVerifyCode, u.status status, ud.profile_image profileImage, ud.first_name firstName, ud.last_name lastName,ud.linkedin_link linkedInLink, c.country_name countryName,ct.city_name, ud.user_brief userBreif,u.status, ' +
            '	DATE_FORMAT(u.dob, "' + constants.MYSQL_DB_DATE + '") DOB, ' +
            '   DATE_FORMAT(CONVERT_TZ(u.created_date, "' + constants.UTCTZ + '", "' + timeZone + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
            '   DATE_FORMAT(CONVERT_TZ(u.updated_date, "' + constants.UTCTZ + '", "' + timeZone + '"), "' + constants.MYSQL_DB_DATE + '") updatedDate ' +
            '	FROM users u, user_details ud ' +
            '   LEFT JOIN countries c ON ud.country_id = c.country_id ' +
            '   LEFT JOIN cities ct ON ud.city_id = ct.city_id' +
            '   LEFT JOIN zone z ON ud.timezone_id = z.zone_id' +
            ' 	WHERE u.user_id = ? AND ud.user_id = u.user_id';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            var image_path = constants.ACCESSURL + "images/noimg.png";
            if (typeof results[0]["profileImage"] != "undefined" && results[0]["profileImage"] != "") {
                fs.stat("./uploads/" + results[0]["profileImage"], function (err, stat) {
                    if (err == null) {
                        image_path = constants.ACCESSURL + "uploads/" + results[0]["profileImage"];
                    }

                    results[0]["fullProfileImageURL"] = image_path;
                    callback(req, res, userId, results[0], objParameterData);
                });
            } else {
                results[0]["fullProfileImageURL"] = image_path;
                callback(req, res, userId, results[0], objParameterData);
            }
        } else {
            callback(req, res, userId, {}, objParameterData);
        }
    });
};
exports.getUserInformation = getUserInformation;
var validateUserToken = function (req, res, forgotasswordToken, tokenType, callback) {
    var queryParam = [encodeURIComponent(forgotasswordToken), tokenType];
    var strQuery = '	SELECT t.user_id userId ' +
            '	FROM tokens t' +
            ' 	WHERE t.token = ?' +
            '	AND t.token_type = ?' +
            '	AND t.is_used = 0';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var flagValidToken = false;
        if (results.length > 0) {
            flagValidToken = true;
        }

        callback(req, res, flagValidToken, forgotasswordToken, tokenType);
    });
};
exports.validateUserToken = validateUserToken;
var resetNewPasswordForUser = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var password = reqBody.password;
    var confPassword = reqBody.confPassword;
    var forgotPasswordToken = reqBody.forgotPasswordToken;
    req.checkBody('password', 'Please enter password.').notEmpty();
    req.checkBody('confPassword', 'Enter valid email.').equals(reqBody.password);
    var errors = req.validationErrors();
    if (errors) {
        req.flash('errors', errors);
        res.redirect('/setNewPassword' + forgotPasswordToken);
    } else {

        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(password, salt);
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [hash, currentUTC, forgotPasswordToken];
        var strQuery = ' UPDATE users u, tokens t ' +
                ' SET u.password = ?, ' +
                ' u.updated_date = ?, ' +
                ' u.status = 1, ' +
                ' t.is_used = 1 ' +
                ' WHERE u.user_id = t.user_id ' +
                ' AND  t.token = ? ' +
                ' AND  t.token_type = "forgot_password" ' +
                ' AND  t.is_used = "0" ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }
            callback(req, res, false);
        });
    }
};
exports.resetNewPasswordForUser = resetNewPasswordForUser;
var checkUserIsDeleted = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var email = reqBody.email;
    var queryParam = [email];
    var strQuery = '    SELECT COUNT(user_id) totalCount ' +
            '   FROM users u ' +
            '   WHERE u.status = 2 ' +
            '   AND u.email = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            var dataToSend = {
                redirectLink: constants.ACCESSURL + 'redirectToEmailActivePage'
            };
            sendResponse.sendJsonResponse(req, res, 200, dataToSend, "0", "RDTEMAILACT");
        } else {
            // user deleted
            if (results[0].totalCount > 0) {
                var dataToSend = {
                    redirectLink: constants.ACCESSURL + 'redirectToEmailActivePage'
                };
                sendResponse.sendJsonResponse(req, res, 200, dataToSend, "0", "RDTEMAILACT");
            } else {
                callback(req, res);
            }
        }
    });
}
exports.checkUserIsDeleted = checkUserIsDeleted;
var checkUserSocialLogin = function (req, res, callback) {

    checkSocialLoginUserStatus(req, res, function (req, res, arrUserSocialData) {

        var reqBody = req.body;
        //JSON.stringify(reqBody, null, 4);

        var uniqueId = reqBody.uniqueId;
        var loginType = reqBody.loginType;
        if (loginType == "gp") {
            // first time user is registering in our system. create social user
            // we will get empty
            if (Object.keys(arrUserSocialData).length == "0") {
                createSocialUserGoogle(req, res, setUserSessionAndSendResponseToRedirectDashboard);
            }
            // user has already registered in our system, either by email or google.
            else {
                var userId = arrUserSocialData.userId;
                var firstName = arrUserSocialData.firstName;
                var lastName = arrUserSocialData.lastName;
                var email = arrUserSocialData.email;
                var status = arrUserSocialData.status;
                var userType = arrUserSocialData.userType;
                var timeZone = arrUserSocialData.zoneName;
                var userProfileImage = arrUserSocialData.profileImage;
                var socialLoginId = arrUserSocialData.socialLoginId;
                var googleId = arrUserSocialData.googleId;
                var googleName = arrUserSocialData.googleName;
                var googleImage = arrUserSocialData.googleImage;
                var googleEmail = arrUserSocialData.googleEmail;
                var linkedInId = arrUserSocialData.linkedInId;
                var linkedInFirstName = arrUserSocialData.linkedInFirstName;
                var linkedInLastName = arrUserSocialData.linkedInLastName;
                var linkedInHeadline = arrUserSocialData.linkedInHeadline;
                var linkedInProfileURL = arrUserSocialData.linkedInProfileURL;
                var linkedINEmail = arrUserSocialData.linkedINEmail;
                var arrNameComponent = reqBody.name.split(" ");
                var arrNameLength = arrNameComponent.length;
                var googleLastName = "";
                var googleFirstName = "";
                if (arrNameLength > 1) {
                    googleLastName = arrNameComponent[arrNameLength - 1];
                    for (var intI = 0; intI < (arrNameLength - 1); intI++) {
                        googleFirstName = arrNameComponent[intI];
                    }
                } else if (arrNameLength == 1) {
                    googleFirstName = arrNameComponent[0];
                }

                // if social login id is null, that means user havent registered to loggedin into the system socially.
                // need to create first entry
                if (socialLoginId == null) {
                    createSocialLoginEntryGoogle(req, res, arrUserSocialData, setUserSessionAndSendResponseToRedirectDashboard);
                } else {

                    // if user had registered with email intitially, then it's google_id will be blank
                    // need to update user social login table only
                    if (!isNaN(socialLoginId) && socialLoginId > 0 && googleId == null) {

                        // data from request
                        var uniqueId = reqBody.uniqueId;
                        var loginType = reqBody.loginType;
                        var name = reqBody.name;
                        var imageUrl = reqBody.imageUrl;
                        var email = reqBody.email;
                        var queryParam = [uniqueId, name, imageUrl, email, socialLoginId];
                        var strQuery = '	UPDATE social_login sl ' +
                                '	SET sl.google_id = ?, ' +
                                '		sl.google_name = ?, ' +
                                '		sl.google_img = ?, ' +
                                '		sl.google_email = ? ' +
                                ' WHERE sl.social_login_id= ? ';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            }

                            if (userProfileImage == "") {
                                request.get(imageUrl, function (error, response, body) {
                                    if (!error && response.statusCode == 200) {
                                        data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
                                        var raw_image_data = data.replace(/^data\:image\/\w+\;base64\,/, '');
                                        var destination = "./uploads/";
                                        var fileNameToUpload = "userProfilePic" + '-' + userId + '-' + Date.now() + ".jpg";
                                        var filePathToUpload = destination + fileNameToUpload;
                                        fs.writeFile(filePathToUpload, raw_image_data, 'base64', function (err) {
                                            if (err) {

                                            } else {
                                                var queryParam = [googleFirstName, googleLastName, fileNameToUpload, userId];
                                                var strQuery = '    UPDATE user_details ud ' +
                                                        '   SET ud.first_name = ?, ' +
                                                        '       ud.last_name = ?, ' +
                                                        '       ud.profile_image = ? ' +
                                                        ' WHERE ud.user_id = ? ';
                                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                    if (error) {
                                                        throw error;
                                                    }
                                                    var dataForSession = {
                                                        userId: userId,
                                                        firstName: googleFirstName,
                                                        lastName: googleLastName,
                                                        userType: userType,
                                                        timeZone: timeZone,
                                                        finalProfileImageUrl: constants.ACCESSURL + 'uploads/' + fileNameToUpload
                                                    };
                                                    //callback(req, res, dataForSession);
                                                    setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession);
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                var queryParam = [googleFirstName, googleLastName, userId];
                                var strQuery = '    UPDATE user_details ud ' +
                                        '   SET ud.first_name = ?, ' +
                                        '       ud.last_name = ?, ' +
                                        //'       ud.profile_image = ? ' +
                                        ' WHERE ud.user_id = ? ';
                                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                    if (error) {
                                        throw error;
                                    }
                                    var dataForSession = {
                                        userId: userId,
                                        firstName: googleFirstName,
                                        lastName: googleLastName,
                                        userType: userType,
                                        timeZone: timeZone,
                                        // finalProfileImageUrl: constants.ACCESSURL + 'uploads/' + fileNameToUpload
                                    };
                                    //callback(req, res, dataForSession);
                                    setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession);
                                });
                            }


                            // var dataForSession = {
                            //     userId: userId,
                            //     firstName: firstName,
                            //     lastName: lastName,
                            //     userType: userType,
                            //     timeZone: timeZone
                            // };

                            // setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession);
                        });
                    } else {
                        var dataForSession = {
                            userId: userId,
                            firstName: firstName,
                            lastName: lastName,
                            userType: userType,
                            timeZone: timeZone,
                            profileImage: userProfileImage
                        };
                        setImagePathOfAllUsers(req, res, [dataForSession], 0, [], function (req, res, dataForSession) {
                            setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession[0]);
                        });
                    }
                }
            }
        } else if (loginType == "ln") {

            // first time user is registering in our system. create social user
            // we will get empty arrUserSocialData
            if (Object.keys(arrUserSocialData).length == "0") {
                createSocialUserLinkedIn(req, res, setUserSessionAndSendResponseToRedirectDashboard);
            } else {

                var userId = arrUserSocialData.userId;
                var firstNameUserData = arrUserSocialData.firstName;
                var lastNameUserData = arrUserSocialData.lastName;
                var email = arrUserSocialData.email;
                var status = arrUserSocialData.status;
                var userType = arrUserSocialData.userType;
                var timeZone = arrUserSocialData.zoneName;
                var userProfileImage = arrUserSocialData.profileImage;
                var socialLoginId = arrUserSocialData.socialLoginId;
                var googleId = arrUserSocialData.googleId;
                var googleName = arrUserSocialData.googleName;
                var googleImage = arrUserSocialData.googleImage;
                var googleEmail = arrUserSocialData.googleEmail;
                var linkedInId = arrUserSocialData.linkedInId;
                var linkedInFirstName = arrUserSocialData.linkedInFirstName;
                var linkedInLastName = arrUserSocialData.linkedInLastName;
                var linkedInHeadline = arrUserSocialData.linkedInHeadline;
                var linkedInProfileURL = arrUserSocialData.linkedInProfileURL;
                var linkedINEmail = arrUserSocialData.linkedINEmail;
                // if social login id is null, that means user havent registered to loggedin into the system socially.
                // need to create first entry
                if (socialLoginId == null) {
                    createSocialLoginEntrylinkedIn(req, res, arrUserSocialData, setUserSessionAndSendResponseToRedirectDashboard);
                } else {

                    // if user had registered with email intitially, then it's google_id will be blank
                    // need to update user social login table only
                    if (!isNaN(socialLoginId) && socialLoginId > 0 && linkedInId == null) {

                        // data from request
                        var uniqueId = reqBody.uniqueId;
                        var firstName = reqBody.firstName;
                        var lastName = reqBody.lastName;
                        var headline = reqBody.headline;
                        var profileUrl = "";
                        if (userProfileImage == "") {
                            profileUrl = reqBody.profileUrl;
                        } else {
                            profileUrl = userProfileImage;
                        }

                        //var profileUrl = reqBody.profileUrl;
                        var loginType = reqBody.loginType;
                        var email = reqBody.email;
                        var queryParam = [uniqueId, firstName, lastName, headline, profileUrl, email, socialLoginId];
                        var strQuery = '	UPDATE social_login sl ' +
                                '	SET sl.linkedin_id = ?, ' +
                                '		sl.linkedin_first_name = ?, ' +
                                '		sl.linkedin_last_name = ?, ' +
                                '		sl.linkedin_headline = ?, ' +
                                '		sl.linkedin_profile_url = ?, ' +
                                '		sl.linkedin_email = ? ' +
                                ' WHERE sl.social_login_id= ? ';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            }



                            var queryParam = [firstName, lastName, userId];
                            var strQuery = '    UPDATE user_details ud ' +
                                    '   SET ud.first_name = ?, ' +
                                    '       ud.last_name = ? ' +
                                    ' WHERE ud.user_id = ? ';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    throw error;
                                }

                                var dataForSession = {
                                    userId: userId,
                                    firstName: firstName,
                                    lastName: lastName,
                                    userType: userType,
                                    timeZone: timeZone,
                                    profileImage: userProfileImage
                                };
                                setImagePathOfAllUsers(req, res, [dataForSession], 0, [], function (req, res, dataForSession) {
                                    setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession[0]);
                                });
                                //setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession);
                            });
                        });
                    } else {
                        var dataForSession = {
                            userId: userId,
                            firstName: linkedInFirstName,
                            lastName: linkedInLastName,
                            userType: userType,
                            timeZone: timeZone,
                            profileImage: userProfileImage
                        };
                        setImagePathOfAllUsers(req, res, [dataForSession], 0, [], function (req, res, dataForSession) {
                            setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession[0]);
                        });
                        //setUserSessionAndSendResponseToRedirectDashboard(req, res, dataForSession);
                    }
                }
            }
        }
    });
};
exports.checkUserSocialLogin = checkUserSocialLogin;
var checkSocialLoginUserStatus = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var uniqueId = reqBody.uniqueId;
    var loginType = reqBody.loginType;
    var email = reqBody.email;
    var queryParam = [email, uniqueId, email, uniqueId, email];
    var strQuery = '	SELECT u.user_id userId, u.email email, ud.profile_image AS profileImage, ud.first_name firstName, ud.last_name lastName, u.status status, sl.social_login_id socialLoginId, sl.google_id googleId, sl.google_name googleName, sl.google_img googleImage, sl.google_email googleEmail, sl.linkedin_id linkedInId, sl.linkedin_first_name linkedInFirstName, sl.linkedin_last_name linkedInLastName, sl.linkedin_headline linkedInHeadline, sl.linkedin_profile_url linkedInProfileURL, sl.linkedin_email linkedINEmail, u.user_type userType, z.zone_name zoneName ' +
            '	FROM users u ' +
            ' 		LEFT JOIN social_login sl ' +
            '		ON sl.user_id = u.user_id, ' +
            '	user_details ud ' +
            '       LEFT JOIN zone z ' +
            '       ON ud.timezone_id = z.zone_id ' +
            '	WHERE ud.user_id = u.user_id' +
            '	AND (u.email = ? OR (sl.google_id = ? AND sl.google_email = ?) OR (sl.linkedin_id = ? AND sl.linkedin_email = ?))';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, {});
        } else {
            callback(req, res, results[0]);
        }
    });
};
var processSocialLogin = function (req, res) {
    res.send(200);
};
exports.processSocialLogin = processSocialLogin;
var createSocialLoginEntryGoogle = function (req, res, arrUserSocialData, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    // data from request
    var uniqueId = reqBody.uniqueId;
    var loginType = reqBody.loginType;
    var name = reqBody.name;
    var imageUrl = reqBody.imageUrl;
    var email = reqBody.email;
    // matching data from DB
    var userId = arrUserSocialData.userId;
    var firstName = arrUserSocialData.firstName;
    var email = arrUserSocialData.email;
    var status = arrUserSocialData.status;
    var userType = arrUserSocialData.userType;
    var timeZone = arrUserSocialData.timeZone;
    var socialLoginId = arrUserSocialData.socialLoginId;
    var googleId = arrUserSocialData.googleId;
    var googleName = arrUserSocialData.googleName;
    var googleImage = arrUserSocialData.googleImage;
    var googleEmail = arrUserSocialData.googleEmail;
    var linkedInId = arrUserSocialData.linkedInId;
    var linkedInFirstName = arrUserSocialData.linkedInFirstName;
    var linkedInLastName = arrUserSocialData.linkedInLastName;
    var linkedInHeadline = arrUserSocialData.linkedInHeadline;
    var linkedInProfileURL = arrUserSocialData.linkedInProfileURL;
    var linkedINEmail = arrUserSocialData.linkedINEmail;
    var arrNameComponent = name.split(" ");
    var arrNameLength = arrNameComponent.length;
    var googleLastName = "";
    var googleFirstName = "";
    if (arrNameLength > 1) {
        googleLastName = arrNameComponent[arrNameLength - 1];
        for (var intI = 0; intI < (arrNameLength - 1); intI++) {
            googleFirstName = arrNameComponent[intI];
        }
    } else if (arrNameLength == 1) {
        googleFirstName = arrNameComponent[0];
    }


    var insertParams = {
        user_id: userId,
        google_id: uniqueId,
        google_name: name,
        google_img: imageUrl,
        google_email: email
    };
    dbconnect.executeQuery(req, res, 'INSERT INTO social_login SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }


        request.get(imageUrl, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
                var raw_image_data = data.replace(/^data\:image\/\w+\;base64\,/, '');
                var destination = "./uploads/";
                var fileNameToUpload = "userProfilePic" + '-' + userId + '-' + Date.now() + ".jpg";
                var filePathToUpload = destination + fileNameToUpload;
                fs.writeFile(filePathToUpload, raw_image_data, 'base64', function (err) {
                    if (err) {

                    } else {
                        var queryParam = [googleFirstName, googleLastName, fileNameToUpload, userId];
                        var strQuery = '    UPDATE user_details ud ' +
                                '   SET ud.first_name = ?, ' +
                                '       ud.last_name = ?, ' +
                                '       ud.profile_image = ? ' +
                                ' WHERE ud.user_id = ? ';
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            }
                            var dataForSession = {
                                userId: userId,
                                firstName: googleFirstName,
                                lastName: googleLastName,
                                userType: userType,
                                timeZone: timeZone,
                                finalProfileImageUrl: constants.ACCESSURL + 'uploads/' + fileNameToUpload
                            };
                            callback(req, res, dataForSession);
                        });
                    }
                });
            }
        });
    });
};
var createSocialLoginEntrylinkedIn = function (req, res, arrUserSocialData, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    // data from request
    var uniqueId = reqBody.uniqueId;
    var firstName = reqBody.firstName;
    var lastName = reqBody.lastName;
    var headline = reqBody.headline;
    var profileUrl = reqBody.profileUrl;
    var loginType = reqBody.loginType;
    var email = reqBody.email;
    // matching data from DB
    var userId = arrUserSocialData.userId;
    //var firstName 		= arrUserSocialData.firstName;
    var email = arrUserSocialData.email;
    var status = arrUserSocialData.status;
    var userType = arrUserSocialData.userType;
    var timeZone = arrUserSocialData.timeZone;
    var userProfileImage = arrUserSocialData.profileImage;
    var socialLoginId = arrUserSocialData.socialLoginId;
    var googleId = arrUserSocialData.googleId;
    var googleName = arrUserSocialData.googleName;
    var googleImage = arrUserSocialData.googleImage;
    var googleEmail = arrUserSocialData.googleEmail;
    var linkedInId = arrUserSocialData.linkedInId;
    var linkedInFirstName = arrUserSocialData.linkedInFirstName;
    var linkedInLastName = arrUserSocialData.linkedInLastName;
    var linkedInHeadline = arrUserSocialData.linkedInHeadline;
    var linkedInProfileURL = arrUserSocialData.linkedInProfileURL;
    var linkedINEmail = arrUserSocialData.linkedINEmail;
    var insertParams = {
        user_id: userId,
        linkedin_id: uniqueId,
        linkedin_first_name: firstName,
        linkedin_last_name: lastName,
        linkedin_headline: headline,
        linkedin_profile_url: profileUrl,
        linkedin_email: email
    };
    dbconnect.executeQuery(req, res, 'INSERT INTO social_login SET ?', insertParams, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var queryParam = [firstName, lastName, userId];
        var strQuery = '    UPDATE user_details ud ' +
                '   SET ud.first_name = ?, ' +
                '       ud.last_name = ? ' +
                ' WHERE ud.user_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var dataForSession = {
                userId: userId,
                firstName: firstName,
                lastName: lastName,
                userType: userType,
                timeZone: timeZone,
                profileImage: userProfileImage
            };
            setImagePathOfAllUsers(req, res, [dataForSession], 0, [], function (req, res, dataForSession) {
                callback(req, res, dataForSession[0]);
            });
            //callback(req, res, dataForSession);
        });
    });
};
var createSocialUserGoogle = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    // data from request
    var uniqueId = reqBody.uniqueId;
    var loginType = reqBody.loginType;
    var name = reqBody.name;
    var imageUrl = reqBody.imageUrl;
    var email = reqBody.email;
    var hidTimeZoneLabel = reqBody.timeZone;
    var arrNameComponent = name.split(" ");
    var arrNameLength = arrNameComponent.length;
    var lastName = "";
    var firstName = "";
    if (arrNameLength > 1) {
        lastName = arrNameComponent[arrNameLength - 1];
        for (var intI = 0; intI < (arrNameLength - 1); intI++) {
            firstName = arrNameComponent[intI];
        }
    } else if (arrNameLength == 1) {
        firstName = arrNameComponent[0];
    }
    var slugName = firstName + "-" + lastName;
    checkUserSlug(req, res, slugName, function (req, res, availableSlug) {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var salt = bcrypt.genSaltSync(10);
        var password = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 16; i++) {
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var hash = bcrypt.hashSync(password, salt);
        var insertParamsUsers = {
            email: email,
            password: hash,
            user_type: "user",
            registered_from: "google",
            slug: availableSlug,
            status: "1",
            created_date: currentUTC,
            updated_date: currentUTC
        };
        dbconnect.executeQuery(req, res, 'INSERT INTO users SET ?', insertParamsUsers, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var insertedUserId = results.insertId;
            miscFunction.getTimeZoneInfoFromZoneName(req, res, hidTimeZoneLabel, function (req, res, arrTimeZoneData) {
                var timeZoneToDb = "0";
                if (typeof arrTimeZoneData.timezoneId != 'undefined') {
                    timeZoneToDb = arrTimeZoneData.timezoneId;
                }

                // inserting data into user_details
                var insertParamsUserDetails = {
                    user_id: insertedUserId,
                    first_name: firstName,
                    last_name: lastName,
                    timezone_id: timeZoneToDb
                };
                dbconnect.executeQuery(req, res, 'INSERT INTO user_details SET ?', insertParamsUserDetails, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    request.get(imageUrl, function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
                            var raw_image_data = data.replace(/^data\:image\/\w+\;base64\,/, '');
                            var destination = "./uploads/";
                            var fileNameToUpload = "userProfilePic" + '-' + insertedUserId + '-' + Date.now() + ".jpg";
                            var filePathToUpload = destination + fileNameToUpload;
                            fs.writeFile(filePathToUpload, raw_image_data, 'base64', function (err) {
                                if (err) {

                                } else {

                                    var queryParamUserDetail = [fileNameToUpload, insertedUserId];
                                    var strQueryUserDetail = '  UPDATE user_details u ' +
                                            '   SET u.profile_image = ? ' +
                                            '   WHERE u.user_id = ?';
                                    dbconnect.executeQuery(req, res, strQueryUserDetail, queryParamUserDetail, function (req, res, error, results, fields) {
                                        if (error) {
                                            throw error;
                                        }

                                        // inserting data into social login table.
                                        var insertParamsSocialLogin = {
                                            user_id: insertedUserId,
                                            google_id: uniqueId,
                                            google_name: name,
                                            google_img: imageUrl,
                                            google_email: email
                                        };
                                        dbconnect.executeQuery(req, res, 'INSERT INTO social_login SET ?', insertParamsSocialLogin, function (req, res, error, results, fields) {
                                            if (error) {
                                                throw error;
                                            }

                                            var templateVariable = {
                                                templateURL: "mailtemplate/socialregister",
                                                userName: name,
                                                registeredEmail: email,
                                                systemGeneratedPassword: password
                                            };
                                            var mailParamsObject = {
                                                templateVariable: templateVariable,
                                                to: email,
                                                subject: 'SourceAdvisor Welcome'
                                            };
                                            var mailerOptions = {
                                                to: mailParamsObject.to,
                                                subject: mailParamsObject.subject
                                            };
                                            mailer.sendMail(req, res, mailParamsObject, function (err) {

                                                if (err) {
                                                    throw err;
                                                } else {
                                                    var dataForSession = {
                                                        userId: insertedUserId,
                                                        firstName: firstName,
                                                        lastName: lastName,
                                                        userType: "user",
                                                        timeZone: hidTimeZoneLabel,
                                                        finalProfileImageUrl: constants.ACCESSURL + 'uploads/' + fileNameToUpload
                                                    };
                                                    callback(req, res, dataForSession);
                                                }
                                            });
                                        });
                                    });
                                }
                            });
                        }
                    });
                });
            });
        });
    });
};
var createSocialUserLinkedIn = function (req, res, callback) {

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    // data from request
    var uniqueId = reqBody.uniqueId;
    var firstName = reqBody.firstName;
    var lastName = reqBody.lastName;
    var headline = reqBody.headline;
    var profileUrl = reqBody.profileUrl;
    var loginType = reqBody.loginType;
    var email = reqBody.email;
    var hidTimeZoneLabel = reqBody.timeZone;
    var slugName = firstName + "-" + lastName;
    checkUserSlug(req, res, slugName, function (req, res, availableSlug) {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var salt = bcrypt.genSaltSync(10);
        var password = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 16; i++) {
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        var hash = bcrypt.hashSync(password, salt);
        var insertParamsUsers = {
            email: email,
            password: hash,
            user_type: "user",
            registered_from: "linkedin",
            slug: availableSlug,
            status: "1",
            created_date: currentUTC,
            updated_date: currentUTC,
        };
        dbconnect.executeQuery(req, res, 'INSERT INTO users SET ?', insertParamsUsers, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var insertedUserId = results.insertId;
            miscFunction.getTimeZoneInfoFromZoneName(req, res, hidTimeZoneLabel, function (req, res, arrTimeZoneData) {
                var timeZoneToDb = "0";
                if (typeof arrTimeZoneData.timezoneId != 'undefined') {
                    timeZoneToDb = arrTimeZoneData.timezoneId;
                }

                //inserting data into user_details
                var usersName = firstName + "  " + lastName;
                var insertParamsUserDetails = {
                    user_id: insertedUserId,
                    first_name: firstName,
                    last_name: lastName,
                    timezone_id: timeZoneToDb
                };
                dbconnect.executeQuery(req, res, 'INSERT INTO user_details SET ?', insertParamsUserDetails, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    }

                    // inserting data into social login table.
                    var insertParamsSocialLogin = {
                        user_id: insertedUserId,
                        linkedin_id: uniqueId,
                        linkedin_first_name: firstName,
                        linkedin_last_name: lastName,
                        linkedin_headline: headline,
                        linkedin_profile_url: profileUrl,
                        linkedin_email: email
                    };
                    dbconnect.executeQuery(req, res, 'INSERT INTO social_login SET ?', insertParamsSocialLogin, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        }

                        var templateVariable = {
                            templateURL: "mailtemplate/socialregister",
                            userName: usersName,
                            registeredEmail: email,
                            systemGeneratedPassword: password
                        };
                        var mailParamsObject = {
                            templateVariable: templateVariable,
                            to: email,
                            subject: 'SourceAdvisor Welcome'
                        };
                        var mailerOptions = {
                            to: mailParamsObject.to,
                            subject: mailParamsObject.subject
                        };
                        mailer.sendMail(req, res, mailParamsObject, function (err) {
                            if (err) {
                                throw err;
                            } else {
                                var dataForSession = {
                                    userId: insertedUserId,
                                    firstName: firstName,
                                    lastName: lastName,
                                    userType: "user",
                                    timeZone: hidTimeZoneLabel,
                                    finalProfileImageUrl: constants.ACCESSURL + 'images/noimg.png'
                                };
                                callback(req, res, dataForSession);
                            }
                        });
                    });
                });
            });
        });
    });
};
var send_message_to_mentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentormsg = reqBody.mentormsg;
    var mentor_id = reqBody.mentor_id;
    var user_id = req.session.userId;
    req.checkBody('mentormsg', 'Please enter Message').notEmpty();
    //req.checkBody('user_id', 'Please enter user id').notEmpty();
    //req.checkBody('mentor_id', 'Please enter mentorid').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        //sessoin mentor id req.session.userId;

        mentormsg = mentormsg.replace(/\n/g, '<br />');
        var strQuery = 'SELECT * FROM `msg_conversation` WHERE message_sender_id =' + mentor_id + ' AND message_receiver_id = ' + user_id + ' OR message_sender_id = ' + user_id + ' AND message_receiver_id =' + mentor_id;
        dbconnect.executeQuery(req, res, strQuery, '', function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"}));
                res.end();
            } else {

                if (results.length > 0) {
                    var conversation_id = results[0].conversation_id;
                    var queryParam = {
                        conversation_id: conversation_id,
                        reply_receiver_id: mentor_id,
                        reply_sender_id: user_id,
                        message_body: mentormsg,
                        status: "unread",
                        created_date: moment.utc().format()
                    };
                    var strQuery = 'insert into msg_replies set ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {

                            var queryParams = [user_id];
                            var sqlQuery = 'SELECT * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';
                            dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                                if (error) {
                                    if (reqBody.sender_id == null)
                                    {
                                        throw error;
                                    } else
                                    {
                                        console.log(error);
                                    }
                                } else {

                                    var sendername = "";
                                    if (results.length > 0) {

                                        sendername = results[0].first_name + " " + results[0].last_name;
                                        var emailData = {
                                            receiver_id: mentor_id,
                                            message: mentormsg,
                                            sendername: sendername,
                                            time: constants.CURR_UTC_DATETIME(),
                                            user_brief: results[0].user_brief,
                                            userSlug: results[0].slug
                                        };
                                        messagesModel.sendUserMessagesEmail(req, res, emailData);
                                    }

                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "success"}));
                                    res.end();
                                }
                            });
                        }
                    });
                } else {

                    var queryParam = {
                        message_receiver_id: mentor_id,
                        message_sender_id: user_id,
                        created_date: moment.utc().format()
                    };
                    var strQuery = 'insert into msg_conversation set ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {
                            var conversation_id = results.insertId;
                            var queryParam = {
                                conversation_id: conversation_id,
                                reply_receiver_id: mentor_id,
                                reply_sender_id: user_id,
                                message_body: mentormsg,
                                status: "unread",
                                created_date: moment.utc().format()
                            };
                            var strQuery = 'insert into msg_replies set ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"}));
                                    res.end();
                                } else {

                                    var queryParam = {
                                        conversation_id: conversation_id,
                                        user_id: mentor_id,
                                        status: "inbox",
                                        created_date: moment.utc().format()
                                    };
                                    var strQuery = 'insert into msg_conv_user_status set ?';
                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                        if (error) {
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "error"}));
                                            res.end();
                                        } else {

                                            var queryParam = {
                                                conversation_id: conversation_id,
                                                user_id: user_id,
                                                status: "inbox",
                                                created_date: moment.utc().format()
                                            };
                                            var strQuery = 'insert into msg_conv_user_status set ?';
                                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                                if (error) {
                                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                                    res.write(JSON.stringify({
                                                        "message": "error"}));
                                                    res.end();
                                                } else {


                                                    var queryParams = [user_id];
                                                    var sqlQuery = 'SELECT * from users u , user_details ud WHERE u.user_id = ud.user_id AND u.user_id = ?';
                                                    dbconnect.executeQuery(req, res, sqlQuery, queryParams, function (req, res, error, results, fields) {
                                                        if (error) {
                                                            if (reqBody.sender_id == null)
                                                            {
                                                                throw error;
                                                            } else
                                                            {
                                                                console.log(error);
                                                            }
                                                        } else {

                                                            var sendername = "";
                                                            if (results.length > 0) {

                                                                sendername = results[0].first_name + " " + results[0].last_name;
                                                                var emailData = {
                                                                    receiver_id: mentor_id,
                                                                    message: mentormsg,
                                                                    sendername: sendername,
                                                                    time: constants.CURR_UTC_DATETIME(),
                                                                    user_brief: results[0].user_brief,
                                                                    userSlug: results[0].slug
                                                                };
                                                                messagesModel.sendUserMessagesEmail(req, res, emailData);
                                                            }

                                                            res.writeHead(200, {'Content-Type': 'application/json'});
                                                            res.write(JSON.stringify({
                                                                "message": "success"}));
                                                            res.end();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};
exports.send_message_to_mentor = send_message_to_mentor;

var checkSlotForPricateInvite = function (req, res, objParams, callback) {
    var slot = objParams.slot;
    var startTime = objParams.startTime;
    var endTime = objParams.endTime;
    var mentorTimezone = objParams.mentorTimezone;
    var slotcount = objParams.slotcount;
    var mentorId = objParams.mentorId;

    if (typeof slot != 'undefined' && slot != "") {
        meetingstarttimeUTC = moment.tz(slot + " " + startTime, mentorTimezone).tz('UTC').format('HH:mm:00');
        meetingendtimeUTC = moment.tz(slot + " " + endTime, mentorTimezone).tz('UTC').format('HH:mm:00');
        callDate11 = moment.tz(slot + " " + startTime, mentorTimezone).tz('UTC').format('YYYY-MM-DD');

        var queryParam = [mentorId, callDate11, meetingstarttimeUTC, meetingendtimeUTC, meetingstarttimeUTC];

        var strQuery = "SELECT * FROM meeting_invitation_slot_mapping psm, meeting_invitations mip " +
                "WHERE psm.mentor_id = ? AND psm.status = 'accepted' AND mip.status='accepted'  " +
                "AND psm.date = ? " +
                "AND psm.invitation_id = mip.invitation_id " +
                "AND psm.id NOT IN (  " +
                "SELECT id  " +
                "FROM meeting_invitation_slot_mapping subpsm  " +
                "WHERE subpsm.mentor_id = psm.mentor_id " +
                "AND subpsm.date = psm.date " +
                "AND (((? <= CONCAT(subpsm.time, ':00')) AND (? < CONCAT(subpsm.time, ':00')))  " +
                "OR (? >= (SELECT DATE_FORMAT(DATE_ADD(CONCAT('2000-01-01 ', subpsm.time), INTERVAL mi.duration MINUTE), '%H:%i:00') " +
                "FROM meeting_invitations mi  " +
                "WHERE subpsm.invitation_id = mi.invitation_id) " +
                "))) ";

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                callback(req, res, {msg: "error"});
            } else {
                if (results.length > 0) {
                    callback(req, res, {msg: "already", slotcount: slotcount});
                } else {
                    callback(req, res, {msg: "continue"});
                }
            }
        });
    } else {
        callback(req, res, {msg: "continue"});
    }
};
exports.checkSlotForPricateInvite = checkSlotForPricateInvite;

var slotBooking = function (req, res, callback) {
    var reqBody = req.body;
    // data from request

    var type = "virtual";
    var call_duration = reqBody.call_duration;
    var requestorId = reqBody.sesuserId;
    var mentorId = reqBody.mentorId;
    var slot1_date = reqBody.slot1_date;
    var slot2_date = reqBody.slot2_date;
    var slot3_date = reqBody.slot3_date;
    var slot1_time = reqBody.slot1_time;
    var slot2_time = reqBody.slot2_time;
    var slot3_time = reqBody.slot3_time;
    var payableAmount = 0;
    var tz = reqBody.timezone;
    var meet_purpose = reqBody.meet_purpose;
    var more_details = reqBody.more_details;
    var documents = reqBody.fileids;

    var slot_date, slot_time;
    slot1_dateI = moment.tz(slot1_date + " " + slot1_time, tz).utcOffset('+0000').format('YYYY-MM-DD');
    slot1_timeI = moment.tz(slot1_date + " " + slot1_time, tz).utcOffset('+0000').format('HH:mm');
    slot2_dateI = moment.tz(slot2_date + " " + slot2_time, tz).utcOffset('+0000').format('YYYY-MM-DD');
    slot2_timeI = moment.tz(slot2_date + " " + slot2_time, tz).utcOffset('+0000').format('HH:mm');
    slot3_dateI = moment.tz(slot3_date + " " + slot3_time, tz).utcOffset('+0000').format('YYYY-MM-DD');
    slot3_timeI = moment.tz(slot3_date + " " + slot3_time, tz).utcOffset('+0000').format('HH:mm');

    var slot1_time_end = moment(slot1_date + " " + slot1_time).add(call_duration, "minutes").format('HH:mm');
    var slot2_time_end = moment(slot2_date + " " + slot2_time).add(call_duration, "minutes").format('HH:mm');
    var slot3_time_end = moment(slot3_date + " " + slot3_time).add(call_duration, "minutes").format('HH:mm');


    //sessoin mentor id req.session.userId;    
    //
    var currentUTC = constants.CURR_UTC_DATETIME();
    var err;
    var queryParam = {
        invitation_id: "DEFAULT",
        requester_id: req.session.userId,
        user_id: mentorId,
        invoice_id: 0,
        meeting_type: type,
        meeting_purpose: meet_purpose,
        meeting_purpose_indetail: more_details,
        duration: call_duration,
        slot_date: slot1_dateI,
        slot_time: slot1_timeI,
        document: documents,
        status: 'pending',
        created_date: moment.utc().format()
    };
    var insertId;
    var strQuery = 'insert into meeting_invitations SET ?';
    var slotsForMail = [];
    var invoiceId = 0;


    async.parallel([
        // function call for slot 1 to check minimuin duration check
        function (checkMeetingInvitationSlotMinimumDuration) {
            var objDataParamsForMinimunDuration = {
                startDateTime: slot1_dateI + " " + slot1_timeI + ":00",
                slotCount: 1
            };

            checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                checkMeetingInvitationSlotMinimumDuration(null, objResponse);
            });
        },
        // function call for slot 2 to check minimuin duration check
        function (checkMeetingInvitationSlotMinimumDuration) {
            var objDataParamsForMinimunDuration = {
                startDateTime: slot2_dateI + " " + slot2_timeI + ":00",
                slotCount: 2
            };

            checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                checkMeetingInvitationSlotMinimumDuration(null, objResponse);
            });
        },
        // function call for slot 3 to check minimuin duration check
        function (checkMeetingInvitationSlotMinimumDuration) {
            var objDataParamsForMinimunDuration = {
                startDateTime: slot3_dateI + " " + slot3_timeI + ":00",
                slotCount: 3
            };

            checkSlotBookingMinimunTimeDuration(req, res, objDataParamsForMinimunDuration, function (req, res, objResponse) {
                checkMeetingInvitationSlotMinimumDuration(null, objResponse);
            });
        },
        // function call to check slot 1 already accepted.
        function (callbackForSlot) {
            var dataToSend = {
                slot: slot1_date,
                startTime: slot1_time,
                endTime: slot1_time_end,
                mentorTimezone: tz,
                mentorId: mentorId,
                slotcount: 1
            };

            checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                callbackForSlot(null, arrSlotData);
            });
        },
        // function call to check slot 2 already accepted.
        function (callbackForSlot) {
            var dataToSend = {
                slot: slot2_date,
                startTime: slot2_time,
                endTime: slot2_time_end,
                mentorTimezone: tz,
                mentorId: mentorId,
                slotcount: 2
            };

            checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                callbackForSlot(null, arrSlotData);
            });
        },
        // function call to check slot 3 already accepted.
        function (callbackForSlot) {

            var dataToSend = {
                slot: slot3_date,
                startTime: slot3_time,
                endTime: slot3_time_end,
                mentorTimezone: tz,
                mentorId: mentorId,
                slotcount: 3
            };
            checkSlotForPricateInvite(req, res, dataToSend, function (req, res, arrSlotData) {
                callbackForSlot(null, arrSlotData);
            });
        }
    ],
            function (err, results) {

                console.log(results);

                var toBeContinue = true;
                var flagAlreadyError = false;
                var flagMinimumSlotTimeError = false;

                var conflictedSlot = [];
                var minimumSlotTimeError = [];
                var minimumExpectedSlot = "";
                var adminTimeDiff = "";
                results.forEach(function (reachResult) {

                    if (typeof reachResult.msg != 'undefined') {
                        if (reachResult.msg == "already") {
                            toBeContinue = false;
                            flagAlreadyError = true;
                            conflictedSlot.push(reachResult.slotcount);
                        }
                    }

                    if (typeof reachResult.flagSlotAllowed != 'undefined') {
                        if (!reachResult.flagSlotAllowed && reachResult.flagConsider) {

                            var meetingExpectedTimeFormat = moment.tz(reachResult.minimumExpectedTime, "UTC").tz(req.session.timezone).format('YYYY-MM-DD hh:mm a');

                            toBeContinue = false;
                            flagMinimumSlotTimeError = true;
                            minimumSlotTimeError.push(reachResult.slotCount);
                            minimumExpectedSlot = meetingExpectedTimeFormat;
                            adminTimeDiff = reachResult.adminTimeDiff;
                        }
                    }


                });

                if (toBeContinue == false) {

                    if (flagMinimumSlotTimeError == true) {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "minimumSlotDiff",
                            invoiceId: invoiceId,
                            objParams: {},
                            "data": minimumSlotTimeError,
                            "minimumExpectedSlot": minimumExpectedSlot,
                            "adminTimeDiff": adminTimeDiff
                        }));
                        res.end();
                    } else if (flagAlreadyError == true) {
                        res.writeHead(200, {'Content-Type': 'application/json'});
                        res.write(JSON.stringify({
                            "message": "already",
                            invoiceId: invoiceId,
                            objParams: {},
                            "data": conflictedSlot
                        }));
                        res.end();
                    }
                } else {
                    // Insert into invitation table
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {

                        meetingInvitationId = results.insertId;
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {
                            var strQuery2 = 'insert into invoice SET ?';
                            // Insert into invoice table
                            var queryParam2 = {
                                user_id: req.session.userId,
                                invitation_id: meetingInvitationId,
                                meeting_id: "0",
                                status: 'unpaid',
                                created_date: currentUTC
                            };
                            dbconnect.executeQuery(req, res, strQuery2, queryParam2, function (req, res, error, results, fields) {
                                invoiceId = results.insertId;
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"}));
                                    res.end();
                                } else {


                                    var strQuery1 = 'insert into meeting_invitation_slot_mapping (invitation_id, mentor_id, date, time, status) VALUES ';
                                    for (var i = 0; i < 3; i++) {
                                        var slots = {};
                                        if (i == 0) {
                                            if (slot1_date != '' && slot1_date != null && slot1_time != null && slot1_time != '') {
                                                slot_date = slot1_dateI;
                                                slot_time = slot1_timeI;
                                                slots.date = slot1_dateI;
                                                slots.time = slot1_timeI;
                                                slotsForMail.push(slots);
                                            } else {
                                                continue;
                                            }
                                        } else if (i == 1) {
                                            if (slot2_date != '' && slot2_date != null && slot2_time != null && slot2_time != '') {
                                                slot_date = slot2_dateI;
                                                slot_time = slot2_timeI;
                                                slots.date = slot2_dateI;
                                                slots.time = slot2_timeI;
                                                slotsForMail.push(slots);
                                            } else {
                                                continue;
                                            }
                                        } else if (i == 2) {
                                            if (slot3_date != '' && slot3_date != null && slot3_time != null && slot3_time != '') {
                                                slot_date = slot3_dateI;
                                                slot_time = slot3_timeI;
                                                slots.date = slot3_dateI;
                                                slots.time = slot3_timeI;
                                                slotsForMail.push(slots);
                                            } else {
                                                continue;
                                            }
                                        }

                                        //id, invitation_id, mentor_id, date, time, status

                                        strQuery1 += ' ("' + meetingInvitationId + '", "' + mentorId + '", "' + slot_date + '", "' + slot_time + '", "pending" ), ';
                                    }

                                    strQuery1 = strQuery1.replace(/, $/, '');
                                    var queryParam1 = [];
                                    // Insert into invitation_slot mapping table
                                    dbconnect.executeQuery(req, res, strQuery1, queryParam1, function (req, res, error, results, fields) {
                                        if (error) {
                                            res.writeHead(400, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "error"}));
                                            res.end();
                                        } else {

                                            var objParamsCaptureInvitationDetails = {
                                                invitationId: meetingInvitationId
                                            };

                                            captureMeetingInvitationDetailsForBookiing(req, res, objParamsCaptureInvitationDetails, function (req, res, objDataResponse) {

                                            });

                                            var objParams = {mentorId: mentorId, slots: slotsForMail, duration: call_duration};
                                            res.writeHead(200, {'Content-Type': 'application/json'});
                                            res.write(JSON.stringify({
                                                "message": "success",
                                                invoiceId: invoiceId,
                                                objParams: objParams
                                            }));
                                            res.end();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
};
exports.slotBooking = slotBooking;



var captureMeetingInvitationDetailsForBookiing = function (req, res, objParams, callback) {
    var invitationId = objParams.invitationId;

    var objToPass = {
        invitationId: invitationId
    };

    cronModel.getInvitationUserMentorData(req, res, objToPass, function (req, res, objMeetingRelatedData) {

        var meetingUserId = objMeetingRelatedData.meetingUserId;
        var meetingMentorId = objMeetingRelatedData.meetingMentorId;
        var invitationId = objMeetingRelatedData.invitationId;
        var meetingDuration = objMeetingRelatedData.meetingDuration;
        var userTimeZone = objMeetingRelatedData.userTimeZone;
        var mentorTimeZone = objMeetingRelatedData.mentorTimeZone;

        async.parallel([
            // function call to get mentor cancellation policies
            function (getPolicyInfoCallback) {
                var objDataPass = {
                    mentorId: meetingMentorId
                };
                miscFunction.getMentorCancellationPolicy(req, res, objDataPass, function (req, res, objMentorCancellationPolicyData) {
                    getPolicyInfoCallback(null, objMentorCancellationPolicyData);
                });
            },
            // function call to get mentor cancellation policy charges
            function (getPolicyInfoCallback) {
                miscFunction.getMentorCancellationCharges(req, res, {}, function (req, res, objResponse) {
                    getPolicyInfoCallback(null, objResponse);
                });
            }
        ], function (error, resultSet) {

            var objMentorCancellationPolicyForUser = resultSet[0];
            var objMentorCancellatiionPolicyForMentor = resultSet[1];

            var cancellationPolicy = "";

            if (objMentorCancellationPolicyForUser.length > 0) {
                cancellationPolicy = objMentorCancellationPolicyForUser[0].cancellationPolicy;

                var objToMakeEntryIntoMeetingPriceImageMaster = {
                    invitationId: invitationId,
                    cancellationPolicy: cancellationPolicy
                };
                makeEntryIntoMeetingPriceImageMaster(req, res, objToMakeEntryIntoMeetingPriceImageMaster, function (req, res, objDataResponse) {
                    var meetingPriceImageMasterId = objDataResponse.meetingPriceImageMasterId;


                    var strValuesToInsert = '';

                    objMentorCancellationPolicyForUser.forEach(function (element) {
                        var policyType = element.policyType;
                        var percentage = element.cancellationPercentage;
                        var hours = element.hours;

                        strValuesToInsert += '(' + meetingPriceImageMasterId + ', "' + policyType + '", "user", ' + percentage + ', "' + hours + '"), ';

                    });

                    if (typeof objMentorCancellatiionPolicyForMentor.cancellationPolicyId != 'undefined') {
                        var policyType = objMentorCancellatiionPolicyForMentor.policyType;
                        var percentage = objMentorCancellatiionPolicyForMentor.cancellationPercentage;
                        var hours = objMentorCancellatiionPolicyForMentor.hours;

                        strValuesToInsert += '(' + meetingPriceImageMasterId + ', "' + policyType + '", "mentor", ' + percentage + ', "' + hours + '"), ';
                    }

                    strValuesToInsert = strValuesToInsert.replace(/(^\s*,)|(,\s*$)/g, '');

                    if (strValuesToInsert != "") {
                        var queryParam = [];
                        var strQuery = 'INSERT INTO cancellation_policy_charges_image (meeting_price_image_master_id, policy_type, policy_for, percentage, hours) VALUES ' + strValuesToInsert;
                        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            }
                            callback(req, res, true);
                        });
                    }
                });
            }
        });
    });

}
exports.captureMeetingInvitationDetailsForBookiing = captureMeetingInvitationDetailsForBookiing;


var makeEntryIntoMeetingPriceImageMaster = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId
    var cancellationPolicy = objDataParams.cancellationPolicy;
    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = {
        invitation_id: invitationId,
        mentor_policy: cancellationPolicy,
        datex: currentUTC
    };
    var strQuery = 'INSERT INTO meeting_price_image_master SET ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            var meetingPriceImageMasterId = results.insertId;
            var objDataToResponse = {
                meetingPriceImageMasterId: meetingPriceImageMasterId
            };
            callback(req, res, objDataToResponse);
        }
    });
}
exports.makeEntryIntoMeetingPriceImageMaster = makeEntryIntoMeetingPriceImageMaster;


var checkSlotBookingMinimunTimeDuration = function (req, res, objDataParams, callback) {
    console.log("objDataParams :: ", objDataParams);

    // var objDataResponse = {
    //     flagSlotAllowed: true,
    //     flagConsider: true,
    //     minimumExpectedTime: "",
    //     slotCount: ""
    // };
    // callback(req, res, objDataResponse);


    var startDateTime = objDataParams.startDateTime;
    var slotCount = objDataParams.slotCount;

    var currentUTC = constants.CURR_UTC_DATETIME();

    if (moment(startDateTime, "YYYY-MM-DD HH:mm:ss", true).isValid()) {
        var systemVariable = "MINSLB";
        if (typeof objDataParams.privateInvite != 'undefined') {
            systemVariable = "MINSBP";
        }
        var queryParam = [currentUTC, systemVariable, startDateTime, currentUTC, systemVariable, systemVariable];

        var strQuery = '    SELECT TIMESTAMPDIFF(MINUTE, DATE_ADD(?, INTERVAL ( ' +
                '	        SELECT IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value * 60, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue     ' +
                '	        FROM system_settings ss     ' +
                '	        WHERE ss.code = ? ' +
                '       ) MINUTE), ?) timeDiff, ' +
                '       DATE_ADD(?, INTERVAL ( ' +
                '	        SELECT IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value * 60, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
                '	        FROM system_settings ss     ' +
                '	        WHERE ss.code = ? ' +
                '       ) MINUTE) minimumExpectedTime, ' +
                '       ( ' +
                '	        SELECT IF(ss.varused = 1, ss.int_value, IF(ss.varused = 2, ss.decimal_value, IF(ss.varused = 3, ss.char_value, IF(ss.varused = 4, ss.text_value, wysiwyg)))) settingValue ' +
                '	        FROM system_settings ss     ' +
                '	        WHERE ss.code = ? ' +
                '       ) adminTimeDiff';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrSlotBookingMinimumTime, fields) {
            if (error) {
                throw error;
                console.log("error :: ", error);
            }

            if (arrSlotBookingMinimumTime.length > 0) {
                var resultSet = arrSlotBookingMinimumTime[0];

                if (resultSet.timeDiff > 0) {
                    var objDataResponse = {
                        flagSlotAllowed: true,
                        flagConsider: true,
                        minimumExpectedTime: resultSet.minimumExpectedTime,
                        adminTimeDiff: resultSet.adminTimeDiff,
                        slotCount: slotCount
                    };
                    callback(req, res, objDataResponse);
                } else {
                    var objDataResponse = {
                        flagSlotAllowed: false,
                        flagConsider: true,
                        minimumExpectedTime: resultSet.minimumExpectedTime,
                        adminTimeDiff: resultSet.adminTimeDiff,
                        slotCount: slotCount
                    };
                    callback(req, res, objDataResponse);
                }
            } else {
                var objDataResponse = {
                    flagSlotAllowed: false,
                    flagConsider: true,
                    minimumExpectedTime: '',
                    slotCount: slotCount
                };
                callback(req, res, objDataResponse);
            }
        });
    } else {
        var objDataResponse = {
            flagSlotAllowed: false,
            flagConsider: false,
            minimumExpectedTime: '',
            slotCount: slotCount
        };
        callback(req, res, objDataResponse);
    }
};
exports.checkSlotBookingMinimunTimeDuration = checkSlotBookingMinimunTimeDuration;
// To send meeting notification email before 30 minutes of scheduled time to user
var sendMeetingInvitationRemainderToUser = function (req, res, userId, arrUserData, objParams, callback) {

    var mentorId = objParams.mentorId;
    var invitationId = objParams.invitation_id;
    var duration = objParams.duration;
    var meetingKey = objParams.meeting_key;
    var slot = {};
    slot.date = moment.tz(objParams.date + " " + objParams.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
    slot.startTime = moment.tz(objParams.date + " " + objParams.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
    slot.endTime = moment.tz(objParams.date + " " + objParams.time, "UTC").tz(arrUserData.zone_name).add(duration, "minutes").format('HH:mm');
    getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
        if (objParams.notified_to_user == "0")
        {
            var email = arrUserData.email;
            var userName = arrUserData.firstName;
            var mentorFullName = arrMentorData.firstName + " " + arrMentorData.lastName;
            var mentorSlug = arrMentorData.slug;
            // Send mail to user 
            var templateVariable = {
                templateURL: "mailtemplate/meetingrequestuserbefore30minutes",
                mentorFullName: mentorFullName,
                userName: userName,
                slot: slot,
                mentorSlug: mentorSlug,
                meetingkey: meetingKey
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: email,
                subject: "Hi " + userName + ", you have a scheduled meeting coming up in next 30 minutes with " + mentorFullName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    strQuery2 = "UPDATE meeting_invitations set notified_to_user='1' where invitation_id=" + invitationId;
                    // Update sent flag in invitation table
                    dbconnect.executeQuery(req, res, strQuery2, {}, function (req, res, error, results, fields) {
                        if (err)
                        {
                            console.log("failed to update invitation mail sent flag : user");
                        } else
                        {
                            console.log("invitation mail sent flag updated : user");
                        }
                    });
                }
            });
        }
        if (objParams.notified_to_mentor == "0")
        {
            // Send mail to mentor before 30 minutes of meeting scheduled time
            var mentorEmail = arrMentorData.email;
            var mentorName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
            var templateVariableMentor = {
                templateURL: "mailtemplate/meetingrequestmentorbefore30minutes",
                userFullName: userFullName,
                userName: mentorName,
                slot: slot,
                meetingkey: meetingKey
            };
            var mailParamsObject = {
                templateVariable: templateVariableMentor,
                to: mentorEmail,
                subject: "Hi " + mentorName + ", you have a scheduled meeting coming up in next 30 minutes with " + userFullName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    strQuery2 = "UPDATE meeting_invitations set notified_to_mentor='1' where invitation_id=" + invitationId;
                    // Update sent flag in invitation table
                    dbconnect.executeQuery(req, res, strQuery2, {}, function (req, res, error, results, fields) {
                        if (err)
                        {
                            console.log("failed to update invitation mail sent flag : mentor");
                        } else
                        {
                            console.log("invitation mail sent flag updated : mentor");
                        }
                    });
                }
            });
        }
    });
};
exports.sendMeetingInvitationRemainderToUser = sendMeetingInvitationRemainderToUser;
var sendMeetingInvitationToUser = function (req, res, userId, arrUserData, objParams, callback) {
    var mentorId = objParams.mentorId;
    var slotsInMail = [];
    var slots = objParams.slots;
    var duration = objParams.duration;
    var amount = objParams.amount;
    var index = 1;
    slots.forEach(function (slot) {
        var slotsInMailObj = {};
        slotsInMailObj.index = index;
        slotsInMailObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
        slotsInMailObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
        slotsInMailObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(duration, "minutes").format('HH:mm');
        slotsInMailObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
        slotsInMail.push(slotsInMailObj);
        index++;
    });
    getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
        mentorModel.getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var email = arrUserData.email;
            var userName = arrUserData.firstName;
            var mentorFullName = arrMentorData.firstName + " " + arrMentorData.lastName;

            // Send mail to user 
            var templateVariable = {
                templateURL: "mailtemplate/meetingrequestuser",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsInMail
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: email,
                subject: "Hi " + userName + ", you have requested a meeting with " + mentorFullName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send mail to user 
            var templateVariable = {
                templateURL: "mailtemplate/meetingrequestusercardauth",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsInMail,
                amount: amount
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: email,
                subject: "Hi " + userName + ", you have pre-authorized your payment card for a booking request made to " + mentorFullName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });


            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingrequestmessage.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsInMail
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res, callback);
        });
    });
};
exports.sendMeetingInvitationToUser = sendMeetingInvitationToUser;
var sendMeetingInvitationToMentor = function (req, res, userId, arrUserData, objParams, callback) {

    var mentorId = objParams.mentorId;
    var slotsInMail = [];
    var slots = objParams.slots;
    var duration = objParams.duration;
    var index = 1;
    getUserInformation(req, res, mentorId, {}, function (req, res, mentorId, arrMentorData, objParamsData) {
        mentorModel.getMentorDetails(req, res, mentorId, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            slots.forEach(function (slot) {
                var slotsInMailObj = {};
                slotsInMailObj.index = index;
                slotsInMailObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsInMailObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsInMailObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(duration, "minutes").format('HH:mm');
                slotsInMailObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsInMail.push(slotsInMailObj);
                index++;
            });
            var email = arrMentorData.email;
            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;
            var templateVariable = {
                templateURL: "mailtemplate/meetingrequestmentor",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsInMail
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: email,
                subject: "Hi " + userName + ", you have a meeting  request from " + userFullName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to mentor's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetingrequestmessagementor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsInMail
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res, callback);
        });
    });
};
exports.sendMeetingInvitationToMentor = sendMeetingInvitationToMentor;
var bookmarkMentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentorId = reqBody.mentorId;
    //var user_id = reqBody.user_id;
    //var mentor_id = reqBody.mentor_id;

    req.checkBody('mentorId', 'Please enter Message').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [req.session.userId, mentorId];
        var strQuery = 'select * from bookmarked_mentor where user_id = ? AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"}));
                res.end();
            } else {

                if (results.length > 0) {

                    var queryParam = [req.session.userId, mentorId];
                    var strQuery = 'Delete from bookmarked_mentor where user_id = ? AND mentor_id = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "Bookmarked removed successfully."}));
                            res.end();
                        }
                    });
                } else {

                    var queryParam = {
                        id: "DEFAULT",
                        mentor_id: mentorId,
                        user_id: req.session.userId,
                        created_date: moment.utc().format()
                    };
                    var strQuery = 'insert into bookmarked_mentor SET ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {
                            res.writeHead(200, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "Mentor bookmarked successfully."}));
                            res.end();
                        }
                    });
                }
            }
        });
    }
};
exports.bookmarkMentor = bookmarkMentor;
var bookmarkThisMentor = function (req, res, callback) {
    var reqBody = req.body;
    var mentorId = reqBody.mentorId;
    var loggedInUserId = req.session.userId;
    req.checkBody('mentorId', 'Please Select Valid mentor').isInt();
    var errors = req.validationErrors();
    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, [], "1", 'INVMENTOR');
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [req.session.userId, mentorId];
        var strQuery = '   SELECT * ' +
                '   FROM bookmarked_mentor ' +
                '   WHERE user_id = ? ' +
                '   AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
            } else {

                if (results.length > 0) {
                    sendResponse.sendJsonResponse(req, res, 200, [], "0", 'MTRALRDYBOOKM');
                } else {

                    var queryParam = {
                        mentor_id: mentorId,
                        user_id: loggedInUserId,
                        created_date: currentUTC
                    };
                    var strQuery = '   INSERT into bookmarked_mentor SET ? ';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                        } else {
                            sendResponse.sendJsonResponse(req, res, 200, [], "0", 'MTRBOOKMARKSUCC');
                        }
                    });
                }
            }
        });
    }
}
exports.bookmarkThisMentor = bookmarkThisMentor;
var removeBookmarkedMentor = function (req, res, callback) {
    var reqBody = req.body;
    var mentorId = reqBody.mentorId;
    var loggedInUserId = req.session.userId;
    req.checkBody('mentorId', 'Please Select Valid mentor').isInt();
    var errors = req.validationErrors();
    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, [], "1", 'INVMENTOR');
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [loggedInUserId, mentorId];
        var strQuery = ' DELETE FROM bookmarked_mentor WHERE user_id = ? AND mentor_id = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
            } else {
                sendResponse.sendJsonResponse(req, res, 200, [], "0", 'MTRBOOKMARRMD');
            }
        });
    }
}
exports.removeBookmarkedMentor = removeBookmarkedMentor;
var chkbookmarkMentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentorId = reqBody.mentorId;
    //var user_id = reqBody.user_id;
    //var mentor_id = reqBody.mentor_id;

    req.checkBody('mentorId', 'Please enter Message').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [req.session.userId, mentorId];
        var strQuery = 'select * from bookmarked_mentor where user_id = ? AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"}));
                res.end();
            } else {

                if (results.length > 0) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "found"}));
                    res.end();
                } else {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "not"}));
                    res.end();
                }
            }
        });
    }
};
exports.chkbookmarkMentor = chkbookmarkMentor;
var getUserFavoriteMentor = function (req, res, callback) {

    var queryParam = [req.session.userId];
    var strQuery = ' Select u.user_id,CONCAT(ud.first_name," ",ud.last_name) name,ct.city_id,md.punchline,md.like_count,GROUP_CONCAT(tag_name,",") AS tags,ct.city_name ,u.email email,u.phone_country phoneCountry, u.phone_number phoneNumber, u.country_code countryCode, u.user_type userType, u.registered_from registeredFrom, u.slug slug, u.email_verify_code emailVerifyCode, u.status status, ud.profile_image profileImage,c.country_id ,c.country_name countryName,md.overview,md.profession,md.company_name,md.hourly_rate,md.currency,ROUND(md.rating) as rating,md.hide_calendar ' +
            '   From users u ' +
            '   LEFT JOIN mentor_organisation_tags mot ON mot.user_id = u.user_id ' +
            '   LEFT JOIN user_details ud ON u.user_id = ud.user_id ' +
            '   LEFT JOIN bookmarked_mentor bm ON bm.mentor_id = u.user_id ' +
            '   LEFT JOIN countries c ON ud.country_id = c.country_id ' +
            '   LEFT JOIN cities ct ON ud.city_id = ct.city_id ' +
            '   LEFT JOIN mentor_details md ON md.user_id = ud.user_id  ' +
            '   WHERE bm.user_id = ? GROUP BY u.user_id';
    return false;
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, {});
        } else {
            setImagePathOfAllUsers(req, res, results, 0, [], callback);
        }
    });
}
exports.getUserFavoriteMentor = getUserFavoriteMentor;
var setImagePathOfAllUsers = function (req, res, resultSet, currentIndex, arrFinalData, callback) {
    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var profilePicture = thisObj.profileImage;
        var finalProfileImageUrl = constants.ACCESSURL + "images/noimg.png";
        if (profilePicture != "") {
            fs.stat("./uploads/" + profilePicture, function (err, stat) {

                if (err == null) {
                    finalProfileImageUrl = constants.ACCESSURL + "uploads/" + profilePicture;
                }

                thisObj.finalProfileImageUrl = finalProfileImageUrl;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);
                currentIndex++;
                setImagePathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callback);
            });
        } else {

            thisObj.finalProfileImageUrl = finalProfileImageUrl;
            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            currentIndex++;
            setImagePathOfAllUsers(req, res, resultSet, currentIndex, arrFinalData, callback);
        }
    } else {
        callback(req, res, arrFinalData);
    }
}
exports.setImagePathOfAllUsers = setImagePathOfAllUsers;
var getUserMeetingData = function (req, res, callback) {

    //var queryParam = [req.session.userId];

    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var id = reqBody.id;
    if (id != "") {
        var search = " mi.user_id = " + id + " AND ";
    } else {
        var search = "";
    }

    var queryParam = [1];
    var strQuery = ' Select mi.invitation_id, u.user_id,CONCAT(ud.first_name," ",ud.last_name) name,u.email email, u.user_type userType, u.slug slug, ud.profile_image profileImage,mi.meeting_type,mi.meeting_purpose,mi.duration,DATE_FORMAT(mi.slot_date, "%d %b %Y") as slot_date,mi.slot_time,mi.status ' +
            '   From meeting_invitations mi ' +
            '   LEFT JOIN users u ON mi.user_id = u.user_id ' +
            '   LEFT JOIN user_details ud ON u.user_id = ud.user_id ' +
            '   WHERE mi.requester_id = ? AND ' + search + ' (mi.status = "accepted" OR mi.status = "completed")';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, {});
        } else {
            setImagePathOfAllUsers(req, res, results, 0, [], callback);
        }
    });
}
exports.getUserMeetingData = getUserMeetingData;
var likeMentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentorId = reqBody.mentorId;
    //var user_id = reqBody.user_id;
    //var mentor_id = reqBody.mentor_id;

    req.checkBody('mentorId', 'Please enter Message').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [req.session.userId, mentorId];
        var strQuery = 'select * from mentor_like_mapping where user_id = ? AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"}));
                res.end();
            } else {

                if (results.length > 0) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "Mentor already liked."}));
                    res.end();
                } else {
                    var queryParam = [mentorId];
                    var strQuery = 'update mentor_details set like_count = like_count + 1 where user_id = ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            res.writeHead(400, {'Content-Type': 'application/json'});
                            res.write(JSON.stringify({
                                "message": "error"}));
                            res.end();
                        } else {

                            var queryParam = {
                                id: "DEFAULT",
                                mentor_id: mentorId,
                                user_id: req.session.userId,
                                created_date: moment.utc().format()
                            };
                            var strQuery = 'insert into mentor_like_mapping SET ?';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    res.writeHead(400, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "error"}));
                                    res.end();
                                } else {
                                    res.writeHead(200, {'Content-Type': 'application/json'});
                                    res.write(JSON.stringify({
                                        "message": "Mentor liked successfully."}));
                                    res.end();
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};
exports.likeMentor = likeMentor;
var likeThisMentor = function (req, res, callback) {

    var reqBody = req.body;
    var mentorId = reqBody.mentorId;
    var loggedInUserId = req.session.userId;
    req.checkBody('mentorId', 'Please Select Valid mentor').isInt();
    var errors = req.validationErrors();
    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, [], "1", 'INVMENTOR');
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [loggedInUserId, mentorId];
        var strQuery = '   SELECT * ' +
                '   FROM mentor_like_mapping ' +
                '   WHERE user_id = ? ' +
                '   AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
            } else {

                if (results.length > 0) {
                    sendResponse.sendJsonResponse(req, res, 200, [], "1", 'ALRDYLKD');
                } else {
                    var queryParam = [mentorId];
                    var strQuery = '   UPDATE mentor_details ' +
                            '   SET like_count = like_count + 1 ' +
                            '   WHERE user_id = ? ';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                        } else {
                            var queryParam = {
                                mentor_id: mentorId,
                                user_id: loggedInUserId,
                                created_date: currentUTC
                            };
                            var strQuery = '   INSERT into mentor_like_mapping SET ? ';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                } else {
                                    var queryParam = [
                                        mentorId
                                    ];
                                    var strQuery = '   SELECT md.like_count ' +
                                            '   FROM mentor_details md ' +
                                            '   WHERE md.user_id = ? ';
                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrResultsCount, fields) {
                                        if (error) {
                                            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                        } else {
                                            if (arrResultsCount.length > 0) {
                                                var likeCount = arrResultsCount[0].like_count;
                                                var dataToSend = {
                                                    likeCount: likeCount
                                                }
                                                sendResponse.sendJsonResponse(req, res, 200, dataToSend, "0", 'MTRLKDSUCC');
                                            } else {
                                                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    }

}
exports.likeThisMentor = likeThisMentor;
var unLikeThisMentor = function (req, res, callback) {
    var reqBody = req.body;
    var mentorId = reqBody.mentorId;
    var loggedInUserId = req.session.userId;
    req.checkBody('mentorId', 'Please Select Valid mentor').isInt();
    var errors = req.validationErrors();
    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, [], "1", 'INVMENTOR');
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [loggedInUserId, mentorId];
        var strQuery = '   SELECT * ' +
                '   FROM mentor_like_mapping ' +
                '   WHERE user_id = ? ' +
                '   AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrREsultSelectMentorLikingMap, fields) {
            if (error) {
                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
            } else {
                if (arrREsultSelectMentorLikingMap.length == 0) {
                    sendResponse.sendJsonResponse(req, res, 200, [], "1", 'MENTORNOTLIKED');
                } else {

                    var queryParam = [mentorId];
                    var strQuery = '   UPDATE mentor_details ' +
                            '   SET like_count = like_count  - 1 ' +
                            '   WHERE user_id = ? ';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, resultUpdateCountCount, fields) {
                        if (error) {
                            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                        } else {
                            var queryParam = [
                                loggedInUserId,
                                mentorId
                            ]

                            var strQuery = ' DELETE FROM mentor_like_mapping WHERE user_id = ? AND mentor_id = ? ';
                            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, resultDeleteLikeEntry, fields) {
                                if (error) {
                                    sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                } else {
                                    var queryParam = [
                                        mentorId
                                    ];
                                    var strQuery = '   SELECT md.like_count ' +
                                            '   FROM mentor_details md ' +
                                            '   WHERE md.user_id = ? ';
                                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, arrResultsCount, fields) {
                                        if (error) {
                                            sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                        } else {
                                            if (arrResultsCount.length > 0) {
                                                var likeCount = arrResultsCount[0].like_count;
                                                var dataToSend = {
                                                    likeCount: likeCount
                                                }
                                                sendResponse.sendJsonResponse(req, res, 200, dataToSend, "0", 'MTRUNLKDSUCC');
                                            } else {
                                                sendResponse.sendJsonResponse(req, res, 200, [], "1", 'SERR');
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    }
}
exports.unLikeThisMentor = unLikeThisMentor;
var chklikeMentor = function (req, res, callback) {
    var reqBody = req.body;
    JSON.stringify(reqBody, null, 4);
    var mentorId = reqBody.mentorId;
    //var user_id = reqBody.user_id;
    //var mentor_id = reqBody.mentor_id;

    req.checkBody('mentorId', 'Please enter Message').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.writeHead(400, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "message": "error"}));
        res.end();
    } else {
        var currentUTC = constants.CURR_UTC_DATETIME();
        var queryParam = [req.session.userId, mentorId];
        var strQuery = 'select * from mentor_like_mapping where user_id = ? AND mentor_id = ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                res.writeHead(400, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"}));
                res.end();
            } else {

                if (results.length > 0) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "found"}));
                    res.end();
                } else {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(JSON.stringify({
                        "message": "not"}));
                    res.end();
                }
            }
        });
    }
};
exports.chklikeMentor = chklikeMentor;
var chklikeMentorFavPage = function (req, res, callback) {
    var reqBody = req.body;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [req.session.userId];
    var strQuery = 'select mentor_id from mentor_like_mapping where user_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            res.writeHead(400, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "message": "error"}));
            res.end();
        } else {

            if (results.length > 0) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": results}));
                res.end();
            } else {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "data": "not"}));
                res.end();
            }
        }
    });
};
exports.chklikeMentorFavPage = chklikeMentorFavPage;
var uploadBookingDocument = function (req, res) {
    var responseData1 = {};
    var responseData = {};
    var form = new formidable.IncomingForm();
    form.multiples = true;
    form.uploadDir1 = path.join('uploads/slotbookingdocs');
    form.uploadDir = path.join(__dirname, '../uploads/slotbookingdocs');
    form.on('file', function (field, file) {
        var uploadedFileName = 'slotbooking-' + Date.now() + path.extname(file.name);
        var uploadedPath = path.join(form.uploadDir1, uploadedFileName)
        fs.rename(file.path, uploadedPath);
        var fileSize = file.size;
        var fileName = file.name;
        var fileType = file.type;
        var filePath = file.path;
        var currentUTC = constants.CURR_UTC_DATETIME();
        var params = {
            user_id: req.session.userId,
            category: 'call_booking',
            document_name: uploadedFileName,
            document_real_name: fileName,
            document_type: fileType,
            document_size: fileSize,
            document_path: uploadedPath,
            document_url: '',
            document_mime: fileType,
            created_date: currentUTC,
            updated_date: currentUTC
        };
        // Do Document entry in database
        var queryParam = [params];
        var strQuery = ' INSERT INTO documents SET ?';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                console.log(error);
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "error"
                }));
                res.end();
            } else {
                responseData1.documentId = results.insertId;
                responseData1.name = fileName;
                responseData1.size = fileSize;
                responseData1.url = "";
                responseData1.thumbnailUrl = "";
                responseData1.deleteUrl = "";
                responseData1.deleteType = "DELETE";
                responseData.files = [responseData1];
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify(responseData));
                res.end();
            }
        });
    });
    form.on('error', function (err) {
        console.log('An error has occured: \n' + err);
    });
    form.parse(req, function (err, fields, filesss) {
//        var fileSize = filesss['files[]'].size;
    });
    form.on('end', function () {
        console.log("success");
    });
};
exports.uploadBookingDocument = uploadBookingDocument;
var updateAccountSettings_model = function (req, res, callback) {
    if (req.body.stop_mentorship || req.body.hide_profile_search || req.body.hide_profile_calendar) {

    }
};
exports.updateAccountSettings_model = updateAccountSettings_model;
/* Get meeting details , which are pending to send notification email prior to 30 minutes of scheduled time */
var getPendingMeetingNotificationUsers = function (req, res, callback) {
    var strQuery = 'SELECT DATE_FORMAT(mimap.date,"%Y-%m-%d") as date,\n\
                    mimap.time as time,\n\
                    mi.invitation_id,\n\
                    mi.duration as duration,\n\
                    mi.requester_id as userId,\n\
                    mi.user_id as mentorId,\n\
                    zo.zone_name as timeZone,\n\
                    mi.notified_to_user as notified_to_user,\n\
                    mi.notified_to_mentor as notified_to_mentor,\n\
                    md.meeting_url_key as meeting_key FROM `meeting_invitation_slot_mapping` mimap \n\
                    inner join `meeting_invitations` \n\
                    mi on mimap.invitation_id=mi.invitation_id \n\
                    left join meeting_details md on mimap.invitation_id=md.invitation_id\n\
                    left join user_details ud on mi.requester_id=ud.user_id\n\
                    left join zone zo on ud.timezone_id=zo.zone_id\n\
                    WHERE mimap.status="accepted" and (mi.notified_to_user="0" || mi.notified_to_mentor="0") and DATE_FORMAT(concat(date," " ,time),"%Y-%m-%d %H:%i" ) = DATE_FORMAT( NOW( ) + INTERVAL 30 MINUTE ,  "%Y-%m-%d %H:%i" ) limit 1';
    dbconnect.executeQuery(req, res, strQuery, [], function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            callback(req, res, []);
        } else {
            callback(req, res, results);
        }
    });
};
exports.getPendingMeetingNotificationUsers = getPendingMeetingNotificationUsers;
var checkUserBasicProfileComplete_model = function (req, res, callback) {
    var queryParams = [req.session.userId];
    var strQuery = " SELECT u.user_id, " +
            " IFNULL(u.dob, '') dob, " +
            " IFNULL(u.phone_number, '') phone_number, " +
            " IFNULL(u.email, '') email, " +
            " IFNULL(ud.first_name, '') first_name, " +
            " IFNULL(ud.last_name, '') last_name, " +
            " IF(ud.address_line1 != '', ud.address_line1, '') address_line1, " +
            " IF(ud.country_id != 0, ud.country_id, '') country_id, " +
            " IF(ud.city_id != 0, ud.city_id, '') city_id, " +
            " IF(ud.timezone_id != 0, ud.timezone_id, '') timezone_id, " +
            " IF(ud.user_brief != '', ud.user_brief, '') user_brief " +
            " FROM users u " +
            " LEFT JOIN user_details ud ON ud.user_id=u.user_id " +
            " WHERE u.user_id= ?";
    dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            callback(req, res, []);
        } else {
            callback(req, res, results[0]);
        }
    });
};
exports.checkUserBasicProfileComplete_model = checkUserBasicProfileComplete_model;

var getPrivateInvites_model = function (req, res, callback) {
    var queryParam = [req.session.userId];

    /*var strQuery = "SELECT upi.id,ud.profile_image profileImage, ud.first_name, ud.last_name, upi.msg,md.hourly_rate,md.currency,md.company_name , md.profession  " +
     " FROM user_private_invitation upi " +
     " LEFT JOIN user_details ud ON ud.user_id = upi.mentor_id " +
     " LEFT JOIN mentor_details md ON md.user_id = upi.mentor_id " +
     " WHERE upi.status = 0 " +
     " AND upi.user_id=? ORDER BY upi.id DESC";
     */
    var strQuery = "SELECT ud.profile_image profileImage, ud.first_name, ud.last_name, DATE_FORMAT(mi.created_date, '" + constants.MYSQL_DB_DATETIME_FORMAT + "') created_date, mi.invitation_id, mi.meeting_type, mi.meeting_purpose, mi.meeting_purpose_indetail, mi.duration, mi.document,md.user_id as mentorId ,md.hourly_rate,md.currency,md.company_name , md.profession,us.slug,ins.invoice_id " +
            " FROM meeting_invitations mi " +
            " LEFT JOIN user_details ud ON ud.user_id = mi.requester_id " +
            " LEFT JOIN users us ON us.user_id = mi.requester_id " +
            " LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
            " LEFT JOIN mentor_details md ON md.user_id = mi.requester_id " +
            " WHERE mi.status = 'pending' AND invitation_type = 1 " +
            " AND mi.user_id = ? AND mi.flag_payment_done = '0' ORDER BY mi.invitation_id DESC";


    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
        if (error) {
            throw error;
        }
        getUserPrivateInvitationsData(req, res, results, 0, [], function (req, res, arrMentorData) {
            setImagePathOfAllUsers(req, res, arrMentorData, 0, [], callback);
        });
    });
};
exports.getPrivateInvites_model = getPrivateInvites_model;

var getUserPrivateInvitationsData = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var private_id = thisObj.invitation_id;
        var queryParam = [private_id];

        /*var sqlQuery = ' SELECT psm.id, psm.date, psm.starttime,psm.endtime ' +
         ' FROM privateinvite_slot_mapping psm ' +
         ' LEFT JOIN user_private_invitation upi ON upi.id = psm.private_id ' +
         ' WHERE psm.private_id= ?';
         */
        var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                ' FROM meeting_invitation_slot_mapping mis ' +
                ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                ' WHERE mis.invitation_id= ?';

        dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                console.log(error);
            } else {
                thisObj.invitation_slots = results;
                arrFinalData.push(thisObj);
                resultSet.splice(0, 1);
                getUserPrivateInvitationsData(req, res, resultSet, currentIndex, arrFinalData, callback);
            }
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.getUserPrivateInvitationsData = getUserPrivateInvitationsData;

var checkUserProfileSlug = function (req, res, dataToVerify, callback) {
    if (typeof dataToVerify.userSlug == 'undefined') {
        callback(req, res, 'NOUSERFOUND', {});
    } else {
        var userSlug = dataToVerify.userSlug;
        var queryParam = [userSlug];
        var strQuery = ' SELECT user_id userId, email email, slug slug, user_type userType ' +
                ' FROM users u ' +
                ' WHERE u.status = "1" ' +
                ' AND u.delete_reason_id = "0" ' +
                ' AND u.slug = ? ';
        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results.length == 0) {
                callback(req, res, 'NOUSERFOUND', {});
            } else {
                results = results[0];
                callback(req, res, 'USERFOUND', results);
            }
        });
    }
}
exports.checkUserProfileSlug = checkUserProfileSlug;

var declineInvitation = function (req, res, callback) {
    if (typeof req.body.invitation_id == "undefined" || req.body.invitation_id == "") {
        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        return false;
    }

    var invitation_id = req.body.invitation_id;
    var mentor_id = req.body.mentor_id;
    var user_id = req.body.user_id;
    var call_duration = req.body.call_duration;
    var invite = req.body.invite;
    var typex = req.body.typex;
    var rval = req.body.rval;
    var transectionId = req.body.transectionId;
    var amount = req.body.amount;
    var invoiceid = req.body.invoiceid;

    var param = {
        typex: typex,
        reason: rval,
        created_date: constants.CURR_UTC_DATETIME()
    };

    var reason = "";
    var payusertype = "";

    if (invite == 1) {
        reason = "MEETINVIDECUSR";
        payusertype = "user";
    } else {
        reason = "MEETINVIDECMTR";
        payusertype = "user";
    }

    var dataParam = {
        amount: amount,
        transactionId: transectionId,
        invitation_id: invitation_id,
        invoiceid: invoiceid,
        paymentAction: "void",
        reason: reason,
        user_id: user_id,
        user_type: payusertype
    };

    paypalFunction.paypalVoidPayment(req, res, dataParam, function (req, res, payment) {
        // paypal_api.authorization.void(transectionId, config_opts, function (error, authorization) {
        if (payment == 'error') {
            callback(req, res, "error", "error");
        } else {
            var strQuery = 'insert into cancellation_reasons set ?';
            dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
                if (error) {
                    console.log(error);
                } else {

                    async.parallel([
                        function (declineCallback) {
                            var invitation_id = req.body.invitation_id;

                            var queryParam = [req.session.userId, results.insertId, invitation_id];
                            var sqlQuery = 'UPDATE meeting_invitations SET status = "declined",declined_by = ?,cancellation_reasons_id = ? ,updated_date = "' + constants.CURR_UTC_DATETIME() + '"  WHERE invitation_id = ?';
                            dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                                if (error) {
                                    declineCallback(null, 'error');
                                } else {
                                    declineCallback(null, 'success');
                                }
                            });
                        }/*,
                         function (declineCallback) {
                         var invitation_id = req.body.invitation_id;
                         var queryParam = [invitation_id];
                         var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="declined" WHERE invitation_id=? ';
                         dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                         if (error) {
                         declineCallback(null, 'error');
                         } else {
                         declineCallback(null, 'success');
                         }
                         });
                         }*/
                    ], function (err, results) {
                        invitation_update = results[0];
                        //invitation_slot_update = results[1];
                        invitation_slot_update = "success";

                        var invitationData = {
                            invitation_id: invitation_id,
                            duration: call_duration
                        };

                        if (invite == 0) {


                            var objDataParamsForUpdateInvoice = {
                                invitationId: invitation_id,
                                meetingUserId: user_id,
                                meetingMentorId: mentor_id
                            };
                            paymentModel.updateInvoiceForNormalInviteDeclineByMentor(req, res, objDataParamsForUpdateInvoice, function (req, res, objResponseData) {
                            });

                            getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                                getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                                    senddeclineInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    callback(req, res, invitation_update, invitation_slot_update);
                                });
                            });
                        } else {

                            var objDataParamsForUpdateInvoice = {
                                invitationId: invitation_id,
                                meetingUserId: user_id,
                                meetingMentorId: mentor_id
                            };
                            paymentModel.updateInvoiceForNormalInviteDeclineByUser(req, res, objDataParamsForUpdateInvoice, function (req, res, objResponseData) {
                            });

                            getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                                getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                                    senddeclinePrivateInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                                    callback(req, res, invitation_update, invitation_slot_update);
                                });
                            });
                        }
                    });
                }
            });
        }
    });
};
exports.declineInvitation = declineInvitation;

var senddeclineInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });
    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'declined') {
                var slotsMentorObj = {};
                var slotsUserObj = {};

                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                //}
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/declinedInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            console.log(arrUserData.email);
            console.log(arrMentorData.email);

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has declined your meeting request"
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/declinedInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/declinedInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", you have  declined a meeting request from " + arrUserData.firstName + " " + arrUserData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/declinedInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.senddeclineInvitationMail = senddeclineInvitationMail;

var senddeclinePrivateInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });
    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'declined') {
                var slotsMentorObj = {};
                var slotsUserObj = {};

                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                //}
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/declinedPrivateInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            console.log(arrUserData.email);
            console.log(arrMentorData.email);

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", You have declined a private meeting invite sent to you by " + arrMentorData.firstName + " " + arrMentorData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/declinedPrivateInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/declinedPrivateInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", " + arrUserData.firstName + " " + arrUserData.lastName + " has declined your private meeting invite "
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/declinedPrivateInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.senddeclinePrivateInvitationMail = senddeclinePrivateInvitationMail;

var cancelInvitation = function (req, res, callback) {
    if (typeof req.body.invitation_id == "undefined" || req.body.invitation_id == "") {
        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        return false;
    }

    var invitation_id = req.body.invitation_id;
    var mentor_id = req.body.mentor_id;
    var user_id = req.body.user_id;
    var mailmentor_id = "";
    var meetingType = req.body.meetingType;
    var call_duration = req.body.call_duration;
    var transactionId = req.body.transactionId;
    var amount = req.body.amount;
    var invoiceid = req.body.invoiceid;
    var reason = "";
    var payusertype = "";

    if (meetingType == 0) {
        reason = "MEETINVICANUSR";
        payusertype = "user";
    } else {
        reason = "MEETINVICANMTR";
        payusertype = "user";
    }


    var dataParam = {
        amount: amount,
        transactionId: transactionId,
        invitation_id: invitation_id,
        invoiceid: invoiceid,
        paymentAction: "void",
        //reason : "Invitation cancelled for meeting",
        reason: reason,
        user_id: user_id,
        user_type: payusertype
    };

    paypalFunction.paypalVoidPayment(req, res, dataParam, function (req, res, payment) {
        console.log(payment);
        // paypal_api.authorization.void(transectionId, config_opts, function (error, authorization) {
        if (payment == 'error') {
            callback(req, res, "error", "error");
        } else {
            async.parallel([
                function (cancelCallback) {
                    var invitation_id = req.body.invitation_id;

                    var queryParam = [invitation_id];
                    var sqlQuery = 'UPDATE meeting_invitations SET status = "cancelled" ,updated_date = "' + constants.CURR_UTC_DATETIME() + '"  WHERE invitation_id = ?';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            cancelCallback(null, 'error');
                        } else {
                            cancelCallback(null, 'success');
                        }
                    });
                },
                function (cancelCallback) {
                    /*var invitation_id = req.body.invitation_id;
                     var queryParam = [invitation_id];
                     var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="cancelled" WHERE invitation_id=? ';
                     dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                     if (error) {
                     cancelCallback(null, 'error');
                     } else {
                     cancelCallback(null, 'success');
                     }
                     });*/
                    cancelCallback(null, 'success');
                }
            ], function (err, results) {
                invitation_update = results[0];
                invitation_slot_update = results[1];

                var invitationData = {
                    invitation_id: invitation_id,
                    duration: call_duration
                };

                if (mentor_id == req.session.userId) {


                    var objDataParamsForUpdateInvoice = {
                        invitationId: invitation_id,
                        meetingUserId: user_id,
                        meetingMentorId: mentor_id
                    };
                    paymentModel.updateInvoiceForNormalInviteCancellByMentor(req, res, objDataParamsForUpdateInvoice, function (req, res, objResponseData) {
                    });

                    getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendMentorCancelInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                            callback(req, res, invitation_update, invitation_slot_update);
                        });
                    });
                } else {

                    var objDataParamsForUpdateInvoice = {
                        invitationId: invitation_id,
                        meetingUserId: user_id,
                        meetingMentorId: mentor_id
                    };
                    paymentModel.updateInvoiceForNormalInviteCancellByUser(req, res, objDataParamsForUpdateInvoice, function (req, res, objResponseData) {
                    });

                    getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendUserCancelInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                            callback(req, res, invitation_update, invitation_slot_update);
                        });
                    });
                }

            });
        }
    });

};
exports.cancelInvitation = cancelInvitation;

var sendMentorCancelInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'cancelled') {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                console.log(slotsMentorObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });

            // Send mail to user           

            var templateVariable = {
                templateURL: "mailtemplate/privateinvitedeleteuser",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                userType: "user",
                mailuser: req.session.userType
            };


            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + userFullName + ", " + arrMentorData.firstName + " " + arrMentorData.lastName + " has cancelled a invitation sent to you before your acceptance."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/privateinvitedeleteusermessage.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/privateinvitedeletementor",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                userType: "mentor",
                mailuser: req.session.userType
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", you have cancelled a invitation sent to   " + userFullName + " before acceptance"
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to mentor inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/privateinvitedeletementormessage.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };

            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendMentorCancelInvitationMail = sendMentorCancelInvitationMail;

var sendUserCancelInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'cancelled') {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                console.log(slotsMentorObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });

            // Send mail to user           

            var templateVariable = {
                templateURL: "mailtemplate/meetinginvitedeleteuser",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                userType: "user",
                mailuser: req.session.userType
            };


            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + userFullName + ", you have cancelled an invite sent to " + arrMentorData.firstName + " " + arrMentorData.lastName + " before acceptance."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            var templateVariable = {
                templateURL: "mailtemplate/meetinginvitedeleteuserchg",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                userType: "user",
                mailuser: req.session.userType
            };


            var userName = arrMentorData.firstName;
            var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + userFullName + ", you have cancelled an invite sent to " + arrMentorData.firstName + " " + arrMentorData.lastName + " before acceptance."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });


            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetinginvitedeleteusermsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/meetinginvitedeletementor",
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                userType: "mentor",
                mailuser: req.session.userType
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", " + userFullName + " had to cancel an invite sent to you before your acceptance "
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to mentor inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/meetinginvitedeletementormsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };

            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendUserCancelInvitationMail = sendUserCancelInvitationMail;

var getMentorDeclinedInvitationsData = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];

        async.parallel([
            function (invitationCallback) {
                var document = thisObj.document;
                if (document != '') {
                    var queryParam = [];
                    var sqlQuery = ' SELECT document_id, document_name, document_real_name, document_size, document_path FROM documents ' +
                            ' WHERE document_id IN (' + document + ') ' +
                            ' AND is_deleted=0';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            invitationCallback(null, []);
                        } else {
                            miscFunction.checkDocumentExistOnPath(req, res, results, 0, [], function (req, res, finalDocument) {
                                invitationCallback(null, finalDocument);
                            });
                        }
                    });
                } else {
                    invitationCallback(null, []);
                }
            },
            function (invitationCallback) {
                var invitation_id = thisObj.invitation_id;
                var queryParam = [invitation_id];
                var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                        ' FROM meeting_invitation_slot_mapping mis ' +
                        ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                        ' WHERE mis.invitation_id= ?';
                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        invitationCallback(null, []);
                    } else {
                        invitationCallback(null, results);
                    }
                });
            }
        ], function (err, results) {
            thisObj.invitation_documents = results[0];
            thisObj.invitation_slots = results[1];

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            getMentorDeclinedInvitationsData(req, res, resultSet, currentIndex, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.getMentorDeclinedInvitationsData = getMentorDeclinedInvitationsData;

var meetingsData = function (req, res, callback) {
    var timezone = req.body.timezone;

    async.parallel([
        function (meetingsDataCallback) {

            var extraParam = {
                meeting_type: "future",
                timezone: timezone
            };
            getMentorMeetingData(req, res, req.session.userId, extraParam, function (req, res, arrData) {
                meetingsDataCallback(null, arrData);
            });
        },
        function (meetingsDataCallback) {

            var extraParam = {
                meeting_type: "completed",
                timezone: timezone
            };
            getMentorMeetingData(req, res, req.session.userId, extraParam, function (req, res, arrData) {
                meetingsDataCallback(null, arrData);
            });
        }
    ], function (err, results) {
        futureData = results[0];
        completedData = results[1];

        var meetingDataRes = {
            futureData: futureData,
            completedData: completedData
        };

        callback(req, res, meetingDataRes);
    });
};
exports.meetingsData = meetingsData;

var meetingsDataPaging = function (req, res, callback) {

    var page = req.body.page;
    var datatype = req.body.datatype;
    var timezone = req.body.timezone;

    var extraParam = {
        meeting_type: datatype,
        page: page,
        timezone: timezone
    };
    getMentorMeetingData(req, res, req.session.userId, extraParam, function (req, res, arrData) {
        callback(req, res, arrData);
    });
};
exports.meetingsDataPaging = meetingsDataPaging;


var getMentorMeetingData = function (req, res, user_id, extraParam, callback) {

    var meetingType = extraParam.meeting_type;
    var timezone = extraParam.timezone;
    var page = 0;
    var thisobj = {};

    if (typeof extraParam.page != 'undefined' && !isNaN(extraParam.page) && extraParam.page > 0) {
        page = extraParam.page;
    }
    var whereCondition = "";

    var queryParam = [user_id, user_id, user_id, user_id, user_id, user_id, user_id];

    if (meetingType == "future") {
        whereCondition = " WHERE mi.status = 'accepted' AND mis.status = 'accepted'" +
                " AND (mi.user_id = ?  OR mi.requester_id = ?) AND mis.invitation_id = mi.invitation_id ORDER BY CONCAT_WS(' ', mis.date, mis.time) ASC";
    } else {
        whereCondition = " WHERE (mi.status != 'accepted' AND mi.status != 'pending') " +
                " AND (mi.user_id = ? OR mi.requester_id = ?) AND mis.invitation_id = mi.invitation_id GROUP BY mi.invitation_id ORDER BY CONCAT_WS(' ', mis.date, mis.time) DESC";
    }

    var pageLimit = frontConstant.MEETINGPERPAGE;

    var sqlQuery = "SELECT" +
            " mi.requester_id,mi.user_id,med.meeting_id,med.meeting_url_key,ur.review_id,md.user_id as mentor_id,ud.profile_image profileImage, " +
            " UPPER(CASE mi.status WHEN 'no_show_user' THEN 'No Show User' WHEN 'no_show_mentor' THEN 'No Show Mentor' WHEN 'no_show_both' THEN 'No Show Both' ELSE mi.status END) as status," +
            " ud.first_name, ud.last_name, UPPER(CONCAT(uds.first_name,' ', uds.last_name)) as declined_by ,DATE_FORMAT(mi.created_date, '" + constants.MYSQL_DB_DATETIME_FORMAT + "') created_date, mi.invitation_id, " +
            " mi.meeting_type,IF(TIMESTAMPDIFF(SECOND,NOW(),CONCAT(mis.date,' ',mis.time )) < 0,'cancelnoshow','cancelshow') AS cancelbtn , " +
            " mi.meeting_purpose,mi.meeting_purpose_indetail, mi.duration, mi.document,md.user_id as mentorId ,md.hourly_rate,md.currency,md.company_name , md.profession,us.slug,md.cancellation_policy_type, " +
            " IF(LENGTH(mi.meeting_purpose) > 80, CONCAT(SUBSTRING(mi.meeting_purpose,1,80), '...'), mi.meeting_purpose) meetingPurposeDisplay, LENGTH(mi.meeting_purpose) as meetingPurposeCount, " +
            " IF(LENGTH(mi.meeting_purpose_indetail) > 80, CONCAT(SUBSTRING(mi.meeting_purpose_indetail,1,80), '...'), mi.meeting_purpose_indetail) meetingPurposeDetailDisplay, LENGTH(mi.meeting_purpose_indetail) as meetingPurposeDetailCount, " +
            " ins.pay_to_mentor,ins.promocode_discount,ins.promocode_discount,ins.invoice_id,ins.total_payable_amount,ins.mentor_hourly_charge,ins.wissenx_user_charge,ins.wissenx_mentor_charge,ins.wissenx_tax_charge,mi.invitation_type,ins.final_pay_to_mentor,ins.final_user_paid_amount, " +
            " IF(mi.status != 'completed','noshow',IF(DATEDIFF(CURDATE(), mi.updated_date) > 2,'noshow','show')) AS reqRef , urs.user_type,mi.updated_date, rr.invitation_id  as aleardyRaised , wt.capture_id " +
            " FROM meeting_invitations mi " +
            " LEFT JOIN user_details ud ON IF(mi.requester_id != ?,ud.user_id = mi.requester_id,ud.user_id = mi.user_id) " +
            " LEFT JOIN user_details uds ON mi.declined_by = uds.user_id " +
            " LEFT JOIN refund_request rr ON mi.invitation_id = rr.invitation_id " +
            " LEFT JOIN users urs ON IF(mi.requester_id != ?,urs.user_id = mi.requester_id,urs.user_id = mi.user_id) " +
            " LEFT JOIN meeting_details med ON mi.invitation_id = med.invitation_id " +
            " LEFT JOIN user_reviews ur ON ur.meeting_detail_id = med.meeting_id AND ur.reviewer_id = ? " +
            " LEFT JOIN users us ON IF(mi.requester_id != ?,us.user_id = mi.requester_id,us.user_id = mi.user_id)  " +
            " LEFT JOIN invoice ins ON ins.invitation_id = mi.invitation_id " +
            " LEFT JOIN wallet_transection wt ON ins.invoice_id = wt.invoice_id AND wt.transection_type = 'PAYCAP' " +
            " LEFT JOIN mentor_details md ON  IF(mi.requester_id != ?, md.user_id = mi.requester_id, md.user_id = mi.user_id), meeting_invitation_slot_mapping mis " + whereCondition;

    sqlQuery += ' LIMIT ' + parseInt(page) * parseInt(pageLimit) + ' ,' + parseInt(pageLimit);

    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
            callback(req, res, {});
        } else {
            var mentor_timezone = "";
            if (timezone == "" || timezone == null) {
                mentor_timezone = req.session.zone;
            } else {
                mentor_timezone = timezone;
            }

            var j = 0;
            results.forEach(function (slot) {
                var created_date = moment.tz(slot.created_date, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format('Do MMMM, Y hh:mm A');
                var updated_date = moment.tz(slot.updated_date, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format('Do MMMM, Y hh:mm A');
                results[j].created_date = created_date;
                results[j].updated_date = updated_date;
                j++;
            });

            getUserMeetingsData(req, res, meetingType, results, timezone, 0, [], function (req, res, arrMentorData) {
                setImagePathOfAllUsers(req, res, arrMentorData, 0, [], function (req, res, MeetingData) {
                    callback(req, res, MeetingData);
                });
            });
        }
    });
};

var getUserMeetingsData = function (req, res, meetingType, resultSet, timezone, currentIndex, arrFinalData, callback) {
    if (resultSet.length > 0) {
        var thisObj = resultSet[0];
        var private_id = thisObj.invitation_id;
        var queryParam = [private_id];
        var mentor_timezone = "";
        var status = "";

        async.parallel([
            function (invitationCallback) {
                var document = thisObj.document;
                if (document != '') {
                    var queryParam = [];
                    var sqlQuery = ' SELECT document_id, document_name, document_real_name, document_size, document_path FROM documents ' +
                            ' WHERE document_id IN (' + document + ') ' +
                            ' AND is_deleted=0';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            invitationCallback(null, []);
                        } else {
                            miscFunction.checkDocumentExistOnPath(req, res, results, 0, [], function (req, res, finalDocument) {
                                invitationCallback(null, finalDocument);
                            });
                        }
                    });
                } else {
                    invitationCallback(null, []);
                }
            },
            function (invitationCallback) {
                if (timezone == "" || timezone == null) {
                    mentor_timezone = req.session.zone;
                } else {
                    mentor_timezone = timezone;
                }

                if (meetingType == 'future' || true) {
                    status = "and mis.status = 'accepted'";
                } else {
                    status = "and mis.status != 'accepted'";
                }

                var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                        ' FROM meeting_invitation_slot_mapping mis ' +
                        ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                        ' WHERE mis.invitation_id= ? ' + status;

                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        invitationCallback(null, []);
                    } else {
                        if (results.length > 0) {
                            var j = 0;
                            var i = 1;
                            results.forEach(function (slot) {
                                var starttext = moment.tz(slot.date_time, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format('Do MMMM, Y hh:mm A');
                                var startForEnd = moment.tz(slot.date_time, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format("YYYY-MM-DD HH:mm:ss");
                                //var endtext = moment.tz(slot.date_time, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).add(slot.duration, 'm').format('hh:mm A');
                                var endtext = moment(startForEnd).add(slot.duration, 'm').format('hh:mm A');

                                results[j].slotCount = i;
                                results[j].displayDate = starttext + " " + "to" + " " + endtext;
                                i++;
                                j++;
                                invitationCallback(null, results);
                            });
                        } else {
                            invitationCallback(null, []);
                        }
                    }
                });
            },
            function (invitationCallback) {

                var queryParam = [private_id];

                var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                        ' FROM meeting_invitation_slot_mapping mis ' +
                        ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                        ' WHERE mis.invitation_id= ? ' +
                        ' ORDER BY mis.id DESC ' +
                        ' LIMIT 0,1 ';

                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        invitationCallback(null, []);
                    } else {
                        if (results.length > 0) {
                            var thisResut = results[0];

                            var starttext = moment.tz(thisResut.date_time, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format('Do MMMM, Y hh:mm A');
                            var startForEnd = moment.tz(thisResut.date_time, "UTC").utcOffset(moment.tz(mentor_timezone).format('Z')).format("YYYY-MM-DD HH:mm:ss");
                            var endtext = moment(startForEnd).add(thisResut.duration, 'm').format('hh:mm A');

                            var objDisplayDate = {
                                starttext: starttext,
                                startForEnd: startForEnd,
                                endtext: endtext
                            }
                            invitationCallback(null, objDisplayDate);
                        } else {
                            invitationCallback(null, []);
                        }
                    }
                });
            }
        ], function (err, results) {
            thisObj.invitation_documents = results[0];
            thisObj.invitation_slots = results[1];
            thisObj.invitationLastSlot = results[2];
            thisObj.loggedinuser = req.session.userId;

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            getUserMeetingsData(req, res, meetingType, resultSet, timezone, currentIndex, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.getUserMeetingsData = getUserMeetingsData;

var timedoutCronMeetingSelect = function (req, res, objDataParams) {
    var flagNormalInvite = false;

    if (typeof objDataParams.flagNormalInvite != 'undefined') {
        flagNormalInvite = objDataParams.flagNormalInvite;
    }

    var objDataParamsToPassForMeetingType = {
        flagNormalInvite: flagNormalInvite
    };
    async.parallel([
        // function call to get all the pending invitations which are over 5 days.
        function (meetingExpireCallback) {
            cronModel.getInvitationsThatPassedExpieryTime(req, res, objDataParamsToPassForMeetingType, function (req, res, objExpiredInvitations) {
                meetingExpireCallback(null, objExpiredInvitations);
            });
        },
        // function call to get those pending meeting invites whoes slots date is less that 5 days and need to expire before 4 hours.
        function (meetingExpireCallback) {
            cronModel.getInvitationsThatAboutToExpireFromCurrentTime(req, res, objDataParamsToPassForMeetingType, function (req, res, objAboutToExpireInvitations) {
                meetingExpireCallback(null, objAboutToExpireInvitations);
            });
        }
    ], function (error, resultSet) {

        var arrInvitesThatPassedExpeiryTime = resultSet[0];
        var arrInvitesThatNeedsToExpireBeforeAccept = resultSet[1];

        recursiveMethodForExpiryMeetingInvites(req, res, arrInvitesThatPassedExpeiryTime, flagNormalInvite, 0, [], function (req, res, objFinalData) {

        });

        recursiveMethodForExpiryMeetingInvites(req, res, arrInvitesThatNeedsToExpireBeforeAccept, flagNormalInvite, 0, [], function (req, res, objFinalData) {

        });

    });
}
exports.timedoutCronMeetingSelect = timedoutCronMeetingSelect;


var recursiveMethodForExpiryMeetingInvites = function (req, res, arrResultSet, flagNormalInvite, counter, arrFinalData, callback) {
    if (arrResultSet.length > 0) {
        var thisResultSet = arrResultSet[0];
        var invitationId = thisResultSet.invitationId;
        var meetingDuration = thisResultSet.meetingDuration;
        var inviteType = thisResultSet.inviteType;
        var meetingUserId = thisResultSet.meetingUserId;
        var meetingMentorId = thisResultSet.meetingMentorId;
        var userTimeZone = thisResultSet.userTimeZone;
        var mentorTimeZone = thisResultSet.mentorTimeZone;

        async.parallel([
            // function call to time out the meeting invitation
            function (inviteExpireCallback) {
                var objDataToSend = {
                    invitationId: invitationId
                };
                expireMeetingInvite(req, res, objDataToSend, function (req, res, flagData) {
                    inviteExpireCallback(null, {});
                });
            },
            // function call to get transection id.
            function (inviteExpireCallback) {

                var queryParams = [invitationId];

                var strQuery = 'select pp.amount,pp.capture_id,pp.transaction_id,ins.invoice_id from meeting_invitations mi' +
                        '   LEFT JOIN invoice ins ON mi.invitation_id = ins.invitation_id ' +
                        '   LEFT JOIN payments_paypal pp ON pp.invoice_id = ins.invoice_id ' +
                        '   WHERE mi.invitation_id = ? ';

                dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results, fields) {
                    inviteExpireCallback(null, results);
                });
            }
        ], function (error, parallelResult) {

            var result = parallelResult[1];
            var transectionId = result[0].transaction_id;
            var invoiceid = result[0].invoice_id;
            var amount = result[0].amount;
            var captureId = result[0].capture_id;

            if (inviteType == "normalInvite") {
                var dataParam = {
                    transactionId: transectionId,
                    invitation_id: invitationId,
                    invoiceid: invoiceid,
                    amount: amount,
                    paymentAction: "Voided",
                    //reason: "Invitation expired for meeting",
                    reason: "MEETINVITIMEOUT",
                    user_id: meetingUserId,
                    user_type: "user"
                };
                paypalFunction.paypalVoidPayment(req, res, dataParam, function (req, res, payment) {
                });
            }
//            else {
//                var dataParam = {
//                    transactionId: transectionId,                    
//                    invoiceid: invoiceid,
//                    paymentAction: "refund",
//                    amount : amount,
//                    captureId : captureId
//                };
//                 paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, payment) {
//                });
//
//            }


            var invitationDataMentor = {
                invitation_id: invitationId,
                duration: meetingDuration,
                timeZone: mentorTimeZone
            };

            getUserInformation(req, res, meetingMentorId, invitationDataMentor, function (req, res, mentorId, arrMentorData, objParameterData) {
                var invitationDataUser = {
                    invitation_id: invitationId,
                    duration: meetingDuration,
                    timeZone: userTimeZone
                };
                getUserInformation(req, res, meetingUserId, invitationDataUser, function (req, res, userId, arrUserData, objParameterData) {
                    var invitationData = {
                        invitation_id: invitationId,
                        duration: meetingDuration,
                        inviteType: inviteType
                    }

                    // for private invite meeting timeout.
                    if (flagNormalInvite) {
                        var objDataToResentInvoiceDetailForPrivateMeetingTimeout = {
                            invitationId: invitationId,
                            meetingUserId: meetingUserId,
                            meetingMentorId: meetingMentorId
                        };
                        paymentModel.updateInvoiceForPrivateMeetingTimeOut(req, res, objDataToResentInvoiceDetailForPrivateMeetingTimeout, function (req, res, objResponse) {
                        });
                    }
                    sendMeetingTimoutedMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                });
            });

            arrResultSet.splice(0, 1);
            arrFinalData.push(thisResultSet);
            counter++;
            recursiveMethodForExpiryMeetingInvites(req, res, arrResultSet, flagNormalInvite, counter, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.recursiveMethodForExpiryMeetingInvites = recursiveMethodForExpiryMeetingInvites;

var sendMeetingTimoutedMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var inviteType = invitationData.inviteType;
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        inviteType: invitationData.inviteType,
        document: ''
    });


    var mentorIdPolicy = 0;

    // if (req.session.userType == "user") {
    //     mentorIdPolicy = arrUserData.user_id;
    // } else {
    //     mentorIdPolicy = arrMentorData.user_id;
    // }
    mentorIdPolicy = arrMentorData.user_id;

    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, mentorIdPolicy, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;



            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'timedout') {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                ///}
            });


            // for normal invite.
            if (inviteType == "normalInvite") {
                // Send mail to user
                var templateVariable = {
                    templateURL: "mailtemplate/timeoutmailuser",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr,
                    userType: "user"
                };


                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrUserData.email,
                    subject: "Hi " + userFullName + ",your meeting request to " + arrMentorData.firstName + " " + arrMentorData.lastName + " has expired."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttouser");
                    }
                });

                var templateVariable = {
                    templateURL: "mailtemplate/timeoutmailuserchg",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr,
                    userType: "user"
                };


                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrUserData.email,
                    subject: "Hi " + userFullName + ",your meeting request to " + arrMentorData.firstName + " " + arrMentorData.lastName + " has expired and hence pre-authorization on your payment card will be released shortly."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttouser");
                    }
                });


                // Send message to user's inbox
                var templateFile = fs.readFileSync('./views/mailtemplate/timeoutusermessage.handlebars', 'utf8');
                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr
                };
                req.body = {};
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.lstUserList = arrUserData.user_id;
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res);

                // Send mail to mentor
                var templateVariable = {
                    templateURL: "mailtemplate/timeoutmailmentor",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsMentorArr,
                    userType: "mentor"
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrMentorData.email,
                    subject: "Hi " + arrMentorData.firstName + ", a meeting request from " + userFullName + " has expired"
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttomentor");
                    }
                });
                // Send message to mentor inbox
                var templateFile = fs.readFileSync('./views/mailtemplate/timeoutmessagementor.handlebars', 'utf8');
                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsMentorArr
                };
                req.body = {};
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.lstUserList = arrMentorData.user_id;
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res);
            }
            // for private invite
            else {

                // Send mail to user           
                var templateVariable = {
                    templateURL: "mailtemplate/timeoutprivatemailuser",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr,
                    userType: "user"
                };


                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrUserData.email,
                    subject: "Hi " + userFullName + ",a private invite from " + arrMentorData.firstName + " " + arrMentorData.lastName + " has expired."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttouser");
                    }
                });

                var templateVariable = {
                    templateURL: "mailtemplate/timeoutmailuserchg",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr,
                    userType: "user"
                };


                var userName = arrMentorData.firstName;
                var userFullName = arrUserData.firstName + " " + arrUserData.lastName;

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrUserData.email,
                    subject: "Hi " + userFullName + ",your meeting request to " + arrMentorData.firstName + " " + arrMentorData.lastName + " has expired and hence pre-authorization on your payment card will be released shortly."
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttouser");
                    }
                });

                // Send message to user's inbox
                var templateFile = fs.readFileSync('./views/mailtemplate/timeoutprivateinviteusermessage.handlebars', 'utf8');
                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsUserArr
                };
                req.body = {};
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.lstUserList = arrUserData.user_id;
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res);

                // Send mail to mentor
                var templateVariable = {
                    templateURL: "mailtemplate/timeoutprivatemailmentor",
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsMentorArr,
                    userType: "mentor"
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: arrMentorData.email,
                    subject: "Hi " + arrMentorData.firstName + ", your private invite sent to " + userFullName + " has expired"
                };
                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("successmailsenttomentor");
                    }
                });
                // Send message to mentor inbox
                var templateFile = fs.readFileSync('./views/mailtemplate/timeoutprivatemessagementor.handlebars', 'utf8');
                var context = {
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData,
                    arrUserData: arrUserData,
                    mentorCancellationPolicy: mentorCancellationPolicy,
                    mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                    slots: slotsMentorArr
                };
                req.body = {};
                var template = handlebars.compile(templateFile);
                var html = template(context);
                req.body.lstUserList = arrMentorData.user_id;
                req.body.msgNewBody = html;
                req.body.sender_id = 1;
                req.body.sendNoResponse = 1;
                messagesModel.sendUserMessages(req, res);


            }
        });
    });
};
exports.sendMeetingTimoutedMail = sendMeetingTimoutedMail;


var expireMeetingInviteTimeSlots = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;

    var queryParams = [invitationId];

    var strQuery = '    UPDATE meeting_invitation_slot_mapping mism ' +
            '   SET mism.status = "timedout" ' +
            '   WHERE mism.invitation_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results, fields) {
        callback(req, res, {});
    });
};
exports.expireMeetingInviteTimeSlots = expireMeetingInviteTimeSlots;

var expireMeetingInvite = function (req, res, objDataParams, callback) {
    var invitationId = objDataParams.invitationId;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParams = [currentUTC, invitationId];

    var strQuery = '    UPDATE meeting_invitations mi' +
            '   SET mi.status = "timedout", ' +
            '   mi.updated_date = ? ' +
            '   WHERE mi.invitation_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results, fields) {
        callback(req, res, {});
    });

}
exports.expireMeetingInvite = expireMeetingInvite;

var chkPromoCode_model = function (req, res, callback) {

    var promocode = req.body.promocode;
    var mentorId = req.body.mentor_id;
    var user_id = req.session.userId;
    var param = [user_id, promocode];

    var strQuery = 'SELECT * FROM promo_codes WHERE user_id = ? AND promo_code = ? AND promo_code_status = 1';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
        } else {
            if (results.length > 0) {
                var thisResultSet = results[0];

                if (thisResultSet.is_used == "1") {
                    var objDataToResponse = {
                        flagMsg: 'is_used'
                    };
                    callback(req, res, objDataToResponse);
                } else if (thisResultSet.mentor_id != mentorId) {
                    var objDataToResponse = {
                        flagMsg: 'mentor_invalid'
                    };
                    callback(req, res, objDataToResponse);
                } else {
                    var objDataToResponse = {
                        flagMsg: 'promo_valid',
                        result: thisResultSet
                    };
                    callback(req, res, objDataToResponse);
                }


            } else {
                var objDataToResponse = {
                    flagMsg: 'no_found'
                };
                callback(req, res, objDataToResponse);
            }
        }
    });
};
exports.chkPromoCode_model = chkPromoCode_model;

var mentorCancelRequest_model = function (req, res, callback) {

    if (typeof req.body.invitationId == "undefined" || req.body.invitationId == "") {
        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        return false;
    }

    var invitation_id = req.body.invitationId;
    var mentor_id = req.body.mentorId;
    var user_id = req.body.userId;
    var call_duration = req.body.duration;
    var typex = req.body.typex;
    var rval = req.body.rval;
    var captureId = req.body.capture_id;

    var param = {
        typex: typex,
        reason: rval,
        created_date: constants.CURR_UTC_DATETIME(),
    };


    var strQuery = 'insert into cancellation_reasons set ?';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
        } else {

            async.parallel([
                function (cancelAfterCallback) {
                    var invitation_id = req.body.invitationId;

                    var queryParam = [req.session.userId, results.insertId, invitation_id];
                    var sqlQuery = 'UPDATE meeting_invitations SET status = "acceptedmeetingcancel",declined_by = ?,cancellation_reasons_id = ?,flag_cron_no_show_check = "Y", flag_cron_meeting_end_check = "Y", updated_date = "' + constants.CURR_UTC_DATETIME() + '"  WHERE invitation_id = ?';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            cancelAfterCallback(null, 'error');
                        } else {
                            cancelAfterCallback(null, 'success');
                        }
                    });
                },
                function (cancelAfterCallback) {

//                    var invitation_id = req.body.invitationId;
//                    var queryParam = [invitation_id];
//                    var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="acceptedmeetingcancel" WHERE invitation_id=? ';
//                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
//                        if (error) {
//                            cancelAfterCallback(null, 'error');
//                        } else {
//                            cancelAfterCallback(null, 'success');
//                        }
//                    });
//                    
                    cancelAfterCallback(null, 'success');
                }
            ], function (err, results) {
                console.log(err);
                invitation_update = results[0];
                invitation_slot_update = results[1];


                /**
                 * function call to update invoice table when mentor cancells the accepted invite.
                 */
                var objParamsToUpdateInvoiceForMentorCancellAcceptedInvite = {
                    invitationId: invitation_id,
                    meetingUserId: user_id,
                    meetingMentorId: mentor_id
                };
                paymentModel.updateInvoiceForMentorCancellAcceptedInvite(req, res, objParamsToUpdateInvoiceForMentorCancellAcceptedInvite, function (req, res, objResponse) {

                    var dataParam = {
                        amount: objResponse.amountBackToUser,
                        captureId: captureId,
                        invitation_id: invitation_id,
                        invoiceid: objResponse.invoiceId,
                        paymentAction: "DEBITED",
                        reason: "MENCANACCMEET",
                        user_id: user_id,
                        user_type: "user"
                    };
                    paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {

                        var data = {
                            invoice_id: objResponse.invoiceId,
                            user_id: mentor_id,
                            user_type: "mentor",
                            reason: "MENCANACCMEET",
                            transection_type: "PAYREF",
                            payment_type: "DEBITED",
                            payment_id: null,
                            transection_id: null,
                            capture_id: null,
                            refund_id: null,
                            amount: objResponse.mentorsCut,
                            status: "completed",
                            created_date: constants.CURR_UTC_DATETIME()
                        };
                        paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                            var objParams = {
                                user_id: mentor_id,
                                amount: objResponse.mentorsCut
                            };
                            paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                var data = {
                                    invoice_id: objResponse.invoiceId,
                                    user_id: "1",
                                    user_type: "admin",
                                    reason: "MENCANACCMEET",
                                    transection_type: "PAYREF",
                                    payment_type: "CREDITED",
                                    payment_id: null,
                                    transection_id: null,
                                    capture_id: null,
                                    refund_id: null,
                                    amount: objResponse.wissenxCut,
                                    status: 'completed',
                                    created_date: constants.CURR_UTC_DATETIME()
                                };
                                paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                    var objParams = {
                                        user_id: "1",
                                        amount: objResponse.wissenxCut
                                    };
                                    paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                    });
                                });
                            });
                        });
                    });


                    var invitationData = {
                        invitation_id: invitation_id,
                        duration: call_duration,
                        userAmt: objResponse.amountBackToUser
                    };

                    getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendMentorCancelAcceptedInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                            callback(req, res, "success");
                        });
                    });
                });
            });
        }
    });
};
exports.mentorCancelRequest_model = mentorCancelRequest_model;

var sendMentorCancelAcceptedInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    var userAmt = invitationData.userAmt;

    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;

            var slotsUserArr = [];
            var slotsMentorArr = [];

            invitationSlots.forEach(function (slot) {
                // if (slot.status == 'acceptedmeetingcancel') {
                var slotsMentorObj = {};
                var slotsUserObj = {};

                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');

                slotsUserArr.push(slotsUserObj);
                console.log(slotsMentorObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/cancelacceptedInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", Unfortunately " + arrMentorData.firstName + " " + arrMentorData.lastName + " had to cancel a confirmed meeting with you."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send mail to user charge
            var templateVariable = {
                templateURL: "mailtemplate/cancelacceptedInvitationMailUserchg",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                userAmt: userAmt
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", Unfortunately " + arrMentorData.firstName + " " + arrMentorData.lastName + " had to cancel a confirmed meeting with you.You will receive a full refund shortly."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/cancelacceptedInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/cancelacceptedInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", Sorry to hear that you had to cancel your confirmed meeting with " + arrUserData.firstName + " " + arrUserData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/cancelacceptedInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendMentorCancelAcceptedInvitationMail = sendMentorCancelAcceptedInvitationMail;

var userCancelRequest_model = function (req, res, callback) {
    if (typeof req.body.invitationId == "undefined" || req.body.invitationId == "") {
        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        return false;
    }

    var invitation_id = req.body.invitationId;
    var mentor_id = req.body.mentorId;
    var user_id = req.body.userId;
    var call_duration = req.body.duration;
    var typex = req.body.typex;
    var rval = req.body.rval;
    var captureId = req.body.captureId;
    var param = {
        typex: typex,
        reason: rval,
        created_date: constants.CURR_UTC_DATETIME(),
    };
    var strQuery = 'insert into cancellation_reasons set ?';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log(error);
        } else {


            async.parallel([
                function (cancelAfterCallback) {
                    var invitation_id = req.body.invitationId;
                    var queryParam = [results.insertId, req.session.userId, invitation_id];
                    var sqlQuery = 'UPDATE meeting_invitations SET status = "acceptedmeetingcancel",cancellation_reasons_id = ?,declined_by = ? ,updated_date = "' + constants.CURR_UTC_DATETIME() + '"  WHERE invitation_id = ?';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            cancelAfterCallback(null, 'error');
                        } else {
                            cancelAfterCallback(null, 'success');
                        }
                    });
                },
                function (cancelAfterCallback) {

//                    var invitation_id = req.body.invitationId;
//                    var queryParam = [invitation_id];
//                    var sqlQuery = ' UPDATE meeting_invitation_slot_mapping SET status="acceptedmeetingcancel" WHERE invitation_id=? ';
//                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
//                        if (error) {
//                            cancelAfterCallback(null, 'error');
//                        } else {
//                            cancelAfterCallback(null, 'success');
//                        }
//                    });

                    cancelAfterCallback(null, 'success');
                }
            ], function (err, results) {
                console.log(err);
                invitation_update = results[0];
                invitation_slot_update = results[1];
                /**
                 * function call to update invoice table when mentor cancells the accepted invite.
                 */
                var objParamsToUpdateInvoiceForUserCancellAcceptedInvite = {
                    invitationId: invitation_id,
                    meetingUserId: user_id,
                    meetingMentorId: mentor_id
                };
                paymentModel.updateInvoiceForUserCancellAcceptedInvite(req, res, objParamsToUpdateInvoiceForUserCancellAcceptedInvite, function (req, res, objResponse) {
                    var dataParam = {
                        amount: objResponse.amountBackToUser,
                        captureId: captureId,
                        invitation_id: invitation_id,
                        invoiceid: objResponse.invoiceId,
                        paymentAction: "CREDITED",
                        reason: "USRCANACCMEET",
                        user_id: user_id,
                        user_type: "user"
                    };
                    paypalFunction.paypalRefundPayment(req, res, dataParam, function (req, res, ref) {

                        var data = {
                            invoice_id: objResponse.invoiceId,
                            user_id: mentor_id,
                            user_type: "mentor",
                            reason: "USRCANACCMEET",
                            transection_type: "PAYREF",
                            payment_type: "CREDITED",
                            payment_id: null,
                            transection_id: null,
                            capture_id: null,
                            refund_id: null,
                            amount: objResponse.mentorsCut,
                            status: "completed",
                            created_date: constants.CURR_UTC_DATETIME()
                        };
                        paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {
                            var objParams = {
                                user_id: mentor_id,
                                amount: objResponse.mentorsCut
                            };
                            paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                var data = {
                                    invoice_id: objResponse.invoiceId,
                                    user_id: "1",
                                    user_type: "admin",
                                    reason: "USRCANACCMEET",
                                    transection_type: "PAYREF",
                                    payment_type: "CREDITED",
                                    payment_id: null,
                                    transection_id: null,
                                    capture_id: null,
                                    refund_id: null,
                                    amount: objResponse.wissenxCut,
                                    status: 'completed',
                                    created_date: constants.CURR_UTC_DATETIME()
                                };
                                paypalFunction.insertWallet(req, res, data, function (req, res, walletRes) {

                                    var objParams = {
                                        user_id: "1",
                                        amount: objResponse.wissenxCut
                                    };
                                    paymentModel.updateWalletBalance(req, res, objParams, function (req, res, updBal) {
                                    });
                                });
                            });
                        });
                    });

                    var invitationData = {
                        invitation_id: invitation_id,
                        duration: call_duration,
                        refundAmount: objResponse.amountBackToUser,
                        mentorCharge: objResponse.mentorsCut,
                        wissenxChange: objResponse.wissenxCut
                    };
                    getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                        getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                            sendUserCancelAcceptedInvitationMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                            callback(req, res, "success");
                        });
                    });
                });
            });
        }
    });
};
exports.userCancelRequest_model = userCancelRequest_model;
var sendUserCancelAcceptedInvitationMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });

    var refundAmt = parseFloat(invitationData.refundAmount);
    var mentorAmt = invitationData.mentorCharge;
    var userAmt = parseFloat(invitationData.wissenxChange) + parseFloat(invitationData.mentorCharge);

    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var slotsUserArr = [];
            var slotsMentorArr = [];
            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'acceptedmeetingcancel') {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsUserArr.push(slotsUserObj);
                console.log(slotsMentorObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/usercancelacceptedInvitationMailUser",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", you have cancelled a scheduled meeting  with " + arrMentorData.firstName + " " + arrMentorData.lastName
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });


            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/usercancelacceptedInvitationMailUserchg",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                refundAmt: refundAmt,
                userAmt: userAmt
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ", you have cancelled a scheduled meeting with " + arrMentorData.firstName + " " + arrMentorData.lastName + " Your payments card is being charged as per mentor’s cancellation policy and balance refunded, if any."
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });

            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/usercancelacceptedInvitationMessageUser.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);

            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/usercancelacceptedInvitationMailMentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", " + arrUserData.firstName + " " + arrUserData.lastName + " has cancelled a scheduled meeting with you "
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });


            // Send mail to mentor
            var templateVariable = {
                templateURL: "mailtemplate/usercancelacceptedInvitationMailMentorchg",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_key,
                mentorAmt: mentorAmt
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", " + arrUserData.firstName + " " + arrUserData.lastName + " has cancelled a scheduled meeting with you. Cancellation charges as per your mentor cancellation policy will be credited to your account shortly. "
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });


            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/usercancelacceptedInvitationMessageMentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
        });
    });
};
exports.sendUserCancelAcceptedInvitationMail = sendUserCancelAcceptedInvitationMail;
var checkMentorAnyFutureMeeting = function (req, res, callback) {

    var currentUTC = constants.CURR_UTC_DATETIME();
    var queryParam = [req.session.userId, currentUTC];
    var strQuery = '    SELECT COUNT(*) totalCnt ' +
            '   FROM meeting_invitations mi, meeting_details md ' +
            '   WHERE mi.invitation_id = md.invitation_id ' +
            '   AND IF(mi.invitation_type = 0, user_id, requester_id) = ? ' +
            '   AND mi.status = "accepted" ' +
            '   AND md.scheduled_start_time > ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            var resultSet = results[0];
            if (resultSet.totalCnt > 0) {
                callback(req, res, true);
            } else {
                callback(req, res, false);
            }
        } else {
            callback(req, res, false);
        }
    });
}
exports.checkMentorAnyFutureMeeting = checkMentorAnyFutureMeeting;
var updateAccountSettings = function (req, res, objDataParams, callback) {
    var stopBeingAMentor = objDataParams.stopBeingAMentor;
    var hideProfileSearch = objDataParams.hideProfileSearch;
    var hideProfileCalendar = objDataParams.hideProfileCalendar;
    async.parallel([
        // fucntion call update stop being a mentor
        function (updateUserSettingCallback) {
            if (stopBeingAMentor == 1) {
                var queryParam = ["user", "user", req.session.userId];
                var strQuery = '    UPDATE users u ' +
                        '   SET u.user_type = ?, ' +
                        '   u.temp_user_type = ? ' +
                        '   WHERE u.user_id = ? ';
                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        throw error;
                    } else {
                        updateUserSettingCallback(null, true);
                    }
                });
            } else {
                updateUserSettingCallback(null, true);
            }
        },
        // function call to update other two options.
        function (updateUserSettingCallback) {

            var dbParamHideProfileSearch = 0;
            var dbParamHideProfileCalendar = 0;
            if (hideProfileSearch == 1) {
                dbParamHideProfileSearch = 1;
            }

            if (hideProfileCalendar == 1) {
                dbParamHideProfileCalendar = 1
            }

            var queryParam = [dbParamHideProfileSearch, dbParamHideProfileCalendar, req.session.userId];
            var strQuery = '    UPDATE mentor_details md ' +
                    '   SET md.hide_from_search = ?, ' +
                    '   md.hide_calendar = ? ' +
                    '   WHERE md.user_id = ? ';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                } else {
                    updateUserSettingCallback(null, true);
                }
            });
        }
    ], function (error, resultSet) {
        callback(req, res, true);
    });
}
exports.updateAccountSettings = updateAccountSettings;
var changeUsersAccountTypeStatus = function (req, res, objDataParams, callback) {
    var statusKey = objDataParams.statusKey;
    switch (statusKey) {
        case "M_U":
            req.session.userType = "user";
            req.session.save(function (err) {
                callback(req, res);
            });
            break;
        case "U_M":
            req.session.userType = "mentor";
            req.session.save(function (err) {
                callback(req, res);
            });
            break;
    }
}
exports.changeUsersAccountTypeStatus = changeUsersAccountTypeStatus;
var raiseRefundRequest = function (req, res, callback) {


    getUniqueRefundRequestNumber(req, res, {}, function (req, res, objResponse) {
        var newRandomUnqRefundRequest = objResponse.newRandomUnqRefundRequest;
        var invitation_id = req.body.invitationId;
        var mentor_id = req.body.mentorId;
        var user_id = req.body.userId;
        var meeting_id = req.body.meeting_id;
        var refund_reason = req.body.refund_reason;
        var refund_desc = req.body.refund_desc;
        var duration = req.body.duration;
        var meeting_url_key = req.body.meeting_url_key;
        var params = {
            refund_request_unq_id: newRandomUnqRefundRequest,
            invitation_id: invitation_id,
            meeting_detail_id: meeting_id,
            requester_id: user_id,
            user_id: mentor_id,
            reason: refund_reason,
            reason_description: refund_desc,
            created_date: constants.CURR_UTC_DATETIME()
        };
        var strQuery = 'INSERT INTO refund_request SET ? ';
        dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            } else {
                var refundRequestId = results.insertId;
                var objDataToSendToCreateMessageEntries = {
                    refundRequestId: refundRequestId,
                    unqRefundRequest: newRandomUnqRefundRequest,
                    meetingUserId: user_id,
                    meetingMentorId: mentor_id
                };
                createMessageEntriesForRefundRequest(req, res, objDataToSendToCreateMessageEntries, function (req, res, objResponse) {

                });
                var invitationData = {
                    invitation_id: invitation_id,
                    duration: duration,
                    newRandomUnqRefundRequest: newRandomUnqRefundRequest,
                    refund_reason: refund_reason,
                    refund_desc: refund_desc,
                    meeting_url_key: meeting_url_key
                };
                getUserInformation(req, res, mentor_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                    getUserInformation(req, res, user_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                        sendRefundRequestMail(req, res, arrMentorData, arrUserData, invitationData, 0);
                        callback(req, res, "success");
                    });
                });
            }
        });
    });
};
exports.raiseRefundRequest = raiseRefundRequest;
var createMessageEntriesForRefundRequest = function (req, res, objDataParams, callback) {
    var refundRequestId = objDataParams.refundRequestId;
    var unqRefundRequest = objDataParams.unqRefundRequest;
    var meetingUserId = objDataParams.meetingUserId;
    var meetingMentorId = objDataParams.meetingMentorId;
    var currentUTC = constants.CURR_UTC_DATETIME();
    var data = {
        refund_request_id: refundRequestId,
        refund_request_unq_id: unqRefundRequest,
        created_date: currentUTC
    }

    var queryParam = [data];
    var strQuery = 'INSERT INTO admin_refund_messages_master set ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            var adminRefudMessageMasterId = results.insertId;
            var data = {
                admin_refund_messages_master_id: adminRefudMessageMasterId,
                user_id: meetingUserId,
                typex: "user",
                created_date: currentUTC
            }

            var queryParam = [data];
            var strQuery = 'INSERT INTO admin_refund_messages_thread set ?';
            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                } else {


                    var data = {
                        admin_refund_messages_master_id: adminRefudMessageMasterId,
                        user_id: meetingMentorId,
                        typex: "mentor",
                        created_date: currentUTC
                    }

                    var queryParam = [data];
                    var strQuery = 'INSERT INTO admin_refund_messages_thread set ?';
                    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        } else {
                            callback(req, req, true);
                        }
                    });
                }
            });
        }
    });
}
exports.createMessageEntriesForRefundRequest = createMessageEntriesForRefundRequest;
var sendRefundRequestMail = function (req, res, arrMentorData, arrUserData, invitationData) {
    var data = [];
    data.push({
        invitation_id: invitationData.invitation_id,
        document: ''
    });
    getMentorDeclinedInvitationsData(req, res, data, 0, [], function (req, res, arrFinalData) {
        mentorModel.getMentorDetails(req, res, arrMentorData.user_id, function (req, res, mentorId, arrMentorFullData) {
            var mentorCancellationPolicy = arrMentorFullData.cancellation_policy_type.replace(/\w\S*/g, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            var invitationId = arrFinalData[0].invitation_id;
            var invitationSlots = arrFinalData[0].invitation_slots;
            var requestNumber = invitationData.newRandomUnqRefundRequest;
            var refund_reason = invitationData.refund_reason;
            var refund_desc = invitationData.refund_desc;
            var refundreason = "";

            if (refund_reason == "other") {
                refundreason = refund_desc;
            } else {
                refundreason = refund_reason;
            }

            var currentUTC = constants.CURR_UTC_DATETIME();
            var curtime = "";
            var slotsUserArr = [];
            var slotsMentorArr = [];
            invitationSlots.forEach(function (slot) {
                //if (slot.status == 'acceptedmeetingcancel') {
                var slotsMentorObj = {};
                var slotsUserObj = {};
                slotsMentorObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('YYYY-MM-DD');
                slotsUserObj.date = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('YYYY-MM-DD');
                slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('MMMM DD, YYYY');
                slotsUserObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('MMMM DD, YYYY');
                slotsMentorObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('HH:mm');
                slotsUserObj.startTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('HH:mm');
                slotsMentorObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsUserObj.endTime = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, "minutes").format('HH:mm');
                slotsMentorObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.displayFormat = moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsMentorObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrMentorData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrMentorData.zone_name).format('Z');
                slotsUserObj.timeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + " " + slot.time, "UTC").tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                curtime = moment.tz(currentUTC, 'UTC').tz(arrUserData.zone_name).format('MMMM DD, YYYY') + " " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).format('hh:mm a') + " to " + moment.tz(slot.date + ' ' + slot.time, 'UTC').tz(arrUserData.zone_name).add(invitationData.duration, 'minutes').format('hh:mm a') + ", UTC " + moment.tz(arrUserData.zone_name).format('Z');
                slotsUserArr.push(slotsUserObj);
                slotsMentorArr.push(slotsMentorObj);
                // }
            });
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/userrequestrefundmail",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_url_key,
                requestNumber: requestNumber
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrUserData.email,
                subject: "Hi " + arrUserData.firstName + ",  we have received your refund request. Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttouser");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/userrequestrefundmsg.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsUserArr,
                meeting_url: invitationData.meeting_url_key,
                requestNumber: requestNumber
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrUserData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            // Send mail to user
            var templateVariable = {
                templateURL: "mailtemplate/userrequestrefundmailmentor",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slots: slotsMentorArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_url_key,
                requestNumber: requestNumber,
                refund_reason: refundreason
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: arrMentorData.email,
                subject: "Hi " + arrMentorData.firstName + ", A Refund request has been raised by " + arrUserData.firstName + " " + arrUserData.lastName + " Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("successmailsenttomentor");
                }
            });
            // Send message to user's inbox
            var templateFile = fs.readFileSync('./views/mailtemplate/userrequestrefundmsgmentor.handlebars', 'utf8');
            var context = {
                frontConstant: frontConstant,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                slots: slotsMentorArr,
                meeting_url: invitationData.meeting_url_key,
                requestNumber: requestNumber,
                refund_reason: refundreason
            };
            var template = handlebars.compile(templateFile);
            var html = template(context);
            req.body.lstUserList = arrMentorData.user_id;
            req.body.msgNewBody = html;
            req.body.sender_id = 1;
            req.body.sendNoResponse = 1;
            messagesModel.sendUserMessages(req, res);
            //send mail to admin



            var templateVariable = {
                templateURL: "mailtemplate/userrequestrefundmailadmin",
                frontConstant: frontConstant,
                arrMentorData: arrMentorData,
                arrUserData: arrUserData,
                slotsMentor: slotsMentorArr,
                slotsUser: slotsUserArr,
                mentorCancellationPolicy: mentorCancellationPolicy,
                mentorCancellationPolicyLink: arrMentorFullData.cancellation_policy_link,
                meeting_url: invitationData.meeting_url_key,
                requestNumber: requestNumber,
                refund_reason: refundreason,
                curtime: curtime
            };
            var mailParamsObject = {
                templateVariable: templateVariable,
                to: frontConstant.ADMIN_EMAIL,
                subject: "Hi Wissenx Admin, A Refund request has been raised by " + arrUserData.firstName + " " + arrUserData.lastName + " Refund Request #" + requestNumber
            };
            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    console.log("admin");
                }
            });
        });
    });
};
exports.sendRefundRequestMail = sendRefundRequestMail;
var getUniqueRefundRequestNumber = function (req, res, objDataParams, callback) {
    var randomUnqRefundRequest = "";
    var possible = "0123456789";
    for (var i = 0; i < 6; i++) {
        randomUnqRefundRequest += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    var newRandomUnqRefundRequest = "REF-WISX-" + randomUnqRefundRequest;
    var queryParam = [newRandomUnqRefundRequest];
    var strQuery = ' SELECT COUNT(*) totalCnt ' +
            ' FROM refund_request rq ' +
            ' WHERE rq.refund_request_unq_id = ? ';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var resultSet = results[0];
        if (resultSet.totalCnt > 0) {
            getUniqueRefundRequestNumber(req, res, objDataParams, callback);
        } else {
            callback(req, res, {newRandomUnqRefundRequest: newRandomUnqRefundRequest});
        }
    });
};
exports.getUniqueRefundRequestNumber = getUniqueRefundRequestNumber;

var getCompletedMeetingData = function (req, res, resultSet, currentIndex, arrFinalData, callback) {

    if (resultSet.length > 0) {
        var thisObj = resultSet[0];

        async.parallel([
            function (invitationCallback) {
                var document = thisObj.document;
                if (document != '') {
                    var queryParam = [];
                    var sqlQuery = ' SELECT document_id, document_name, document_real_name, document_size, document_path FROM documents ' +
                            ' WHERE document_id IN (' + document + ') ' +
                            ' AND is_deleted=0';
                    dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                        if (error) {
                            invitationCallback(null, []);
                        } else {
                            miscFunction.checkDocumentExistOnPath(req, res, results, 0, [], function (req, res, finalDocument) {
                                invitationCallback(null, finalDocument);
                            });
                        }
                    });
                } else {
                    invitationCallback(null, []);
                }
            },
            function (invitationCallback) {
                var invitation_id = thisObj.invitation_id;
                var queryParam = [invitation_id];
                var sqlQuery = ' SELECT mis.*, DATE_FORMAT(CONCAT(mis.date, " ", mis.time), "' + constants.MYSQL_DB_DATETIME_FORMAT + '") date_time, mi.duration ' +
                        ' FROM meeting_invitation_slot_mapping mis ' +
                        ' LEFT JOIN meeting_invitations mi ON mi.invitation_id = mis.invitation_id ' +
                        ' WHERE mis.invitation_id= ? and mis.status = "accepted"';
                dbconnect.executeQuery(req, res, sqlQuery, queryParam, function (req, res, error, results, fields) {
                    if (error) {
                        invitationCallback(null, []);
                    } else {
                        invitationCallback(null, results);
                    }
                });
            }
        ], function (err, results) {
            thisObj.invitation_documents = results[0];
            thisObj.invitation_slots = results[1];

            arrFinalData.push(thisObj);
            resultSet.splice(0, 1);
            getMentorDeclinedInvitationsData(req, res, resultSet, currentIndex, arrFinalData, callback);
        });
    } else {
        callback(req, res, arrFinalData);
    }
};
exports.getCompletedMeetingData = getCompletedMeetingData;