var dateFormat = require('dateformat');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var path = require('path');
var fs = require("fs");

var dbconnect = require('./../modules/dbconnect');
var constants = require('./../modules/constants');
var mailer = require('./../modules/mailer');
var sendResponse = require('./../modules/sendresponse');

var frontConstant = require('./../modules/front_constant');
var userModel = require('./../model/users_model');
var miscFunction = require('./../model/misc_model');

var exports = module.exports = {};


var updateUserProfile = function (req, res, callback) {

    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);
    var userId = req.session.userId;
    var txtFirstName = reqBody.txtFirstName;
    var txtLastName = reqBody.txtLastName;
    var txtEmail = reqBody.txtEmail;
    var txtEmailOld = reqBody.txtEmailOld;
    var txtMobileNumberExt = reqBody.txtMobileNumberExt;
    var hidPhoneCounterIso2 = reqBody.hidPhoneCounterIso2;
    var txtMobileNumber = reqBody.txtMobileNumber;
    var txtDateOfBirth = reqBody.txtDateOfBirth;
    var txtAddress = reqBody.txtAddress;
    var txtAddress2 = reqBody.txtAddress2;
    var lstCountry = reqBody.lstCountrytxt;
    var iso2 = reqBody.iso2;
    var txtCityName = reqBody.txtCityName;
    var hidCityId = reqBody.hidCityId;
    var lstTimeZone = reqBody.lstTimeZone;
    var state = reqBody.state;
    var taBreif = reqBody.taBreif;
    var hidOldProfilePic = reqBody.hidOldProfilePic;
    var hidProfileImageChange = reqBody.hidProfileImageChange;
    var txtProfileImageData = reqBody.txtProfileImageData;
    var hidUploadImageType = reqBody.hidUploadImageType;
    var linkedin_link = reqBody.linkedin_link;


    var objParams = {
        country: lstCountry,
        state: state,
        city: txtCityName,
        iso2: iso2
    };

    miscFunction.getLocationIds(req, res, objParams, function (req, res, idRespons) {

        var countryId = idRespons.countryId;
        var stateId = idRespons.stateId;
        var cityId = idRespons.cityId;

        var queryParam = [txtEmail, userId];

        var strQuery = '	SELECT COUNT(*) totalCnt ' +
                '	FROM users u ' +
                ' 	WHERE u.email = ? ' +
                '	AND u.user_id <> ?';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            if (results[0].totalCnt > 0) {
                req.flash('errors', 'Email address already used.');
                res.redirect('/users/profile');
            } else {
                var currentUTC = constants.CURR_UTC_DATETIME();

                var dobFormated = moment(txtDateOfBirth, 'DD/MM/YYYY');

                var queryParam = [dobFormated.format(constants.JS_DB_DATE), txtMobileNumberExt, hidPhoneCounterIso2, txtMobileNumber, currentUTC, userId];

                var strQuery = ' UPDATE users u ' +
                        ' SET u.dob = ?, ' +
                        ' u.phone_country = ?, ' +
                        ' u.phone_country_iso2 = ?, ' +
                        ' u.phone_number = ?, ' +
                        ' u.updated_date = ? ' +
                        ' WHERE u.user_id = ?';

                dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, errorUsers, results, fields) {
                    if (errorUsers) {
                        throw errorUsers;
                    }

                    var queryParamUserDetail = [txtFirstName, txtLastName, txtAddress, txtAddress2, countryId, stateId, cityId, lstTimeZone, taBreif, linkedin_link, userId];

                    var strQueryUserDetail = ' UPDATE user_details u ' +
                            ' SET u.first_name = ?, ' +
                            ' u.last_name = ?, ' +
                            ' u.address_line1 = ?, ' +
                            ' u.address_line2 = ?, ' +
                            ' u.country_id = ?, ' +
                            ' u.state_id = ?, ' +
                            ' u.city_id = ?, ' +
                            ' u.timezone_id = ?, ' +
                            ' u.user_brief = ?, ' +
                            ' u.linkedin_link = ? ' +
                            ' WHERE u.user_id = ?';

                    dbconnect.executeQuery(req, res, strQueryUserDetail, queryParamUserDetail, function (req, res, error, results, fields) {
                        if (error) {
                            throw error;
                        }
                        miscFunction.getTimeZoneList(req, res, lstTimeZone, function (req, res, timeZoneList) {
                            req.session.firstName = txtFirstName;
                            req.session.lastName = txtLastName;
                            req.session.timezone = timeZoneList[0].zoneName;
                            req.session.USERTZBROWSER = moment().tz(timeZoneList[0].zoneName).format('Z');

                            req.flash('messages', 'User profile updated successfully.');
                            res.redirect('/users/profile');
                        });
                    });
                });
            }
        });
    });
}
exports.updateUserProfile = updateUserProfile;


var uploadUserProfileImage = function (req, res, callback) {
    var reqBody = req.body;

    var imageDataFrom = reqBody.imageDataFrom;
    var imageData = reqBody.imageData;

    var userId = req.session.userId;

    var destination = "./uploads/";
    var fileNameToUpload = "userProfilePic" + '-' + userId + '-' + Date.now() + ".jpg";

    var filePathToUpload = destination + fileNameToUpload;

    fs.writeFile(filePathToUpload, imageData, 'base64', function (err) {
        if (err) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "1", "ERRUPLOAD");
        } else {
            var queryParam = [fileNameToUpload, userId];

            var strQuery = '	UPDATE user_details u ' +
                    '	SET u.profile_image = ? ' +
                    '	WHERE u.user_id = ?';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }

                req.session.finalProfileImageUrl = constants.ACCESSURL + 'uploads/' + fileNameToUpload;

                req.session.save(function (err) {
                    sendResponse.sendJsonResponse(req, res, 200, {finalProfileImageUrl: req.session.finalProfileImageUrl}, "0", "SUCUPLOAD");
                });
            });
        }
    });
}
exports.uploadUserProfileImage = uploadUserProfileImage;



var getUserProfileData = function (req, res, userId, callback) {

    if (typeof userId == 'undefined' || isNaN(userId) || userId <= 0) {
        userId = req.session.userId;
    }

    var queryParam = [userId];

    var strQuery = '	SELECT u.email userEmail, u.slug, u.phone_country phoneCountry, u.phone_country_iso2 phoneCountryISO, u.phone_number phoneNumber, u.country_code countryCode, u.user_type userType, u.registered_from registeredFrom, u.slug slug, u.status status, ud.user_detail_id userDetailId, ud.first_name firstName, ud.last_name lastName, ud.profile_image profileImageName, ud.address_line1 address1, ud.address_line2 address2, ud.country_id countryId,ct.wc_mentor_percentage mentorPercentage, ud.state_id stateId, ud.city_id city, ud.zip_code zipCode, ud.timezone_id timeZoneId, ud.user_brief userBreif, CONCAT_WS(", ", c.city_name, s.state_name) cityLabel,c.city_name,ud.linkedin_link linkedInLink, z.zone_name zoneName,ct.country_name,ct.country_code,st.state_name, ' +
            '	DATE_FORMAT(u.dob, "' + constants.MYSQL_DB_DATE + '") DOB, ' +
            '   DATE_FORMAT(CONVERT_TZ(u.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
            '   DATE_FORMAT(CONVERT_TZ(u.updated_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") updatedDate ' +
            '	FROM users u, user_details ud LEFT JOIN cities c INNER JOIN states s ON c.state_id = s.state_id ON c.city_id = ud.city_id  ' +
            '	LEFT JOIN countries ct ON ct.country_id = ud.country_id' +
            '	LEFT JOIN states st ON st.state_id = ud.state_id' +
            '   LEFT JOIN zone z ON ud.timezone_id = z.zone_id ' +
            ' 	WHERE u.user_id = ? AND ud.user_id = u.user_id ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, userId, {});
        } else {

            var image_path = constants.ACCESSURL + "images/noimg.png";

            if (results[0]["profileImageName"] != "") {
                fs.stat("./uploads/" + results[0]["profileImageName"], function (err, stat) {
                    if (err == null) {
                        image_path = constants.ACCESSURL + "uploads/" + results[0]["profileImageName"];
                    }
                    results[0]["fullProfileImageURL"] = image_path;
                    callback(req, res, userId, results[0]);
                });
            } else {
                results[0]["fullProfileImageURL"] = image_path;
                callback(req, res, userId, results[0]);
            }
        }
    });
}
exports.getUserProfileData = getUserProfileData;


var getUserSocialLoginData = function (req, res, userId, callback) {

    var queryParam = [userId];

    var strQuery = '   SELECT sl.social_login_id socialLoginId, sl.user_id userId, sl.google_id googleId, sl.google_name googleName, sl.google_img googleImg, sl.google_email googleEmail, sl.linkedin_id linkedInId, sl.linkedin_user_data linkedInUserData, sl.linkedin_first_name linkedInFirstName, sl.linkedin_last_name linkedInLastName, sl.linkedin_headline linkedInHeadline, sl.linkedin_profile_url linkedInProfileUrl, sl.linkedin_email linkedInEmail ' +
            '   FROM social_login sl ' +
            '   WHERE sl.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length == 0) {
            callback(req, res, userId, {});
        } else {
            callback(req, res, userId, results[0]);
        }
    });
}
exports.getUserSocialLoginData = getUserSocialLoginData;



var getUserSettingsInfo = function (req, res) {
    getMainSettingData(req, res, getSubSettingInfo);
}
exports.getUserSettingsInfo = getUserSettingsInfo;


var getMainSettingData = function (req, res, callback) {

    var userId = req.session.userId;

    var queryParam = [userId];

    var strQuery = '	SELECT s.setting_id settingId, s.setting_name settingName, s.setting_desc settingDescription, s.setting_type settingType, s.relates_to relatedTo, us.user_setting_id userSettingId, ' +
            '   DATE_FORMAT(CONVERT_TZ(s.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
            '   DATE_FORMAT(CONVERT_TZ(s.updated_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") updatedDate ' +
            '	FROM settings s LEFT JOIN user_settings us ON s.setting_id = us.setting_id AND  us.user_id = ?' +
            ' 	WHERE s.relates_to = "0" ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        return callback(req, res, results, "0", [], getSubSettingInfo);
    });
}


var getSubSettingInfo = function (req, res, arrRootSettings, currentIndex, arrFinalData, callback) {
    // If main settings enteries are remaining
    if (arrRootSettings.length > 0) {
        var arrSettingToFetch = arrRootSettings[0];

        var mainSettingId = arrSettingToFetch.settingId;

        arrFinalData.push(arrSettingToFetch);


        var queryParam = [mainSettingId];

        var strQuery = '	SELECT s.setting_id settingId, s.setting_name settingName, s.setting_desc settingDescription, s.setting_type settingType, s.relates_to relatedTo, ' +
                '   DATE_FORMAT(CONVERT_TZ(s.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
                '   DATE_FORMAT(CONVERT_TZ(s.updated_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") updatedDate ' +
                '	FROM settings s ' +
                ' 	WHERE s.relates_to = ? ';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            var newResults = Object.keys(results).map(function (key) {
                return results[key]
            });

            arrFinalData[currentIndex]['subSettings'] = newResults;
            arrRootSettings.splice(0, 1);
            currentIndex++;
            return callback(req, res, arrRootSettings, currentIndex, arrFinalData, getSubSettingInfo);
        });
    }
    // if all the main settings entries sub settings are fetched then return.
    else {
        //getDeleteAccountReason(req, res, arrFinalData, renderSettingPage);
        getDeleteAccountReason(req, res, arrFinalData, getMentorAccountInfo);
    }
}


var getDeleteAccountReason = function (req, res, arrFinalData, callback) {
    var queryParam = [];
    var strQuery = '	SELECT a.account_delete_reason_id accountDeleteReasonId, a.reason reason' +
            '	FROM account_delete_reasons a ' +
            ' 	ORDER BY a.sort_order ASC, a.account_delete_reason_id ASC ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var dataToPass = [];

        dataToPass['settings'] = arrFinalData;
        dataToPass['accountDeleteReason'] = results;

        callback(req, res, dataToPass);
    });
};


var getMentorAccountInfo = function (req, res, objDataParams, callback) {

    var queryParam = [req.session.userId];

    var strQuery = '	SELECT md.hide_from_search hideFromSearch, md.hide_calendar hideFromCalendar ' +
            '	FROM mentor_details md ' +
            ' 	WHERE user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        if (results.length > 0) {
            resultSet = results[0];
            objDataParams['accountSettings'] = resultSet;

            renderSettingPage(req, res, objDataParams);

        } else {
            objDataParams['accountSettings'] = {
                hideFromSearch: 0,
                hideFromCalendar: 0
            };

            renderSettingPage(req, res, objDataParams);
        }
    });
}


var getUserSelectedSettings = function (req, res, arrFinalData, callback) {
    var userId = req.session.userId;

    var queryParam = [userId];

    var strQuery = '	SELECT us.user_setting_id userSettingId, us.user_id userId, us.setting_id settingId, us.user_preference userPreference, ' +
            '   DATE_FORMAT(CONVERT_TZ(us.created_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") createdDate, ' +
            '   DATE_FORMAT(CONVERT_TZ(us.updated_date, "' + constants.UTCTZ + '", "' + req.session.USERTZBROWSER + '"), "' + constants.MYSQL_DB_DATE + '") updateDate ' +
            '	FROM user_settings us ' +
            ' 	WHERE us.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }

        var dataToPass = [];

        dataToPass['settings'] = arrFinalData['settings'];
        dataToPass['accountDeleteReason'] = arrFinalData['accountDeleteReason'];
        dataToPass['userSelectedSetting'] = results;

        callback(req, res, dataToPass);
    });
};
exports.getUserSelectedSettings = getUserSelectedSettings;

var renderSettingPage = function (req, res, dataToPass) {
    var errors = req.flash('errors');
    var messages = req.flash('messages');
    var userId = req.session.userId;
    getUserProfileData(req, res, userId, function (req, res, userId, arrUserData) {

        var renderParams = {
            layout: 'dashboard_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrUserSettingData: dataToPass,
            arrSessionData: req.session,
            arrUserData: arrUserData
        };
        res.render('users/settings', renderParams);
    });
};


var updateUserSettings = function (req, res) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);

    var userId = req.session.userId;
    var settingId = reqBody.settingId;
    var settingValue = reqBody.settingValue;

    // delete setting
    if (settingValue == "0") {
        var queryParam = [userId, settingId];

        var strQuery = '	DELETE FROM user_settings ' +
                ' 	WHERE user_id = ? ' +
                '	AND setting_id = ?';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "SETTRM");
        });
    }
    // insert setting
    else {

        var currentUTC = constants.CURR_UTC_DATETIME();

        var queryParam = {
            user_id: userId,
            setting_id: settingId,
            user_preference: "",
            created_date: currentUTC,
            updated_date: currentUTC
        };
        dbconnect.executeQuery(req, res, 'INSERT INTO user_settings SET ?', queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "SETTINS");
        });
    }
};
exports.updateUserSettings = updateUserSettings;


var checkDuplicateEmail = function (req, res, callback) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);

    var email = reqBody.email;
    var userId = req.session.userId;

    req.checkBody('email', 'Enter valid email.').isEmail();

    var errors = req.validationErrors();

    if (errors) {
        sendResponse.sendJsonResponse(req, res, 200, {}, "1", "INVEMAIL");
    } else {
        var queryParam = [email, userId];
        var strQuery = ' SELECT u.user_id userId, sl.social_login_id socialLoginId ' +
                ' FROM users u LEFT JOIN social_login sl ON u.user_id = sl.user_id' +
                ' WHERE u.email = ? ' +
                ' AND u.user_id <> ? ';

        dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
            if (error) {
                throw error;
            }

            // found user
            if (results.length > 0) {
                sendResponse.sendJsonResponse(req, res, 200, {}, "1", "DUPEMAIL");
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, "0", "EMAILAVL");
            }
        });
    }
};
exports.checkDuplicateEmail = checkDuplicateEmail;


var updateUserPassword = function (req, res, callback) {
    var reqBody = req.body;

    //JSON.stringify(reqBody, null, 4);

    var txtOldPassword = reqBody.txtOldPassword;
    var txtNewPassword = reqBody.txtNewPassword;
    var userId = req.session.userId;


    var queryParam = [userId];
    var strQuery = '	SELECT u.user_id userId, u.email email, u.password password, ud.first_name firstName, ud.last_name lastName ' +
            '	FROM users u, user_details ud ' +
            ' 	WHERE u.user_id = ud.user_id ' +
            '   AND u.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        var passwordFromDB = results[0]['password'];
        var email = results[0]['email'];
        var firstName = results[0]['firstName'];
        var lastName = results[0]['lastName'];

        var fullName = firstName + " " + lastName;

        var flagPasswordCheck = bcrypt.compareSync(txtOldPassword, passwordFromDB);

        if (!flagPasswordCheck) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "1", "INVOLDPASS");
        } else {

            var salt = bcrypt.genSaltSync(10);
            var hash = bcrypt.hashSync(txtNewPassword, salt);

            var currentUTC = constants.CURR_UTC_DATETIME();

            var queryParam = [hash, currentUTC, userId];

            var strQuery = ' UPDATE users u ' +
                    ' SET u.password = ?, ' +
                    ' 	u.updated_date = ? ' +
                    ' WHERE u.user_id = ? ';

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
                if (error) {
                    throw error;
                }

                var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                var operatingSystem = req.useragent.os;
                var browser = req.useragent.browser;
                var ipAddress = ip;

                var formatedDateTime = moment(currentUTC).format('LLLL') + " (UTC)";

                var templateVariable = {
                    templateURL: "mailtemplate/changepassword",
                    userName: fullName,
                    changePasswordTimeStamp: formatedDateTime,
                    operatingSystem: operatingSystem,
                    browser: browser,
                    ipAddress: ipAddress
                };

                var mailParamsObject = {
                    templateVariable: templateVariable,
                    to: email,
                    subject: 'SourceAdvisor Change Password.'
                };

                mailer.sendMail(req, res, mailParamsObject, function (err) {
                    if (err) {
                        sendResponse.sendJsonResponse(req, res, 200, {}, "1", "PASSERRMSG");
                    } else {
                        sendResponse.sendJsonResponse(req, res, 200, {}, "0", "PASSUPD");
                    }
                });
            });
        }
    });
};
exports.updateUserPassword = updateUserPassword;


var deleteUserAccount = function (req, res, callback) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);

    var lstReason = reqBody.lstReason;
    var chkDisclaimer = reqBody.chkDisclaimer;
    var userId = req.session.userId;

    var currentUTC = constants.CURR_UTC_DATETIME();

    var queryParam = [lstReason, "2", currentUTC, userId];

    var strQuery = ' UPDATE users u ' +
            ' 	SET u.delete_reason_id = ?, ' +
            ' 	u.status = ?, ' +
            ' 	u.updated_date = ? ' +
            ' 	WHERE u.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        userModel.getUserInformation(req, res, userId, {}, function (req, res, userId, arrUserInformation, objParameterData) {
            var userName = arrUserInformation.name;
            var userEmail = arrUserInformation.email;

            var templateVariable = {
                templateURL: "mailtemplate/deleteaccount",
                userName: userName
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: userEmail,
                subject: 'SourceAdvisor, Account deletion.'
            };


            var mailerOptions = {
                to: mailParamsObject.to,
                subject: mailParamsObject.subject
            };

            mailer.sendMail(req, res, mailParamsObject, function (err) {
                if (err) {
                    throw err;
                } else {
                    req.session.regenerate(function (err) {
                        req.flash('messages', 'Your account has been deleted.')
                        res.redirect('/login');
                    });
                }
            });
        });

    });
};
exports.deleteUserAccount = deleteUserAccount;


var validateUserPassword = function (req, res, callback) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);

    var password = reqBody.txtAskDeletePassword;
    var userId = req.session.userId;

    var queryParam = [userId];
    var strQuery = '    SELECT u.user_id userId, u.email email, u.password password, ud.first_name firstName, ud.last_name lastName ' +
            '   FROM users u, user_details ud ' +
            '   WHERE u.user_id = ud.user_id ' +
            '   AND u.user_id = ? ';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        }
        var passwordFromDB = results[0]['password'];

        var flagPasswordCheck = bcrypt.compareSync(password, passwordFromDB);

        if (!flagPasswordCheck) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "1", "INVOLDPASS");
        } else {
            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "PASSVERIFY");
        }
    });
};
exports.validateUserPassword = validateUserPassword;

var updateUserSlug = function (req, res, callback) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);

    var slug = reqBody.slug;
    var userId = req.session.userId;

    var queryParam = [slug, userId];
    var strQuery = 'Update users set slug = ? where user_id = ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            throw error;
        } else {
            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "updated");
        }

    });
};
exports.updateUserSlug = updateUserSlug;