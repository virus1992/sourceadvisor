var moment = require('moment');
var momentTz = require('moment-timezone');


//url parameters
const BASE_URL = "http://localhost:3038/";
//const BASE_URL = "https://www.wissenx.com/";
//const BASE_URL = "https://dev.wissenx.com/";

const BASE_URL_ADMIN = "http://localhost:3038/admin/";
//const BASE_URL_ADMIN = "https://www.wissenx.com/admin/";
//const BASE_URL_ADMIN = "https://dev.wissenx.com/admin/";

//site blockpopup credential
const SITEBLOCKPOPUSERNAME = "wissenxU";
const SITEBLOCKPOPPASSWORD = "wissenxP";
// Google api keys.
//const google_map_apikey = "AIzaSyBJ-u9nVcC_VNYmHkR0dcqbgkEzws7GTLo";
const google_map_apikey = "AIzaSyA6R7zRNxJ7T_0aPp5fcPA46Z1Rvi8hu6c";
// datetime parameters
const JS_DATE = 'mm/dd/yyyy';
const JS_TIME = 'H:i:s';
const JS_DATETIME = 'mm/dd/yyyy H:i:s';
const DB_TIME = 'H:i:s';
const DB_DATE = 'Y-m-d';
const DB_DATETIME = 'Y-m-d H:i:s';
const JS_DB_SHORT_DATE = 'YYYY-MM';
const JS_DB_DATE = 'YYYY-MM-DD';
const JS_DB_TIME = 'HH:MM:ss';
const JS_DB_DATETIME = 'yyyy/mm/dd HH:MM:ss';
var CURR_UTC_DATETIME = function () {
    return moment().utc().format("YYYY-MM-DD HH:mm:ss");
};
var CURR_UTC_DATE = function () {
    return moment().utc().format("YYYY-MM-DD");
};
var CURR_UTC_TIME = function () {
    return moment().utc().format("HH:mm:ss");
};
const MYSQL_DB_DATE = '%d/%m/%Y';
const MYSQL_DB_TIME = '%H:%i %p';
const MYSQL_DB_DATETIME = '%m/%d/%Y %h:%i %p';
const MYSQL_DB_DATETIME_FORMAT = '%Y-%m-%d %H:%i:%s';
const CLIENT_DATE = 'MM/DD/YYYY';
// mailing parameters

const MAIL_FROM = 'SourceAdvisor <sourceadvisortest@gmail.com>';
const MAIL_FROM_AUTH = 'sourceadvisortest@gmail.com';
const MAIL_PASSWORD = 'solulab@123';
const MAIL_HOST = 'smtp.gmail.com';
const MAIL_PORT = '587';
const MAIL_METHOD = 'SMTP';
const MAIL_SECURE = false;

// const MAIL_FROM = 'wissenxco@gmail.com';
// const MAIL_FROM_AUTH = 'wissenxco@gmail.com';
// const MAIL_PASSWORD = '!Wissenx6040';
// const MAIL_HOST = 'smtp.gmail.com';
// const MAIL_PORT = '587';
// const MAIL_METHOD = 'SMTP';
// const MAIL_SECURE = false;


//const TOKBOX_API_KEY = '45814932';
//const TOKBOX_SECRET_KEY = '8b0815eb0e07c246bd7b4702dd2bca0ebe6ec472';
// const TOKBOX_API_KEY = '3269082';
// const TOKBOX_SECRET_KEY = 'dddbeb2f395891709a46c45530bac9bac5f8f033';

//const TOKBOX_API_KEY = '45839062';
//const TOKBOX_SECRET_KEY = '7cb2c1e133def7f8760d5a7f702cda14907456f4';
const TOKBOX_API_KEY = '46488512';
const TOKBOX_SECRET_KEY = '2e860956bccece096a1831d2410f07a9375cd6cd';


const STRIPE_SERVER_SECRET_KEY = "sk_test_FBjXXumZacoKFirICPewX50C";
const STRIPE_CURRENCY = "usd";


const TWILIO_ACCOUNT_SID = "123";
const TWILIO_AUTH_TOKEN = "123";
const TWILIO_FROM_NUMBER = "123";


// Default Timezone
const defaultTz = "Asia/Kolkata";
const UTCTZ = '+00:00';

//Paypal Constants
const HOST = "api.sandbox.paypal.com";
const PORT = "";
//const CLIENT_ID = "AdXVavdETum0MNlgid0HvSoveyyGaSwxlW7w87MNFkOHajFKM6fQ12OsYzw1qsj8rmUf5mK5xfoJZB7r";
const CLIENT_ID = "Affl_b1ka6DgcvleqZ1Y4zRa8wDCRzkoFg1zbMO-McfJ7x23IxKk0vFncC3OZUINDvbruvJdRcc1ytVP";
//const CLIENT_ID = "AQBnsvNAVfo-zYG0_H32iA1f3l5OQMsJ7q7XRQTe_dlsR6uoByFFjYSqvTEs_dPM4MZwBPJ2tPpcppER";
const CLIENT_SECRET = "EEwAMi3UE6qJodza-JuBJI8mzWxT1TiLr36Ue0M1SU2TfGTYdKTSdEBjdsfTEZvWDCSkW1qyS-ihOsds";
//const CLIENT_SECRET = "ECstZJ4138YiDWx4JPX6ifpHlwGsOA0TQB6ssGtmAhAsYAqjd1rWfcsbcnSsVDQ9pIIc9DaHEmpP4Qeh";
//const CLIENT_SECRET = "ENHoGfi36s1pgIqHDeWrpEYLokP8KnYObinh6LOeTpFUc51nZvB1kt3Hduyt3n_YTifnnyXcM8izlSYD";
const MODE = 'sandbox'; //sandbox or live
const PAYPAL_CURRENCY = "USD";
const RETURNURL = "https://www.wissenx.com/mentor/paypalIdentityCheck"
//const RETURNURL = "https://dev.wissenx.com/mentor/paypalIdentityCheck"

module.exports = {
    ACCESSURL: BASE_URL,
    ACCESSURL_ADMIN: BASE_URL_ADMIN,
    JS_DATE: JS_DATE,
    JS_TIME: JS_TIME,
    JS_DATETIME: JS_DATETIME,
    DB_DATE: DB_DATE,
    DB_TIME: DB_TIME,
    DB_DATETIME: DB_DATETIME,
    JS_DB_SHORT_DATE: JS_DB_SHORT_DATE,
    JS_DB_DATE: JS_DB_DATE,
    JS_DB_TIME: JS_DB_TIME,
    JS_DB_DATETIME: JS_DB_DATETIME,
    CURR_UTC_DATETIME: CURR_UTC_DATETIME,
    CURR_UTC_DATE: CURR_UTC_DATE,
    CURR_UTC_TIME: CURR_UTC_TIME,
    MYSQL_DB_DATE: MYSQL_DB_DATE,
    MYSQL_DB_TIME: MYSQL_DB_TIME,
    MYSQL_DB_DATETIME: MYSQL_DB_DATETIME,
    MYSQL_DB_DATETIME_FORMAT: MYSQL_DB_DATETIME_FORMAT,
    CLIENT_DATE: CLIENT_DATE,
    TOKBOX_API_KEY: TOKBOX_API_KEY,
    TOKBOX_SECRET_KEY: TOKBOX_SECRET_KEY,
    MAIL_FROM: MAIL_FROM,
    MAIL_FROM_AUTH: MAIL_FROM_AUTH,
    MAIL_PASSWORD: MAIL_PASSWORD,
    MAIL_HOST: MAIL_HOST,
    MAIL_PORT: MAIL_PORT,
    MAIL_METHOD: MAIL_METHOD,
    MAIL_SECURE: MAIL_SECURE,
    UTCTZ: UTCTZ,
    google_map_apikey: google_map_apikey,
    SITEBLOCKPOPUSERNAME: SITEBLOCKPOPUSERNAME,
    SITEBLOCKPOPPASSWORD: SITEBLOCKPOPPASSWORD,
    DEFAULT_TIMEZONE: defaultTz,
    STRIPE_SERVER_SECRET_KEY: STRIPE_SERVER_SECRET_KEY,
    STRIPE_CURRENCY: STRIPE_CURRENCY,
    TWILIO_ACCOUNT_SID: TWILIO_ACCOUNT_SID,
    TWILIO_AUTH_TOKEN: TWILIO_AUTH_TOKEN,
    TWILIO_FROM_NUMBER: TWILIO_FROM_NUMBER,
    HOST: HOST,
    PORT: PORT,
    CLIENT_ID: CLIENT_ID,
    CLIENT_SECRET: CLIENT_SECRET,
    MODE: MODE,
    PAYPAL_CURRENCY: PAYPAL_CURRENCY,
    RETURNURL: RETURNURL
}