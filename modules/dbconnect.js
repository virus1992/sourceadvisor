var mysql = require('mysql');
var mailer = require('./../modules/mailer.js');
var pool;

var dbConnect = function () {
    if (pool) {
        return pool;
    }

    var flagBeta = false;  //DEV
    // var flagBeta = true;   //BETA

    // beta server credentials
    if (flagBeta) {

        // pool = mysql.createPool({
        //     connectionLimit: 100,
        //     waitForConnections: false,
        //     host: 'wissenx-beta.cl36m4qxmrxq.us-west-2.rds.amazonaws.com',
        //     user: 'wissenx',
        //     password: '!Wissenx6040',
        //     database: 'wissenx_beta',
        //     dateStrings: true,
        //     acquireTimeout: 99999999
        // });
        // return pool;


        pool = mysql.createPool({
            connectionLimit: 100,
            waitForConnections: false,
//            host: 'wissenxrds.cl36m4qxmrxq.us-west-2.rds.amazonaws.com',
//            user: 'wissenx',
//            password: '!Wissenx6040',
//            database: 'wissenx_beta',
            host: 'sourceadvisor.cnjcesw2tuv6.us-west-2.rds.amazonaws.com',
            user: 'source_advisor',
            password: 'source_advisor',
            database: 'source_advisor',
            dateStrings: true,
            acquireTimeout: 99999999
        });
        return pool;
    }
    // dev server credentials
    else {

        pool = mysql.createPool({
            connectionLimit: 100,
            waitForConnections: false,
//            host: 'wissenxrds.cl36m4qxmrxq.us-west-2.rds.amazonaws.com',
//            user: 'wissenx',
//            password: '!Wissenx6040',
//            database: 'wissenx',
            host: 'sourceadvisor.cnjcesw2tuv6.us-west-2.rds.amazonaws.com',
            user: 'source_advisor',
            password: 'source_advisor',
            database: 'source_advisor',
            dateStrings: true,
            acquireTimeout: 99999999
        });
        return pool;
    }
};

module.exports.dbConnect = dbConnect;

var executeQuery = function (req, res, strQuery, qryParams, callback) {
    var queryParams = qryParams;
    var strQuery = strQuery;

    if (typeof pool == "undefined") {
        pool = dbConnect();
    }

    pool.getConnection(function (err, connection) {
        if (err) {
            console.log(strQuery);
            console.log(err);

            var templateVariable = {
                templateURL: "mailtemplate/blanktemplate.handlebars",
                blankData: JSON.stringify(err)
            };

            var mailParamsObject = {
                templateVariable: templateVariable,
                to: 'harsh@solulab.com',
                subject: 'MYSQL DB ERROR'
            };
            mailer.sendMail(req, res, mailParamsObject, function (error) {
                if (error) {
                    console.log("DB ERROR MAIL NOT SEND", err);
                } else {
                    console.log("DB ERROR MAIL SEND");
                }
            });
        } else {
            var q = connection.query(strQuery, queryParams, function (error, results, fields) {

                connection.release();
                if (error) {
                    console.log(q.sql);
                    console.log(error);
                    console.log(results);
                    console.log(strQuery);
                }
                callback(req, res, error, results, fields);
            });
        }
    });
};
module.exports.executeQuery = executeQuery;