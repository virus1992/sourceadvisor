var expressMailer = require('express-mailer');
var CryptoJS = require("crypto-js");
var crypto = require('crypto');
var constants = require('./../modules/constants');
var dbconnect = require('./../modules/dbconnect');


var genrateKey = function (req, res, objParams, callback) {

    var user_id = objParams.user_id;

    // generate random passphrase binary data 
    var r_pass = crypto.randomBytes(128);
    r_pass = r_pass.toString("base64");

    var finalres = {};
    var queryParam = {
        enc_key: r_pass,
        user_id: user_id,
        created_date: constants.CURR_UTC_DATETIME()
    };
    var strQuery = 'INSERT INTO enc_key SET ?';

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {

        if (error) {
            callback(req, res, 'error');
        } else {

            finalres = {
                encId: results.insertId,
                encKey: r_pass
            };

            callback(req, res, finalres);
        }
    });
};
exports.genrateKey = genrateKey;

var decData = function (req, res, objParams, callback) {

    var encId = objParams.encId;
    var encData = objParams.encData;
    var queryParam = [encId];
    var strQuery = 'Select enc_key from enc_key where enc_id = ?';
    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results, fields) {
        if (error) {
            callback(req, res, error);
        } else {
            if (results.length > 0) {
                // Decrypt 
                var key = results[0].enc_key;
                var bytes = CryptoJS.AES.decrypt(encData, key);
                var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
                callback(req, res, decryptedData);
            } else {
                callback(req, res, "");
            }
        }
    });
};
exports.decData = decData;