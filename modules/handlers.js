var fs = require('fs');
var sys = require('util');
var exec = require('child_process').exec;

function upload(response, postData) {
    var files = JSON.parse(postData);

    // writing audio file to disk
    _upload(response, files.audio, "aud");

    if (files.uploadOnlyAudio || files.audio.name == files.video.name) {
        response.statusCode = 200;
        response.writeHead(200, {'Content-Type': 'application/json'});
        response.end(files.audio.newName);
    } else if (!files.uploadOnlyAudio) {
        // writing video file to disk
        _upload(response, files.video, "vid");

        merge(response, files);
    }
}
module.exports.upload = upload;

// this function merges wav/webm files

function merge(response, files) {
    // detect the current operating system
    var isWin = !!process.platform.match(/^win/);

    if (isWin) {
        ifWin(response, files);
    } else {
        ifMac(response, files);
    }
}
module.exports.merge = merge;

function ifWin(response, files) {
    // following command tries to merge wav/webm files using ffmpeg
    var merger = __dirname + '\\merger.bat';
    var audioFile = __dirname + '\\uploads\\' + files.audio.name;
    var videoFile = __dirname + '\\uploads\\' + files.video.name;
    var mergedFile = __dirname + '\\uploads\\' + files.audio.name.split('.')[0] + '-merged.webm';

    // if a "directory" has space in its name; below command will fail
    // e.g. "c:\\dir name\\uploads" will fail.
    // it must be like this: "c:\\dir-name\\uploads"
    var command = merger + ', ' + audioFile + " " + videoFile + " " + mergedFile + '';
    exec(command, function (error, stdout, stderr) {
        if (error) {
            console.log(error.stack);
            console.log('Error code: ' + error.code);
            console.log('Signal received: ' + error.signal);
        } else {
            response.statusCode = 200;
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(files.audio.name.split('.')[0] + '-merged.webm');

            fs.unlink(audioFile);
            fs.unlink(videoFile);
        }
    });
}

function ifMac(response, files) {
    // its probably *nix, assume ffmpeg is available
    var audioFile = files.audio;
    var videoFile = files.video;
    var mergedFile = files.audio.split('.')[0] + '-merged.webm';

    var util = require('util'),
            exec = require('child_process').exec;

    var command = "/home/administrator/ffmpeg/ffmpeg -i " + audioFile + " -i " + videoFile + " -map 0:0 -map 1:0 " + mergedFile;

    exec(command, function (error, stdout, stderr) {
        if (stdout)
            console.log(stdout);
        if (stderr)
            console.log(stderr);

        if (error) {
            console.log('exec error: ' + error);
            response.statusCode = 404;
            response.end();

        } else {
            response.statusCode = 200;
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(files.audio.split('.')[0] + '-merged.webm');

            // removing audio/video files
            //fs.unlink(audioFile);
            //fs.unlink(videoFile);
        }

    });
}

module.exports.upload = upload;

function setupPostUploadImage(app) {
    app.post('/admin/upload/image', function (req, res) {
        global.logger.info('post image');

        var form = new formidable.IncomingForm();
        form.parse(req, function (error, fields, files) {
            var file = files["uploaded_image_file"];

            async.series([function (callback) {
                    global.logger.info('copying image ' + file.path + ' to ./public/uploaded_images/');

                    fs.copy(file.path, './public/uploaded_images/' + file.name, function (error) {
                        callback(error);
                    });
                },
                function (callback) {
                    fs.remove(file.path, function (error, removed) {
                        callback(error, removed);
                    });
                }
            ],
                    function (error, results) {
                        if (!error) {
                            res.redirect('/admin/upload/images');
                        } else {
                            global.logger.error('Error uploading image: ' + error);
                            res.redirect('/error?message=12');
                        }
                    });

        });
    });
}