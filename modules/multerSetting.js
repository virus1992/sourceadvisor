var multer = require('multer');
var path = require('path');

var defaultStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
module.exports.defaultStorage = defaultStorage;

var keynoteStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/keynotes/');
    },
    filename: function (req, file, cb) {
        cb(null, 'keynoteVideo_' + req.session.userId + path.extname(file.originalname));
    }
});
module.exports.keynoteStorage = keynoteStorage;

var workfolioStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/workfolio/');
    },
    filename: function (req, file, cb) {
        cb(null, 'workfolio_' + req.session.userId + '-' + Date.now() + path.extname(file.originalname));
    }
});
module.exports.workfolioStorage = workfolioStorage;

var uploadMeetingDoc = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/meetingdocs')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
exports.uploadMeetingDoc = uploadMeetingDoc;

var uploadMessageDoc = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/messagedocs')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
exports.uploadMessageDoc = uploadMessageDoc;

var slotbookingdocs = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/slotbookingdocs')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
exports.slotbookingdocs = slotbookingdocs;