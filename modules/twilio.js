var expressMailer = require('express-mailer');
var twilio = require('twilio');
var constants = require('./../modules/constants');
var miscFunction = require('./../model/misc_model');
var sendResponse = require('./../modules/sendresponse');


var client = twilio(constants.TWILIO_ACCOUNT_SID, constants.TWILIO_AUTH_TOKEN);

var sendMessage = function (req, res, messageParamsObject, callback) {

    var to = messageParamsObject.number;
    var body = messageParamsObject.message;

    client.sendMessage({
          to: to,
          from: constants.TWILIO_FROM_NUMBER,
          body: body
    }, function (err, responseData) {
        if (!err) {
            console.log(responseData.from); // outputs "+14506667788"
            console.log(responseData.body); // outputs "word to your mother."
            messageParamsObject.status = 1;
            miscFunction.createMessageLog(req, res, messageParamsObject, function (req, res, msgResponce) {
                if (msgResponce == "success") {
                    callback(req, res, "sent");
                } else {
                    callback(req, res, "log_not_inserted");
                }
            });

        } else {
            messageParamsObject.status = 0;
            miscFunction.createMessageLog(req, res, messageParamsObject, function (req, res, msgResponce) {
                if (msgResponce == "success") {
                    callback(req, res, "not_sent");
                } else {
                    callback(req, res, "log_not_inserted");
                }
            });
        }
    });
}
exports.sendMessage = sendMessage;