var absPath = "";
$(document).ready(function () {

    absPath = $('#absPath').val();

    /*================================= GLOBAL SEARCH START =================================*/
    $("#txtGlobalKeyWordSearch").tokenInput(absPath + "miscRoutes/getSearchQueryStringData", {
        theme: "facebook",
        minChars: 3,
        queryParam: "term",
        propertyToSearch: "value",
        noResultsText: "No result found.",
        searchingText: "Please wait. We are searching your query.",
        preventDuplicates: true,
        resultsFormatter: function (item) {
            return "<li class='js-tokenInput'>" + item.value + "<span class='js-TokenResultHeader'>" + item.tokenTypeLabel + "</span></li>";
        },
        onAdd: function (item) {
            var id = item.id;
            var tokenTypeLabel = item.tokenTypeLabel;
            var tokenTypeX = item.tokenTypeX;
            var value = item.value;

            var objDataToSend = {};

            switch (tokenTypeX) {
                case "I":
                    objDataToSend.industry = {};
                    objDataToSend.industry.industryId = id;
                    break;

                case "SI":
                    objDataToSend.industry = {};
                    objDataToSend.industry.arrSubIndustries = [id];
                    break;

                case "D":
                    objDataToSend.domain = [id];
                    break;

                case "FN":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorFirstName = id;
                    break;

                case "LN":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorLastName = id;
                    break;
                
                case "FULLNAME":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorFullName = id;
                    break;

                case "ME":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorExpertise = id;
                    break;
            }


            var locationTypeX = $('#hidLocationType').val();
            var hidLocationId = $('#hidLocationId').val();

            if (!isNaN(hidLocationId) && hidLocationId > 0) {
                objDataToSend.location = {};
                objDataToSend.location.locationType = locationTypeX;
                objDataToSend.location.locationId = hidLocationId;
                redirectToListingPage(objDataToSend);
            }

            redirectToListingPage(objDataToSend);
        }
    });


    /*$("#txtGlobalLocationSearch").autocomplete({
     source: absPath + "miscRoutes/getDataForSearchLocation",
     minLength: 2,
     response: function(event, ui) {
     // ui.content is the array that's about to be sent to the response callback.
     if (ui.content.length === 0) {
     $('#hidLocationType').val("");
     $('#hidLocationId').val("");
     }
     },
     select: function( event, ui ) {
     
     var locationId = ui.item.locationId;
     var locationTypeX = ui.item.typeX;
     
     if(ui.item.typeX == "R") {
     locationId = ui.item.locationId;
     }
     
     var objDataToSend = {};
     objDataToSend.location = {};
     objDataToSend.location.locationType = locationTypeX;
     objDataToSend.location.locationId = locationId;
     redirectToListingPage(objDataToSend);
     }
     });*/
    /*================================= GLOBAL SEARCH END   =================================*/










    /*================================= HOME PAGE SEARCH START  =================================*/
    var autoCompleteLocationOptions = {
        url: function(phrase) {
            $('#imgLocationLoader').removeClass('hidden');
            $('#hidLocationSelected').val("N");
            return absPath + "miscRoutes/getDataForSearchLocation?term=" + phrase;
        },
        ajaxSettings: {
            dataType: "json",
            method: "GET",
            data: {
                dataType: "json"
            }
        },
        getValue: function(element) {
            return element.value
        },
        list: {
            onClickEvent: function(element){
                var objClickedEventData = $("#txtLocation").getSelectedItemData();
                selectLocationFromSearchBar(objClickedEventData);
            },
            onChooseEvent: function(element) {
                var objChooseEventData = $("#txtLocation").getSelectedItemData();
                selectLocationFromSearchBar(objChooseEventData);
            },
            onHideListEvent: function() {
                if($('#hidLocationSelected').val() == "N") {
                    $("#txtLocation").val("");
                    $('#hidLocationType').val("");
                    $('#hidLocationId').val("");
                }
                else {
                    $('#hidLocationSelected').val("N");
                }
            },
            onShowListEvent: function() {
                $('#imgLocationLoader').addClass('hidden');
            },
            showAnimation: {
                type: "slide", //normal|slide|fade
                time: 400,
                callback: function() {

                }
            },
            hideAnimation: {
                type: "slide", //normal|slide|fade
                time: 400,
                callback: function() {

                }
            },
            match: {
                enabled: false
            },
            maxNumberOfElements: 10,
        },
        requestDelay: 500,
        placeholder: "Search Experts by Region..."
    };
    $("#txtLocation").easyAutocomplete(autoCompleteLocationOptions);

    /*$("#txtLocation").autocomplete({
        source: absPath + "miscRoutes/getDataForSearchLocation",
        minLength: 2,
        response: function (event, ui) {
            // ui.content is the array that's about to be sent to the response callback.
            if (ui.content.length === 0) {
                $('#hidLocationType').val("");
                $('#hidLocationId').val("");
            }
        },
        select: function (event, ui) {

            var locationId = ui.item.locationId;
            var locationTypeX = ui.item.typeX;

            if (ui.item.typeX == "R") {
                locationId = ui.item.locationId;
            }

            $('#hidLocationType').val(locationTypeX);
            $('#hidLocationId').val(locationId);
        }
    });*/




    var prePopulatedToken = [];
    /*var upperTags = $('.js-upperTagsClose');
     if(upperTags.length > 0) {
     $.each(upperTags, function(index, arrValue){
     if($(arrValue).attr('data-tokenType') != 'LOCATION' && $(arrValue).attr('data-tokenType') != 'TAG') {
     var tmpData = {
     id: $(arrValue).attr('data-tokenId'),
     value: $(arrValue).attr('data-tokenKeyword')
     };
     prePopulatedToken.push(tmpData);
     }
     });
    }*/
    


    var autoCompleteExpertOptions = {
        url: function(phrase) {
            $('#imgKeywordLoader').removeClass('hidden');
            return absPath + "miscRoutes/getSearchQueryStringData?term=" + phrase;
        },
        ajaxSettings: {
            dataType: "json",
            method: "GET",
            data: {
                dataType: "json"
            }
        },
        getValue: function(element) {
            return element.value;
        },
        template: {
            type: "description",
            fields: {
                description: "tokenTypeLabel"
            }
        },
        list: {
            onClickEvent: function(element){
                var objClickedEventData = $("#txtQueryString").getSelectedItemData();
                selectExpertiseFromSearchBar(objClickedEventData);
            },
            onChooseEvent: function() {
                var objClickedEventData = $("#txtQueryString").getSelectedItemData();
                selectExpertiseFromSearchBar(objClickedEventData);
            },
            onHideListEvent: function(){
                $("#txtQueryString").val("");
            },
            onShowListEvent: function(){
                $('#imgKeywordLoader').addClass('hidden');
            },
            showAnimation: {
                type: "slide", //normal|slide|fade
                time: 400,
                callback: function() {

                }
            },
            hideAnimation: {
                type: "slide", //normal|slide|fade
                time: 400,
                callback: function() {

                }
            },
            match: {
                enabled: false
            },
            maxNumberOfElements: 10,
        },
        requestDelay: 500,
        placeholder: "Search Experts by industry, domain or name .."
    };
    $("#txtQueryString").easyAutocomplete(autoCompleteExpertOptions);

    /*$("#txtQueryString").tokenInput(absPath + "miscRoutes/getSearchQueryStringData", {
        theme: "facebook",
        minChars: 3,
        queryParam: "term",
        propertyToSearch: "value",
        noResultsText: "No result found.",
        searchingText: "Please wait. We are searching your query.",
        preventDuplicates: true,
        prePopulate: prePopulatedToken,
        placeholder: "Search Experts by Keyword…",
        resultsFormatter: function (item) {
            return "<li class='js-tokenInput'>" + item.value + "<span class='js-TokenResultHeader'> - " + item.tokenTypeLabel + "</span></li>";
        },
        onAdd: function (item) {

            var id = item.id;
            var tokenTypeLabel = item.tokenTypeLabel;
            var tokenTypeX = item.tokenTypeX;
            var value = item.value;

            var objDataToSend = {};

            switch (tokenTypeX) {
                case "I":
                    objDataToSend.industry = {};
                    objDataToSend.industry.industryId = id;
                    break;

                case "SI":
                    objDataToSend.industry = {};
                    objDataToSend.industry.arrSubIndustries = [id];
                    break;

                case "D":
                    objDataToSend.domain = [id];
                    break;

                case "FN":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorFirstName = id;
                    break;

                case "LN":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorLastName = id;
                    break;

                case "ME":
                    objDataToSend.mentoryProfile = {};
                    objDataToSend.mentoryProfile.mentorExpertise = id;
                    break;
            }


            var locationTypeX = $('#hidLocationType').val();
            var hidLocationId = $('#hidLocationId').val();

            if (!isNaN(hidLocationId) && hidLocationId > 0) {
                objDataToSend.location = {};
                objDataToSend.location.locationType = locationTypeX;
                objDataToSend.location.locationId = hidLocationId;
                redirectToListingPage(objDataToSend);
            }

            redirectToListingPage(objDataToSend);
        }
    });*/

    $("#token-input-txtQueryString").attr("placeholder", "Search Experts by Keyword…");
    $("#token-input-txtGlobalKeyWordSearch").attr("placeholder", "Search Experts by Keyword…");


    $('#txtQueryString').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#frmHomePageSearch').submit();
        }
    });



    $('.js-homePageTagSearch').on('click', function (e) {
        e.preventDefault();
        var tagId = $(this).attr('data-tagId');
        var tagKeyword = $(this).attr('data-tagKeyword');

        var objDataToSend = {};
        objDataToSend.mentoryProfile = {};
        objDataToSend.mentoryProfile.mentorExpertise = tagId;
        redirectToListingPage(objDataToSend);
    });

    $('.js-IndustrySearch').on('click', function (e) {
        e.preventDefault();
        var tokenId = $(this).attr('data-tokenId');
        var tokenKeyword = $(this).attr('data-tokenKeyword');
        var tokenType = $(this).attr('data-tokenType');

        var objDataToSend = {};
        objDataToSend.industry = {};
        objDataToSend.industry.industryId = tokenId;

        redirectToListingPage(objDataToSend);
    });
    /*================================= HOME PAGE SEARCH END  =================================*/

    /*
     // preventing form submit at the time of pressing enter.
     if($('#frmHomePageSearch')) {
     document.querySelector('#frmHomePageSearch').onkeypress = checkEnter;
     }*/


    $('#frmHomePageSearch').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            //element.after(error);
            //element.parent().parent().find("#searchErrorDiv").after(error);
            element.parent().parent().parent().after(error);
        },
        groups: {
            names: "txtLocation txtQueryString"
        },
        onfocusout: false,
        rules: {
            txtLocation: {
                require_from_group: [1, ".search-field"]
            },
            txtQueryString: {
                require_from_group: [1, ".search-field"]
            }
        },
        messages: {
            txtLocation: {
                require_from_group: "Please select atleast location or a single keyword"
            },
            txtQueryString: {
                require_from_group: "Please select atleast location or a single keyword"
            }
        },
        submitHandler: function (form) {
            var locationTypeX = $('#hidLocationType').val();
            var hidLocationId = $('#hidLocationId').val();

            var objDataToSend = {};

            if (!isNaN(hidLocationId) && hidLocationId > 0) {
                objDataToSend.location = {};
                objDataToSend.location.locationType = locationTypeX;
                objDataToSend.location.locationId = hidLocationId;
                redirectToListingPage(objDataToSend);
            }
        }
    });

    $('#btnHomePageSearch').on('click', function () {
        $('#frmGlobalPageSearch').submit();
    });


    $('#frmGlobalPageSearch').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            //element.after(error);
            //element.parent().parent().find("#searchErrorDiv").after(error);
            element.parent().parent().after(error);
        },
        onfocusout: false,
        rules: {
            hidLocationType: true
        },
        messages: {
            hidLocationType: "Please select atleast location or a single keyword"
        },
        submitHandler: function (form) {
            var locationTypeX = $('#hidLocationType').val();
            var hidLocationId = $('#hidLocationId').val();

            var objDataToSend = {};

            if (!isNaN(hidLocationId) && hidLocationId > 0) {
                objDataToSend.location = {};
                objDataToSend.location.locationType = locationTypeX;
                objDataToSend.location.locationId = hidLocationId;
                redirectToListingPage(objDataToSend);
            }
        }
    });




    /*========================================= FOR DELETING UPPER TAGS START ====================================*/
    $(document.body).delegate('.js-upperTagsClose', 'click', function () {

        var objTags = $('.js-upperTagsClose');

        var id = $(this).attr('data-id');
        var faceValue = $(this).attr('data-faceValue');
        var typeX = $(this).attr('data-typeX');

        var thisSelectedId = $(this).attr('data-id');

        if (objTags.length > 0) {

            var hidSearchParamJSON = $('#hidSearchParamJSON').val();
            var SearchParamJSON = JSON.parse(hidSearchParamJSON);

            switch (typeX) {

                case "INDUSTRY":
                    SearchParamJSON.industry = {};
                    break

                case "SUBINDUSTRY":
                    var arrSubIndustries = SearchParamJSON.industry.arrSubIndustries;
                    var indexOfThisId = arrSubIndustries.indexOf(thisSelectedId);
                    var remainingArray = arrSubIndustries;

                    if (!isNaN(indexOfThisId) && indexOfThisId >= 0) {
                        remainingArray = arrSubIndustries.splice(indexOfThisId, 1);
                    }

                    if (remainingArray == thisSelectedId) {
                        SearchParamJSON.industry.arrSubIndustries = arrSubIndustries;
                    }
                    break;

                case "DOMAIN":
                    var arrDomains = SearchParamJSON.domain;
                    var indexOfThisId = arrDomains.indexOf(thisSelectedId);
                    var remainingArray = arrDomains;

                    if (!isNaN(indexOfThisId) && indexOfThisId >= 0) {
                        remainingArray = arrDomains.splice(indexOfThisId, 1);
                    }

                    if (remainingArray == thisSelectedId) {
                        SearchParamJSON.domain = arrDomains;
                    }
                    break;



                case "SIDEBARREGION":
                case "LOCATIONTEXTFIELD":

                    SearchParamJSON.region = {};
                    SearchParamJSON.location = {};
                    break;

                case "SIDEBARSUBREGION":
                case "LOCATIONTEXTFIELD":
                    var arrSubRegionId = SearchParamJSON.region.arrSubRegionId;
                    var indexOfThisId = arrSubRegionId.indexOf(thisSelectedId);
                    var remainingArray = arrSubRegionId;

                    if (!isNaN(indexOfThisId) && indexOfThisId >= 0) {
                        remainingArray = arrSubRegionId.splice(indexOfThisId, 1);
                    }

                    if (remainingArray == thisSelectedId) {
                        SearchParamJSON.region.arrSubRegionId = arrSubRegionId;
                    }
                    SearchParamJSON.location = {};
                    break;

                case "MENTORFIRSTNAME":
                    SearchParamJSON.mentoryProfile.mentorFirstName = "";
                    break;

                case "MENTORLASTNAME":
                    SearchParamJSON.mentoryProfile.mentorLastName = "";
                    break;

                case "MENTORFULLNAME":
                    SearchParamJSON.mentoryProfile.mentorFullName = "";
                    break;

                case "MENTORDESIGNATION":
                    SearchParamJSON.mentoryProfile.mentorDesignation = "";
                    break;

                case "MENTORCOMPANYWORKING":
                    SearchParamJSON.mentoryProfile.mentoryCompany = "";
                    break;

                case "MENTORPRFILECOUNTRY":
                    SearchParamJSON.mentoryProfile.mentorCountryId = "0";
                    break;

                case "MENTORPRFILECITY":
                    SearchParamJSON.mentoryProfile.mentorCityId = "0";
                    break;

                case "MENTOREXPERTISE":
                    SearchParamJSON.mentoryProfile.mentorExpertise = "";
                    break;
            }


            hidSearchParamJSON = JSON.stringify(SearchParamJSON);
            $('#hidSearchParamJSON').val(hidSearchParamJSON);

            initSearchProcess();
        } else {
            displayAjaxNotificationMessage("Please select atleast location or a single keyword", "danger");
        }

    });
    /*========================================= FOR DELETING UPPER TAGS END ====================================*/



    /*=================================== LISTING PAGE SEARCH START  =====================================*/
    $(document.body).delegate('.js-tagSearch', 'click', function (e) {
        e.preventDefault();

        var tokenId = $(this).attr('data-tokenId');
        var tokenKeyword = $(this).attr('data-tokenKeyword');
        var tokenType = $(this).attr('data-tokenType');

        $('#hidPage').val("0");
        $('#txtLocation').val("");
        $('#hidLocationType').val("");
        $('#hidLocationId').val("");
        $('#hidSortByPreference').val("SORTBY_PHL");
        $('#txtQueryString').tokenInput("clear");
        $('#txtQueryString').val(tokenId);

        $('#hidTagSelected').val("");

        initSearchProcess();

    });


    $(document.body).delegate('.js-SortBy', 'click', function (e) {
        e.preventDefault();

        var thisObj = $(this);

        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var tokenId = thisObj.attr('data-tokenId');
        var tokenKeyword = thisObj.attr('data-tokenKeyword');
        var tokenType = thisObj.attr('data-tokenType');

        SearchParamJSON.sortBy = tokenId;

        // no regresh sidebar
        $('#hidRefreshSideBar').val("N");

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        // initiate search process
        initSearchProcess();

    });

    $('#token-input-txtQueryString').on('focus', function () {
        $("#txtQueryString").tokenInput('clear');
    });

    /*=================================== LISTING PAGE SEARCH END  =====================================*/



    /*===============================================================================================================================*/
    /*=================================================== SIDE BAR CLICK ACTION START ===============================================*/
    /*===============================================================================================================================*/
    // if main industry click
    $(document.body).delegate('.js-filterIndustry', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);
        var oldIndustrySelected = "0";

        var thisIndustryId = $(this).attr('data-id');
        var thisIndustryChecked = $(this).prop("checked");

        SearchParamJSON.industry = {};
        if (thisIndustryChecked) {
            SearchParamJSON.industry.industryId = thisIndustryId;
            SearchParamJSON.industry.arrSubIndustries = [];
        }

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);

        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });

    // if main region click
    $(document.body).delegate('.js-filterRegion', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);
        var oldIndustrySelected = "0";

        var thisRegionId = $(this).attr('data-id');

        var thisRegionChecked = $(this).prop("checked");

        SearchParamJSON.region = {};
        if (thisRegionChecked) {
            SearchParamJSON.region.regionId = thisRegionId;
            SearchParamJSON.region.arrSubRegionId = [];
        }

        if (typeof SearchParamJSON.location != 'undefined') {
            SearchParamJSON.location = {};
        }

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);

        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });

    // if domain click
    $(document.body).delegate('.js-filterDomain', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisDomainId = $(this).attr('data-id');

        var tmpArrDoamin = [];
        var objDomain = $('.js-filterDomain');
        if (objDomain.length > 0) {
            $.each(objDomain, function (index, element) {
                var isChecked = $(element).prop('checked');
                if (isChecked) {
                    var id = $(element).attr('data-id');
                    tmpArrDoamin.push(id);
                }
            });
        }

        SearchParamJSON.domain = tmpArrDoamin;
        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });


    // if sub industry click
    $(document.body).delegate('.js-filterSubIndustry', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);
        var oldIndustrySelected = "0";

        var thisSubIndustryId = $(this).attr('data-id');



        if (typeof SearchParamJSON.industry != 'undefined' && !isNaN(SearchParamJSON.industry.industryId) && SearchParamJSON.industry.industryId > 0) {

            var tmpArrSubIndustry = [];
            var objSubIndustry = $('.js-filterSubIndustry');

            if (objSubIndustry.length > 0) {
                $.each(objSubIndustry, function (index, element) {
                    var isChecked = $(element).prop('checked');
                    if (isChecked) {
                        var id = $(element).attr('data-id');
                        tmpArrSubIndustry.push(id);
                    }
                });
            }
            SearchParamJSON.industry.arrSubIndustries = tmpArrSubIndustry;
        } else {
            //SearchParamJSON.industry.arrSubIndustries = [];
            var id = $(this).attr('data-id');

            var tmpArrSubIndustry = [];
            var objSubRegions = $('.js-filterSubIndustry');
            var industryId = $(this).closest('.sub-category').parent().parent().find('.js-filterIndustry').attr('data-id');
            SearchParamJSON.industry = {};
            if (objSubRegions.length > 0) {
                $.each(objSubRegions, function (index, element) {
                    var isChecked = $(element).prop('checked');
                    if (isChecked) {
                        var id = $(element).attr('data-id');
                        tmpArrSubIndustry.push(id);
                    }
                });
            }

            if (typeof SearchParamJSON.location != 'undefined') {
                SearchParamJSON.location = {};
            }

            //SearchParamJSON.industry.industryId = industryId;
            SearchParamJSON.industry.arrSubIndustries = tmpArrSubIndustry;

        }

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);

        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });


    // if sub region click
    $(document.body).delegate('.js-filterSubRegion', 'click', function (e) {

        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);
        var oldIndustrySelected = "0";

        var thisSubRegionId = $(this).attr('data-id');

        if (typeof SearchParamJSON.region != 'undefined' && !isNaN(SearchParamJSON.region.regionId) && SearchParamJSON.region.regionId > 0) {

            var tmpArrSubRegions = [];
            var objSubRegions = $('.js-filterSubRegion');

            if (objSubRegions.length > 0) {
                $.each(objSubRegions, function (index, element) {
                    var isChecked = $(element).prop('checked');
                    if (isChecked) {
                        var id = $(element).attr('data-id');
                        tmpArrSubRegions.push(id);
                    }
                });
            }

            SearchParamJSON.region.arrSubRegionId = tmpArrSubRegions;
        } else {
            var id = $(this).attr('data-id');

            var tmpArrSubRegions = [];
            var objSubRegions = $('.js-filterSubRegion');
            var regionObj = $(this).closest('.sub-category').parent().parent().find('.js-filterRegion');
            var regionId = regionObj.attr('data-id');
            SearchParamJSON.region = {};
            if (objSubRegions.length > 0) {
                $.each(objSubRegions, function (index, element) {
                    var isChecked = $(element).prop('checked');
                    if (isChecked) {
                        var id = $(element).attr('data-id');
                        tmpArrSubRegions.push(id);
                    }
                });
            }

            if ($(regionObj).prop('checked')) {
                SearchParamJSON.region.regionId = regionId;
            }
            SearchParamJSON.region.arrSubRegionId = tmpArrSubRegions;
        }

        if (typeof SearchParamJSON.location != 'undefined') {
            SearchParamJSON.location = {};
        }

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);

        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });

    /*===============================================================================================================================*/
    /*=================================================== SIDE BAR CLICK ACTION END ===============================================*/
    /*===============================================================================================================================*/


    /*=====================================================================================================================================*/
    /*=================================================== MENTOR PROFILE CLICK ACTION START ===============================================*/
    /*=====================================================================================================================================*/

    // if country is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCountrySearch', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileCountryId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorCountryId = thisMentorProfileCountryId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });


    // if city is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCitySearch', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileCityId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorCityId = thisMentorProfileCityId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });



    // if profession is clicked from mentor profile box.
    $(document.body).delegate('.js-tagProfessionSearch', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileProfessionId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorDesignation = thisMentorProfileProfessionId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });


    // if company is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCompanySearch', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileCompanyId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentoryCompany = thisMentorProfileCompanyId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });

    // if job description is clicked from mentor profile box.
    $(document.body).delegate('.js-tagJobDescription', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileCompanyId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorJobDescription = thisMentorProfileCompanyId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });




    // if mentor expertise is clicked from mentor profile box.
    $(document.body).delegate('.js-tagProfileSearch', 'click', function (e) {
        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var SearchParamJSON = JSON.parse(hidSearchParamJSON);

        var thisMentorProfileExpertiseId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorExpertise = thisMentorProfileExpertiseId;

        hidSearchParamJSON = JSON.stringify(SearchParamJSON);
        $('#hidSearchParamJSON').val(hidSearchParamJSON);

        initSearchProcess();
    });


    $(document.body).delegate('.openLoginPopUp', 'click', function (e) {
        e.preventDefault();
        //setTimeZoneForRegisteringUser();
        $('#loginPopUp').modal();
    });

    /*=====================================================================================================================================*/
    /*=================================================== MENTOR PROFILE CLICK ACTION END  ================================================*/
    /*=====================================================================================================================================*/










    /*========================================================================================================================================================*/
    /*=================================================== MENTOR PROFILE CLICK ACTION START FOR DIRECT SEARCH ================================================*/
    /*========================================================================================================================================================*/
    // if country is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCountrySearchDirect', 'click', function (e) {

        var SearchParamJSON = {};

        var thisMentorProfileCountryId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorCountryId = thisMentorProfileCountryId;

        redirectToListingPage(SearchParamJSON);

    });

    // if city is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCitySearchDirect', 'click', function (e) {

        var SearchParamJSON = {};

        var thisMentorProfileCityId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorCityId = thisMentorProfileCityId;

        redirectToListingPage(SearchParamJSON);

    });

    // if Expertise is clicked from mentor profile box.
    $(document.body).delegate('.js-tagProfileSearchDirect', 'click', function (e) {

        var SearchParamJSON = {};

        var thisMentorProfileTagName = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorExpertise = thisMentorProfileTagName;

        redirectToListingPage(SearchParamJSON);

    });

    // if profession is clicked from mentor profile box.
    $(document.body).delegate('.js-tagProfessionSearchDirect', 'click', function (e) {

        var SearchParamJSON = {};

        var thisMentorProfileProfession = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorDesignation = thisMentorProfileProfession;

        redirectToListingPage(SearchParamJSON);

    });

    // if job description is clicked from mentor profile box.
    $(document.body).delegate('.js-tagJobDescriptionDirect', 'click', function (e) {
        var SearchParamJSON = {};
        var thisMentorProfileCompanyId = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentorJobDescription = thisMentorProfileCompanyId;

        redirectToListingPage(SearchParamJSON);
    });


    // if company is clicked from mentor profile box.
    $(document.body).delegate('.js-tagCompanySearchDirect', 'click', function (e) {

        var SearchParamJSON = {};

        var thisMentorProfileCompany = $(this).attr('data-id');

        SearchParamJSON = {};
        SearchParamJSON.mentoryProfile = {};
        SearchParamJSON.mentoryProfile.mentoryCompany = thisMentorProfileCompany;

        redirectToListingPage(SearchParamJSON);

    });

    /*========================================================================================================================================================*/
    /*=================================================== MENTOR PROFILE CLICK ACTION END FOR DIRECT SEARCH ================================================*/
    /*========================================================================================================================================================*/
});



function setPriceFilterAndInitSearch(newMinValue, newMaxValue) {

    var hidSearchParamJSON = $('#hidSearchParamJSON').val();
    var SearchParamJSON = JSON.parse(hidSearchParamJSON);

    $('#hidRefreshSideBar').val('N');

    SearchParamJSON.price = {};
    SearchParamJSON.price.minPrice = newMinValue;
    SearchParamJSON.price.maxPrice = newMaxValue;

    hidSearchParamJSON = JSON.stringify(SearchParamJSON);
    $('#hidSearchParamJSON').val(hidSearchParamJSON);

    initSearchProcess();
}

function selectLocationFromSearchBar(element) {
    var locationId = element.locationId;
    var locationTypeX = element.typeX;

    if(element.typeX != "NR") {
        if (element.typeX == "R") {
            locationId = element.locationId;
        }

        $('#hidLocationType').val(locationTypeX);
        $('#hidLocationId').val(locationId);
        $('#hidLocationSelected').val("Y");
    }
    else {
        $('#txtLocation').val("");
        $('#hidLocationType').val("");
        $('#hidLocationId').val("");
        $('#hidLocationSelected').val("N");
    }
}


function selectExpertiseFromSearchBar(element) {
    var id = element.id;
    var tokenTypeLabel = element.tokenTypeLabel;
    var tokenTypeX = element.tokenTypeX;
    var value = element.value;

    var objDataToSend = {};

    if(tokenTypeX != "NR") {
        switch (tokenTypeX) {
            case "I":
                objDataToSend.industry = {};
                objDataToSend.industry.industryId = id;
                break;

            case "SI":
                objDataToSend.industry = {};
                objDataToSend.industry.arrSubIndustries = [id];
                break;

            case "D":
                objDataToSend.domain = [id];
                break;

            case "FN":
                objDataToSend.mentoryProfile = {};
                objDataToSend.mentoryProfile.mentorFirstName = id;
                break;

            case "LN":
                objDataToSend.mentoryProfile = {};
                objDataToSend.mentoryProfile.mentorLastName = id;
                break;
            
            case "FULLNAME":
                objDataToSend.mentoryProfile = {};
                objDataToSend.mentoryProfile.mentorFullName = id;
                break;

            case "ME":
                objDataToSend.mentoryProfile = {};
                objDataToSend.mentoryProfile.mentorExpertise = id;
                break;
        }

        var locationTypeX = $('#hidLocationType').val();
        var hidLocationId = $('#hidLocationId').val();

        if (!isNaN(hidLocationId) && hidLocationId > 0) {
            objDataToSend.location = {};
            objDataToSend.location.locationType = locationTypeX;
            objDataToSend.location.locationId = hidLocationId;
            redirectToListingPage(objDataToSend);
        }

        redirectToListingPage(objDataToSend);
    }
}



function redirectToListingPage(objForPostParams) {
    var redirectURI = absPath + "mentor/listing";
    $.redirect(redirectURI, objForPostParams, "GET");
}

function checkEnter(e) {
    e = e || event;
    return (e.keyCode || e.which || e.charCode || 0) !== 13;
}



function setSideBarFilterCheckBoxex(dataParamsForSearch) {
    if (typeof dataParamsForSearch.hidLocationType != 'undefined' && dataParamsForSearch.hidLocationType == "R") {
        if (typeof dataParamsForSearch.hidLocationId != 'undefined' && !isNaN(dataParamsForSearch.hidLocationId) && dataParamsForSearch.hidLocationId > 0) {
            var regionId = dataParamsForSearch.hidLocationId;

            var objRegionHeader = $('#regionHeader' + regionId);
            objRegionHeader.prop('checked', true);
        }
    }

    if (typeof dataParamsForSearch.hidLocationType != 'undefined' && dataParamsForSearch.hidLocationType == "C") {
        if (typeof dataParamsForSearch.hidLocationId != 'undefined' && !isNaN(dataParamsForSearch.hidLocationId) && dataParamsForSearch.hidLocationId > 0) {
            var regionId = dataParamsForSearch.hidLocationId;

            var objRegionHeader = $('#js-filterSubRegion' + regionId);
            objRegionHeader.prop('checked', true);
        }
    }


    if (typeof dataParamsForSearch.industryId != 'undefined' && !isNaN(dataParamsForSearch.industryId) && dataParamsForSearch.industryId > 0) {
        var industryId = dataParamsForSearch.industryId;

        var objIndustryHeader = $('#industryHeader' + industryId);
        objIndustryHeader.prop('checked', true);
    }

    if (typeof dataParamsForSearch.subIndustryIds != 'undefined' && dataParamsForSearch.subIndustryIds.length > 0) {
        var subIndustryIds = dataParamsForSearch.subIndustryIds;
        $.each(subIndustryIds, function (index, element) {
            var subIndustryId = element;
            var objIndustryHeader = $('#js-filterSubIndustry' + subIndustryId);
            objIndustryHeader.prop('checked', true);
        });
    }


    if (typeof dataParamsForSearch.domain != 'undefined' && dataParamsForSearch.domain.length > 0) {
        var domain = dataParamsForSearch.domain;
        $.each(domain, function (index, element) {
            var subIndustryId = element;
            var objDomainElement = $('#js-filterDomain' + subIndustryId);
            objDomainElement.prop('checked', true);
        });
    }

    if (typeof dataParamsForSearch.regionId != 'undefined' && !isNaN(dataParamsForSearch.regionId) && dataParamsForSearch.regionId > 0) {
        var regionId = dataParamsForSearch.regionId;

        var objRegionHeader = $('#regionHeader' + regionId);
        objRegionHeader.prop('checked', true);
    }

    if (typeof dataParamsForSearch.subRegion != 'undefined' && dataParamsForSearch.subRegion.length > 0) {
        var subRegion = dataParamsForSearch.subRegion;
        $.each(subRegion, function (index, element) {
            var subRegionId = element;
            var objSubRegionHeader = $('#js-filterSubRegion' + subRegionId);
            objSubRegionHeader.prop('checked', true);
        });
    }
}