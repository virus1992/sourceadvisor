var notificationCloseInterval = "";
var serverNotificationCloseInterval = "";
var cropper = "";
var absPath = "";
var oldImageFromDb = '';
var cropBoxData = "";
var canvasData = "";
var dbConstant = '';

$(document).ready(function () {


    $('.btn-linkedinPopUp').on('click', function () {
        onLinkedInLoadForPopUP();
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    absPath = $('#absPath').val();

    var profileCompletePopupHtml = '<div id="profileCompletePopup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header red">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Complete Profile Reminder</h4>' +
        '</div>' +
        '<div class="modal-body formPopup clearfix">' +
        '<p>Please complete your basic profile before you can sign up as or interact with a advisor:</p>' +
        // '<p>You need to fill in the below details in your Basic Profile before interacting with the Mentor:</p>' +
        '<ul id="pendingProfileItems" class="default"></ul>' +
        '</div>' +
        '<div class="modal-footer">' +
        '<div id="addbtn">' +
        //'<a href="' + absPath + 'users/profile" target="_BLANK" class="btn red-bg btn-reg gotoeditprofile">Go to Edit Profile</a>' +
        '<a href="javascript:;" target="_BLANK" class="btn red-bg btn-reg gotoeditprofile">Go to Edit Profile</a>' +
        '<button class="btn red-bg btn-reg" data-dismiss="modal">Cancel</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $(document.body).prepend(profileCompletePopupHtml);   
    
    $(document.body).on('click','.gotoeditprofile',function(){
        $("#profileCompletePopup").modal('hide');
        window.open( absPath + 'users/profile');
    });

    $.ajax({
        url: absPath + 'getDbConstants',
        type: 'POST',
        success: function (response) {
            dbConstant = response.data;
        }
    });
    oldImageFromDb = $('#imgUserProfile').attr('src');

    $("#flUserProfileImage").change(function () {
        return readURL(this);
    });

    /*================================= Disappering Notification messages start =================================*/
    var objAlertMessage = $('.js-MessageVanish');
    var divMessageClass = objAlertMessage.css('display');

    if (divMessageClass == "block") {
        serverNotificationCloseInterval = setInterval(function () {
            objAlertMessage.css('display', 'none');
            clearInterval(serverNotificationCloseInterval);
        }, 5000);
    }
    /*================================= Disappering Notification messages start =================================*/

    /*======================================CAMERA UPLOADS JS FUCNTIONS START================================*/
    //checking if camera is attached with the system

    if (typeof $('#cameraPreview').val() != 'undefined') {

    }

    // user initiate camera upload image.
    $('#btnCameraUpload').on('click', function () {
        $('#captureImageModal').modal('show');

        Webcam.on('error', function (err) {
            $(".webCamFound").hide();
            $(".webCamNotFound").show();
        });

        Webcam.set({
            // live preview size
            width: 320,
            height: 240,
            // device capture size
            dest_width: 320,
            dest_height: 240,
            // final cropped size
            crop_width: 240,
            crop_height: 240,
            // format and quality
            image_format: 'jpeg',
            jpeg_quality: 100
        });
        Webcam.attach('#cameraPreview');

    });

    // user wants to save camera upload image
    $('#saveImageFromCamera').on('click', function () {
        $('#imgUserProfile').attr('src', cameraCaptureImage);

        var raw_image_data = cameraCaptureImage.replace(/^data\:image\/\w+\;base64\,/, '');
        $('#txtProfileImageData').val(raw_image_data);

        $('#hidUploadImageType').val('CAM');

        //$('#divRemovePhoto').css('display', 'block');

        $('#captureImageModal').modal('hide');

        $('#cameraResult').html("");

        $('#hidProfileImageChange').val('Y');

        Webcam.off('#cameraPreview');
        Webcam.reset();

        uploadUserProfileImage();
    });

    // user cancel camera upload image
    $('#cancelImageFromCamera').on('click', function () {
        closeCameraPopUP();
    });

    // remove uploaded photo
    $('#flRemoveThisUploadedFile').on('click', function () {
        $('#txtProfileImageData').val("");
        $('#hidUploadImageType').val("NUL");
        $('#cameraResult').html("");
        $('#imgUserProfile').attr('src', oldImageFromDb);
        $('#hidProfileImageChange').val('N');
        //$('#divRemovePhoto').css('display', 'none');
    });

    // after closing cropper modal destroying cropper instance.
    $("#imgCropperModal").on('hidden.bs.modal', function () {
        cropper.destroy();
        $('#hidProfileImageChange').val('N');
    });

    $('#captureImageModal').on('hidden.bs.modal', function () {
        closeCameraPopUP();
    });

    // before openingin cropped modal initialize cropper.
    $("#imgCropperModal").on('shown.bs.modal', function () {
        var image = document.getElementById('mainCropperImage');
        cropperInit(image);
    });

    // when user want to save cropped image.
    $('#btnCropAndSave').on('click', function () {
        var croppedCanvas = cropper.getCroppedCanvas();
        var imageData = croppedCanvas.toDataURL('image/jpeg');

        $('#imgUserProfile').attr('src', imageData);

        var raw_image_data = imageData.replace(/^data\:image\/\w+\;base64\,/, '');

        $('#txtProfileImageData').val(raw_image_data);
        $('#hidUploadImageType').val('CROP');
        //$('#divRemovePhoto').css('display', 'block');

        $('#hidProfileImageChange').val('Y');

        $('#imgCropperModal').modal('hide');

        uploadUserProfileImage();
    });
    /*======================================CAMERA UPLOADS JS FUCNTIONS END ================================*/


    /*=================== MAIL WEBSITE POPUP CHECK  START ======================*/
    //checking Wissenx cookies for the mail website popup
    var wissenXCookie = $.cookie("WISSENXCOK");

    // if cookie is not set then show popup.
    if (wissenXCookie != "USRVF") {
        // $('#modalSiteLock').modal('show');
        $('.modal-backdrop').css('opacity', '1.5');
    }

    $('#frmSourceAdvisorMailPopUP').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtSourceAdvisorUserName: {
                required: true,
                noBlankSpace: true
            },
            txtSourceAdvisorPassword: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            txtSourceAdvisorUserName: {
                required: "Please enter username"
            },
            txtSourceAdvisorPassword: {
                required: "Please enter Password"
            }
        },
        submitHandler: function (form) {
            var userName = $('#txtSourceAdvisorUserName').val();
            var password = $('#txtSourceAdvisorPassword').val();

            var dataToSend = {
                userName: userName,
                password: password
            };

            $.ajax({
                url: absPath + 'checkMailWebSitePassword',
                method: "POST",
                data: dataToSend,
                success: function (response) {

                    if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                        displayAjaxNotificationMessage("Some error occured", "danger");
                    }

                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVUSRN") {
                            displayAjaxNotificationMessage("Invalid username.", "danger");
                        } else if (response['flagMsg'] == "INVPASS") {
                            displayAjaxNotificationMessage("Invalid Password.", "danger");
                        }
                        $('#hidEmailAvailable').val("N");
                    } else {
                        if (response['flagMsg'] == "VLUSR") {
                            $.cookie("WISSENXCOK", "USRVF");
                            $('#modalSiteLock').modal('hide');
                        }
                    }
                },
                error: function (response) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                }
            });
        }
    });
    /*=================== MAIL WEBSITE POPUP CHECK  START ======================*/



    /*========================= AFTER LOGIN POPUP ACTION START ========================*/
    var allSystemCookies = $.cookie();
    if ((typeof allSystemCookies.sendMessageToThisMentor !== 'undefined' && allSystemCookies.sendMessageToThisMentor == "1") &&
        (typeof allSystemCookies.doActionForMentor != 'undefined' && $.isNumeric(allSystemCookies.doActionForMentor) && allSystemCookies.doActionForMentor > 0)) {
        var mentorId = allSystemCookies.doActionForMentor;
        $.removeCookie('doActionForMentor', { expires: 7, path: '/' });
        $.removeCookie('sendMessageToThisMentor', { expires: 7, path: '/' });
        if ($('#hidLoggedInUserId').val() == mentorId) {
            displayAjaxNotificationMessage("Cannot send message to own user.", "danger");
        } else {
            sendMessageToThisMentor(mentorId);
        }
    }


    if ((typeof allSystemCookies.bookSlotForThisMentor !== 'undefined' && allSystemCookies.bookSlotForThisMentor == "1") &&
        (typeof allSystemCookies.doActionForMentor != 'undefined' && allSystemCookies.doActionForMentor != "")) {
        var mentorSlug = allSystemCookies.doActionForMentor;
        $.removeCookie('doActionForMentor', { expires: 7, path: '/' });
        $.removeCookie('bookSlotForThisMentor', { expires: 7, path: '/' });
        if ($('#hidLoggedInUserId').val() == mentorId) {
            displayAjaxNotificationMessage("Cannot book slot to own user.", "danger");
        } else {
            bookSlotForThisMentor(mentorSlug);
        }
    }


    /*========================= AFTER LOGIN POPUP ACTION END  ========================*/



    /*===================================== COMMON LOGIN POPUP START ======================================= */
    $('#frmPopUpLogin').validate({
        errorClass: "validate_error",
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtEmailPopUp: {
                required: true,
                noBlankSpace: true,
                email: true
            },
            txtPasswordPopUp: {
                required: true,
                noBlankSpace: true,
                minlength: 6
            }
        },
        messages: {
            txtEmailPopUp: {
                required: "Please enter Email Address",
                noBlankSpace: "Please enter Email Address",
                email: "Please enter Valid Email Address"
            },
            txtPasswordPopUp: {
                required: "Please enter Password",
                noBlankSpace: "Please enter Password",
                minlength: "Password should be minimum 6 characters"
            }
        },
        submitHandler: function (form) {
            var data = {
                email: $('#txtEmailPopUp').val(),
                password: $('#txtPasswordPopUp').val(),
            };
            sendRequestToServerForLoginCheck(data);
        }
    });

    $('#frmLoginSendMessagePopUp').validate({
        errorClass: "validate_error",
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            taMessageToMentorPopUp: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            taMessageToMentorPopUp: {
                required: "Please enter message",
                noBlankSpace: "Please enter message"
            }
        },
        submitHandler: function (form) {
            var hibDoActionAfterLoginForMentor = $('#hibDoActionAfterLoginForMentor').val();
            var data = {
                taMessageToMentorPopUp: $('#taMessageToMentorPopUp').val(),
                lstUserList: hibDoActionAfterLoginForMentor
            };

            var hidSendMessage = $('#hidSendMessage').val();

            if (hidSendMessage == 0) {
                sendMessageFromLoginPopUp(data, reloadSearhPage);
            } else {
                sendMessageFromLoginPopUp(data, showMessageSendNotification);
                $('#hidSendMessage').val(0)
            }
        }
    });

    // when bookmark clicked from mentor listing boxex
    $(document.body).delegate('.openLoginPopUp', 'click', function (e) {
        var doAction = $(this).attr('data-doaction');
        $('#hibDoActionAfterLogin').val(doAction);

        var mentorId = $(this).attr('data-id');
        $('#hibDoActionAfterLoginForMentor').val(mentorId);
    });

    // START When user loggedin and click on mentor side buttons.
    $(document.body).delegate('.bookMarkThisMentor', 'click', function (e) {
        e.preventDefault();
        var flagAlreadyBookMarked = $(this).parent().hasClass('activebanner');
        var mentorId = $(this).attr('data-id');

        if (flagAlreadyBookMarked) {
            // if mentor is already bookmarked, then unbookmark the mentor.
            removeThisBookMarkedMentor(mentorId, changeProfileSideBannerClass);
        } else {
            // if mentor not bookmarked, so booknow the mentor.
            bookmarkThisMentor("0", mentorId, changeProfileSideBannerClass);
        }

    });

    $(document.body).delegate('.likeThisMentor', 'click', function (e) {
        e.preventDefault();
        var flagAlreadyLiked = $(this).parent().hasClass('activebanner');
        var mentorId = $(this).attr('data-id');

        if (flagAlreadyLiked) {
            // if mentor is already bookmarked, then unbookmark the mentor.
            unLikeThisMentor(mentorId, changeProfileSideBannerClass, "L");
        } else {
            // if mentor not bookmarked, so booknow the mentor.
            likeThisMentor("0", mentorId, changeProfileSideBannerClass, "L");
        }
    });

    $(document.body).delegate('.sendMessageToThisMentor', 'click', function (e) {
        e.preventDefault();
        var mentorId = $(this).attr('data-id');
        sendMessageToThisMentor(mentorId);
    });

    $(document.body).delegate('.js-BookSlotForThisMentor', 'click', function (e) {
        var mentorSlug = $(this).attr('data-id');
        bookSlotForThisMentor(mentorSlug);
    });
    // END When user loggedin and click on mentor side buttons.
    /*===================================== COMMON LOGIN POPUP END  ======================================= */

    trimJsTrimData();
});


function sendMessageToThisMentor(mentorId) {
    checkUserBasicProfileComplete(function () {
        $('#hibDoActionAfterLoginForMentor').val(mentorId);
        $('#taMessageToMentorPopUp').val('');
        $('#sendMessageToMentorPopUp').modal('show');
        $('#hidSendMessage').val("1");
    });
}

function bookSlotForThisMentor(mentorSlug) {
    checkUserBasicProfileComplete(function () {
        redirectToBookASlot(mentorSlug);
    });
}


function trimJsTrimData() {
    var objTrimEle = $('.js-trimData');

    if (objTrimEle.length > 0) {
        $.each(objTrimEle, function (index, element) {
            var trimLength = "100";
            var elementTrimLength = $(element).attr('data-trim-length');
            var elementContent = $(element).html();

            if (typeof elementTrimLength != 'undefined' && !$.isNumeric(elementTrimLength) && elementTrimLength <= 0) {
                trimLength = elementTrimLength;
            }

            var newContent = htmlToStringConversion(elementContent, trimLength);

            $(element).html(newContent);
        });
    }
}

function changeProfileSideBannerClass(mentorId, BannerType) {
    if (typeof mentorId == 'undefined' || isNaN(mentorId) || mentorId <= 0) {
        return false;
    }

    if (typeof BannerType == 'undefined' || BannerType == "") {
        BannerType = "B";
    }

    var strParentClass = "bookMarkThisMentor";
    if (BannerType == "L") {
        strParentClass = "likeThisMentor";
    }

    var objAEle = $('a.' + strParentClass + '[data-id=' + mentorId + ']');
    var objAParenetEle = objAEle.parent();

    if (objAParenetEle.hasClass('activebanner')) {
        objAParenetEle.removeClass('activebanner');
    } else {
        objAParenetEle.addClass('activebanner');
    }
}

function sendMessageFromLoginPopUp(data, callback) {
    var dataParams = {
        lstUserList: data.lstUserList,
        msgNewBody: data.taMessageToMentorPopUp
    }
    $.ajax({
        url: absPath + 'messages/sendUserMessages',
        method: "POST",
        data: dataParams,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.message == "error") {
                displayAjaxNotificationMessage("Message not sent", "danger");
            } else {
                if (response.message == "block") {
                    displayAjaxNotificationMessage("You are blocked by this user.", "danger");
                } else if (response.message == "blocked") {
                    displayAjaxNotificationMessage("Unblock this user to send message.", "success");
                } else {
                    displayAjaxNotificationMessage("Message sent successfully", "success");
                }
            }

            $('#sendMessageToMentorPopUp').modal('hide');
            if (typeof callback == 'function') {
                callback(data.lstUserList);
            }
            $.unblockUI();
        },
        error: function (response) {
            displayAjaxNotificationMessagePopUp("Some error occured", "danger");
            $.unblockUI();
            if (typeof callback == 'function') {
                callback(data.lstUserList);
            }
        }
    });
}

function sendRequestToServerForLoginCheck(dataParams) {
    $.ajax({
        url: absPath + 'loginCheckPopUp',
        method: "POST",
        data: dataParams,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessagePopUp("Some error occured", "danger");
            } else {
                if (response['error'] == "1") {
                    if (response['flagMsg'] == "USRNTEXS" || response['flagMsg'] == "ACCNF") {
                        displayAjaxNotificationMessagePopUp("User not exists in the system", "danger");
                    } else if (response['flagMsg'] == "INVP") {
                        displayAjaxNotificationMessagePopUp("Invalid Password", "danger");
                    } else if (response['flagMsg'] == "PLVFYEMAIL") {
                        displayAjaxNotificationMessagePopUp("Please verify your mail", "danger");
                    }
                } else {
                    if (response['flagMsg'] == "LOGGEDIN") {
                        toProcessAfterLoginFlowFromCommonPopUp(response['data'], reloadSearhPage);
                    }
                }
            }
        },
        error: function (response) {
            displayAjaxNotificationMessagePopUp("Some error occured", "danger");
        }
    });
}

function removeThisBookMarkedMentor(mentorId, callback) {
    var dataParams = {
        mentorId: mentorId
    }

    $.ajax({
        url: absPath + 'users/removeBookmarkedMentor',
        method: "post",
        data: dataParams,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some error occured", "danger");
            } else {
                if (response['error'] == "1") {
                    if (response['flagMsg'] == "INVMENTOR") {
                        displayAjaxNotificationMessage("Invalid Mentor selected.", "danger");
                    } else if (response['flagMsg'] == "SERR") {
                        displayAjaxNotificationMessage("Some error occured.", "danger");
                    }
                } else {
                    if (response['flagMsg'] == "MTRBOOKMARRMD") {
                        displayAjaxNotificationMessage("Mentor removed from bookmark", "success");
                    }
                }
            }

            if (typeof callback == 'function') {
                callback(mentorId);
            }
        },
        error: function (response) {
            displayAjaxNotificationMessage("Some error occured", "danger");
            if (typeof callback == 'function') {
                callback(mentorId);
            }
        }
    });
}

function bookmarkThisMentor(loggedInUser, mentorId, callback) {

    var dataParams = {
        mentorId: mentorId
    }

    if (loggedInUser != mentorId) {

        $.ajax({
            url: absPath + 'users/bookmarkThisMentor',
            method: "post",
            data: dataParams,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                $('#loginPopUp').modal('hide');

                if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                } else {
                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVMENTOR") {
                            displayAjaxNotificationMessage("Invalid Mentor selected.", "danger");
                        } else if (response['flagMsg'] == "SERR") {
                            displayAjaxNotificationMessage("Some error occured.", "danger");
                        } else if (response['flagMsg'] == "ALRDYLKD") {
                            displayAjaxNotificationMessage("Mentor Already liked", "danger");
                        }
                    } else {
                        if (response['flagMsg'] == "MTRALRDYBOOKM") {
                            displayAjaxNotificationMessage("Mentor is already bookmarked", "success");
                        } else if (response['flagMsg'] == "MTRBOOKMARKSUCC") {
                            displayAjaxNotificationMessage("Mentor bookmarked successfully.", "success");
                        }
                    }
                }
                if (typeof callback == 'function') {
                    callback(mentorId);
                }
            },
            error: function (response) {
                displayAjaxNotificationMessage("Some error occured", "danger");
                if (typeof callback == 'function') {
                    callback(mentorId);
                }
            }
        });
    } else {
        $('#loginPopUp').modal('hide');
        displayAjaxNotificationMessage("Cannot bookmark yourself", "danger");
        location.reload();
    }
}

function likeThisMentor(loggedInUser, mentorId, callback) {

    var dataParams = {
        mentorId: mentorId
    }

    if (loggedInUser != mentorId) {

        $.ajax({
            url: absPath + 'users/likeThisMentor',
            method: "post",
            data: dataParams,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                $('#loginPopUp').modal('hide');

                if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                } else {
                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVMENTOR") {
                            displayAjaxNotificationMessage("Invalid Mentor selected.", "danger");
                        } else if (response['flagMsg'] == "SERR") {
                            displayAjaxNotificationMessage("Some error occured.", "danger");
                        } else if (response['flagMsg'] == "ALRDYLKD") {
                            displayAjaxNotificationMessage("Mentor Already liked", "danger");
                        }
                    } else {
                        if (response['flagMsg'] == "MTRLKDSUCC") {
                            var likeCount = 0;
                            if (typeof response['data']['likeCount'] != 'undefined' && $.isNumeric(response['data']['likeCount']) && response['data']['likeCount'] > 0) {
                                likeCount = response['data']['likeCount'];
                            }
                            $('.js-mentorLikeCount[data-id="' + mentorId + '"]').html(likeCount);
                            displayAjaxNotificationMessage("Mentor liked successfully", "success");
                        }
                    }
                }
                if (typeof callback == 'function') {
                    callback(mentorId, 'L');
                }
            },
            error: function (response) {
                displayAjaxNotificationMessage("Some error occured", "danger");
                if (typeof callback == 'function') {
                    callback(mentorId, 'L');
                }
            }
        });
    } else {
        $('#loginPopUp').modal('hide');
        displayAjaxNotificationMessage("Cannot like yourself", "danger");
        location.reload();
    }
}

function unLikeThisMentor(mentorId, callback) {
    var dataParams = {
        mentorId: mentorId
    }

    $.ajax({
        url: absPath + 'users/unLikeThisMentor',
        method: "post",
        data: dataParams,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            $('#loginPopUp').modal('hide');

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some error occured", "danger");
            } else {
                if (response['error'] == "1") {
                    if (response['flagMsg'] == "INVMENTOR") {
                        displayAjaxNotificationMessage("Invalid Mentor selected.", "danger");
                    } else if (response['flagMsg'] == "SERR") {
                        displayAjaxNotificationMessage("Some error occured.", "danger");
                    } else if (response['flagMsg'] == "ALRDYLKD") {
                        displayAjaxNotificationMessage("Mentor Already liked", "danger");
                    }
                } else {
                    if (response['flagMsg'] == "MTRUNLKDSUCC") {
                        var likeCount = 0;
                        if (typeof response['data']['likeCount'] != 'undefined' && $.isNumeric(response['data']['likeCount']) && response['data']['likeCount'] > 0) {
                            likeCount = response['data']['likeCount'];
                        }
                        $('.js-mentorLikeCount[data-id="' + mentorId + '"]').html(likeCount);
                        displayAjaxNotificationMessage("Mentor unliked successfully", "success");
                    }
                }
            }
            if (typeof callback == 'function') {
                callback(mentorId, 'L');
            }
        },
        error: function (response) {
            displayAjaxNotificationMessage("Some error occured", "danger");
            if (typeof callback == 'function') {
                callback(mentorId, 'L');
            }
        }
    });
}

function reloadSearhPage() {

    if (typeof $('#hidSearchParamJSON').val() != 'undefined') {
        setTimeout(function () {
            var hidSearchParamJSON = $('#hidSearchParamJSON').val();
            jsonObj = JSON.parse(hidSearchParamJSON);
            redirectToListingPage(jsonObj);
        }, 1000);
    } else {
        location.reload();
    }

}

function showMessageSendNotification() {
    displayAjaxNotificationMessage("Message sent to mentor successfully", "success");
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0)
        return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + sizes[i];
}

function closeCameraPopUP() {
    var imageUploadTypeX = $('#hidUploadImageType').val();
    if (imageUploadTypeX == "NUL") {
        $('#imgUserProfile').attr('src', oldImageFromDb);
    }
    cameraCaptureImage = "";
    $('#cameraResult').html("");
    Webcam.reset();
}

function displayAjaxNotificationMessage(strMessage, msgType) {
    if (msgType === 'undefined') {
        msgType = "S";
    }

    var notificationDivClass = "alert-success";

    switch (msgType) {
        case "success":
            notificationDivClass = "alert-success";
            break;
        case "info":
            notificationDivClass = "alert-info";
            break;
        case "warning":
            notificationDivClass = "alert-warning";
            break;
        case "danger":
        default:
            notificationDivClass = "alert-danger";
            break;
    }

    if ($('#divAjaxNotification')) {
        var divObj = $('#divAjaxNotification');

        divObj.addClass(notificationDivClass);
        divObj.css('display', 'block');

        $('#spnAjaxNotification').html(strMessage);

        // scroll top to the message notification.
        // $('html, body').animate({
        //     scrollTop: ($("#divAjaxNotification").offset().top - 100)
        // }, 800);

        notificationCloseInterval = setInterval(function () {
            hideAjaxNotification(notificationDivClass)
        }, 5000);
    }
}

function hideAjaxNotification(notificationDivClass) {
    if ($('#divAjaxNotification')) {
        var divObj = $('#divAjaxNotification');
        divObj.removeClass(notificationDivClass);
        divObj.css('display', 'none');

        $('#spnAjaxNotification').html("");

        clearInterval(notificationCloseInterval);
    }
}

/*========================================= DISPLAY NOTIFICAITON IN POPUP START =======================================*/
function displayAjaxNotificationMessagePopUp(strMessage, msgType) {
    if (msgType === 'undefined') {
        msgType = "S";
    }

    var notificationDivClass = "alert-success";

    switch (msgType) {
        case "success":
            notificationDivClass = "alert-success";
            break;
        case "info":
            notificationDivClass = "alert-info";
            break;
        case "warning":
            notificationDivClass = "alert-warning";
            break;
        case "danger":
        default:
            notificationDivClass = "alert-danger";
            break;
    }

    if ($('#divAjaxNotificationPopUp')) {
        var divObj = $('#divAjaxNotificationPopUp');

        divObj.addClass(notificationDivClass);
        divObj.css('display', 'block');

        $('#spnAjaxNotificationPopUp').html(strMessage);

        notificationCloseInterval = setInterval(function () {
            hideAjaxNotificationPopUp(notificationDivClass)
        }, 5000);
    }
}

function hideAjaxNotificationPopUp(notificationDivClass) {
    if ($('#divAjaxNotificationPopUp')) {
        var divObj = $('#divAjaxNotificationPopUp');
        divObj.removeClass(notificationDivClass);
        divObj.css('display', 'none');

        $('#spnAjaxNotificationPopUp').html("");

        clearInterval(notificationCloseInterval);
    }
}
/*========================================= DISPLAY NOTIFICAITON IN POPUP END =======================================*/

function cropperInit(image) {
    var width = $('.imageCropperDiv').width();
    var ratio = "1.5";

    width = parseInt(width);

    var height = parseInt(parseFloat(width) / parseFloat(ratio));
    /*cropper = new Cropper(image, {
     aspectRatio: 1 / 1,
     crop: function (e) {
     console.log(e.detail.x);
     console.log(e.detail.y);
     console.log(e.detail.width);
     console.log(e.detail.height);
     console.log(e.detail.rotate);
     console.log(e.detail.scaleX);
     console.log(e.detail.scaleY);
     },
     viewMode: 0,
     responsive: false,
     modal: true,
     guides: true,
     center: true,
     autoCropArea: 0.5,
     minContainerWidth: width,
     minContainerHeight: height,
     minCanvasWidth: width,
     minCanvasHeight: height
     });
     $('.cropper-container').css('width', width + 'px');
     $('.cropper-container').css('height', height + 'px');*/



    cropper = new Cropper(image, {
        aspectRatio: 1 / 1,
        autoCropArea: 0.5,
        viewMode: 0,
        ready: function () {
            // Strict mode: set crop box data first
            cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
        },
        build: function () {

        }/*,
         responsive: true,
         minContainerWidth: width,
         minContainerHeight: height,
         minCanvasWidth: width,
         minCanvasHeight: height*/
    });

    //$('.cropper-container').css('width', width + 'px');
    //$('.cropper-container').css('height', height + 'px');

}

function readURL(input) {

    if (input.files && input.files[0]) {

        var objFile = input.files[0];

        var fileType = objFile.type;

        if (fileType == "image/jpeg" || fileType == "image/png" || fileType == "image/bmp") {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#hidProfileImageChange').val('Y');
                //$('#imgUserProfile').attr('src', e.target.result);

                openCropperForImageUpload(e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
            //$('#divRemovePhoto').css('display', 'block');
            $('#hidUploadImageType').val('UPL');
            return true
        } else {
            $("#imgUserProfile").val("");
            $('#hidProfileImageChange').val('N');
            alert("invalid image format");
        }
        return false;
    }
}

function openCropperForImageUpload(imageData) {
    $('#mainCropperImage').attr('src', imageData);
    $('#imgCropperModal').modal('show');
}

function take_snapshot() {

    var absPath = $('#absPath').val();

    // preload shutter audio clip
    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? absPath + 'audio/shutter.ogg' : absPath + 'audio/shutter.mp3';

    // play sound effect
    shutter.play();

    // take snapshot and get image data
    Webcam.snap(function (data_uri) {
        // display results in page
        cameraCaptureImage = data_uri;
        $('#cameraResult').html('<h2>Here is your image:</h2>' + '<img src="' + data_uri + '"/>');
    });
}

function uploadUserProfileImage() {
    var imageData = $('#txtProfileImageData').val();
    var imageDataFrom = $('#hidUploadImageType').val();

    var dataToSend = {
        imageData: imageData,
        imageDataFrom: imageDataFrom
    };

    $.ajax({
        url: absPath + 'users/updateUserProfilePic',
        method: "POST",
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some error occured", "danger");
            }

            if (response['error'] == "1") {
                if (response['flagMsg'] == "ERRUPLOAD") {
                    displayAjaxNotificationMessage("Error In uploadingFile", "danger");
                }
            } else {
                if (response['flagMsg'] == "SUCUPLOAD") {
                    $('.dropdown.userprofile img').attr('src', response['data'].finalProfileImageUrl)
                    displayAjaxNotificationMessage("User profile picture uploaded successfully.", "success");
                }
            }
        },
        error: function (response) {
            displayAjaxNotificationMessage("Some error occured", "danger");
        }
    });
}

function openNav() {
    document.getElementById("mySidenav").style.right = "0";
}

function closeNav() {
    document.getElementById("mySidenav").style.right = "-250px";
}

function htmlToStringConversion(input, length) {
    var tmp = document.createElement('div');
    tmp.innerHTML = input;
    var content = tmp.textContent || tmp.innerText || "";
    if (typeof length == "undefined" || length == '') {
        return content;
    } else {
        if (content.length < length) {
            return content;
        } else {
            return content.substr(0, length) + '...';
        }
    }
}

/**
 * COMMON LOGIN POPUP FOR COMPLETE WEBSITE START
 */
function setTimeZoneForRegisteringUser() {

    var userIpAddress = $('#userIPAdd').val();
    $.getJSON("//timezoneapi.io/api/" + userIpAddress, function (data) {
        $('#hidTimeZoneLabel').val(data.data.timezone.id);
        $('#hidTimeZoneOffset').val(moment().tz(data.time_zone).utcOffset() / 60);
    });
}

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoadForPopUP() {
    IN.Event.on(IN, "auth", getLinkedInProfileDetailForPopUp);
}

// Use the API call wrapper to request the member's basic profile data
function getLinkedInProfileDetailForPopUp() {
    IN.API.Raw("/people/~").result(onSuccessLinkedInResponse).error(onErrorLinkedInResponse);
}

// Handle the successful return from the API call
function onSuccessLinkedInResponse(data) {

    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };

        userSocialLoginPopUpHandler(dataParam);
        IN.User.logout();

    }).error(function (profileErr) {
        alert('error occured.');
    });

}

// Handle an error response from the API call
function onErrorLinkedInResponse(error) {
    console.log(error);
    alert('error occured.');
}

function userSocialLoginPopUpHandler(dataParams) {

    var absPath = $('#absPath').val();

    var timeZone = $('#hidTimeZoneLabel').val();
    dataParams.timeZone = timeZone;

    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessagePopUp("Some error occured", "danger");
            } else {
                if (response['error'] == "1") {
                    alert('some error occured.');
                } else {
                    if (response['flagMsg'] == "RDTDASH") {
                        toProcessAfterLoginFlowFromCommonPopUp("0", reloadSearhPage);
                    } else if (response['flagMsg'] == "RDTEMAILACT") {
                        window.location = response['data']['redirectLink'];
                    }
                }
            }
        }
    });
}



function toProcessAfterLoginFlowFromCommonPopUp(loggedInUser, callback) {

    var hibDoActionAfterLogin = $('#hibDoActionAfterLogin').val();
    var hibDoActionAfterLoginForMentor = $('#hibDoActionAfterLoginForMentor').val();

    if (typeof hibDoActionAfterLogin != 'undefined' && (typeof hibDoActionAfterLoginForMentor != 'undefined' && hibDoActionAfterLoginForMentor != "")) {
        switch (hibDoActionAfterLogin) {
            case "bookMarkThisMentor":
                bookmarkThisMentor(loggedInUser, hibDoActionAfterLoginForMentor, callback);
                break;
            case "likeThisMentor":
                likeThisMentor(loggedInUser, hibDoActionAfterLoginForMentor, callback);
                break;
            case "sendMessageToThisMentor":
                setCookieForAfterLoginAction("sendMessageToThisMentor", hibDoActionAfterLoginForMentor, callback);
                break;
            case "js-BookSlotForThisMentor":
                setCookieForAfterLoginAction("bookSlotForThisMentor", hibDoActionAfterLoginForMentor, callback);
                break;
        }
    } else {
        alert('Some error occured.');
    }
}

function setCookieForAfterLoginAction(cookieName, mentorId, callback) {
    $.cookie(cookieName, '1', { expires: 7, path: '/' });
    $.cookie('doActionForMentor', mentorId, { expires: 7, path: '/' });
    callback();
}

function redirectToBookASlot(mentorSlug) {
    if ($('#userLogin').val() != $('#mentorId').val()) {
        var URL = absPath + 'users/slot_booking/' + mentorSlug;
        window.location.href = URL;
    } else {
        displayAjaxNotificationMessage("Cannot book your own slot.", "danger");
    }
}
/**
 * COMMON LOGIN POPUP FOR COMPLETE WEBSITE END
 */

function checkUserBasicProfileComplete(callback) {
    $.ajax({
        url: absPath + 'checkUserBasicProfileComplete',
        type: 'POST',
        async: true,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'MISSING') {
                $('#profileCompletePopup #pendingProfileItems li').remove();
                $.each(response.data, function (index, item) {
                    if (item == 'dob') {
                        item = 'Date of Birth';
                    } else if (item == 'phone_number') {
                        item = 'Phone Number';
                    } else if (item == 'address_line1') {
                        item = 'Address Line 1';
                    } else if (item == 'country_id') {
                        item = 'Country';
                    } else if (item == 'city_id') {
                        item = 'City';
                    } else if (item == 'user_brief') {
                        item = 'About';
                    } else if (item == 'first_name') {
                        item = 'First Name';
                    } else if (item == 'last_name') {
                        item = 'Last Name';
                    }
                    $('#profileCompletePopup #pendingProfileItems').append('<li>' + item + '</li>')
                });
                $.unblockUI();
                $('#profileCompletePopup').modal('show');
                return false;
            } else {
                callback();
            }
        }
    });
}

function chkbeamentor() {
    checkUserBasicProfileComplete(function () {
        redirectToMentor();
    });
}

function redirectToMentor() {
    var URL = absPath + 'mentor/become_a_mentor';
    window.location.href = URL;
}


function onGoogleSignInGPApi(googleObj) {
    if (typeof googleObj.access_token != 'undefined') {
        var accessToken = googleObj.access_token;

        var googleDataUrl = 'https://www.googleapis.com/plus/v1/people/me?access_token=' + accessToken;

        $.ajax({
            url: googleDataUrl,
            method: "GET",
            data: "",
            success: function (response) {

                if (typeof response.id != 'undefined' && response.id != "") {

                    var imageUrl = "";
                    var profileId = response.id;
                    var name = response.displayName;
                    if (response.image.isDefault) {
                        imageUrl = "";
                    } else {
                        imageUrl = response.image.url;
                    }
                    var email = response.emails[0].value;

                    var objDataParams = {
                        uniqueId: profileId,
                        loginType: 'gp',
                        name: name,
                        imageUrl: imageUrl,
                        email: email
                    }

                    if (typeof $('#hidUseLoginGoogleSignUp').val() != 'undefined') {
                        userSocialLoginHandle(objDataParams);
                    } else {
                        userSocialLoginPopUpHandler(objDataParams);
                    }
                } else {
                    displayAjaxNotificationMessage("Some Error Ocurred With Google Login", "danger");
                }
            },
            error: function (response) {
                displayAjaxNotificationMessage("Some Error Ocurred With Google Login", "danger");
            }
        });
    } else {
        displayAjaxNotificationMessage("Some Error Ocurred With Google Login", "danger");
    }
}


function userSocialLoginHandle(dataParams) {

    var absPath = $('#absPath').val();

    var timeZone = "";

    if ($('#hidTimeZoneLabel')) {
        timeZone = $('#hidTimeZoneLabel').val();
    }
    dataParams.timeZone = timeZone;

    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "RDTDASH") {
                    window.location = '/';
                } else if (response['flagMsg'] == "RDTEMAILACT") {
                    window.location = response['data']['redirectLink'];
                }
            }
        }
    });
}

function setUserInitialImage(setterParam) {
    /***** Img avatar code  ******/
    // var imgInterval = setInterval(function () {
        if (setterParam.loopClass != "" && $("." + setterParam.loopClass).length > 0) {
            // clearInterval(imgInterval);
            $.each($("." + setterParam.loopClass), function (index, item) {
//                console.log(index);
//                console.log(item);

                //if(setterParam.section == "high_profile" || setterParam.section == "search_mentor_listing") {
                    var img = $(item).find('.' + setterParam.srcClass).attr('src');
                //} else {
                    //var img = $('.' + setterParam.srcClass).attr('src');
                //}
                var chunks = [];
                if (img != "") {
                    chunks = img.split('/');
                    if (chunks[chunks.length - 1] == "noimg.png") {
                        if (setterParam.type == "input") {
                            var name = $("." + setterParam.nameClass).val().split(" ");
                        } else {
                            var name = $(this).find("." + setterParam.nameClass).text().split(" ");
                        }
                        var namef = name[0];
                        var namel = name[name.length - 1];
                        var initial = namef.charAt(0).toUpperCase() + namel.charAt(0).toUpperCase();

                        $(this).find("." + setterParam.nameHtmlClass).html(initial);
                        $(this).find("." + setterParam.nameHtmlClass).css("display", "block");
                        $(this).find("." + setterParam.imgClass).css("display", "none");
                    } else {
                        $(this).find("." + setterParam.nameHtmlClass).css("display", "none");
                        $(this).find("." + setterParam.imgClass).css("display", "block");
                    }
                }
            });
        }
    // }, 10);
    /***** Img avatar code  ******/
}
