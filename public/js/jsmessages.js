function displayNotificationMessage(flagMsg) {
    if (flagMsg === undefined) {
        return false;
    }

    var arrMsgDetails = getFlagMsgDetailsFromArray(flagMsg);

    console.log(arrMsgDetails);
}


function getFlagMsgDetailsFromArray(flagMsg) {

    var arrReturn = [];

    if (flagMsg === undefined) {
        return arrReturn;
    }

    var arrAllApiMsgs = arrAllApiMsgInfo();

    $.each(arrAllApiMsgs, function (index, arrSubMessage) {
        $.each(arrSubMessage, function (subIndex, subValue) {
            if (flagMsg == subIndex) {
                arrReturn = {
                    "message": subValue,
                    "status": index
                };
            }
        });
    });

    return arrReturn;
}


function arrAllApiMsgInfo() {
    /*var arrMessages = [	
     "S"	: 	['ad'],
     "E"	:	['fgf']
     ];*/

    var arrMessages = {
        "S": {
            "USRF": "User Found",
            "USRF2": "User Found2"
        },
        "E": {
            "INVP": "Invalid Parameters",
            "INVP2": "Invalid Parameters2"
        }
    };


    return arrMessages;
}