$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('#frmAdminLogin').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtUserName: {
                required: true,
                noBlankSpace: true
            },
            txtPassword: {
                required: true,
                noBlankSpace: true,
                minlength: 6
            }
        },
        messages: {
            txtUserName: {
                required: "Please enter Email Address",
            },
            txtPassword: {
                required: "Please enter Password",
                minlength: "Password should be minimum 6 characters"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});