var broadCastDataTables = '';
var filterUserStatus = '';
$(document).ready(function () {

    var max = 150;
    $('#msg').keypress(function (e) {
        if (e.which < 0x20) {
            // e.which < 0x20, then it's not a printable character
            // e.which === 0 - Not a character
            return;     // Do nothing
        }
        if (this.value.length == max) {
            e.preventDefault();
        } else if (this.value.length > max) {
            // Maximum exceeded
            this.value = this.value.substring(0, max);
        }
    });

    $("#startDate").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: 'today',
        endDate: '+1y',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function () {
        var temp = $(this).datepicker('getDate');
        var d = new Date(temp);
        d.setDate(d.getDate());
        $('#endDate').datepicker({
            autoclose: true,
            disableEntry: true,
            default: true,
            startDate: d,
            endDate: '+1y',
            format: 'yyyy-mm-dd'
        });
    });
//    $("#endDate").datepicker({
//        autoclose: true,
//        disableEntry: true,
//        default: true,
//        startDate: 'today',
//        endDate: '+1y',
//        format: 'yyyy-mm-dd'
//    });
    $('#filterUserType, #filterUserStatus').on('change', function () {
        broadCastDataTables.draw();
    });
    broadCastDataTables = $('#broadCastDataTables').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "columns": [
            {"width": "50%"},
            null,
            null,
            null
        ],
        "ajax": {
            url: adminUrl + "misc/getBroadCastData",
            type: "POST",
            data: function (d) {
                //d.filterUserStatus = $('#filterUserStatus').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    return row.msg;
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.start_date;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.end_date;
                },
                "targets": 2
            },
            {
                "render": function (data, type, row) {
                    var html = "";
                    html += '<a href="javascript:;" class="editmsg" data-id="' + row.id + '">Edit</a> | <a href="javascript:;" class="delmsg" data-id="' + row.id + '" target="_blank">Delete</a>';
                    return html;
                },
                "targets": 3
            }
        ]
    });
    
    $('#addMsg').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            msg: {
                required: true
            },
            startDate: {
                required: true
            },
            endDate: {
                required: true
            }
        },
        messages: {
            msg: {
                required: 'Please enter Message.'
            },
            startDate: {
                required: 'Please enter Start date.'
            },
            endDate: {
                required: 'Please enter End date.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: adminUrl + "misc/addBroadCastMsg",
                method: "POST",
                data: $('#addMsg').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.data.flagSuccess == true) {
                        $('#addMsg')[0].reset();
                        $('#addMsgModal').modal('toggle');
                        displayAjaxNotificationMessage("Message Added successfully.", "success");
                        broadCastDataTables.draw();
                    } else {
                        $('#addMsg')[0].reset();
                        $('#addMsgModal').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#addMsg')[0].reset();
                    $('#addMsgModal').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });
    
    $(document.body).on('click', '.delmsg', function () {
        var id = $(this).attr('data-id');
        $.ajax({
            url: adminUrl + "misc/delBroadCastMsg",
            method: "POST",
            data: {id: id},
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.data.flagSuccess == true) {
                    displayAjaxNotificationMessage("Message deleted successfully.", "success");
                    broadCastDataTables.draw();
                } else {
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }
            },
            error: function (response) {
                $.unblockUI();
                displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
            }

        });
        return false;
    });

    $(document.body).on('click', '.editmsg', function () {
        var id = $(this).attr('data-id');
        $.ajax({
            url: adminUrl + "misc/getMsgData",
            method: "POST",
            data: {id: id},
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.data != "") {
                    $("#editid").val(response.data[0].id);
                    $("#editmsg").text(response.data[0].msg);
                    $("#editstartDate").val(response.data[0].start_date);
                    $("#editendDate").val(response.data[0].end_date);                                        
                    
                    $("#editstartDate").datepicker({
                        autoclose: true,
                        disableEntry: true,
                        default: true,                        
                        endDate: '+1y',                        
                        format: 'yyyy-mm-dd'
                    }).on('changeDate', function () {
                        var temp = $(this).datepicker('getDate');
                        var d = new Date(temp);
                        d.setDate(d.getDate());
                        $('#editendDate').datepicker({
                            autoclose: true,
                            disableEntry: true,
                            default: true,
                            startDate: d,
                            endDate: '+1y',
                            format: 'yyyy-mm-dd'
                        });
                    });                    
                    $('#editstartDate').datepicker('setDate', response.data[0].start_date);
                    $('#editendDate').datepicker('setDate', response.data[0].end_date);
                    $("#editMsgModal").modal('toggle');
                }
            },
            error: function (response) {
                $.unblockUI();
                displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
            }

        });
        return false;
    });
    
    $('#editMsg').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            editmsg: {
                required: true
            },
            editstartDate: {
                required: true
            },
            editendDate: {
                required: true
            }
        },
        messages: {
            editmsg: {
                required: 'Please enter Message.'
            },
            editstartDate: {
                required: 'Please enter Start date.'
            },
            editendDate: {
                required: 'Please enter End date.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: adminUrl + "misc/editBroadCastMsg",
                method: "POST",
                data: $('#editMsg').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.data == true) {
                        $('#editMsg')[0].reset();
                        $('#editMsgModal').modal('toggle');
                        displayAjaxNotificationMessage("Message update successfully.", "success");
                        broadCastDataTables.draw();
                    } else {
                        $('#editMsg')[0].reset();
                        $('#editMsgModal').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#editMsg')[0].reset();
                    $('#addMsgModal').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });

});