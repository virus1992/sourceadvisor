var cityDataTables = '';

$(document).ready(function(){
    cityDataTables = $('#tblCities').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order":[[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getCityList",
            type: "POST",
            data: function(d) {
                d.countryId = $('#countryId').val();
                d.cityId = $('#cityId').val();
            }
        }
        ,
        "columnDefs":[
            {
                "render": function(data, type, row){
                    return row.cityName;
                },
                "targets":0
            },
            {
                "render": function(data, type, row){

                    var cityId = row.cityId;
                    var status = row.status;

                    if (status == 1) {
                        return '<a href="javaScript:;" class="btnToInactive" data-cityId="' + cityId + '"  title="Click to inactivate"><label class="label listingBtn label-primary">Active</label></a>';
                    } else {
                        return '<a href="javaScript:;" class="btnToactive" data-cityId="' + cityId + '"  title="Click to activate"><label class="label listingBtn label-warning">In-Active</label></a>';
                    }

                },
                "targets":1
            },
            {
                "render": function(data, type, row){
                    var cityId = row.cityId;

                    return '<button type="button" class="btn btn-md btn-primary btnEditState" data-cityId="' + cityId + '">Edit</button>&nbsp;&nbsp;<button type="button" class="btn btn-md btn-primary btnDeleteState" data-cityId="' + cityId + '">Delete</button>';
                },
                "targets":2
            }
        ]
    });



    $(document.body).delegate('.btnDeleteState', 'click', function (e) {
        var cityId = $(this).attr('data-cityId');

        var dataToSend = {
            cityId: cityId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteCity",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    displayAjaxNotificationMessage("City Deleted successfully.", "success");
                    cityDataTables.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditState', 'click', function (e) {
        e.preventDefault();
        var cityId = $(this).attr('data-cityId');

        var dataToSend = {
            cityId: cityId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getCityDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {
                console.log(response);

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    var data = response.data;

                    var cityId = data.city_id;
                    var cityName = data.city_name;


                    $('#txtCityName').val(cityName);

                    $('#modalCityAddEditPopUp .modal-title').html('Edit City');
                    $('#hidActionType').val('update');
                    $('#hidCityId').val(cityId);

                    openCityAddEditModal();
                }
                $.unblockUI();
            }
        });

    });


    $('#btnAddCountry').on('click', function () {
        $('#modalCityAddEditPopUp .modal-title').html('Add City');
        $('#hidActionType').val('add');
        $('#hidCityId').val("0");
        openCityAddEditModal();
    });

    $('#btnCloseStatePopup').on('click', function () {
        $('#modalCityAddEditPopUp').modal('hide');
        //closeCityAddEditModal();
    });

    $('#btnProcessStatePopup').on('click', function () {
        $('#frmCityAddEdit').submit();
    });


    $('#frmCityAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtCityName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtCityName: {
                noBlankSpace: true,
                required: "Please enter city name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addCity",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    } else {
                        if (response.data.flagSuccess) {
                            $('#modalCityAddEditPopUp').modal('hide');
                            cityDataTables.draw();
                            if ($('#hidActionType').val() == 'add') {
                                displayAjaxNotificationMessage("City Added successfully", "success");
                            } else {
                                displayAjaxNotificationMessage("City Information updated successfully", "success");
                            }
                        } else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    $('#modalCityAddEditPopUp').on('hidden.bs.modal', function () {
        closeCityAddEditModal();
    });

    $(document.body).delegate('.btnToInactive', 'click', function (e) {
        e.preventDefault();

        var cityId = $(this).attr('data-cityId');        
        
        var dataToSend = {
            cityId: cityId,
            //statusToChange: '2'
            statusToChange: '0'
        };
        changeCityStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function (e) {
        e.preventDefault();

        var cityId = $(this).attr('data-cityId');
            
        var dataToSend = {
            cityId: cityId,
            statusToChange: '1'
        };
        changeCityStatus(dataToSend);
    });
});


function openCityAddEditModal() {
    //$('#frmCountryAddEdit')[0].reset();
    $('#frmCityAddEdit label.validate_error').css('display', 'none');
    $('#modalCityAddEditPopUp').modal('show');
}
function closeCityAddEditModal() {
    $('#frmCityAddEdit')[0].reset();
    $('#frmCityAddEdit label.validate_error').css('display', 'none');
    $('#modalCityAddEditPopUp').modal('hide');
}

function changeCityStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForCity",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            } else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}