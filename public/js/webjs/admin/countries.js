var dataTableCountries = '';
$(document).ready(function () {

    $('#btnGlobalWissenxCharge').on('click', function () {
        openWissenxChargeModal();
    });

    $('#btnUpdateWissenxCharge').on('click', function () {
        $('#frmGlobalWissenxCharge').submit();
    });

    $('#btnCancelUpdateWissenxCharge').on('click', function () {
        closeWissenxChargeModal();
    });

    $(document.body).delegate('.btnDeleteCountry', 'click', function (e) {
        var countryId = $(this).attr('data-countryId');

        var dataToSend = {
            countryId: countryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteCountry",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    displayAjaxNotificationMessage("Country Deleted successfully.", "success");
                    dataTableCountries.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditCountry', 'click', function (e) {
        e.preventDefault();
        var countryId = $(this).attr('data-countryId');

        var dataToSend = {
            countryId: countryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getCountryDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    var data = response.data;

                    if (data.length > 0) {
                        var countryDetails = data[0];

                        var countryCode = countryDetails.countryCode;
                        var countryId = countryDetails.countryId;
                        var countryName = countryDetails.countryName;
                        var countryPhoneCode = countryDetails.countryPhoneCode;
                        var regionName = countryDetails.regionName;
                        var regionId = countryDetails.regionId;
                        var status = countryDetails.status;
                        var wissenxMentorPercentage = countryDetails.wissenxMentorPercentage;
                        var wissenxUserPercentage = countryDetails.wissenxUserPercentage;
                        var minpayout = countryDetails.minpayout;
                        var maxpayout = countryDetails.maxpayout;
                        var maxpayoutperweek = countryDetails.maxpayoutperweek;


                        $('#txtCountryName').val(countryName);
                        $('#txtCountryCode').val(countryCode);
                        $('#txtPhoneCode').val(countryPhoneCode);
                        $('#lstRegion').val(regionId);
                        $('#txtWissenxUserChargeAddEdit').val(wissenxUserPercentage);
                        $('#txtWissenxMentorChargeAddEdit').val(wissenxMentorPercentage);
                        $('#minpayout').val(minpayout);
                        $('#maxpayout').val(maxpayout);
                        $('#maxpayoutperweek').val(maxpayoutperweek);

                        $('#modalCountryAddEditPopup .modal-title').html('Edit Country');
                        $('#hidActionType').val('update');
                        $('#hidCountryId').val(countryId);
                        openCountryAddEditModal();
                    }
                }
                $.unblockUI();
            }
        });

    });


    $('#btnAddCountry').on('click', function () {
        $('#modalCountryAddEditPopup .modal-title').html('Add Country');
        $('#hidActionType').val('add');
        $('#hidCountryId').val("0");
        openCountryAddEditModal();
    });

    $('#btnCloseCountryPopup').on('click', function () {
        $('#modalCountryAddEditPopup').modal('hide');
        //closeCountryAddEditModal();
    });

    $('#btnProcessCountryPopup').on('click', function () {
        $('#frmCountryAddEdit').submit();
    });


    $('#frmCountryAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtCountryName: {
                noBlankSpace: true,
                required: true
            },
            txtCountryCode: {
                noBlankSpace: true,
                required: true
            },
            txtPhoneCode: {
                noBlankSpace: true,
                required: true,
                number: true
            },
            // lstRegion: {
            //     noBlankSpace: true,
            //     required: true
            // },
            txtWissenxUserChargeAddEdit: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txtWissenxMentorChargeAddEdit: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            minpayout: {
                noBlankSpace: true,
                required: true,
                number: true
            },
            maxpayout: {
                noBlankSpace: true,
                required: true,
                number: true
            },
            maxpayoutperweek: {
                noBlankSpace: true,
                required: true,
                number: true
            }
        },
        messages: {
            txtCountryName: {
                noBlankSpace: true,
                required: "Please enter country name"
            },
            txtCountryCode: {
                noBlankSpace: true,
                required: "Please enter country code"
            },
            txtPhoneCode: {
                noBlankSpace: true,
                required: "Please enter phone code",
                number: "Please enter numeric value"
            },
            // lstRegion: {
            //     noBlankSpace: true,
            //     required: "Please select region"
            // },
            txtWissenxUserChargeAddEdit: {
                noBlankSpace: true,
                required: "Please enter wissenx user charge",
                number: "Please enter numeric wissenx user charge.",
                range: "Please enter number between 0 to 100"
            },
            txtWissenxMentorChargeAddEdit: {
                noBlankSpace: true,
                required: "Please enter wissenx mentor charge",
                number: "Please enter numeric wissenx mentor charge.",
                range: "Please enter number between 0 to 100"
            },
            minpayout: {
                noBlankSpace: true,
                required: "Please enter minimum payout amount",
                number: "Please enter numeric minimum payout amount"
            },
            maxpayout: {
                noBlankSpace: true,
                required: "Please enter maximum payout amount",
                number: "Please enter numeric maximum payout amount"
            },
            maxpayoutperweek: {
                noBlankSpace: true,
                required: "Please enter maximum payout amount per week",
                number: "Please enter numeric maximum payout amount per week"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addCountry",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    } else {
                        if (response.data.flagSuccess) {
                            $('#modalCountryAddEditPopup').modal('hide');
                            dataTableCountries.draw();
                            if ($('#hidActionType').val() == 'add') {
                                displayAjaxNotificationMessage("Country Added successfully", "success");
                            } else {
                                displayAjaxNotificationMessage("Country Information updated successfully", "success");
                            }
                        } else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });





    $('#frmGlobalWissenxCharge').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtWissenxUserCharge: {
                noBlankSpace: true,
                number: true,
                range: [00, 100],
                require_from_group: [1, '.wissenxGlobalClass']
            },
            txtWissenxeMentorCharge: {
                noBlankSpace: true,
                number: true,
                range: [00, 100],
                require_from_group: [1, '.wissenxGlobalClass']
            }
        },
        messages: {
            txtWissenxUserCharge: {
                number: "Please enter numeric wissenx user charge.",
                range: "Please enter number between 0 to 100"
            },
            txtWissenxeMentorCharge: {
                number: "Please enter numeric wissenx user charge.",
                range: "Please enter number between 0 to 100"
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/updateWissenxCharges",
                type: 'POST',
                data: frmData,
                success: function (response) {

                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    } else {
                        displayAjaxNotificationMessage("Global SourceAdvisor charges updated successfully.", "success");
                        closeWissenxChargeModal();
                        dataTableCountries.draw();
                    }
                    $.unblockUI();
                }
            });
        }
    });

    dataTableCountries = $('#tblCountries').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getCountryList",
            type: "POST",
            data: function (d) {
                d.regionId = $('#regionId').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    var countryId = row.countryId;
                    //return row.countryName;
                    return '<a href="' + adminUrl + 'states/' + countryId + '" target="_blank">' + row.countryName + '</a>';
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.countryCode;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.regionName;
                },
                "targets": 2
            },
            {
                "render": function (data, type, row) {
                    return row.countryPhoneCode;
                },
                "targets": 3
            },
            {
                "render": function (data, type, row) {
                    return row.wissenxUserPercentage;
                },
                "targets": 4
            },
            {
                "render": function (data, type, row) {
                    return row.wissenxMentorPercentage;
                },
                "targets": 5
            },
            {
                "render": function (data, type, row) {
                    return row.minpayout;
                },
                "targets": 6
            },
            {
                "render": function (data, type, row) {
                    return row.maxpayout;
                },
                "targets": 7
            },
            {
                "render": function (data, type, row) {
                    return row.maxpayoutperweek;

                },
                "targets": 8
            },
            {
                "render": function (data, type, row) {
                    var status = row.status;
                    var countryId = row.countryId;
                    var regionId = row.regionId;
                    if (status == 1) {
                        return '<a href="javaScript:;" class="btnToInactive" data-regionId="' + regionId + '" data-countryId="' + countryId + '" title="Click to inactivate"><label class="label listingBtn label-primary">Active</label></a>';
                    } else {
                        return '<a href="javaScript:;" class="btnToactive" data-regionId="' + regionId + '" data-countryId="' + countryId + '" title="Click to activate"><label class="label listingBtn label-warning">In-Active</label></a>';
                    }

                },
                "targets": 9
            },
            {
                "render": function (data, type, row) {
                    var countryId = row.countryId;
                    return '<button type="button" class="btn btn-md btn-primary btnEditCountry" data-countryId="' + countryId + '">Edit</button>&nbsp;&nbsp;<button type="button" class="btn btn-md btn-primary btnDeleteCountry" data-countryId="' + countryId + '">Delete</button>';
                    //return row.status;
                },
                "targets": 10
            }
        ]
    });

    $('#modalCountryAddEditPopup').on('hidden.bs.modal', function () {
        closeCountryAddEditModal();
    });

    $(document.body).delegate('.btnToInactive', 'click', function (e) {
        e.preventDefault();

        var countryId = $(this).attr('data-countryId');
        var regionId = $(this).attr('data-regionId');
        var dataToSend = {
            countryId: countryId,
            regionId: regionId,
            //statusToChange: '2'
            statusToChange: '0'
        };
        changeCountryStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function (e) {
        e.preventDefault();

        var countryId = $(this).attr('data-countryId');
        var regionId = $(this).attr('data-regionId');
        var dataToSend = {
            countryId: countryId,
            regionId: regionId,
            statusToChange: '1'
        };
        changeCountryStatus(dataToSend);
    })


});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openCountryAddEditModal() {
    //$('#frmCountryAddEdit')[0].reset();
    $('#frmCountryAddEdit label.validate_error').css('display', 'none');
    $('#modalCountryAddEditPopup').modal('show');
}
function closeCountryAddEditModal() {
    $('#frmCountryAddEdit')[0].reset();
    $('#frmCountryAddEdit label.validate_error').css('display', 'none');
    $('#modalCountryAddEditPopup').modal('hide');
}

function changeCountryStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForCountry",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            } else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}