var dataTableRegion = '';
$(document).ready(function(){

    $(document.body).delegate('.btnDeleteIndustry', 'click', function (e) {
        var industryId = $(this).attr('data-IndustryId');

        var dataToSend = {
            industryId: industryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteIndustry",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    location.reload();
                    // displayAjaxNotificationMessage("Industry Deleted successfully.", "success");
                    // dataTableRegion.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditIndustry', 'click', function(e){
        e.preventDefault();
        var industryId = $(this).attr('data-IndustryId');

        var dataToSend = {
            industryId: industryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getIndustryDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    var data = response.data;

                    var industryId = data.industryId;
                    var industryName = data.industryName;

                    $('#txtIndustrynName').val(industryName);
                    
                    $('#modalIndustryAddEditPopUp .modal-title').html('Edit Industry');
                    $('#hidActionType').val('update');
                    $('#hidIndustryId').val(industryId);
                    openRegionAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function(e){
        e.preventDefault();

        var industryId = $(this).attr('data-IndustryId');
        var dataToSend = {
            industryId: industryId,
            statusToChange: '0'
        };
        changeIndustryStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function(e){
        e.preventDefault();

        var industryId = $(this).attr('data-IndustryId');
        var dataToSend = {
            industryId: industryId,
            statusToChange: '1'
        };
        changeIndustryStatus(dataToSend);
    });


    $('#btnAddRegion').on('click', function(){
        $('#modalIndustryAddEditPopUp .modal-title').html('Add Industry');
        $('#hidActionType').val('add');
        $('#hidIndustryId').val("0");
        openRegionAddEditModal();
    });

    $('#btnCloseIndustryPopup').on('click', function(){
        $('#modalIndustryAddEditPopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessIndustryPopup').on('click', function(){
        $('#frmIndustryAddEdit').submit();
    });


    $('#frmIndustryAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtIndustrynName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtIndustrynName: {
                noBlankSpace: true,
                required: "Please enter Industry name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addIndustry",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.data.flagSuccess) {
                            location.reload();
                            // $('#modalIndustryAddEditPopUp').modal('hide');
                            // dataTableRegion.draw();
                            // if($('#hidActionType').val() == 'add') {
                            //     displayAjaxNotificationMessage("Industry Added successfully", "success");
                            // }
                            // else {
                            //     displayAjaxNotificationMessage("Industry Information updated successfully", "success");
                            // }
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableRegion = $('#tblRegion').DataTable({
        "autoWidth": false,
        "columnDefs" : [
            
            {   "width": "10%", "targets": 1 },
            {   "width": "15%", "targets": 2 }
        ]
    });

    $('#modalIndustryAddEditPopUp').on('hidden.bs.modal', function(){
        closeRegionAddEditModal();
    });
});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openRegionAddEditModal(){
    //$('#frmIndustryAddEdit')[0].reset();
    $('#frmIndustryAddEdit label.validate_error').css('display', 'none');
    $('#modalIndustryAddEditPopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmIndustryAddEdit')[0].reset();
    $('#frmIndustryAddEdit label.validate_error').css('display', 'none');
    $('#modalIndustryAddEditPopUp').modal('hide');
}

function changeIndustryStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForIndustry",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            }
            else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}