var mentorPayoutDataTables = '';
var filterUserStatus = '';
var amount = 0;
var walBal = 0;
var chk = 0;

$(document).ready(function () {

    $(document.body).on('click', '.mentorPayoutModal', function () {        
        walBal = parseFloat($(this).attr('data-walBal')).toFixed(2);
        var userId = $(this).attr('data-userId');        
        var id = $(this).attr('data-recordId');        
        var paypalemail = $(this).attr('data-paypalemail');
        var email = $(this).attr('data-email');
        var paypalphone = $(this).attr('data-paypalphone');
        var reqamt = $(this).attr('data-reqamt');
        var mentorName = $(this).attr('data-mentorName');
        $("#id").val(id);        
        $("#user_id").val(userId);        
        $("#mentorEmail").val(email);
        $("#mentorpaypalEmail").val(paypalemail);
        $("#payoutAmount").val(reqamt);        
        $("#mentorPhn").val(paypalphone);        
        $("#walBal").val(walBal);
        $("#mentorName").val(mentorName);
        var msg = "";
        if ($(this).attr('data-walBal') != "null") {
            if ($(this).attr('data-reqamt') != "null") {
                if ((reqamt * 1) >= (walBal * 1)) {                    
                    msg = "Maximum Payout for this mentor is " + walBal;                    
                    $("#updatebtn").attr('disabled', true);
                } else {
                    msg = "Maximum Payout for this mentor is " + walBal;
                    $("#updatebtn").attr('disabled', false);
                }
            }
        } else {
            msg = "This mentor does not have wallet balance for payout";
            $("#updatebtn").attr('disabled', true);
        }
        $("#maxMsg").text(msg);
    });

    $(document.body).on("keyup", "#payoutAmount", function () {
        var curVal = parseFloat($(this).val()).toFixed(2);
        var comp = parseFloat($("#walBal").val()).toFixed(2);
        
        if ((curVal * 1) <= comp) {
            $("#updatebtn").attr('disabled', false);
        } else {
            $("#updatebtn").attr('disabled', true);
        }

    });

    $("#payoutAmount").bind('paste', function (event) {
        var _this = this;
        // Short pause to wait for paste to complete
        setTimeout(function () {
            var curVal = $(_this).val();
            var comp = parseFloat($("#walBal").val()).toFixed(2);            

            if ((curVal * 1) <= comp) {
                $("#updatebtn").attr('disabled', false);
            } else {
                $("#updatebtn").attr('disabled', true);
            }

        }, 100);
    });

    $("#payoutAmount").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

    $('#filterUserType, #filterUserStatus').on('change', function () {
        mentorPayoutDataTables.draw();
    });

    mentorPayoutDataTables = $('#mentorPayoutTable').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getMentorList",
            type: "POST",
            data: function (d) {
                d.filterUserStatus = $('#filterUserStatus').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    return '<a href="' + $('#absPath').val() + 'mentorprofile/' + row.slug + '"  target="_blank">' + row.fullName + '</a>';
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.email;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.phone_number;
                },
                "targets": 2
            },
//            {
//                "render": function (data, type, row) {
//                    return row.status;
//                },
//                "targets": 3
//            },
            {
                "render": function (data, type, row) {
                    return row.wallet_balance;
                },
                "targets": 3
            },
             {
                "render": function (data, type, row) {
                    return row.walamt;
                },
                "targets": 4
            },
            {
                "render": function (data, type, row) {
                    return row.request_amount;
                },
                "targets": 5
            },
             {
                "render": function (data, type, row) {
                    return row.amount_paid;
                },
                "targets": 6
            },
            {
                "render": function (data, type, row) {
                    return row.paid_date;
                },
                "targets": 7
            },
             {
                "render": function (data, type, row) {
                    return row.requested_date;
                },
                "targets": 8
            },
            {
                "render": function (data, type, row) {
                    //var minpay = row.minpayout;
                    var wallBal = row.wallet_balance;
                    var html = "";                    
                    if(row.payStatus == 0){
                        html += '<a href="#" data-toggle="modal" id="mentorPayoutModalId" class="mentorPayoutModal" data-recordId="' + row.id + '" data-email="' + row.email + '" data-paypalemail="' + row.paypal_email + '" data-paypalphone="' + row.paypal_phone + '" data-userId="' + row.user_id + '" data-walBal="' + wallBal + '" data-reqamt="' + row.request_amount + '" data-mentorName="' + row.fullName + '"  data-target="#mentorPayoutModal">Payout</a>';
                    }else{
                        html += 'Payout Done';
                    }
                    
//                    if (wallBal >= minpay && wallBal != '0') {
//                        html += ' | <a href="#" data-toggle="modal" id="mentorPayoutModalId" class="mentorPayoutModal" data-mentorName="' + row.fullName + '" data-mentorEmail="' + row.email + '" data-userId="' + row.user_id + '" data-walBal="' + wallBal + '" data-minpayout="' + row.minpayout + '" data-maxpayout="' + row.maxpayout + '" data-maxpayoutperweek="' + row.maxpayoutperweek + '" data-target="#mentorPayoutModal">Payout</a>';
//                    }
                    return html;
                },
                "targets": 9
            }
        ]
    });


    $('#updatePayoutAmount').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            payoutAmount: {
                required: true,
                number: true,
                noBlankSpace: true
            }
        },
        messages: {
            payoutAmount: {
                required: 'Please enter Amount.',
                number: 'Please enter digits only.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: adminUrl + "misc/payoutMentorWallet",
                method: "POST",
                data: $('#updatePayoutAmount').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {                    
                    if (response.data == "success") {
                        $('#updatePayoutAmount')[0].reset();
                        $('#mentorPayoutModal').modal('toggle');
                        displayAjaxNotificationMessage("Wallet updated successfully.", "success");
                        mentorPayoutDataTables.draw();
                    } else if (response.data.msg == "stop") {
                        if (response.data.pcnt == 0) {
                            $('#updatePayoutAmount')[0].reset();
                            $('#mentorPayoutModal').modal('toggle');
                            displayAjaxNotificationMessage("Maximum weekly amount for payout is " + response.data.wAmt + " only " + response.data.amt + " remaining ", "danger");
                        } else {
                            $('#updatePayoutAmount')[0].reset();
                            $('#mentorPayoutModal').modal('toggle');
                            displayAjaxNotificationMessage("Maximum weekly allowed payout is " + response.data.acnt + " and you have already done " + response.data.pcnt + " payouts this week. ", "danger");
                        }
                    } else {
                        $('#updatePayoutAmount')[0].reset();
                        $('#mentorPayoutModal').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#updatePayoutAmount')[0].reset();
                    $('#mentorPayoutModal').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });




});