var mentorPayoutDetailsTable = '';

$(document).ready(function () {

    mentorPayoutDetailsTable = $('#mentorPayoutDetailsTable').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getPayoutDetails",
            type: "POST",
            data: function (d) {
                d.mentorId = $('#mentorId').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    return '<a href="' + $('#absPath').val() + 'mentorprofile/' + row.slug + '"  target="_blank">' + row.fullName + '</a>';
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.email;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.amount;
                },
                "targets": 2
            },
            {
                "render": function (data, type, row) {
                    return row.cdate;
                },
                "targets": 3
            }
        ]
    });

});