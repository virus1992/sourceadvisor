var dataTableSystemSetting = '';
$(document).ready(function(){
    
    $('.classSlot1Match').on('keyup', function(e){
        $('.classSlot1Match').val($(this).val());
    });

    $('.classSlot2Match').on('keyup', function(e){
        $('.classSlot2Match').val($(this).val());
    });

    $('#frmSaveMentorPolicyForUser').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden], .hidden input, .hidden textarea, .hidden select',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txt_easy_first: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_easy_slot1: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_easy_second: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_easy_slot2: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_easy_third: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },

            txt_moderate_first: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_moderate_slot1: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_moderate_second: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_moderate_slot2: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_moderate_third: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },

            txt_strict_first: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_strict_slot1: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_strict_second: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            },
            txt_strict_slot2: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txt_strict_third: {
                noBlankSpace: true,
                required: true,
                number: true,
                range: [00, 100]
            }
        },
        messages: {
            txt_easy_first: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_easy_slot1: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_easy_second: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_easy_slot2: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_easy_third: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },

            txt_moderate_first: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_moderate_slot1: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_moderate_second: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_moderate_slot2: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_moderate_third: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },

            txt_strict_first: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_strict_slot1: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_strict_second: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            },
            txt_strict_slot2: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            },
            txt_strict_third: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
                range: "Please enter value less than 100"
            }
        },
        submitHandler: function (form) {

            $.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/updateMentorCancellationPolicyForUsers",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.flagMsg == "UPDATED") {
                            displayAjaxNotificationMessage("Mentor policy update for user.", "success");
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });
    

    $('#frmUpdateMentorCancellationCharges').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden], .hidden input, .hidden textarea, .hidden select',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txt_mentor_first: {
                noBlankSpace: true,
                required: true,
                number: true,
            },
            txt_mentor_first_hour: {
                noBlankSpace: true,
                required: true,
                digits: true
            }
        },
        messages: {
            txt_mentor_first: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                number: "Please enter valid number",
            },
            txt_mentor_first_hour: {
                noBlankSpace: "Please enter valid value",
                required: "Please enter valid value",
                digits: "Please enter only integer value"
            }
        },
        submitHandler: function (form) {

            $.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/updateMentorCancellationPolicyForMentor",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.flagMsg == "UPDATED") {
                            displayAjaxNotificationMessage("Mentor policy update.", "success");
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    //$(".summernote").markdown({autofocus:false,savable:false});

    $('#lstValueType').on('change', function(){
        var thisValue = $(this).val();

        $('.valueTypeElements').addClass('hidden');
        $('.valueTypeElements[data-ValueType="' + thisValue + '"]').removeClass('hidden');
        if(thisValue == 5) {
            //$('.valueTypeElements[data-ValueType="' + thisValue + '"]').attr('data-provide', 'markdown');
        }
    });

    $(document.body).delegate('.btnDeleteSystemVariable', 'click', function (e) {
        var systemSettingVariableId = $(this).attr('data-systemSettingVariableId');

        var dataToSend = {
            systemSettingVariableId: systemSettingVariableId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteSystemVariable",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    location.reload();
                    // displayAjaxNotificationMessage("Domain Deleted successfully.", "success");
                    // dataTableSystemSetting.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditSystemVariable', 'click', function(e){
        e.preventDefault();
        var systemSettingVariableId = $(this).attr('data-systemSettingVariableId');

        var dataToSend = {
            systemSettingVariableId: systemSettingVariableId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getSystemVariableDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    var data = response.data;
                    var systemSettingVariableIdForEdit = data.systemSettingVariableId;
                    var code = data.code;
                    var description = data.description;
                    var varused = data.varused;
                    var intValue = data.intValue;
                    var decimalValue = data.decimalValue;
                    var charValue = data.charValue;
                    var textValue = data.textValue;
                    var editorValue = data.editorValue;
                    var domainName = data.domainName;

                    $('#txtCode').val(code);
                    $('#taDescription').val(description);
                    $('#lstValueType').val(varused);
                    $('#txtIntValue').val(intValue);
                    $('#txtFloatValue').val(decimalValue);
                    $('#txtCharValue').val(charValue);
                    $('#txtTextValue').val(textValue);
                    $('#taEditorValue').val(editorValue);

                    $('.valueTypeElements').addClass('hidden');
                    $('.valueTypeElements[data-ValueType="' + varused + '"]').removeClass('hidden');
                    
                    $('#modalAddUpdatePopUp .modal-title').html('Edit Domain');
                    $('#hidActionType').val('update');
                    $('#hidSystemSettingVariableId').val(systemSettingVariableIdForEdit);
                    openRegionAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function(e){
        e.preventDefault();

        var systemSettingVariableId = $(this).attr('data-systemSettingVariableId');
        var dataToSend = {
            systemSettingVariableId: systemSettingVariableId,
            statusToChange: '2'
        };
        changeDomainStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function(e){
        e.preventDefault();

        var systemSettingVariableId = $(this).attr('data-systemSettingVariableId');
        var dataToSend = {
            systemSettingVariableId: systemSettingVariableId,
            statusToChange: '1'
        };
        changeDomainStatus(dataToSend);
    });


    $('#btnAddRegion').on('click', function(){
        $('#modalAddUpdatePopUp .modal-title').html('Add Domain');
        $('#hidActionType').val('add');
        $('#hidSystemSettingVariableId').val("0");
        openRegionAddEditModal();
    });

    $('#btnCloseSystemVariablePopup').on('click', function(){
        $('#modalAddUpdatePopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessSystemSettingPopup').on('click', function(){
        $('#frmAddEdit').submit();
    });


    $('#frmAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden], .hidden input, .hidden textarea, .hidden select',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtCode: {
                noBlankSpace: true,
                required: true
            },
            taDescription: {
                noBlankSpace: true,
                required: true
            },
            lstValueType: {
                noBlankSpace: true,
                required: true
            },
            txtIntValue: {
                noBlankSpace: true,
                required: true,
                digits: true
            },
            txtFloatValue: {
                noBlankSpace: true,
                required: true,
                number: true
            },
            txtCharValue: {
                noBlankSpace: true,
                required: true,
                maxlength: 1
            },
            txtTextValue: {
                noBlankSpace: true,
                required: true
            },
            taEditorValue: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtCode: {
                noBlankSpace: "Please enter valid code",
                required: "Please enter valid code"
            },
            taDescription: {
                noBlankSpace: "Please enter valid description",
                required: "Please enter valid description"
            },
            lstValueType: {
                noBlankSpace: "Please select value type",
                required: "Please select value type"
            },
            txtIntValue: {
                noBlankSpace: "Please enter valid Int Value",
                required: "Please enter valid Int Value",
                digits: "Please enter valid Int Value"
            },
            txtFloatValue: {
                noBlankSpace: "Please enter valid Decimal Value",
                required: "Please enter valid Decimal Value",
                number: "Please enter valid Decimal Value"
            },
            txtCharValue: {
                noBlankSpace: "Please enter valid Character Value",
                required: "Please enter valid Character Value",
                maxlength: "Please enter valid Character Value"
            },
            txtTextValue: {
                noBlankSpace: "Please enter valid Text Value",
                required: "Please enter valid Text Value"
            },
            taEditorValue: {
                noBlankSpace: "Please enter valid Editor Value",
                required: "Please enter valid Editor Value"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addSystemVariable",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.data.flagSuccess) {
                            location.reload();
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableSystemSetting = $('#tblSystemSetting').DataTable({
        "autoWidth": false,
        "columnDefs" : [
            {   "width": "20%", "targets": 0 },
            {   "width": "10%", "targets": 1 },
            {   "width": "55%", "targets": 2 },
            {   "width": "15%", "targets": 3 }
        ]
    });

    $('#modalAddUpdatePopUp').on('hidden.bs.modal', function(){
        closeRegionAddEditModal();
    });
});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openRegionAddEditModal(){
    //$('#frmAddEdit')[0].reset();
    $('#frmAddEdit label.validate_error').css('display', 'none');
    $('#modalAddUpdatePopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmAddEdit')[0].reset();
    $('#frmAddEdit label.validate_error').css('display', 'none');
    $('#modalAddUpdatePopUp').modal('hide');
}

function changeDomainStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForDomain",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            }
            else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}