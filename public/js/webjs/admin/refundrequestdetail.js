var dataTableRegion = '';
$(document).ready(function(){

    $('#chkRefundType').on('change', function(){
        var objThis = $(this);

        if(objThis.prop('checked') == true) {
            // full refund
            $('#txtAmountToRefund').val($('#totalPayAmountDisplay').val()).prop('disabled', 'disabled');
        }
        else {
            // partial refund
            $('#txtAmountToRefund').val('').prop('disabled', null);
        }
    });

    $("#divUserMessage").scrollTop($('#divUserMessage')[0].scrollHeight);

    $('a.chatTabsDiv').on('shown.bs.tab', function(){
        var tab = $(this).attr('href');
        var tabId = tab.substring(1);

        var chatDivId = $('#' + tabId).find('.chats').attr('id');
        $("#" + chatDivId).scrollTop($('#' + chatDivId)[0].scrollHeight);
    });

    $('#frmRefundRequest').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            if(element.attr('id') == 'taRefundDeclineRequest') {
                element.parent().find('.cke').after(error);
            }
            else {
                element.after(error);
            }
        },
        rules: {
            txtAmountToRefund: {
                required: true,
                number: true,
                maxlength: function(value) {
                    var totalPayAmountDisplay = $('#totalPayAmountDisplay').val();
                    var fieldValue = $(value).val();

                    totalPayAmountDisplay = parseFloat(totalPayAmountDisplay);
                    fieldValue = parseFloat(fieldValue);
                    
                    if(fieldValue > totalPayAmountDisplay) {
                        return false;
                    }
                }
            },
            taRefundMessage: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            txtAmountToRefund: {
                required: "Please enter valid number",
                number: "Please enter valid number",
                maxlength: "Please enter values less than meeting cost"
            },
            taRefundMessage: {
                required: "Please enter message",
                noBlankSpace: "Please enter message"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();

            $.ajax({
                url: adminUrl + "misc/partialRefundForRefundRequest",
                type: 'POST',
                data: frmData,
                success: function (response) {

                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.flagMsg == "SUCCESS") {
                            location.href = adminUrl + 'refundrequestlist';
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });

    $('#frmRefundRequestDecline').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            if(element.attr('id') == 'taRefundDeclineRequest') {
                element.parent().find('.cke').after(error);
            }
            else {
                element.after(error);
            }
        },
        rules: {
            taRefundDeclineRequest: {
                required: function(textarea) {
                    CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                    var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                    return editorcontent.length === 0;
                }
            }
        },
        messages: {
            taRefundDeclineRequest: {
                required: "Please enter message to user."
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/declineRefundRequest",
                type: 'POST',
                data: frmData,
                success: function (response) {

                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.flagMsg == "SUCCESS") {
                            location.href = adminUrl + 'refundrequestlist';
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });

    $('#taAdminMessageToUser').keypress(function(e){
        // if(e.which == 13) {
        //     var taAdminMessage = $('#taAdminMessageToUser').val();

        //     var objDataToSend = {
        //         taAdminMessage: taAdminMessage,
        //         userType: "user"
        //     }
        //     sendAdminMessage(objDataToSend);
        // }
    });

    $('#btnSendMessageToUser').on('click', function(e){
        e.preventDefault();
        
        var taAdminMessage = $('#taAdminMessageToUser').val();

        var objDataToSend = {
            taAdminMessage: taAdminMessage,
            userType: "user"
        }
        sendAdminMessage(objDataToSend);
    });


    $('#taAdminMessageToMentor').keypress(function(e){
        // if(e.which == 13) {
        //     var taAdminMessage = $('#taAdminMessageToMentor').val();

        //     var objDataToSend = {
        //         taAdminMessage: taAdminMessage,
        //         userType: "mentor"
        //     }
        //     sendAdminMessage(objDataToSend);
        // }
    });

    $('#btnSendMessageToMentor').on('click', function(e){
        e.preventDefault();
        
        var taAdminMessage = $('#taAdminMessageToMentor').val();

        var objDataToSend = {
            taAdminMessage: taAdminMessage,
            userType: "mentor"
        }
        sendAdminMessage(objDataToSend);
    });
});


function sendAdminMessage(sendMessageFor) {

    var taAdminMessage = sendMessageFor.taAdminMessage;
    var userType = sendMessageFor.userType;

    var messageDiv = "divUserMessage";
    var messageTa = "taAdminMessageToUser";
    if(userType == "mentor") {
        messageDiv = "divMentorMessage";
        messageTa = "taAdminMessageToMentor";
    }

    $('#' + messageTa).prop("disabled", "disabled");

    if(taAdminMessage != "") {

        $('#' + messageDiv).closest('.panel-body').block();

        var adminRefundMessageMasterId = $('#adminRefundMessageMasterId').val();
        var refundRequestId = $('#refundRequestId').val();
        var refundRequestUnqId = $('#refundRequestUnqId').val();
        var invitationId = $('#invitationId').val();
        var meetingUserId = $('#meetingUserId').val();
        var meetingMentorId = $('#meetingMentorId').val();
        var meetingDuration = $('#meetingDuration').val();
        var adminTimeZone = $('#adminTimeZone').val();
        
        var objDataToSend = {
            adminRefundMessageMasterId: adminRefundMessageMasterId,
            message: taAdminMessage,
            sendMessageFor: userType,
            refundRequestId:refundRequestId,
            refundRequestUnqId:refundRequestUnqId,
            invitationId:invitationId,
            meetingUserId:meetingUserId,
            meetingMentorId:meetingMentorId
        }

        $.ajax({
            url: adminUrl + "misc/sendAdminMessagesToUsers",
            type: 'POST',
            data: objDataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    if(response.flagMsg == "SUCCESS") {
                        if(response.data.status){
                            var messageTime = response.data.currentUTCTime;
                            var convertedTime = moment.tz(messageTime, 'UTC').tz(adminTimeZone).format('YYYY-MM-DD HH:MM:ss');
                            taAdminMessage = taAdminMessage.replace(/\n/g, '<br />');
                            var strMessage = '<div class="right-chat">' + 
                                                '<p>' + taAdminMessage + '<span>' + convertedTime + '</span></p>' + 
                                            '</div>';
                            $('#' + messageDiv).append(strMessage);
                        }
                        else {
                            alert("Some error occured.");
                        }
                    }
                    else {
                        alert("Some error occured.");
                    }
                }
                $("#" + messageDiv).scrollTop($('#' + messageDiv)[0].scrollHeight);
                $('#' + messageDiv).closest('.panel-body').unblock();
                $('#' + messageTa).val("");
                $('#' + messageTa).prop("disabled", null);
                $('#' + messageTa).focus();
            }
        });
    }
}