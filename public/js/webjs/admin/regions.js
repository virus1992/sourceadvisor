var dataTableRegion = '';
$(document).ready(function(){

    $(document.body).delegate('.btnDeleteRegion', 'click', function (e) {
        var regionId = $(this).attr('data-RegionId');

        var dataToSend = {
            regionId: regionId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteRegion",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    location.reload();
                    // displayAjaxNotificationMessage("Region Deleted successfully.", "success");
                    // dataTableRegion.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditRegion', 'click', function(e){
        e.preventDefault();
        var regionId = $(this).attr('data-RegionId');

        var dataToSend = {
            regionId: regionId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getRegionDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    var data = response.data;

                    var regionId = data.regionId;
                    var regionName = data.regionName;

                    $('#txtRegionName').val(regionName);
                    
                    $('#modalRegionAddEditPopUp .modal-title').html('Edit Region');
                    $('#hidActionType').val('update');
                    $('#hidRegionId').val(regionId);
                    openRegionAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function(e){
        e.preventDefault();

        var regionId = $(this).attr('data-RegionId');
        var dataToSend = {
            regionId: regionId,
            //statusToChange: '2'
            statusToChange: '0'
        };
        changeRegionStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function(e){
        e.preventDefault();

        var regionId = $(this).attr('data-RegionId');
        var dataToSend = {
            regionId: regionId,
            statusToChange: '1'
        };
        changeRegionStatus(dataToSend);
    });


    $('#btnAddRegion').on('click', function(){
        $('#modalRegionAddEditPopUp .modal-title').html('Add Region');
        $('#hidActionType').val('add');
        $('#hidRegionId').val("0");
        openRegionAddEditModal();
    });

    $('#btnCloseRegionPopup').on('click', function(){
        $('#modalRegionAddEditPopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessRegionPopup').on('click', function(){
        $('#frmRegionAddEdit').submit();
    });


    $('#frmRegionAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtRegionName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtRegionName: {
                noBlankSpace: true,
                required: "Please enter Region name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addRegion",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.data.flagSuccess) {
                            location.reload();
                            // $('#modalRegionAddEditPopUp').modal('hide');
                            // dataTableRegion.draw();
                            // if($('#hidActionType').val() == 'add') {
                            //     displayAjaxNotificationMessage("Region Added successfully", "success");
                            // }
                            // else {
                            //     displayAjaxNotificationMessage("Region Information updated successfully", "success");
                            // }
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableRegion = $('#tblRegion').DataTable({
        "autoWidth": false,
        "columnDefs" : [
            
            {   "width": "10%", "targets": 1 },
            {   "width": "15%", "targets": 2 }
        ]
    });

    $('#modalRegionAddEditPopUp').on('hidden.bs.modal', function(){
        closeRegionAddEditModal();
    });
});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openRegionAddEditModal(){
    //$('#frmRegionAddEdit')[0].reset();
    $('#frmRegionAddEdit label.validate_error').css('display', 'none');
    $('#modalRegionAddEditPopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmRegionAddEdit')[0].reset();
    $('#frmRegionAddEdit label.validate_error').css('display', 'none');
    $('#modalRegionAddEditPopUp').modal('hide');
}

function changeRegionStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForRegion",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            }
            else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}