var reportDataTable = "";

$(document).ready(function () {

    //var start = moment().subtract(29, 'days');
    var start = moment();
    var end = moment();

    $('#filterSD').val(start.format('YYYY-MM-DD'));
    $('#filterED').val(end.format('YYYY-MM-DD'));

    function cb(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#filterSD').val(start.format('YYYY-MM-DD'));
        $('#filterED').val(end.format('YYYY-MM-DD'));
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);

    $("#filterBtn").on('click', function () {
        getReportData();
    });

    getReportData();

});

function getReportData() {
    reportDataTable = $('#repotTable').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "DESC"]],
        "ajax": {
            url: adminUrl + "misc/getReportData",
            type: "POST",
            data: function (d) {
                d.user_type = $('#user_type').val();
                d.transTypefilter = $('#transTypefilter').val();
                d.filterSD = $('#filterSD').val();
                d.filterED = $('#filterED').val();

            }
        }
        ,
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    return row.created_date;
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    var ti = row.transection_id;
                    var tid = "";
                    if (ti == null) {
                        tid = "-";
                    } else {
                        tid = ti;
                    }
                    return tid;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.name;
                },
                "targets": 2
            },
            {
                "render": function (data, type, row) {
                    return row.reasonDisplay;
                },
                "targets": 3
            },
            {
                "render": function (data, type, row) {
                    var mk = row.meeting_url_key;
                    var mkf = "";
                    if (mk == null) {
                        mkf = "-";
                    } else {
                        mkf = mk;
                    }
                    return mkf;
                },
                "targets": 4
            },
            {
                "render": function (data, type, row) {

                    var status = row.payment_type;
                    var statusf = "";
                    if (status == null || status == "") {
                        statusf = "CARD AUTHORIZATION";
                    } else {
                        statusf = status;
                    }
                    return statusf;
                },
                "targets": 5
            },
            {
                "render": function (data, type, row) {
                    return parseFloat(row.amount).toFixed(2);
                },
                "targets": 6
            }
        ]
    });
}