var siteUserDataTables = '';
var filterUserType = '';
var filterUserStatus = '';

$(document).ready(function () {

    $('#filterUserType, #filterUserStatus').on('change', function () {
        siteUserDataTables.draw();
    });

    siteUserDataTables = $('#userMentorTable').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getSiteUsersList",
            type: "POST",
            data: function (d) {
                d.filterUserType = $('#filterUserType').val();
                d.filterUserStatus = $('#filterUserStatus').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {

                    if (row.userType == "user") {
                        return '<a href="' + $('#absPath').val() + 'users/userprofile/' + row.slug + '" target="_blank">' + row.fullName + '</a>';
                    } else {
                        return '<a href="' + $('#absPath').val() + 'mentorprofile/' + row.slug + '"  target="_blank">' + row.fullName + '</a>';
                    }
                },
                "targets": 0
            },
            {
                "render": function (data, type, row) {
                    return row.userType;
                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    return row.email;
                },
                "targets": 2
            },
            {
                "render": function (data, type, row) {
                    return row.phone_number;
                },
                "targets": 3
            },
            {
                "render": function (data, type, row) {
                    if (row.status == "1") {
                        return "Active";
                    } else if (row.status == "0") {
                        return "Inactive";
                    } else if (row.status == "2") {
                        return "Deleted";
                    } else if (row.status == "3") {
                        return "Suspended";
                    }
                },
                "targets": 4
            },
            {
                "render": function (data, type, row) {
                    var user_id = row.user_id;
                    var userType = row.userType;

                    if (userType == "user") {
                        return '<a href="' + adminUrl + 'userdetails/' + row.user_id + '">View details</a>';
                    } else {
                        return '<a href="' + adminUrl + 'mentordetails/' + row.user_id + '">View details</a>';
                    }
                },
                "targets": 5
            }
        ]
    });

});