var dataTableStates = '';
$(document).ready(function () {    

    $(document.body).delegate('.btnDeleteState', 'click', function (e) {
        var stateid = $(this).attr('data-stateid');

        var dataToSend = {
            stateid: stateid
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteState",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    displayAjaxNotificationMessage("State Deleted successfully.", "success");
                    dataTableStates.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditState', 'click', function (e) {
        e.preventDefault();
        var stateid = $(this).attr('data-stateid');

        var dataToSend = {
            stateid: stateid
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getStateDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {
                console.log(response);

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    var data = response.data;

                    if (data.length > 0) {
                        var stateDetails = data[0];
                        
                        var countryId = stateDetails.country_id;
                        var stateName = stateDetails.state_name;                                                
                        var stateId = stateDetails.state_id;                                                


                        $('#txtStateName').val(stateName);                       
                        $('#lstCountry').val(countryId);                       

                        $('#modalStateAddEditPopup .modal-title').html('Edit State');
                        $('#hidActionType').val('update');
                        $('#hidStateId').val(stateId);
                        openStateAddEditModal();
                    }
                }
                $.unblockUI();
            }
        });

    });


    $('#btnAddCountry').on('click', function () {
        $('#modalStateAddEditPopup .modal-title').html('Add State');
        $('#hidActionType').val('add');
        $('#hidStateId').val("0");
        openStateAddEditModal();
    });

    $('#btnCloseStatePopup').on('click', function () {
        $('#modalStateAddEditPopup').modal('hide');
        //closeStateAddEditModal();
    });

    $('#btnProcessStatePopup').on('click', function () {
        $('#frmStateAddEdit').submit();
    });


    $('#frmStateAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtStateName: {
                noBlankSpace: true,
                required: true
            },
            lstCountry: {                
                required: true
            }
        },
        messages: {
            txtStateName: {
                noBlankSpace: true,
                required: "Please enter state name"
            },
            lstCountry: {                
                required: "Please select country"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addState",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    } else {
                        if (response.data.flagSuccess) {
                            $('#modalStateAddEditPopup').modal('hide');
                            dataTableStates.draw();
                            if ($('#hidActionType').val() == 'add') {
                                displayAjaxNotificationMessage("State Added successfully", "success");
                            } else {
                                displayAjaxNotificationMessage("State Information updated successfully", "success");
                            }
                        } else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableStates = $('#tblStates').DataTable({
        "processing": true,
        "bDestroy": true,
        "serverSide": true,
        "bFilter": true,
        "sScrollX": "100%",
        "sScrollXInner": "100%",
        "order": [[0, "asc"]],
        "ajax": {
            url: adminUrl + "misc/getStatesList",
            type: "POST",
            data: function (d) {
                d.countryId = $('#countryId').val();
            }
        },
        "columnDefs": [
            {
                "render": function (data, type, row) {
                    var countryId = row.country_id;                    
                    var stateId = row.state_id;

                    return '<a href="' + adminUrl + 'cities/' + stateId + '/' + countryId + '" title="Goto Cities">' + row.state_name + '</a>';
                },
                "targets": 0
            },           
            {
                "render": function (data, type, row) {
                    var status = row.status;
                    var countryId = row.country_id;                    
                    var stateId = row.state_id;
                    if (status == 1) {
                        return '<a href="javaScript:;" class="btnToInactive" data-countryId="' + countryId + '" data-stateId="' + stateId + '" title="Click to inactivate"><label class="label listingBtn label-primary">Active</label></a>';
                    } else {
                        return '<a href="javaScript:;" class="btnToactive" data-countryId="' + countryId + '" data-stateId="' + stateId + '" title="Click to activate"><label class="label listingBtn label-warning">In-Active</label></a>';
                    }

                },
                "targets": 1
            },
            {
                "render": function (data, type, row) {
                    var stateId = row.state_id;
                    return '<button type="button" class="btn btn-md btn-primary btnEditState" data-stateId="' + stateId + '">Edit</button>&nbsp;&nbsp;<button type="button" class="btn btn-md btn-primary btnDeleteState" data-stateId="' + stateId + '">Delete</button>';
                    //return row.status;
                },
                "targets": 2
            }
        ]
    });

    $('#modalStateAddEditPopup').on('hidden.bs.modal', function () {
        closeStateAddEditModal();
    });

    $(document.body).delegate('.btnToInactive', 'click', function (e) {
        e.preventDefault();

        var stateId = $(this).attr('data-stateId');        
        
        var dataToSend = {
            stateId: stateId,
            //statusToChange: '2'
            statusToChange: '0'
        };
        changeStateStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function (e) {
        e.preventDefault();

        var stateId = $(this).attr('data-stateId');
            
        var dataToSend = {
            stateId: stateId,
            statusToChange: '1'
        };
        changeStateStatus(dataToSend);
    });


});


function openStateAddEditModal() {
    //$('#frmCountryAddEdit')[0].reset();
    $('#frmStateAddEdit label.validate_error').css('display', 'none');
    $('#modalStateAddEditPopup').modal('show');
}
function closeStateAddEditModal() {
    $('#frmStateAddEdit')[0].reset();
    $('#frmStateAddEdit label.validate_error').css('display', 'none');
    $('#modalStateAddEditPopup').modal('hide');
}

function changeStateStatus(dataToSend) {    
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForState",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            } else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}