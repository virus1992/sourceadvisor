var dataTableSubIndustry = '';
var industryId = '';

$(document).ready(function(){

    industryId = $('#hidIndustryId').val();
    $(document.body).delegate('.btnDeleteSubIndustry', 'click', function (e) {
        var subIndustryKeywordId = $(this).attr('data-subIndustryKeywordId');

        var dataToSend = {
            subIndustryKeywordId: subIndustryKeywordId
        }

        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteSubIndustryKeyword",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    location.reload();
                    // displayAjaxNotificationMessage("Industry Deleted successfully.", "success");
                    // dataTableSubIndustry.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditSubIndustry', 'click', function(e){
        e.preventDefault();
        var subIndustryKeywordId = $(this).attr('data-subIndustryKeywordId');
        var subIndustryId = $(this).attr('data-subIndustryId');

        var dataToSend = {
            subIndustryKeywordId: subIndustryKeywordId,
            subIndustryId: subIndustryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getSubIndustryKeywordInfo",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    var data = response.data;

                    var createdDate = data.createdDate;
                    var keyword = data.keyword;
                    var status = data.status;
                    var subIndustryId = data.subIndustryId;
                    var subIndustryKeywordId = data.subIndustryKeywordId;
                    var updatedDate = data.updatedDate;

                    $('#txtSubIndustryKeywordName').val(keyword);
                    
                    $('#modalSubIndustryKeywordAddEditPopUp .modal-title').html('Edit Sub-Industry Keyword - ' + keyword);
                    $('#hidActionType').val('update');
                    $('#subIndustryKeywordId').val(subIndustryKeywordId);
                    openSubIndustryAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function(e){
        e.preventDefault();

        var subIndustryId = $(this).attr('data-subIndustryId');
        var subIndustryKeywordId = $(this).attr('data-subIndustryKeywordId');
        var dataToSend = {
            subIndustryId: subIndustryId,
            subIndustryKeywordId:subIndustryKeywordId,
            statusToChange: '0'
        };
        changeSubIndustryKeywordStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function(e){
        e.preventDefault();

        var subIndustryId = $(this).attr('data-subIndustryId');
        var subIndustryKeywordId = $(this).attr('data-subIndustryKeywordId');
        var dataToSend = {
            subIndustryId: subIndustryId,
            subIndustryKeywordId:subIndustryKeywordId,
            statusToChange: '1'
        };
        changeSubIndustryKeywordStatus(dataToSend);
    });


    $('#btnAddSubIndustryKeywords').on('click', function(){
        $('#modalSubIndustryKeywordAddEditPopUp .modal-title').html('Add Sub-Industry Keyword');
        $('#hidActionType').val('add');
        $('#subIndustryKeywordId').val("0");
        openSubIndustryAddEditModal();
    });

    $('#btnCloseSubIndustryKeywordPopup').on('click', function(){
        $('#modalSubIndustryKeywordAddEditPopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessSubIndustryKeywordPopup').on('click', function(){
        $('#frmSubIndustryKeywordAddEdit').submit();
    });


    $('#frmSubIndustryKeywordAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtSubIndustryKeywordName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtSubIndustryKeywordName: {
                noBlankSpace: true,
                required: "Please enter Sub Industry keyword name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addSubIndustryKeyword",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.data.flagSuccess) {
                            location.reload();
                            // $('#modalSubIndustryKeywordAddEditPopUp').modal('hide');
                            // dataTableSubIndustry.draw();
                            // if($('#hidActionType').val() == 'add') {
                            //     displayAjaxNotificationMessage("Industry Added successfully", "success");
                            // }
                            // else {
                            //     displayAjaxNotificationMessage("Industry Information updated successfully", "success");
                            // }
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableSubIndustry = $('#tblSubIndustry').DataTable({
        "autoWidth": false,
        "columnDefs" : [
            {   "width": "15%", "targets": 0 },
            {   "width": "30%", "targets": 1 },
            {   "width": "30%", "targets": 2 },
            {   "width": "10%", "targets": 3 },
            {   "width": "15%", "targets": 4 }
        ]
    });

    $('#modalSubIndustryKeywordAddEditPopUp').on('hidden.bs.modal', function(){
        closeRegionAddEditModal();
    });
});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openSubIndustryAddEditModal(){
    //$('#frmSubIndustryKeywordAddEdit')[0].reset();
    $('#frmSubIndustryKeywordAddEdit label.validate_error').css('display', 'none');
    $('#modalSubIndustryKeywordAddEditPopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmSubIndustryKeywordAddEdit')[0].reset();
    $('#frmSubIndustryKeywordAddEdit label.validate_error').css('display', 'none');
    $('#modalSubIndustryKeywordAddEditPopUp').modal('hide');
}

function changeSubIndustryKeywordStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForSubIndustryKeyword",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            }
            else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}