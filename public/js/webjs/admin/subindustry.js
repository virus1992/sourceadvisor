var dataTableSubIndustry = '';
var industryId = '';
$(document).ready(function(){
    industryId = $('#hidIndustryId').val();
    $(document.body).delegate('.btnDeleteSubIndustry', 'click', function (e) {
        var subIndustryId = $(this).attr('data-subIndustryId');

        var dataToSend = {
            industryId: industryId,
            subIndustryId: subIndustryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteSubIndustry",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    location.reload();
                    // displayAjaxNotificationMessage("Industry Deleted successfully.", "success");
                    // dataTableSubIndustry.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditSubIndustry', 'click', function(e){
        e.preventDefault();
        var subIndustryId = $(this).attr('data-subIndustryId');

        var dataToSend = {
            subIndustryId: subIndustryId,
            industryId : industryId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getSubIndustryInfo",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                }
                else {
                    var data = response.data;

                    var industryIdFromResponse = data.industryId;
                    var industryName = data.industryName;
                    var status = data.status;
                    var subIndustryDescription = data.subIndustryDescription;
                    var subIndustryId = data.subIndustryId;
                    var subIndustryKeyWork = data.subIndustryKeyWork;
                    var subIndustryName = data.subIndustryName;


                    $('#txtIndustrynName').val(subIndustryName);
                    $('#taDescription').val(subIndustryDescription);
                    $('#taKeywords').val(subIndustryKeyWork);

                    
                    $('#modalSubIndustryAddEditPopUp .modal-title').html('Edit Sub Industry - ' + subIndustryName);
                    $('#hidActionType').val('update');
                    $('#hidSubIndustryId').val(subIndustryId);
                    openSubIndustryAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function(e){
        e.preventDefault();

        var subIndustryId = $(this).attr('data-subIndustryId');
        var industryId = $(this).attr('data-industryId');
        var dataToSend = {
            subIndustryId: subIndustryId,
            industryId:industryId,
            statusToChange: '0'
        };
        changeSubIndustryStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function(e){
        e.preventDefault();

        var subIndustryId = $(this).attr('data-subIndustryId');
        var industryId = $(this).attr('data-industryId');
        var dataToSend = {
            subIndustryId: subIndustryId,
            industryId:industryId,
            statusToChange: '1'
        };
        changeSubIndustryStatus(dataToSend);
    });


    $('#btnAddSubIndustries').on('click', function(){
        $('#modalSubIndustryAddEditPopUp .modal-title').html('Add Industry');
        $('#hidActionType').val('add');
        $('#hidSubIndustryId').val("0");
        openSubIndustryAddEditModal();
    });

    $('#btnCloseSubIndustryPopup').on('click', function(){
        $('#modalSubIndustryAddEditPopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessSubIndustryPopup').on('click', function(){
        $('#frmSubIndustryAddEdit').submit();
    });


    $('#frmSubIndustryAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtIndustrynName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtIndustrynName: {
                noBlankSpace: true,
                required: "Please enter Sub Industry name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addSubIndustry",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    }
                    else {
                        if(response.data.flagSuccess) {
                            location.reload();
                            // $('#modalSubIndustryAddEditPopUp').modal('hide');
                            // dataTableSubIndustry.draw();
                            // if($('#hidActionType').val() == 'add') {
                            //     displayAjaxNotificationMessage("Industry Added successfully", "success");
                            // }
                            // else {
                            //     displayAjaxNotificationMessage("Industry Information updated successfully", "success");
                            // }
                        }
                        else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableSubIndustry = $('#tblSubIndustry').DataTable({
        "autoWidth": false,
        "columnDefs" : [
            {   "width": "15%", "targets": 0 },
            {   "width": "30%", "targets": 1 },
            {   "width": "30%", "targets": 2 },
            {   "width": "10%", "targets": 3 },
            {   "width": "15%", "targets": 4 }
        ]
    });

    $('#modalSubIndustryAddEditPopUp').on('hidden.bs.modal', function(){
        closeRegionAddEditModal();
    });
});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openSubIndustryAddEditModal(){
    //$('#frmSubIndustryAddEdit')[0].reset();
    $('#frmSubIndustryAddEdit label.validate_error').css('display', 'none');
    $('#modalSubIndustryAddEditPopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmSubIndustryAddEdit')[0].reset();
    $('#frmSubIndustryAddEdit label.validate_error').css('display', 'none');
    $('#modalSubIndustryAddEditPopUp').modal('hide');
}

function changeSubIndustryStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForSubIndustry",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            }
            else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}