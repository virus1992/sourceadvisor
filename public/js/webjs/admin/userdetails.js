var dataTableRegion = '';
$(document).ready(function () {

    var elem_3 = document.querySelector('.js-switch_3');
    var switchery_3 = new Switchery(elem_3, {color: '#ff0000'});

    $('#chkUserSuspend').on('change', function () {
        var element = $(this);
        if (element.prop('checked')) {
            $('#suspend').modal('show');
        } else {
            continueUser();
        }
    });


    $(document.body).delegate('.btnDeleteDomain', 'click', function (e) {
        var domainId = $(this).attr('data-domainId');

        var dataToSend = {
            domainId: domainId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/deleteDomain",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    location.reload();
                    // displayAjaxNotificationMessage("Domain Deleted successfully.", "success");
                    // dataTableRegion.draw();
                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnEditDomain', 'click', function (e) {
        e.preventDefault();
        var domainId = $(this).attr('data-domainId');

        var dataToSend = {
            domainId: domainId
        }
        $.blockUI();
        $.ajax({
            url: adminUrl + "misc/getDomainDetails",
            type: 'POST',
            data: dataToSend,
            success: function (response) {

                if (response.flagMsg == 'ADMNOTLOG') {
                    window.location.href = adminUrl;
                } else {
                    var data = response.data;
                    var domainIdForEdit = data.domainId;
                    var domainName = data.domainName;

                    $('#txtDomainName').val(domainName);

                    $('#modalAddUpdatePopUp .modal-title').html('Edit Domain');
                    $('#hidActionType').val('update');
                    $('#hidDomainId').val(domainIdForEdit);
                    openRegionAddEditModal();

                }
                $.unblockUI();
            }
        });
    });

    $(document.body).delegate('.btnToInactive', 'click', function (e) {
        e.preventDefault();

        var domainId = $(this).attr('data-domainId');
        var dataToSend = {
            domainId: domainId,
            statusToChange: '2'
        };
        changeDomainStatus(dataToSend);
    });

    $(document.body).delegate('.btnToactive', 'click', function (e) {
        e.preventDefault();

        var domainId = $(this).attr('data-domainId');
        var dataToSend = {
            domainId: domainId,
            statusToChange: '1'
        };
        changeDomainStatus(dataToSend);
    });


    $('#btnAddRegion').on('click', function () {
        $('#modalAddUpdatePopUp .modal-title').html('Add Domain');
        $('#hidActionType').val('add');
        $('#hidDomainId').val("0");
        openRegionAddEditModal();
    });

    $('#btnCloseDomainPopup').on('click', function () {
        $('#modalAddUpdatePopUp').modal('hide');
        //closeRegionAddEditModal();
    });

    $('#btnProcessDomainPopup').on('click', function () {
        $('#frmAddEdit').submit();
    });


    $('#frmAddEdit').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            txtDomainName: {
                noBlankSpace: true,
                required: true
            }
        },
        messages: {
            txtDomainName: {
                noBlankSpace: true,
                required: "Please enter domain name"
            }
        },
        submitHandler: function (form) {

            //$.blockUI();
            var frmData = $(form).serialize();
            $.ajax({
                url: adminUrl + "misc/addDomain",
                type: 'POST',
                data: frmData,
                success: function (response) {
                    if (response.flagMsg == 'ADMNOTLOG') {
                        window.location.href = adminUrl;
                    } else {
                        if (response.data.flagSuccess) {
                            location.reload();
                            // $('#modalAddUpdatePopUp').modal('hide');
                            // dataTableRegion.draw();
                            // if($('#hidActionType').val() == 'add') {
                            //     displayAjaxNotificationMessage("Domain Added successfully", "success");
                            // }
                            // else {
                            //     displayAjaxNotificationMessage("Domain Information updated successfully", "success");
                            // }
                        } else {
                            displayAjaxNotificationMessage("Some Error Occured", "danger");
                        }
                    }
                    $.unblockUI();
                }
            });
        }
    });


    dataTableRegion = $('#tblUserTransaction').DataTable({
        "autoWidth": false,
        "columnDefs": [
            {"width": "20%", "targets": 0},
            {"width": "55%", "targets": 1},
            {"width": "10%", "targets": 2},
            {"width": "15%", "targets": 3}
        ]
    });

    $('#modalAddUpdatePopUp').on('hidden.bs.modal', function () {
        closeRegionAddEditModal();
    });



    $('#suspendFrm').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            suspendReason: {
                required: true
            }
        },
        messages: {
            suspendReason: {
                required: 'Please select reason.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: adminUrl + 'misc/suspendUser',
                method: "POST",
                data: $('#suspendFrm').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.data == "success") {
                        $('#suspendFrm')[0].reset();
                        $('#suspend').modal('toggle');
                        displayAjaxNotificationMessage("User suspended successfully.", "success");                        
                    } else {
                        $('#suspendFrm')[0].reset();
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#suspendFrm')[0].reset();
                    $('#suspend').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });


});

function openWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('show');
}
function closeWissenxChargeModal() {
    $('#frmGlobalWissenxCharge')[0].reset();
    $('#modalGlobalWissenxCharge').modal('hide');
}
function openRegionAddEditModal() {
    //$('#frmAddEdit')[0].reset();
    $('#frmAddEdit label.validate_error').css('display', 'none');
    $('#modalAddUpdatePopUp').modal('show');
}
function closeRegionAddEditModal() {
    $('#frmAddEdit')[0].reset();
    $('#frmAddEdit label.validate_error').css('display', 'none');
    $('#modalAddUpdatePopUp').modal('hide');
}

function changeDomainStatus(dataToSend) {
    $.blockUI();
    $.ajax({
        url: adminUrl + "misc/changeStatusForDomain",
        type: 'POST',
        data: dataToSend,
        success: function (response) {

            if (response.flagMsg == 'ADMNOTLOG') {
                window.location.href = adminUrl;
            } else {
                location.reload();
            }
            //$.unblockUI();
        }
    });
}

function continueUser() {
    var user = $("#suspendUser").val();
    var data = {
        suspendUser: user
    }


    $.ajax({
        url: adminUrl + 'misc/continueUser',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == "success") {
                displayAjaxNotificationMessage("User activated successfully.", "success");
            } else {
                displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
            }
        },
        error: function (response) {
            $.unblockUI();
            displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
        }

    });
}