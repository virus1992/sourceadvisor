$(".modal-file").on('hidden.bs.modal', function () {
    $("#filelbl").text("");
});

$("#blockUser").on('hidden.bs.modal', function () {
    $('input[id="radiofirst"]').trigger('click');
});

var selectedConversationId = "";
var conversationList = [];
var conversationMessagesList = [];
var messages_status = 'inbox';
var sendMessagePanelHeight = '';
var searchKeyWord = '';
var firstLoad = true;
var whenLoadedBlockUI = setInterval(function () {
    if (typeof $.blockUI == "function") {
        $.blockUI();
        clearInterval(whenLoadedBlockUI);
    }
}, 200);


$(document).ready(function () {

    $("#reasontxt").hide();
    $("#chars").parent().hide();

    selectedConversationId = $('#selectedConversationId').val();

    sendMessagePanelHeight = $("#panelfooter").height();
    getUserList();

//    window.setInterval(function () {
//        getUserConversations(role, searchKeyWord);
//    }, 5000);

    getUserConversations(role, searchKeyWord);
    resize();
    window.onresize = function () {
        resize();
    };

    $('#message_status').change(function () {
        messages_status = $(this).val();
        if (messages_status != "inbox") {
            $("#panelfooter").hide();
            $(".fileUploadTags").hide();
            $('#PersonalMessagesBlocker').height($('#PersonalMessagesBlocker').height() + sendMessagePanelHeight + 'px');
        } else {
            $('#PersonalMessagesBlocker').height($('#PersonalMessagesBlocker').height() - sendMessagePanelHeight + 'px');
            $("#panelfooter").show();
            $("#openBlockUser").show();
            $(".fileUploadTags").show();
            $("#delete").show();
            $("#unarchive").hide();
            $("#unBlockUser").hide();
            $("#archive").show();
        }


        if (messages_status == "archived") {
            $("#archive").hide();
            $("#unarchive").show();
            $("#openBlockUser").hide();
            $("#delete").hide();
            $("#unBlockUser").hide();
        } else {
            //$("#archive").show();
            //$("#unarchive").hide();
        }

        if (messages_status == "blocked") {
            $("#openBlockUser").hide();
            $("#archive").hide();
            $("#delete").hide();
            $("#unBlockUser").show();
            $("#unarchive").hide();
        } else {
            // $("#openBlockUser").show();
            // $("#unBlockUser").hide();
        }

        if (messages_status == "deleted") {
            $(".text-right").hide();
        } else {
            $(".text-right").show();
        }

        $("#chatNotSelected").show();
        $("#chatSelected").hide();

        getUserConversations(role, searchKeyWord);

    });

    $("#msgBody").on('keyup', function () {
        var text = $(this).val();
        text = $.trim(text);
        if (text == "") {
            $("#btn-chat").prop('disabled', true);
        } else {
            $("#btn-chat").prop('disabled', false);
        }

    });

    $("#msgNewBody").on('keyup', function () {
        var text = $(this).val();
        text = $.trim(text);
        if (text == "") {
            $("#sendNewMessage").prop('disabled', true);
        } else {
            $("#sendNewMessage").prop('disabled', false);
        }

    });

    var maxLength = $('#chars').text();
    $('textarea#reasontxt').keyup(function () {
        var length = $(this).val().length;
        $('#chars').text(maxLength - length);

        if (length > 0) {
            $("#reasonbtn").prop('disabled', false);
        } else {
            $("#reasonbtn").prop('disabled', true);
        }
    });

    $(":radio").on('change', function () {
        var chk = $(this).val();

        if (chk == "other") {
            $("#reasontxt").show();
            $("#chars").parent().show();
            $("#reasonbtn").prop('disabled', true);
        } else {
            $("#reasontxt").hide();
            $("#chars").parent().hide();
            $("#reasonbtn").prop('disabled', false);
        }
    });

    $("#msg_doc").change(function () {
        var detail = $(this);
        var name = detail[0].files[0].name;

        if (name == "") {
            $("#btn-chat").prop('disabled', true);
        } else {
            $("#btn-chat").prop('disabled', false);
        }


    });

    $('#searchbox').on('keypress', function (e) {
        if (e.which === 13) {
            searchKeyWord = $('#searchbox').val();
            if ($('#searchbox').val() != '') {
                getUserConversations(role, searchKeyWord);
            }
        }
    });

    $('#searchbox').keyup(function () {
        searchKeyWord = $('#searchbox').val();
        if ($('#searchbox').val() == '') {
            getUserConversations(role, searchKeyWord);
        }
    });

    $(document.body).on("click", "#conversationList li a", function () {

        $("#conversationList li").removeClass('active');
        $(this).parent().addClass('active');

        if ($(this).attr('data-messagetype') == "normalMessage") {
            $('#divTopRightChat').css('display', 'block');
            $('.upload').css('display', 'block');
            getConversationMessages($(this).attr('data-conversation-id'));
        } else {
            var adminRefundMessageId = $(this).attr('data-adminRefundMessageId');
            var adminRefundMessageMasterId = $(this).attr('data-adminRefundMessageMasterId');
            var adminRefundMessageThreadId = $(this).attr('data-adminRefundMessageThreadId');
            var userId = $('#hiddenId').val();

            $('#divTopRightChat').css('display', 'none');
            $('.upload').css('display', 'none');

            var objDataToPassToGetAdminMessage = {
                adminRefundMessageId: adminRefundMessageId,
                adminRefundMessageMasterId: adminRefundMessageMasterId,
                adminRefundMessageThreadId: adminRefundMessageThreadId,
                userId: userId
            }

            getAdminMessagesForTheUser(objDataToPassToGetAdminMessage);
        }

    });

    $("#archive").on("click", function () {
        var id = $(".conversationId").val();
        var status = "";
        archiveConversation(id, status);
    });

    $("#delete").on("click", function () {
        var id = $(".conversationId").val();
        var status = "deleted";
        deleteConversation(id, status);
    });

    $("#unarchive").on("click", function () {
        var id = $(".conversationId").val();
        var status = "inbox";
        archiveConversation(id, status);
    });

    $("#unBlockUser").on("click", function () {
        var id = $("#oppUserId").val();
        var reason = "";
        var con_id = $(".conversationId").val();
        blockUser(id, reason, con_id);
    });

    $("#reasonbtn").on("click", function (e) {
        e.preventDefault();
        var id = $("#oppUserId").val();


        var val = $('input[name=reason]:checked').val()

        if (val != "other") {
            var reason = val;
        } else {
            var reason = $('#reasontxt').val();
        }

        var con_id = $(".conversationId").val();
        blockUser(id, reason, con_id);
    });

    $("#sendMsg").submit(function () {
        $('#PersonalMessagesBlocker').block();
        $(this).ajaxSubmit({
            url: $('#absPath').val() + 'messages/sendUserMessages', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse, // post-submit callback 
            resetForm: true
        });
        return false;
    });

    $("#sendNewMsg").submit(function () {
        $.blockUI();
        $(this).ajaxSubmit({
            url: $('#absPath').val() + 'messages/sendUserMessages', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponseNew, // post-submit callback 
            resetForm: true
        });
        $('#NewMessage').modal('toggle');
        return false;
    });

    $("#msg_doc").on('change', function () {

        var items = $(this)[0].files;
        var lg = items.length;

        var fragment = "";

        if (lg > 5) {
            $("#filelbl").text("You cannot select more than 5 files.");
            $("#filemsg").modal('toggle');
            return false;
        }

        if (lg >= 5) {
            lg = 5;
        }

        if (lg > 0) {
            for (var i = 0; i < lg; i++) {
                var fileName = items[i].name; // get file name
                var fileSizeCondition = (parseFloat(items[i].size) / 1024) / 1024; // get file size 
                var fileSize = items[i].size;
                var fileType = items[i].type; // get file type

                var fileType = fileType.split('/');

                /*if (fileSizeCondition > 10) {
                 $("#filelbl").text("Please send files with size less than 10 MB.");
                 $("#filemsg").modal('toggle');
                 $("#msg_doc").val("");
                 $('#ulList').empty();
                 return false;
                 }*/

                // append li to UL tag to display File info
                fragment += "<li><span>" + fileName + "</span><b>(" + Math.round(parseInt(fileSize) / 1024) + " kb)</b> </li>";
            }

            $("#ulList").append(fragment);

            if ($('.fileUploadTags ul li').length > 5) {
                $("#filelbl").text("You cannot select more than 5 files.");
                $("#filemsg").modal('toggle');
                $(".fileUploadTags :nth-child(5)").nextAll("li").remove();
                return false;
            }
        }



    });

});

// pre-submit callback 
function showRequest(formData, jqForm, options) {
    var oppUserId = $('#oppUserId').val();
    if (oppUserId == "-1") {
        var adminRefundMessageId = $('#conversationList li.active a').attr('data-adminrefundmessageid');
        var adminRefundMessageMasterId = $('#conversationList li.active a').attr('data-adminRefundMessageMasterId');
        var adminRefundMessageThreadId = $('#conversationList li.active a').attr('data-adminRefundMessageThreadId');

        var objAdminRefundMessageId = {
            "name": "adminRefundMessageId",
            "required": false,
            "type": "hiddden",
            "value": adminRefundMessageId
        }

        formData.push(objAdminRefundMessageId);


        var objAdminRefundMessageMasterId = {
            "name": "adminRefundMessageMasterId",
            "required": false,
            "type": "hiddden",
            "value": adminRefundMessageMasterId
        }

        formData.push(objAdminRefundMessageMasterId);


        var objAdminRefundMessageThreadId = {
            "name": "adminRefundMessageThreadId",
            "required": false,
            "type": "hiddden",
            "value": adminRefundMessageThreadId
        }

        formData.push(objAdminRefundMessageThreadId);

    }
    var queryString = $.param(formData);
    return true;
}

// post-submit callback 
function showResponse(responseText, statusText, xhr, $form) {

    if (responseText.message == "block") {
        $('.blockdiv label').text('You are blocked by this user.');
        $("#blockUserMsg").modal('toggle');
    }

    if (responseText.message == "blocked") {
        $('.blockdiv label').text('Unblock this user to send message.');
        $("#blockUserMsg").modal('toggle');
    }


    if (responseText.message == "success") {
        $("#msgBody").val("");
        //$('li.active a').trigger("click");
        $('#PersonalMessagesBlocker').unblock();
        $.unblockUI();
        $('li.active a').trigger("click");
        $("#msg_doc").val("");
        $('#ulList').empty();
        //getUserConversations(role, searchKeyWord);

    } else {
        $('#PersonalMessagesBlocker').unblock();
        $.unblockUI();
        //$('#PersonalMessagesBlocker').block();
    }
}

// post-submit callback 
function showResponseNew(responseText, statusText, xhr, $form) {

    if (responseText.message == "block") {
        $('.blockdiv label').text('You are blocked by this user.');
        $("#blockUserMsg").modal('toggle');
    }

    if (responseText.message == "blocked") {
        $('.blockdiv label').text('Unblock this user to send message.');
        $("#blockUserMsg").modal('toggle');
    }

    if (responseText.message == "success") {
        $("#msgBody").val("");
        //$('li.active a').trigger("click");
        $('#PersonalMessagesBlocker').unblock();
        $.unblockUI();

        var roles = "";

        if (role == "user") {
            roles = "users";
        } else {
            roles = "mentor";
        }

        window.location = '/' + roles + '/messages';
        //$('li.active a').trigger("click");
        getUserConversations(role, searchKeyWord);

    } else {
        $('#PersonalMessagesBlocker').unblock();
        $.unblockUI();
        //$('#PersonalMessagesBlocker').block();
    }
}

function getConversationMessages(conversationId) {

    $.ajax({
        url: $('#absPath').val() + 'messages/getConversationMessages',
        type: 'POST',
        data: {
            conversationId: conversationId,
            status: messages_status
        },
        beforeSend: function () {
            if (!firstLoad) {
                $.blockUI();
            } else {
                firstLoad = false;
            }
        },
        complete: function () {

        },
        success: function (response) {
            var html = '';
            var conversationDetail = conversationList.conversation.filter(function (element, index, array) {
                if (element.conversation_id == conversationId) {
                    if (element.username == 'SourceAdvisor Admin') {
                        $('.msgerTop .opponent_profile_image').attr('src', $('#absPath').val() + 'images/imgpsh_fullsize.png');
                        $('.opponent_profile_image').css('background','grey');
                    } else {
                        $('.msgerTop .opponent_profile_image').attr('src', element.finalProfileImageUrl);
                    }
                    if (element.user_type == 'mentor') {
                        $('.msgerTop .opponent_user_name').html('<a class="msgUsernameLink" href="' + $('#absPath').val() + 'mentorprofile/' + element.slug + '" target="_blank">' + element.username + '</a>');

                    } else if (element.user_type == 'user') {
                        $('.msgerTop .opponent_user_name').html('<a class="msgUsernameLink" href="' + $('#absPath').val() + 'users/userprofile/' + element.slug + '" target="_blank">' + element.username + '</a>');
                    } else {
                        $('.msgerTop .opponent_user_name').html(element.username);
                    }

                    messages_status = $("#message_status").val();
                    if (messages_status != "inbox") {
                        $("#panelfooter").hide();
                        $(".fileUploadTags").hide();
                        // $('#PersonalMessagesBlocker').height($('#PersonalMessagesBlocker').height() + sendMessagePanelHeight + 'px');
                    } else {
                        // $('#PersonalMessagesBlocker').height($('#PersonalMessagesBlocker').height() - sendMessagePanelHeight + 'px');
                        $("#panelfooter").show();
                        $("#openBlockUser").show();
                        $(".fileUploadTags").show();
                        $("#delete").show();
                        $("#unarchive").hide();
                        $("#unBlockUser").hide();
                        $("#archive").show();
                    }


                    if (messages_status == "archived") {
                        $("#archive").hide();
                        $("#unarchive").show();
                        $("#openBlockUser").hide();
                        $("#delete").hide();
                        $("#unBlockUser").hide();
                    } else {
                        //$("#archive").show();
                        //$("#unarchive").hide();
                    }

                    if (messages_status == "blocked") {
                        $("#openBlockUser").hide();
                        $("#archive").hide();
                        $("#delete").hide();
                        $("#unBlockUser").show();
                        $("#unarchive").hide();
                    } else {
                        // $("#openBlockUser").show();
                        // $("#unBlockUser").hide();
                    }

                    if (messages_status == "deleted") {
                        $(".text-right").hide();
                    } else {
                        $(".text-right").show();
                    }

                    $("#chatNotSelected").show();
                    $("#chatSelected").hide();


                    $('#oppUserId').val(element.user_id);
                    var oppUser = $("#oppUserId").val();

                    if (messages_status == "inbox") {
                        if (oppUser == "") {
                            $("#btn-chat").hide();
                            $("#uploadDoc").hide();
                            $("#panelfooter").hide();
                        } else {
                            $("#btn-chat").show();
                            $("#uploadDoc").show();
                            $("#panelfooter").show();
                        }
                    }

                }
            });
            generateMessagesHtml(response.data);
            $(".active").find('span').remove();
//            setTimeout(function () {
            $("#PersonalMessages").scrollTop($('#PersonalMessages')[0].scrollHeight);
            $("#chatNotSelected").hide();
            $("#chatSelected").show();
            $.unblockUI();
//            }, 3000);

        }
    });
}


function getAdminMessagesForTheUser(objDataParams) {
    $.ajax({
        url: $('#absPath').val() + 'messages/getAdminMessagesForTheUser',
        type: 'POST',
        data: objDataParams,
        beforeSend: function () {
            if (!firstLoad) {
                $.blockUI();
            } else {
                firstLoad = false;
            }
        },
        complete: function () {

        },
        success: function (response) {
            console.log(response);
            var html = '';

            $('.msgerTop .opponent_profile_image').attr('src', $('#absPath').val() + 'images/imgpsh_fullsize.png');
            $('.msgerTop .opponent_user_name').html('<a class="msgUsernameLink" href="javaScript:;" >Wissenx Admin</a>');

            $('#oppUserId').val("-1");

            generateMessagesHtml(response.data, true);
            $(".active").find('span').remove();

            $("#PersonalMessages").scrollTop($('#PersonalMessages')[0].scrollHeight);
            $("#chatNotSelected").hide();
            $("#chatSelected").show();
            $.unblockUI();
            var oppUser = $("#oppUserId").val();

            if (oppUser == "") {
                $("#btn-chat").hide();
                $("#uploadDoc").hide();
                $("#panelfooter").hide();
            } else {
                $("#btn-chat").show();
                $("#uploadDoc").show();
                $("#panelfooter").show();
            }



        }
    });
}

function getUserConversations(role, searchKeyWord) {
    $.ajax({
        url: $('#absPath').val() + 'messages/getUserConversations',
        type: 'POST',
        data: {
            status: messages_status,
            role: role,
            searchKeyWord: searchKeyWord
        },
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var html = '';
            conversationList = response.data;
            generateConversationHtml();
        }
    });
}

function resize() {
    sendMessagePanelHeight = $("#panelfooter").height();
    var heights = window.innerHeight;
    heights = parseInt(heights);
    var height = heights - 60;
    document.getElementById("chatPage").style.height = height + "px";

    var hgt = heights - 320;
    document.getElementById("PersonalMessages").style.height = hgt + "px";
}

function generateConversationHtml(customConversationList) {
    if (typeof customConversationList == "undefined") {
        customConversationList = conversationList;
    }
    var html = '';

    var normalUserMessages = customConversationList.conversation;
    var AdminMessages = customConversationList.adminMessages;

    $.each(normalUserMessages, function (index, item) {

        html += '<li>';
        html += '<a href="javascript:;" data-conversation-id="' + item.conversation_id + '" data-opponent-id="' + item.opponent_id + '" data-messageType="normalMessage">';
        if (item.count_unread != 0) {
            html += '<span class="badge-unread">' + item.count_unread + '</span>'
        }
        if (item.username != "Admin ") {
            if (item.username != "SourceAdvisor Admin") {
                html += '<img src="' + item.finalProfileImageUrl + '">';
            } else {
                html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
            }
        } else {
            html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
        }
        html += '<div class="MsgerName">';
        html += '<h2>' + item.username + ' <time>' + moment.tz(item.last_message_datetime, 'UTC').tz(userTimezone).fromNow() + '</time></h2>';
        //html += '<span>' + item.last_message + '</span>';
        html += '</div>';
        html += '</a>';
        html += '</li>';
    });

    $.each(AdminMessages, function (index, item) {
        html += '<li>';
        html += '<a href="javascript:;" data-adminRefundMessageId="' + item.adminRefundMessageId + '" data-adminRefundMessageMasterId="' + item.adminRefundMessageMasterId + '" data-adminRefundMessageThreadId="' + item.adminRefundMessageThreadId + '" data-messageType="adminMessage">';
        if (item.unreadCount != 0) {
            html += '<span class="badge-unread">' + item.unreadCount + '</span>'
        }
        html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
        html += '<div class="MsgerName">';
        html += '<h2>' + item.refundRequestUniqId + ' <time>' + moment.tz(item.lastMessageSendDate, 'UTC').tz(userTimezone).fromNow() + '</time></h2>';
        //html += '<span>' + item.last_message + '</span>';
        html += '</div>';
        html += '</a>';
        html += '</li>';
    });

    $('.UserMsgList ul#conversationList').html(html);
    if (typeof selectedConversationId != "undefined" && selectedConversationId != "") {
        $('.UserMsgList ul#conversationList a[data-conversation-id=' + selectedConversationId + ']').trigger("click");
    } else if (customConversationList.length > 0 && $('.UserMsgList ul#conversationList li.active').length == 0) {
        $('.UserMsgList ul#conversationList a:first').trigger("click");
    }
}

function generateMessagesHtml(data, flagAdminMessage) {

    if (typeof flagAdminMessage != 'boolean') {
        flagAdminMessage = false;
    }

    var html = '';
    $.each(data, function (index, item) {
        var status_class = '';

        if (!flagAdminMessage) {
            if ($('#hiddenId').val() == item.reply_sender_id) {
                status_class = 'sent';
            } else {
                status_class = 'receive';
            }
        } else {
            if (item.messageFrom == "U") {
                status_class = 'sent';
            } else {
                status_class = 'receive';
            }
        }

        html += '<div class="row msg_container base_' + status_class + '">';
        html += '<div class="col-md-11 col-xs-11">';
        html += '<div class="messages msg_' + status_class + '">';

        if (!flagAdminMessage) {
            if (status_class == "receive") {
                if (item.file_name != null && item.message_body == "File Sent") {
                    html += '<div> File Recived </div>';
                } else {
                    html += '<div>' + item.message_body + '</div>';
                }
            } else {
                html += '<div>' + item.message_body + '</div>';
            }
            if (item.file_name != null) {
                html += '<img src=' + $('#absPath').val() + 'images/file.png' + ' style=" width : 70px;" />';
                // if (status_class == "receive") {
                html += '<a href=' + $('#absPath').val() + 'messageDocs/' + item.file_name + '?dl >Download</a>';
                // }
            }
            html += '<time>' + moment.tz(item.msg_sent_date, 'UTC').tz(userTimezone).fromNow() + '</time>';
        } else {
            html += '<div>' + item.message + '</div>';
            html += '<time>' + moment.tz(item.messageSendDate, 'UTC').tz(userTimezone).fromNow() + '</time>';
        }

        html += '</div>';

        if (!flagAdminMessage) {
            html += '<input type="hidden" class="conversationId" name="conversationId" value="' + item.conversation_id + '" />';
        } else {
            html += '<input type="hidden" class="adminRefundMessageId" name="adminRefundMessageId" value="' + item.adminRefundMessageId + '" />';
        }
        html += '</div>';
        html += '</div>';
    });
    $('div#PersonalMessages').html(html).unblock();
//    setTimeout(function () {
    $("#PersonalMessages").scrollTop($('#PersonalMessages')[0].scrollHeight);
    $("#chatNotSelected").hide();
    $("#chatSelected").show();
//    }, 3000);

}

/*function sendUserMessage(userId, msgBody) {
 
 $.ajax({
 url: $('#absPath').val() + 'messages/sendUserMessages',
 type: 'POST',
 data: {
 oppUserId: userId,
 msgBody: msgBody
 },
 beforeSend: function () {
 $('#PersonalMessagesBlocker').block();
 },
 complete: function () {
 $('#PersonalMessagesBlocker').unblock();
 },
 success: function (response) {
 
 if (response.message == "success") {
 $("#msgBody").val("");
 $('li.active a').trigger("click");
 getUserConversations();
 }
 }
 });
 }*/

function getUserList() {
    $.ajax({
        url: $('#absPath').val() + 'mentor/userList',
        method: "POST",
        data: "",
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                $('#lstUserList').html("");
                var strNewOptions = "";
                strNewOptions += "<option value=''>Select User</option>";
                $.each(arrUserData, function (index, arrUserInfo) {
                    strNewOptions += "<option value='" + arrUserInfo.user_id + "'>" + arrUserInfo.username + "</option>";
                });
                $('#lstUserList').html(strNewOptions);
                $("#lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

function archiveConversation(id, status) {

    var data = {id: id, status: status};

    $.ajax({
        url: $('#absPath').val() + 'messages/archiveMsg',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                $("#chatNotSelected").show();
                $("#chatSelected").hide();
                getUserConversations(role, searchKeyWord);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

function deleteConversation(id, status) {

    var data = {id: id, status: status};

    $.ajax({
        url: $('#absPath').val() + 'messages/deleteMsg',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                $("#chatNotSelected").show();
                $("#chatSelected").hide();
                getUserConversations(role, searchKeyWord);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

function blockUser(id, reason, con_id) {

    var data = {id: id, reason: reason, con_id: con_id};

    $.ajax({
        url: $('#absPath').val() + 'messages/blockUsers',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                $('#blockUser').modal('hide');
                $("#chatNotSelected").show();
                $("#chatSelected").hide();
                $("#reason").val("");
                getUserConversations(role, searchKeyWord);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}