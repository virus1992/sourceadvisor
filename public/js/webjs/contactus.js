$(document).ready(function () {
    $('#frm_sndmsg').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            name: {
                required: true,
                noBlankSpace: true
            },
            email: {
                required: true,
                email: true,
                noBlankSpace: true
            },
            number: {
                required: true,
                noBlankSpace: true
            },
            comment: {
                required: true,
                noBlankSpace: true,
            },
         
        },
        messages: {
            name: {
                required: "Please enter  name"
            },
            email: {
                required: "Please enter email address",
                email: "Please enter valid email address"
            },
            number: {
                required: "Please enter number"
            },
            comment: {
                required: "Please enter password",
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: $('#absPath').val() + 'messages/sendContactMsg',
                type: 'POST',
                data: $('#frm_sndmsg').serialize(),
                beforeSend: function() {
                    $.blockUI();
                },
                complete: function() {
                    $.unblockUI();
                },
                success: function(response) {
        
                    var data = response.data;
        
                    if(data.length > 0) {
                      
                    }
                    else {
                        $('#hidReviewMoreDataAvailable').val('N');
                    }
                }
            });
        }
    });
    });