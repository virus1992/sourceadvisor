$(document).ready(function(){

	$('#frmForgotPassword').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
			element.after(error);
        },
        rules: {
            email: {
                required: true,
                email: true,
                noBlankSpace: true
            },
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Please enter Valid Email Address"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});