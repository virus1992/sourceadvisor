role = (role == 'user') ? 'users' : 'mentor';
var isloggedIn = $("#loginuser").val();
var isloggedInUser = $("#hidLoggedInUserId").val();
var userInitialConfig = {
    type: "input",
    section: "userHeader",
    loopClass: "dropdown.header-avatar-trigger",
    srcClass: "himg",
    nameClass: "hfname",
    nameHtmlClass: "himgdiv",
    imgClass: "himg"
};
var userInitialInterval = setInterval(function () {
    if ($('.himg').length > 0) {
        clearInterval(userInitialInterval);
        setUserInitialImage({
            type: "input",
            section: "userHeader",
            loopClass: "dropdown.header-avatar-trigger",
            srcClass: "himg",
            nameClass: "hfname",
            nameHtmlClass: "himgdiv",
            imgClass: "himg"
        });
    }
}, 1);
$(document).ready(function () {
    getUserConversation(role);
    checkMeetingPopUp();

    $(document.body).on('click', '#dropdown_menu_msg li a', function () {
        var conversation_id = $(this).attr('data-conversation-id');
        $('form#openSelectedConversation #hdn_conversation_id').val(conversation_id);
        $('form#openSelectedConversation').submit();
    });

    if (isloggedInUser != "") {
        window.setInterval(function () {
            checkMeetingPopUp();
        }, 60000);
    }

    $(".meetingbtn").on("click", function () {
        $("#upcomingmeetingpopup").modal('hide');
    });

});

if (isloggedInUser != "") {
    window.setInterval(function () {
        getUserConversation(role);
    }, 10000);
}

function checkMeetingPopUp() {
    $.ajax({
        url: $('#absPath').val() + 'chkMeeting',
        type: 'POST',
        data: {},
        success: function (response) {
            $.each(response.data, function (index, item) {
                if (item.requester_id == isloggedInUser) {
                    var time = moment.tz(item.slot_date + " " + item.slot_time, "UTC").tz(item.userZone).fromNow();
                    if (time == "in 5 minutes") {
                        $(".meetingbtn").attr("href", $('#absPath').val() + "mentor/startMeeting/" + item.meeting_url_key);
                        $("#upcomingmeetingpopup").modal();
                    }
                } else if (item.user_id == isloggedInUser) {
                    var time = moment.tz(item.slot_date + " " + item.slot_time, "UTC").tz(item.metorZone).fromNow();
                    if (time == "in 5 minutes") {
                        $(".meetingbtn").attr("href", $('#absPath').val() + "mentor/startMeeting/" + item.meeting_url_key);
                        $("#upcomingmeetingpopup").modal();
                    }
                }

            });
        }
    });
}

function getUserConversation(role) {
    $.ajax({
        url: $('#absPath').val() + 'messages/getUserConversations',
        type: 'POST',
        data: {
            status: 'inbox',
            role: role,
            action: 'dashboard'
        },
        success: function (response) {
            var html = '';
            response.data;
            generateConversationsHtml(response.data);
        }
    });
}


function generateConversationsHtml(conversationList) {
    var html = '';
    var htmlblank = '';
    var noti_count = 0;
    var count = 0;
    $("#loadingLi").hide();

    var normalUserMessages = [];
    if (typeof conversationList.conversation != 'undefined') {
        if (conversationList.conversation.length > 0) {
            normalUserMessages = conversationList.conversation;
        }
    }
    var AdminMessages = conversationList.adminMessages;

    $.each(normalUserMessages, function (index, item) {
        if (count >= "5") {
            return false;
        }
        var msg = item.last_message.substr(0, 10);
        html += '<li>';
        html += '<div class="MsgNotif">';
        html += '<div class="image" style="background: grey;">';
        //html += '<img src="' + item.finalProfileImageUrl + '">';
        if (item.username != "Admin ") {
            if (item.username != "SourceAdvisor Admin") {
                html += '<img src="' + item.finalProfileImageUrl + '">';
            } else {
                html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
            }
        } else {
            html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
        }
        if (item.count_unread != 0) {
            html += '<span class="unread"></span>';
            ++noti_count;
        }
        html += '</div>';
        html += '<div class="details">';
        html += '<a href="javascript:;" data-conversation-id="' + item.conversation_id + '" data-opponent-id="' + item.opponent_id + '">';
        html += '<h3>' + item.username + ' <time>' + moment.tz(item.last_message_datetime, 'UTC').tz(userTimeZone).fromNow() + '</time></h3>';
        html += '</a>';
        html += '<label class="label-content">' + htmlToStringConversionAdmin(item.last_message, 100) + '</label>';
        html += '</div>';
        html += '</div>';
        html += '</li>';
        count++;
    });



    $.each(AdminMessages, function (index, item) {
        if (count >= "5") {
            return false;
        }
        var msg = item.lastMessageSendDate.substr(0, 10);
        html += '<li>';
        html += '<div class="MsgNotif">';
        html += '<div class="image" style="background : grey;">';
        html += '<img src="' + $('#absPath').val() + 'images/imgpsh_fullsize.png">';
        if (item.unreadCount != 0) {
            html += '<span class="unread"></span>';
            ++noti_count;
        }
        html += '</div>';
        html += '<div class="details">';
        //html += '<a href="javascript:;" data-conversation-id="' + item.conversation_id + '" data-opponent-id="' + item.opponent_id + '">';
        html += '<a href="javascript:;" data-adminRefundMessageId="' + item.adminRefundMessageId + '" data-adminRefundMessageMasterId="' + item.adminRefundMessageMasterId + '" data-adminRefundMessageThreadId="' + item.adminRefundMessageThreadId + '" data-messageType="adminMessage">';
        html += '<h3>' + item.refundRequestUniqId + ' <time>' + moment.tz(item.lastMessageSendDate, 'UTC').tz(userTimeZone).fromNow() + '</time></h3>';
        html += '</a>';
        html += '<label class="label-content">' + htmlToStringConversionAdmin(item.lastMessage, 100) + '</label>';
        html += '</div>';
        html += '</div>';
        html += '</li>';
        count++;
    });

    if (role == "user") {
        role = "users";
    }

    html += '<li><a href="' + $("#frontUrl").val() + role + '/messages" class="view-all">View All</a></li>';
    $('#navbarMsgCount').html(noti_count);
    $('.dropdown #dropdown_menu_msg ').html("");
    $('.dropdown #dropdown_menu_msg ').html(html);

}

function htmlToStringConversionAdmin(input, length) {
    var tmp = document.createElement('div');
    tmp.innerHTML = input;
    var content = tmp.textContent || tmp.innerText || "";
    if (typeof length == "undefined" || length == '') {
        return content;
    } else {
        if (content.length < length) {
            return content;
        } else {
            return $.trim(content.substr(0, length)) + "...";
            //return content.substr(0, length) ;
        }
    }
}