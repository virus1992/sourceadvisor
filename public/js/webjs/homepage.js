var userInitialConfig = {
    section: "high_profile",
    loopClass: "col-sm-4.high-profile-mentors.text-center",
    srcClass: "metors-pic img",
    nameClass: "metors-details h4 a",
    nameHtmlClass: "high-profile-mentorsImgDiv",
    imgClass: "metors-pic img"
};

$(document).ready(function () {
    getPost();
    var absPath = $('#absPath').val();

    setListingData();

    setTimeZoneForRegisteringUser();

    $(".modal-fullscreen").on('show.bs.modal', function () {
        setTimeout(function () {
            $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        }, 0);
    });

    $(".modal-fullscreen").on('hidden.bs.modal', function () {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    });

    // preventing form submit at the time of pressing enter.
    document.querySelector('#frmHomePageSearch').onkeypress = checkEnter;

    $('#frmHomePageSearch').validate({
        errorClass: "validate_error",
        ignore: '',
        errorPlacement: function (error, element) {
            //element.after(error);
            //element.parent().parent().find("#searchErrorDiv").after(error);
            element.parent().parent().after(error);
        },
        groups: {
            names: "hidLocationId txtQueryString"
        },
        onfocusout: false,
        rules: {
            hidLocationId: {
                require_from_group: [1, ".js-searchValidation"]
            },
            txtQueryString: {
                require_from_group: [1, ".js-searchValidation"]
            }
        },
        messages: {
            hidLocationId: {
                require_from_group: "Please select atleast location or a single keyword"
            },
            txtQueryString: {
                require_from_group: "Please select atleast location or a single keyword"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    setUserInitialImage(userInitialConfig);




});

function checkEnter(e) {
    e = e || event;
    return (e.keyCode || e.which || e.charCode || 0) !== 13;
}

function setListingData() {
    var showChar = 20;
    $('.sc_team_item_description').each(function () {
        var content = $(this).html();
        $(this).html(htmlToStringConversion(content, showChar));
    });
}

function getPost() {
    var absPath = $('#absPath').val();    
    $.ajax({
        url: absPath + 'blog/wp-json/wp/v2/posts',
        method: "GET",
        data: "",
        success: function (response) {
            if (response != "") {
                var html = "";
                var count = 0;
                $(".RecentPost").html('');
                $.each(response, function (index, item) {
                    html = '';
                    html += '<div class="col-sm-4"><div class="post">';
                    html += '<div class="post-img-content">';
                    html += '<div class="post-thumb"><img src="" width="323" height="194"  class = "fiid_' + item.featured_media + ' img-responsive"  />';
                    html += '</div>';
                    html += '<h4 class="post-title">' + item.title.rendered + '</h4>';
                    html += '</div>';
                    html += '<div class="content">';
                    html += htmlToStringConversion(item.excerpt.rendered, "100");                    
                    html += '<a href="' + item.link + '" class="simple-btn" target="_blank">Read more</a>';
                    html += '</div>';                    
                    html += '</div>';
                    html += '</div>';
                    count++;
                    if (count == 4) {
                        return false;
                    }

                    $(".RecentPost").append(html);
                    getImage(item.featured_media);
                });
            } else {

            }
        }
    });

}

function getImage(featured_media) {
    var imgurl = "";
    var absPath = $('#absPath').val();    
    if (featured_media != 0) {
        $.ajax({
            url: absPath + 'blog/wp-json/wp/v2/media/' + featured_media,
            method: "GET",
            data: "",
            success: function (response) {
                imgurl = response.guid.rendered;
                $('.fiid_' + featured_media).attr("src", imgurl);
            }
        });
    } else {
        imgurl = absPath + "images/noimg.png";
        $('.fiid_' + featured_media).attr("src", imgurl);
    }
}
