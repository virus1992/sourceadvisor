var meeting_data = '';
var mentor_timezone = '';

$("#giveReview").on('hidden.bs.modal', function () {
    $("#oppsiteUser").val("");
    $(".feedbackbtn").removeClass('delElement');
    $(".mtu").removeClass("starRating");
    $(".utm").removeClass("starRating");
    $('.fa').addClass('fa-star-o').removeClass('fa-star');

    $("#stars_U_M_1 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_2 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_3 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_4 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_5 span").first().removeClass("fa-star-o").addClass("fa-star");

    $("#stars_M_U_1 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_M_U_2 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_M_U_3 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_overAll span").first().removeClass("fa-star-o").addClass("fa-star");
});

$(document).ready(function () {
    /* Code for star */

    $('#noclick').css('pointerEvents', 'none');

    $(".starrr").starrr({
        rating: 1
    });

    $('.starrr').on('starrr:change', function (e, value) {
        setAvg();
    });

    $("#feedback").on('keyup', function () {
        var feedback = $(this).val();

        if (feedback != "") {
            $("#submitreview").attr("disabled", false);
        } else {
            $("#submitreview").attr("disabled", true);
        }

    });

    $("#submitreview").on("click", function () {

        $('#stars_overAll').html('');

        var objRating = $('.starRating');
        var divider = 0;
        var totalOptions = 0;
        var starRatingData = [];
        starRatingData[0] = 0;

        if (objRating.length > 0) {
            totalOptions = objRating.length;
            $.each(objRating, function (index, element) {
                var thisRating = $(element);
                var indexing = thisRating.attr('data-index');
                var starElement = thisRating.find('.fa-star');
                var thisRatingValue = parseInt(starElement.length);

                starRatingData[indexing] = thisRatingValue;
                if (thisRatingValue > 0) {
                    divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
                }
            });
        }

        var avgRating = parseInt(divider) / parseInt(totalOptions);
        avgRating = Math.round(avgRating);

        overallhtml = setOverallRatingHtml(avgRating);
        $('#stars_overAll').html(overallhtml);

        var feedback = $("#feedback").val();
        var loggedInUser = $('#loggedInUser').val();
        var opposite = $("#oppsiteUser").val();
        var meetingDetailId = $("#meetingId").val();
        var userType = $("#userType").val();
        var data = {
            user_id: opposite,
            reviewer_id: loggedInUser,
            starRatingData: starRatingData,
            review_rating: avgRating,
            review_desc: feedback,
            meetingDetailId: meetingDetailId,
            userType:userType
        };

        data = JSON.stringify(data);

        $.ajax({
            url: $('#absPath').val() + "mentor/saveReview",
            type: 'POST',
            data: "data=" + data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.message == "success") {                    
                    updateKey();
                }
            }
        });
    });

    /* Code for star */

    $(document.body).on('click', ".meetingPurpose", function () {
        var purpose = $(this).attr('data-purpose');

        $("#morePurposeSpan").text("");
        $("#morePurposeSpan").text(purpose);
    });

    $(document.body).on('click', ".moreDetails", function () {
        var moreDetail = $(this).attr('data-moreDetail');

        $("#moreDetailSpan").html("");
        $("#moreDetailSpan").html(moreDetail);
    });

});

function setAvg() {
    var overallhtml = "";

    $('#stars_overAll').html(overallhtml);

    var objRating = $('.starRating');
    var divider = 0;
    var totalOptions = 0;

    if (objRating.length > 0) {
        totalOptions = objRating.length;
        $.each(objRating, function (index, element) {
            var thisRating = $(element);
            var starElement = thisRating.find('.fa-star');
            var thisRatingValue = parseInt(starElement.length);
            if (thisRatingValue > 0) {
                divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
            }
        });
    }

    var avgRating = parseInt(divider) / parseInt(totalOptions);
    avgRating = Math.round(avgRating);

    overallhtml = setOverallRatingHtml(avgRating);

    $('#stars_overAll').html(overallhtml);
}

function setOverallRatingHtml(value) {
    var html = "";
    for (var i = 1; i <= 5; i++) {
        if (i <= value) {
            html += "<span class='fa .fa-star-o fa-star'></span>";
        } else {
            html += "<span class='fa .fa-star-o fa-star-o'></span>";
        }
    }
    return html;
}

function updateKey() {    
    var key = $("#key").val();
    var data = {key : key};
    $.ajax({
        url: $('#absPath').val() + "updateKey",
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {                
                window.location.href = $('#absPath').val() + "feedback/" + key;
            }
        }
    });
}