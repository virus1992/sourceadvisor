$(document).ready(function () {

    $('.btn-linkedin').on('click', function(){
        onLinkedInLoad();
    });

    setTimeZoneForRegisteringUser();

    $('#frmLogin').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            email: {
                required: true,
                noBlankSpace: true,
                email: true
            },
            password: {
                required: true,
                noBlankSpace: true,
                minlength: 6
            }
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Please enter Valid Email Address"
            },
            password: {
                required: "Please enter Password",
                minlength: "Password should be minimum 6 characters"
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});




function setTimeZoneForRegisteringUser() {
    var userIpAddress = $('#userIPAdd').val();
    $.getJSON("//timezoneapi.io/api/" + userIpAddress, function (data) {
        $('#hidTimeZoneLabel').val(data.data.timezone.id);
        $('#hidTimeZoneOffset').val(moment().tz(data.time_zone).utcOffset() / 60);
    });
}




// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {

    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };

        userSocialLoginHandle(dataParam);
        IN.User.logout();
    }).error(function (profileErr) {
        alert('error occured.');
    });

}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
    alert('error occured.');
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}


