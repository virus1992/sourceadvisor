$(document).ready(function () {

    chkBookMark();
    chklikeMentor();

    $("#book_now").on("click", function () {
        var id = $("#userLogin").val();
        $("#book_now").attr("data-val", "book_now");
        if (id == "") {
            $("#loginChk").modal('toggle');
        } else {
            window.location = '/users/slot_booking/' + $(this).data('slug');
        }
    });

    $("#bookmark").on("click", function () {
        var id = $("#userLogin").val();
        $("#bookmark").attr("data-val", "bookmark");
        if (id == "") {
            $("#loginChk").modal('toggle');
        } else {
            bookmarkMentor();
        }
    });

    $("#likeMentor").on("click", function () {
        var id = $("#userLogin").val();
        $("#likeMentor").attr("data-val", "likeMentor");
        if (id == "") {
            $("#loginChk").modal('toggle');
        } else {
            likeMentor();
        }
    });

    $("#msgMentor").on("click", function () {
        var id = $("#userLogin").val();
        $("#msgMentor").attr("data-val", "msgMentor");
        if (id == "") {
            $("#loginChk").modal('toggle');
        } else {
            $("#sendMsgMentor").modal('toggle');
        }
    });

    $("#sendMentorMsgBtn").on("click", function () {
        sendMentorMsg();
    });

    $('#frmLogin').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            email: {
                required: "Please enter Email Address",
                email: "Please enter Valid Email Address"
            },
            password: {
                required: "Please enter Password",
                minlength: "Password should be minimum 6 characters"
            }
        },
        submitHandler: function (form) {
            var absPath = $('#absPath').val();
            var condition = $("#bookmark").attr("data-val");
            var likecondition = $("#likeMentor").attr("data-val");
            var msgcondition = $("#msgMentor").attr("data-val");
            var mentorSlug = $("#mentorSlug").val();
            var booknow = $("#book_now").attr("data-val");

            if (msgcondition != "") {
                $.ajax({
                    url: absPath + 'loginchk',
                    method: "post",
                    data: $("#frmLogin").serialize(),
                    success: function (response) {
                        if (response.message != "success") {
                            displayAjaxNotificationMessage(response.message, "danger");
                        } else {
                            $("#loginChk").modal('toggle');
                            $("#bookmark").attr("data-val", "");
                            $("#likeMentor").attr("data-val", "");
                            $("#msgMentor").attr("data-val", "");
                            $("#book_now").attr("data-val", "");
                            //window.location = '/mentorprofile/' + mentorSlug;
                            window.location.reload();
                        }
                    }
                });
            }

            if (likecondition != "") {
                $.ajax({
                    url: absPath + 'loginchk',
                    method: "post",
                    data: $("#frmLogin").serialize(),
                    success: function (response) {
                        if (response.message != "success") {
                            displayAjaxNotificationMessage(response.message, "danger");
                        } else {
                            $("#loginChk").modal('toggle');
                            $("#bookmark").attr("data-val", "");
                            $("#likeMentor").attr("data-val", "");
                            $("#msgMentor").attr("data-val", "");
                            $("#book_now").attr("data-val", "");
                            likeMentor();
                        }
                    }
                });
            }
            if (booknow != "") {
                $.ajax({
                    url: absPath + 'loginchk',
                    method: "post",
                    data: $("#frmLogin").serialize(),
                    success: function (response) {
                        if (response.message != "success") {
                            displayAjaxNotificationMessage(response.message, "danger");
                        } else {
                            $("#loginChk").modal('toggle');
                            $("#bookmark").attr("data-val", "");
                            $("#likeMentor").attr("data-val", "");
                            $("#msgMentor").attr("data-val", "");
                            $("#book_now").attr("data-val", "");
                            window.location = '/users/slot_booking/' + $('#book_now').data('slug');
                        }
                    }
                });
            }
            if (condition == "bookmark") {
                $.ajax({
                    url: absPath + 'loginchk',
                    method: "post",
                    data: $("#frmLogin").serialize(),
                    success: function (response) {
                        if (response.message != "success") {
                            displayAjaxNotificationMessage(response.message, "danger");
                        } else {
                            $("#loginChk").modal('toggle');
                            $("#bookmark").attr("data-val", "");
                            $("#likeMentor").attr("data-val", "");
                            $("#msgMentor").attr("data-val", "");
                            $("#book_now").attr("data-val", "");
                            bookmarkMentor();
                        }
                    }
                });
            }

        }
    });
});

//function to bookmark the mentor

function bookmarkMentor() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentorId").val();
    var mentorSlug = $("#mentorSlug").val();
    var id = $("#userLogin").val();

    var data = {mentorId: mentorId};

    $.ajax({
        url: absPath + 'users/bookmarkMentor',
        method: "post",
        data: data,
        success: function (response) {
            if (response.message == "error") {
                if (id == "") {
                    window.location = '/mentorprofile/' + mentorSlug;
                }
                chkBookMark();
                displayAjaxNotificationMessage(response.message, "danger");
            } else {
                if (id == "") {
                    window.location = '/mentorprofile/' + mentorSlug;
                }
                chkBookMark();
                displayAjaxNotificationMessage(response.message, "success");
            }
        }
    });
}

// Like mentor

function likeMentor() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentorId").val();
    var mentorSlug = $("#mentorSlug").val();
    var id = $("#userLogin").val();

    var data = {mentorId: mentorId};

    $.ajax({
        url: absPath + 'users/likeMentor',
        method: "post",
        data: data,
        success: function (response) {
            if (response.message == "error") {
                if (id == "") {
                    window.location = '/mentorprofile/' + mentorSlug;
                }
                chklikeMentor();
                displayAjaxNotificationMessage(response.message, "danger");
            } else {
                if (id == "") {
                    window.location = '/mentorprofile/' + mentorSlug;
                }
                chklikeMentor();
                displayAjaxNotificationMessage(response.message, "success");
            }
        }
    });
}

//function to chk mentor is bookmarked or not

function chklikeMentor() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentorId").val();

    var data = {mentorId: mentorId};

    $.ajax({
        url: absPath + 'users/chklikeMentor',
        method: "post",
        data: data,
        success: function (response) {
            if (response.message == "found") {
                $("#likeMentor").css("background", "#177892");
                $("#likeMentor").css("width", "40px");
            } else {
                $("#likeMentor").css("background", "#333");
                $("#likeMentor").css("width", "30px");
            }
        }
    });
}

//function to chk mentor is bookmarked or not

function chkBookMark() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentorId").val();

    var data = {mentorId: mentorId};

    $.ajax({
        url: absPath + 'users/chkbookmarkMentor',
        method: "post",
        data: data,
        success: function (response) {
            if (response.message == "found") {
                $("#bookmark").css("background", "#177892");
                $("#bookmark").css("width", "40px");
            } else {
                $("#bookmark").css("background", "#333");
                $("#bookmark").css("width", "30px");
            }
        }
    });
}


// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    
    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };

        userSocialLoginHandle(dataParam);
        IN.User.logout();

    }).error(function (profileErr) {
        alert('error occured.');
    });

}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
    alert('error occured.');
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}




function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();

    var profileId = profile.getId();
    var name = profile.getName();
    var imageUrl = profile.getImageUrl();
    var email = profile.getEmail();

    var dataParams = {
        uniqueId: profileId,
        loginType: 'gp',
        name: name,
        imageUrl: imageUrl,
        email: email
    };

    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        userSocialLoginHandle(dataParams);
    });
}

function userSocialLoginHandle(dataParams) {

    var absPath = $('#absPath').val();

    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "RDTDASH") {
                    window.location = '/users/slot_booking/' + $("#book_now").data('slug');
                }
            }
        }
    });
}

function sendMentorMsg() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentorId").val();
    var msgbody = $("#msgbody").val();
    var mentorSlug = $("#mentorSlug").val();
    var id = $("#userLogin").val();

    var data = {lstUserList: mentorId, msgNewBody: msgbody};

    $.ajax({
        url: absPath + 'messages/sendUserMessages',
        method: "post",
        data: data,
        success: function (response) {
            if (response.message == "error") {
                if (id == "") {
                    window.location = '/mentorprofile/' + mentorSlug;
                }
                displayAjaxNotificationMessage("Message not sent", "danger");
            } else {

                if (response.message == "block") {
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                    $('.blockdiv label').text('You are blocked by this user.');
                    $("#blockUserMsgDetailPage").modal('toggle');
                } else if (response.message == "blocked") {
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                    $('.blockdiv label').text('Unblock this user to send message.');
                    $("#blockUserMsgDetailPage").modal('toggle');
                } else {


                    if (id == "") {
                        window.location = '/mentorprofile/' + mentorSlug;
                    }
                    displayAjaxNotificationMessage("Message sent successfully", "success");
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                }
            }
        }
    });
}