$.ajax({
    url: $('#absPath').val() + 'mentor/getMentorFullData',
    type: 'POST',
    async: true,
    beforeSend: function () {
        $.blockUI();
    },
    complete: function () {
        $.unblockUI();
    },
    success: function (response) {
        var html = '';
        console.log(response);
        if (response.flagMsg == 'USRNLOGIN') {
            alert('User session has expired. Please login and try again.');
            window.location.href = $('#absPath').val() + 'login';
            return false;
        }

        if (typeof response.data.userData.userType != "undefined" && response.data.userData.userType == 'mentor') {
            $('input[name="bcm_mentor"]').filter('[value="mentor"]').prop('checked', true);
            $('input[name="organisation_name"]').val('').parent().hide();
        } else if (typeof response.data.userData.userType != "undefined" && response.data.userData.userType == 'organisation') {
            $('input[name="bcm_mentor"]').filter('[value="organisation"]').prop('checked', true);
            $('input[name="organisation_name"]').val(response.data.mentor_details.organization_id).parent().show();
        } else {

        }
        $("input[name='bcm_mentor']").trigger('change');
    }
});
$(document).ready(function () {
    $("input[name='bcm_mentor']").change(function () {
        var value = $("input[name='bcm_mentor']:checked").val();
        if (value == 'organisation') {
            $('.mentorOrganization').show();
            $('.go-to-step-1').hide();
            $('.go-to-step-2').show();
        } else if (value == 'mentor') {
            $('.mentorOrganization').hide();
            $('.go-to-step-1').show();
            $('.go-to-step-2').hide();
        } else {
            $('.mentorOrganization').hide();
            $('.go-to-step-1').hide();
            $('.go-to-step-2').hide();
        }
    });
    $("input[name='bcm_mentor']").trigger("change");

    $('#become_a_mentor_submit_frm').validate({
        errorClass: "validate_error",
        rules: {
            bcm_mentor: {
                required: true,
                noBlankSpace: true
            },
            organisation_name: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            bcm_mentor: {
                required: 'Please select one of the options'
            },
            organisation_name: {
                required: "Please enter Organisation Name"
            }
        },
        errorPlacement: function (error, element) {
            if ($(element).attr('name') == 'bcm_mentor')
                $(element).parent().parent().find('div.validationError').append(error);
            else {
                $(element).after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI()
            var radio_value = $("input[name='bcm_mentor']:checked").val();
            if (radio_value == 'mentor') {
                form.submit();
            } else {
                $.ajax({
                    url: $('#absPath').val() + 'mentor/become_a_mentor_update',
                    method: "POST",
                    data: $('#become_a_mentor_submit_frm').serialize(),
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.message == "success") {
                            $('.open_successMentor_popup').trigger('click');
                        } else {
                            alert('Error Occured. Please try again.')
                        }
                    },
                    error: function (response) {
                        console.log(response);
                    }

                });
                return false;
            }
        }
    });
});

