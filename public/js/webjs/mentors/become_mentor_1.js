$.ajax({
    url: $('#absPath').val() + 'mentor/getMentorFullData',
    type: 'POST',
    beforeSend: function () {
        $.blockUI();
    },
    complete: function () {
        $.unblockUI();
    },
    success: function (response) {
        var html = '';
        console.log(response);
        if (response.flagMsg == 'USRNLOGIN') {
            alert('User session has expired. Please login and try again.');
            window.location.href = $('#absPath').val() + 'login';
            return false;
        }

        if (typeof response.data.mentor_details.is_compliance_passed != "undefined" && response.data.mentor_details.is_compliance_passed == '0') {
            if ($('#compliance_test_btn').length > 0) {
                $('#compliance_test_btn').show();
            }
        }

        if (typeof response.data.mentor_details.cancellation_policy_type != "undefined" && response.data.mentor_details.cancellation_policy_type != '') {
            //$('input[name="cancellation_policy_type"]').val(response.data.mentor_details.cancellation_policy_type);
            $('input[name="cancellation_policy_type"]').filter('[value="' + response.data.mentor_details.cancellation_policy_type + '"]').prop('checked', true);
        }
        if (typeof response.data.mentor_details.currency != "undefined" && response.data.mentor_details.currency != '') {
            $('#rate_currency').val(response.data.mentor_details.currency);
        }
        if (typeof response.data.mentor_details.hourly_rate != "undefined" && response.data.mentor_details.hourly_rate != '') {
            $('#hourly_rate').val(response.data.mentor_details.hourly_rate);
            $("#hourly_rate").trigger('keyup');
        }

        if (typeof response.data.userData.slug != "undefined" && response.data.userData.slug != '') {
            $('#slug').val(response.data.userData.slug);
            if ($('#view_wissenx_profile_btn').length > 0) {
                $('#view_wissenx_profile_btn').attr('href', $('#absPath').val() + 'mentorprofile/' + response.data.userData.slug).show();
            }
        }

    }
});

var userInitialConfig = {
    section: "header",
    loopClass: "photoContainer.mentor_dp",
    type: "input",
    srcClass: "imgUserProfile",
    nameClass: "hfname",
    nameHtmlClass: "imgdiv",
    imgClass: "imgUserProfile"
};
setUserInitialImage(userInitialConfig);

$(document).ready(function () {
    var userType=$('#userType').val();;
    if(datax && userType==='mentor'){
    $("#lstTimeZone").select2({
        data: datax,
        templateResult: function (d) {
            return $(d.text); },
        templateSelection: function (d) {
            var found = datax.find(function(element) {
                if(d.selected){
                    return element.id === parseInt(d.id);
                }else{
                    return element.id === parseInt(element.selectedId);
                }    
              });
              $('#lstTimeZone').val(found.id)
            return $(found.text); 
        },
        
    })
}
    $('.select2-selection__rendered').removeAttr('title');

    $("#rate_currency").change(function () {
        $('#rate_currency_text').text($(this).val());
    });
    $("#hourly_rate").numeric();
    $("#hourly_rate").on("keyup", function () {
        if ($("#hourly_rate").val() > 0) {
            var val = $("#hourly_rate").val();
            var wissenx_mentor_charge = $("#mentorCountryCharge").val();
            var perc = ((100 - wissenx_mentor_charge) / 100) * val;
            $('#hourly_rate_text').text(Math.round(perc * 100) / 100);
        } else {
            $('#hourly_rate_text').text(0);
        }
    });
    var calculatePaymentInterval = setInterval(function () {
        if (typeof dbConstant == "object") {
            $("#hourly_rate").trigger('keyup');
            clearInterval(calculatePaymentInterval);
        }
    }, 100);

    $('#step1_submit_frm').validate({
        errorClass: "validate_error",
        rules: {
            hourly_rate: {
                required: true,
                maxlength: 5,
                max: 10000,
                noDecimal: true,
                noBlankSpace: true
            },
            currency: {
                required: true,
                noBlankSpace: true
            },
            cancellation_policy_type: {
                required: true,
                noBlankSpace: true
            },
            slug: {
                required: true,
                pattern: "^[a-zA-Z0-9_]+$",
                remote: {
                    url: $('#absPath').val() + 'mentor/checkSlug',
                    type: 'POST',
                    data: {
                        slug: function () {
                            return $("#slug").val();
                        }
                    }
                }
            }
        },
        messages: {
            hourly_rate: {
                required: 'Please enter Hourly Rate'
            },
            currency: {
                required: 'Please select Currency'
            },
            cancellation_policy_type: {
                required: "Please select Cancellation Policy Type"
            },
            slug: {
                required: "Please enter Slug",
                pattern: "Only Alphabets, Numbers and Underscores  are allowed",
                remote: "Slug is already registered with another Mentor"
            }
        },
        errorPlacement: function (error, element) {
            if ($(element).attr('name') == 'cancellation_policy_type') {
                $(element).parent().parent().parent().append(error);
            } else if ($(element).attr('id') == 'slug') {
                $(element).parent().after(error);
            } else {
                $(element).after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            //if ($(document.activeElement).hasClass('ajaxsubmit_commercialdetail')) {
            $(form).ajaxSubmit({
                success: function (responseText, statusText, xhr, $form) {

                    if (typeof becomeAMentorPage != 'undefined' && becomeAMentorPage == "1") {
                        gotoNextTab();
                        $.unblockUI();
                    } else {
                        $.unblockUI();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        displayAjaxNotificationMessage(responseText.message, 'success');
                    }
                }
            });
            // } else {
            //     form.submit();
            // }
        }
    });
});
