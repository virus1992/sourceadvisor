var expertise_tabledata = '';
var employmenthistory_tabledata = '';
var isSaveAndAddMore = false;
$.ajax({
    url: $('#absPath').val() + 'mentor/getMentorFullData',
    type: 'POST',
    beforeSend: function () {
        $.blockUI();
    },
    complete: function () {
        $.unblockUI();
    },
    success: function (response) {
        var html = '';
        console.log(response);
        if (response.flagMsg == 'USRNLOGIN') {
            alert('User session has expired. Please login and try again.');
            window.location.href = $('#absPath').val() + 'login';
            return false;
        }

        var mentorEmpHistory = response.data.mentorEmpHistory;
        var mentorExpertise = response.data.mentorExpertise;
        var mentor_details = response.data.mentor_details;
        var mentor_tags = response.data.mentorOrgTags;

        employmenthistory_tabledata = $('.employmenthistory_table').DataTable({"lengthMenu": [5, 10, 25]});
        if (mentorEmpHistory.length > 0) {
            $('#check_emp_his').val(mentorEmpHistory.length);
            $.each(mentorEmpHistory, function (index, item) {
                addEmpHisRow(item, 'add');
            });
        } else {
            $('#check_emp_his').val('');
        }

        // expertise_tabledata = $('.expertise_table').DataTable({"lengthMenu": [5, 10, 25]});
        // if (mentorExpertise.length > 0) {
        //     var industryId = "";
        //     $.each(mentorExpertise, function (index, item) {
        //         industryId += item.industry + ",";
        //         addEmpExpRow(item, 'add');
        //     });
        //     industryId = industryId.replace(/,+$/, '');
        //     $('#check_expertise_ids').val(industryId);
        //     $('#check_expertise').val(mentorExpertise.length);

        // } else {
        //     $('#check_expertise').val('');
        //     $('#check_expertise_ids').val('');
        // }


        expertise_tabledata = $('.expertise_table').DataTable({"lengthMenu": [5, 10, 25]});
        if (mentorExpertise.length > 0) {
            //$('#check_expertise').val(mentorExpertise.length);
            $.each(mentorExpertise, function (index, item) {
                addEmpExpRow(item, 'add');
            });
        } else {
            $('#check_expertise').val('');
        }


        if (typeof mentor_details.keynote_id != "undefined" && mentor_details.keynote_id != "0") {
            $('.keynote_option').hide();
            $('.keynote_option_final').show();
            $('.video_container').html('');

            if (mentor_details.category == 'keynote_youtube') {
                $('<iframe>', {
                    src: "https://www.youtube.com/embed/" + mentor_details.document_name + '?rel=0&controls=0',
                    id: 'youtube_embed',
                    frameborder: 0,
                    scrolling: 'no',
                    controls: 0
                }).prependTo('.video_container');
                var keynoteTitle = '<span class="keynotetitle">Title: <label>' + mentor_details.description + '</label></span>';
                //$('#btnNextKeyNote').removeClass('hidden');
                $('.video_container').prepend(keynoteTitle);
            } else if (mentor_details.category == 'keynote_upload') {
                var html = '<span class="keynotetitle">Title: <label>' + mentor_details.description + '</label></span>' +
                        '<video id="example_video_' + mentor_details.document_id + '" class="video-js vjs-default-skin"' +
                        'controls preload="auto" width="640" height="264"' +
                        'data-setup=\'{"example_option":true}\'>' +
                        '<source src="' + $('#absPath').val() + 'keyNoteVideo/' + mentor_details.document_name + '" type="' + mentor_details.document_mime + '" />' +
                        '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                        '</video>';
                $('.video_container').html(html);
                videojs('example_video_' + mentor_details.document_id, {}, function () {});
            } else if (mentor_details.category == 'keynote_record') {

            }
        }

        if (typeof mentor_details.profession != "undefined" && mentor_details.profession != '') {
            $('#profession').val(mentor_details.profession);
        }
        if (typeof mentor_details.job_description != "undefined" && mentor_details.job_description != '') {
            $('#job_description').val(mentor_details.job_description);
        }
        if (typeof mentor_details.employment_type != "undefined" && mentor_details.employment_type != '') {
            $('#employment_type').val(mentor_details.employment_type);
        }
        if (typeof mentor_details.company_name != "undefined" && mentor_details.company_name != '') {
            $('#company_name').val(mentor_details.company_name);
            $('#employment_type').trigger('change');
        } else {
            $('#employment_type').trigger('change');
        }
        if (typeof mentor_details.punchline != "undefined" && mentor_details.punchline != '') {
            $('#punchline').val(mentor_details.punchline);
        }
        if (typeof mentor_details.overview != "undefined" && mentor_details.overview != '') {
            var whenCreatedInstance = setInterval(function () {
                if (typeof CKEDITOR.instances['overview'] != "undefined") {
                    CKEDITOR.instances['overview'].setData(mentor_details.overview);
                    CKEDITOR.instances['overview'].updateElement();
                    clearInterval(whenCreatedInstance);
                }
            }, 500);
        }

        var whenTagsLoaded = setInterval(function () {
            if (typeof $.tags == "function") {
                clearInterval(whenTagsLoaded);
                if (mentor_tags != null) {
                    if (mentor_tags.length > 0) {
                        tags = $("#maxNumTags").tags({
                            maxNumTags: 10,
                            tagData: mentor_tags,
                            afterAddingTag: function (tag) {
                                updateOrganizationTag(tag, 'add');
                            },
                            beforeDeletingTag: function (tag) {
                                if (!confirm('Are you sure you want to delete this tag.')) {
                                    return false;
                                }
                            },
                            afterDeletingTag: function (tag) {
                                updateOrganizationTag(tag, 'delete');
                            }
                        });
                    } else {
                        tags = $("#maxNumTags").tags({
                            maxNumTags: 10,
                            afterAddingTag: function (tag) {
                                updateOrganizationTag(tag, 'add');
                            },
                            beforeDeletingTag: function (tag) {
                                if (!confirm('Are you sure you want to delete this tag.')) {
                                    return false;
                                }
                            },
                            afterDeletingTag: function (tag) {
                                updateOrganizationTag(tag, 'delete');
                            }
                        });
                    }
                } else {
                    tags = $("#maxNumTags").tags({
                        maxNumTags: 10,
                        afterAddingTag: function (tag) {
                            updateOrganizationTag(tag, 'add');
                        },
                        beforeDeletingTag: function (tag) {
                            if (!confirm('Are you sure you want to delete this tag.')) {
                                return false;
                            }
                        },
                        afterDeletingTag: function (tag) {
                            updateOrganizationTag(tag, 'delete');
                        }
                    });
                }
            }
        }, 500);
    }
});

var player = '';

function trackSaveButton(formname, isClosePopup) {
    isSaveAndAddMore = isClosePopup;
    //$("#"+formname).parents('form').submit();
    $("#" + formname).submit();

}

function trackSaveButtonB(formname, isClosePopup) {
    isSaveAndAddMore = isClosePopup;
    //$("#"+formname).parents('form').submit();
    $("#" + formname).submit();

}
$(document).ready(function () {
    $('#employment_type, #emp_his_self_emp').change(function () {
        if ($(this).val() == 'self_employed') {
            if ($(this).attr('id') == "employment_type") {
                $('#company_name').attr('disabled', true).val('');
            } else if ($(this).attr('id') == "emp_his_self_emp") {
                $('#emp_his_comp_name').attr('disabled', true).val('');
            }
        } else {
            if ($(this).attr('id') == "employment_type") {
                $('#company_name').attr('disabled', false);
            } else if ($(this).attr('id') == "emp_his_self_emp") {
                $('#emp_his_comp_name').attr('disabled', false);
            }
        }
    });

    $('#employment_type').trigger('change');

    $('.keynote_option > a').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('recorded_video')) {
            $('#addRecordedVideo').modal('show');
        } else if ($(this).hasClass('youtube_video')) {
            $('#addYoutubeVideo').modal('show');
            //$('#btnNextKeyNote').removeClass('hidden');
        } else if ($(this).hasClass('uploaded_video')) {
            $('#addUploadVideo').modal('show');
        }
        return false;
    });

    $('#expertise_industry').change(function () {
        var industry_id = $(this).val();
        $('#expertise_sub_industry').val('');
        if (industry_id == "" || typeof industry_id == "undefined") {
            $('#expertise_sub_industry').attr('disabled', true);
        } else {
            $('#expertise_sub_industry').attr('disabled', false);
        }
        $('#expertise_sub_industry option').each(function (index, item) {
            if ($(item).attr('data-industry-id') == industry_id || typeof $(item).attr('data-industry-id') == "undefined") {
                if ($(item).parent().is("span")) {
                    $(item).unwrap();
                }
            } else {
                // $(item).hide();
                if (!$(item).parent().is("span")) {
                    $(item).wrap('<span style="display:none"></span>');
                }
            }
        });
    });

    $('#expertise_region').change(function () {
        var region_id = $(this).val();
        $('#expertise_sub_region').val('');

        if (region_id == "" || typeof region_id == "undefined") {
            $('#expertise_sub_region').attr('disabled', true);
        } else {
            $('#expertise_sub_region').attr('disabled', false);
        }

        if (region_id == 1) {
            $('#expertise_sub_region').attr('disabled', true);
            $('#expertise_sub_region option[value!=""]').hide();
        } else {
            $('#expertise_sub_region option').each(function (index, item) {
                if ($(item).attr('data-region-id') == region_id || typeof $(item).attr('data-region-id') == "undefined") {
                    if ($(item).parent().is("span")) {
                        $(item).unwrap();
                    }
                } else {
                    if (!$(item).parent().is("span")) {
                        $(item).wrap('<span style="display:none"></span>');
                    }
                    //$(item).wrap('<span style="display:none"></span>');
                }
            });
        }
    });

    $('#expertise_industry, #expertise_region').trigger("change");

    $('button.sanchit').click(function () {
        onClickConnectLinkedIn();
    });

    $("#addYoutubeVideo").on('hide.bs.modal', function () {
        $("#ytvalid").css("display", "none");
        $(".lblHint").css("display", "none");
    });

    $("#addEmployment, #addExpertise").on('hide.bs.modal', function () {
        if ($(this).find('form').attr('id') == 'addExpertise_form') {
            $('.savemore').show();
            $('#expertise_sub_industry').attr('disabled', true);
            //$('#expertise_sub_industry option[value!=""]').hide();
            $('#expertise_sub_region').attr('disabled', true);
            //$('#expertise_sub_region option[value!=""]').hide();
            addExpertise_form.resetForm();
            $('#expertiseModalTitle').text('Add an');
            $('#expertise_id').val(0);
        } else if ($(this).find('form').attr('id') == 'add_emp_his_form') {
            $('.savemore').show();
            add_emp_his_form.resetForm();
            $('#emp_his_dur_to').attr('disabled', false);
            $('#employment_id').val(0);
            $('#employmentModalTitle').text('Add');
        } else {
            $(this).find('form input').val('');
        }
    });

    $('button[data-target="#addExpertise"]').on("click", function (e) {
        e.stopPropagation();
        if ($('#check_expertise').rules().max > $('#check_expertise').val()) {

            $('#expertise_industry option').each(function (index, item) {
                //$(item).show();
                if ($(item).parent().is("span")) {
                    $(item).unwrap();
                }
            });

            $('#addExpertise').modal('show');
        } else {
            var existingExpertiseIds = $('#check_expertise_ids').val().split(',');
            $('#expertise_industry option').each(function (index, item) {
                var itemval = $.inArray($(item).val(), existingExpertiseIds);
                if (itemval > -1) {
                    if ($(item).parent().is("span")) {
                        $(item).unwrap();
                    }
                    //$(item).show();
                } else {
                    //$(item).hide();
                    if (!$(item).parent().is("span")) {
                        $(item).wrap('<span style="display:none"></span>');
                    }
                }
            });
            alert('Please delete one industry if you wish to add a new industry to your expertise');
            $('#addExpertise').modal('show');
        }
    });

    /*
     player = videojs("myVideo", {
     controls: true,
     controlBar: {
     volumeMenuButton: true,
     fullscreenToggle: false
     },
     loop: false,
     width: 400,
     height: 300,
     plugins: {
     record: {
     image: false,
     audio: true,
     video: {
     mandatory: {
     minWidth: 1280,
     minHeight: 720
     }
     },
     frameWidth: 1280,
     frameHeight: 720,
     maxLength: 300,
     debug: true
     }
     }
     });
     player.on('startRecord', function () {
     $('.progress-bar').text('0%');
     $('.progress-bar').width('0%');
     });
     player.on('stopRecord', function () {
     $.blockUI();
     });
     player.on('finishRecord', function () {
     var audioFile = player.recordedData;
     var filesList = [audioFile];
     console.log("recordedData", filesList);
     var files = filesList;
     if (files.length > 0) {
     var formData = new FormData();
     $.each(files[0], function (index, item) {
     formData.append('uploads[]', item, item.name);
     });
     uploadRecordedVideoAjax(formData);
     }
     });
     */

    function uploadRecordedVideoAjax(formData) {
        $.ajax({
            url: $('#absPath').val() + 'mentor/mentorRecordKeynoteVideo',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log('upload successful!\n' + data);
            },
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener('progress', function (evt) {
                    if (evt.lengthComputable) {
                        $.unblockUI();
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.progress-bar').text(percentComplete + '%');
                        $('.progress-bar').width(percentComplete + '%');
                        if (percentComplete === 100) {
                            $('.progress-bar').html('Done');
                        }
                    }
                }, false);
                return xhr;
            }
        });
    }

    $('#btnNextKeyNote').on('click', function () {
        gotoNextTab();
    });

    $('#btnNextEmployement').on('click', function () {
        gotoNextTab();
    });

    var add_keynote_youtube = $('#add_keynote_youtube').validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
            youtube_embed: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            youtube_embed: {
                required: 'Please enter Youtube Embed Link'
            }
        },
        submitHandler: function (form) {
            var url = $(form).find('input[name="youtube_embed"]').val();
            var db_url = '';
            if (url.search('watch') != -1) {
                db_url = url.split('v=')[1];
                db_url = db_url.split('&')[0];
                $("#ytvalid").css('display', 'none');
            } else if (url.search('youtu.be') != -1) {
                db_url = url.split('youtu.be/')[1];
                db_url = db_url.split('?')[0];
                $("#ytvalid").css('display', 'none');
            } else if (url.search('embed') != -1) {
                db_url = url.split('embed/')[1];
                db_url = db_url.split('"')[0];
                db_url = db_url.split('?')[0];
                $("#ytvalid").css('display', 'none');
            } else {
                $("#ytvalid").css('display', 'block');
                return false;
            }

            $.ajax({
                url: $('#absPath').val() + 'mentor/addKeynoteYoutube',
                method: 'POST',
                data: {
                    keynote_type: 'youtube',
                    youtube_embed: db_url,
                    keynote_title: $(form).find('input[name="keynote_title"]').val()
                },
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('.keynote_option').hide();
                    $('.keynote_option_final').show();
                    $('.video_container').html('');
                    $('<iframe>', {
                        src: "https://www.youtube.com/embed/" + response.data.document_name + '?rel=0&controls=0',
                        id: 'youtube_embed',
                        frameborder: 0,
                        scrolling: 'no',
                        controls: 0
                    }).prependTo('.video_container');
                    var keynoteTitle = '<span class="keynotetitle">Title: <label>' + response.data.description + '</label></span>';
                    $('.video_container').prepend(keynoteTitle);
                    $('#addYoutubeVideo').modal('hide');
                    $(form)[0].reset();
                    $(form).find('input:hidden').val('youtube');
                    $('#btnNextKeyNote').removeClass('hidden');
                }
            });
        }
    });

    var add_keynote_record = $('#add_keynote_record').validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
            youtube_embed: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            youtube_embed: {
                required: 'Please enter Youtube Embed Link'
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: $('#absPath').val() + 'mentor/addKeynoteRecord',
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('.keynote_option').hide();
                    $('<iframe>', {
                        src: response.data + '?rel=0&controls=0',
                        id: 'youtube_embed',
                        frameborder: 0,
                        scrolling: 'no',
                        controls: 0
                    }).prependTo('.keynote_option_final');
                    $('#addYoutubeVideo').modal('hide');
                }
            });
        }
    });

    var add_keynote_upload = $('#add_keynote_upload').validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
            upload_video: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            upload_video: {
                required: 'Please select Video to Upload'
            }
        },
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                url: $('#absPath').val() + 'mentor/addKeynoteUpload',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                beforeSubmit: function (formData) {
                    var queryString = $.param(formData);
                    return true;
                },
                success: function (response) {
                    $('.keynote_option').hide();
                    var html = '<span class="keynotetitle">Title: <label>' + response.data.description + '</label></span>' +
                            '<video id="example_video_' + response.data.document_id + '" class="video-js vjs-default-skin"' +
                            'controls preload="auto" width="640" height="264">' +
                            '<source src="' + $('#absPath').val() + 'keyNoteVideo/' + response.data.document_name + '" type="' + response.data.document_mime + '" />' +
                            '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                            '</video>';
                    $('.video_container').html(html);
                    videojs('example_video_' + response.data.document_id, {}, function () {});
                    $('.keynote_option_final').show();
                    $('#addUploadVideo').modal('hide');
                    $(form)[0].reset();
                    $(form).find('input:hidden').val('upload')
                }
            });
        }
    });


    var step2_submit_frm = $('#step2_submit_frm').validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
//            check_emp_his: {
//                required: true,
//                noBlankSpace: true
//            },
            check_expertise: {
                required: true,
                noBlankSpace: true,
                max: 4
            },
//            profession: {
//                required: true,
//                noBlankSpace: true
//            },
//            job_description: {
//                required: true,
//                noBlankSpace: true
//            },
//            employment_type: {
//                required: true,
//                noBlankSpace: true
//            },
//            company_name: {
//                required: true,
//                noBlankSpace: true
//            },
//            punchline: {
//                required: true,
//                noBlankSpace: true
//            },
//            overview: {
//                required: function () {
//                    CKEDITOR.instances.overview.updateElement();
//                },
//                noBlankSpace: true
//            },
//            location_country: {
//                required: true,
//                noBlankSpace: true
//            },
//            location_city: {
//                required: true,
//                noBlankSpace: true
//            }
        },
        messages: {
            check_emp_his: {
                required: 'Please add atleast one Employment History'
            },
            check_expertise: {
                required: 'Please add atleast one Expertise',
                max: 'Please delete one industry if you wish to add another industry to your expertise'
            },
            profession: {
                required: 'Please enter your Role'
            },
            job_description: {
                required: 'Please enter Job Description'
            },
            employment_type: {
                required: 'Please select Employment Type'
            },
            company_name: {
                required: 'Please enter Company Name'
            },
            punchline: {
                required: 'Please enter Punchline'
            },
            overview: {
                required: 'Please enter Overview'
            },
//            location_country: {
//                required: 'Please select Location Country'
//            },
//            location_city: {
//                required: 'Please enter Location City'
//            }
        },
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'check_emp_his') {
                $('.' + $(element).attr('id')).after(error);
            } else if ($(element).attr('id') == 'check_expertise') {
                $('.' + $(element).attr('id')).after(error);
            } else {
                $(element).after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'mentor/updateUserToMentor',
                type: 'GET',
                success: function (response) {
                    if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                        displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                    }

                    if (response['error'] == "1") {
                        displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                    } else {
                        if (response['flagMsg'] == "SUCCESS") {
                            gotoNextTab();
                            //displayAjaxNotificationMessage('adfad', 'success');
                            $.unblockUI();
                        }
                    }
                }
            });
            // if ($(document.activeElement).hasClass('ajaxsubmit_professionaldetail')) {
            //     CKEDITOR.instances['overview'].updateElement();
            //     $(form).ajaxSubmit({
            //         success: function (responseText, statusText, xhr, $form) {
            //             if(typeof becomeAMentorPage != 'undefined' && becomeAMentorPage == "1") {
            //                 gotoNextTab();
            //                 $.unblockUI();
            //             }
            //             else {
            //                 $.unblockUI();
            //                 $("html, body").animate({scrollTop: 0}, "slow");
            //                 displayAjaxNotificationMessage(responseText.message, 'success');
            //             }
            //         }
            //     });
            // } else {
            //     form.submit();
            // }
        }
    });


    var step2_submit_frm_Profession = $('#step2_submit_frm_Profession').validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
//            check_emp_his: {
//                required: true,
//                noBlankSpace: true
//            },
            // check_expertise: {
            //     required: true,
            //     noBlankSpace: true,
            //     max: 4
            // },
//            profession: {
//                required: true,
//                noBlankSpace: true
//            },
//            job_description: {
//                required: true,
//                noBlankSpace: true
//            },
//            employment_type: {
//                required: true,
//                noBlankSpace: true
//            },
//            company_name: {
//                required: true,
//                noBlankSpace: true
//            },
//            punchline: {
//                required: true,
//                noBlankSpace: true
//            },
//            overview: {
//                required: function () {
//                    CKEDITOR.instances.overview.updateElement();
//                },
//                noBlankSpace: true
//            },
//            location_country: {
//                required: true,
//                noBlankSpace: true
//            },
//            location_city: {
//                required: true,
//                noBlankSpace: true
//            }
        },
        messages: {
            check_emp_his: {
                required: 'Please add atleast one Employment History'
            },
            check_expertise: {
                required: 'Please add atleast one Expertise',
                max: 'Please delete one industry if you wish to add another industry to your expertise'
            },
            profession: {
                required: 'Please enter your Role'
            },
            job_description: {
                required: 'Please enter Job Description'
            },
            employment_type: {
                required: 'Please select Employment Type'
            },
            company_name: {
                required: 'Please enter Company Name'
            },
            punchline: {
                required: 'Please enter Punchline'
            },
            overview: {
                required: 'Please enter Overview'
            },
//            location_country: {
//                required: 'Please select Location Country'
//            },
//            location_city: {
//                required: 'Please enter Location City'
//            }
        },
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'check_emp_his') {
                $('.' + $(element).attr('id')).after(error);
            } else if ($(element).attr('id') == 'check_expertise') {
                $('.' + $(element).attr('id')).after(error);
            } else {
                $(element).after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            CKEDITOR.instances['overview'].updateElement();
            $(form).ajaxSubmit({
                success: function (responseText, statusText, xhr, $form) {
                    if (typeof becomeAMentorPage != 'undefined' && becomeAMentorPage == "1") {
                        gotoNextTab();
                        $.unblockUI();
                    } else {
                        $.unblockUI();
                        $("html, body").animate({scrollTop: 0}, "slow");
                        displayAjaxNotificationMessage(responseText.message, 'success');
                    }
                }
            });
        }
    });

    var addExpertise_form = $('#addExpertise_form').validate({
        rules: {
            expertise_industry: {
                required: true,
                noBlankSpace: true
            },
            expertise_sub_industry: {
                required: true,
                noBlankSpace: true
            },
            expertise_domain: {
                required: true,
                noBlankSpace: true
            },
            expertise_region: {
                required: true,
                noBlankSpace: true
            },
            expertise_sub_region: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            expertise_industry: {
                required: "Please select Industry"
            },
            expertise_sub_industry: {
                required: "Please select Sub-Industry"
            },
            expertise_domain: {
                required: "Please select Domain"
            },
            expertise_region: {
                required: "Please select Region"
            },
            expertise_sub_region: {
                required: "Please select Sub-Region"
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    var html = '';
                    if (response.message == 'add_success') {
                        html = addEmpExpRow(response.data, 'add');
                        //$('.employment_expertise table tbody').prepend(html);
                        updateCheckExpertiseValue(response.data);
                        if ($('#check_expertise').val() == 4) {
                            $('#addExpertise').modal('hide');
                        }
                        $('#check_expertise').valid();
                    } else if (response.message == 'update_success') {
                        html = addEmpExpRow(response.data, 'replace');
                        //$('.employment_expertise table tbody tr.emp_exp_' + response.data.mentor_expertise_id).replaceWith(html);
                        updateCheckExpertiseValue(response.data);
                        if ($('#check_expertise').val() == 4) {
                            $('#addExpertise').modal('hide');
                        }
                    }
                    //if ($(document.activeElement).hasClass('savemore')) {
                    if (isSaveAndAddMore) {
                        addExpertise_form.resetForm();
                        $('#emp_his_dur_to').attr('disabled', false);
                        $('#expertise_industry').change();
                        $('#expertise_region').change();
                    } else {
                        $('#addExpertise button.close').trigger('click');
                    }
                }
            });
        }
    });

    var add_emp_his_form = $('#add_emp_his_form').validate({
        ignore: [':disabled'],
        rules: {
            emp_his_role: {
                required: true,
                noBlankSpace: true
            },
            /* emp_his_job_func: {
             required: true,
             noBlankSpace: true
             },*/
            emp_his_self_emp: {
                required: true,
                noBlankSpace: true
            },
            emp_his_comp_name: {
                required: true,
                noBlankSpace: true
            },
            emp_his_dur_from: {
                required: true,
                dateMY: true,
                noBlankSpace: true,
                lessThanToday: true
            },
            emp_his_dur_to: {
                required: true,
                dateMY: true,
                noBlankSpace: true,
                greaterThan: ["#emp_his_dur_from", "Duration From"]
            },
            emp_his_job_desc: {
                required: function () {
                    CKEDITOR.instances.emp_his_job_desc.updateElement();
                }
            }
        },
        messages: {
            emp_his_role: {
                required: "Please enter role / designation"
            },
            /* emp_his_job_func: {
             required: "Please enter Job Function"
             },*/
            emp_his_self_emp: {
                required: "Please select employment status"
            },
            emp_his_comp_name: {
                required: "Please enter company name"
            },
            emp_his_dur_from: {
                required: "Please specify duration from",
                lessThanToday: "From date should be less than current"
            },
            emp_his_dur_to: {
                required: "Please specify duration to"
            },
            emp_his_job_desc: {
                required: "Please enter job description"
            }
        },
        submitHandler: function (form) {
            $.ajax({
                url: form.action,
                type: form.method,
                data: $(form).serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    var html = '';
                    if (response.message == 'add_success') {
                        html = addEmpHisRow(response.data, 'add');
                        //$('.employment_history table tbody').prepend(html);
                        if ($('#check_emp_his').val() == "") {
                            $('#check_emp_his').val(1);
                        } else {
                            $('#check_emp_his').val(parseInt($('#check_emp_his').val()) + 1);
                        }
                        $('#check_emp_his').valid();
                    } else if (response.message == 'update_success') {
                        html = addEmpHisRow(response.data, 'replace');
                        //$('.employment_history table tbody tr.emp_his_' + response.data.mentor_employment_history_id).replaceWith(html);
                    }
                    //if ($(document.activeElement).hasClass('savemore')) {
                    if (isSaveAndAddMore) {
                        add_emp_his_form.resetForm();
                        CKEDITOR.instances['emp_his_job_desc'].setData('');
                    } else {
                        $('#addEmployment button.close').trigger('click');
                        CKEDITOR.instances['emp_his_job_desc'].setData('');
                    }
                }
            });
        }
    });

    $('#emp_his_dur_present').on('change', function () {
        if ($(this).is(':checked')) {
            $('#emp_his_dur_to').attr('disabled', true);
            $('#emp_his_dur_to').attr('placeholder', 'Present').val('');
        } else {
            $('#emp_his_dur_to').attr('disabled', false);
            $('#emp_his_dur_to').attr('placeholder', 'MM/YYYY');
        }
    });

    $(document.body).on("click", "a.edit", function (e) {
        e.preventDefault();
        $('.savemore').hide();
        if ($(this).hasClass('emp_expertise')) {
            var expertiseId = $(this).attr('data-id');
            $.ajax({
                url: $('#absPath').val() + 'mentor/getMentorExpertise',
                data: 'expertise_id=' + expertiseId,
                type: 'POST',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == 'success') {
                        var data = response.data.mentorExpertise[0];
                        $('button[data-target="#addExpertise"]').trigger('click');
                        $('#expertiseModalTitle').text('Edit');
                        $('#expertise_id').val(data.mentor_expertise_id);
                        $('#expertise_industry').val(data.industry);
                        $('#expertise_industry').change();
                        $('#expertise_sub_industry').val(data.sub_industry);
                        $('#expertise_domain').val(data.domain);
                        $('#expertise_region').val(data.region);
                        $('#expertise_region').change();
                        $('#expertise_sub_region').val(data.sub_region);
                    } else {
                        displayAjaxNotificationMessage('Error Occured. Please Try Again', 'danger');
                    }
                }
            });
        } else if ($(this).hasClass('emp_history')) {
            var empHisId = $(this).attr('data-id');
            $.ajax({
                url: $('#absPath').val() + 'mentor/getMentorEmpHistory',
                data: 'emp_history_id=' + empHisId,
                type: 'POST',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == 'success') {
                        var data = response.data.mentorEmpHistory[0];
                        $('button[data-target="#addEmployment"]').trigger('click');
                        $('#employmentModalTitle').text('Edit');
                        $('#employment_id').val(data.mentor_employment_history_id);
                        $('#emp_his_role').val(data.role);
                        $('#emp_his_job_func').val(data.job_function);
                        $('#emp_his_self_emp').val(data.employment_status);
                        $('#emp_his_comp_name').val(data.company_name);
                        $('#emp_his_dur_from').val(data.durationFrom);

                        $('#emp_his_self_emp').trigger('change');

                        if (data.is_current == "1") {
                            $('#emp_his_dur_to').val("");
                            $('#emp_his_dur_to').attr("placeholder", "Present");
                            $('#emp_his_dur_to').attr("disabled", true);
                            $('#emp_his_dur_present').attr('checked', true);
                        } else {
                            $('#emp_his_dur_to').val(data.durationTo);
                            $('#emp_his_dur_to').attr("placeholder", "MM/YYYY");
                            $('#emp_his_dur_to').attr("disabled", false);
                            $('#emp_his_dur_present').attr('checked', false);
                        }
                        CKEDITOR.instances['emp_his_job_desc'].setData(data.job_description);
                    } else {
                        displayAjaxNotificationMessage('Error Occured. Please Try Again', 'danger');
                    }
                }
            });
        }
    });

    $(document.body).on("click", "a.remove_emp_data", function (e) {
        e.preventDefault();
        if (confirm("Are you sure you want to delete this record?") == false) {
            return;
        }
        if ($(this).hasClass('emp_expertise')) {
            var expertiseId = $(this).attr('data-id');
            var industry_id = $(this).attr('data-industry-id');
            var removeTag = this;
            $.ajax({
                url: $('#absPath').val() + 'mentor/deleteMentorExpertise',
                data: 'expertise_id=' + expertiseId + '&industry_id=' + industry_id,
                type: 'POST',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == 'success') {
                        displayAjaxNotificationMessage('Expertise Deleted Successfully', 'success');
                        //$(removeTag).parents('tr').remove();

                        expertise_tabledata.row($(removeTag).parents('tr')).remove().draw();

                        updateCheckExpertiseValue({delete: true, industry: industry_id});
                    } else {
                        displayAjaxNotificationMessage('Error Occured. Please Try Again', 'danger');
                    }
                }
            });
        } else if ($(this).hasClass('emp_history')) {
            var empHisId = $(this).attr('data-id');
            var removeTag = this;
            $.ajax({
                url: $('#absPath').val() + 'mentor/deleteMentorEmpHistory',
                data: 'emp_history_id=' + empHisId,
                type: 'POST',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == 'success') {
                        displayAjaxNotificationMessage('Employment History Deleted Successfully', 'success');
                        //$(removeTag).parents('tr').remove();
                        employmenthistory_tabledata.row($(removeTag).parents('tr')).remove().draw();
                        $('#check_emp_his').val(($('#check_emp_his').val() - 1) == 0 ? '' : ($('#check_emp_his').val() - 1));
                    } else {
                        displayAjaxNotificationMessage('Error Occured. Please Try Again', 'danger');
                    }
                }
            });
        }
    });

    $(document.body).on("click", "a.remove_keynote", function (e) {
        e.preventDefault();
        if (confirm("Are you sure you want to remove your Keynote Video?") == true) {
            $.ajax({
                url: $('#absPath').val() + 'mentor/removeKeynoteVideo',
                type: 'POST',
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('.video_container').html('');
                    $('.keynote_option_final').hide();
                    $('.keynote_option').show();
                    //$('#btnNextKeyNote').addClass('hidden');
                }
            });
        }
    });
    $('#next_button').show();
});

function onClickConnectLinkedIn() {
    $.ajax({
        url: $('#absPath').val() + 'mentor/get_mentor_linkedin_data',
        method: "POST",
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == 'success') {
                $('#profession').val();
                $('#job_description').val();
                $('#employment_type').val();
                $('#company_name').val();
                $('#punchline').val();
                $('#overview').val();
                $('#location_country').val();
                $('#location_city').val();
            } else if (response.message == 'no_data_fround') {
                onLinkedInLoad();
            }
        },
        error: function (response) {

        }

    });
}

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };
        userSocialLoginHandle(dataParam);
        IN.User.logout();
    }).error(function (profileErr) {
        alert('error occured.');
    });
}

// Handle an error response from the API call
function onError(error) {
    alert('error occured.');
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}

// Generate Mentor Employment History table html
function addEmpHisRow(data, addToDom) {
    var html = '<tr class="emp_his_' + data.mentor_employment_history_id + '">';
    html += '<td>' + data.role + '</td>';
    html += '<td>' + data.job_function + '</td>';
    html += '<td>' + data.employment_status.replace(/_/g, ' ').charAt(0).toUpperCase() + data.employment_status.replace(/_/g, ' ').slice(1) + '</td>';
    html += '<td>' + data.company_name + '</td>';
    html += '<td>' + data.durationFrom + ' - ' + (data.is_current == "1" ? "Present" : data.durationTo) + '</td>';
    html += '<td>';
    html += htmlToStringConversion(data.job_description, 200) + '...';
    html += '</td>';
    //html += '<td>' + data.job_description + '</td>';
    html += '<td>';
    html += '<a href="#" class="edit emp_history" data-id="' + data.mentor_employment_history_id + '"><i class="fa fa-edit"></i></a>';
    html += '<a href="#" class="remove_emp_data emp_history" data-id="' + data.mentor_employment_history_id + '"><i class="fa fa-times"></i></a>';
    html += '</td>';
    html += '</tr>';
    if (addToDom == 'add') {
        employmenthistory_tabledata.row.add($(html));
        employmenthistory_tabledata.draw();
        //$('.employment_history table tbody').append(html);
    } else if (addToDom == 'replace') {
        employmenthistory_tabledata.row($('.employment_history table tbody tr.emp_his_' + data.mentor_employment_history_id)).remove();
        employmenthistory_tabledata.row.add($(html)).draw();
        //$('.employment_history table tbody').append(html);
    } else {
        return html;
    }
}

// Generate Mentor Expertise table html
function addEmpExpRow(data, action) {
    updateCheckExpertiseValue(data);
    var html = '<tr class="emp_exp_' + data.mentor_expertise_id + '">' +
            '<td class="td_industry_id" data-industry-id="' + data.industry + '">' + data.industry_name + '</td>' +
            '<td>' + data.subindustry_name + '</td>' +
            '<td>' + data.domain_name + '</td>' +
            '<td>' + data.region_name + '</td>' +
            ' <td>' + (data.sub_region_name !== null ? data.sub_region_name : "") + '</td>' +
            '<td>' +
            '<a href="#" class="edit emp_expertise" data-id="' + data.mentor_expertise_id + '"><i class="fa fa-edit"></i></a>' +
            '<a href="#" class="remove_emp_data emp_expertise" data-industry-id="' + data.industry + '" data-id="' + data.mentor_expertise_id + '"><i class="fa fa-times"></i></a>' +
            '</td>' +
            '</tr>';
    if (action == 'add') {
        expertise_tabledata.row.add($(html));
        expertise_tabledata.draw();
        //$('.employment_expertise table tbody').append(html);
    } else if (action == 'replace') {
        expertise_tabledata.row($('.employment_expertise table tbody tr.emp_exp_' + data.mentor_expertise_id)).remove();
        expertise_tabledata.row.add($(html)).draw();
    } else {
        return html;
    }
}

//Update (add/delete) Organization Tags from DB
function updateOrganizationTag(tag, action) {
    $.ajax({
        url: $('#absPath').val() + 'mentor/updateOrganizationTag',
        method: 'POST',
        data: "tagName=" + tag + "&action=" + action,
        beforeSend: function () {
            //$.blockUI();
        },
        complete: function () {
            //$.unblockUI();
        },
        success: function (response) {
            if (action == 'add') {

            }
        }
    });
}

function updateCheckExpertiseValue(data) {
    var existVal = $('#check_expertise_ids').val();

    if (existVal == '') {
        $('#check_expertise_ids').val(data.industry);
        if ($('#check_expertise').val() == "") {
            $('#check_expertise').val(1);
        } else {
            $('#check_expertise').val(parseInt($('#check_expertise').val()) + 1);
        }
    } else {
        var splitedVal = existVal.split(',');
        var arrCurrentIndustriesInDataTable = [];

        var dataTableData = expertise_tabledata.data();
        $.each(dataTableData, function (index, item) {
            var parseStr = item[5];

            var arrRegEx = /^([\w\d\s<="#->])*[d][a][t][a][-][i][n][d][u][s][t][r][y][-][i][d][=]["](\d*)["]+/;

            var regExp = arrRegEx;
            var pattern = new RegExp(regExp);
            var arrMatchPattern = pattern.exec(parseStr);

            var thisIndustryId = arrMatchPattern[2];
            if ($.inArray(thisIndustryId.toString(), arrCurrentIndustriesInDataTable) == "-1") {
                arrCurrentIndustriesInDataTable.push(thisIndustryId);
            }
        });

        if ($.inArray(data.industry.toString(), splitedVal) != -1) {
            if (typeof data.delete != "undefined" && data.delete == true) {
                //if(expertise_tabledata.data().length > 0) {
                //if ($('.td_industry_id[data-industry-id=' + data.industry + ']').length == 0) {
                if ($.inArray(data.industry.toString(), arrCurrentIndustriesInDataTable) == -1) {
                    var index = splitedVal.indexOf(data.industry);
                    if (index > -1) {
                        splitedVal.splice(index, 1);
                        $('#check_expertise_ids').val(splitedVal.join());
                        var newVal = parseInt($('#check_expertise').val()) - 1;
                        if (newVal == 0) {
                            $('#check_expertise').val('');
                        } else {
                            $('#check_expertise').val(parseInt($('#check_expertise').val()) - 1);
                        }
                    }
                }
            } else {
                var current_vals = [];
                var arrCurrentIndustries = [];
                var currentExpertiseTableData = expertise_tabledata.data();
                $.each(currentExpertiseTableData, function (index, item) {
                    var parseStr = item[5];

                    var arrRegEx = /^([\w\d\s<="#->])*[d][a][t][a][-][i][n][d][u][s][t][r][y][-][i][d][=]["](\d*)["]+/;

                    var regExp = arrRegEx;
                    var pattern = new RegExp(regExp);
                    var arrMatchPattern = pattern.exec(parseStr);

                    var thisIndustryId = arrMatchPattern[2];

                    if ($.inArray(thisIndustryId, current_vals) == -1) {
                        current_vals.push(thisIndustryId);
                    }
                });
                $('#check_expertise').val(current_vals.length);
                $('#check_expertise_ids').val(current_vals.join());
            }
        } else {
            $('#check_expertise_ids').val($('#check_expertise_ids').val() + ',' + data.industry);
            if ($('#check_expertise').val() == "") {
                $('#check_expertise').val(1);
            } else {
                $('#check_expertise').val(parseInt($('#check_expertise').val()) + 1);
            }
        }
    }
}
