var videojs_instances = [];
var flagCompliancePassed = 0;
function become_mentor_step3_ajax() {
    $.ajax({
        url: $('#absPath').val() + 'mentor/getMentorFullData',
        type: 'POST',
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log(response);
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            flagCompliancePassed = response.data.mentor_details.is_compliance_passed;

            var mentorExpertiseLength = response.data.mentorExpertise.length;
            var industry_arr = [];
            $(".expertise_div").html('');
            $.each(response.data.mentorExpertise, function (index, element) {

                if ($('#industry option[value=' + element.industry + ']').length == 0) {
                    $('#industry').append('<option value="' + element.industry + '">' + element.industry_name + '</option>');
                    $('#filterByIndustry').append('<option value="' + element.industry + '">' + element.industry_name + '</option>');
                }

                if ($.inArray(element.industry, industry_arr) == -1) {
                    industry_arr.push(element.industry);
                    var new_elem = "";
                    new_elem = $(".workfolioClone").clone();
                    new_elem.removeClass("workfolioClone").addClass("findIndustryId").addClass("industry_id_" + element.industry).css("display", "block");
                    new_elem.find('label#expertiseTitle').text(element.industry_name);
                    new_elem.find('#industry_id').val(element.industry);

                    new_elem.find('div.project_link').attr('data-industry-id', element.industry);
                    //new_elem.find('ul.media_link').attr('data-industry-id', element.industry);
                    //new_elem.find('ul.upload_link').attr('data-industry-id', element.industry);

                    //new_elem.find('form.uploadMediaLink').removeClass('uploadMediaLink').attr('id', 'uploadMediaLink_' + element.industry);
                    //new_elem.find('form#uploadMediaLink_' + element.industry + ' input[name="industry_id"]').val(element.industry);

                    //new_elem.find('form.addProjectLink').removeClass('addProjectLink').attr('id', 'addProjectLink_' + element.industry);
                    //new_elem.find('form#addProjectLink_' + element.industry + ' input[name="industry_id"]').val(element.industry);

                    //new_elem.find('form.addMediaLink').removeClass('addMediaLink').attr('id', 'addMediaLink_' + element.industry);
                    //new_elem.find('form#addMediaLink_' + element.industry + ' input[name="industry_id"]').val(element.industry);

                    $(".expertise_div").append(new_elem);

                    var whenCreatedInstance = setInterval(function () {
                        if (typeof $.validator != "undefined") {
                            validateProjectForm('addProjectLink_' + element.industry);
                            validateMediaForm('addMediaLink_' + element.industry);
                            clearInterval(whenCreatedInstance);
                        }
                    }, 500);
                }
            });

            if (mentorExpertiseLength > 0) {
                addLinksToHtml(response.data.mentorExpertiseWorkfolio);
            } else {
                $(".expertise_div").css({"text-align": "center", "font-weight": "bold", "margin-bottom": "25px"}).html("Please add at least one expertise under Work Profile tab in your Profile in order to upload your portfolio for that expertise");
            }
        }
    });
}

become_mentor_step3_ajax();

function validateProjectForm(form_id) {
    $('#' + form_id).validate({
        errorClass: 'validate_error',
        ignore: [],
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'project_link') {
                $(element).parent().after(error);
            } else {
                $(element).after(error);
            }
        },
        rules: {
            project_link: {
                required: true,
                noBlankSpace: true,
                url_without_http: true
            }
        },
        messages: {
            project_link: {
                required: 'Please enter Project Link',
                url: 'Please enter a valid Project Link'
            }
        },
        submitHandler: function (form) {
            var industry_id = $(form).parents('.findIndustryId').find('#industry_id').val();
            $.ajax({
                url: $('#absPath').val() + 'mentor/add_workfolio_items',
                type: 'POST',
                data: $(form).serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('input[name="project_link"]').val('');
                    $(form).resetForm();
                    addLinksToHtml(response.data);
                }
            });
        }
    });
}

function validateMediaForm(form_id) {
    $('#' + form_id).validate({
        errorClass: 'validate_error',
        ignore: [],
        rules: {
            media_link: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            media_link: {
                required: 'Please enter Media Embed Link'
            }
        },
        submitHandler: function (form) {
            var industry_id = $(form).parents('.findIndustryId').find('#industry_id').val();
            var url = $(form).find('input[name="media_link"]').val();
            var db_url = '';
            if (url.search('watch') != -1) {
                db_url = url.split('v=')[1];
                db_url = db_url.split('&')[0];
            } else if (url.search('youtu.be') != -1) {
                db_url = url.split('youtu.be/')[1];
                db_url = db_url.split('?')[0];
            } else if (url.search('embed') != -1) {
                db_url = url.split('embed/')[1];
                db_url = db_url.split('"')[0];
                db_url = db_url.split('?')[0];
            } else {
                return false;
            }

            $.ajax({
                url: $('#absPath').val() + 'mentor/add_workfolio_items',
                type: 'POST',
                data: $(form).serialize() + "&new_media_link=" + db_url,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $(form).resetForm();
                    addLinksToHtml(response.data);
                }
            });
        }
    });
}

function validateAddWorkfolioForm() {
    $('#addWorkfolioForm').validate({
        errorClass: 'validate_error',
        errorPlacement: function (error, element) {
            if ($(element).attr('id') == 'project_link') {
                $(element).parent().after(error);
            } else if ($(element).attr('name') == 'workfolio_type') {
                $(element).parent().parent().append(error);
            } else {
                $(element).after(error);
            }
        },
        rules: {
            industry: {
                required: true
            },
            workfolio_type: {
                required: true
            },
            project_link: {
                required: true,
                noBlankSpace: true,
                url_without_http: true
            },
            media_link: {
                required: true
            },
            upload_video: {
                required: true,
                notVideoUpload: true
            }
        },
        messages: {
            industry: {
                required: "Please select Industry"
            },
            workfolio_type: {
                required: 'Please select Workfolio Type'
            },
            project_link: {
                required: 'Please enter Project Link',
                url: 'Please enter a valid Project Link'
            },
            media_link: {
                required: "Please enter Embed Media Link"
            },
            upload_video: {
                required: "Please select Media to upload",
                notVideoUpload: "No Video Files can be uploaded"
            }
        },
        submitHandler: function (form) {
            if ($('input[name="workfolio_type"]:checked').val() == 'project') {
                $.ajax({
                    url: $('#absPath').val() + 'mentor/add_workfolio_items',
                    type: 'POST',
                    data: $(form).serialize(),
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('input[name="project_link"]').val('');
                        $(form).resetForm();
                        addLinksToHtml(response.data);
                    }
                });
            } else if ($('input[name="workfolio_type"]:checked').val() == 'media') {
                var url = $(form).find('input[name="media_link"]').val();
                var db_url = '';
                if (url.search('watch') != -1) {
                    db_url = url.split('v=')[1];
                    db_url = db_url.split('&')[0];
                } else if (url.search('youtu.be') != -1) {
                    db_url = url.split('youtu.be/')[1];
                    db_url = db_url.split('?')[0];
                } else if (url.search('embed') != -1) {
                    db_url = url.split('embed/')[1];
                    db_url = db_url.split('"')[0];
                    db_url = db_url.split('?')[0];
                } else {
                    return false;
                }

                $.ajax({
                    url: $('#absPath').val() + 'mentor/add_workfolio_items',
                    type: 'POST',
                    data: $(form).serialize() + "&new_media_link=" + db_url,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $(form).resetForm();
                        addLinksToHtml(response.data);
                    }
                });
            } else if ($('input[name="workfolio_type"]:checked').val() == 'upload') {

                $(form).ajaxSubmit({
                    url: $('#absPath').val() + 'mentor/add_workfolio_uploads',
                    method: 'POST',
                    xhr: function () {
                        var xhr = new XMLHttpRequest();
                        xhr.upload.addEventListener('progress', function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);

                                $('.progress-bar').text(percentComplete + '%');
                                $('.progress-bar').width(percentComplete + '%');

                                if (percentComplete === 100) {
                                    $('.progress-bar').html('Done');
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    beforeSubmit: function (formData) {
                        var queryString = $.param(formData);
                        return true;
                    },
                    success: function (response) {
                        //$(form).find('input[name="uploadMedia"]').val('');
                        $(form).resetForm();
                        addLinksToHtml(response.data);
                        $('.progress-bar').text('0%');
                        $('.progress-bar').width('0%');
                    }
                });
            }
            $('.hidden_div, .description_div').hide();
            $('#addMentorWorkfolio').modal('hide');
            return false;


            $.ajax({
                url: $('#absPath').val() + 'mentor/add_workfolio_items',
                type: 'POST',
                data: $(form).serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    $('input[name="project_link"]').val('');
                    $(form).resetForm();
                    addLinksToHtml(response.data);
                }
            });
        }
    });
}

$(document).ready(function () {

    absPath = $('#absPath').val();

    $('#btnBecomeAMentorFinished').on('click', function(e){
        e.preventDefault();

        var redirectUrl = $(this).attr('data-nextURL');
        if(flagCompliancePassed == 1) {
            redirectUrl = absPath + "users/changeUserTypeAndRedirectToHome/U_M"
        }

        window.location = redirectUrl;
    });

    $('#addMentorWorkfolio').on('hidden.bs.modal', function () {
        $('#addWorkfolioForm')[0].reset();
        $('.hidden_div, .description_div').hide();
        $('#chars').text(150);
    });

    validateAddWorkfolioForm();

    var maxLength = $('#chars').text();
    $('textarea#media_desc').keyup(function () {
        var length = $(this).val().length;
        $('#chars').text(maxLength - length);
    });

    $('#filterByIndustry').change(function () {
        if ($(this).val() != "") {
            $('.findIndustryId').hide();
            $('.findIndustryId.industry_id_' + $(this).val()).show();
        } else {
            $('.findIndustryId').show();
        }
    });

    $(document.body).delegate('a#port[data-toggle="tab"]', 'click', function (e) {
        $.each(videojs_instances, function (index, item) {
            item.dispose();
        });
        become_mentor_step3_ajax();
    });
    // $('a#port[data-toggle="tab"]').on('click', function(){
    //     $.each(videojs_instances, function (index, item) {
    //         item.dispose();
    //     });
    //     become_mentor_step3_ajax();
    // });
    // $('a#port[data-toggle="tab"]').click(function () {
    // });

    $('input[type="radio"][name="workfolio_type"]').change(function () {
        $('.hidden_div').hide();
        $('.' + $(this).val() + '_div').show();
        $('.description_div').show();
    });

    $(document.body).on("click", "a.uploadMedia", function (e) {
        e.preventDefault();
        $(this).prev().click();
    });

    $(document.body).on("change", 'input[name="uploadMedia"]', function (e) {
        e.preventDefault();
        var form = $(this).parent();

        $(form).ajaxSubmit({
            url: $('#absPath').val() + 'mentor/add_workfolio_uploads',
            method: 'POST',
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener('progress', function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        $('.progress-bar').text(percentComplete + '%');
                        $('.progress-bar').width(percentComplete + '%');

                        if (percentComplete === 100) {
                            $('.progress-bar').html('Done');
                        }
                    }
                }, false);
                return xhr;
            },
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            beforeSubmit: function (formData) {
                var queryString = $.param(formData);
                return true;
            },
            success: function (response) {
                //$(form).find('input[name="uploadMedia"]').val('');
                $(form).resetForm();
                addLinksToHtml(response.data);
                $('.progress-bar').text('0%');
                $('.progress-bar').width('0%');
            }
        });
    });

    $(document.body).on("click", "a.remove_workfolio", function (e) {
        e.preventDefault();
        if (confirm("Are you sure you want to delete this workfolio?")) {
            var type = '';
            var industry_id = $(this).attr('data-industry-id');
            var data_id = $(this).attr('data-id');

            if ($(this).hasClass('remove_project')) {
                type = 'project';
            } else if ($(this).hasClass('remove_media')) {
                type = 'media';
            } else if ($(this).hasClass('remove_upload')) {
                type = 'upload';
            }

            if (type != '') {
                $.ajax({
                    url: $('#absPath').val() + 'mentor/delete_workfolio_items',
                    type: 'POST',
                    data: "type=" + type + "&data_id=" + data_id + "&industry_id=" + industry_id,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('.remove_workfolio[data-id=' + response.data.workfolio_id + ']').parent().remove();
                    }
                });
            } else {
                return;
            }
        }
    });

    $(document.body).on("click", "a.add", function (e) {
        e.preventDefault();
        $(this).prev().trigger('click');
    });
});

function addLinksToHtml(data) {
    $.each(data, function (index, item) {
        if (item.media_type == 'project') {
            addProjectLink(item, true);
        } else if (item.media_type == 'media') {
            addMediaLink(item, true);
        } else if (item.media_type == 'upload') {
            addUploadLink(item, true);
        }
    });
}

function addProjectLink(data, addToDom) {
    var html = '<div class="workfolioItem col-md-4">' +
            '<a href="#" class="remove_workfolio remove_project" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
    if (data.media_title != null) {
        html += '<span>' + data.media_title + '</span>';
    }
    html += '<br><a class="projectLinkWrapper" href="//' + data.media_value + '" target="_BLANK">' +
            '<span>http://' + data.media_value + '</span>' +
            '</a>' +
            '<p class="workfolio_description">' + data.media_desc + '</p>' +
            '</div>';
    if (typeof data != "undefined" && addToDom == true) {
        $('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.project_link').prepend(html);
    } else {
        return html;
    }
}

function addMediaLink(data, addToDom) {
    var html = '<div class="workfolioItem col-md-4">' +
            '<a href="#" class="remove_workfolio remove_media" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
    if (data.media_title != null) {
        html += '<span>' + data.media_title + '</span>'
    }
    html += '<iframe src="https://www.youtube.com/embed/' + data.media_value + '?rel=0" frameborder="0" allowfullscreen></iframe>' +
            '<p class="workfolio_description">' + data.media_desc + '</p>' +
            '</div>';
    if (typeof data != "undefined" && addToDom == true) {
        $('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.project_link').prepend(html);
    } else {
        return html;
    }
}

function addUploadLink(data, addToDom) {
    var html = '';
    //console.log(data.media_mime);
    if (data.media_mime.split('/')[0] == 'audio') {
        html = '<div class="workfolioItem col-md-4">' +
                '<a href="#" class="remove_workfolio remove_upload" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<audio id="example_audio_' + data.mentor_workfolio_id + '" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" preload="auto" />' +
                '<p class="workfolio_description">' + data.media_desc + '</p>' +
                '</div>';
    } else if (data.media_mime == 'application/pdf') {
        html = '<div class="workfolioItem col-md-4">' +
                '<a href="#" class="remove_workfolio remove_upload" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
        if (data.media_title != "") {
            html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>' + data.media_title + '</span></a><br/>';
        } else {
            html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>View/Download</span></a><br/>';
        }
        //html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>' + data.media_value + '</span></a>' +
        html += '<iframe frameborder=0 scrolling="no" class="no-center" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '"></iframe>' +
                '<p class="workfolio_description">' + data.media_desc + '</p>' +
                '</div>';
    } else if (data.media_mime.split('/')[0] == 'video') {
        html = '<div class="workfolioItem col-md-4">' +
                '<a href="#" class="remove_workfolio remove_upload" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<video style="background: black;" id="example_video_' + data.mentor_workfolio_id + '" class="video-js vjs-default-skin"' +
                'controls preload="auto" width="308" height="150">' +
                '<source src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" type="' + data.media_mime + '" />' +
                '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
                '</video>' +
                '<p class="workfolio_description">' + data.media_desc + '</p>' +
                '</div>';
    } else if (data.media_mime.split('/')[0] == 'image') {
        html = '<div class="workfolioItem col-md-4">' +
                '<a href="#" class="remove_workfolio remove_upload" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<img style="width: 308px;height: 150px;"class="myImg img-responsive" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '">' +
                '<p class="workfolio_description">' + data.media_desc + '</p>' +
                '</div>';
    } else /*if (data.media_mime.split('/')[0] == 'application')*/ {
        html = '<div class="workfolioItem col-md-4">' +
                '<a href="#" class="remove_workfolio remove_upload" data-id="' + data.mentor_workfolio_id + '" data-industry-id="' + data.industry_id + '"><i class="fa fa-minus-circle"></i></a>';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span><br/>';
        }
        html += '<a class="applicationWrapper" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><img style="width: 150px;height: 150px;"class="myImg img-responsive" src="' + $('#absPath').val() + 'images/doc.jpg"></a>' +
                '<p class="workfolio_description">' + data.media_desc + '</p>' +
                '</div>';
    }

    if (typeof data != "undefined" && addToDom == true) {
        $('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.project_link').prepend(html);

        if (data.media_mime.split('/')[0] == 'audio') {
            $("audio#example_audio_" + data.mentor_workfolio_id).audioPlayer({
                classPrefix: 'audioplayer',
                strPlay: 'Play',
                strPause: 'Pause',
                strVolume: 'Volume'
            });
        } else if (data.media_mime.split('/')[0] == 'application') {

        } else if (data.media_mime.split('/')[0] == 'video') {
            videojs_instances.push(videojs("example_video_" + data.mentor_workfolio_id, {}, function () {}));
        } else if (data.media_mime.split('/')[0] == 'image') {

        }

    } else {
        return html;
    }
}