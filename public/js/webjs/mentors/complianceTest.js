var test = {
    section: "header",
    loopClass: "MentProfile .mentor_dp",
    type: "input",
    srcClass: "himg",
    nameClass: "hfname",
    nameHtmlClass: "imgdiv",
    imgClass: "imgUserProfile"
};
$("#startTest").click(function () {
    var data_id = $(this).data('id');
    $('#' + data_id).hide();
    $('.' + data_id).hide();
    $('#complianceTestIframe').show();
    $('.complianceTestIframe').show();
});
$(document).ready(function () {
    setUserInitialImage(test);
    $('#complianceTest').attr('height', $('.well.steps').height() - 50);
    $('#complianceTest').attr('width', $('.well.steps').width() - 50);
});