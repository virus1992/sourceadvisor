var promocode_tabledata = '';
var amount = 0;
var walBal = 0;
var chk = 0;

$(document).ready(function () {

    $('.transection_table').DataTable(
            {"lengthMenu": [10, 25, 50, 100],
                "aaSorting": [[3, "desc"]],
                "language": {
                    "emptyTable": "There are no transactions associated with your account as of now."
                }
            }
    );

    $('#payout').click(function () {
        amount = parseFloat($('#maxDayPayout').val()).toFixed(2);
        walBal = parseFloat($('#walBal').val()).toFixed(2);
        var userId = $('#user_id').val();
        var mentorName = $('#mentorName').val();
        var mentorEmail = $('#mentorEmail').val();
        var weekPayout = $('#weekPayout').val();
        var maxDayPayout = $('#maxDayPayout').val();


        var msg = "";
        if (walBal != null && walBal != "0.00") {
            if (weekPayout != "null" && weekPayout != "") {
                if (maxDayPayout != "null" && maxDayPayout != "") {
                    if ((walBal * 1) <= (amount * 1)) {
                        msg = "Maximum Payout for this mentor is " + walBal;
                        chk = 1;
                        $("#updatebtn").attr('disabled', false);
                    } else {
                        msg = "Maximum Payout for this mentor is " + amount;
                        $("#updatebtn").attr('disabled', false);
                    }
                } else {
                    msg = "For this mentor country payout is not available";
                    $("#updatebtn").attr('disabled', true);
                }
            } else {
                msg = "For this mentor country payout is not available";
                $("#updatebtn").attr('disabled', true);
            }
        } else {
            msg = "This mentor does not have wallet balance for payout";
            $("#updatebtn").attr('disabled', true);
        }
        $("#walmsg").text(msg);

        getPaypalEmail();
    });


    $(document.body).on('click', ".LIwPP", function () {
        $('#Manage').modal('toggle');
    });

    $(document.body).on("keyup", "#payoutAmount", function () {
        var curVal = parseFloat($(this).val()).toFixed(2);
        //var walBal = parseFloat($("#wallbal").val()).toFixed(2);
        var comp = 0;

        if (chk == 0) {
            comp = amount;
        } else {
            comp = walBal;
        }

        if ((curVal * 1) <= comp) {

            //if ((curVal * 1) <= walBal) {
            $("#updatebtn").attr('disabled', false);
            $("#walmsg").text("");
        } else {
            $("#updatebtn").attr('disabled', true);
            $("#walmsg").text("You cannot request amount more than " + walBal);
        }

    });

    $("#payoutAmount").bind('paste', function (event) {
        var _this = this;
        // Short pause to wait for paste to complete
        setTimeout(function () {
            var curVal = $(_this).val();
            //var walBal = parseFloat($("#wallbal").val()).toFixed(2);
            var comp = 0;

            if (chk == 0) {
                comp = amount;
            } else {
                comp = walBal;
            }

            if ((curVal * 1) <= comp) {
                //if ((curVal * 1) <= walBal) {
                $("#updatebtn").attr('disabled', false);
                $("#walmsg").text("");
            } else {
                $("#updatebtn").attr('disabled', true);
                $("#walmsg").text("You cannot request amount more than " + walBal);
            }

        }, 100);
    });

    $('#paypalEmailFrm').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            paypalEmail: {
                required: true,
                email: true
            },
            payoutAmount: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            paypalEmail: {
                required: 'Please enter Paypal Email.',
                email: 'Please enter valid email'
            },
            payoutAmount: {
                required: 'Please enter amount to withdraw.',
                text: 'Please enter amount to withdraw.'
            }
        },
        submitHandler: function (form) {

            var data = {};
            var payoutAmount = $('#payoutAmount').val();

            $.ajax({
                url: $('#absPath').val() + 'mentor/requestPayout',
                method: "POST",
                data: {payoutAmount: payoutAmount},
                // encData: encDatas
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response.data.msg == "success") {
                        $('#paypalEmailFrm')[0].reset();
                        $('#Manage').modal('toggle');
                        displayAjaxNotificationMessage("Request sent", "success");
                    } else if (response.data.msg == "stop") {
                        if (response.data.pcnt == 0) {
                            $('#paypalEmailFrm')[0].reset();
                            $('#Manage').modal('toggle');
                            displayAjaxNotificationMessage("Maximum weekly amount for payout is " + response.data.wAmt + " only " + response.data.amt + " remaining ", "danger");
                        } else {
                            $('#paypalEmailFrm')[0].reset();
                            $('#Manage').modal('toggle');
                            displayAjaxNotificationMessage("Maximum weekly allowed payout is " + response.data.acnt + " and you have already done " + response.data.pcnt + " payouts this week. ", "danger");
                        }
                    } else {
                        $('#paypalEmailFrm')[0].reset();
                        $('#Manage').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#Manage').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });

});

function encData(data, key) {

    // Encrypt 
    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), key);
    return ciphertext.toString();
}

function getPaypalEmail() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/getPaypalEmail',
        method: "POST",
        data: {},
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.data.paypal_email != "" && response.data.paypal_email != null) {
                $("#paypalbtn").css("display", "none");
                $("#paypalfrm").css("display", "block");
                $('#paypalEmail').val(response.data.paypal_email);
                $('#Manage').modal('toggle');
            } else {
                $("#paypalbtn").css("display", "block");
                $("#paypalfrm").css("display", "none");
                $('#Manage').modal('toggle');
                $('#paypalEmail').val("");
            }
        },
        error: function (response) {
            $.unblockUI();
        }

    });
    return false;
}