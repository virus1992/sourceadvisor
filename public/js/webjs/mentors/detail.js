$(".modal-fullscreen").on('show.bs.modal', function() {
    setTimeout(function() {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        if ($('#keynote_video_uploaded').length > 0) {
            $('#keynote_video_uploaded')[0].play();
        } else if ($('#keynote_video_youtube').length > 0) {
            $('#keynote_video_youtube')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
        }
    }, 0);
});

$(".modal-fullscreen").on('hidden.bs.modal', function() {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    if ($('#keynote_video_uploaded').length > 0) {
        $('#keynote_video_uploaded')[0].pause();
    } else if ($('#keynote_video_youtube').length > 0) {
        $('#keynote_video_youtube')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
    }
});

$(document).ready(function() {


    /*************img code */

    var img = $('#detailImg').attr('src');
    var chunks = [];
    if (img != "") {
        chunks = img.split('/');
        if (chunks[4] == "noimg.png") {
            var name = $(".blog-title").text();
           
            var namef = $("#fnameforimg").val();
            var nameSplitf = namef.split("");
            var initialf = nameSplitf[0];

            var namel = $("#lnameforimg").val();
            var nameSplitl = namel.split("");
            var initiall = nameSplitl[0];


            $("#detailImgDiv").html(initialf + initiall);
            $("#detailImgDiv").css("display", "block");
            $("#detailImg").css("display", "none");
        } else {
            $("#detailImgDiv").css("display", "none");
            $("#detailImg").css("display", "block");
        }
    }

    /***************img code  */




    var absPath = $('#absPath').val();
    //var $container = $('.masonry-container');

    $('a[href="#tab_Portfolio"]').click(function() {
        var $this = $(this);
        $this.on('shown.bs.tab', function() {
            $objMasonary = avtivateMasonary();
        });
    });

    $(window).resize(function() {
        $objMasonary = avtivateMasonary();
    });


    
    $(window).scroll(function () {
        if($('ul.nav.nav-tabs li.active a[href=#tab_reviews]').length == "1") {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var hidReviewPage = parseInt($('#hidReviewPage').val());
                hidReviewPage += 1;
                if($('#hidReviewMoreDataAvailable').val() == "Y") {
                    getMentorReviewPagination(hidReviewPage);
                    $('#hidReviewPage').val(hidReviewPage);
                }
            }
        }
    });


    setTimeZoneForRegisteringUser();

    $(".docViewer").on("click", function() {
        $('.myFrame').remove();
        $('.myImg').remove();
        $('.fileType').remove();

        var media_type = $(this).attr('data-media_type');
        var media_value = $(this).attr('data-media_value');
        var name = $(this).attr('data-name');
        var extension = $(this).attr('data-extension');
        var media_mime = $(this).attr('data-media_mime');
        var media_title = $(this).attr('data-media_title');
        var media_desc = $(this).attr('data-media_desc');

        if (media_type == "project") {
            return true;
        } else if (media_type == "media") {
            media_value = "https://www.youtube.com/embed/" + media_value + "?enablejsapi=1&version=3&playerapiid=ytplayer&rel=0";
            
            $('<iframe>', {
                src: media_value,
                class: 'myFrame',
                frameborder: 0,
                scrolling: 'no',
                width: '100%'
            }).appendTo('#viewercontainer');

            $("#btnDownloadWorkFolio").hide();

        } else {

            $("#btnDownloadWorkFolio").show();
            var filePath = absPath + 'workfolioVideo/' + media_value;
            var fileDownload = filePath + "?dl";

            if ((media_mime == "application" && extension == "application/pdf")/* ||
                (media_mime == "video") || 
                (media_mime == "audio")*/) {
                $('<iframe>', {
                    src: filePath,
                    class: 'myFrame',
                    frameborder: 0,
                    scrolling: 'no',
                    width: '100%'
                }).appendTo('#viewercontainer');
            }
            else if(media_mime == "video") {
                var strVideo =  '   <video style="background: black;" id="videoPlayback" class="video-js vjs-default-skin" controls preload="none" width="100%" height="auto" data-setup=\'{"example_option":true}\'>' + 
                                '       <source src="' + filePath + '" type="' + extension + '" />' +
                                '       <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' + 
                                '   </video>';
                $('#viewercontainer').html(strVideo);
            }
            else if(media_mime == "audio") {
                
                //var strVideo = '<audio id="example_audio_" src="' + filePath + '" preload="auto" />';

                var strVideo =  '<audio controls id="videoPlayback" style="width: 100%;">' +
                                    '<source src="' + filePath + '" type="' + extension + '">' +
                                    'Your browser does not support the audio element.' + 
                                '</audio>';

                $('#viewercontainer').html(strVideo);
            }
            else {
                if (media_mime != "image") {
                    var placeHolderImage = $(this).find('.portfolioicon').attr('src');
                    filePath = placeHolderImage;
                }
                var img = $('<img class="myImg img-responsive">');
                img.attr('src', filePath);
                img.appendTo('#viewercontainer');
            }

            var file_type = media_value.split(".");
            var filetype = file_type[1];

            if (media_type != "image") {
                var span = $('<span />').addClass("fileType").html(filetype + " file");
                span.insertAfter('.myImg');
            }
            $("#btnDownloadWorkFolio").attr('href', fileDownload);
            $('#viewerheader').html(media_title);
            $('#viewerdescription').html(media_desc);
        }

        $('#viewer').modal('show');
    });

    $('#viewer').on('hidden.bs.modal', function(){
        $('#viewercontainer').html("");
        $('.myFrame').remove();
        $('.myImg').remove();
        $('.fileType').remove();
    });

    $("#showmore").click(function() {
        var text = $(this).text();
        if (text == "Show More") {
            $(this).text("Show Less");
        } else {
            $(this).text("Show More");
        }
        $("#overviewdiv").toggleClass("reveal-open");
    });

    $('#industry_dropdown').change(function() {
        var selectedIndustry = $(this).val();
        filterPortfolio(selectedIndustry);
    });

});


function getMentorReviewPagination(pageNumber) {

    var mentorId = $('#mentorId').val();
    var dataToSend = {
        pageNumber : pageNumber,
        mentorId: mentorId
    };

    $.ajax({
        url: $('#absPath').val() + 'mentor/getMentorReviewPagination',
        type: 'GET',
        data: dataToSend,
        beforeSend: function() {
            $.blockUI();
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {

            var data = response.data;

            if(data.length > 0) {
                $.each(data, function(index, arrMentorReview){
                    /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                    // Retrieve the HTML from the script tag we setup in step 1​
                    // We use the id (header) of the script tag to target it on the page​
                    var theTemplateScript = $("#mentorReviewListingBoxes").html();

                    // The Handlebars.compile function returns a function to theTemplate variable​
                    var theTemplate = Handlebars.compile(theTemplateScript);

                    $("#mentorProfileListingContainer").append(theTemplate(arrMentorReview));
                    /*************************************** SEARCH BOX TEMPLATE END  ****************************************/
                });
            }
            else {
                $('#hidReviewMoreDataAvailable').val('N');
            }
        }
    });
}



function filterPortfolio(selectedIndustry) {
    if($.isNumeric(selectedIndustry) && selectedIndustry > 0) {
        $('.eachportfoliobox').fadeOut("fast", function() {
            if($('.eachportfoliobox.industry_' + selectedIndustry).length > 0) {
                $('.eachportfoliobox.industry_' + selectedIndustry).fadeIn("fast", function(){
                    $objMasonary = avtivateMasonary();
                });
            }
        });
    }
    else {
        $('.eachportfoliobox').fadeIn("fast", function() {
            $objMasonary = avtivateMasonary();
        });
    }
}


function avtivateMasonary() {

    $container = $('.masonry-container');
    var $masonary = $container.masonry({
        columnWidth: '.eachportfolioboxsizer',
        itemSelector: '.eachportfoliobox',
        isAnimated: true
    });
    return $masonary;
}

function loadPortfolioItems(industry_id) {
    $.ajax({
        url: $('#absPath').val() + 'mentor/getMentorWorkfolio',
        type: 'POST',
        data: {
            userId: $('#mentorId').val(),
            industry_id: industry_id
        },
        beforeSend: function() {
            $.blockUI();
        },
        complete: function() {
            $.unblockUI();
        },
        success: function(response) {
            var mentorExpertiseLength = response.data.mentorExpertiseWorkfolio.length;

            if (mentorExpertiseLength > 0) {
                addLinksToHtml(response.data.mentorExpertiseWorkfolio, industry_id);
            } else {
                $(".expertise_div").css({ "text-align": "center", "font-weight": "bold", "margin-bottom": "25px" }).html("Please add at least one expertise under Work Profile tab in your Profile in order to upload your portfolio for that expertise");
            }
        }
    });
}

function addLinksToHtml(data, industry_id) {
    if (typeof industry_id == "undefined") {
        $('#industry_dropdown').html('');
        $('#industry_dropdown').append('<option value=""> --Filter by Industry-- </option>');
    }

    $('.masonry').html('').append('<div class="masonry-container"></div>');
    $.each(data, function(index, item) {
        if (typeof industry_id == "undefined") {
            if ($('#industry_dropdown option[value=' + item.industry_id + ']').length == 0) {
                if (typeof industry_id != "undefined" && industry_id == item.industry_id) {
                    $('#industry_dropdown').append('<option selected value="' + item.industry_id + '">' + item.industry_name + '</option>');
                } else {
                    $('#industry_dropdown').append('<option value="' + item.industry_id + '">' + item.industry_name + '</option>');
                }
            }
        }
        if (item.media_type == 'project') {
            addProjectLink(item, true);
        } else if (item.media_type == 'media') {
            addMediaLink(item, true);
        } else if (item.media_type == 'upload') {
            addUploadLink(item, true);
        }
    });

    var $container = $('.masonry-container');
    $container.imagesLoaded(function() {
        $container.masonry({
            columnWidth: '.item',
            itemSelector: '.item'
        });
    });
}

function addProjectLink(data, addToDom) {
    var html = '';
    if (data.media_title != null) {
        html += '<span>' + data.media_title + '</span>';
    }
    html += '<a class="projectLinkWrapper" href="//' + data.media_value + '" target="_BLANK">' +
        '<span>http://' + data.media_value + '</span>' +
        '</a>';
    if (data.media_desc != null) {
        html += '<p class="workfolio_description">' + data.media_desc + '</p>';
    }
    if (typeof data != "undefined" && addToDom == true) {
        //$('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.project_link').append(html);
        $('.masonry-container').append("<div class='mentorPortfolioWrapper item'><div class='thumbnail'>" + html + "</div></div>");
    } else {
        return html;
    }
}

function addMediaLink(data, addToDom) {
    var html = '';
    if (data.media_title != null) {
        html += '<span>' + data.media_title + '</span>';
    }
    html += '<iframe src="https://www.youtube.com/embed/' + data.media_value + '?rel=0" frameborder="0" allowfullscreen></iframe>';
    if (data.media_desc != null) {
        html += '<p class="workfolio_description">' + data.media_desc + '</p>';
    }
    if (typeof data != "undefined" && addToDom == true) {
        //$('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.media_link').prepend(html);
        $('.masonry-container').append("<div class='mentorPortfolioWrapper item'><div class='thumbnail'>" + html + "</div></div>");
    } else {
        return html;
    }
}

function addUploadLink(data, addToDom) {
    var html = '';
    //console.log(data.media_mime);
    if (data.media_mime.split('/')[0] == 'audio') {
        html = '';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<audio id="example_audio_' + data.mentor_workfolio_id + '" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" preload="auto" />';
        if (data.media_desc != null) {
            html += '<p class="workfolio_description">' + data.media_desc + '</p>';
        }
    } else if (data.media_mime == 'application/pdf') {
        html = '';
        //html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>' + data.media_value + '</span></a>' +
        //if (data.media_title != null) {
        //    html += '<br/><a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>' + data.media_title + '</span></a><br/>';
        //}
        if (data.media_title != "") {
            html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>' + data.media_title + '</span></a><br/>';
        } else {
            html += '<a class="no-center" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><span>View/Download</span></a><br/>';
        }
        html += '<iframe frameborder=0 scrolling="no" class="no-center" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '"></iframe>';
        if (data.media_desc != null) {
            html += '<p class="workfolio_description">' + data.media_desc + '</p>';
        }
    } else if (data.media_mime.split('/')[0] == 'video') {
        html = '';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<video style="background: black;" id="example_video_' + data.mentor_workfolio_id + '" class="video-js vjs-default-skin"' +
            'controls preload="none" width="308" height="150"' +
            'data-setup=\'{"example_option":true}\'>' +
            '<source src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" type="' + data.media_mime + '" />' +
            '<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>' +
            '</video>';
        if (data.media_desc != null) {
            html += '<p class="workfolio_description">' + data.media_desc + '</p>';
        }
    } else if (data.media_mime.split('/')[0] == 'image') {
        html = '';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<img style="width: 308px;height: 150px;"class="myImg img-responsive" src="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '">';
        if (data.media_desc != null) {
            html += '<p class="workfolio_description">' + data.media_desc + '</p>';
        }
    } else /*if (data.media_mime.split('/')[0] == 'application')*/ {
        html = '';
        if (data.media_title != null) {
            html += '<span>' + data.media_title + '</span>';
        }
        html += '<a class="applicationWrapper" href="' + $('#absPath').val() + 'workfolioVideo/' + data.media_value + '" target="_BLANK"><img style="width: 150px;height: 150px;"class="myImg img-responsive" src="' + $('#absPath').val() + 'images/doc.jpg"></a>';
        if (data.media_desc != null) {
            html += '<p class="workfolio_description">' + data.media_desc + '</p>';
        }
    }

    if (typeof data != "undefined" && addToDom == true) {
        //$('input[name="industry_id"][value=' + data.industry_id + ']').parents('.findIndustryId').find('.upload_link').prepend(html);
        $('.masonry-container').append("<div class='mentorPortfolioWrapper item'><div class='thumbnail'>" + html + "</div></div>");
        if (data.media_mime.split('/')[0] == 'audio') {
            $("audio#example_audio_" + data.mentor_workfolio_id).audioPlayer({
                classPrefix: 'audioplayer',
                strPlay: 'Play',
                strPause: 'Pause',
                strVolume: 'Volume'
            });
        } else if (data.media_mime.split('/')[0] == 'application') {

        } else if (data.media_mime.split('/')[0] == 'video') {

        } else if (data.media_mime.split('/')[0] == 'image') {

        }

    } else {
        return html;
    }
}