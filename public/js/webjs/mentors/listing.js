var absPath = "";
var allSideBarData = [];
var priceSlider;
var userInitialConfig = {
    section: "search_mentor_listing",
    loopClass: "new-mentorlist-item-wrapper.row",
    srcClass: "listingImg",
    nameClass: "mentor-name",
    nameHtmlClass: "listingImgDiv",
    imgClass: "listingImg"
};
$(document).ready(function () {

    absPath = $('#absPath').val();


    /*if ($(window).width() > 768) {
     $(window).load(function () {
     $("#left-control").sticky({topSpacing: 70});
     $("#left-control").sticky({bottomSpacing: 270});
     });
     }*/

    setTimeZoneForRegisteringUser();


    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            getNextPageData();
        }
    });


    initSearchProcess();
});




function initSearchProcess() {
    $('#hidPage').val("0");

    var hidPerPageCount = $('#hidPerPageCount').val();
    var hidSortByPreference = $('#hidSortByPreference').val();
    var hidRefreshSideBar = $('#hidRefreshSideBar').val();

    var hidSearchParamJSON = $('#hidSearchParamJSON').val();
    console.log(hidSearchParamJSON);

    $('#hidFlagMoreData').val('Y');

    $.ajax({
        url: absPath + 'mentor/listingSearch',
        method: 'POST',
        type: 'json',
        data: { data: hidSearchParamJSON },
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            }

            if (response['error'] == "1") {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            } else {
                if (response['flagMsg'] == "MENTORLISTFETCH") {

                    var arrTokenTagsToDisplay = response.data.arrTokenTagsToDisplay;
                    var arrSideBarData = response.data.objSideBarData;
                    var arrMentorListingData = response.data.arrMentorDataToDisplay;
                    var loggedInUserId = response.data.loggedInUserId;

                    $.each(arrMentorListingData, function (index, objEle) {
                        objEle.overViewListingDisplay = htmlToStringConversion(objEle.overViewListing, 150);
                        objEle.punchLineListingDisplay = htmlToStringConversion(objEle.punchLineListing, 50);
                        objEle.professionDisplay = htmlToStringConversion(objEle.profession, 18);
                        objEle.jobDescriptionDisplay = htmlToStringConversion(objEle.jobDescription, 18);
                        objEle.company_nameDisplay = htmlToStringConversion(objEle.company_name, 18);
                    });

                    var arrListingDisplayData = {
                        loggedInUserId: loggedInUserId,
                        arrMentorListingData: arrMentorListingData,
                        inListing: "1"
                    };

                    // setting checks in sode bar filters.
                    var dataParamsForSearch = response.data.arrDataParams;


                    /*************************************** TOKEN TAGS TEMPLATE START ****************************************/
                    // Retrieve the HTML from the script tag we setup in step 1​
                    // We use the id (header) of the script tag to target it on the page​
                    var searchTagsTemplate = $("#mentorListingTags").html();

                    // The Handlebars.compile function returns a function to theTemplate variable​
                    var searchTagTemplateComplied = Handlebars.compile(searchTagsTemplate);

                    $("#js-mentorListingTags").html(searchTagTemplateComplied({ arrTokenData: arrTokenTagsToDisplay }));
                    /*************************************** TOKEN TAGS TEMPLATE END  ****************************************/


                    var maxPrice, minPrice = 0;
                    var flagShowPriceFilter = false;
                    if (arrSideBarData.arrSidebarPriceFilter.length > 0) {
                        var arrSidebarPriceFilter = arrSideBarData.arrSidebarPriceFilter[0];

                        if ((typeof arrSidebarPriceFilter.maxPrice != 'undefined' && $.isNumeric(arrSidebarPriceFilter.maxPrice) && arrSidebarPriceFilter.maxPrice > -1) &&
                            (typeof arrSidebarPriceFilter.minPrice != 'undefined' && $.isNumeric(arrSidebarPriceFilter.minPrice) && arrSidebarPriceFilter.minPrice > -1)) {

                            maxPrice = arrSidebarPriceFilter.maxPrice;
                            minPrice = arrSidebarPriceFilter.minPrice;

                            flagShowPriceFilter = true;
                        }
                    }

                    $('#hidMinPrice').val(minPrice);
                    $('#hidMaxPrice').val(maxPrice);

                    var priceFilterData = {
                        maxPrice: maxPrice,
                        minPrice: minPrice
                    };


                    var mentorCount = response.data.arrMentorAllDataWithoutLimit.length;

                    if (mentorCount > 0) {

                        /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                        // Retrieve the HTML from the script tag we setup in step 1​
                        // We use the id (header) of the script tag to target it on the page​
                        var theTemplateScript = $("#mentorListingBoxes").html();

                        // The Handlebars.compile function returns a function to theTemplate variable​
                        var theTemplate = Handlebars.compile(theTemplateScript);

                        $("#searchResultDiv").html(theTemplate({ arrMentorData: arrListingDisplayData }));
                        $('#mentorListingCount').html(mentorCount);
                        /*************************************** SEARCH BOX TEMPLATE END  ****************************************/

                        /*************************************** FILTER TEMPLATE START ****************************************/
                        // Retrieve the HTML from the script tag we setup in step 1​
                        // We use the id (header) of the script tag to target it on the page​
                        var theTemplateScriptFilter = $("#mentorListingFilter").html();

                        // The Handlebars.compile function returns a function to theTemplate variable​
                        var theTemplateFilter = Handlebars.compile(theTemplateScriptFilter);
                        var dataForFilter = {
                            totalMentor: mentorCount,
                            activeSort: dataParamsForSearch.sortBy
                        };

                        $("#divMentorSearchListing").html(theTemplateFilter(dataForFilter));
                        $('#divMentorSearchListing').css("display", "block");
                        /*************************************** FILTER TEMPLATE END  ****************************************/


                        /*************************************** SIDEBAR FILTER START ****************************************/
                        if (hidRefreshSideBar == "Y") {
                            // Retrieve the HTML from the script tag we setup in step 1​
                            // We use the id (header) of the script tag to target it on the page​
                            var sideBarTemplate = $("#sideBarFilter").html();

                            // The Handlebars.compile function returns a function to theTemplate variable​
                            var theSideBarTemplate = Handlebars.compile(sideBarTemplate);
                            var dataForSideBarFilter = {
                                domainData: arrSideBarData.arrDomainSidebarFilter,
                                arrDomainSidebarFilter: arrSideBarData.arrDomainSidebarFilter,
                                arrIndustrySideBarFilter: arrSideBarData.arrIndustrySideBarFilter,
                                arrRegionSidebarFilter: arrSideBarData.arrRegionSidebarFilter
                            };

                            if (flagShowPriceFilter) {
                                dataForSideBarFilter.priceFilterData = priceFilterData;
                            }

                            $("#left-control").html(theSideBarTemplate(dataForSideBarFilter));
                        }
                        /*========================= PRICE FILTER INITIALIZATION START ==========================*/
                        priceSlider = $('#ex1').bootstrapSlider();
                        priceSlider.on('slideStop', function (event) {

                            var newMinValue = event.value[0];
                            var newMaxValue = event.value[1];

                            $('#hidMinPrice').val(newMinValue);
                            $('#hidMaxPrice').val(newMaxValue);

                            setPriceFilterAndInitSearch(newMinValue, newMaxValue);
                        });

                        priceSlider.on('slide', function (event) {
                            var newMinValue = event.value[0];
                            var newMaxValue = event.value[1];

                            $('#spanMinPriceFilter').html(newMinValue);
                            $('#spanMaxPriceFilter').html(newMaxValue);
                        });
                        /*========================= PRICE FILTER INITIALIZATION END  ==========================*/

                        setSideBarFilterCheckBoxex(dataParamsForSearch);



                        /*************************************** SIDEBAR FILTER END  ****************************************/

                        if (mentorCount < hidPerPageCount) {
                            $('#hidFlagMoreData').val("N");
                        }

                    } else {


                        /*************************************** SIDEBAR FILTER START ****************************************/
                        if (hidRefreshSideBar == "Y") {
                            // Retrieve the HTML from the script tag we setup in step 1​
                            // We use the id (header) of the script tag to target it on the page​
                            var sideBarTemplate = $("#sideBarFilter").html();

                            // The Handlebars.compile function returns a function to theTemplate variable​
                            var theSideBarTemplate = Handlebars.compile(sideBarTemplate);
                            var dataForSideBarFilter = {
                                domainData: arrSideBarData.arrDomainSidebarFilter,
                                arrDomainSidebarFilter: arrSideBarData.arrDomainSidebarFilter,
                                arrIndustrySideBarFilter: arrSideBarData.arrIndustrySideBarFilter,
                                arrRegionSidebarFilter: arrSideBarData.arrRegionSidebarFilter
                            };

                            if (flagShowPriceFilter) {
                                dataForSideBarFilter.priceFilterData = priceFilterData;
                            }

                            $("#left-control").html(theSideBarTemplate(dataForSideBarFilter));
                        }
                        setSideBarFilterCheckBoxex(dataParamsForSearch);
                        /*========================= PRICE FILTER INITIALIZATION START ==========================*/

                        /*************************************** SEARCH BOX NO RESULT TEMPLATE START ****************************************/
                        // Retrieve the HTML from the script tag we setup in step 1​
                        // We use the id (header) of the script tag to target it on the page​
                        var theTemplateScript = $("#mentorListingNoResult").html();

                        // The Handlebars.compile function returns a function to theTemplate variable​
                        var theTemplate = Handlebars.compile(theTemplateScript);

                        $("#searchResultDiv").html(theTemplate({}));
                        /*************************************** SEARCH BOX NO RESULT TEMPLATE START ****************************************/

                        $('#divMentorSearchListing').css("display", "none");
                        $('#hidFlagMoreData').val("N");
                    }
                    $('#hidPage').val("0");
                    setUserInitialImage(userInitialConfig);
                }
            }
            $('#hidFlagRequesting').val("N");
            $('#hidRefreshSideBar').val("Y");
        }
    });

}


function getNextPageData() {
    var hidFlagMoreData = $('#hidFlagMoreData').val();
    var hidFlagRequesting = $('#hidFlagRequesting').val();
    var hidPerPageCount = $('#hidPerPageCount').val();

    if (hidFlagMoreData == "Y" && hidFlagRequesting == "N") {

        $('#hidFlagRequesting').val("Y");

        var hidPage = parseInt(parseInt($('#hidPage').val()) + 1);

        var hidSearchParamJSON = $('#hidSearchParamJSON').val();
        var objSearchParams = JSON.parse(hidSearchParamJSON);
        objSearchParams.pageNumber = hidPage;
        hidSearchParamJSON = JSON.stringify(objSearchParams);

        $.ajax({
            url: absPath + 'mentor/listingSearch',
            method: 'POST',
            data: { data: hidSearchParamJSON },
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                $('#hidFlagRequesting').val("N");
            },
            success: function (response) {


                if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                    displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                }

                if (response['error'] == "1") {
                    displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                } else {
                    if (response['flagMsg'] == "MENTORLISTFETCH") {

                        var arrMentorListingData = response.data.arrMentorDataToDisplay;
                        var mentorCount = response.data.arrMentorDataToDisplay.length;
                        var loggedInUserId = response.data.loggedInUserId;

                        $.each(arrMentorListingData, function (index, objEle) {
                            objEle.overViewListingDisplay = htmlToStringConversion(objEle.overViewListing, 150);
                            objEle.punchLineListingDisplay = htmlToStringConversion(objEle.punchLineListing, 50);
                            objEle.professionDisplay = htmlToStringConversion(objEle.profession, 18);
                            objEle.jobDescriptionDisplay = htmlToStringConversion(objEle.jobDescription, 18);
                            objEle.company_nameDisplay = htmlToStringConversion(objEle.company_name, 18);
                        });

                        var arrListingDisplayData = {
                            loggedInUserId: loggedInUserId,
                            arrMentorListingData: arrMentorListingData,
                            inListing: "1"
                        };

                        if (mentorCount > 0) {

                            /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                            // Retrieve the HTML from the script tag we setup in step 1​
                            // We use the id (header) of the script tag to target it on the page​
                            var theTemplateScript = $("#mentorListingBoxes").html();

                            // The Handlebars.compile function returns a function to theTemplate variable​
                            var theTemplate = Handlebars.compile(theTemplateScript);

                            $("#searchResultDiv").append(theTemplate({ arrMentorData: arrListingDisplayData }));
                            /*************************************** SEARCH BOX TEMPLATE END  ****************************************/
                            if (mentorCount < hidPerPageCount) {
                                $('#hidFlagMoreData').val("N");
                            }
                        } else {
                            $('#hidFlagMoreData').val("N");
                        }
                        setUserInitialImage(userInitialConfig);
                    }
                }

                $('#hidFlagRequesting').val("N");
                $('#hidPage').val(hidPage);
            }
        });
    }

}

function redirectToListingPage(objForPostParams) {
    var redirectURI = absPath + "mentor/listing";
    $.redirect(redirectURI, objForPostParams, "GET");
}
