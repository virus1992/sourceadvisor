var global_meeting_invites = '';
var mentor_timezone = $('#mentorTimezone').val();
var userType = $('#userType').val();

$(document).ready(function () {
        $(window).scroll(function () {
        if ($('li.active a[href="#tab_receive"]').length == "1") {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var hidFutureMeetingPage = parseInt($('#hidePageReceive').val());
                hidFutureMeetingPage += 1;
                if ($('#hideReceiveData').val() == "Y") {
                    inviteData(hidFutureMeetingPage, "receive");
                    $('#hidePageReceive').val(hidFutureMeetingPage);
                }
            }
        }

        if ($('li.active a[href="#tab_sent"]').length == "1") {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var hidCompletedMeetingPage = parseInt($('#hidePageSent').val());
                hidCompletedMeetingPage += 1;
                if ($('#hideSentData').val() == "Y") {
                    inviteData(hidCompletedMeetingPage, "sent");
                    $('#hidePageSent').val(hidCompletedMeetingPage);
                }
            }
        }
    });




    $("#lstTimeZone").select2({
        data: datax,
        templateResult: function (d) {
            return $(d.text); },
        templateSelection: function (d) {
            var found = datax.find(function(element) {
                if(d.selected){
                    return element.cZone ===d.id;
                }else{
                    return element.cZone === mentor_timezone;
                }    
              });
              $('#lstTimeZone').val(found.id)
            return $(found.text); 
        },
        
    })
    $('.select2-selection__rendered').removeAttr('title');
    $("#lstTimeZone").on("change", function () {
        $.blockUI();
        mentor_timezone = $(this).find("option:selected").text();
        found = datax.find(function(element) {
            return element.text === mentor_timezone;  
        });
        mentor_timezone=found.cZone;
        $('#mentorPendingInvitesListing').html('');
        $('#mentorPendingSentInvitesListing').html('');
        generateMeetingInvitesHtml(global_meeting_invites);
        generateMeetingSentInvitesHtml(global_meeting_invites);
    });

    $.ajax({
        url: $('#absPath').val() + 'mentor/getMeetingPendingInvites',
        type: 'POST',
        beforeSend: function () {
            $.blockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.data.receivedData.length > 0) {
                $('#mentorPendingInvitesListing').show();
                $('#noMentorPendingInvitesListing').hide();
                global_meeting_invites = response.data;

                $('#mentorPendingInvitesListing').html('');
                generateMeetingInvitesHtml(response.data);

                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            } else {
                $('#mentorPendingInvitesListing').hide();
                $('#noMentorPendingInvitesListing').show();
                $.unblockUI();
            }

            if (response.data.sentData.length > 0) {
                $('#mentorPendingSentInvitesListing').show();
                $('#noMentorPendingSentInvitesListing').hide();
                global_meeting_invites = response.data;

                $('#mentorPendingSentInvitesListing').html('');
                generateMeetingSentInvitesHtml(response.data);

                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            } else {
                $('#mentorPendingSentInvitesListing').hide();
                $('#noMentorPendingSentInvitesListing').show();
                $.unblockUI();
            }

        }
    });

    $(document.body).on("click", ".sendNewInvite", function (e) {
        e.preventDefault();
        alert('Functionality Pending');
        return false;
    });

    $(document.body).on("click", ".acceptInvite", function (e) {
        e.preventDefault();

        var invitation_id = $(this).parents('.col-sm-6.item').attr('id').split('_')[1];
        var invitation_slot_id = $(this).parents('.col-sm-6.item').find('.invitationSlotSelected:checked').val();
        var transactionId = $(this).parents('.col-sm-6.item').find(".transactionId").attr('id').split('_')[1];
        var amount = $(this).parents('.col-sm-6.item').find(".amount").attr('id').split('_')[1];
        var invoiceid = $(this).parents('.col-sm-6.item').find(".invoiceid").attr('id').split('_')[1];
        var mentor_id = $(this).parents('.col-sm-6.item').find(".userid").attr('id').split('_')[1];

        if (invitation_slot_id != "" && typeof invitation_slot_id != "undefined") {

            var obj = {
                invitation_id: invitation_id,
                invitation_slot_id: invitation_slot_id,
                transactionId: transactionId,
                amount: amount,
                invoiceid: invoiceid,
                mentorId: mentor_id
            };

            chkAcceptedSlots(obj, acceptInvitation);
        } else {
            $("#selectRadoiModal").modal('toggle');
        }
    });

    /************* mentor reason pop up **************/

    $(document.body).on("click", ".declineInvite", function (e) {
        e.preventDefault();
        var invitation_id = $(this).parents('.col-sm-6.item').attr('id').split('_')[1];
        var mentor_id = $(this).parents('.col-sm-6.item').find(".mentorid").attr('id').split('_')[1];
        var user_id = $(this).parents('.col-sm-6.item').find(".userid").attr('id').split('_')[1];
        var call_duration = $(this).parents('.col-sm-6.item').find(".duration").attr('id').split('_')[1];
        var transactionId = $(this).parents('.col-sm-6.item').find(".transactionId").attr('id').split('_')[1];
        var amount = $(this).parents('.col-sm-6.item').find(".amount").attr('id').split('_')[1];
        var invoiceid = $(this).parents('.col-sm-6.item').find(".invoiceid").attr('id').split('_')[1];

        $("#invitation_id_declineMentor").val("");
        $("#mentor_id_declineUser").val("");
        $("#duration_declineUser").val("");
        $("#user_id_declineMentor").val("");
        $("#invitation_id_declineMentor").val(invitation_id);
        $("#transectionId_declineMentor").val(transactionId);
        $("#amount_declineMentor").val(amount);
        $("#invoice_declineMentor").val(invoiceid);
        $("#mentor_id_declineMentor").val(mentor_id);
        $("#user_id_declineMentor").val(user_id);
        $("#duration_declineMentor").val(call_duration);
        $("#mentorCancelModal").modal('toggle');
    });

    $("#mentor_reasontxt").hide();
    $("#mentor_chars").parent().hide();

    var maxLength = $('#mentor_chars').text();
    $('textarea#mentor_reasontxt').keyup(function () {
        var length = $.trim($(this).val()).length;
        $('#mentor_chars').text(maxLength - length);

        if (length > 0) {
            $("#mentor_reasonbtn").prop('disabled', false);
            $("#mentor_valid").css('display', 'none');
        } else {
            $("#mentor_reasonbtn").prop('disabled', true);
            $("#mentor_valid").css('display', 'block');
        }
    });


    $("#mentorReasonForm input").on('change', function () {
        var chk = $('input[name=mentor_reason]:checked', '#mentorReasonForm').val();

        if (chk == "mentor_other") {
            $("#mentor_reasontxt").show();
            $("#mentor_chars").parent().show();
            $("#mentor_reasonbtn").prop('disabled', true);
            $("#mentor_valid").css('display', 'block');
        } else {
            $("#mentor_reasontxt").hide();
            $("#mentor_chars").parent().hide();
            $("#mentor_reasonbtn").prop('disabled', false);
            $("#mentor_valid").css('display', 'none');
        }
    });

    $(document.body).on('click', "#mentor_reasonbtn", function () {
        var invitationId = $("#invitation_id_declineMentor").val();
        var duration = $("#duration_declineMentor").val();
        var mentorId = $("#mentor_id_declineMentor").val();
        var userId = $("#user_id_declineMentor").val();
        var transectionId = $("#transectionId_declineMentor").val();
        var amount = $("#amount_declineMentor").val();
        var invoiceid = $("#invoice_declineMentor").val();
        var radioval = $('input[name=mentor_reason]:checked', '#mentorReasonForm').val();
        var typex = "";
        var rval = "";


        if (radioval == "mentor_other") {
            typex = 'OT';
            rval = $("#mentor_reasontxt").val();
        } else {
            typex = 'PD';
            rval = radioval;
        }

        var data = {
            invitation_id: invitationId,
            typex: typex,
            rval: rval,
            call_duration: duration,
            mentor_id: mentorId,
            user_id: userId,
            invite: 0,
            transectionId: transectionId,
            amount: amount,
            invoiceid: invoiceid
        };

        declineInvite(data);
    });

    /************* mentor reason pop up **************/

    /***** user reason pop up ***********/

    $(document.body).on("click", ".declineInvitePrivate", function (e) {
        e.preventDefault();
        var invitation_id = $(this).parents('.col-sm-6.item').attr('id').split('_')[1];
        var mentor_id = $(this).parents('.col-sm-6.item').find(".mentorid").attr('id').split('_')[1];
        var user_id = $(this).parents('.col-sm-6.item').find(".userid").attr('id').split('_')[1];
        var call_duration = $(this).parents('.col-sm-6.item').find(".duration").attr('id').split('_')[1];

        $("#invitation_id_declineUser").val("");
        $("#mentor_id_declineUser").val("");
        $("#user_id_declineUser").val("");
        $("#duration_declineUser").val("");
        $("#invitation_id_declineUser").val(invitation_id);
        $("#mentor_id_declineUser").val(mentor_id);
        $("#user_id_declineUser").val(user_id);
        $("#duration_declineUser").val(call_duration);
        $("#UserCancelModal").modal('toggle');

    });

    $(document.body).on('click', "#user_reasonbtn", function () {
        var invitationId = $("#invitation_id_declineUser").val();
        var duration = $("#duration_declineUser").val();
        var mentorId = $("#mentor_id_declineUser").val();
        var userId = $("#user_id_declineUser").val();

        var radioval = $('input[name=user_reason]:checked', '#userReasonForm').val();
        var typex = "";
        var rval = "";


        if (radioval == "user_other") {
            typex = 'OT';
            rval = $("#user_reasontxt").val();
        } else {
            typex = 'PD';
            rval = radioval;
        }

        var data = {
            invitation_id: invitationId,
            call_duration: duration,
            mentor_id: mentorId,
            user_id: userId,
            typex: typex,
            rval: rval,
            invite: 1
        };

        declineInvitePrivate(data);
    });

    $("#user_reasontxt").hide();
    $("#user_chars").parent().hide();

    var maxLength = $('#user_chars').text();
    $('textarea#user_reasontxt').keyup(function () {
        var length = $.trim($(this).val()).length;
        $('#user_chars').text(maxLength - length);

        if (length > 0) {
            $("#user_reasonbtn").prop('disabled', false);
            $("#user_valid").css('display', 'none');
        } else {
            $("#user_reasonbtn").prop('disabled', true);
            $("#user_valid").css('display', 'block');
        }
    });


    $("#userReasonForm input").on('change', function () {
        var chk = $('input[name=user_reason]:checked', '#userReasonForm').val();

        if (chk == "user_other") {
            $("#user_reasontxt").show();
            $("#user_chars").parent().show();
            $("#user_reasonbtn").prop('disabled', true);
            $("#user_valid").css('display', 'block');
        } else {
            $("#user_reasontxt").hide();
            $("#user_chars").parent().hide();
            $("#user_reasonbtn").prop('disabled', false);
            $("#user_valid").css('display', 'none');
        }
    });

    /***** user reason pop up ***********/

    $(document.body).on("click", ".cancel", function (e) {
        e.preventDefault();

        var invitation_id = $(this).parents('.col-sm-6.item').attr('id').split('_')[1];
        var mentor_id = $(this).parents('.col-sm-6.item').find(".mentorid").attr('id').split('_')[1];
        var user_id = $(this).parents('.col-sm-6.item').find(".userid").attr('id').split('_')[1];
        var call_duration = $(this).parents('.col-sm-6.item').find(".duration").attr('id').split('_')[1];
        var meetingType = $(this).parents('.col-sm-6.item').find(".meetingType").attr('id').split('_')[1];
        var transactionId = $(this).parents('.col-sm-6.item').find(".transactionId").attr('id').split('_')[1];
        var amount = $(this).parents('.col-sm-6.item').find(".amount").attr('id').split('_')[1];
        var invoiceid = $(this).parents('.col-sm-6.item').find(".invoiceid").attr('id').split('_')[1];

        $.ajax({
            url: $('#absPath').val() + 'users/cancel',
            type: 'POST',
            data: {
                invitation_id: invitation_id,
                mentor_id: mentor_id,
                call_duration: call_duration,
                meetingType: meetingType,
                user_id: user_id,
                transactionId: transactionId,
                amount: amount,
                invoiceid: invoiceid
            },
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.flagMsg == 'USRNLOGIN') {
                    alert('User session has expired. Please login and try again.');
                    window.location.href = $('#absPath').val() + 'login';
                    return false;
                }

                if (response.flagMsg == 'success') {
                    $('#invitationid_' + invitation_id).remove();
                    if ($('#mentorPendingInvitesListing .col-sm-6.item').length == 0) {
                        $('#mentorPendingInvitesListing').hide();
                        $('#noMentorPendingInvitesListing').show();
                    } else {
                        var $container = $('.masonry-container');
                        $container.masonry({
                            columnWidth: '.item',
                            itemSelector: '.item'
                        });
                    }
                } else if (response.flagMsg == 'error') {
                    displayAjaxNotificationMessage('Some unexpected error occured. Please try again.', 'error')
                }
            }
        });
    });

    $(document.body).on("click", ".acceptInvitePrivate", function (e) {
        e.preventDefault();

        var invitation_id = $(this).parents('.col-sm-6.item').attr('id').split('_')[1];
        var invitation_slot_id = $(this).parents('.col-sm-6.item').find('.invitationSlotSelected:checked').val();
        var mentor_id = $(this).parents('.col-sm-6.item').find(".userid").attr('id').split('_')[1];
        var invoiceId = $(this).parents('.col-sm-6.item').find(".invoiceid").attr('id').split('_')[1];
        var call_duration = $(this).parents('.col-sm-6.item').find(".duration").attr('id').split('_')[1];
        var mentorSlug = $(this).parents('.col-sm-6.item').find(".mentorslug").attr('id').split('_')[1];

        var objParams = {mentorId: mentor_id, slots: "", duration: call_duration};
        if (invitation_slot_id != "" && typeof invitation_slot_id != "undefined") {
            var objForPostParams = {
                invoiceId: invoiceId,
                mentorSlug: mentorSlug,
                invitation_id: invitation_id,
                invitation_slot_id: invitation_slot_id,
                privateInvite: 1,
                mentorId: mentor_id,
                objParams: JSON.stringify(objParams)
            };

            chkAcceptedSlots(objForPostParams, function (objForPostParams) {
                var redirectURI = $('#absPath').val() + 'users/slot_booking_payment';
                $.redirect(redirectURI, objForPostParams, "POST");
            });
        } else {
            $("#selectRadoiModal").modal('toggle');
        }
    });


    $(document.body).on('click', '.download a.filenotexist', function (e) {
        e.preventDefault();
        return false;
    });

    $('.meeting_invites_tab a').on('shown.bs.tab', function (event) {
        var $container = $('.masonry-container');
        $container.masonry({
            columnWidth: '.item',
            itemSelector: '.item'
        });
    });

    if (userType == "user") {
        $(".tzdpdw .select2").css("float", "right");
    }


});

function inviteData(page, datatype) {

    var data = {
        page: page,
        datatype: datatype
    }


    $.ajax({
        url: $('#absPath').val() + 'mentor/inviteDataPaging',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var inviteReceivedCount = response.data.receivedData.length;
            var inviteSentCount = response.data.sentData.length;
            if (datatype != "sent") {
                if (inviteReceivedCount > 0) {
                    generateMeetingInvitesHtml(response.data);
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                } else {
                    $('#hideReceiveData').val('N');
                }
            } else {
                if (inviteSentCount > 0) {
                    generateMeetingSentInvitesHtml(response.data);
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                } else {
                    $('#hideSentData').val('N');
                }
            }

        }
    });
}

function generateMeetingInvitesHtml(data) {
    if(data.receivedData!==undefined){
        if (data.receivedData.length == 0) {
            $('#mentorPendingInvitesListing').hide();
            $('#noMentorPendingInvitesListing').show();
        } else {
            $('#mentorPendingInvitesListing').show();
            $('#noMentorPendingInvitesListing').hide();
    
            $.each(data.receivedData, function (index, item) {
                var newItem = $('#hiddenListingToClone').clone(true);
                newItem.removeAttr('id').removeAttr('style').attr('id', 'invitationid_' + item.invitation_id);
    
                if (item.invitation_type == 0) {
                    newItem.find('.mentorid').attr("id", 'mentor-id_' + item.user_id);
                    newItem.find('.userid').attr("id", 'user-id_' + item.requester_id);
                } else {
                    newItem.find('.mentorid').attr("id", 'mentor-id_' + item.requester_id);
                    newItem.find('.userid').attr("id", 'user-id_' + item.user_id);
                }
                newItem.find('.invoiceid').attr("id", 'invoice-id_' + item.invoice_id);
                newItem.find('.duration').attr("id", 'duration_' + item.duration);
                newItem.find('.mentorslug').attr("id", 'mentor-slug_' + item.slug);
                newItem.find('.transactionId').attr("id", 'transactionId_' + item.transaction_id);
                newItem.find('.amount').attr("id", 'amount_' + item.amount);
    
                newItem.find('#fileAttached').attr('id', 'fileAttached_' + item.invitation_id);
                newItem.find('a[data-parent="#fileAttached"]').attr('data-parent', '#fileAttached_' + item.invitation_id);
    
                newItem.find('#fileAttachedDiv').attr('id', 'fileAttachedDiv_' + item.invitation_id);
                newItem.find('a[href="#fileAttachedDiv"]').attr('href', '#fileAttachedDiv_' + item.invitation_id);
    
                newItem.find('.image img').attr("src", item.finalProfileImageUrl);
                newItem.find('.mentSumName').html(item.first_name + " " + item.last_name);
                newItem.find('.recievedTime').html(moment.tz(item.created_date, "UTC").tz(mentor_timezone).fromNow());
                newItem.find('.meetingTypeText').html(item.meeting_type);
                newItem.find('.meetingTimeText').html(item.duration);
                if (item.invitation_type == 0) {
                    newItem.find('.meetingPurposeText').html(item.meeting_purpose);
                } else {
                    newItem.find('.meetingp').hide();
                }
    
                if (item.meeting_purpose_indetail == "" || item.meeting_purpose_indetail == null || item.meeting_purpose_indetail == ".") {
                    newItem.find('.meetingd').hide();
                } else {
                    newItem.find('.meetingPurposeDetailText').html(item.meeting_purpose_indetail).prev().addClass('pull-left');
                }
    
    
                if (item.invitation_type == 0) {
                    newItem.find('.accept').addClass('acceptInvite').removeClass('accept').attr('invitation-id', item.invitation_id);
                    newItem.find('.decline').addClass('declineInvite').removeClass('decline').attr('invitation-id', item.invitation_id);
                } else {
                    newItem.find('.accept').addClass('acceptInvitePrivate').removeClass('accept').attr('invitation-id', item.invitation_id);
                    newItem.find('.decline').addClass('declineInvitePrivate').removeClass('decline').attr('invitation-id', item.invitation_id);
    
                }
    
                newItem.find('.invite').addClass('sendNewInvite').removeClass('invite').attr('invitation-id', item.invitation_id);
                newItem.find('.cmodal');
    
                $.each(item.invitation_slots, function (slot_index, slot_item) {
                    var starttext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).format('Do MMMM, Y hh:mm A');
                    var endtext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).add(item.duration, 'minutes').format('hh:mm A');
                    var disabled = (moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone) > moment.tz(mentor_timezone)) ? '' : 'disabled';
    
                    var slot_html = '<li><input ' + disabled + ' type="radio" name="invitation_' + item.invitation_id + '" class="mt2 mr5 invitationSlotSelected" value="' + slot_item.id + '">' + starttext + ' to ' + endtext + '</li>';
                    newItem.find('.slot').append(slot_html);
                });
    
                if (item.invitation_documents.length == 0) {
                    newItem.find('.PendingMeetingInvites').addClass('hidden');
                } else {
                    $.each(item.invitation_documents, function (doc_index, doc_item) {
                        var document_html = '<li class="mb0">';
                        document_html += '<div class="download">';
                        if (doc_item.isFileExist) {
                            document_html += '<a href="' + $('#absPath').val() + doc_item.document_path.replace("uploads/", "") + '?dl"><i class="fa fa-arrow-down"></i></a>';
                        } else {
                            document_html += '<a href="#" class="filenotexist"><i class="fa fa-times"></i></a>';
                        }
                        document_html += '</div>';
                        document_html += '<div class="detail">';
                        document_html += '<p class="filename1">' + (doc_item.document_real_name != "" ? doc_item.document_real_name : doc_item.document_name) + '</p>';
                        if (doc_item.isFileExist) {
                            document_html += '<span class="filesize1">' + bytesToSize(doc_item.document_size) + '</span>';
                        } else {
                            document_html += '<span class="filesize1">File Not Found</span>';
                        }
                        document_html += '</div>';
                        document_html += '</li>';
                        newItem.find('.SharedDocuments ul').append(document_html);
                    });
                }
    
                $('#mentorPendingInvitesListing').append(newItem);
                $('#fileAttached_' + item.invitation_id).on('shown.bs.collapse', function () {
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                }).on('hidden.bs.collapse', function () {
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                });
            });
        }
        $.unblockUI();
    }else{
        $.unblockUI();
    }
    
}
function generateMeetingSentInvitesHtml(data) {
    if(data.sentData!==undefined){
    if (data.sentData.length == 0) {
        $('#mentorPendingSentInvitesListing').hide();
        $('#noMentorPendingSentInvitesListing').show();
    } else {
        $('#mentorPendingSentInvitesListing').show();
        $('#noMentorPendingSentInvitesListing').hide();

        $.each(data.sentData, function (index, item) {
            var newItem = $('#hiddenListingToClone').clone(true);
            newItem.removeAttr('id').removeAttr('style').attr('id', 'invitationid_' + item.invitation_id);

            newItem.find('#fileAttached').attr('id', 'fileAttached_' + item.invitation_id);
            newItem.find('a[data-parent="#fileAttached"]').attr('data-parent', '#fileAttached_' + item.invitation_id);


            if (item.invitation_type == 0) {
                newItem.find('.mentorid').attr("id", 'mentor-id_' + item.user_id);
                newItem.find('.userid').attr("id", 'user-id_' + item.requester_id);
            } else {
                newItem.find('.mentorid').attr("id", 'mentor-id_' + item.requester_id);
                newItem.find('.userid').attr("id", 'user-id_' + item.user_id);
            }

            newItem.find('#fileAttachedDiv').attr('id', 'fileAttachedDiv_' + item.invitation_id);
            newItem.find('a[href="#fileAttachedDiv"]').attr('href', '#fileAttachedDiv_' + item.invitation_id);

            newItem.find('.invoiceid').attr("id", 'invoice-id_' + item.invoice_id);
            newItem.find('.duration').attr("id", 'duration_' + item.duration);
            newItem.find('.mentorslug').attr("id", 'mentor-slug_' + item.slug);
            newItem.find('.meetingType').attr("id", 'meeting-type_' + item.invitation_type);
            newItem.find('.transactionId').attr("id", 'transactionId_' + item.transaction_id);
            newItem.find('.amount').attr("id", 'amount_' + item.amount);

            newItem.find('.image img').attr("src", item.finalProfileImageUrl);
            newItem.find('.mentSumName').html(item.first_name + " " + item.last_name);
            newItem.find('.mentSumName').html('<a href="' + $('#absPath').val() + 'mentorprofile/' + item.mslug + '" target="_blank">' + item.first_name + " " + item.last_name + '</a>');
            newItem.find('.recievedTime').html(moment.tz(item.created_date, "UTC").tz(mentor_timezone).fromNow());
            newItem.find('.meetingTypeText').html(item.meeting_type);
            newItem.find('.meetingTimeText').html(item.duration);
            if (item.invitation_type == 0) {
                newItem.find('.meetingPurposeText').html(item.meeting_purpose);
            } else {
                newItem.find('.meetingp').hide();
            }

            if (item.meeting_purpose_indetail == "" || item.meeting_purpose_indetail == null || item.meeting_purpose_indetail == ".") {
                newItem.find('.meetingd').hide();
            } else {
                newItem.find('.meetingPurposeDetailText').html(item.meeting_purpose_indetail).prev().addClass('pull-left');
            }


            newItem.find('.accept').hide();
            newItem.find('.cmodal').hide();
            newItem.find('.decline').hide();
            newItem.find('.invite').hide();
            newItem.find('.cancel').show();
            var count = 1;
            $.each(item.invitation_slots, function (slot_index, slot_item) {
                var starttext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).format('Do MMMM, Y hh:mm A');
                var endtext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).add(item.duration, 'minutes').format('hh:mm A');
                var disabled = (moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone) > moment.tz(mentor_timezone)) ? '' : 'disabled';

                var slot_html = '<li> <b>Slot' + count + " :</b> " + starttext + ' to ' + endtext + '</li>';
                newItem.find('.slot').append(slot_html);
                count++;
            });

            if (item.invitation_documents.length == 0) {
                newItem.find('.PendingMeetingInvites').addClass('hidden');
            } else {
                $.each(item.invitation_documents, function (doc_index, doc_item) {
                    var document_html = '<li class="mb0">';
                    document_html += '<div class="download">';
                    if (doc_item.isFileExist) {
                        document_html += '<a href="' + $('#absPath').val() + doc_item.document_path.replace("uploads/", "") + '?dl"><i class="fa fa-arrow-down"></i></a>';
                    } else {
                        document_html += '<a href="#" class="filenotexist"><i class="fa fa-times"></i></a>';
                    }
                    document_html += '</div>';
                    document_html += '<div class="detail">';
                    document_html += '<p class="filename1">' + (doc_item.document_real_name != "" ? doc_item.document_real_name : doc_item.document_name) + '</p>';
                    if (doc_item.isFileExist) {
                        document_html += '<span class="filesize1">' + bytesToSize(doc_item.document_size) + '</span>';
                    } else {
                        document_html += '<span class="filesize1">File Not Found</span>';
                    }
                    document_html += '</div>';
                    document_html += '</li>';
                    newItem.find('.SharedDocuments ul').append(document_html);
                });
            }

            $('#mentorPendingSentInvitesListing').append(newItem);
            $('#fileAttached_' + item.invitation_id).on('shown.bs.collapse', function () {
                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            }).on('hidden.bs.collapse', function () {
                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            });
        });
    }
    $.unblockUI();
}else{
    $.unblockUI();
}
}

function declineInvite(data) {

    $.ajax({
        url: $('#absPath').val() + 'users/declineInvitation',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.flagMsg == 'success') {
                $("#mentorCancelModal").modal('toggle');
                $('#invitationid_' + data.invitation_id).remove();
                if ($('#mentorPendingInvitesListing .col-sm-6.item').length == 0) {
                    $('#mentorPendingInvitesListing').hide();
                    $('#noMentorPendingInvitesListing').show();
                } else {
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                }
            } else if (response.flagMsg == 'error') {
                $("#mentorCancelModal").modal('toggle');
                displayAjaxNotificationMessage('Some unexpected error occured. Please try again.', 'error');
            }
        }
    });
}

function declineInvitePrivate(data) {

    $.ajax({
        url: $('#absPath').val() + 'users/declineInvitation',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.flagMsg == 'success') {
                $("#UserCancelModal").modal('toggle');
                $('#invitationid_' + data.invitation_id).remove();
                if ($('#mentorPendingInvitesListing .col-sm-6.item').length == 0) {
                    $('#mentorPendingInvitesListing').hide();
                    $('#noMentorPendingInvitesListing').show();
                } else {
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                }
            } else if (response.flagMsg == 'error') {
                displayAjaxNotificationMessage('Some unexpected error occured. Please try again.', 'error')
            }
        }
    });
}

function acceptInvitation(obj) {

    var invitation_id = obj.invitation_id;
    var invitation_slot_id = obj.invitation_slot_id;
    var transactionId = obj.transactionId;
    var amount = obj.amount;
    var invoiceid = obj.invoiceid;
    var mentorId = obj.mentorId;

    if (invitation_slot_id != "" && typeof invitation_slot_id != "undefined" && transactionId != "") {
        $.ajax({
            url: $('#absPath').val() + 'mentor/acceptInvitation',
            type: 'POST',
            data: {
                invitation_id: invitation_id,
                invitation_slot_id: invitation_slot_id,
                transactionId: transactionId,
                amount: amount,
                invoiceid: invoiceid,
                mentorId: mentorId
            },
            beforeSend: function (xhr) {
//                $.blockUI();
                showLoader();
            },
//            complete: function (jqXHR, textStatus) {
//                $.unblockUI();
//            },
            success: function (response) {

                if (response.flagMsg == 'USRNLOGIN') {
                    alert('User session has expired. Please login and try again.');
                    window.location.href = $('#absPath').val() + 'login';
                    return false;
                }

                if (response.flagMsg == 'success' && response.data != 'break') {
                    $('#invitationid_' + invitation_id).remove();

                    if ($('#mentorPendingInvitesListing .col-sm-6.item').length == 0) {
                        $('#mentorPendingInvitesListing').hide();
                        $('#noMentorPendingInvitesListing').show();
                    } else {
                        var $container = $('.masonry-container');
                        $container.masonry({
                            columnWidth: '.item',
                            itemSelector: '.item'
                        });
                    }
//                    $.unblockUI();
                    hideLoader();
                    var win = window.open($('#absPath').val() + 'mentor/thankyou/' + invitation_id, '_blank');
                } else if (response.flagMsg == 'success' && response.data == 'break') {
                    $.unblockUI();
                    displayAjaxNotificationMessage('Slot is already accepted.', 'error');
                } else if (response.flagMsg == 'error') {
                    $.unblockUI();
                    displayAjaxNotificationMessage('Some unexpected error occured. Please try again.', 'error');
                }
            }
        });
    } else {
        alert("Please select a suitable slot before accepting the invite.");
    }
}


function chkAcceptedSlots(obj, callback) {

    var invitation_id = obj.invitation_id;
    var invitation_slot_id = obj.invitation_slot_id;
    var transactionId = obj.transactionId;
    var mentorId = obj.mentorId;

    $.ajax({
        url: $('#absPath').val() + 'mentor/chkAcceptedSlots',
        type: 'POST',
        data: {
            invitation_id: invitation_id,
            invitation_slot_id: invitation_slot_id,
            mentorId: mentorId,
            transactionId: transactionId
        },
        beforeSend: function (xhr) {
            //$.blockUI();
        },
//        complete: function (jqXHR, textStatus) {
//            $.unblockUI();
//        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.flagMsg == "success" && response.data != "already") {
                if (typeof callback == 'function') {
                    $.unblockUI();
                    callback(obj);
                }
            } else if (response.flagMsg == "success" && response.data == "already") {
                $.unblockUI();
                displayAjaxNotificationMessage('Slot is already accepted.', 'error');
            } else {
                $.unblockUI();
                displayAjaxNotificationMessage('Something went wrong try again.', 'error');
            }
        }
    });
}

function showLoader() {
    $('.loader').removeClass('hide');
    $("#bar").width(600);
    var progress = setInterval(function () {
        var $bar = $("#bar");

        if ($bar.width() >= 600) {
            clearInterval(progress);
        } else {
            $bar.width($bar.width() + 60);
        }
        $bar.text(($bar.width() / 6).toFixed(2) + "%");
        if ($bar.width() / 6 == 100) {
            $bar.text("Still working ... " + $bar.width() / 6 + "%");
        }
    }, 1);

}
function hideLoader() {
    $('.loader').addClass('hide');
}