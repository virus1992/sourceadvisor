var events = "";
var slotTime = "00:30:00";
var recurringOBJ = {};
$('#addAvail').on('hidden.bs.modal', function () {
    $('#add_Avail')[0].reset();
    $("#recurring").trigger("click");
    $("#endDate").show();
    $("#recurring").removeAttr("disabled");
    $("#onetime").removeAttr("disabled");
    $("#delSDate").val('');
    $("#delEDate").val('');
    initDateTime();

    $(".chkl").show();
    $('[type="checkbox"]').attr('checked', false);
    $(".checkbox-inline").show();
    $(".endTypeRadio").show();
    $("#addbtn").show();
    $("#updbtn").hide();
    $("#parent_id").val("");
    $("#main_id").val("");
});

$('#privateInvite').on('hidden.bs.modal', function () {
    $('#privateInviteTimePicker1').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '12:00'
    });
    $('#privateInviteTimePicker2').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '13:00'
    });
    $('#privateInviteTimePicker3').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker4').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker5').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker6').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $("#privateInviteTimePicker1").timepicker('setTime', "12:00");
    $("#privateInviteTimePicker2").timepicker('setTime', "13:00");
    $("#callDate2").val('');
    $("#callDate3").val('');
    getUserList();
});


$('#update_privateInvite').on('hidden.bs.modal', function () {
    $('#update_privateInviteTimePicker1').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '12:00'
    });
    $('#update_privateInviteTimePicker2').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#update_privateInviteTimePicker3').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $("#update_privateInviteTimePicker1").timepicker('setTime', "12:00");
    $("#update_callDate1").val('');
    $("#update_callDate2").val('');
    $("#update_callDate3").val('');
    $("#update_privateInviteMsg").text('');
    getUserList();
});
function fTimeZone(zone, cb) {
    return datax.find(function (element) {
        return element.text === zone;
    }).cZone;
}
$(document).ready(function () {

    /*$("#add2div").on('click',function(){
     $("#slot2div").css("display","block");
     $("#add2div").css("display","none");
     $("#remove2div").css("display","block");
     });*/

    $("#resetFrm").on('click', function () {
        $('#sendInvite')[0].reset();
        $('#lstUserList').val('');
        $('#lstUserList').trigger('change');
    });

    $("#set_btn").on('click', function () {
        $('#update_callDate1').datepicker('setDate', null);
        $('#update_callDate2').datepicker('setDate', null);
        $('#update_callDate3').datepicker('setDate', null);
    });

    $("#callDate2").on('change', function () {
        var value = $(this).val();

        if (value == "") {
            $("#callDate3").attr("disabled", "disabled");
        } else {
            $("#callDate3").removeAttr("disabled");
        }

    });

    $("#update_callDate2").on('change', function () {
        var value = $(this).val();

        if (value == "") {
            $("#update_callDate3").attr("disabled", "disabled");
        } else {
            $("#update_callDate3").removeAttr("disabled");
        }

    });

    $('#privateInviteTimePicker1').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '12:00'
    });
    $('#privateInviteTimePicker2').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '13:00'
    });
    $('#privateInviteTimePicker3').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker4').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker5').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#privateInviteTimePicker6').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });


    $('#update_privateInviteTimePicker1').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '12:00'
    });
    $('#update_privateInviteTimePicker2').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });
    $('#update_privateInviteTimePicker3').timepicker({
        showMeridian: false,
        showInputs: true,
        defaultTime: '00:00'
    });



    $("#privateInviteTimePicker1").timepicker('setTime', "12:00");
    $("#privateInviteTimePicker2").timepicker('setTime', "13:00");


    // Select "one time by default"

    $('#addAvailButton').on('click', function () {
        $("#onetime").prop("checked", true).trigger('click');
    });

    jQuery.validator.addMethod("greaterThan", function (value, element, params) {
        if (!/Invalid|NaN/.test(new Date(value))) {
            return new Date(value) >= new Date($(params).val());
        }
        return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
    }, 'Must be greater than Start date.');
    jQuery.validator.addMethod("greaterThanTime", function (value, element, params) {

        if (!/Invalid|NaN/.test(value)) {
            return value > $(params).val();
        }

        return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
    }, 'Must be greater than From Time.');

    /* On document load initialize basic calendar function  */
    //intCal(slotTime, events);
    getUserList(); //get user list for send invitation

    // On timezone selection change mentor event timings accordingly
    $("#lstTimeZone").select2({
        data: datax,
        templateResult: function (d) {
            return $(d.text);
        },
        templateSelection: function (d) {
            var found = datax.find(function (element) {
                if (d.selected) {
                    return element.cZone === d.cZone;
                } else {
                    return element.cZone === element.selectedId;
                }
            });
            $('#lstTimeZone').val(found.id)
            return $(found.text);
        },

    })
    $('.select2-selection__rendered').removeAttr('title');
    $(document).on('change', '#lstTimeZone', function () {
        var selectedTz = fTimeZone($(this).find("option:selected").text());
        $('.updateTz').val(selectedTz);
        $('#zone').val(selectedTz);
        getMentorEvents(selectedTz);
    });

    getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text())); //get mentor events

    //initialize date and time picker
    initDateTime();
    //Call to googleSync function 

    $("#gSync").on('click', function () {
        googleSync();
    });
    //Hide and show the fields according radio btn checked in add availalibity.

    $('input:radio[name="availType"]').on("click", function () {
        if ($(this).is(':checked')) {
            if ($(this).val() == "onetime") {
                $(".checkbox-inline").hide();
                $(".endTypeRadio").hide();
                $(".chkl").hide();
                //                $("#endDate").hide();
                $("#onetimeendDate").hide();

                //                $("#lableEndDate").hide();
                avail.resetForm();
                $("#timepicker1").val("12:00");
                $("#timepicker2").val("13:00");
                $("#onetime").prop("checked", true);
            } else {
                $(".chkl").show();
                $(".checkbox-inline").show();
                $(".endTypeRadio").show();
                $("#lableEndDate").show();
                $("#endDate").show();
                avail.resetForm();
                $("#timepicker1").val("12:00");
                $("#timepicker2").val("13:00");
                $("#recurring").prop("checked", true);
            }
        }
    });
    $('input:radio[name="availType"]').on("change", function () {
        if ($(this).is(':checked')) {
            if ($(this).val() == "onetime") {
                $(".checkbox-inline").hide();
                $(".endTypeRadio").hide();
                $(".chkl").hide();
                //                $("#endDate").hide();
                $("#onetimeendDate").hide();

                //                $("#lableEndDate").hide();
                avail.resetForm();
                $("#timepicker1").val("12:00");
                $("#timepicker2").val("13:00");
                $("#onetime").prop("checked", true);
            } else {
                $(".chkl").show();
                $(".checkbox-inline").show();
                $(".endTypeRadio").show();
                $("#lableEndDate").show();
                $("#endDate").show();
                avail.resetForm();
                $("#timepicker1").val("12:00");
                $("#timepicker2").val("13:00");
                $("#recurring").prop("checked", true);
            }
        }
    });
    // Create Event => when radio of end date changes
    $('input:radio[name="endType"]').on("change", function () {
        if ($(this).is(':checked')) {
            if ($(this).val() == "neverend") {
                $("#endDate").hide();
            } else {
                $("#endDate").show();
            }
        }
    });
    // Update Event => when radio of end date changes
    $('input:radio[name="endTypeUpdate"]').on("change", function () {
        if ($(this).is(':checked')) {
            if ($(this).val() == "neverend") {
                $("#endDateUpdate").hide();
            } else {
                $("#endDateUpdate").show();
            }
        }
    });
    //send invitation validation and saving data

    $('#sendInvite').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            lstUserList: {
                required: true,
                noBlankSpace: true
            },
            callDate1: {
                required: true,
                noBlankSpace: true
            },
            privateInviteTimePicker1: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            lstUserList: {
                required: 'Please select one of the user'
            },
            callDate1: {
                required: "Please select date"
            },
            privateInviteTimePicker1: {
                required: "Please select start time"
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'lstUserList') {
                element.parent().append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'mentor/send_private_invitation',
                method: "POST",
                data: $('#sendInvite').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == "already") {
                        var strSlotCounts = "";
                        if (response.data.length > 0) {
                            strSlotCounts += "<ul>";
                        }
                        $.each(response.data, function (index, value) {
                            strSlotCounts += "<li>Slot number " + value + " is already occupied.</li>";
                        });
                        if (response.data.length > 0) {
                            strSlotCounts += "</ul>";
                        }
                        displayAjaxNotificationMessagePopUp(strSlotCounts, "danger");
                    } else if (response.message == "minimumSlotDiff") {
                        var strSlotCounts = "";
                        if (response.data.length > 0) {
                            strSlotCounts += "<ul>";
                        }
                        var strTmp = "";
                        $.each(response.data, function (index, value) {
                            strTmp += value + ", ";
                            //strSlotCounts += "<li>Slot number " + value + " is already occupied.</li>";
                        });

                        strSlotCounts += "<li>Meeting book must be after " + response.minimumExpectedSlot + ", Please change slot number " + strTmp + ".</li>";

                        if (response.data.length > 0) {
                            strSlotCounts += "</ul>";
                        }
                        displayAjaxNotificationMessagePopUp(strSlotCounts, "danger");
                    } else if (response.message == "success") {
                        $('#sendInvite')[0].reset();
                        $('#privateInvite').modal('toggle');
                        displayAjaxNotificationMessage("Invitation sent successfully.", "success");
                        getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                    } else {
                        $('#sendInvite')[0].reset();
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                        getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#sendInvite')[0].reset();
                    $('#privateInvite').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                }
            });
            return false;
        }
    });

    $("#update_btn").on('click', function () {
        $('#update_sendInvite').validate({
            errorClass: "validate_error",
            ignore: 'input[type=hidden]',
            rules: {
                update_lstUserList: {
                    required: true,
                    noBlankSpace: true
                },
                update_callDate1: {
                    required: true,
                    noBlankSpace: true
                },
                update_privateInviteTimePicker1: {
                    required: true,
                    noBlankSpace: true
                }
            },
            messages: {
                update_lstUserList: {
                    required: 'Please select one of the user'
                },
                update_callDate1: {
                    required: "Please select date"
                },
                update_privateInviteTimePicker1: {
                    required: "Please select start time"
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr('id') == 'update_lstUserList') {
                    element.parent().append(error);
                } else {
                    element.after(error);
                }
            },
            submitHandler: function (form) {
                $.blockUI();
                $.ajax({
                    url: $('#absPath').val() + 'mentor/update_private_invitation',
                    method: "POST",
                    data: $('#update_sendInvite').serialize(),
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        if (response.message == "already") {

                        } else if (response.message == "success") {
                            $("#update_privateInvite").modal('toggle');
                            displayAjaxNotificationMessage("Invitation updated successfully.", "success");
                            getMentorEvents();
                        } else {

                        }
                    },
                    error: function (response) {

                    }
                });
                return false;
            }
        });
    });

    $("#delete_btn").on('click', function () {
        $.ajax({
            url: $('#absPath').val() + 'mentor/delete_private_invitation',
            method: "POST",
            data: $('#update_sendInvite').serialize(),
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.message == "already") {

                } else if (response.message == "success") {
                    $("#update_privateInvite").modal('toggle');
                    displayAjaxNotificationMessage("Invitation deleted successfully.", "success");
                    getMentorEvents();
                } else {

                }
            },
            error: function (response) {

            }
        });
        return false;
    });
    //Add availability validation and saving data

    var avail = $('#add_Avail').validate({
        errorClass: "validate_error",
        ignore: ":hidden",
        rules: {
            //            'week[]': {
            //                required: true,
            //                noBlankSpace: true
            //            },
            startDate: {
                required: true,
                noBlankSpace: true
            },
            endDate: {
                required: true,
                noBlankSpace: true,
                greaterThan: "#startDate"
            },
            timepicker1: {
                required: true,
                noBlankSpace: true
            },
            timepicker2: {
                required: true,
                noBlankSpace: true,
                //                 greaterThanTime: "#timepicker1"
            }
        },
        messages: {
            //            'week[]': {
            //                required: 'Please select atleast one day from week'
            //            },
            startDate: {
                required: "Please select start date"
            },
            endDate: {
                required: "Please select end date"
            },
            timepicker1: {
                required: "Please select start time"
            },
            timepicker2: {
                required: "Please select end time"
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr('name') == 'week[]' || element.attr('name') == 'timepicker1' || element.attr('name') == 'timepicker2') {
                element.parent().parent().append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'mentor/addAvailability',
                method: "POST",
                data: $('#add_Avail').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == "success") {
                        $('#add_Avail')[0].reset();
                        $("#endDate").show();
                        initDateTime();

                        $(".chkl").show();
                        $(".checkbox-inline").show();
                        $(".endTypeRadio").show();
                        $('#addAvail').modal('toggle');
                        displayAjaxNotificationMessage("Availability added successfully.", "success");
                        getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#add_Avail')[0].reset();
                    $("#endDate").show();
                    initDateTime();
                    $(".chkl").show();
                    $(".checkbox-inline").show();
                    $(".endTypeRadio").show();
                    $("#timepicker1").val("12:00");
                    $("#timepicker2").val("12:00");
                    $('#addAvail').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }
            });
            return false;
        }
    });

    //To delete any single mentor availability event from calender
    $("#deleteUpdate").on('click', function () {
        var id = $("#eventId").val();
        var timeZone = fTimeZone($('#lstTimeZone').find("option:selected").text());
        if (recurringOBJ.repeat_type === 1) {
            var delSDate = $("#delSDate").val();
            var delEDate = $("#delEDate").val();

            var data = {
                eventId: id,
                type: 'recurring',
                delSDate: delSDate,
                delEDate: delEDate,
                tz: timeZone,
            };
        } else {
            var data = {
                eventId: id,
                type: 'one_time'
            }
        }

        $.ajax({
            url: $('#absPath').val() + 'mentor/deleteCalEvent',
            method: "POST",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
                getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
            },
            success: function (response) {
                $('#updateAvail').modal('toggle');
                displayAjaxNotificationMessage("Event deleted successfully.", "success");
            },
            error: function (response) {
                $(".alert-danger").show();
            }
        });
    });

    $("#deleteAllUpdate").on('click', function () {
        var id = $("#eventId").val();
        var data = {
            eventId: id,
            type: 'one_time'
        }
        $.ajax({
            url: $('#absPath').val() + 'mentor/deleteAllRecuring',
            method: "POST",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
                getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
            },
            success: function (response) {
                $('#updateAvail').modal('toggle');
                displayAjaxNotificationMessage("Event deleted successfully.", "success");
            },
            error: function (response) {
                $(".alert-danger").show();
            }
        });
    });


    $("#deleteAll").on('click', function () {
        var id = $("#main_id").val();
        var data = {
            eventId: id,
            type: 'one_time'
        }

        $.ajax({
            url: $('#absPath').val() + 'mentor/deleteCalEvent',
            method: "POST",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
                getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
            },
            success: function (response) {
                $('#updateAvail').modal('toggle');
                displayAjaxNotificationMessage("Event deleted successfully.", "success");
            },
            error: function (response) {
                $(".alert-danger").show();
            }
        });
    });
    $("#updateEventButton").on('click', function () {
        $('#update_Avail').validate({
            errorClass: "validate_error",
            ignore: ":hidden",
            rules: {
                'week[]': {
                    required: true,
                    noBlankSpace: true
                },
                startDateUpdate: {
                    required: true,
                    noBlankSpace: true
                },
                endDateUpdate: {
                    required: true,
                    noBlankSpace: true,
                    greaterThan: "#startDateUpdate"
                },
                fromTimeUpdate: {
                    required: true,
                    noBlankSpace: true
                },
                toTimeUpdate: {
                    required: true,
                    noBlankSpace: true,
                }
            },
            messages: {
                'week[]': {
                    required: 'Please select atleast one day from week'
                },
                startDateUpdate: {
                    required: "Please select start date"
                },
                endDateUpdate: {
                    required: "Please select end date"
                },
                fromTimeUpdate: {
                    required: "Please select start time"
                },
                toTimeUpdate: {
                    required: "Please select end time"
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr('name') == 'week[]' || element.attr('name') == 'fromTimeUpdate' || element.attr('name') == 'toTimeUpdate') {
                    element.parent().parent().append(error);
                } else {
                    element.after(error);
                }
            },
            submitHandler: function (form) {
                $.blockUI();
                $.ajax({
                    url: $('#absPath').val() + 'mentor/updateAvailability',
                    method: "POST",
                    data: $('#update_Avail').serialize(),
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                        $('#updateAvail').modal('toggle');
                    },
                    success: function (response) {
                        if (response.message == "success") {
                            displayAjaxNotificationMessage("Event Updated Successfully.", "success");
                            getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                        }
                    },
                    error: function (response) {
                        $('#updateAvail').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }

                });
                return false;
            }
        });
    });

    /*
     * Set end date as start date by default in "Add Availability"
     */
    $("#startDate").on('changeDate', function (e) {
        $("#endDate").datepicker('setDate', e.date);
    });
    /*
     * Set end date as start date by default in "Update Availability"
     */
    $("#startDateUpdate").on('changeDate', function (e) {
        $("#endDateUpdate").datepicker('setDate', e.date);
    });

    $('#timepicker1').on('changeTime.timepicker', function (e) {
        var timeArray = e.time.value.split(":");
        var hours = timeArray[0];
        var minutes = timeArray[1];
        var olddate = new Date(2011, 6, 15, hours, minutes);
        var added = olddate.addHours(1);
        var newtime = added.getHours() + ':' + added.getMinutes();
        $('#timepicker2').timepicker({ showMeridian: false });
        $('#timepicker2').timepicker('setTime', newtime);
    });

    $('#privateInviteTimePicker1').on('changeTime.timepicker', function (e) {
        var timeArray = e.time.value.split(":");
        var hours = timeArray[0];
        var minutes = timeArray[1];
        var olddate = new Date(2011, 6, 15, hours, minutes);
        var added = olddate.addHours(1);
        var newtime = added.getHours() + ':' + added.getMinutes();
        $('#privateInviteTimePicker2').timepicker({ showMeridian: false });
        $('#privateInviteTimePicker2').timepicker('setTime', newtime);
    });

    $('#privateInviteTimePicker3').on('changeTime.timepicker', function (e) {
        var timeArray = e.time.value.split(":");
        var hours = timeArray[0];
        var minutes = timeArray[1];
        var olddate = new Date(2011, 6, 15, hours, minutes);
        var added = olddate.addHours(1);
        var newtime = added.getHours() + ':' + added.getMinutes();
        $('#privateInviteTimePicker4').timepicker({ showMeridian: false });
        $('#privateInviteTimePicker4').timepicker('setTime', newtime);
    });

    $('#privateInviteTimePicker5').on('changeTime.timepicker', function (e) {
        var timeArray = e.time.value.split(":");
        var hours = timeArray[0];
        var minutes = timeArray[1];
        var olddate = new Date(2011, 6, 15, hours, minutes);
        var added = olddate.addHours(1);
        var newtime = added.getHours() + ':' + added.getMinutes();
        $('#privateInviteTimePicker6').timepicker({ showMeridian: false });
        $('#privateInviteTimePicker6').timepicker('setTime', newtime);
    });


    $('#fromTimeUpdate').on('changeTime.timepicker', function (e) {
        var timeArray = e.time.value.split(":");
        var hours = timeArray[0];
        var minutes = timeArray[1];
        var olddate = new Date(2011, 6, 15, hours, minutes);
        var added = olddate.addHours(1);
        var newtime = added.getHours() + ':' + added.getMinutes();
        $('#toTimeUpdate').timepicker({ showMeridian: false });
        $('#toTimeUpdate').timepicker('setTime', newtime);
    });

    // $('#toTimeUpdate').on('changeTime.timepicker', function (e) {
    //     var toTimeArray = parseInt(e.time.value.split(":")[0]);
    //     var fromTimeArray = parseInt($('#fromTimeUpdate').val().split(':')[0]);

    //     if (toTimeArray <= fromTimeArray) {
    //         var hours = e.time.value.split(":")[0];
    //         var minutes = e.time.value.split(":")[1];
    //         var olddate = new Date(2011, 6, 15, hours, minutes);
    //         var added = olddate.subtractHours(1);
    //         var newtime = added.getHours() + ':' + added.getMinutes();
    //         $('#fromTimeUpdate').timepicker({ showMeridian: false });
    //         $('#fromTimeUpdate').timepicker('setTime', newtime);
    //     }
    // });

});


function initDateTime() {

    //var date = new Date();
    //var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    //var last = new Date();
    //last.setFullYear(last.getFullYear() + 1);

    //Loading datepicker n timepicker in pop up of add avial n send private invitation
    $("#startDate").datepicker('destroy');
    $("#startDate").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: 'today',
        endDate: '+1y',
        format: 'yyyy-mm-dd'
    });
    $("#endDate").datepicker('destroy');
    $("#endDate").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: 'today',
        endDate: '+1y',
        format: 'yyyy-mm-dd'
    });
    $("#callDate1").datepicker('destroy');
    $("#callDate2").datepicker('destroy');
    $("#callDate3").datepicker('destroy');
    $("#callDate1").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: 'today',
        endDate: '+1y',
        format: 'yyyy-mm-dd'
    });
    $("#callDate2").datepicker({
        autoclose: true,
        disableEntry: true,
        startDate: 'today',
        format: 'yyyy-mm-dd'

    });
    $("#callDate3").datepicker({
        autoclose: true,
        disableEntry: true,
        startDate: 'today',
        format: 'yyyy-mm-dd'
    });

    $("#update_callDate1").datepicker({
        autoclose: true,
        disableEntry: true,
        format: 'yyyy-mm-dd',
        startDate: 'today',
        endDate: '+1y'
    });
    $("#update_callDate2").datepicker({
        autoclose: true,
        disableEntry: true,
        format: 'yyyy-mm-dd',
        startDate: 'today',
        endDate: '+1y'

    });
    $("#update_callDate3").datepicker({
        autoclose: true,
        disableEntry: true,
        format: 'yyyy-mm-dd',
        startDate: 'today',
        endDate: '+1y'
    });

    $('#timepicker1').timepicker({
        showMeridian: false,
        defaultTime: '12:00',
        showInputs: true
    });
    $('#timepicker2').timepicker({
        showMeridian: false,
        defaultTime: '13:00',
        showInputs: true
    });
    // For Update Availability
    $('#fromTimeUpdate').timepicker({
        showMeridian: false,
        showInputs: true
    });
    $('#toTimeUpdate').timepicker({
        showMeridian: false,
        showInputs: true
    });
    // Ends
    $('#privateInviteTimePicker1').timepicker({
        showMeridian: false,
        showInputs: true
    });
    $('#privateInviteTimePicker2').timepicker({
        showMeridian: false,
        showInputs: true
    });
    $("#startDate").datepicker('setDate', 'today');
    $("#endDate").datepicker('setDate', 'today');
    $("#callDate1").datepicker('setDate', 'today');

    // Load Default values in timepicker and datepicker
    $("#timepicker1").val("12:00");
    $("#timepicker2").val("13:00");

    $("#timepicker1").timepicker('setTime', "12:00");
    $("#timepicker2").timepicker('setTime', "13:00");

    $("#endDate").val(moment().format('YYYY-MM-DD'));
    $("#startDate").val(moment().format('YYYY-MM-DD'));
}

// get user details for the send private invitation user drop down.
function getUserList() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/userList',
        method: "POST",
        data: "",
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                $('#lstUserList').html("");
                $('#update_lstUserList').html("");
                var strNewOptions = "";
                strNewOptions += "<option value=''>Select User</option>";
                $.each(arrUserData, function (index, arrUserInfo) {
                    strNewOptions += "<option value='" + arrUserInfo.user_id + "'>" + arrUserInfo.username + "</option>";
                });
                $('#lstUserList').html(strNewOptions);
                $("#lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
                $('#update_lstUserList').html(strNewOptions);
                $("#update_lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}
function constructOBJ(item) {

    return {
        id: item.id,
        start: item.start,
        end: item.end,
        title: item.title,
        description: item.description,
        url: item.url,
        // repeat_when: "",
        // recc_start: item.start,
        // recc_end: item.end,
        repeat_type: 1,
        // type: item.type,

        // dow: JSON.parse(item.repeat_when),
        // ranges: [{
        //     start: item.start,
        //     end: item.end,
        // }]
    }
}

// get mentor event from database.
function getMentorEvents(tz) {
    $.ajax({
        url: $('#absPath').val() + 'mentor/eventList',
        method: "POST",
        data: { tz: tz },
        async: false,
        success: function (response) {
            if (response.message == "success") {
                if (response.data.length > 0) {
                    var eventArray = [];
                    var arrUserData = response.data;
                    arrUserData.forEach(function (events) {
                        if (events.repeat_type === 1) {
                            eventArray.push(constructOBJ(events));
                        } else {
                            eventArray.push(events)
                        }
                    });
                    var deleteDate = response.delData;
                    arrUserData = eventArray;
                }
                createEvents(arrUserData, deleteDate);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}


function createEvents(arrUserData, deleteDate) {
    var enabled_dates = [];
    var arrEvent = {};


    $.each(arrUserData, function (index, arrUserInfo) {




        if (arrUserInfo.title == null) {
            arrUserInfo.title = "Available slot";
        }

        if (arrUserInfo.description == null) {

            arrUserInfo.description = "Available slot for user";
        }

        arrEvent['id'] = arrUserInfo.id;
        arrEvent['title'] = arrUserInfo.title;
        arrEvent['description'] = arrUserInfo.description;
        arrEvent['url'] = arrUserInfo.url;

        // Change color of event in case of google calendar event
        if (arrUserInfo.type == "1") {
            arrEvent['color'] = "#97b35d";
            arrEvent['className'] = "googleCalEvent";

        } else if (arrUserInfo.type == "2") {
            arrEvent['color'] = "#ffff00";
            arrEvent['textColor'] = '#FF0000';
            arrEvent['className'] = "privateInviteEvent";
            arrEvent['url'] = "javascript:;";
        }

        // Ends
        /*var starttemp = tempstartdate.split(" ");
         var start_date = starttemp[0];
         var sdate = new Date(start_date);
         var endtemp = tempenddate.split(" ");
         var end_date = endtemp[0];
         var edate = new Date(end_date);*/

        //var zone = $("#zone").val();
        var zone = fTimeZone($('#lstTimeZone option:selected').text());
        //        if (zone != "") {
        //            starttemp[1] = toTimeZone(tempstartdate, zone);
        //            endtemp[1] = toTimeZone(tempenddate, zone);
        //        }

        if (arrUserInfo.repeat_type == 0) {

            var tempstartdate = moment.tz(arrUserInfo.start, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");
            var tempenddate = moment.tz(arrUserInfo.end, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");

            //Single time    
            //start date and time
            /*var startMonth = sdate.getMonth() + 1;
             if (startMonth > 9) {
             startMonth = startMonth;
             } else {
             startMonth = "0" + startMonth;
             }
             
             var odate = sdate.getDate();
             
             if (odate > 9) {
             odate = odate;
             } else {
             odate = "0" + odate;
             }
             
             //end date and time
             var endMonth = edate.getMonth() + 1;
             if (endMonth > 9) {
             endMonth = endMonth;
             } else {
             endMonth = "0" + endMonth;
             }
             
             var tdate = edate.getDate();
             
             if (tdate > 9) {
             tdate = tdate;
             } else {
             tdate = "0" + tdate;
             }
             
             start_date = sdate.getFullYear() + '-' + startMonth + '-' + odate + 'T' + starttemp[1];
             end_date = edate.getFullYear() + '-' + endMonth + '-' + tdate + 'T' + endtemp[1];
             arrEvent['start'] = start_date;
             arrEvent['end'] = end_date;
             */
            arrEvent['start'] = tempstartdate.replace(' ', 'T');
            arrEvent['end'] = tempenddate.replace(' ', 'T');

            enabled_dates.push(arrEvent);
            arrEvent = {};
            //enabled_dates.push(start_date);

        } else if (arrUserInfo.repeat_type === 1) {

            // var tempstartdate = moment.tz(arrUserInfo.ranges[0].start, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");
            // var tempenddate = moment.tz(arrUserInfo.ranges[0].end, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");

            var tempstartdate = moment.tz(arrUserInfo.start, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");
            var tempenddate = moment.tz(arrUserInfo.end, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text())).format("YYYY-MM-DD HH:mm:ss");
            arrEvent['start'] = tempstartdate.replace(' ', 'T');
            arrEvent['end'] = tempenddate.replace(' ', 'T');

            // arrUserInfo['start'] = tempstartdate.split(' ')[1];
            // arrUserInfo['end'] = tempenddate.split(' ')[1];
            enabled_dates.push(arrEvent);
            arrEvent = {};
            // enabled_dates.push(arrUserInfo);
        } else {
            console.log("HERE");
            //Recurring.

            // For recurring
            var tempstartdateRecurring = arrUserInfo.start;
            var tempenddateRecurring = arrUserInfo.end;
            var starttempR = tempstartdateRecurring.split(" ");
            var start_dateR = starttempR[0];
            var sdateR = new Date(start_dateR);
            var endtempR = tempenddateRecurring.split(" ");
            var end_dateR = endtempR[0];
            var edateR = new Date(end_dateR);

            var week = new Date(start_dateR);

            var weekday = week.getDay();
            var daystoadd = 0;
            if (arrUserInfo.repeat_when == 0) {
                daystoadd = 7;
            } else {
                daystoadd = arrUserInfo.repeat_when;
            }
            var addDays = daystoadd - parseInt(weekday);
            var start_dateR = moment(start_dateR).add("days", addDays);
            var day = start_dateR.format('DD');
            var month = start_dateR.format('MM');
            var year = start_dateR.format('YYYY');
            var first_start_date = year + '-' + month + '-' + day + 'T' + starttempR[1];
            var fend_date = year + '-' + month + '-' + day + 'T' + endtempR[1];
            arrEvent['end'] = fend_date;
            arrEvent['start'] = first_start_date;
            start_dateR = year + '-' + month + '-' + day;


            for (var d = new Date(start_dateR + " " + starttempR[1]); d <= new Date(end_dateR + " " + endtempR[1]); d.setDate(d.getDate() + 7)) {
                //                var fstart_date = moment.tz(d, "UTC").tz($('#lstTimeZone').find("option:selected").text());
                var fstart_date = moment(d);

                var day = fstart_date.format('DD');
                var month = fstart_date.format('MM');
                var year = fstart_date.format('YYYY');
                var fend_date = year + '-' + month + '-' + day + ' ' + endtempR[1];
                fstart_date = year + '-' + month + '-' + day + ' ' + starttempR[1];
                fstart_date = moment.tz(fstart_date, "UTC").tz($('#lstTimeZone').find("option:selected").text()).format("YYYY-MM-DDTHH:mm:ss");
                fend_date = moment.tz(fend_date, "UTC").tz($('#lstTimeZone').find("option:selected").text()).format("YYYY-MM-DDTHH:mm:ss");
                arrEvent['id'] = arrUserInfo.id;
                arrEvent['title'] = arrUserInfo.title;
                arrEvent['description'] = arrUserInfo.description;
                arrEvent['url'] = arrUserInfo.url;
                arrEvent['start'] = fstart_date;
                arrEvent['end'] = fend_date;
                arrEvent['backgroundColor'] = "yellow";
                if (moment(d) >= moment(tempstartdateRecurring)) {
                    enabled_dates.push(arrEvent);
                } else {
                    continue;
                }
                arrEvent = {};
                $("#update_privateInviteTimePicker1").timepicker("setTime", "02:00");
            }
        }

    });

    //To remove event from event array which are deleted by mentor
    var FinalAvail = [];
    if (deleteDate != undefined) {
        enabled_dates.forEach(function (enableDate) {
            if (enableDate.dow) {
                var found = deleteDate.filter(function (element) {
                    if (enableDate.id === element.availability_id) {
                        return element;
                    }
                });
                if (found.length > 0) {
                    FinalAvail.push(deleteDates(enableDate, found));
                } else {
                    FinalAvail.push(enableDate)
                }

            } else {
                FinalAvail.push(enableDate)
            }
        });
    } else {
        FinalAvail = enabled_dates;
    }

    //     if (deleteDate != undefined) {
    //         for (var i = 0; i < enabled_dates.length; i++) {
    //             for (var j = 0; j < deleteDate.length; j++) {
    // //                var del_starttemp = deleteDate[j].start.split(" ");
    // //                var del_endtemp = deleteDate[j].end.split(" ");
    //                 var del_fstart_date = moment.tz(deleteDate[j].start, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text()));
    //                 var del_fend_date = moment.tz(deleteDate[j].end, "UTC").tz(fTimeZone($('#lstTimeZone').find("option:selected").text()));
    //                 var del_fday = del_fstart_date.format('DD');
    //                 var del_fmonth = del_fstart_date.format('MM');
    //                 var del_fyear = del_fstart_date.format('YYYY');
    //                 var del_lday = del_fend_date.format('DD');
    //                 var del_lmonth = del_fend_date.format('MM');
    //                 var del_lyear = del_fend_date.format('YYYY');
    //                 var del_starttemp = del_fstart_date.format("HH:mm:ss");
    //                 var del_endtemp = del_fend_date.format("HH:mm:ss");
    //                 var del_fstart_date = del_fyear + '-' + del_fmonth + '-' + del_fday + 'T' + del_starttemp;
    //                 var del_fend_date = del_lyear + '-' + del_lmonth + '-' + del_lday + 'T' + del_endtemp;

    //                 if (enabled_dates[i].start.toString() == del_fstart_date.toString() && enabled_dates[i].end.toString() == del_fend_date.toString()) {
    //                     enabled_dates.splice(i, 1);
    //                 }
    //             }
    //         }
    //     }

    intCal(slotTime, FinalAvail);
}
function deleteDates(enabledt, deletedt) {
    var deleteAray = [];
    deletedt.forEach(function (dldt) {
        deleteAray.push(dldt.start.split(' ')[0])
    });
    deleteAray = deleteAray.sort();
    var tempRangesVR = [];
    var RangeVR = [];
    var breakFlag = false;
    var start = enabledt.ranges[0].start.split(' '),
        end = enabledt.ranges[0].end.split(' ')
    var DateArray = getDateArray(start[0], end[0])

    DateArray.forEach(function (edates) {
        if (edates === deleteAray[0]) {
            deleteAray.splice(0, 1);
            RangeVR.push({ start: tempRangesVR[0] + " " + start[1], end: tempRangesVR[tempRangesVR.length - 1] + " " + end[1] })
            tempRangesVR = []
        } else {
            tempRangesVR.push(edates)
        }
    });
    if (tempRangesVR.length > 0) {
        RangeVR.push({ start: tempRangesVR[0] + " " + start[1], end: tempRangesVR[tempRangesVR.length - 1] + " " + end[1] })
        tempRangesVR = []
    }
    if (RangeVR.length > 0) {
        enabledt.ranges = RangeVR;
    }

    // [
    //     {
    //         start:rangesVR[0]+" "+start[1],
    //         end:rangesVR[rangesVR.length-1]+ " "+end[1]
    //     },
    //     {
    //         start:DateArray[rangesVR.length+1]+" "+start[1],
    //         end:DateArray[DateArray.length-1]+" "+end[1],
    //     }
    // ]
    return enabledt;
}
function getDateArray(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}
var repeatingEvents;
//initialize the calendar
function intCal(slotTime, events) {
    repeatingEvents = events;
    /* initialize the calendar
     -----------------------------------------------------------------*/
    $('#calendar').fullCalendar('destroy');
    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        //        defaultDate: getCalendarCookie('start'),
        defaultDate: getCalendarCookie('start'),
        allDaySlot: false,
        //        defaultView: 'agendaWeek',
        defaultView: getCalendarCookie('currentView'),
        slotDuration: '00:15:00',
        slotEventOverlap: false,
        editable: false,
        defaultTimedEventDuration: slotTime,
        forceEventDuration: true,
        eventOverlap: false,
        nextDayThreshold: "00:00:00",
        droppable: true, // this allows things to be dropped onto the calendar        
        drop: function (start, end) {
            $(this).remove();
        },
        viewRender: function (view, element) {
            //            console.log(view);
            setCalendarCookie(view.start, view.end, view.name);
            //var now = new Date();
            //var end = new Date();
            //end.setMonth(now.getMonth() + 12); //Adjust as needed

            //if (end < view.end) {
            if (moment().add('12', 'month') < view.end) {
                //$("#calendar .fc-next-button").hide();
                $("#calendar .fc-next-button").addClass('fc-state-disabled');
                return false;
            } else {
                //$("#calendar .fc-next-button").show();
                $("#calendar .fc-next-button").removeClass('fc-state-disabled');
            }

            //if (view.start < now) {
            if (view.start < moment()) {
                //$("#calendar .fc-prev-button").hide();
                $("#calendar .fc-prev-button").addClass('fc-state-disabled');
                return false;
            } else {
                //$("#calendar .fc-prev-button").show();
                $("#calendar .fc-prev-button").removeClass('fc-state-disabled');
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            jsEvent.preventDefault();
            recurringOBJ = {}
            if (calEvent.dow !== undefined) {
                recurringOBJ = calEvent
            }

            var clickedClass = calEvent.className[0];

            // Go to and show day view for start date of clicked event
            //calendar.fullCalendar('gotoDate', calEvent.start);

            if (clickedClass == "privateInviteEvent") {
                getPrivateInviteData(calEvent);
            } else {
                getUpdateEventData(calEvent);
            }

        },
        eventRender: function (event, element, view) {
            if (event.ranges !== undefined) {
                return (event.ranges.filter(function (range) {
                    return (event.start.isBefore(range.end) &&
                        event.end.isAfter(range.start));
                }).length) > 0;
            }
        },
        events: function (start, end, timezone, callback) {
            var events = getEvents(start, end); //this should be a JSON request
            callback(events);
        },
        // events: events
    });
}
var getEvents = function (start, end) {
    return repeatingEvents;
}


function getPrivateInviteData(calEvent) {
    var delSDate = calEvent.start._i;
    var delEDate = calEvent.end._i;
    var tz = fTimeZone($('#lstTimeZone option:selected').text());

    var data = {
        id: calEvent.id,
        tz: tz
    }

    $.ajax({
        url: $('#absPath').val() + 'mentor/getPrivateInviteById',
        method: "POST",
        data: data,
        success: function (response) {
            if (response.message == "success") {
                var count = 1;

                $.each(response.data, function (index, value) {

                    $("#update_lstUserList").val(value.user_id).trigger('change');
                    $("#update_call_duration").val(value.duration).trigger('change');
                    $("#update_invitation_id").val(value.invitation_id);

                    $("#update_callDate" + count).datepicker();
                    $("#update_callDate" + count).val(value.start);
                    $("#update_callDate" + count).datepicker("setDate", value.start);

                    $("#update_privateInviteTimePicker" + count).timepicker("setTime", value.time);
                    $("#update_privateInviteMsg").text(value.meeting_purpose);

                    count++;
                });
                $("#update_privateInvite").modal('toggle');
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}


// get mentor event from database.
function getUpdateEventData(calEvent) {
    var delSDate = calEvent.start._d;
    var delEDate = calEvent.end._d;
    var tz = fTimeZone($('#lstTimeZone option:selected').text())

    var data = {
        id: calEvent.id,
        tz: tz
    }
    $.ajax({
        url: $('#absPath').val() + 'mentor/availableData',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;

                if (arrUserData[0].repeat_type == 0) {
                    // One time event
                    showOnetimeUpdateForm();
                    $("#onetime").attr("disabled", false);
                    $("#onetimeUpdate").trigger("click");
                    $("#recurringUpdate").attr("disabled", true);

                    var start_date = arrUserData[0].start;
                    var end_date = arrUserData[0].end;


                    start_date = moment(start_date).format("YYYY-MM-DD");
                    end_date = moment(end_date).format("YYYY-MM-DD");
                    start_time = moment(start_date).format("HH:mm");
                    end_time = moment(end_date).format("HH:mm");
                    var currDate = moment().format("YYYY-MM-DD");

                    var pickerStartDate, pickerEndDate;
                    if (currDate <= start_date) {
                        pickerStartDate = currDate;
                    } else {
                        pickerStartDate = start_date;
                    }
                    pickerEndDate = moment().add(1, "y").format("YYYY-MM-DD");
                    $("#startDateUpdate").val(start_date);
                    $("#startDateUpdate").datepicker("destroy");
                    $("#startDateUpdate").datepicker({
                        autoclose: true,
                        disableEntry: true,
                        default: true,
                        startDate: pickerStartDate,
                        endDate: pickerEndDate,
                        format: 'yyyy-mm-dd'
                    });
                    $("#endDateUpdate").val(end_date);
                    $("#endDateUpdate").datepicker("destroy");
                    $("#endDateUpdate").datepicker({
                        autoclose: true,
                        disableEntry: true,
                        default: true,
                        startDate: pickerStartDate,
                        endDate: pickerEndDate,
                        format: 'yyyy-mm-dd'
                    });
                    start_time = moment(calEvent.start._i).format("HH:mm");
                    end_time = moment(calEvent.end._i).format("HH:mm");
                    $("#fromTimeUpdate").val(start_time);
                    $("#fromTimeUpdate").timepicker('setTime', start_time);
                    $("#toTimeUpdate").val(end_date);
                    $("#toTimeUpdate").timepicker('setTime', end_time);
                    $("#eventId").val(arrUserData[0].id);

                } else {
                    showRecurringUpdateForm();
                    $("#recurringUpdate").attr("disabled", false);
                    $("#recurringUpdate").trigger("click");
                    $("#onetime").attr("disabled", true);
                    // var repeat = arrUserData[0].repeat_when.split(",");
                    // $.each(repeat, function (index, value) {
                    //     $("#" + value).prop('checked', true);
                    // );
                    $('#sun').prop('checked', false);
                    $('#mon').prop('checked', false);
                    $('#tue').prop('checked', false);
                    $('#wed').prop('checked', false);
                    $('#thu').prop('checked', false);
                    $('#fri').prop('checked', false);
                    $('#sat').prop('checked', false);

                    var daysArrUTC = JSON.parse(arrUserData[0].repeat_when);
                    var daysArr = [];
                    var currentTimeZone = fTimeZone($('#lstTimeZone').find("option:selected").text());

                    for (i = moment.tz(arrUserData[0].recc_start, "YYYY-MM-DD HH:mm", "UTC"); i < moment.tz(arrUserData[0].recc_end, "YYYY-MM-DD HH:mm", "UTC"); i.add(1, "day")) {
                        j = i.clone();
                        if (daysArrUTC.indexOf(j.format("e")) >= 0) {
                            daysArr.push(j.tz(currentTimeZone).format("e"));
                        }
                    }

                    daysArr.forEach(function (e) {
                        if (0 == e) {
                            $('#sun').prop('checked', true);
                        }
                        if (1 == e) {
                            $('#mon').prop('checked', true);
                        }
                        if (2 == e) {
                            $('#tue').prop('checked', true);
                        }
                        if (3 == e) {
                            $('#wed').prop('checked', true);
                        }
                        if (4 == e) {
                            $('#thu').prop('checked', true);
                        }
                        if (5 == e) {
                            $('#fri').prop('checked', true);
                        }
                        if (6 == e) {
                            $('#sat').prop('checked', true);
                        }
                    })

                    var parent_id = arrUserData[0].parent_id;
                    $("#parent_id").val(parent_id);
                    $("#delSDate").val(delSDate);
                    $("#delEDate").val(delEDate);

                    var start_date = arrUserData[0].recc_start;
                    var end_date = arrUserData[0].recc_end;

                    var start_date_temp = start_date.split(" ");
                    start_date = start_date_temp[0];
                    var start_time_temp = start_date_temp[1].split(":");
                    var start_time = start_time_temp[0] + ":" + start_time_temp[1];
                    var dateAr = start_date.split('-');
                    var start_date = dateAr[0] + '-' + dateAr[1] + '-' + dateAr[2];
                    $("#startDateUpdate").val(start_date);
                    $("#startDateUpdate").datepicker("destroy");
                    $("#startDateUpdate").datepicker({
                        autoclose: true,
                        disableEntry: true,
                        default: true,
                        startDate: start_date,
                        format: 'yyyy-mm-dd'
                    });
                    //if (current_data <= start_date) {
                    $("#startDateUpdate").datepicker('setDate', start_date);
                    //}

                    if (arrUserData[0].end_type == 1) {
                        $("#particular").trigger("click");
                    } else {
                        $("#never").trigger("click");
                    }

                    var end_date_temp = end_date.split(" ");
                    end_date = end_date_temp[0];
                    var end_date_temp = end_date_temp[1].split(":");
                    var end_time = end_date_temp[0] + ":" + end_date_temp[1];
                    var endDateAr = end_date.split('-');
                    var end_date = endDateAr[0] + '-' + endDateAr[1] + '-' + endDateAr[2];
                    $("#endDateUpdate").val(end_date);
                    $("#endDateUpdate").datepicker("destroy");
                    $("#endDateUpdate").datepicker({
                        autoclose: true,
                        disableEntry: true,
                        default: true,
                        startDate: start_date,
                        format: 'yyyy-mm-dd'
                    });
                    $("#endDateUpdate").datepicker('setDate', end_date);

                    $("#fromTimeUpdate").val(arrUserData[0].start);
                    $("#toTimeUpdate").val(arrUserData[0].end);
                    $("#fromTimeUpdate").timepicker('setTime', arrUserData[0].start.split(' ')[1]);
                    $("#toTimeUpdate").timepicker('setTime', arrUserData[0].end.split(' ')[1]);
                    $("#eventId").val(arrUserData[0].id);

                }

                // if (arrUserData[0].repeat_type === 1) {
                // DOn't update in case of google cal event
                // $("#updateEventButton").hide();
                // }
                $("#updateAvail").modal('toggle');
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

showOnetimeUpdateForm = function () {
    $('#daysSelection').hide();
    $('#endTypeRadioUpdate').hide();
    $("#deleteAllUpdate").hide();
}

showRecurringUpdateForm = function () {
    $('#daysSelection').show();
    $('#endTypeRadioUpdate').show();
    $("#deleteAllUpdate").show();
}

var clientId = '1010642876159-73m98arkc0mg0okuaa3ie34467j5tppp.apps.googleusercontent.com';
var apikey = 'AIzaSyBJ-u9nVcC_VNYmHkR0dcqbgkEzws7GTLo';
var scopes = ["https://www.googleapis.com/auth/calendar.readonly"];
//Called when clicked on google sync button

function googleSync() {
    var clientId = '1010642876159-73m98arkc0mg0okuaa3ie34467j5tppp.apps.googleusercontent.com';
    var apikey = 'AIzaSyBJ-u9nVcC_VNYmHkR0dcqbgkEzws7GTLo';
    var scopes = ["https://www.googleapis.com/auth/calendar.readonly"];
    checkAuth();
}

// google calendar sync function

function handleClientLoad() {
    gapi.client.setApiKey('AIzaSyCRdYe - M9M3DUZ061OQnMWy333EhWJG59M');
    window.setTimeout(checkAuth, 1);
}

// google calendar sync function

function checkAuth() {
    gapi.auth.authorize({ client_id: clientId, scope: scopes, immediate: true }, handleAuthResult);
}

// google calendar sync function

function handleAuthResult(authResult) {

    if (authResult && !authResult.error) {
        makeApiCall();
    } else {
        if (authResult.error !== 'popup_closed_by_user') {
            handleAuthClick();
        }
    }
}

// google calendar sync function

function handleAuthClick(event) {
    gapi.auth.authorize({ client_id: clientId, scope: scopes, immediate: false }, handleAuthResult);
    return false;
}


// google calendar sync function

function makeApiCall() {

    var calendarID = "";
    // Step 4: Load the Google+ API
    gapi.client.load('calendar', 'v3').then(function () {

        var calrequest = gapi.client.calendar.calendarList.list();
        calrequest.execute(function (resp) {
            //calendarID = resp.items[0].id;
            calendarID = "primary";

            // Fetch one year events from today
            //            Date.prototype.addDays = function (days)
            //            {
            //                var dat = new Date(this.valueOf());
            //                dat.setDate(dat.getDate() + days);
            //                return dat;
            //            }
            //var minTime = moment().tz($("#zone").val()).format();
            //var maxTime = moment().tz($("#zone").val()).add(365, 'days').format();
            var minTime = moment().tz(fTimeZone($('#lstTimeZone option:selected').text())).format();
            var maxTime = moment().tz(fTimeZone($('#lstTimeZone option:selected').text())).add(365, 'days').format();
            //            var toOneYear = dat.addDays(365);
            // Step 5: Assemble the API request
            var request = gapi.client.calendar.events.list({
                'calendarId': calendarID,
                'timeMin': minTime,
                'timeMax': maxTime,
                'singleEvents': true,
            });
            // Step 6: Execute the API request
            request.then(function (resp) {

                var eventsList = [];
                var ajaxData = {};
                var successArgs;
                var successRes;
                if (resp.result.error) {
                    reportError('Google Calendar API: ' + data.error.message, data.error.errors);
                } else if (resp.result.items) {
                    var timeZone = resp.result.timeZone;

                    $.ajax({
                        url: $('#absPath').val() + 'mentor/gcalSyncStep1',
                        method: "POST",
                        data: ajaxData,
                        success: function (response) {

                        },
                        error: function (response) {
                            //$(".alert-danger").css("display", "block");
                        }

                    });
                    $.each(resp.result.items, function (i, entry) {

                        var url = entry.htmlLink;
                        // make the URLs for each event show times in the correct timezone
                        //if (timezoneArg) {
                        //    url = injectQsComponent(url, 'ctz=' + timezoneArg);
                        //}
                        var startDate, startDateTemp, endDate, endDateTemp;
                        if (entry.start.date != null) {
                            startDateTemp = entry.start.date + " 00:00:00";
                            startDate = moment.tz(startDateTemp, timeZone).utcOffset('+0000').format('YYYY-MM-DD HH:mm');
                            endDateTemp = entry.start.date + " 23:55:00";
                            endDate = moment.tz(endDateTemp, timeZone).utcOffset('+0000').format('YYYY-MM-DD HH:mm');
                        } else {
                            startDateTemp = entry.start.dateTime;
                            endDateTemp = entry.end.dateTime;
                            startDate = moment(startDateTemp).utcOffset('+0000').format('YYYY-MM-DD HH:mm');
                            endDate = moment(endDateTemp).utcOffset('+0000').format('YYYY-MM-DD HH:mm');
                        }
                        ajaxData = {
                            id: entry.id,
                            title: entry.summary,
                            //                            start: entry.start.dateTime || entry.start.date, // try timed. will fall back to all-day
                            //                            end: entry.end.dateTime || entry.end.date, // same
                            start: startDate,
                            end: endDate,
                            url: url,
                            description: entry.description
                        };
                        eventsList.push({
                            id: entry.id,
                            title: entry.summary,
                            //                            start: entry.start.dateTime || entry.start.date, // try timed. will fall back to all-day
                            //                            end: entry.end.dateTime || entry.end.date, // same
                            start: startDate,
                            end: endDate,
                            url: url,
                            location: entry.location,
                            description: entry.description
                        });
                        $.ajax({
                            url: $('#absPath').val() + 'mentor/gcalSync',
                            method: "POST",
                            data: ajaxData,
                            async: false,
                            beforeSend: function () {
                                $.blockUI();
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (response) {
                            },
                            error: function (response) {
                                //$(".alert-danger").css("display", "block");
                            }

                        });
                    });
                    // call the success handler(s) and allow it to return a new events array
                    successArgs = [eventsList].concat(Array.prototype.slice.call(arguments, 1)); // forward other jq args
                    successRes = $.fullCalendar.applyAll(true, this, successArgs);
                    if ($.isArray(successRes)) {
                        return successRes;
                    }
                }

                if (eventsList.length > 0) {
                    //intCal('00:30:00', eventsList);
                    getMentorEvents(fTimeZone($('#lstTimeZone option:selected').text()));
                    displayAjaxNotificationMessage("Google calendar synchronization performed successfully.", "success");
                    // Here create your calendar but the events options is :
                    //fullcalendar.events: eventsList (Still looking for a methode that remove current event and fill with those news event without recreating the calendar.

                }
                return eventsList;
            });
        });
    });
}

function setCalendarCookie(start, end, view) {
    var temp;
    start = moment(start).format("YYYY-MM-DD");
    if (view == "month") {
        temp = moment(start).add("7", "days").format("YYYY-MM-DD");
    } else {
        temp = start;
    }
    var currentView;
    if (view == null) {
        currentView = "agendaWeek";
    } else {
        currentView = view;
    }
    document.cookie = "currentView=" + currentView;
    document.cookie = "start=" + temp;
}

function getCookie(cname) {

    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}
function getCalendarCookie(cname) {

    var defaultView = getCookie(cname);

    if (defaultView == null) {
        if (cname == "currentView") {
            return "agendaWeek"; // default view
        } else if (cname == "start") {
            return moment().format("YYYY-MM-DD"); // default date
        }
    } else {
        return defaultView;
    }
}
Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}
Date.prototype.subtractHours = function (h) {
    this.setTime(this.getTime() - (h * 60 * 60 * 1000));
    return this;
}
