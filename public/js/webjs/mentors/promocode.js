var promocode_tabledata = '';

$('#AddPromo').on('hidden.bs.modal', function () {
    $('#addPromoCodeForm')[0].reset();

    $("#lstUserList").select2();
    $("#lstUserList").val(null).trigger("change");
});

$(document).ready(function () {

//get list of user for promocode
    getUserList();
    //genrate Promo Code

    $("#genPromoCode").on('click', function () {
        genratePromoCode();
    });
    // get promo code  
    promocode_tabledata = $('.promocode_table').DataTable({
        "lengthMenu": [10, 25],
        "language": {
            "emptyTable": "There are no promotional codes as of now."
        }
    });
    getPromoCode();
    //add promocode
    $('#addPromoCodeForm').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            lstUserList: {
                required: true,
                noBlankSpace: true
            },
            inputpromocode: {
                required: true,
                noBlankSpace: true
            },
            discount: {
                required: true,
                number: true,
                range: [00, 100]
            }
        },
        messages: {
            lstUserList: {
                required: 'Please select one of the user'
            },
            inputpromocode: {
                required: "Please genrate code"
            },
            discount: {
                required: "Please enter discount",
                number: "Please enter valid number",
                range: "Please enter number between 0 to 100"
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'lstUserList') {
                element.parent().append(error);
            } else if (element.attr('id') == 'inputpromocode') {
                element.parent().parent().append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'mentor/addPromoCode',
                method: "POST",
                data: $('#addPromoCodeForm').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == "success") {
                        $("#ifNoPromotions").hide();
                        $("#ifYesPromotions").show();
                        $('#addPromoCodeForm')[0].reset();
                        $('#AddPromo').modal('toggle');
                        displayAjaxNotificationMessage("Promo code added successfully", "success");
                        getPromoCode();
                    } else {
                        $('#addPromoCodeForm')[0].reset();
                        $('#AddPromo').modal('toggle');
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#addPromoCodeForm')[0].reset();
                    $('#AddPromo').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }
            });
            return false;
        }
    });
    //delete promocode

    $(document.body).on('click', '.mentor_promocode', function () {
        var promocodeId = $(this).attr('data-id');
        $("#delpcode").val(promocodeId);
        //delPromoCode(promocodeId);
    });

    $(document.body).on('click', '#delCode', function () {
        var promocodeId = $("#delpcode").val();
        delPromoCode(promocodeId);
    });
});
// get user details for the promo code user drop down.
function getUserList() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/userList',
        method: "POST",
        data: "",
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                $('#lstUserList').html("");
                $('#update_lstUserList').html("");
                var strNewOptions = "";
                strNewOptions += "<option value=''>Select User</option>";
                $.each(arrUserData, function (index, arrUserInfo) {
                    strNewOptions += "<option value='" + arrUserInfo.user_id + "'>" + arrUserInfo.username + "</option>";
                });
                $('#lstUserList').html(strNewOptions);
                $("#lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
                $('#update_lstUserList').html(strNewOptions);
                $("#update_lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

// get user details for the promo code user drop down.
function genratePromoCode() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/genratePromoCode',
        method: "POST",
        data: "",
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == "SUCCESS") {
                var promocode = response.data;
                $("#inputpromocode").val(promocode.promoCode);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

function getPromoCode() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/getPromoCodeData',
        type: 'POST',
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var html = '';
            console.log(response);
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            var promoCodeData = response.data;
            promocode_tabledata.clear();
            console.log(promoCodeData.length);
            if (promoCodeData.length > 0) {
                $("#ifNoPromotions").hide();
                $("#ifYesPromotions").show();
                //$('#check_expertise').val(mentorExpertise.length);
                $.each(promoCodeData, function (index, item) {
                    addPromoCodeRow(item, 'add');
                });
            } else {
                $("#ifNoPromotions").show();
                $("#ifYesPromotions").hide();
                addPromoCodeRow(promoCodeData, 'add1');
            }
        }
    });
}

// Generate Mentor Expertise table html
function addPromoCodeRow(data, action) {
//updateCheckExpertiseValue(data);

    var usedStatus = "";
    var status = "";
    var deletebtn = "";

    if (action == 'add') {
        if (data.is_used == 0) {
            usedStatus = "Not Used";
            status = "DELETE";
            //deletebtn = '<a href="javascript:;" class="edit mentor_promocode btn-red btn" data-id="' + data.promo_code_id + '">' + status + '</a>';
            deletebtn = '<a href="#" class=" edit mentor_promocode btn red-bg btn-reg" data-toggle="modal" id="pcodeModalId" data-id="' + data.promo_code_id + '" data-target="#pcodeModal">' + status + '</a>';
        } else {
            usedStatus = "Used";
            status = "";
            deletebtn = "";
        }

        var html = '<tr>' +
                '<td><span>' + data.promo_code + '</span></td>' +
                '<td>' + data.name + '</td>' +
                '<td>' + data.discount + '%</td>' +
                '<td>' + usedStatus + '</td>' +
                '<td>' + data.created_date + '</td>' +
                '<td> ' + deletebtn + '</td>' +
                '</tr>';


        promocode_tabledata.row.add($(html));
        promocode_tabledata.draw();
    } else {
        promocode_tabledata.clear();
        promocode_tabledata.draw();
    }
}

//delete promocode
function delPromoCode(id) {

    var data = {
        id: id
    };

    $.ajax({
        url: $('#absPath').val() + 'mentor/deletePromoCode',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var html = '';
            if (response.message == "success") {
                displayAjaxNotificationMessage("Promo code deleted successfully", "success");
                $("#pcodeModal").modal('toggle');
                getPromoCode();
            } else {
                displayAjaxNotificationMessage("something went wrong. Try again..", "danger");
                $("#pcodeModal").modal('toggle');
                getPromoCode();
            }
        }
    });
}