var count = 3;

$(document).ready(function () {

    $("#backbtn").on('click', function () {
        $.blockUI();
        $("#div2").hide();
        $("#div1").show();
        $.unblockUI();
    });

    $("#geocomplete").geocomplete({
        types: ['(cities)'],
        details: "form div.col-sm-4",
        detailsAttribute: "data-geo"
    });

    $('#orgIndustry').change(function () {
        var industry_id = $(this).val();
        $('#orgSubIndustry').val('');
        if (industry_id == "" || typeof industry_id == "undefined") {
            $('#orgSubIndustry').attr('disabled', true);
        } else {
            $('#orgSubIndustry').attr('disabled', false);
        }
        $('#orgSubIndustry option').each(function (index, item) {
            if ($(item).attr('data-industry-id') == industry_id || typeof $(item).attr('data-industry-id') == "undefined") {
                $(item).show();
            } else {
                $(item).hide();
            }
        });
    });


    /************** form1 validation *****************/

    $("#orgRegFrm1").validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            orgName: {
                required: true,
                noBlankSpace: true
            },
            orgWebSite: {
                required: true,
                noBlankSpace: true,
                url: true
            },
            orgAdd1: {
                required: true
            },
            orgAdd2: {
                required: true
            },
            orgZip: {
                required: true,
                number: true
            },
            geocomplete: {
                required: true
            },
            orgIndustry: {
                required: true
            },
            orgSubIndustry: {
                required: true
            },
            comOffFirstName: {
                required: true
            },
            comOffLastName: {
                required: true
            },
            comOffContactNumber: {
                required: true,
                number: true
            },
            comOffEmail: {
                required: true,
                email: true
            }
        },
        messages: {
            orgName: {
                required: 'Please enter Organisation Name'
            },
            orgWebSite: {
                required: "Please enter Website",
                url: "Please enter valid URL"
            },
            orgAdd1: {
                required: "Please enter Address Line 1"
            },
            orgAdd2: {
                required: "Please enter Address Line 2"
            },
            orgZip: {
                required: "Please enter Zip Code",
                number: "Only digits are allowed"
            },
            geocomplete: {
                required: "Please enter Location",
            },
            orgIndustry: {
                required: "Please select Industry",
            },
            orgSubIndustry: {
                required: "Please select Sub-Industry",
            },
            comOffFirstName: {
                required: "Please enter First Name",
            },
            comOffLastName: {
                required: "Please enter Last Name",
            },
            comOffContactNumber: {
                required: "Please enter Contact Number",
                number: "Only digits are allowed"
            },
            comOffEmail: {
                required: "Please enter Email Address",
                email: "Please enter valid Email Address"
            }
        },
        errorPlacement: function (error, element) {
            element.after(error);
        },
        submitHandler: function (form) {

            // console.log(JSON.stringify($('#orgRegFrm1').serializeArray()));
            var formData = JSON.stringify($('#orgRegFrm1').serializeArray());
            $("#formdata1").val(formData);
            return false;

            $.blockUI();
            $("#div1").hide();
            $("#div2").show();
            $.unblockUI();

//            $.ajax({
//                url: $('#absPath').val() + 'mentor/addPromoCode',
//                method: "POST",
//                data: $('#addPromoCodeForm').serialize(),
//                beforeSend: function () {
//                    $.blockUI();
//                },
//                complete: function () {
//                    $.unblockUI();
//                },
//                success: function (response) {
//                    if (response.message == "success") {
//                        $('#addPromoCodeForm')[0].reset();
//                        $('#AddPromo').modal('toggle');
//                        displayAjaxNotificationMessage("Promo code added successfully", "success");
//                        getPromoCode();
//                    } else {
//                        $('#addPromoCodeForm')[0].reset();
//                        $('#AddPromo').modal('toggle');
//                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
//                    }
//                },
//                error: function (response) {
//                    $.unblockUI();
//                    $('#addPromoCodeForm')[0].reset();
//                    $('#AddPromo').modal('toggle');
//                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
//                }
//            });
            return false;
        }
    });

    /************** form validation *****************/


});

function userSocialLoginHandle(dataParams) {

    var absPath = $('#absPath').val();

    var timeZone = $('#hidTimeZoneLabel').val();
    dataParams.timeZone = timeZone;

    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "RDTDASH") {
                    window.location = '/';
                } else if (response['flagMsg'] == "RDTEMAILACT") {
                    window.location = response['data']['redirectLink'];
                }
            }
        }
    });
}