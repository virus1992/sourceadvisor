$(document).ready(function () {

    $('.btn-linkedin').on('click', function(){
        onLinkedInLoad();
        $('.btn-linkedin').attr("disabled", true);
        return false;
    });
    setTimeZoneForRegisteringUser();

    $("#mobile").intlTelInput({
        separateDialCode: true,
        utilsScript: "js/intcountrydd/js/utils.js",
        geoIpLookup: function (callback) {
            $.get("//ipinfo.io", function () {}, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        initialCountry: "auto"
    });

    $(".modal-fullscreen").on('show.bs.modal', function () {
        setTimeout(function () {
            $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
        }, 0);
    });

    $(".modal-fullscreen").on('hidden.bs.modal', function () {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    });

    $('.datepicker').datepicker({
        endDate: '-18y',
        autoclose: true,
        disableEntry: true,
        default: true,
        format: "dd/mm/yyyy"
    });

    $("#lstCountry").select2().on("select2:close", function (e) {
        $(this).valid();
    });

    $("#lstTimeZone").select2().on("select2:close", function (e) {
        $(this).valid();
    });


    $('#frm_register').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'lstCountry' || element.attr('id') == 'lstTimeZone') {
                element.parent().append(error);
            }
            if (element.attr('id') == 'chkDisclaimer') {
                element.parent().append(error);
            } else {
                element.after(error);
            }
        },
        rules: {
            first_name: {
                required: true,
                noBlankSpace: true
            },
            last_name: {
                required: true,
                noBlankSpace: true
            },
            email: {
                required: true,
                email: true,
                noBlankSpace: true
            },
            dob: {
                required: true,
                noBlankSpace: true,
                check_date_of_birth: [true, 18]
            },
            password: {
                required: true,
                minlength: 6,
                noBlankSpace: true
            },
            cnfpassword: {
                required: true,
                equalTo: "#password",
                noBlankSpace: true
            },
            chkDisclaimer: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter first name"
            },
            last_name: {
                required: "Please enter last name"
            },
            email: {
                required: "Please enter email address",
                email: "Please enter valid email address"
            },
            dob: {
                required: "Please enter date of birth"
            },
            password: {
                required: "Please enter password",
                minlength: "Password should be minimum 6 characters"
            },
            cnfpassword: {
                required: "Please enter confirm password",
                equalTo: "Password and confirm password should match"
            },
            chkDisclaimer: {
                required: "Please agree to terms and conditions"
            }
        },
        submitHandler: function (form) {
            var objSelectedCountry = $('#mobile').intlTelInput("getSelectedCountryData");
            $('#mobile_ext').val('+' + objSelectedCountry.dialCode);
            form.submit();
        }
    });
});

function setTimeZoneForRegisteringUser() {
    var userIpAddress = $('#userIPAdd').val();
    $.getJSON("//timezoneapi.io/api/" + userIpAddress, function (data) {
        $('#hidTimeZoneLabel').val(data.data.timezone.id);
        $('#hidTimeZoneOffset').val(moment().tz(data.time_zone).utcOffset() / 60);
    });
}

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    
    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };

        userSocialLoginHandle(dataParam);
        IN.User.logout();
    }).error(function (profileErr) {
        alert('error occured.');
    });

}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
    alert('error occured.');
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}




function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();

    var profileId = profile.getId();
    var name = profile.getName();
    var imageUrl = profile.getImageUrl();
    var email = profile.getEmail();

    var dataParams = {
        uniqueId: profileId,
        loginType: 'gp',
        name: name,
        imageUrl: imageUrl,
        email: email
    };

    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        userSocialLoginHandle(dataParams);
    });
}

function userSocialLoginHandle(dataParams) {

    var absPath = $('#absPath').val();

    var timeZone = $('#hidTimeZoneLabel').val();
    dataParams.timeZone = timeZone;

    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "RDTDASH") {
                    window.location = '/';
                } else if (response['flagMsg'] == "RDTEMAILACT") {
                    window.location = response['data']['redirectLink'];
                }
            }
        }
    });
}