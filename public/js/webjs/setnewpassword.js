$(document).ready(function(){

    $('#frmSetNewPassword').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            element.after(error);
        },
        rules: {
            password: {
                required: true,
                minlength: 6,
                noBlankSpace: true
            },
            confPassword: {
                required: true,
                equalTo: "#password",
                noBlankSpace: true
            },
        },
        messages: {
            password: {
                required: "Please enter Password",
                minlength: "Password should be minimum 6 characters"
            },
            confPassword: {
                required: "Please enter confirm password.",
                equalTo: "Password and Confirm Password should match"
            },

        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});