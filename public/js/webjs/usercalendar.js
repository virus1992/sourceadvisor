$(document).ready(function () {

    var slotTime = "00:30:00";
    /* On document load initialize basic calendar function  */

    intCal(slotTime);
    intEvents();
    /* Duration drop down change handling */

    $("#slottime").on("change", function () {
        $('#calendar').fullCalendar('removeEvents');
        var mainDiv = $('#external-events');
        var htmlDiv = '<h6 class="mt0">Drag slot to the calendar at your desired times.</h6><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 1</div></div><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 2</div></div><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 3</div></div>';
        mainDiv.html(htmlDiv);
        intEvents();
        slotTime = $(this).val();
        intCal(slotTime);
    });
    /* Reset button click event */

    $("#reset").on("click", function () {
        $('#calendar').fullCalendar('removeEvents');
        var mainDiv = $('#external-events');
        var htmlDiv = '<h6 class="mt0">Drag slot to the calendar at your desired times.</h6><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 1</div></div><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 2</div></div><div class="col-sm-4 slotBtn"><div class="fc-event">Slot 3</div></div>';
        mainDiv.html(htmlDiv);
        intEvents();
    });
    // when the timezone selector changes, dynamically change the calendar option
    $('#timezone-selector').on('change', function () {
        $('#calendar').fullCalendar('option', 'timezone', 'Asia/Kolkata');
    });
    //send message to mentor validation and saving data

    $('#sendMessage').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            mentormsg: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            mentormsg: {
                required: 'Please enter some message.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'users/send_message_mentor',
                method: "POST",
                data: $('#sendMessage').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == "success") {
                        $('#sendMessage')[0].reset();
                        $('#askTimeSlot').modal('toggle');
                        displayAjaxNotificationMessage("Message sent successfully.", "success");
                    } else {
                        $('#sendMessage')[0].reset();
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#sendMessage')[0].reset();
                    $('#askTimeSlot').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });

});

/* initialize the calendar */

function intCal(slotTime) {

    /* initialize the calendar
     -----------------------------------------------------------------*/
    $('#calendar').fullCalendar('destroy');
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        allDaySlot: false,
        defaultView: 'agendaWeek',
        slotDuration: '00:30:00',
        slotEventOverlap: false,
        editable: false,
        defaultTimedEventDuration: slotTime,
        forceEventDuration: true,
        eventOverlap: false,
        droppable: true, // this allows things to be dropped onto the calendar
        events: [
            {
                title: 'Long Event',
                start: '2016-06-07',
                end: '2016-06-10'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-06-09T16:00:00'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2016-06-16T16:00:00'
            },
            {
                title: 'Meeting',
                start: '2016-09-12T13:30:00',
                end: '2016-09-12T14:30:00'
            },
            {
                title: 'Meeting',
                start: '2016-09-12T10:30:00',
                end: '2016-09-12T12:30:00'
            },
            {
                title: 'Lunch',
                start: '2016-06-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2016-06-12T14:30:00'
            },
            {
                title: 'Happy Hour',
                start: '2016-06-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2016-06-12T20:00:00'
            },
            {
                title: 'Birthday Party',
                start: '2016-06-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2016-06-28'
            }
        ],
        viewRender: function (view, element) {
            if (view.name === "month") {
                $(".fc-event").hide();

            } else {
                $(".fc-event").show();
            }
        },
        dayRender: function (date, cell) {

        },
        drop: function (start, end) {
            $(this).remove();
        },
        eventDrop: function (event, delta, revertFunc) {
            if (checkOverlap(event)) {
                revertFunc();
            }
        }
        /*eventConstraint: {
         start: '2016-09-07T10:00:00',
         end: '2016-09-08T22:00:00'
         }*/
    });




}

/* initialize the external events (Drag n Drop Slots) */

function intEvents() {

    /* initialize the external events
     -----------------------------------------------------------------*/

    $('#external-events .fc-event').each(function () {

// store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });
        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });
    });
}