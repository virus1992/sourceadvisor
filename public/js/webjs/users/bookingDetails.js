var absPath = "";
$(document).ready(function () {
    var id = "";
    absPath = $('#absPath').val();
    getUserMeetingData(id);
    getUserList(); //get user list


    $("#lstUserList").on('change', function () {
        var id = $(this).val();
        getUserMeetingData(id);
    });


});

function getUserMeetingData(id) {
    var data = {id: id};

    $.ajax({
        url: absPath + 'users/getUserMeetingData',
        method: "POST",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            var meeting = "";
            var acceptedhtml = "";
            var completedhtml = "";

            if (response.data.length > 0) {


                $.each(response.data, function (index, arrUserData) {
                    if (arrUserData.status == "accepted") {
                        if (arrUserData.meeting_type == "virtual") {
                            meeting = "One time conference";
                        } else {
                            meeting = " One time In Person";
                        }

                        acceptedhtml += '<div class="meetingSection">' +
                                '<div class="row">' +
                                '<div class="col-sm-3">' +
                                '<div class="profileinfo">' +
                                '<div class="profilePic">' +
                                '<img src="' + arrUserData.finalProfileImageUrl + '" class="img-responsive">' +
                                '</div>' +
                                '<h2><a href="' + absPath + ' /mentorprofile/' + arrUserData.slug + '">' + arrUserData.name + '</a></h2>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-9">' +
                                '<div class="meetingHistory">' +
                                '<p class="meetingListDetail"><label>Meeting Date</label>' + arrUserData.slot_date + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Type</label> ' + meeting + ' call for ' + arrUserData.duration + ' minutes</p>' +
                                '<p class="meetingListDetail"><label>Meeting Purpose</label>' + arrUserData.meeting_purpose + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Time</label>' + arrUserData.slot_time + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Cost</label>$4502 <a href="#" class="btn-simple">View Cost Breakup</a></p>' +
                                '<p class="meetingListDetail"><label>Additinal Participants</label>You havent added any additional participant <a href="#" class="btn-simple">Add</a></p>' +
                                '<div class="mt20 mb20">' +
                                '<a href="#" class="buttons btn-black">Cancel Meeting</a>' +
                                '<a href="#" class="buttons btn-blue">Reschedule Meeting</a>' +
                                '<a href="#" class="buttons btn-red">Send Private Message</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                    } else {

                        completedhtml += '<div class="meetingSection">' +
                                '<div class="row">' +
                                '<div class="col-sm-3">' +
                                '<div class="profileinfo">' +
                                '<div class="profilePic">' +
                                '<img src="' + arrUserData.finalProfileImageUrl + '" class="img-responsive">' +
                                '</div>' +
                                '<h2><a href="' + absPath + ' /mentorprofile/' + arrUserData.slug + '">' + arrUserData.name + '</a></h2>' +
                                '</div>' +
                                '</div>' +
                                '<div class="col-sm-9">' +
                                '<div class="meetingHistory">' +
                                '<p class="meetingListDetail"><label>Meeting Date</label>' + arrUserData.slot_date + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Type</label> ' + meeting + ' call for ' + arrUserData.duration + ' minutes</p>' +
                                '<p class="meetingListDetail"><label>Meeting Purpose</label>' + arrUserData.meeting_purpose + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Time</label>' + arrUserData.slot_time + '</p>' +
                                '<p class="meetingListDetail"><label>Meeting Cost</label>$4502 <a href="#" class="btn-simple">View Cost Breakup</a></p>' +
                                '<p class="meetingListDetail"><label>Additinal Participants</label>You havent added any additional participant <a href="#" class="btn-simple">Add</a></p>' +
                                '<div class="mt20 mb20">' +
                                '<a href="#" class="buttons btn-red" data-toggle="modal" data-target="#refundRequest">Request Refund</a>' +
                                '<a href="#" class="buttons btn-blue">Leave a Feedback</a>' +
                                '<a href="#" class="buttons btn-black">Book Another Call</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';

                    }
                });
                $("#tab_future").append(acceptedhtml);
                $("#tab_completed").append(completedhtml);
            } else {
                $("#tab_future").html("");
                $("#tab_completed").html("");
            }
        }
    });
}

function getUserList() {

    $.ajax({
        url: $('#absPath').val() + 'mentor/userList',
        method: "POST",
        data: "",
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                $('#lstUserList').html("");
                var strNewOptions = "";
                strNewOptions += "<option value=''>Select User</option>";
                $.each(arrUserData, function (index, arrUserInfo) {
                    strNewOptions += "<option value='" + arrUserInfo.user_id + "'>" + arrUserInfo.username + "</option>";
                });
                $('#lstUserList').html(strNewOptions);
                $("#lstUserList").select2().on("select2:close", function (e) {
                    $(this).valid();
                });
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}