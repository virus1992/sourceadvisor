var promocode_tabledata = '';

$(document).ready(function () {

    $('.transection_table').DataTable(
            {"lengthMenu": [10, 25, 50, 100],
                "aaSorting": [[3, "desc"]],
                "language": {
                    "emptyTable": "There are no transactions associated with your account as of now."
                }
            });

});