var absPath = "";
$(document).ready(function () {
    absPath = $('#absPath').val();

    initSearchProcess();

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            getNextPageData();
        }
    });

//    $(document.body).delegate('.js-removeThisBookmarkMentor', 'click', function (e) {
//        var mentorId = $(this).attr('data-id');
//        //alert(mentorId);
//        //removeBookmarkMentorDiv(mentorId);
//        removeThisBookMarkedMentor(mentorId, removeBookmarkMentorDiv);
//    });

    $(document.body).delegate('.bookMarkThisMentor', 'click', function (e) {
        var cla = $(this).parent().prop('className');
        var mentorId = $(this).attr('data-id');
        if (cla != "") {
            $('div[data-mentorListingBox=' + mentorId + ']').fadeOut('slow');
        }
        //alert(mentorId);
        //removeBookmarkMentorDiv(mentorId);
        // removeThisBookMarkedMentor(mentorId, removeBookmarkMentorDiv);
    });

});



function removeBookmarkMentorDiv(mentorId) {
    var objMentorListingBox = $('div[data-mentorListingBox=' + mentorId + ']');
    objMentorListingBox.fadeOut('slow');
}




function initSearchProcess() {
    $('#hidPage').val("0");

    var hidPerPageCount = $('#hidPerPageCount').val();
    var hidSortByPreference = $('#hidSortByPreference').val();

    var hidSearchParamJSON = $('#hidSearchParamJSON').val();

    $('#hidFlagMoreData').val('Y');

    var dataToSend = {
        flagBookmarkedMentor: 1
    };

    $.ajax({
        url: absPath + 'users/getUserBookmarkedMentors',
        method: 'POST',
        type: 'json',
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            }

            if (response['error'] == "1") {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            } else {
                if (response['flagMsg'] == "BOOKAMRKMENTORFETCH") {

                    var arrMentorListingData = response.data.arrMentorDataToDisplay;
                    var loggedInUserId = response.data.loggedInUserId;
                    var arrMentorAllDataWithoutLimit = response.data.arrMentorAllDataWithoutLimit;

                    $.each(arrMentorListingData, function (index, objEle) {
                        objEle.overViewListingDisplay = htmlToStringConversion(objEle.overViewListing, 150);
                        objEle.punchLineListingDisplay = htmlToStringConversion(objEle.punchLineListing, 50);
                        objEle.professionDisplay = htmlToStringConversion(objEle.profession, 18);
                        objEle.jobDescriptionDisplay = htmlToStringConversion(objEle.jobDescription, 18);
                        objEle.company_nameDisplay = htmlToStringConversion(objEle.company_name, 18);
                    });

                    var arrListingDisplayData = {
                        loggedInUserId: loggedInUserId,
                        arrMentorListingData: arrMentorListingData,
                        inBookmarkMentor: 1
                    };

                    // setting checks in sode bar filters.
                    var dataParamsForSearch = response.data.arrDataParams;

                    var mentorCount = response.data.arrMentorAllDataWithoutLimit.length;

                    if (mentorCount > 0) {

                        /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                        // Retrieve the HTML from the script tag we setup in step 1​
                        // We use the id (header) of the script tag to target it on the page​
                        var theTemplateScript = $("#mentorListingBoxes").html();

                        // The Handlebars.compile function returns a function to theTemplate variable​
                        var theTemplate = Handlebars.compile(theTemplateScript);

                        $("#searchResultDiv").html(theTemplate({arrMentorData: arrListingDisplayData}));
                        $('#mentorListingCount').html(mentorCount);
                        /*************************************** SEARCH BOX TEMPLATE END  ****************************************/

                        if (mentorCount < hidPerPageCount) {
                            $('#hidFlagMoreData').val("N");
                        }

                    } else {

                        /*************************************** SEARCH BOX NO RESULT TEMPLATE START ****************************************/
                        // Retrieve the HTML from the script tag we setup in step 1​
                        // We use the id (header) of the script tag to target it on the page​
                        var theTemplateScript = $("#bookmarkMentorListingNoResult").html();

                        // The Handlebars.compile function returns a function to theTemplate variable​
                        var theTemplate = Handlebars.compile(theTemplateScript);

                        $("#searchResultDiv").html(theTemplate({}));
                        /*************************************** SEARCH BOX NO RESULT TEMPLATE START ****************************************/

                        $('#divMentorSearchListing').css("display", "none");
                        $('#hidFlagMoreData').val("N");
                    }
                    $('#hidPage').val("0");
                }
            }
            $('#hidFlagRequesting').val("N");
            $('#hidRefreshSideBar').val("Y");
        }
    });
}


function getNextPageData() {
    var hidFlagMoreData = $('#hidFlagMoreData').val();
    var hidFlagRequesting = $('#hidFlagRequesting').val();
    var hidPerPageCount = $('#hidPerPageCount').val();

    if (hidFlagMoreData == "Y" && hidFlagRequesting == "N") {

        $('#hidFlagRequesting').val("Y");

        var hidPage = parseInt(parseInt($('#hidPage').val()) + 1);

        var dataToSend = {
            flagBookmarkedMentor: 1,
            pageNumber: hidPage
        };

        $.ajax({
            url: absPath + 'users/getUserBookmarkedMentors',
            method: 'POST',
            data: dataToSend,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                $('#hidFlagRequesting').val("N");
            },
            success: function (response) {

                if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                    displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                }

                if (response['error'] == "1") {
                    displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
                } else {
                    if (response['flagMsg'] == "BOOKAMRKMENTORFETCH") {

                        var arrMentorListingData = response.data.arrMentorDataToDisplay;
                        var mentorCount = response.data.arrMentorDataToDisplay.length;
                        var loggedInUserId = response.data.loggedInUserId;

                        $.each(arrMentorListingData, function (index, objEle) {
                            objEle.overViewListingDisplay = htmlToStringConversion(objEle.overViewListing, 150);
                            objEle.punchLineListingDisplay = htmlToStringConversion(objEle.punchLineListing, 50);
                            objEle.professionDisplay = htmlToStringConversion(objEle.profession, 18);
                            objEle.jobDescriptionDisplay = htmlToStringConversion(objEle.jobDescription, 18);
                            objEle.company_nameDisplay = htmlToStringConversion(objEle.company_name, 18);
                        });

                        var arrListingDisplayData = {
                            loggedInUserId: loggedInUserId,
                            arrMentorListingData: arrMentorListingData,
                            inBookmarkMentor: 1
                        };

                        if (mentorCount > 0) {

                            /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                            // Retrieve the HTML from the script tag we setup in step 1​
                            // We use the id (header) of the script tag to target it on the page​
                            var theTemplateScript = $("#mentorListingBoxes").html();

                            // The Handlebars.compile function returns a function to theTemplate variable​
                            var theTemplate = Handlebars.compile(theTemplateScript);

                            $("#searchResultDiv").append(theTemplate({arrMentorData: arrListingDisplayData}));
                            /*************************************** SEARCH BOX TEMPLATE END  ****************************************/
                            if (mentorCount < hidPerPageCount) {
                                $('#hidFlagMoreData').val("N");
                            }
                        } else {
                            $('#hidFlagMoreData').val("N");
                        }
                    }
                }

                $('#hidFlagRequesting').val("N");
                $('#hidPage').val(hidPage);
            }
        });
    }

}