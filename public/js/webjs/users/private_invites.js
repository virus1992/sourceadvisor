var global_meeting_invites = '';
var mentor_timezone = $('#mentorTimezone').val();

$(document).ready(function () {
    $('#lstTimeZone').select2();

    $("#lstTimeZone").on("change", function () {
        $.blockUI();
        mentor_timezone = $(this).find("option:selected").text();
        $('#userPendingInvitesListing').html('');
        generatePrivateInvitesHtml(global_meeting_invites);
    });

    $.ajax({
        url: $('#absPath').val() + 'users/getPrivateInvites',
        type: 'POST',
        beforeSend: function () {
            $.blockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.data.length > 0) {
                $('#userPendingInvitesListing').show();
                $('#noUserPendingInvitesListing').hide();
                global_meeting_invites = response.data;

                $('#userPendingInvitesListing').html('');
                generatePrivateInvitesHtml(response.data);

                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            } else {
                $('#userPendingInvitesListing').hide();
                $('#noUserPendingInvitesListing').show();
                $.unblockUI();
            }
        }
    });
});

$(document.body).on("click", ".acceptInvite", function (e) {
    e.preventDefault();

    var invitation_id = $(this).parents('.col-sm-4').attr('id').split('_')[1];
    var invitation_slot_id = $(this).parents('.col-sm-4').find('.invitationSlotSelected:checked').val();
    var mentor_id = $(this).parents('.col-sm-4').find(".mentorid").attr('id').split('_')[1];
    var invoiceId = $(this).parents('.col-sm-4').find(".invoiceid").attr('id').split('_')[1];
    var call_duration = $(this).parents('.col-sm-4').find(".duration").attr('id').split('_')[1];
    var mentorSlug = $(this).parents('.col-sm-4').find(".mentorslug").attr('id').split('_')[1];

    var objParams = {mentorId: mentor_id, slots: "", duration: call_duration};
    if (invitation_slot_id != "" && typeof invitation_slot_id != "undefined") {
        var objForPostParams = {
            invoiceId: invoiceId,
            mentorSlug: mentorSlug,
            invitation_id: invitation_id,
            invitation_slot_id: invitation_slot_id,
            privateInvite: 1,
            objParams: JSON.stringify(objParams)
        };
        var redirectURI = $('#absPath').val() + 'users/slot_booking_payment';
        $.redirect(redirectURI, objForPostParams, "POST");
    } else {
         alert("Please select a suitable slot before accepting the invite.");
    }
});

$(document.body).on("click", ".declineInvite", function (e) {
    e.preventDefault();

    var invitation_id = $(this).parents('.col-sm-4').attr('id').split('_')[1];
    var mentor_id = $(this).parents('.col-sm-4').find(".mentorid").attr('id').split('_')[1];
    var call_duration = $(this).parents('.col-sm-4').find(".duration").attr('id').split('_')[1];

    $.ajax({
        url: $('#absPath').val() + 'users/declineInvitation',
        type: 'POST',
        data: {
            invitation_id: invitation_id,
            mentor_id: mentor_id,
            call_duration: call_duration
        },
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                alert('User session has expired. Please login and try again.');
                window.location.href = $('#absPath').val() + 'login';
                return false;
            }

            if (response.flagMsg == 'success') {
                $('#invitation_id_' + invitation_id).remove();
                if ($('#userPendingInvitesListing .col-sm-4.item').length == 0) {
                    $('#userPendingInvitesListing').hide();
                    $('#noUserPendingInvitesListing').show();
                } else {
                    var $container = $('.masonry-container');
                    $container.masonry({
                        columnWidth: '.item',
                        itemSelector: '.item'
                    });
                }
            } else if (response.flagMsg == 'error') {
                displayAjaxNotificationMessage('Some unexpected error occured. Please try again.', 'error')
            }
        }
    });
});


function generatePrivateInvitesHtml(data) {
    if (data.length == 0) {
        $('#userPendingInvitesListing').hide();
        $('#noUserPendingInvitesListing').show();
    } else {
        $('#userPendingInvitesListing').show();
        $('#noUserPendingInvitesListing').hide();
        $.each(data, function (index, item) {
            var newItem = $('#hiddenCardToClone').clone(true);
            newItem.removeAttr('id').removeAttr('style').attr('id', 'invitation-id_' + item.invitation_id);

            newItem.find('.mentorid').attr("id", 'mentor-id_' + item.mentorId);
            newItem.find('.invoiceid').attr("id", 'invoice-id_' + item.invoice_id);
            newItem.find('.duration').attr("id", 'duration_' + item.duration);
            newItem.find('.mentorslug').attr("id", 'mentor-slug_' + item.slug);

            newItem.find('.avatar img').attr("src", item.finalProfileImageUrl);
            newItem.find('.mentSumName').html(item.first_name + " " + item.last_name);
            newItem.find('.cdesc').html(item.company_name);
            newItem.find('.pdesc').html(item.profession);
            newItem.find('.charge').html(item.hourly_rate);
            newItem.find('.currency').html(item.currency);

            newItem.find('.accept').addClass('acceptInvite').removeClass('accept').attr('invitation-id', item.invitation_id);
            newItem.find('.decline').addClass('declineInvite').removeClass('decline').attr('invitation-id', item.invitation_id);

            $.each(item.invitation_slots, function (slot_index, slot_item) {
                var starttext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).format('Do MMMM, Y hh:mm A');
                var endtext = moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone).add(item.duration, 'minutes').format('hh:mm A');
                var disabled = (moment.tz(slot_item.date_time, "UTC").tz(mentor_timezone) > moment.tz(mentor_timezone)) ? '' : 'disabled';

                var slot_html = '<li><input ' + disabled + ' type="radio" name="invitation_' + item.invitation_id + '" class="mt2 mr5 invitationSlotSelected" value="' + slot_item.id + '">' + starttext + ' to ' + endtext + '</li>';
                newItem.find('.slot').append(slot_html);
            });


            $('#userPendingInvitesListing').append(newItem);

            $('#fileAttached_' + item.invitation_id).on('shown.bs.collapse', function () {
                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            }).on('hidden.bs.collapse', function () {
                var $container = $('.masonry-container');
                $container.masonry({
                    columnWidth: '.item',
                    itemSelector: '.item'
                });
            });
        });
    }
    $.unblockUI();
}