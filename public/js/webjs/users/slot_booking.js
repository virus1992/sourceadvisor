var options = '';
var zone = "";
var tzone = $("#zone").val();
var noFiles = 0;
var maxFiles = 10;
var previousTime = '';
var zoneArray = [];
if (tzone != "") {
    zone = tzone;
}
function fTimeZone(zoneId, cb) {
    return zoneArray.find(function (element) {
        return element.cZone === zoneId;
    }).cZone;
}
$(document).ready(function () {
    var chkses = $("#sesuserId").val();
    if (chkses == "") {
        //window.location = '/login';
    }

    //get time zone list
    getTimeZone();



    //get mentor availability date and load it in datepicker
    getMentorAvailability($('#zone').val(), $('#mentorId').val());

    // called on timezone is selected
    $("#timezone").on("change", function () {
        zone = fTimeZone($(this).val());
        var selectedTz = fTimeZone($(this).find("option:selected").val());
        getMentorAvailability(selectedTz, $('#mentorId').val());
        $("#slot1_date").val("");
        $("#slot2_date").val("");
        $("#slot3_date").val("");
        $("#slot1_time").val("");
        $("#slot2_time").val("");
        $("#slot3_time").val("");

        if ($("#slot1_date").val() != "") {
            getMentorAvailabilityTimeSlot1($("#slot1_date").val());
        }
        if ($("#slot2_date").val() != "") {
            getMentorAvailabilityTimeSlot2($("#slot2_date").val());
        }
        if ($("#slot3_date").val() != "") {
            getMentorAvailabilityTimeSlot3($("#slot3_date").val());
        }
    });

    $("#call_duration").on("change", function () {
        var selectedTz = fTimeZone($("#timezone").val());
        getMentorAvailability(selectedTz, $('#mentorId').val());

        if ($("#slot1_date").val() != "") {
            getMentorAvailabilityTimeSlot1($("#slot1_date").val());
        }
        if ($("#slot2_date").val() != "") {
            getMentorAvailabilityTimeSlot2($("#slot2_date").val());
        }
        if ($("#slot3_date").val() != "") {
            getMentorAvailabilityTimeSlot3($("#slot3_date").val());
        }
    });

    /*** on change of date picker get available time on that date accordingly ***/
    $("#slot1_date").on("change", function () {
        var week = new Date($(this).val());
        var weekday = week.getDay();
        if ($(this).val() != "") {
            getMentorAvailabilityTimeSlot1($(this).val(), weekday);
        }
    });

    $("#slot2_date").on("change", function () {
        var week = new Date($(this).val());
        var weekday = week.getDay();
        if ($(this).val() != "") {
            getMentorAvailabilityTimeSlot2($(this).val(), weekday);
        }
    });

    $("#slot3_date").on("change", function () {
        var week = new Date($(this).val());
        var weekday = week.getDay();
        if ($(this).val() != "") {
            getMentorAvailabilityTimeSlot3($(this).val(), weekday);
        }
    });

    //send message to mentor validation and saving data

    $('#sendMessage').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        rules: {
            mentormsg: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            mentormsg: {
                required: 'Please enter some message.'
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            $.ajax({
                url: $('#absPath').val() + 'users/send_message_mentor',
                method: "POST",
                data: $('#sendMessage').serialize(),
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    if (response.message == "success") {
                        $('#sendMessage')[0].reset();
                        $('#askTimeSlot').modal('toggle');
                        displayAjaxNotificationMessage("Message sent successfully.", "success");
                    } else {
                        $('#sendMessage')[0].reset();
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                    }
                },
                error: function (response) {
                    $.unblockUI();
                    $('#sendMessage')[0].reset();
                    $('#askTimeSlot').modal('toggle');
                    displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                }

            });
            return false;
        }
    });

    $('#bookSlotForm').validate({
        errorClass: "validate_error",
        ignore: ":hidden",
        /* groups: {// consolidate messages into one
         names: "slot1_date slot2_date slot3_date slot1_time slot2_time slot3_time"
         },*/
        rules: {
            slot1_date: {
                require_from_group: [1, ".date"],
                noBlankSpace: true
            },
            slot1_time: {
                require_from_group: [1, ".slottimepicker"],
                noBlankSpace: true
            },
            call_duration: {
                required: true,
                noBlankSpace: true
            },
            meet_purpose: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            slot1_date: {
                require_from_group: 'Please specify atleast one date slot.'
            },
            slot1_time: {
                require_from_group: 'Please specify atleast one time slot.'
            },
            call_duration: {
                required: 'Please select Duration'
            },
            meet_purpose: {
                required: 'Please enter meeting purpose'
            }
        },
        errorPlacement: function (error, element) {
            if (element.attr('name') == 'slot1_date' || element.attr('name') == 'slot2_date' || element.attr('name') == 'slot3_date' || element.attr('name') == 'slot1_time' || element.attr('name') == 'slot2_time' || element.attr('name') == 'slot3_time') {
                element.parent().append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            $.blockUI();
            var multi = $('.fileidsp');
            var files_array = [];

            $.each(multi, function (index, item) {
                files_array.push($(item).data('documentid'));
            });
            $("#fileids").val(files_array);
            $(form).ajaxSubmit({
                url: $('#absPath').val() + 'users/bookSlot', // target element(s) to be updated with server response 
                beforeSubmit: function (formData, jqForm, options) {
                    var queryString = $.param(formData);
                    return true;
                }, // pre-submit callback 
                success: function (objResponse, statusText, xhr, $form) {
                    if (typeof objResponse == 'object') {
                        if (objResponse.message == "success") {
                            var invoiceId = objResponse.invoiceId;
                            var mentorSlug = $('#mentorSlug').val();

                            var objForPostParams = {
                                invoiceId: invoiceId,
                                mentorSlug: mentorSlug,
                                objParams: JSON.stringify(objResponse.objParams)
                            };
                            var redirectURI = $('#absPath').val() + 'users/slot_booking_payment';
                            $.redirect(redirectURI, objForPostParams, "POST");

                        } else if (objResponse.message == "error") {
                            displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                            $.unblockUI();
                        } else if (objResponse.message == "already") {
                            var strSlotCounts = "";
                            if (objResponse.data.length > 0) {
                                strSlotCounts += "<ul>";
                            }
                            $.each(objResponse.data, function (index, value) {
                                strSlotCounts += "<li>The time you have selected in slot " + value + " to request this meeting is already booked. kindly select another time for the proposed meeting.</li>";
                            });
                            if (objResponse.data.length > 0) {
                                strSlotCounts += "</ul>";
                            }
                            displayAjaxNotificationMessage(strSlotCounts, "danger");
                            $.unblockUI();
                        } else if (objResponse.message == "minimumSlotDiff") {
                            var strSlotCounts = "";
                            if (objResponse.data.length > 0) {
                                strSlotCounts += "<ul>";
                            }
                            var strTmp = "";
                            $.each(objResponse.data, function (index, value) {
                                if (value == 1) {
                                    strTmp += value + "st time slot, ";
                                } else if (value == 2) {
                                    strTmp += value + "nd time slot, ";
                                } else if (value == 3) {
                                    strTmp += value + "rd time slot, ";
                                }
                                //strSlotCounts += "<li>Slot number " + value + " is already occupied.</li>";
                            });


                            //strSlotCounts += "<li>Meeting book must be after " + objResponse.minimumExpectedSlot + ", Please change slot number " + strTmp + ".</li>";
                            strSlotCounts += "<li>The earliest meeting request that can be sent out is " + parseFloat(objResponse.adminTimeDiff).toFixed(2) + " hours from your current time, kindly request a meeting only after" + objResponse.minimumExpectedSlot + " from mentor's available time slots. Kindly amend " + strTmp + " accordingly. </li>";

                            if (objResponse.data.length > 0) {
                                strSlotCounts += "</ul>";
                            }
                            displayAjaxNotificationMessage(strSlotCounts, "danger");
                            $.unblockUI();
                        }
                    } else {
                        displayAjaxNotificationMessage("Something went wrong.Try Again!!", "danger");
                        $.unblockUI();
                    }
                }, // post-submit callback 
                //resetForm: true
            });
            return false;
        }
    });

    $("#makePayment").prop("disabled", false);

    $('#call_duration').change(function () {
        //calculate_payment();
    });

    $('#apply_discount_btn').click(function (e) {
        e.preventDefault();

    });

    $(document.body).on('click', ".remove_slot_document", function () {
        if (confirm("Are you sure you want to remove this document?")) {
            $(this).parent().remove();
            --noFiles;
        } else {
            return false;
        }
    });

    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = $('#absPath').val() + 'users/uploadfile';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        limitConcurrentUploads: maxFiles,
        //acceptFileTypes: /(\.|\/)(gif|jpe?g|png|sh)$/i,
        maxFileSize: 10000000,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                //var fileUploaded = $('<p data-documentId=' + file.documentId + ' class="fileidsp"/>').text(file.name + " - File Uploaded Successfully!");
                var fileHtml = '<p data-documentId=' + file.documentId + ' class="fileidsp">' + file.name + '- <a href="javascript:;" class="remove_slot_document"><i class="fa fa-times"></i></a></p>';
                $(fileHtml).appendTo('#files');
            });
            $('#progress .progress-bar').css('width', '0%');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width', progress + '%');
        },
        change: function (e, data) {
            if (data.files.length > maxFiles || noFiles > maxFiles || (data.files.length + noFiles) > maxFiles) {
                alert("Max 10 files are allowed");
                return false;
            }
        }
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index];
        if (file.error) {
            $('#files').append($('<span class="text-danger"/>').text(file.name + " - " + file.error));
        }
    }).on('fileuploaddone', function (e, data) {
        noFiles = noFiles + 1;
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    // CKEDITOR Custom confiduration
    var editor = CKEDITOR.replace('ck', {
        customConfig: '/js/ckeditor/slot_ckconfig.js'
    });
    editor.on('change', function (evt) {
        // getData() returns CKEditor's HTML content.
        $(".ckeditor").val(evt.editor.getData());
    });

    $('.slottimepicker').on('focus', function () {
        previousTime = this.value;
    }).change(function () {
        showHideOption(previousTime, $('[name="' + $(this).attr('name').replace('time', 'date') + '"]').val(), 'show');
        if (this.value != '') {
            showHideOption(this.value, $('[name="' + $(this).attr('name').replace('time', 'date') + '"]').val(), 'hide');
        }
    });
});

function showHideOption(value, dateValue, action) {
    if (action == 'show') {
        $.each($('.slottimepicker'), function (index, item) {
            if (dateValue != $('[name="' + $(this).attr('name').replace('time', 'date') + '"]')) {
                $(this).find('option[value="' + value + '"]').show();
            }
        });
    } else if (action == 'hide') {
        $.each($('.slottimepicker'), function (index, item) {
            if (dateValue == $('[name="' + $(this).attr('name').replace('time', 'date') + '"]')) {
                $(this).find('option[value="' + value + '"]').hide();
            }
        });
    }
}

// get mentor availability date from database.
function getMentorAvailability(tz, mentorId) {
    var dataToSend = { tz: tz, mentorId: mentorId };
    $.ajax({
        url: $('#absPath').val() + 'users/availableDate',
        method: "POST",
        async: false,
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                getDeletedDates(dataToSend, arrUserData)
                // initDate(arrUserData);
            }
        },
        complete: function () {
            $.unblockUI();
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

//Get deleted dates
function getDeletedDates(dataToSend, arrUserData) {
    $.ajax({
        url: $('#absPath').val() + 'users/getDeletedDates',
        method: "POST",
        async: false,
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                var arrDeletedData = response.data;
                initDate(arrUserData, arrDeletedData);
            }
        },
        complete: function () {
            $.unblockUI();
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}
/*
 * Get Mentor Available dates from date range according to selected timezone
 */
function getDates(startDate, stopDate) {
    var dateArray = new Array();
    var currentDate = startDate;

    while (currentDate <= stopDate) {
        if (currentDate >= moment().tz(zone).format('YYYY-MM-DD')) {
            dateArray.push(moment(currentDate).format("DD.MM.YYYY"));
        }
        currentDate = moment(currentDate).add(1, 'days').format('YYYY-MM-DD');
    }
    return dateArray;
}
function getDateArray(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}
//initialize datepciker
function initDate(arrUserData, arrDeletedData) {

    var deletedDates = [];
    if (arrDeletedData.length > 0) {
        arrDeletedData.forEach(function (element) {
            var date = element.start_date_time.split(' ')[0];
            deletedDates.push(date)

        })
    }
    var Tz = fTimeZone($('#timezone').val());
    var date = moment().tz(Tz);
    var today = moment().tz(Tz);
    var last = moment().tz(Tz).add(1, 'year');

    var enabled_dates = [];
    var range = [];
    $.each(arrUserData, function (index, arrUserInfo) {
        if (arrUserInfo.repeat_type == 0) {
            var tempstartdate = moment.tz(arrUserInfo.start, "UTC").tz(Tz).format("YYYY-MM-DD");
            var tempenddate = moment.tz(arrUserInfo.end, "UTC").tz(Tz).format("YYYY-MM-DD");
            range = getDates(tempstartdate, tempenddate);
            for (var i = 0; i < range.length; i++) {
                enabled_dates.push(range[i]);
            }
        } else {
            var tempstartdate = moment.tz(arrUserInfo.start, "UTC").tz(Tz).format("YYYY-MM-DD");
            var tempenddate = moment.tz(arrUserInfo.end, "UTC").tz(Tz).format("YYYY-MM-DD");
            range = getDates(tempstartdate, tempenddate);
            for (var i = 0; i < range.length; i++) {
                enabled_dates.push(range[i]);
            }
            // var repeater = JSON.parse(arrUserInfo.repeat_when)
            // var tempstartdate = moment.tz(arrUserInfo.start, "UTC").tz(Tz).format("YYYY-MM-DD");
            // var tempenddate = moment.tz(arrUserInfo.end, "UTC").tz(Tz).format("YYYY-MM-DD");
            // range = getDateArray(tempstartdate, tempenddate);
            // range.forEach(function (rngDate) {
            //     var found = repeater.filter(function (element) {
            //         //var tempDt = moment.tz(rngDate, "UTC").tz(Tz).format("YYYY-MM-DD");
            //         var tempDt = moment.tz(rngDate, "UTC").format("YYYY-MM-DD");
            //         var d = new Date(moment(tempDt));
            //         var n = d.getDay();
            //         if (element == n) {
            //             return rngDate;
            //         }
            //     });
            //     var deleteFound = deletedDates.filter(function (element) {
            //         if (element == rngDate) {
            //             return rngDate;
            //         }
            //     });
            //     if (found.length > 0 && deleteFound.length === 0) {

            //         enabled_dates.push(moment(rngDate).format("DD.MM.YYYY"))
            //     }
            // })
            //Recurring.

            //            var week = new Date(start_date);
            //            var weekday = week.getDay();
            //
            //            if (arrUserInfo.repeat_when == 0) {
            //                var addDays = 7 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                // enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //            } else if (arrUserInfo.repeat_when == 1) {
            //                var addDays = 1 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                // enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            } else if (arrUserInfo.repeat_when == 2) {
            //                var addDays = 2 - parseInt(weekday);
            //
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //
            //                //enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            } else if (arrUserInfo.repeat_when == 3) {
            //                var addDays = 3 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                //enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            } else if (arrUserInfo.repeat_when == 4) {
            //                var addDays = 4 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                //enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            } else if (arrUserInfo.repeat_when == 5) {
            //                var addDays = 5 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                //enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            } else if (arrUserInfo.repeat_when == 6) {
            //                var addDays = 6 - parseInt(weekday);
            //                var start_date = moment(start_date).add("days", addDays);
            //
            //                var day = start_date.format('DD');
            //                var month = start_date.format('MM');
            //                var year = start_date.format('YYYY');
            //
            //                first_start_date = day + '.' + month + '.' + year;
            //                //enabled_dates.push(first_start_date);
            //                start_date = year + '-' + month + '-' + day;
            //
            //            }
            //            if (arrUserInfo.repeat_when != 0) {
            //                var start_date = new Date(start_date);
            //                start_date.setDate(start_date.getDate() + 7);
            //            }
            //            for (var d = new Date(start_date); d <= new Date(end_date); d.setDate(d.getDate() + 7)) {
            //                var fstart_date = moment(d);
            //                var day = fstart_date.format('DD');
            //                var month = fstart_date.format('MM');
            //                var year = fstart_date.format('YYYY');
            //
            //                fstart_date = day + '.' + month + '.' + year;
            //
            //                enabled_dates.push(fstart_date);
            //            }

        }
    });

    jQuery("#slot1_date").datepicker("destroy"); // To reinitialize available dates
    $("#slot1_date").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: date.format('YYYY-MM-DD'),
        endDate: last.format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd',
        beforeShowDay: function (date) {
            //var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd.mm.yyyy', 'pl');
            //var formattedDate = date.toLocaleDateString('pl', {day: '2-digit', year: 'numeric', month: '2-digit'});
            var formattedDate = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
            if ($.inArray(formattedDate.toString(), enabled_dates) != -1) {
                return {
                    enabled: true,
                    classes: "highlightDate"
                };
            } else {
                return {
                    enabled: false,
                    classes: "disabledDate"
                };
            }
            return;
        }
    });


    jQuery("#slot2_date").datepicker("destroy"); // To reinitialize available dates
    $("#slot2_date").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: date.format('YYYY-MM-DD'),
        endDate: last.format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd',
        beforeShowDay: function (date) {
            //var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd.mm.yyyy', 'pl');
            //var formattedDate = date.toLocaleDateString('pl', {day: '2-digit', year: 'numeric', month: '2-digit'});
            var formattedDate = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
            if ($.inArray(formattedDate.toString(), enabled_dates) != -1) {
                return {
                    enabled: true,
                    classes: "highlightDate"
                };
            } else {
                return {
                    enabled: false,
                    classes: "disabledDate"
                };
            }
            return;
        }
    });

    jQuery("#slot3_date").datepicker("destroy");   // To reinitialize available dates
    $("#slot3_date").datepicker({
        autoclose: true,
        disableEntry: true,
        default: true,
        startDate: date.format('YYYY-MM-DD'),
        endDate: last.format('YYYY-MM-DD'),
        format: 'yyyy-mm-dd',
        beforeShowDay: function (date) {
            //var formattedDate = $.fn.datepicker.DPGlobal.formatDate(date, 'dd.mm.yyyy', 'pl');
            //var formattedDate = date.toLocaleDateString('pl', {day: '2-digit', year: 'numeric', month: '2-digit'});
            var formattedDate = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear();
            if ($.inArray(formattedDate.toString(), enabled_dates) != -1) {
                return {
                    enabled: true,
                    classes: "highlightDate"
                };
            } else {
                return {
                    enabled: false,
                    classes: "disabledDate"
                };
            }
            return;
        }
    });
}

// get mentor time for slot1 from database.
function getMentorAvailabilityTimeSlot1(date, day) {

    var dataToSend = {
        date: date,
        day: day,
        tz: fTimeZone($("#timezone").val()),
        mentorId: $('#mentorId').val()
    };

    $.ajax({
        url: $('#absPath').val() + 'users/availableTime',
        method: "POST",
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response.message == "success") {
                var arrUserData = response.data;
                initTimeSlot1(arrUserData);
            }

        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

// get mentor time for slot2 from database.
function getMentorAvailabilityTimeSlot2(date, day) {

    var dataToSend = {
        date: date,
        day: day,
        tz: fTimeZone($("#timezone").val()),
        mentorId: $('#mentorId').val()
    };

    $.ajax({
        url: $('#absPath').val() + 'users/availableTime',
        method: "POST",
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                initTimeSlot2(arrUserData);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

// get mentor time for slot3 from database.
function getMentorAvailabilityTimeSlot3(date, day) {

    var dataToSend = {
        date: date,
        day: day,
        tz: fTimeZone($("#timezone").val()),
        mentorId: $('#mentorId').val()
    };

    $.ajax({
        url: $('#absPath').val() + 'users/availableTime',
        method: "POST",
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "success") {
                var arrUserData = response.data;
                initTimeSlot3(arrUserData);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}

/*
 * @author : Rahul Thakkar
 * returns timeslots in interval of selected duration from selected available date
 */

// function getTimeSlotsFromDateRange(arrUserData, slotnumber) {
//     
// var timeSlots = [];
// var duration = $("#call_duration").val();
// var zone = $("#timezone").val();
// var datePicker1 = $("#slot" + slotnumber + "_date").val();
// $.each(arrUserData, function (index, arrUserInfo) {
//     
//     var startDate = arrUserInfo.start;
//     var startTime = arrUserInfo.start.split(' ')[1];
//     var endTime = arrUserInfo.end.split(' ')[1];
//     var endDate = arrUserInfo.end;

//     if (arrUserInfo.repeat_type == "1") {
//         startDate = datePicker1 + ' ' + startTime;
//         endDate = datePicker1 + ' ' + endTime;
//     }
//     var currentServerTime = arrUserInfo.currentServerTime;
//     var startDateinTz = moment.tz(startDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
//     var endDateinTz = moment.tz(endDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
//     var tempStartDt=startDateinTz.split(' ')[0]
//     var tempEndTime=endDateinTz.split(' ')[1]
//     var tempEndDt=endDateinTz.split(' ')[0]
//     if(tempStartDt!==tempEndDt){
//         endDateinTz=tempStartDt+' '+tempEndTime;
//     }
//     var currentTimeOfTz = moment.tz(currentServerTime, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
//     var slot, nextslot;
//     for (var i = moment(startDateinTz); i <= moment(endDateinTz); i.add(duration, "minutes"))
//     {
//         var diffDuration = moment.duration(moment(endDateinTz).diff(i)).asMinutes();
//         slot = i.format("HH:mm");
//         var j = i.clone();
//         
//         nextslot = j.add(duration - 1, "minutes").format("YYYY-MM-DD");
//         
//         var objCurrentTimeMoment = moment(currentTimeOfTz);
//         
//         // i.format("YYYY-MM-DD") == datePicker1 && //Removed condition from below if
//         if (nextslot == i.format("YYYY-MM-DD") && (i >= objCurrentTimeMoment))
//         {
//             
//             if ((moment($('#slot1_date').val() + ' ' + $('#slot1_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot2_date').val() + ' ' + $('#slot2_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot3_date').val() + ' ' + $('#slot3_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm'))) {
//                 if (timeSlots.indexOf(slot) == "-1")
//                 {
//                     if (diffDuration >= duration) {
//                         timeSlots.push(i.format("HH:mm"));
//                     }
//                 }
//             }
//         }
//     }
// });
// return timeSlots;
// }
function getTimeSlotsFromDateRange(arrUserData, slotnumber) {
    var timeSlots = [];
    var duration = $("#call_duration").val();
    var zone = $("#timezone").val();
    var datePicker1 = $("#slot" + slotnumber + "_date").val();
    $.each(arrUserData, function (index, arrUserInfo) {

        if (arrUserInfo.repeat_type == "0") {
            var startDate = arrUserInfo.start;
            var endDate = arrUserInfo.end;
            var currentServerTime = arrUserInfo.currentServerTime;
            var startDateinTz = moment.tz(startDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var endDateinTz = moment.tz(endDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var currentTimeOfTz = moment.tz(currentServerTime, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var slot, nextslot;
            for (var i = moment(startDateinTz); i <= moment(endDateinTz); i.add(duration, "minutes")) {
                var diffDuration = moment.duration(moment(endDateinTz).diff(i)).asMinutes();
                slot = i.format("HH:mm");
                var j = i.clone();

                nextslot = j.add(duration - 1, "minutes").format("YYYY-MM-DD");
                var objCurrentTimeMoment = moment(currentTimeOfTz);
                if (i.format("YYYY-MM-DD") == datePicker1 && nextslot == i.format("YYYY-MM-DD") && (i >= objCurrentTimeMoment)) {
                    if ((moment($('#slot1_date').val() + ' ' + $('#slot1_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot2_date').val() + ' ' + $('#slot2_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot3_date').val() + ' ' + $('#slot3_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm'))) {
                        if (timeSlots.indexOf(slot) == "-1") {
                            if (diffDuration >= duration) {
                                timeSlots.push(i.format("HH:mm"));
                            }
                        }
                    }
                }
            }
        } else {
            var startDate = arrUserInfo.start;
            var endDate = arrUserInfo.end;
            var currentServerTime = arrUserInfo.currentServerTime;
            var startDateinTz = moment.tz(startDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var endDateinTz = moment.tz(endDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var currentTimeOfTz = moment.tz(currentServerTime, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            var slot, nextslot;
            for (var i = moment(startDateinTz); i <= moment(endDateinTz); i.add(duration, "minutes")) {
                var diffDuration = moment.duration(moment(endDateinTz).diff(i)).asMinutes();
                slot = i.format("HH:mm");
                var j = i.clone();

                nextslot = j.add(duration - 1, "minutes").format("YYYY-MM-DD");
                var objCurrentTimeMoment = moment(currentTimeOfTz);
                if (i.format("YYYY-MM-DD") == datePicker1 && nextslot == i.format("YYYY-MM-DD") && (i >= objCurrentTimeMoment)) {
                    if ((moment($('#slot1_date').val() + ' ' + $('#slot1_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot2_date').val() + ' ' + $('#slot2_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot3_date').val() + ' ' + $('#slot3_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm'))) {
                        if (timeSlots.indexOf(slot) == "-1") {
                            if (diffDuration >= duration) {
                                timeSlots.push(i.format("HH:mm"));
                            }
                        }
                    }
                }
            }
            // var startDate = arrUserInfo.start;
            // var startTime = arrUserInfo.start.split(' ')[1];
            // var endTime = arrUserInfo.end.split(' ')[1];
            // var endDate = arrUserInfo.end;

            // if (arrUserInfo.repeat_type == "1") {
            //     startDate = datePicker1 + ' ' + startTime;
            //     endDate = datePicker1 + ' ' + endTime;
            // }
            // var currentServerTime = arrUserInfo.currentServerTime;
            // var startDateinTz = moment.tz(startDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            // var endDateinTz = moment.tz(endDate, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            // var tempStartDt = startDateinTz.split(' ')[0]
            // var tempEndTime = endDateinTz.split(' ')[1]
            // var tempEndDt = endDateinTz.split(' ')[0]
            // if (tempStartDt !== tempEndDt) {
            //     endDateinTz = tempStartDt + ' ' + tempEndTime;
            // }
            // var currentTimeOfTz = moment.tz(currentServerTime, "UTC").tz(zone).format("YYYY-MM-DD HH:mm");
            // var slot, nextslot;
            // for (var i = moment(startDateinTz); i <= moment(endDateinTz); i.add(duration, "minutes")) {
            //     var diffDuration = moment.duration(moment(endDateinTz).diff(i)).asMinutes();
            //     slot = i.format("HH:mm");
            //     var j = i.clone();

            //     nextslot = j.add(duration - 1, "minutes").format("YYYY-MM-DD");

            //     var objCurrentTimeMoment = moment(currentTimeOfTz);

            //     // i.format("YYYY-MM-DD") == datePicker1 && //Removed condition from below if
            //     if (nextslot == i.format("YYYY-MM-DD") && (i >= objCurrentTimeMoment)) {

            //         if ((moment($('#slot1_date').val() + ' ' + $('#slot1_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot2_date').val() + ' ' + $('#slot2_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm')) && (moment($('#slot3_date').val() + ' ' + $('#slot3_time').val()).format('YYYY-MM-DD HH:mm') != i.format('YYYY-MM-DD HH:mm'))) {
            //             if (timeSlots.indexOf(slot) == "-1") {
            //                 if (diffDuration >= duration) {
            //                     timeSlots.push(i.format("HH:mm"));
            //                 }
            //             }
            //         }
            //     }
            // }
        }

    });
    return timeSlots;
}
function initTimeSlot1(arrUserData) {
    var timeSlots = [];
    timeSlots = getTimeSlotsFromDateRange(arrUserData, 1);
    $('#slot1_time option[value!=""]').remove();
    if (timeSlots.length > 0) {
        for (var i = 0; i < timeSlots.length; i++) {
            $('<option/>').val(timeSlots[i]).html(timeSlots[i]).appendTo('#slot1_time');
        }
    }
}

function initTimeSlot2(arrUserData) {
    var timeSlots = [];
    timeSlots = getTimeSlotsFromDateRange(arrUserData, 2);
    $('#slot2_time option[value!=""]').remove();

    if (timeSlots.length > 0) {
        for (var i = 0; i < timeSlots.length; i++) {
            $('<option/>').val(timeSlots[i]).html(timeSlots[i]).appendTo('#slot2_time');
        }
    }
}

function initTimeSlot3(arrUserData) {
    var timeSlots = [];
    timeSlots = getTimeSlotsFromDateRange(arrUserData, 3);
    $('#slot3_time option[value!=""]').remove();

    if (timeSlots.length > 0) {
        for (var i = 0; i < timeSlots.length; i++) {
            $('<option/>').val(timeSlots[i]).html(timeSlots[i]).appendTo('#slot3_time');
        }
    }
}

// get timezone list.
function getTimeZone() {

    $.ajax({
        url: $('#absPath').val() + 'users/getTimeZone',
        method: "POST",
        data: "",
        async: false,
        success: function (response) {
            if (response.error == "0") {
                var arrUserData = response.data;
                $.each(arrUserData, function (index, arrUserInfo) {
                    var Data = {};
                    Data = {
                        id: arrUserInfo.timeZoneName,
                        text: '<div style="display: inline-block;width: 100%;"><span style="text-align:left;">' + arrUserInfo.timeZoneName + '</span><span style="text-align:left;float:right;">' + arrUserInfo.timeZoneDisplayName + '</span></div>',
                        cZone: arrUserInfo.timeZoneName
                    }
                    zoneArray.push(Data)
                });

                $('#timezone').select2(
                    {
                        data: zoneArray,
                        templateResult: function (d) {
                            return $(d.text);
                        },
                        templateSelection: function (d) {
                            var found = zoneArray.find(function (element) {
                                if (d.selected) {
                                    return element.cZone === d.cZone;
                                } else {
                                    return element.cZone === tzone;
                                }
                            });
                            $('#timezone').val(found.id)
                            return $(found.text);
                            // return $(d.text);
                        }

                    });
                $('.select2-selection__rendered').removeAttr('title');
                // $("#timezone").select2();
                //                 $('#timezone').html("");

                //                 var strNewOptions = "";
                // //                strNewOptions += "<option value=''>Select Timezone</option>";

                //                 $.each(arrUserData, function (index, arrUserInfo) {
                //                     var select = "";
                //                     if (tzone == arrUserInfo.timeZoneName) {
                //                         select = "selected";
                //                     }
                //                     strNewOptions += "<option value='" + arrUserInfo.timeZoneName + "' " + select + " >" + arrUserInfo.timeZoneName + " ("+ arrUserInfo.timeZoneDisplayName + ")"+ "</option>";
                //                 });

                //                 $('#timezone').html(strNewOptions);
            }
        },
        error: function (response) {
            //$(".alert-danger").css("display", "block");
        }

    });
    return false;
}