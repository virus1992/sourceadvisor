var absPath = "";

$(document).ready(function () {

//    $("#cardExpiryYY").datepicker({
//        format: "yyyy", // Notice the Extra space at the beginning
//        viewMode: "years",
//        minViewMode: "years",
//        autoclose: true,
//        startDate: 'y'
//    });


    absPath = $('#absPath').val();

    Stripe.setPublishableKey(STIPEPUBLICKEY);

    var calculatePaymentInterval = setInterval(function () {
        if (typeof dbConstant == "object") {
            calculate_payment();
            clearInterval(calculatePaymentInterval);
        }
    }, 100);


    $("#makePayment").on('click', function () {
        $('#successfulMeetingModal').modal('toggle');
        $('#frmCreditCardPayment').submit();
    });
    $('#frmCreditCardPayment').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            //  console.log(element);
            if (element.attr('id') == 'cardNumber') {
                element.parent().after(error);
            } else {
                element.after(error);
            }
        },
        rules: {
            cardNumber: {
                required: true,
                noBlankSpace: true,
                creditcard: true
            },
            cardExpiryMM: {
                required: true,
                noBlankSpace: true,
                dateM: true
            },
            cardExpiryYY: {
                required: true,
                noBlankSpace: true,
                //dateY: true,
                //minlength: 4,
                //maxlength: 4
            },
            cardCVC: {
                required: true,
                noBlankSpace: true,
                maxlength: 3,
                minlength: 3,
                number: true
            }
        },
        messages: {
            cardNumber: {
                required: "Please Enter Credit Card Number",
                noBlankSpace: "Please Enter Credit Card Number",
                creditcard: "Please Enter Valid Credit Card Number"
            },
            cardExpiryMM: {
                required: "Please Enter Expiry Month Number",
                noBlankSpace: "Please Enter Expiry Month Number",
                dateM: "Please Enter Valid Expiry Month Number"
            },
            cardExpiryYY: {
                required: "Please Enter Expiry Year Number",
                noBlankSpace: "Please Enter Expiry Year Number",
                dateY: "Please Enter Valid Expiry Year Number",
                minlength: "Please Enter Year In YYYY Format",
                maxlength: "Please Enter Year In YYYY Format"
            },
            cardCVC: {
                required: "Please Enter CVC Number",
                noBlankSpace: "Please Enter Valid CVC Number",
                maxlength: "Please Enter Valid CVC Number",
                minlength: "Please Enter Valid CVC Number",
                number: "Please Enter Valid CVC Number"
            }
        },
        submitHandler: function (form) {
//            $.blockUI();

            // Disable the submit button to prevent repeated clicks:
            //form.find('.submit').prop('disabled', true);

            // Request a token from Stripe:
            //Stripe.card.createToken(form, stripeResponseHandler);
            //var res = $('form').serialize();            
            sendRequestToServerForCreatingCharge();
        }
    });

    $("#applyPromoCodeBtnid").on("click", function () {
        var promocode = $("#promocode").val();
        var mentor_id = $("#mentor_id").val();

        if (promocode == "") {
            $("#validation").css("display", "block");
            return false;
        } else {
            chkPromoCode(promocode, mentor_id);
        }
    });

    $("#promocode").on("keyup", function () {
        var promo = $(this).val();

        if (promo == "") {
            $("#validation").css("display", "block");
        } else {
            $("#validation").css("display", "none");
        }

    });


    $("#removePromoCodeBtn").on("click", function () {
        $("#promocode_id").val("0");
        $("#promocode").prop("disabled", false);
        $("#promocode_discount").val("0");
        $("#promocode").val("");
        $('#discount_row').hide();
        calculate_payment();
        $("#removePromoCodeBtn").css("display", "none");
        $("#applyPromoCodeBtnid").css("display", "block");
    });

});


function stripeResponseHandler(status, response) {
    if (response.error) { // Problem!
        displayAjaxNotificationMessage(response.error.message, "danger");
        $.unblockUI();
    } else { // Token was created!
        // Get the token ID:
        var token = response.id;
        sendRequestToServerForCreatingCharge(response);
    }
}


function sendRequestToServerForCreatingCharge() {
    var data = {};

    var invoiceId = $('#hidInvoiceId').val();
    var hidMentorSlug = $('#hidMentorSlug').val();

    var privateInvite = $('#privateInvite').val();
    var invitation_slot_id = $('#invitation_slot_id').val();
    var invitation_id = $('#invitation_id').val();
    var promocode_id = $('#promocode_id').val();

    var cardType = $('#cardType').val();
    var cardNumber = $('#cardNumber').val();
    var cardExpiryMM = $('#cardExpiryMM').val();
    var cardExpiryYY = $('#cardExpiryYY').val();
    var cardCVC = $('#cardCVC').val();

    var encId = $('#encId').val();
    var encKey = $('#encKey').val();

    data.invoiceId = invoiceId;
    data.mentorSlug = hidMentorSlug;
    data.promocode_id = promocode_id;
    data.objSlotParams = JSON.parse($('#objSlotParams').val());

    data.privateInvite = privateInvite;
    data.invitation_slot_id = invitation_slot_id;
    data.invitation_id = invitation_id;

    data.cardNumber = cardNumber;
    data.cardExpiryMM = cardExpiryMM;
    data.cardExpiryYY = cardExpiryYY;
    data.cardCVC = cardCVC;
    data.cardType = cardType;

    //var stringData = JSON.stringify(data);
    var encDatas = encData(data, encKey);
    $.ajax({
        url: absPath + 'payment/chargeCustomerForCard',
        method: 'POST',
        type: 'json',
        data: {encData: encDatas, encId: encId},
        beforeSend: function () {
//            $.blockUI();
            showLoader();
        },
        complete: function () {
//            $.unblockUI();
            hideLoader();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            }

            if (response['error'] == "1") {
                var displayErr = response['data'];
                displayAjaxNotificationMessage(displayErr, "danger");
            } else {
                if (response['flagMsg'] == "PAYMENTDONE") {
                    var invoiceId = response['data'].invoiceId;
                    var mentorSlug = response['data'].mentorSlug;

                    var redirectURI = absPath + 'payment/payment_successfull/';
                    var objForPostParams = {
                        invoiceId: invoiceId,
                        mentorSlug: mentorSlug
                    };

                    $.redirect(redirectURI, objForPostParams, "POST");
                } else if (response['flagMsg'] == "PAYMENTDONEI") {
                    var invitation_id = response['data']['invitation_id'];
                    var win = window.open($('#absPath').val() + 'mentor/thankyou/' + invitation_id, '_blank');
                    var redirectURI = absPath + 'mentor/meeting_invites';
                    $.redirect(redirectURI, objForPostParams, "GET");
                }

            }
        }
    });
}

function encData(data, key) {

    // Encrypt 
    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), key);
    return ciphertext.toString();
}

function calculate_payment() {

    var mentor_real_rate = parseFloat($('#hdn_mentor_rate_real').val()).toFixed(2);
    var duration = parseFloat($('#hdn_meeting_duration').val()).toFixed(2);
    var wissenx_charge_percentage = parseFloat($('#wissenxChargesUserPercentage').val()).toFixed(2);
    var taxes_percentage = parseFloat($('#TAXCRG').val()).toFixed(2);
    var discount = 0;
    var total_payment = '';
    var wissenx_min_user_charge = parseFloat($('#MINWCP').val()).toFixed(2);

    var mentor_charge = mentor_real_rate;
    var wissenx_user_percentage = wissenx_charge_percentage;
    var wissenx_user_charge = 0;
    var promocode_id = $("#promocode_id").val();
    var promocode_discount_percentage = parseFloat($("#promocode_discount").val()).toFixed(2);
    var wissenx_tax_percentage = taxes_percentage;
    var wissenx_tax_charge = 0;
    var total_payable_amount = 0;
    var pay_to_mentor = 0;
    var pay_to_wissenx = 0;
    var wissenx_mentor_charge = 0;

    var wissenxUserChargePercentage = parseFloat($('#wissenxChargesUserPercentage').val()).toFixed(2);
    var wissenxMentorChargePercentage = parseFloat($('#wissenxChargesMentorPercentage').val()).toFixed(2);
    var taxChargePercentage = parseFloat($('#TAXCRG').val()).toFixed(2);

    var timeConstraint = parseFloat(parseFloat(duration) / 60);
    var mentorPayableCharge = parseFloat(parseFloat(mentor_charge) * parseFloat(timeConstraint)).toFixed(2);
    var mentorPayableChargeWithoutDiscount = parseFloat(parseFloat(mentor_charge) * parseFloat(timeConstraint)).toFixed(2);

    var promocode_discount_value = parseFloat(parseFloat(parseFloat(mentorPayableCharge) * parseFloat(promocode_discount_percentage)) / 100).toFixed(2);

    mentorPayableCharge = parseFloat(parseFloat(mentorPayableCharge) - parseFloat(promocode_discount_value)).toFixed(2);

    wissenx_user_charge = parseFloat(((parseFloat(mentorPayableCharge) * (parseFloat(wissenx_user_percentage) / 100)))).toFixed(2);

    if (parseFloat(wissenx_user_charge) < parseFloat(wissenx_min_user_charge)) {
        wissenx_user_charge = parseFloat(wissenx_min_user_charge).toFixed(2);
    }


    var taxableCharge = parseFloat(parseFloat(mentorPayableCharge) + parseInt(wissenx_user_charge) - parseFloat(promocode_discount_value));

    wissenx_tax_charge = parseFloat(parseFloat(taxableCharge) * parseFloat(parseFloat(taxChargePercentage) / 100)).toFixed(2);





    total_payable_amount = parseFloat(parseFloat(mentorPayableCharge) + parseFloat(wissenx_user_charge) + parseFloat(wissenx_tax_charge)).toFixed(2);

    pay_to_mentor = parseFloat(parseFloat(mentorPayableCharge) - parseFloat(mentorPayableCharge) * (parseFloat(wissenxMentorChargePercentage) / 100)).toFixed(2);

    pay_to_wissenx = parseFloat(parseFloat(total_payable_amount) - parseFloat(pay_to_mentor)).toFixed(2);

    wissenx_mentor_charge = parseFloat(parseFloat(total_payable_amount) - (parseFloat(pay_to_mentor) + parseFloat(wissenx_user_charge))).toFixed(2);

    /*
     console.log("timeConstraint :: ", timeConstraint);
     console.log("wissenx_user_charge :: ", wissenx_user_charge);
     console.log("taxableCharge :: ", taxableCharge);
     console.log("total_payable_amount :: ", total_payable_amount);
     console.log("pay_to_mentor :: ", pay_to_mentor);
     console.log("pay_to_wissenx :: ", pay_to_wissenx);*/

    $("#hdn_mentor_payable_charge").val(mentorPayableCharge);
    $("#hdn_mentor_rate").val(mentorPayableChargeWithoutDiscount);
    $("#mentor_rate").html(mentorPayableChargeWithoutDiscount);

    $("#hdn_wissenx_charge").val(wissenx_user_charge);
    $("#wissenx_charge").html(wissenx_user_charge);

    $("#hdn_taxes").val(wissenx_tax_charge);
    $("#taxes").html(wissenx_tax_charge);

    $("#hdn_discount").val(promocode_discount_value);
    $("#discount").html(promocode_discount_value);

    if (promocode_id > 0) {
        $('#discount_row').show();
    }

    $("#hdn_totalpayable").val(total_payable_amount);
    $("#totalpayable").html(total_payable_amount);

}

function chkPromoCode(promocode, mentor_id) {

    var data = {
        promocode: promocode,
        mentor_id: mentor_id
    };

    $.ajax({
        url: absPath + 'users/chkPromoCode',
        method: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            }

            if (response.error == "1") {
                displayAjaxNotificationMessage("Some Error occured, Please try again after some time.", "danger");
            } else {
                var objData = response.data;
                var serverMsg = objData.flagMsg;

                if (serverMsg == "is_used") {
                    displayAjaxNotificationMessage("Promo code you have entered is already used.", "danger");
                } else if (serverMsg == "mentor_invalid") {
                    displayAjaxNotificationMessage("Promo code you have entered is assigned to by other mentor.", "danger");
                } else if (serverMsg == "no_found") {
                    displayAjaxNotificationMessage("Promo code you have entered is invalid.", "danger");
                } else {
                    $("#promocode").prop("disabled", true);
                    $("#removePromoCodeBtn").css("display", "block");
                    $("#applyPromoCodeBtnid").css("display", "none");
                    var data = objData.result;
                    $("#promocode_id").val(data.promo_code_id);
                    $("#promocode_discount").val(data.discount);
                    calculate_payment();
                }
            }
        }
    });
}

$(function () {
    $('#cardNumber').validateCreditCard(function (result) {
        $('#cardType').val(result.card_type == null ? '' : result.card_type.name);
    });
});

function showLoader() {
    $('.loader').removeClass('hide');
    $("#bar").width(600);
    var progress = setInterval(function () {
        var $bar = $("#bar");

        if ($bar.width() >= 600) {
            clearInterval(progress);
        } else {
            $bar.width($bar.width() + 60);
        }
        $bar.text(($bar.width() / 6).toFixed(2) + "%");
        if ($bar.width() / 6 == 100) {
            $bar.text("Still working ... " + $bar.width() / 6 + "%");
        }
    }, 1);

}
function hideLoader() {
    $('.loader').addClass('hide');
}