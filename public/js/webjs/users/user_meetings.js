var meeting_data = '';
var mentor_timezone = '';
$("#sendMsgMentor").on('hidden.bs.modal', function () {
    $("#msgbody").val("");
});
$("#UserCancelModal").on('hidden.bs.modal', function () {
    $('input[id="userradiofirst"]').trigger('click');
    $("#user_reasontxt").val('');
});
$("#mentorCancelModal").on('hidden.bs.modal', function () {
    $('input[id="mentorradiofirst"]').trigger('click');
    $("#mentor_reasontxt").val('');
});
$("#giveReview").on('hidden.bs.modal', function () {
    $("#oppsiteUser").val("");
    $(".feedbackbtn").removeClass('delElement');
    $(".mtu").removeClass("starRating");
    $(".utm").removeClass("starRating");
    $('.fa').addClass('fa-star-o').removeClass('fa-star');
    $("#stars_U_M_1 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_2 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_3 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_4 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_U_M_5 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_M_U_1 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_M_U_2 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_M_U_3 span").first().removeClass("fa-star-o").addClass("fa-star");
    $("#stars_overAll span").first().removeClass("fa-star-o").addClass("fa-star");
});
$("#refundRequest").on('hidden.bs.modal', function () {
    $('#refund_reason option:eq(0)').attr('selected', 'selected');
    $("#refund_desc").val("");
});
$(document).ready(function () {

    $('#lstTimeZone').select2(
        {
            data: datax,
            templateResult: function (d) {
                return $(d.text); },
            templateSelection: function (d) {
                var found = datax.find(function(element) {
                   
                    if(d.selected){
                        return element.cZone === d.cZone;
                    }else{
                        return element.cZone === element.selectedId;
                    }    
                  });
                  $('#lstTimeZone').val(found.id)
                return $(found.text); 
            },
            
        });
        $('.select2-selection__rendered').removeAttr('title');
        var found ;
    $("#lstTimeZone").on("change", function () {
        $("#hidePageFuture").val('0');
        $("#hidePageCompleted").val('0');
        $('#hidefutureData').val('Y');
        $('#hidecompletedData').val('Y');
        $.blockUI();
               
        mentor_timezone = $(this).find("option:selected").text();
        found = datax.find(function(element) {
            return element.text === mentor_timezone;  
        });
        getMeetingData(found.cZone);
    });
    mentor_timezone = $('#lstTimeZone').select2('data')[0].text;
    found = datax.find(function(element) {
        return element.text === mentor_timezone;  
    });
    mentor_timezone=found.cZone
    getMeetingData(mentor_timezone);
    /* Code for star */

    $(document.body).on('click', '#giveReviewBtn', function () {

        var user = $(this).attr('data-rate_to');
        var oppUser = $(this).attr('data-mentor_id');
        var meetingId = $(this).attr('data-meeting_id');
        $(this).addClass('delElement');
        $("#oppsiteUser").val(oppUser);
        $("#meetingId").val(meetingId);
        if (user == "mentor") {
            $(".utm").addClass("starRating");
            $(".mtu").removeClass("starRating");
            //$("#usertomentor").css("display", "block");
            $("#usertomentor").removeClass('hidden');
            //$("#mentortouser").css("display", "none");
            $("#mentortouser").addClass('hidden');
        } else {
            $(".mtu").addClass("starRating");
            $(".utm").removeClass("starRating");
            //$("#usertomentor").css("display", "none");
            //$("#mentortouser").css("display", "block");
            $("#usertomentor").addClass('hidden');
            $("#mentortouser").removeClass('hidden');
        }

        $("#giveReview").modal('show');
    });
    $('#noclick').css('pointerEvents', 'none');
    $(".starrr").starrr({
        rating: 1
    });
    $('.starrr').on('starrr:change', function (e, value) {
        setAvg();
    });
    $("#feedback").on('keyup', function () {
        var feedback = $(this).val();
        if (feedback != "") {
            $("#submitreview").attr("disabled", false);
        } else {
            $("#submitreview").attr("disabled", true);
        }

    });
    $("#submitreview").on("click", function () {

        $('#stars_overAll').html('');
        var objRating = $('.starRating');
        var divider = 0;
        var totalOptions = 0;
        var starRatingData = [];
        starRatingData[0] = 0;
        if (objRating.length > 0) {
            totalOptions = objRating.length;
            $.each(objRating, function (index, element) {
                var thisRating = $(element);
                var indexing = thisRating.attr('data-index');
                var starElement = thisRating.find('.fa-star');
                var thisRatingValue = parseInt(starElement.length);
                starRatingData[indexing] = thisRatingValue;
                if (thisRatingValue > 0) {
                    divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
                }
            });
        }

        var avgRating = parseInt(divider) / parseInt(totalOptions);
        avgRating = Math.round(avgRating);
        overallhtml = setOverallRatingHtml(avgRating);
        $('#stars_overAll').html(overallhtml);
        var feedback = $("#feedback").val();
        var loggedInUser = $('#loggedInUser').val();
        var opposite = $("#oppsiteUser").val();
        var meetingDetailId = $("#meetingId").val();
        var data = {
            user_id: opposite,
            reviewer_id: loggedInUser,
            starRatingData: starRatingData,
            review_rating: avgRating,
            review_desc: feedback,
            meetingDetailId: meetingDetailId
        };
        data = JSON.stringify(data);
        $.ajax({
            url: $('#absPath').val() + "mentor/saveReview",
            type: 'POST',
            data: "data=" + data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.message == "success") {
                    $('#giveReview').modal('toggle');
                    $(".delElement").remove();
                    //////window.location.href = $('#absPath').val();
                }
            }
        });
    });
    /* Code for star */

    $(document.body).on('click', ".sendMentorMsg", function () {
        var mentor_id = $(this).attr("data-mentor_id");
        $("#mentor_id").val(mentor_id);
    });
    $(document.body).on('click', ".cancelMeetingUser", function () {

        var policy = $(this).attr("data-policy");
        var invitation_id = $(this).attr("data-invitation_id");
        var duration = $(this).attr("data-duration");
        var user_id = $(this).attr("data-user_id");
        var mentor_id = $(this).attr("data-mentor_id");
        var capture_id = $(this).attr("data-capture_id");
        $(".invitationIdUser").val(invitation_id);
        $("#user_duration").val(duration);
        $("#user_mentor_id").val(mentor_id);
        $("#user_user_id").val(user_id);
        $("#user_captureId").val(capture_id);
        var data = {
            userId: user_id,
            mentorId: mentor_id,
            invitationId: invitation_id,
            cancellationFor: "user"
        };
        $.ajax({
            url: absPath + 'miscRoutes/getCancellationPriceForUser',
            method: "post",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#displayPolicy").text(response.data.mentorPolicyAtTimeOfBooking);
                if (response.data.amountToRefund != "") {
                    $("#displayUserCost").text(response.data.amountToRefund);
                }
            }
        });
    });
    $(document.body).on('click', ".cancelMeetingMentor", function () {

        var invitation_id = $(this).attr("data-invitation_id");
        var user_id = $(this).attr("data-user_id");
        var mentor_id = $(this).attr("data-mentor_id");
        var duration = $(this).attr("data-duration");
        var capture_id = $(this).attr("data-capture_id");
        $(".invitationIdMentor").val(invitation_id);
        $("#mentor_duration").val(duration);
        $("#mentor_mentor_id").val(mentor_id);
        $("#mentor_user_id").val(user_id);
        $("#mentor_capture_id").val(capture_id);
        var data = {
            userId: user_id,
            mentorId: mentor_id,
            invitationId: invitation_id,
            cancellationFor: "mentor"
        };
        $.ajax({
            url: absPath + 'miscRoutes/getCancellationPriceForUser',
            method: "post",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.data.amountToRefund != "") {
                    $("#displayMentorCost").text(parseFloat(response.data.amountToRefund).toFixed(2));
                }
            }
        });
    });
    /***** user reason pop up ***********/

    $(document.body).on('click', ".cancelmeetinguserbtn", function () {
        $("#cancelMeetingUser").modal('toggle');
        $("#UserCancelModal").modal('toggle');
    });
    $(document.body).on('click', "#user_reasonbtn", function () {
        var invitationId = $(".invitationIdUser").val();
        var duration = $("#user_duration").val();
        var mentorId = $("#user_mentor_id").val();
        var userId = $("#user_user_id").val();
        var captureId = $("#user_captureId").val();
        var radioval = $('input[name=user_reason]:checked', '#userReasonForm').val();
        var typex = "";
        var rval = "";
        if (radioval == "user_other") {
            typex = 'OT';
            rval = $("#user_reasontxt").val();
        } else {
            typex = 'PD';
            rval = radioval;
        }

        var data = {
            invitationId: invitationId,
            duration: duration,
            mentorId: mentorId,
            userId: userId,
            typex: typex,
            rval: rval,
            captureId: captureId
        };
        userCancelRequest(data);
    });
    $("#user_reasontxt").hide();
    $("#user_chars").parent().hide();
    var maxLength = $('#user_chars').text();
    $('textarea#user_reasontxt').keyup(function () {
        var length = $(this).val().length;
        $('#user_chars').text(maxLength - length);
        if (length > 0) {
            $("#user_reasonbtn").prop('disabled', false);
            $("#user_valid").css('display', 'none');
        } else {
            $("#user_reasonbtn").prop('disabled', true);
            $("#user_valid").css('display', 'block');
        }
    });
    $("#userReasonForm input").on('change', function () {
        var chk = $('input[name=user_reason]:checked', '#userReasonForm').val();
        if (chk == "user_other") {
            $("#user_reasontxt").show();
            $("#user_chars").parent().show();
            $("#user_reasonbtn").prop('disabled', true);
            $("#user_valid").css('display', 'block');
        } else {
            $("#user_reasontxt").hide();
            $("#user_chars").parent().hide();
            $("#user_reasonbtn").prop('disabled', false);
            $("#user_valid").css('display', 'none');
        }
    });
    /************* mentor reason pop up **************/

    $(document.body).on('click', ".cancelmeetingmentorbtn", function () {
        $("#cancelMeetingMentor").modal('toggle');
        $("#mentorCancelModal").modal('toggle');
    });
    $(document.body).on('click', "#mentor_reasonbtn", function () {
        var invitationId = $(".invitationIdMentor").val();
        var duration = $("#mentor_duration").val();
        var mentorId = $("#mentor_mentor_id").val();
        var userId = $("#mentor_user_id").val();
        var capture_id = $("#mentor_capture_id").val();
        var radioval = $('input[name=mentor_reason]:checked', '#mentorReasonForm').val();
        var typex = "";
        var rval = "";
        if (radioval == "mentor_other") {
            typex = 'OT';
            rval = $("#mentor_reasontxt").val();
        } else {
            typex = 'PD';
            rval = radioval;
        }

        var data = {
            invitationId: invitationId,
            duration: duration,
            mentorId: mentorId,
            userId: userId,
            typex: typex,
            rval: rval,
            capture_id: capture_id
        };
        mentorCancelRequest(data);
    });
    $("#mentor_reasontxt").hide();
    $("#mentor_chars").parent().hide();
    var maxLength = $('#mentor_chars').text();
    $('textarea#mentor_reasontxt').keyup(function () {
        var length = $(this).val().length;
        $('#mentor_chars').text(maxLength - length);
        if (length > 0) {
            $("#mentor_reasonbtn").prop('disabled', false);
            $("#mentor_valid").css('display', 'none');
        } else {
            $("#mentor_reasonbtn").prop('disabled', true);
            $("#mentor_valid").css('display', 'block');
        }
    });
    $("#mentorReasonForm input").on('change', function () {
        var chk = $('input[name=mentor_reason]:checked', '#mentorReasonForm').val();
        if (chk == "mentor_other") {
            $("#mentor_reasontxt").show();
            $("#mentor_chars").parent().show();
            $("#mentor_reasonbtn").prop('disabled', true);
            $("#mentor_valid").css('display', 'block');
        } else {
            $("#mentor_reasontxt").hide();
            $("#mentor_chars").parent().hide();
            $("#mentor_reasonbtn").prop('disabled', false);
            $("#mentor_valid").css('display', 'none');
        }
    });
    /********* send msg **************/

    $("#msgbody").on('keyup', function () {
        var text = $(this).val();
        text = $.trim(text);
        if (text == "") {
            $("#sendMentorMsgBtn").prop('disabled', true);
        } else {
            $("#sendMentorMsgBtn").prop('disabled', false);
        }

    });
    $("#sendMentorMsgBtn").on("click", function () {
        sendMentorMsg();
    });
    $(window).scroll(function () {
        if ($('li.active a[href="#tab_future"]').length == "1") {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var hidFutureMeetingPage = parseInt($('#hidePageFuture').val());
                hidFutureMeetingPage += 1;
                if ($('#hidefutureData').val() == "Y") {
                    meetingData(hidFutureMeetingPage, "future", mentor_timezone);
                    $('#hidePageFuture').val(hidFutureMeetingPage);
                }
            }
        }

        if ($('li.active a[href="#tab_completed"]').length == "1") {
            if ($(window).scrollTop() + $(window).height() == $(document).height()) {
                var hidCompletedMeetingPage = parseInt($('#hidePageCompleted').val());
                hidCompletedMeetingPage += 1;
                if ($('#hidecompletedData').val() == "Y") {
                    meetingData(hidCompletedMeetingPage, "completed", mentor_timezone);
                    $('#hidePageCompleted').val(hidCompletedMeetingPage);
                }
            }
        }
    });
    $(document.body).on('click', ".costBreakApi", function () {
        var invitation_id = $(this).attr('data-invitation_id');
        var userType = $(this).attr('data-userType');
        getCostBreakUp(invitation_id, userType);
    });
    $(document.body).on('click', ".moreDetails", function () {
        var moreDetail = $(this).attr('data-moreDetail');
        $("#moreDetailSpan").html("");
        $("#moreDetailSpan").html(moreDetail);
    });
    $(document.body).on('click', ".meetingPurpose", function () {
        var purpose = $(this).attr('data-purpose');
        $("#morePurposeSpan").text("");
        $("#morePurposeSpan").text(purpose);
    });
    /******************* Request Refund Code ******************/

    $(document.body).on('click', ".refundRequestPopUp", function () {

        var invitation_id = $(this).attr("data-invitation_id");
        var user_id = $(this).attr("data-user_id");
        var mentor_id = $(this).attr("data-mentor_id");
        var meeting_id = $(this).attr("data-meeting_id");
        var duration_id = $(this).attr("data-duration");
        var meeting_url_key = $(this).attr("data-meeting_url_key");
        $("#refund_invitation_id").val(invitation_id);
        $("#refund_meeting_id").val(meeting_id);
        $("#refund_mentor_id").val(mentor_id);
        $("#refund_user_id").val(user_id);
        $("#refund_duration").val(duration_id);
        $("#refund_meeting_url_key").val(meeting_url_key);
    });
    $(document.body).on('click', "#raiseRefundRequestbtn", function () {

        var invitation_id = $("#refund_invitation_id").val();
        var meeting_id = $("#refund_meeting_id").val();
        var refund_reason = $("#refund_reason").val();
        var mentor_id = $("#refund_mentor_id").val();
        var user_id = $("#refund_user_id").val();
        var refund_desc = $("#refund_desc").val();
        var duration = $("#refund_duration").val();
        var meeting_url_key = $("#refund_meeting_url_key").val();
        var data = {
            userId: user_id,
            mentorId: mentor_id,
            invitationId: invitation_id,
            meeting_id: meeting_id,
            refund_reason: refund_reason,
            refund_desc: refund_desc,
            duration: duration,
            meeting_url_key: meeting_url_key
        };
        $.ajax({
            url: absPath + 'users/raiseRefundRequest',
            method: "post",
            data: data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                if (response.data == "success") {
                    displayAjaxNotificationMessage("Refund request raised successfully", "success");
                    $("#refundRequest").modal('toggle');
                    getMeetingData(mentor_timezone);
                } else {
                    displayAjaxNotificationMessage("Someting went wrong. Try again", "danger");
                    $("#refundRequest").modal('toggle');
                    getMeetingData(mentor_timezone);
                }
            }
        });
    });
    /******************* Request Refund Code ******************/




});
function setAvg() {
    var overallhtml = "";
    $('#stars_overAll').html(overallhtml);
    var objRating = $('.starRating');
    var divider = 0;
    var totalOptions = 0;
    if (objRating.length > 0) {
        totalOptions = objRating.length;
        $.each(objRating, function (index, element) {
            var thisRating = $(element);
            var starElement = thisRating.find('.fa-star');
            var thisRatingValue = parseInt(starElement.length);
            if (thisRatingValue > 0) {
                divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
            }
        });
    }

    var avgRating = parseInt(divider) / parseInt(totalOptions);
    avgRating = Math.round(avgRating);
    overallhtml = setOverallRatingHtml(avgRating);
    $('#stars_overAll').html(overallhtml);
}

function setOverallRatingHtml(value) {
    var html = "";
    for (var i = 1; i <= 5; i++) {
        if (i <= value) {
            html += "<span class='fa .fa-star-o fa-star'></span>";
        } else {
            html += "<span class='fa .fa-star-o fa-star-o'></span>";
        }
    }
    return html;
}

function getMeetingData(mentor_timezone) {

    var data = {
        timezone: mentor_timezone
    }

    $.ajax({
        url: $('#absPath').val() + 'users/meetingsData',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            console.log("response :: ", response);
            var meetingfutureCount = response.data.futureData.length;
            var meetingcompletedCount = response.data.completedData.length;
            var meetingfutureData = response.data.futureData;
            var meetingcompletedData = response.data.completedData;
            if (meetingfutureCount > 0) {
                $.each(meetingfutureData, function (index, thisResult) {
                    thisResult.pay_to_mentor = parseFloat(thisResult.pay_to_mentor).toFixed(2);
                    thisResult.total_payable_amount = parseFloat(thisResult.total_payable_amount).toFixed(2);
                    thisResult.final_user_paid_amount = parseFloat(thisResult.final_user_paid_amount).toFixed(2);
                    thisResult.final_pay_to_mentor = parseFloat(thisResult.final_pay_to_mentor).toFixed(2);
                    if (thisResult.invitation_documents.length > 0) {
                        $.each(thisResult.invitation_documents, function (index, thisResultDoc) {
                            thisResultDoc.document_path = $('#absPath').val() + thisResultDoc.document_path.replace("uploads/", "") + "?dl";
                            thisResultDoc.document_size = bytesToSize(thisResultDoc.document_size);
                        });
                    }
                });
            }

            if (meetingcompletedCount > 0) {
                $.each(meetingcompletedData, function (index, thisResult) {
                    thisResult.pay_to_mentor = parseFloat(thisResult.pay_to_mentor).toFixed(2);
                    thisResult.total_payable_amount = parseFloat(thisResult.total_payable_amount).toFixed(2);
                    thisResult.final_user_paid_amount = parseFloat(thisResult.final_user_paid_amount).toFixed(2);
                    thisResult.final_pay_to_mentor = parseFloat(thisResult.final_pay_to_mentor).toFixed(2);
                    if (thisResult.invitation_documents.length > 0) {
                        $.each(thisResult.invitation_documents, function (index, thisResultDoc) {
                            thisResultDoc.document_path = $('#absPath').val() + thisResultDoc.document_path.replace("uploads/", "") + "?dl";
                            thisResultDoc.document_size = bytesToSize(thisResultDoc.document_size);
                        });
                    }
                });
            }

            var arrListingFutureMeetingDisplayData = {
                meetingfutureData: meetingfutureData,
            };
            if (meetingfutureCount > 0) {

                /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                // Retrieve the HTML from the script tag we setup in step 1​
                // We use the id (header) of the script tag to target it on the page​
                var theTemplateScript = $("#meetingListingBoxes").html();
                // The Handlebars.compile function returns a function to theTemplate variable​
                var theTemplate = Handlebars.compile(theTemplateScript);
                $("#tab_future .meetinglisting").html(theTemplate({ meetingfutureAllData: arrListingFutureMeetingDisplayData }));
                //$('#mentorListingCount').html(meetingfutureCount);
            } else {
                $("#tab_future .user_section").html("<p style='font-size: 16px;padding: 30px;'>You do not have any upcoming meetings. Please check back later if you have any confirmed meetings coming up. Or click below to schedule a meeting</p>" +
                    "<a class='btn btn-primary' href='" + $("#absPath").val() + "mentor/listing'>New Invite</a>");

                $("#tab_future .mentor_section").html("<p style='font-size: 16px;padding: 30px;'>You do not have any upcoming meetings. Please check back later if you have any confirmed meetings coming up. Or click below to schedule a meeting</p>" +
                    "<a class='btn btn-primary' href='" + $("#absPath").val() + "mentor/calendar'>Send a Private Invite</a>");
            }

            var arrListingFutureMeetingDisplayData = {
                meetingfutureData: meetingcompletedData,
            };
            if (meetingcompletedCount > 0) {

                /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                // Retrieve the HTML from the script tag we setup in step 1​
                // We use the id (header) of the script tag to target it on the page​
                var theTemplateScript = $("#meetingListingBoxes").html();
                // The Handlebars.compile function returns a function to theTemplate variable​
                var theTemplate = Handlebars.compile(theTemplateScript);
                $("#tab_completed .meetinglisting").html(theTemplate({ meetingfutureAllData: arrListingFutureMeetingDisplayData }));
                //$('#mentorListingCount').html(meetingcompletedCount);
            } else {
                $("#tab_completed .user_section").html("<p style='font-size: 16px;padding: 30px;'>You do not have any completed meetings. Please check back later if you have any meetings that are ending soon. Or click below to schedule a meeting</p>" +
                    "<a class='btn btn-primary' href='" + $("#absPath").val() + "mentor/listing'>Request a Call</a>");

                $("#tab_completed .mentor_section").html("<p style='font-size: 16px;padding: 30px;'>You do not have any completed meetings. Please check back later if you have any meetings that are ending soon. Or click below to schedule a meeting</p>" +
                    "<a class='btn btn-primary' href='" + $("#absPath").val() + "mentor/calendar'>Send a Private Invite</a>");
            }
            $.unblockUI();
        }
    });
}

function meetingData(page, datatype, mentor_timezone) {

    var data = {
        page: page,
        datatype: datatype,
        timezone: mentor_timezone
    }


    $.ajax({
        url: $('#absPath').val() + 'users/meetingsDataPaging',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            var meetingCount = response.data.length;
            var meetingData = response.data;
            if (meetingCount > 0) {
                $.each(meetingData, function (index, thisResult) {
                    thisResult.pay_to_mentor = parseFloat(thisResult.pay_to_mentor).toFixed(2);
                    thisResult.total_payable_amount = parseFloat(thisResult.total_payable_amount).toFixed(2);
                    //thisResult.created_date = moment(thisResult.created_date).format('Do MMMM, Y hh:mm A');
                    thisResult.final_user_paid_amount = parseFloat(thisResult.final_user_paid_amount).toFixed(2);
                    thisResult.final_pay_to_mentor = parseFloat(thisResult.final_pay_to_mentor).toFixed(2);
                    if (thisResult.invitation_documents.length > 0) {
                        $.each(thisResult.invitation_documents, function (index, thisResultDoc) {
                            thisResultDoc.document_path = $('#absPath').val() + thisResultDoc.document_path.replace("uploads/", "") + "?dl";
                            thisResultDoc.document_size = bytesToSize(thisResultDoc.document_size);
                        });
                    }
                });
            }

            var arrListingFutureMeetingDisplayData = {
                meetingfutureData: meetingData
            };
            if (meetingCount > 0) {

                /*************************************** SEARCH BOX TEMPLATE START ****************************************/
                // Retrieve the HTML from the script tag we setup in step 1​
                // We use the id (header) of the script tag to target it on the page​
                var theTemplateScript = $("#meetingListingBoxes").html();
                // The Handlebars.compile function returns a function to theTemplate variable​
                var theTemplate = Handlebars.compile(theTemplateScript);
                if (datatype == "future") {
                    $("#tab_future .meetinglisting").append(theTemplate({ meetingfutureAllData: arrListingFutureMeetingDisplayData }));
                } else {
                    $("#tab_completed").append(theTemplate({ meetingfutureAllData: arrListingFutureMeetingDisplayData }));
                }
                //$('#mentorListingCount').html(meetingfutureCount);
            } else {
                if (datatype == "future") {
                    $('#hidefutureData').val('N');
                } else {
                    $('#hidecompletedData').val('N');
                }
            }

        }
    });
}

function sendMentorMsg() {
    var absPath = $('#absPath').val();
    var mentorId = $("#mentor_id").val();
    var msgbody = $("#msgbody").val();
    var data = { lstUserList: mentorId, msgNewBody: msgbody };
    $.ajax({
        url: absPath + 'messages/sendUserMessages',
        method: "post",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "error") {
                window.location = '/users/meetings/';
                displayAjaxNotificationMessage("Message not sent", "danger");
            } else {

                if (response.message == "block") {
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                    $('.blockdiv label').text('You are blocked by this user.');
                    $("#blockUserMsgDetailPage").modal('toggle');
                } else if (response.message == "blocked") {
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                    $('.blockdiv label').text('Unblock this user to send message.');
                    $("#blockUserMsgDetailPage").modal('toggle');
                } else {
                    displayAjaxNotificationMessage("Message sent successfully", "success");
                    $("#sendMsgMentor").modal('toggle');
                    $("#msgbody").val("");
                }
            }
        }
    });
}

function mentorCancelRequest(data) {

    $.ajax({
        url: absPath + 'users/mentorCancelRequest',
        method: "post",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "error") {
                window.location = '/users/meetings/';
                displayAjaxNotificationMessage("Message not sent", "danger");
            } else {
                console.log(response);
                if (response == "success") {
                    displayAjaxNotificationMessage("Meeting canceled successfully", "success");
                    $("#mentorCancelModal").modal('toggle');
                    getMeetingData(mentor_timezone);
                } else {
                    displayAjaxNotificationMessage("Meeting canceled successfully", "success");
                    $("#mentorCancelModal").modal('toggle');
                    getMeetingData(mentor_timezone);
                }
            }
        }
    });
}

function userCancelRequest(data) {

    $.ajax({
        url: absPath + 'users/userCancelRequest',
        method: "post",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "error") {
                window.location = '/users/meetings/';
                displayAjaxNotificationMessage("Message not sent", "danger");
            } else {
                console.log(response);
                if (response == "success") {
                    displayAjaxNotificationMessage("Meeting canceled successfully", "success");
                    $("#UserCancelModal").modal('toggle');
                    getMeetingData(mentor_timezone);
                } else {
                    displayAjaxNotificationMessage("Meeting canceled successfully", "success");
                    $("#UserCancelModal").modal('toggle');
                    getMeetingData(mentor_timezone);
                }
            }
        }
    });
    $.unblockUI();
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0)
        return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + sizes[i];
}

/*function getCostBreakUp(invitation_id, userType) {
 
 
 var data = {
 invitation_id: invitation_id
 }
 
 $.ajax({
 url: absPath + 'miscRoutes/getCostBreakUp',
 method: "post",
 data: data,
 beforeSend: function () {
 $.blockUI();
 },
 complete: function () {
 $.unblockUI();
 },
 success: function (response) {
 console.log(response);
 if (response.message == "error") {
 
 } else {
 
 if (response.data[0].discount != '0' && response.data[0].discount != '0.00') {
 $("#disdiv").show();
 } else {
 $("#disdiv").hide();
 }
 
 $("#mentorRateSpan").text("");
 $("#wissenxChargesSpan").text("");
 //$("#taxes").text("");
 $(".viewtitle").text("");
 $("#finallabel").text("");
 $("#final").text("");
 $("#discount").text("");
 $("#condilabel").text("");
 $("#conditiondiv").text("");
 
 var refundType = response.data[0].refund_type;
 var promocode_id = response.data[0].promocode_id;
 var promocode_discount = parseFloat(response.data[0].promocode_discount).toFixed(2);
 var mentor_final = parseFloat(response.data[0].final_pay_to_mentor).toFixed(2);
 var user_final = parseFloat(response.data[0].final_user_paid_amount).toFixed(2);
 var final_pay_to_mentor = parseFloat(response.data[0].final_pay_to_mentor).toFixed(2);
 var final_user_paid_amount = parseFloat(response.data[0].final_user_paid_amount).toFixed(2);
 
 if (userType == "Mentor") {
 $(".viewtitle").text("Mentor Earnings");
 $("#finallabel").text("Mentor Earnings");
 } else {
 $(".viewtitle").text("Meeting Cost");
 $("#finallabel").text("Total Charges");
 }
 
 switch (refundType) {
 case "FULLREF" :
 $("#condilabel").text("Full Refund Charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "PARTIALREF" :
 $("#condilabel").text("Partial Refund Charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "PVTINTO" :
 $("#condilabel").text("Private Invite Time Out Charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "NOSHOWBOTH" :
 $("#condilabel").text("NO Show by Both Parties Charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "NOSHOWMENT" :
 $("#condilabel").text("No Show by Mentor Charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "NOSHOWUSR" :
 $("#condilabel").text("No Show by User Charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "PNTINVDECUSR" :
 $("#condilabel").text("Private invite decline by user charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "PNTINVCANMENT" :
 $("#condilabel").text("Private invite cancell by mentor charges");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "NICANBYUSR" :
 $("#condilabel").text("Normal Invite cancell by user charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "NIDECBYMEN" :
 $("#condilabel").text("Normal Invite decline by mentor charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "ACCMTCANMENT" :
 $("#condilabel").text("Accepted Meeting Cancel by mentor Charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 case "ACCMTCANUSR" :
 $("#condilabel").text("Accepted Meeting Cancell by user Charge");
 if (userType == "Mentor") {
 $("#conditiondiv").text(final_pay_to_mentor);
 } else {
 $("#conditiondiv").text(final_user_paid_amount);
 }
 $("#condiv").show();
 $("#normalPay").hide();
 break;
 default :
 case "PAYNOR" :
 if (promocode_id > '0') {
 $("#disdiv").show();
 } else {
 $("#disdiv").hide();
 }
 
 $("#mentorRateSpan").text(parseFloat(response.data[0].mentor_charge).toFixed(2));
 $("#discount").text(parseFloat(promocode_discount).toFixed(2));
 if (userType == "Mentor") {
 $("#wissenxChargesSpan").text(parseFloat(response.data[0].wissenx_mentor_charge).toFixed(2));
 //$("#taxes").text("0.00");
 $(".viewtitle").text("Mentor Earnings");
 $("#finallabel").text("Mentor Earnings");
 $("#final").text(mentor_final);
 } else {
 $("#wissenxChargesSpan").text(parseFloat(response.data[0].wissenx_user_charge).toFixed(2));
 //$("#taxes").text("0.00");
 $(".viewtitle").text("Meeting Cost");
 $("#finallabel").text("Total Charges");
 $("#final").text(user_final);
 }
 $("#condiv").hide();
 $("#normalPay").show();
 break;
 }
 
 $("#costBreakUp").modal('toggle');
 }
 }
 });
 }*/



function getCostBreakUp(invitation_id, userType) {

    $('#normalPay').html("");
    var data = {
        invitation_id: invitation_id
    };
    var arrData = {};
    arrData.list = [];
    arrData.total = [];
    $.ajax({
        url: absPath + 'miscRoutes/getCostBreakUp',
        method: "post",
        data: data,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            if (response.message == "error") {

            } else {

                if (response.data[0].discount != '0' && response.data[0].discount != '0.00') {
                    $("#disdiv").show();
                } else {
                    $("#disdiv").hide();
                }

                $("#mentorRateSpan").text("");
                $("#wissenxChargesSpan").text("");
                //$("#taxes").text("");
                $(".viewtitle").text("");
                $("#finallabel").text("");
                $("#final").text("");
                $("#discount").text("");
                $("#condilabel").text("");
                $("#conditiondiv").text("");
                var refundType = response.data[0].refund_type;
                var promocode_id = response.data[0].promocode_id;
                var promocode_discount = parseFloat(response.data[0].promocode_discount).toFixed(2);
                var mentor_final = parseFloat(response.data[0].final_pay_to_mentor).toFixed(2);
                var user_final = parseFloat(response.data[0].final_user_paid_amount).toFixed(2);
                var final_pay_to_mentor = parseFloat(response.data[0].final_pay_to_mentor).toFixed(2);
                var pay_to_mentor = parseFloat(response.data[0].pay_to_mentor).toFixed(2);
                var final_user_paid_amount = parseFloat(response.data[0].final_user_paid_amount).toFixed(2);
                var total_payable_amount = parseFloat(response.data[0].total_payable_amount).toFixed(2);
                var refund_paid_to_user = parseFloat(response.data[0].refund_paid_to_user).toFixed(2);
                var final_pay_to_wissenx = parseFloat(response.data[0].final_pay_to_wissenx).toFixed(2);
                if (userType == "Mentor") {
                    $(".viewtitle").text("Mentor Earnings");
                    $("#finallabel").text("Mentor Earnings");
                } else {
                    $(".viewtitle").text("Meeting Cost");
                    $("#finallabel").text("Total Charges");
                }

                switch (refundType) {
                    case "FULLREF":
                        //                        $("#condilabel").text("Full Refund Charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            //                            arrData.list = [
                            //                                {label: 'Full Refund Charge', value: final_pay_to_mentor}
                            //                            ];
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: pay_to_mentor },
                                { label: "Mentor's Refund Share", value: pay_to_mentor }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_pay_to_mentor }
                            ];
                        } else {
                            var tot = parseFloat(final_user_paid_amount) - parseFloat(refund_paid_to_user);
                            tot = tot.toFixed(2);
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: final_user_paid_amount },
                                { label: 'Refund Approved by SourceAdvisor', value: refund_paid_to_user }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: tot }
                            ];
                        }

                        break;
                    case "PARTIALREF":
                        //                        $("#condilabel").text("Partial Refund Charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            var mentorshare = parseFloat(pay_to_mentor) - parseFloat(final_pay_to_mentor);
                            arrData.list = [
                                { label: 'Original Meeting Earnings', value: pay_to_mentor },
                                //{label: 'Refund Accepted', value: refund_paid_to_user}
                                { label: "Mentor's Refund share", value: mentorshare.toFixed(2) }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Earnings', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'Refund Approved by SourceAdvisor', value: refund_paid_to_user }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: total_payable_amount - refund_paid_to_user }
                            ];
                        }

                        break;
                    case "PVTINTO":
                        //                        $("#condilabel").text("Private Invite Time Out Charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            arrData.list = [
                                { label: 'Private Invite Time Out Charges', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Private Invite Time Out Charges', value: final_user_paid_amount }
                            ];
                        }

                        break;
                    case "NOSHOWBOTH":
                        //                        $("#condilabel").text("NO Show by Both Parties Charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();


                        if (userType == "Mentor") {
                            arrData.list = [
                                { label: 'Original Mentor Earnings', value: pay_to_mentor },
                                { label: 'NO Show by Both Parties(Refund)', value: pay_to_mentor }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'NO Show by Both Parties(Refund)', value: refund_paid_to_user }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "NOSHOWMENT":
                        //                        $("#condilabel").text("No Show by Mentor Charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {

                            var finalpay = parseFloat(total_payable_amount) + parseFloat(final_pay_to_mentor);
                            finalpay = finalpay.toFixed(2);
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: pay_to_mentor },
                                { label: 'Reund to User due to Mentor No Show', value: pay_to_mentor },
                                { label: 'No Show by Mento Charges', value: final_pay_to_wissenx }
                            ];
                            arrData.total = [
                                { label: 'Net Earning', value: final_pay_to_mentor }
                            ];
                        } else {
                            var finalpay = parseFloat(total_payable_amount) - parseFloat(final_user_paid_amount);
                            finalpay = finalpay.toFixed(2);
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'No Show by Mentor Refund', value: finalpay }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_user_paid_amount }
                            ];
                        }

                        break;
                    case "NOSHOWUSR":
                        //                        $("#condilabel").text("No Show by User Charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            var finalpay = parseFloat(pay_to_mentor) - parseFloat(final_pay_to_mentor);
                            finalpay = finalpay.toFixed(2);
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: pay_to_mentor },
                                { label: 'User Refund due to User No Show', value: finalpay }
                            ];
                            arrData.total = [
                                { label: 'Net Earning', value: final_pay_to_mentor }
                            ];
                        } else {
                            var finalpay = parseFloat(total_payable_amount) - parseFloat(final_user_paid_amount);
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'No Show by You Refund', value: finalpay }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "PNTINVDECUSR":
                        //                        $("#condilabel").text("Private invite decline by user charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();
                        if (userType == "Mentor") {
                            arrData.list = [
                                { label: 'Private invite decline by user charges', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Private invite decline by user charges', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "PNTINVCANMENT":
                        //                        $("#condilabel").text("Private invite cancell by mentor charges");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            arrData.list = [
                                { label: 'Private invite cancell by mentor charges', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Private invite cancell by mentor charges', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "NICANBYUSR":
                        //                        $("#condilabel").text("Normal Invite cancel by user charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {

                            arrData.list = [
                                { label: 'Original Meeting Cost', value: pay_to_mentor },
                                { label: 'User Refund due to Cancellation', value: pay_to_mentor }
                            ];
                            arrData.total = [
                                { label: 'Net Earning', value: final_pay_to_mentor }
                            ];


                            //                            arrData.list = [
                            //                                {label: 'Normal Invite cancel by user charge', value: final_pay_to_mentor}
                            //                            ];
                        } else {
                            arrData.list = [
                                { label: 'Normal Invite cancel by user charge', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "NIDECBYMEN":

                        //                        $("#condilabel").text("Normal Invite decline by mentor charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();


                        if (userType == "Mentor") {
                            arrData.list = [
                                { label: 'Meeting Request Declined', value: final_pay_to_mentor }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Meeting Request Declined', value: final_user_paid_amount }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_user_paid_amount }
                            ];
                        }
                        break;
                    case "ACCMTCANMENT":

                        //                        $("#condilabel").text("Accepted Meeting Cancel by mentor Charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            arrData.list = [
                                //{label: 'Original Meeting Cost', value: total_payable_amount},
                                //{label: 'User Refund due to Cancellation', value: refund_paid_to_user},
                                { label: 'Original Meeting Cost', value: pay_to_mentor },
                                { label: 'User Refund due to Cancellation', value: pay_to_mentor },
                                { label: 'Mentor Cancellation charge', value: final_pay_to_wissenx }
                            ];
                            arrData.total = [
                                { label: 'Net Earning', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'Refund', value: refund_paid_to_user }
                            ];
                            arrData.total = [
                                { label: 'Net Meeting Cost', value: final_user_paid_amount }
                            ];
                        }


                        break;
                    case "ACCMTCANUSR":
                        //                        $("#condilabel").text("Accepted Meeting Cancell by user Charge");
                        //                        if (userType == "Mentor") {
                        //                            $("#conditiondiv").text(final_pay_to_mentor);
                        //                        } else {
                        //                            $("#conditiondiv").text(final_user_paid_amount);
                        //                        }
                        //                        $("#condiv").show();
                        //                        $("#normalPay").hide();

                        if (userType == "Mentor") {
                            var amt = parseFloat(pay_to_mentor) - parseFloat(final_pay_to_mentor);
                            amt = amt.toFixed(2);
                            arrData.list = [
                                { label: 'Original Mentor Earnings', value: pay_to_mentor },
                                { label: 'User Refund due to Cancellation', value: amt }
                            ];
                            arrData.total = [
                                { label: 'Net Earning', value: final_pay_to_mentor }
                            ];
                        } else {
                            arrData.list = [
                                { label: 'Original Meeting Cost', value: total_payable_amount },
                                { label: 'Cancellation Charges', value: final_user_paid_amount }
                            ];
                            arrData.total = [
                                { label: 'Refund Due', value: refund_paid_to_user }
                            ];
                        }
                        break;
                    default:
                    case "PAYNOR":
                        //                        if (promocode_id > '0') {
                        //                            $("#disdiv").show();
                        //                        } else {
                        //                            $("#disdiv").hide();
                        //                        }
                        //
                        //                        $("#mentorRateSpan").text(parseFloat(response.data[0].mentor_charge).toFixed(2));
                        //                        $("#discount").text(parseFloat(promocode_discount).toFixed(2));
                        //
                        //
                        //
                        //                        if (userType == "Mentor") {
                        //                            $("#wissenxChargesSpan").text(parseFloat(response.data[0].wissenx_mentor_charge).toFixed(2));
                        //                            //$("#taxes").text("0.00");
                        //                            $(".viewtitle").text("Mentor Earnings");
                        //                            $("#finallabel").text("Mentor Earnings");
                        //                            $("#final").text(mentor_final);
                        //                        } else {
                        //                            $("#wissenxChargesSpan").text(parseFloat(response.data[0].wissenx_user_charge).toFixed(2));
                        //                            //$("#taxes").text("0.00");
                        //                            $(".viewtitle").text("Meeting Cost");
                        //                            $("#finallabel").text("Total Charges");
                        //                            $("#final").text(user_final);
                        //                        }

                        if (userType == "Mentor") {
                            var tmp = [];
                            tmp = [
                                { label: 'Mentor Charges', value: (response.data[0].mentor_charge + response.data[0].promocode_discount).toFixed(2) }
                            ];
                            if (promocode_id > '0') {
                                tmp.push({ label: 'Discount', value: (response.data[0].promocode_discount).toFixed(2) });
                            }
                            tmp.push({ label: 'SourceAdvisor Charges ', value: (response.data[0].wissenx_mentor_charge).toFixed(2) });
                            arrData.list = tmp;

                            arrData.total = [
                                { label: 'Mentor Earnings', value: mentor_final }
                            ];
                        } else {
                            var tmp = [];
                            tmp = [
                                { label: 'Mentor Charges', value: (response.data[0].mentor_charge + response.data[0].promocode_discount).toFixed(2) }
                            ];
                            if (promocode_id > '0') {
                                tmp.push({ label: 'Discount', value: (response.data[0].promocode_discount).toFixed(2) });
                            }
                            tmp.push({ label: 'SourceAdvisor Charges ', value: (response.data[0].wissenx_user_charge).toFixed(2) });
                            arrData.list = tmp;
                            arrData.total = [
                                { label: 'Total Charges', value: user_final }
                            ];
                        }
                        //                        $("#condiv").hide();
                        //                        $("#normalPay").show();
                        break;
                }

                var strHtml = '';
                $.each(arrData.list, function (index, objElement) {
                    strHtml += '<div class="col-sm-12">' +
                        '<div class="col-sm-6">' +
                        '<label>' + objElement.label + '</label>' +
                        '</div>' +
                        '<div class="col-sm-2">' +
                        '<span class="pull-right">' + objElement.value + '</span>' +
                        '</div>' +
                        '<div class="col-sm-4">USD</div>' +
                        '</div>';
                });
                $.each(arrData.total, function (index, objElement) {
                    strHtml += '<div class="col-sm-12">' +
                        '<div class="col-sm-6">' +
                        '<label>' + objElement.label + '</label>' +
                        '</div>' +
                        '<div class="col-sm-2" style="border-top: 1px solid">' +
                        '<span class="pull-right">' + objElement.value + '</span>' +
                        '</div>' +
                        '<div class="col-sm-4">USD</div>' +
                        '</div>';
                });
                $('#normalPay').append(strHtml);
                $("#costBreakUp").modal('toggle');
            }
        }
    });
}