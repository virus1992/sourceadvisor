var cameraCaptureImage = "";
var oldImageFromDb = "";
var cropper = '';
var absPath = "";
var citySearchUrl = '';
var userType='';
var userInitialInterval = setInterval(function () {
    if ($('.photoContainer.mentor_dp').length > 0) {
        clearInterval(userInitialInterval);
        setUserInitialImage({
            type: "input",
            section: "userHeader",
            loopClass: "photoContainer.mentor_dp",
            srcClass: "imgUserProfile",
            nameClass: "hfname",
            nameHtmlClass: "imgdiv",
            imgClass: "imgUserProfile"
        });
    }
}, 1);
$(document).ready(function () {
    userType=$('#userType').val();
    $(".becomeAMentorBtn").on("click", function () {
        chkbeamentor();
    });

    absPath = $('#absPath').val();
    $('#txtCityName').autocomplete({
        serviceUrl: absPath + 'users/getCityList/',
        minChars: 4,
        onSearchStart: function (params) {
            params.country_id = $('#lstCountry option:selected').val();
        },
        onSelect: function (suggestion) {
            console.log(suggestion);
            $('#hidCityId').val(suggestion.cityId);
        }
    });
    oldImageFromDb = $('#imgUserProfile').attr('src');
    $("#flUserProfileImage").change(function () {
        return readURL(this);
    });
    $('#txtDateOfBirth').datepicker({
        endDate: '-18y',
        autoclose: true,
        disableEntry: true,
        default: true,
        format: "dd/mm/yyyy"
    });
    var lstCountrySelect = $("#lstCountry").select2();
    lstCountrySelect.on("change", function (e) {
        var objCountry = $(this);
        var countryCode = $('#lstCountry option:selected').attr('data-country-code').toLowerCase();
        $("#txtMobileNumber").intlTelInput("setCountry", countryCode);
        $('#hidCityId').val("");
        $('#txtCityName').val("");
        checkForCityInsideCountry(objCountry.val());
    });
    lstCountrySelect.on("select2:close", function () {
        $(this).valid();
    });
    $("#lstTimeZone").select2().on("select2:close", function (e) {
        $(this).valid();
    });
    var InitialCountryCode = $('#hidPhoneCounterIso2').val();
    // my code for initial case 
    if (InitialCountryCode == "" || InitialCountryCode === "undefined") {
        /*$.get("http://ipinfo.io", function (response) {
         $('#lstCountry option[data-country-code="' + response.country + '"]').attr('selected', 'selected');
         lstCountrySelect.trigger('change');
         }, "jsonp");*/

        var userIpAddress = $('#userIPAdd').val();
        $.getJSON("//timezoneapi.io/api/" + userIpAddress, function (data) {
            $('#lstCountry option[data-country-code="' + data.data.timezone.country_code.toLowerCase() + '"]').attr('selected', 'selected');
            lstCountrySelect.trigger('change');
        });
    }


    $("#txtMobileNumber").intlTelInput({
        separateDialCode: true,
        //utilsScript: absPath + "js/intcountrydd/js/utils.js",
        utilsScript: "",
        /*geoIpLookup: function (callback) {
         $.get("http://ipinfo.io", function () {}, "jsonp").always(function (resp) {
         var countryCode = (resp && resp.country) ? resp.country : "";
         callback(countryCode);
         });
         },*/
        initialCountry: InitialCountryCode
    });
    $('#txtMobileNumber').on("countrychange", function (e, countryData) {
        var objSelectedCountry = $('#txtMobileNumber').intlTelInput("getSelectedCountryData");
        var selectedCountryIso2 = objSelectedCountry.iso2.toUpperCase();
        $('#lstCountry option[data-country-code="' + selectedCountryIso2 + '"]').attr('selected', 'selected');
        lstCountrySelect.trigger('change');
    });
    $('#btnUpdateUserProfile').on('click', function () {
        $('#frmUserProfile').submit();
    });
    $('#frmUserProfile').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden][name!="hidCityId"]',
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'lstCountry' || element.attr('id') == 'lstCountry') {
                element.parent().append(error);
            } else {
                element.after(error);
            }
        },
        rules: {
            lstCountrytxt: {
                required: true
            },
            txtFirstName: {
                required: true,
                noBlankSpace: true
            },
            txtLastName: {
                required: true,
                noBlankSpace: true
            },
            txtEmail: {
                required: true,
                email: true,
                noBlankSpace: true
            },
            txtMobileNumber: {
                required: true,
                number: true,
                noBlankSpace: true
            },
            txtDateOfBirth: {
                required: true,
                noBlankSpace: true,
                check_date_of_birth: [true, 18]
            },
            txtAddress: {
                required: true,
                noBlankSpace: true
            },
            txtAddress2: {
                required: true,
                noBlankSpace: true
            },
            lstTimeZone: {
                required: true,
                noBlankSpace: true
            },
            taBreif: {
                required: true,
                noBlankSpace: true
            },
            txtCityName: {
                required: true
            }
        },
        messages: {
            lstCountrytxt: {
                required: "Please enter Location"
            },
            txtFirstName: {
                required: "Please enter First Name"
            },
            txtLastName: {
                required: "Please enter Last Name"
            },
            txtEmail: {
                required: "Please enter Email",
                email: "Please enter valid Email"
            },
            txtMobileNumber: {
                required: "Please enter number",
                number: "Please enter numeric value"
            },
            txtDateOfBirth: {
                required: "Please select Date of Birth"
            },
            txtAddress: {
                required: "Please enter Address"
            },
            txtAddress2: {
                required: "Please enter zip Code"
            },
            lstTimeZone: {
                required: "Please select Timezone"
            },
            taBreif: {
                required: "Please enter about yourself"
            },
            txtCityName: {
                required: "Please enter Location"
            }
        },
        submitHandler: function (form) {
            var objSelectedCountry = $('#txtMobileNumber').intlTelInput("getSelectedCountryData");
            $('#txtMobileNumberExt').val('+' + objSelectedCountry.dialCode);
            $('#hidPhoneCounterIso2').val(objSelectedCountry.iso2);
            form.submit();
        }
    });
    $('#txtEmail').on('blur', function () {

        var oldEmail = $('#txtEmailOld').val();
        var email = $(this).val();
        if (oldEmail == email) {
            return false;
        } else {
            var dataToSend = {
                email: email
            };
            $.ajax({
                url: absPath + 'users/checkEmailAvailable',
                method: "POST",
                data: dataToSend,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                        displayAjaxNotificationMessage("Some error occured", "danger");
                    }

                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVEMAIL") {
                            displayAjaxNotificationMessage("Invalid Email.", "danger");
                        } else if (response['flagMsg'] == "DUPEMAIL") {
                            displayAjaxNotificationMessage("Email already exists.", "danger");
                        }
                        $('#hidEmailAvailable').val("N");
                    } else {
                        if (response['flagMsg'] == "EMAILAVL") {
                            displayAjaxNotificationMessage("Email is available.", "success");
                            $('#hidEmailAvailable').val("Y");
                        }
                    }
                },
                error: function (response) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                }
            });
        }
    });
    $('#txtCityName').on('blur', function () {
        if ($(this).val() == "") {
            $('#hidCityId').val("");
        }
    });

   if(datax && userType==='user'){
       $("#lstTimeZone").select2({
           data: datax,
           templateResult: function (d) {
               return $(d.text); },
           templateSelection: function (d) {
               var found = datax.find(function(element) {
                   if(d.selected){
                       return element.id === parseInt(d.id);
                   }else{
                       return element.id === parseInt(d.selectedId);
                   }    
                 });
                 if(found){
                 $('#lstTimeZone').val(found.id)  
               return $(found.text); 
            }else{
            return "";
        }
           },
           
       })
       $('.select2-selection__rendered').removeAttr('title');
   }

});
// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {

    IN.API.Profile("me").fields("first-name", "last-name", "email-address").result(function (profileres) {

        var dataParam = {
            uniqueId: data.id,
            firstName: data.firstName,
            lastName: data.lastName,
            headline: data.headline,
            profileUrl: data.siteStandardProfileRequest.url,
            loginType: 'ln',
            email: profileres.values[0].emailAddress
        };
        userSocialLoginHandle(dataParam);
        IN.User.logout();
    }).error(function (profileErr) {
        alert('error occured.');
    });
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
    alert('error occured.');
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}




function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var profileId = profile.getId();
    var name = profile.getName();
    var imageUrl = profile.getImageUrl();
    var email = profile.getEmail();
    var dataParams = {
        uniqueId: profileId,
        loginType: 'gp',
        name: name,
        imageUrl: imageUrl,
        email: email
    };
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        userSocialLoginHandle(dataParams);
    });
}

function userSocialLoginHandle(dataParams) {

    var absPath = $('#absPath').val();
    $.ajax({
        url: absPath + 'socialLoginCheckUser',
        method: "post",
        data: dataParams,
        success: function (response) {
            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "RDTDASH") {
                    //window.location = '/dashboard';
                }
            }
        }
    });
}



function checkForCityInsideCountry(countryId) {
    var absPath = $('#absPath').val();
    var dataToSend = {
        countryId: countryId
    };
    $.ajax({
        url: absPath + 'miscRoutes/getCityListForCountry',
        method: "GET",
        data: dataToSend,
        beforeSend: function () {
            $.blockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                alert('Some error occured.');
            }

            if (response['error'] == "1") {
                alert('some error occured.');
            } else {
                if (response['flagMsg'] == "FETCH") {
                    if (response['data']['totalCount'] > 0) {
                        $('#txtCityName').val("");
                        $('#hidCityId').val("");
                        $('#txtCityName').attr('disabled', false);
                    } else {
                        $('#txtCityName').val("");
                        $('#hidCityId').val("");
                        $('#txtCityName').attr('disabled', 'disabled');
                    }
                }
            }
            $.unblockUI();
        }
    });
}