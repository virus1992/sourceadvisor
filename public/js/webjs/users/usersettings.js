$(document).ready(function () {

    var absPath = $('#absPath').val();

    $("#changeSlug").on("click", function () {
        $(".settingsFrom").hide();
        $("#frmChangeSlug").show();
    });

    $("#stopBeingMentor").on("click", function () {
        $(".settingsFrom").hide();
        $("#frmMentorProfileSettings").show();
    });

    $("#changePwd").on("click", function () {
        $(".settingsFrom").hide();
        $("#frmChangePassword").show();
    });

    $('#stop_mentorship').change(function () {
        if ($(this).is(':checked')) {
            $('#hide_profile_search').prop('checked', true).prop('readonly', true);
            $('#hide_profile_calendar').prop('checked', true).prop('readonly', true);
        } else {
            $('#hide_profile_search').prop('readonly', false);
            $('#hide_profile_calendar').prop('readonly', false);
        }
    });

    $('.js-SettingCheckBox').on('change', function () {
        var chkObj = $(this);

        var settingId = chkObj.attr('data-settingId');

        var checkBoxChecked = "0";
        if (chkObj.prop('checked')) {
            checkBoxChecked = "1";
        }

        var dataParams = {
            settingId: settingId,
            settingValue: checkBoxChecked
        };
        $.ajax({
            url: absPath + 'users/updateUserSettings',
            method: "post",
            data: dataParams,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {

                if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                }

                if (response['error'] == "1") {
                    if (response['flagMsg'] == "INVEMAIL") {
                        displayAjaxNotificationMessage("Invalid Email.", "danger");
                    }
                } else {
                    if (response['flagMsg'] == "SETTRM" || response['flagMsg'] == "SETTINS") {
                        displayAjaxNotificationMessage("Updated notification settings.", "success");
                    }
                }
            },
            error: function (response) {
                displayAjaxNotificationMessage("Some error occured", "danger");
            }
        });
    });

    $('#frmChangePassword').validate({
        errorClass: "validate_error",
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'lstCountry' || element.attr('id') == 'lstTimeZone') {
                element.parent().append(error);
            } else {
                element.after(error);
            }
            element.after(error);
        },
        rules: {
            txtOldPassword: {
                required: true,
                noBlankSpace: true
            },
            txtNewPassword: {
                required: true,
                minlength: 6,
                noBlankSpace: true
            },
            txtNewConfPassword: {
                required: true,
                equalTo: "#txtNewPassword",
                noBlankSpace: true
            }
        },
        messages: {
            txtOldPassword: {
                required: "Please enter old password"
            },
            txtNewPassword: {
                required: "Please enter new password",
                minlength: "Password must be atleast 6 characters"
            },
            txtNewConfPassword: {
                required: "Please enter confirm password",
                equalTo: "Password and Confirm Password should match"
            }
        },
        submitHandler: function (form) {
            var txtOldPassword = $('#txtOldPassword').val();
            var txtNewPassword = $('#txtNewPassword').val();

            var dataParams = {
                txtOldPassword: txtOldPassword,
                txtNewPassword: txtNewPassword
            };

            $.ajax({
                url: absPath + 'users/updateUserPassword',
                method: "POST",
                data: dataParams,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                        displayAjaxNotificationMessage("Some error occured", "danger");
                    }

                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVOLDPASS") {
                            displayAjaxNotificationMessage("Invalid old password.", "danger");
                        } else if (response['flagMsg'] == "PASSERRMSG") {
                            displayAjaxNotificationMessage("Some error occured while sending notification mail.", "danger");
                        }
                    } else {
                        if (response['flagMsg'] == "PASSUPD") {
                            //displayAjaxNotificationMessage("Password updated.", "success");
                            window.location = absPath + 'users/passwordChangeSuccessFully';
                        }
                    }
                },
                error: function (response) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                }
            });

            form.reset();
            return false;
        }
    });

    $('#frmMentorProfileSettings').validate({
        errorClass: "validate_error",
        errorPlacement: function (error, element) {
            console.log(element);
            element.parent().after(error);
        },
        rules: {
            /*stop_mentorship: {
                require_from_group: [1, '.accountSettingChkbox']
            },
            hide_profile_search: {
                require_from_group: [1, '.accountSettingChkbox']
            },
            hide_profile_calendar: {
                require_from_group: [1, '.accountSettingChkbox']
            }*/
        },
        messages: {},
        submitHandler: function (form) {

            if($('#stop_mentorship').prop('checked')) {
                $.ajax({
                    url: absPath + 'users/checkMentorAnyFutureMeeting',
                    method: "POST",
                    data: {},
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        //$.unblockUI();
                    },
                    success: function (response) {

                        if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                            displayAjaxNotificationMessage("Some error occured", "danger");
                            $.unblockUI();
                        }

                        if (response['error'] == "1") {
                            displayAjaxNotificationMessage("Some error occured while sending notification mail.", "danger");
                            $.unblockUI();
                        } else {
                            if (response['flagMsg'] == "SUCCESS") {

                                var flagMeetingsPresent = response.data;
                                if(!flagMeetingsPresent) {
                                    updateAccountSettings();
                                }
                                else {
                                    displayAjaxNotificationMessage("You still have a few meetings outstanding. Kindly complete or cancel them before you disable your mentoring practice.", "danger");
                                    $.unblockUI();
                                }
                            }
                        }
                    },
                    error: function (response) {
                        displayAjaxNotificationMessage("Some error occured", "danger");
                        $.unblockUI();
                    }
                });
            }
            else {
                updateAccountSettings();
            }
        }
    });

    $('#frmaskPasswordToDeleteAccount').validate({
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            /*if (element.attr('id') == 'chkDisclaimer') {
             element.parent().append(error);
             }
             else if (element.attr('id') == 'lstReason') {
             alert("asdfasdfasdf");
             element.parent().append(error);
             }*/
            element.after(error);
        },
        rules: {
            txtAskDeletePassword: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            txtAskDeletePassword: {
                required: "Please enter password"
            }
        },
        submitHandler: function (form) {
            var txtAskDeletePassword = $('#txtAskDeletePassword').val();

            var dataParams = {
                txtAskDeletePassword: txtAskDeletePassword
            };

            $('#divAskPasswordAccountDelete').css('display', 'none');
            $('#askPasswordAccountDelete').html("");

            $.ajax({
                url: absPath + 'users/validateUserPassword',
                method: "POST",
                data: dataParams,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                        $('#divAskPasswordAccountDelete').css('display', 'block');
                        $('#askPasswordAccountDelete').html("Invalid old password");
                    }

                    if (response['error'] == "1") {
                        if (response['flagMsg'] == "INVOLDPASS") {
                            $('#divAskPasswordAccountDelete').css('display', 'block');
                            $('#askPasswordAccountDelete').html("Invalid old password");
                        }
                    } else {
                        if (response['flagMsg'] == "PASSVERIFY") {
                            $('#askPasswordToDeleteAccount').modal('hide');
                            form.reset();
                            $('#deleteAccount').modal('show');
                            $.unblockUI();
                        }
                    }
                },
                error: function (response) {
                    $('#divAskPasswordAccountDelete').css('display', 'block');
                    $('#askPasswordAccountDelete').html("Invalid old password");
                }
            });
        }
    });

    $('#frmDeleteAccount').validate({
        ignore: 'input[type=hidden]',
        errorPlacement: function (error, element) {
            if (element.attr('id') == 'chkDisclaimer') {
                element.parent().append(error);
            } else if (element.attr('id') == 'lstReason') {
                alert("asdfasdfasdf");
                element.parent().append(error);
            }
        },
        rules: {
            lstReason: {
                required: true,
                noBlankSpace: true
            },
            chkDisclaimer: {
                required: true,
                noBlankSpace: true
            }
        },
        messages: {
            lstReason: {
                required: "Please select reason"
            },
            chkDisclaimer: {
                required: "Please select checkbox"
            }
        },
        submitHandler: function (form) {

            form.submit();
        }
    });

    $('#frmChangeSlug').validate({
        errorClass: "validate_error",
        rules: {
            slug: {
                required: true,
                remote: {
                    url: absPath + 'mentor/checkSlug',
                    type: 'POST',
                    data: {
                        slug: function () {
                            return $("#slug").val();
                        }
                    }
                }
            }
        },
        messages: {
            slug: {
                required: "Please enter nvestlink profile link",
                remote: "nvestlink profile link already exist"
            }
        },
        submitHandler: function (form) {
            var slug = $('#slug').val();

            var dataParams = {
                slug: slug
            };

            $.ajax({
                url: absPath + 'users/updateUserSlug',
                method: "POST",
                data: dataParams,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {

                    if (response['flagMsg'] == "updated") {
                        displayAjaxNotificationMessage("Slug updated.", "success");
                        $('#slug').val(slug);
                    }
                },
                error: function (response) {
                    displayAjaxNotificationMessage("Some error occured", "danger");
                }
            });

            form.reset();
            return false;

        }
    });

    $('#slug').bind('keypress', function (event) {

        var charCode = (event.which) ? event.which : event.keyCode;

        if(charCode == 8) {
            return true;
        }
        else {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
                event.preventDefault();
                return false;
            }
        }
    });
});

function updateAccountSettings() {
    $.ajax({
        url: absPath + 'users/updateAccountSettings',
        method: "POST",
        data: $('#frmMentorProfileSettings').serialize(),
        beforeSend: function () {
            //$.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {

            if (response['data'] === undefined || response['error'] === undefined || response['flagMsg'] === undefined) {
                displayAjaxNotificationMessage("Some error occured", "danger");
            }

            if (response['error'] == "1") {
                displayAjaxNotificationMessage("Some error occured while sending notification mail.", "danger");
            } else {
                if (response['flagMsg'] == "SUCCESS") {
                    if($('#stop_mentorship').prop('checked')) {
                        window.location = absPath + "users/changeUserAccountTypeStatus/M_U";
                    }
                    else {
                        displayAjaxNotificationMessage("Account Settings Updated Successfully", "success");
                    }
                }
            }
        },
        error: function (response) {
            displayAjaxNotificationMessage("Some error occured", "danger");
        }
    });
}