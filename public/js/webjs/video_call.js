var open_call = false;
var userName = '';
var numberofConnections = 0;
var session = '';
var connectionCount = 0;
var timer = 0;
var timePassedForBtn = 0;
var currentServerTimeStampLog = 0;


var token = '';
var isCallEnded = false;
var call_duration = 0;
var meeting_id = 0;
var meetingDetailId = 0;
var meeting_status = '';
var component = '';
var publisherOptionsTemplate = {
    height: "100%",
    width: "100%",
    insertMode: "append",
    name: userName,
    resolution: "1280x720",
    showControls: true,
    style: {
        audioLevelDisplayMode: "on",
        buttonDisplayMode: "on",
        nameDisplayMode: "on",
        videoDisabledDisplayMode: "on"
    }
};

var publisher = '';

/* Timer realted variables START */
var secondsElapsed = 0;
var startTimerAfterTheseSecond = 0;
var timerRunning = 0;
var duration = 0;
var runTimerInterval = 0;
var scheduledStartTime = "";
var scheduledEndTime = "";
var timerIntervalForCheckMeetingEnded = 0;

var timerShowUserOppositeUserNoShow = '';
var startingTimer = 0;
/* Timer realted variables END  */

/* Misc START*/
var otherParticipantDisconnectTimeOut = 0;
/* Misc END */

var pageLeaveTimer = "";

$(document).ready(function () {
    $('#noclick').css('pointerEvents', 'none');
    getTimerInfo();
    resize();
    //checkOppositeUserConnectedToMeeting();
    token = htmlDecode(token);

    $('.nav.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        resize();
    });

    $(".starrr").starrr({
        rating: 1
    });

    /* If user closes the browser, need to update users login logout log */
    window.onbeforeunload = function (event) {
        //makeRequestToServerForMeetingEndForUser();
        $.ajax({
            url: $('#absPath').val() + "meetinglog/updateUserLeavingMeetingWindow",
            type: 'POST',
            async: false,
            data: {
                meeting_key: $('#meeting_key').val(),
                participantMeetingLogMasterId: $('#participantMeetingLogMasterId').val(),
                participantMeetingLogId: $('#participantMeetingLogId').val(),
                disconnect_reason: 'page_close'
            },
            success: function (response) {

                if (response.flagMsg == 'USRNLOGIN') {
                    window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
                }
                var message = 'Are you sure want to leave meeting.';
                if (typeof event == 'undefined') {
                    event = window.event;
                }
                if (event) {
                    event.returnValue = message;
                }

                // function call to make if user choose to stay on page
                pageLeaveTimer = setTimeout(function () {
                    makeRequestToServerThatUserChooseToStayForMeeting('page_leave');
                }, 7000);

                return message;
            }
        });
    };
    //$('.bottomButtons').fadeIn('slow');
    /** All function reated to meeting audio video turn on/off START */
    $('.meetDetailVideo').mouseenter(function () {
        $('.bottomButtons').fadeIn('slow');
    })
        .mouseleave(function () {
            $('.bottomButtons').fadeOut('slow');
        });

    $('#btnMicrophoneMute').on('click', function () {
        var objItag = $(this).find('.innerIcon');

        // need to turn off audio.
        if (objItag.hasClass('fa-microphone')) {
            objItag.removeClass('fa-microphone').addClass('fa-microphone-slash');
            $(this).addClass('videoCallSwitchButtonDisabled');
            publisher.publishAudio(false);
        }
        // need to turn on audio.
        else {
            objItag.removeClass('fa-microphone-slash').addClass('fa-microphone');
            $(this).removeClass('videoCallSwitchButtonDisabled');
            publisher.publishAudio(true);
        }
    });

    $('#btnVideoTurnOnOff').on('click', function () {
        var objVideoTurnOn = $(this).find('.videoTurnOn');
        var objVideoTurnOff = $(this).find('.videoTurnOff');

        // video is off, and need to turn on the video
        if (objVideoTurnOn.hasClass('hidden')) {
            $(this).removeClass('videoCallSwitchButtonDisabled');
            objVideoTurnOn.removeClass('hidden');
            objVideoTurnOff.addClass('hidden');

            publisher.publishVideo(true);

        }
        // video is on, need to turn off the video.
        else {
            $(this).addClass('videoCallSwitchButtonDisabled');
            objVideoTurnOff.removeClass('hidden');
            objVideoTurnOn.addClass('hidden');

            publisher.publishVideo(false);
        }

    });
    /** All function reated to meeting audio video turn on/off END ` */


    $.ajax({
        url: $('#absPath').val() + "mentor/getUserDetail",
        type: 'POST',
        data: {
            meeting_key: $('#meeting_key').val()
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            }
            open_call = true;
            userName = response.data.userData.firstName + ' ' + response.data.userData.lastName;
            timer = response.data.meetingInfo.elapsed_time;
            call_duration = response.data.meetingInfo.duration;
            meeting_id = response.data.meetingInfo.meeting_id;
            meeting_status = response.data.meetingInfo.status;
            meetingDetailId = response.data.meetingInfo.meetingDetailId;

            //meetingDetailId
            //renderTimer();
        }
    });

    fetchUpdateLogInfo();

    var logFetchInterval = setInterval(function () {
        fetchUpdateLogInfo();
    }, 5000);

    var initializeVideoCall = setInterval(function () {
        if (open_call) {
            clearInterval(initializeVideoCall);
            //initVideoCall();
        }
    }, 500);


    $("#feedback").on('keyup', function () {
        var feedback = $(this).val();

        if (feedback != "") {
            $("#submitreview").attr("disabled", false);
        } else {
            $("#submitreview").attr("disabled", true);
        }

    });

    $('.starrr').on('starrr:change', function (e, value) {
        setAvg();
    });

    $("#submitreview").on("click", function () {

        $('#stars_overAll').html('');

        var objRating = $('.starRating');
        var divider = 0;
        var totalOptions = 0;
        var starRatingData = [];
        starRatingData[0] = 0;

        if (objRating.length > 0) {
            totalOptions = objRating.length;
            $.each(objRating, function (index, element) {
                var thisRating = $(element);
                var indexing = thisRating.attr('data-index');
                var starElement = thisRating.find('.fa-star');
                var thisRatingValue = parseInt(starElement.length);

                starRatingData[indexing] = thisRatingValue;
                if (thisRatingValue > 0) {
                    divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
                }
            });
        }

        var avgRating = parseInt(divider) / parseInt(totalOptions);
        avgRating = Math.round(avgRating);

        overallhtml = setOverallRatingHtml(avgRating);

        $('#stars_overAll').html(overallhtml);

        var feedback = $("#feedback").val();
        var loggedInUser = $('#loggedInUser').val();
        var opposite = $("#oppsiteUser").val();
        var userType = $("#userType").val();

        var data = {
            user_id: opposite,
            reviewer_id: loggedInUser,
            starRatingData: starRatingData,
            review_rating: avgRating,
            review_desc: feedback,
            userType: userType,
            meetingDetailId: meetingDetailId
        };

        data = JSON.stringify(data);

        $.ajax({
            url: $('#absPath').val() + "mentor/saveReview",
            type: 'POST',
            data: "data=" + data,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                //$.unblockUI();
            },
            success: function (response) {
                if (response.message == "success") {
                    $('#EndMeet').modal('toggle');
                    window.onbeforeunload = null;
                    window.location.href = $('#absPath').val();
                }
            }
        });
    });

    $('#endMeetingButton').click(function (e) {
        e.preventDefault();
        // $('#EndMeet').modal('show');
        // return false;
        if (confirm("Are you sure you want to End this Meeting? Please note you won't be able to resume this meeting again.")) {
            endMeetingAjax(function () {
                window.onbeforeunload = null;
                // disconnet the session and show review modal
                session.disconnect();
                $('#EndMeet').modal('show');

                // // if user clicks end meeting button, then remove query beforeunload and redirect to home page.
                // window.onbeforeunload = null;
                // window.location.href = $('#absPath').val();
            });
        } else {
            return false;
        }
    });

    $('#btnSystemSettingOpen').on('click', function () {
        systemSettingAndTroubleShooting();
    });

    $('#btnSystemSetting').on('click', function () {

        $('#systemSetting').modal('hide');
        var flagVideoCallInitiated = $('#flagVideoCallInitiated').val();

        publisherOptions = {
            height: "100%",
            width: "100%",
            insertMode: "append",
            name: userName,
            resolution: "1280x720",
            showControls: true,
            style: {
                audioLevelDisplayMode: "on",
                buttonDisplayMode: "on",
                nameDisplayMode: "on",
                videoDisabledDisplayMode: "on"
            },
            audioSource: component.audioSource().deviceId,
            videoSource: component.videoSource().deviceId
        };

        if (flagVideoCallInitiated == "N") {
            initVideoCall();
        } else {
            updateVideoCall();
        }
    });

    systemSettingAndTroubleShooting();


    $('#btnScheduledMeetingEndOk').on('click', function () {
        $('#EndMeetNotification').modal('hide');
        // var finalMeetingEnd = setTimeout(function(){
        //     endMeetingAjax(function(){
        //         window.onbeforeunload = null;
        //         session.disconnect();
        //         $('#EndMeet').modal('show');
        //     });
        // }, (15 * 60 * 1000));
    });

    $('#btnScheduledMeetingEndEndMeeting').on('click', function () {
        $('#EndMeetNotification').modal('hide');
        endMeetingAjax(function () {
            window.onbeforeunload = null;
            session.disconnect();
            $('#EndMeet').modal('show');
        });
    });

    $('#btnFullScreen').on('click', function () {
        toggleFullScreen();
    });


    $('#btnOppositeUserNoShowOK').on('click', function () {
        $('#oppositeUserNoShowModal').modal('hide');
        userLeavePageUpdateLogManually(function () {
            window.onbeforeunload = null;
            window.close();
        });
    });


    $('#btn-chat').on('click', function () {
        sendChatMessage();
    });


    $('#txtChatMessage').keypress(function (e) {
        if (e.which == 13) {
            sendChatMessage();
        }
    });
});

function sendChatMessage() {
    var txtChatMessage = $.trim($('#txtChatMessage').val());
    var chatMasterId = $('#hidChatMasterId').val();
    var meetingDetailId = $('#hidMeetingDetailId').val();

    if (txtChatMessage != "") {
        var dataToSend = {
            message: txtChatMessage,
            dateTimeStamp: moment.tz(userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss'),
            chatMessageFrom: $('#hidLoggedInUserId').val(),
            senderProfileImageURL: $('#hidLoggedInUserProfileImage').val()
        };

        session.signal({ data: dataToSend }, chatSendCallback);
    }
}


function userLeavePageUpdateLogManually(callbackFunction) {
    $.ajax({
        url: $('#absPath').val() + "meetinglog/updateUserLeavingMeetingWindow",
        type: 'POST',
        async: false,
        data: {
            meeting_key: $('#meeting_key').val(),
            participantMeetingLogMasterId: $('#participantMeetingLogMasterId').val(),
            participantMeetingLogId: $('#participantMeetingLogId').val(),
            disconnect_reason: 'page_close'
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else {
                if (typeof callbackFunction == 'function') {
                    callbackFunction();
                }
            }
        }
    });
}

function makeRequestToServerThatUserChooseToStayForMeeting(disconnect_reason) {

    if (typeof disconnect_reason == 'undefined') {
        disconnect_reason = '';
    }

    $.ajax({
        url: $('#absPath').val() + "meetinglog/updateUserNotLeavingMeetingWindow",
        type: 'POST',
        async: true,
        data: {
            meeting_key: $('#meeting_key').val(),
            participantMeetingLogMasterId: $('#participantMeetingLogMasterId').val(),
            participantMeetingLogId: $('#participantMeetingLogId').val(),
            disconnect_reason: disconnect_reason
        },
        success: function (response) {

            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            }
            var message = 'Are you sure want to leave meeting.';
            if (typeof event == 'undefined') {
                event = window.event;
            }
            if (event) {
                event.returnValue = message;
            }

            return message;
        }
    });
}

function toggleFullScreen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) ||
        (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        if ($('#iFullScreen').hasClass('fa-arrows-alt')) {
            $('#iFullScreen').removeClass('fa-arrows-alt');
        }
        $('#iFullScreen').addClass('fa-compress');


        $('.VideoSection').addClass('fullscreenvideo');
        $('.ChatSection').addClass('hidden');

    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
        if ($('#iFullScreen').hasClass('fa-compress')) {
            $('#iFullScreen').removeClass('fa-compress');
        }
        $('#iFullScreen').addClass('fa-arrows-alt');

        $('.VideoSection').removeClass('fullscreenvideo');
        $('.ChatSection').removeClass('hidden');
    }
}

function systemSettingAndTroubleShooting() {
    var element = document.getElementById('systemTroubleContainer');
    $('#systemTroubleContainer').html("");
    var options = {
        insertMode: 'append'
    };

    component = createOpentokHardwareSetupComponent(element, options, function (error) {
        if (error) {
            console.error('Error: ', error);
            element.innerHTML = '<strong>Error getting devices</strong>: '
            error.message;
            return;
        }
        // Add a button to call component.destroy() to close the component.
    });

    $('#systemSetting').modal('show');
}

function endMeetingAjax(callback) {

    $.ajax({
        url: $('#absPath').val() + "mentor/endMeetingSession",
        type: 'POST',
        data: {
            meeting_id: meeting_id,
            elapsed_time: timer,
            meeting_key: $('#meeting_key').val(),
            participantMeetingLogMasterId: $('#participantMeetingLogMasterId').val(),
            participantMeetingLogId: $('#participantMeetingLogId').val(),
            disconnect_reason: 'end_meeting'
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.onbeforeunload = null;
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else if (response.flagMsg == 'DBERROR') {
                displayAjaxNotificationMessage(response.message, 'error');
            } else if (response.flagMsg == 'SUCCESS') {

                if (typeof callback == 'function') {
                    callback();
                } else {
                    // default action for callback
                    window.onbeforeunload = null;
                    // disconnet the session and show review modal
                    session.disconnect();
                    $('#EndMeet').modal('show');


                    // // if user clicks end meeting button, then remove query beforeunload and redirect to home page.
                    // window.onbeforeunload = null;
                    // window.location.href = $('#absPath').val();
                }

                // window.onbeforeunload = null;
                // if(!flagDirect) {
                //     meeting_status = 'ended';
                //     if(!flagGraceMeetingEnd) {
                //         $('#EndMeet').modal('show');
                //     }
                // }
                // else {
                //     window.location.href = $('#absPath').val();                    
                // }
            }
        }
    });
}

function startMeetingAjax() {
    $.ajax({
        url: $('#absPath').val() + "mentor/startMeetingSession",
        type: 'POST',
        data: {
            meeting_id: meeting_id
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else if (response.flagMsg == 'DBERROR') {
                displayAjaxNotificationMessage(response.message, 'error');
            }
        }
    });
}

function setOverallRatingHtml(value) {
    var html = "";
    for (var i = 1; i <= 5; i++) {
        if (i <= value) {
            html += "<span class='fa .fa-star-o fa-star'></span>";
        } else {
            html += "<span class='fa .fa-star-o fa-star-o'></span>";
        }
    }
    return html;
}

function setAvg() {
    var overallhtml = "";

    $('#stars_overAll').html(overallhtml);

    var objRating = $('.starRating');
    var divider = 0;
    var totalOptions = 0;

    if (objRating.length > 0) {
        totalOptions = objRating.length;
        $.each(objRating, function (index, element) {
            var thisRating = $(element);
            var starElement = thisRating.find('.fa-star');
            var thisRatingValue = parseInt(starElement.length);
            if (thisRatingValue > 0) {
                divider = parseInt(parseInt(divider) + parseInt(thisRatingValue));
            }
        });
    }

    var avgRating = parseInt(divider) / parseInt(totalOptions);
    avgRating = Math.round(avgRating);

    overallhtml = setOverallRatingHtml(avgRating);

    $('#stars_overAll').html(overallhtml);
}

function htmlDecode(input) {
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes[0].nodeValue;
}

function resize() {
    var heights = window.innerHeight;
    heights = parseInt(heights);

    $('.videoMeetingSection').css('height', heights + 'px');
    document.getElementById("maxheight").style.height = heights + "px";
    document.getElementById("chatMessages").style.height = (heights - $('.chatTop').height() - $('#Chatting .panel-footer').height() - $('.nav.nav-tabs').height() - 55) + "px";

    //document.getElementById("fileShare").style.height = "100%";

    $(".SharedDocuments").css('height', (heights - $('.chatTop').height() - $('.nav.nav-tabs').height() - $('#uploadfile_section').height()) + 'px');
    $(".meetingLogEntries").css('height', (heights - $('.chatTop').height() - $('.nav.nav-tabs').height() - parseInt(23)) + 'px');
    $(".meetingLogEntries").css('overflow-y', 'scroll');

    $('.VideoSection').css('height', (heights - $('.chatHeader').height() - $('.MeetTopSection').height()) + 'px');
}

window.onresize = function () {
    resize();
};


function updateVideoCall() {

    session.unpublish(publisher);

    publisher = OT.initPublisher('publisher', publisherOptions, function (publishError) {
        session.publish(publisher, function (error) {
            if (error) {
                console.log(error);
            } else {
                console.log('Publishing a stream.');
            }
        });
    });
}


function initVideoCall() {

    $('#flagVideoCallInitiated').val("Y");

    // Initialize an OpenTok Session object
    session = OT.initSession(apiKey, sessionId);
    //OT.setLogLevel(OT.DEBUG);

    /* // Initialize a Publisher, and place it into the element with id="publisher"
     publisher = OT.initPublisher('publisher', publisherOptions);
     // Attach event handlers
     session.on({
     connectionCreated: function (event) {
     connectionCount++;
     if (event.connection.connectionId != session.connection.connectionId) {
     //console.log('Another client connected. ' + connectionCount + ' total.');
     }
     //console.log('Total clients connected. ' + connectionCount);
     if (connectionCount == 2) {
     if (meeting_status != 'started') {
     meeting_status = 'started';
     startMeetingAjax();
     }
     runTimer();
     } else if (connectionCount > 2) {
     alert('Only 2 users are allowed per call session. You will be redirected to dashboard', function () {
     window.location.href = $('#absPath').val();
     });
     }
     },
     connectionDestroyed: function connectionDestroyedHandler(event) {
     connectionCount--;
     //console.log('A client disconnected. ' + connectionCount + ' total.');
     if (connectionCount < 2) {
     saveTimerInDB();
     stopTimer();
     otherParticipantDisconnected();
     }
     //console.log('Total clients connected. ' + connectionCount);
     },
     // This function runs when session.connect() asynchronously completes
     sessionConnected: function (event) {
     // Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
     // clients)
     session.publish(publisher);
     },
     // This function runs when another client publishes a stream (eg. session.publish())
     streamCreated: function (event) {
     // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
     // the element with id="subscribers"
     var subContainer = document.createElement('div');
     subContainer.id = 'stream-' + event.stream.streamId;
     document.getElementById('subscribers').appendChild(subContainer);
     // Subscribe to the stream that caused this event, put it inside the container we just made
     var subscriber = session.subscribe(event.stream, subContainer, {
     height: "100%",
     width: "100%",
     name: userName,
     resolution: "1280x720",
     showControls: true,
     style: {
     audioLevelDisplayMode: "auto",
     buttonDisplayMode: "auto",
     nameDisplayMode: "auto",
     videoDisabledDisplayMode: "auto"
     }
     });
     },
     streamDestroyed: function () {
     numberofConnections = numberofConnections - 1;
     }
     
     });
     
     // Connect to the Session using the 'apiKey' of the application and a 'token' for permission
     session.connect(apiKey, token);*/
    session.connect(token, function (error) {
        // If the connection is successful, initialize a publisher and publish to the session
        //publisherOptions = publisherOptionsTemplate;
        publisherOptions = {
            height: "100%",
            width: "100%",
            insertMode: "append",
            name: userName,
            resolution: "1280x720",
            showControls: true,
            style: {
                audioLevelDisplayMode: "on",
                buttonDisplayMode: "on",
                nameDisplayMode: "on",
                videoDisabledDisplayMode: "on"
            },
            audioSource: component.audioSource().deviceId,
            videoSource: component.videoSource().deviceId,
            
        };
        //put condition here
        // publisherOptions.publishVideo=false;
        // $('.videoTurnOn').hide();
        //put condition here
        if (!error) {
            publisher = OT.initPublisher('publisher', publisherOptions);
            session.publish(publisher);
        } else {
            console.log('There was an error connecting to the session:', error.code, error.message);
        }
    });

    session.on('streamCreated', function (event) {

        // Create a container for a new Subscriber, assign it an id using the streamId, put it inside
        // the element with id="subscribers"
        var subContainer = document.createElement('div');
        subContainer.id = 'stream-' + event.stream.streamId;
        document.getElementById('subscribers').appendChild(subContainer);
        // Subscribe to the stream that caused this event, put it inside the container we just made
        var subscriber = session.subscribe(event.stream, subContainer, {
            height: "100%",
            width: "100%",
            name: $('#hidOppositeUsersName').val(),
            resolution: "1280x720",
            showControls: true,
            style: {
                audioLevelDisplayMode: "on",
                buttonDisplayMode: "on",
                nameDisplayMode: "on",
                videoDisabledDisplayMode: "on"
            }
        }, function (error) {
            console.log("Error while subscribing stream. :: ", error);
        });
    });

    session.on("streamDestroyed", function (event) {
        console.log("Stream " + event.stream.name + " ended. " + event.reason);
        //makeRequestToServerThatUserChooseToStayForMeeting(event.reason);
        otherParticipantDisconnectTimeOut = setTimeout(function () {
            otherParticipantDisconnected();
        }, 5 * 1000);
        //otherParticipantDisconnected();
    })

    session.on('sessionDisconnected', function (event) {
        console.log("sessionDisconnected event :: ", event);
        ///makeRequestToServerThatUserChooseToStayForMeeting('session_disconnect');
        //otherParticipantDisconnected();
    });

    session.on("signal", function (event) {
        var chatData = event.data;
        var message = chatData.message;
        var messageTime = chatData.dateTimeStamp;
        var chatMessageFrom = chatData.chatMessageFrom;
        var senderProfileImageURL = chatData.senderProfileImageURL;
        var strMessage = "";

        var timeToDisplay = moment.tz('UTC').tz(userTimeZone).format('MM/DD/YYYY HH:mm a');

        if (chatMessageFrom == $('#hidLoggedInUserId').val()) {
            strMessage = '<div class="row msg_container base_sent">' +
                '<div class="col-xs-10 col-md-10">' +
                '<div class="messages msg_sent">' +
                '<p>' + message + '</p>' +
                '<time>' + timeToDisplay + '</time>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2 col-xs-2 avatar">' +
                '<img src="' + senderProfileImageURL + '" class=" img-responsive ">' +
                '</div>' +
                '</div>';
        } else {
            strMessage = '<div class="row msg_container base_receive">' +
                '<div class="col-md-2 col-xs-2 avatar">' +
                '<img src="' + senderProfileImageURL + '" class=" img-responsive ">' +
                '</div>' +
                '<div class="col-md-10 col-xs-10">' +
                '<div class="messages msg_receive">' +
                '<p>' + message + '</p>' +
                '<time>' + timeToDisplay + '</time>' +
                '</div>' +
                '</div>' +
                '</div>';
        }

        $('#chatMessages').append(strMessage);
        $('#chatMessages').scrollTop($('#chatMessages')[0].scrollHeight);

    });
}

function chatSendCallback(error) {
    if (error) {
        console.log("signal error (" + error.name + "): " + error.message);
    } else {

        var txtChatMessage = $('#txtChatMessage').val();
        var chatMasterId = $('#hidChatMasterId').val();
        var meetingDetailId = $('#hidMeetingDetailId').val();

        var dataToSend = {
            message: txtChatMessage,
            dateTimeStamp: moment.tz(userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss'),
            chatMasterId: chatMasterId,
            meetingDetailId: meetingDetailId,
            meetingUrlKey: $('#meeting_key').val()
        };

        saveChatMessageToDB(dataToSend);


        console.log("signal sent.");
    }

    $('#txtChatMessage').val('');
}


function saveChatMessageToDB(objDataToSend) {
    console.log(objDataToSend);
    $.ajax({
        url: $('#absPath').val() + "meetinglog/saveChatMessageToDB",
        type: 'POST',
        data: objDataToSend,
        success: function (response) {
            //console.log(response);
            if (response.flagMsg == 'USRNLOGIN') {
                alert("Your session has expired. You will be redirected to login screen");
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else if (response.flagMsg == 'DONE') {

            }
        }
    });
}

function checkMeetingEnded() {
    $.ajax({
        url: $('#absPath').val() + "mentor/checkMeetingSession",
        type: 'POST',
        data: {
            meeting_id: meeting_id
        },
        success: function (response) {
            //console.log(response);
            if (response.flagMsg == 'USRNLOGIN') {
                //alert("Your session has expired. You will be redirected to login screen");
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else if (response.flagMsg == 'CONTINUE') {
                return false;
            } else if (response.flagMsg == 'END') {
                clearInterval(timerIntervalForCheckMeetingEnded);
                stopTimer(runTimerInterval);
                timer = response.data.elapsed_time;
                //renderTimer();
                session.disconnect();
                $('#EndMeet').modal('show');
            }
        }
    });
}



function stopTimer() {
    clearInterval(runTimerInterval);
}

function saveTimerInDB() {
    if (meeting_status != 'ended') {
        $.ajax({
            url: $('#absPath').val() + "mentor/updateVideoSessionElapsedTime",
            type: 'POST',
            data: {
                elapsed_time: timer,
                meeting_key: $('#meeting_key').val()
            },
            success: function (response) {

            }
        });
    } else {
        return false;
    }
}

function str_pad_left(string, pad, length) {
    return (new Array(length + 1).join(pad) + string).slice(-length);
}


function otherParticipantDisconnected() {
    $.ajax({
        url: $('#absPath').val() + "meetinglog/otherParticipantDisconnected",
        type: 'POST',
        data: {
            meeting_key: $('#meeting_key').val()
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            }
        }
    });
}

function fetchUpdateLogInfo() {

    var minuteElapsed = Math.floor(timer / 60);    
    var meetingTime = Math.floor(timePassedForBtn + minuteElapsed);    

    $.ajax({
        url: $('#absPath').val() + "meetinglog/getMeetingLog",
        type: 'GET',
        data: {
            meeting_key: $('#meeting_key').val()
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else {
                var logData = response.data.logData;
                currentServerTimeStampLog = response.data.currentServerTimeStamp;

                if (logData.length > 0) {
                    var strData = "";
                    var meetingLogs = logData;
                    var loggedInUser = $('#loggedInUser').val();
                    var flagOppositeUserEntered = false;
                    var flagOppositeUserFirstEntryCheck = true;

                    $.each(meetingLogs, function (index, objEachLog) {
                        var logClass = "green";
                        var label = "Connected";
                        var firstName = objEachLog.firstName;
                        var logTime = objEachLog.convertedDateX;
                        var participantId = objEachLog.participantId;

                        if (loggedInUser != participantId) {
                            flagOppositeUserEntered = true;

                            //noShowUserPopUpShouldNotGetDisplayed();

                            // $('#hidOtherParticipantConnected').val("Y");
                            // clearTimeout(timerShowUserOppositeUserNoShow);

                            if (flagOppositeUserFirstEntryCheck) {
                                // if opposite user first log entry is disconnect.
                                if (objEachLog.logType != "LOGIN") {
                                    $('#oppositeUserParticipantName').addClass('hidden');
                                    if (meetingTime >= '8') {
                                        removeEntery(objEachLog.participantId, objEachLog.pid);
                                    }
                                } else {
                                    clearTimeout(otherParticipantDisconnectTimeOut);
                                    $('#oppositeUserParticipantName').removeClass('hidden');
                                }

                                flagOppositeUserFirstEntryCheck = false;
                            }

                        }

                        if (objEachLog.logType != "LOGIN") {
                            logClass = "red";
                            label = "Disconnected";
                        }
                        strData += '<div class="logs-chat ' + logClass + '">';
                        strData += '<i class="fa fa-clock-o"></i>' + firstName + ' ' + label + ' @ ' + logTime;
                        strData += '</div>';
                    });

                    $('.meetingLogEntries').html(strData);

                    // if opposite user entered then stop no show popup.
                    if (flagOppositeUserEntered) {
                        noShowUserPopUpShouldNotGetDisplayed();
                        // $('#hidOtherParticipantConnected').val("Y");
                        // clearTimeout(timerShowUserOppositeUserNoShow);
                    }
                    // if still opposite user dosent come then reset timer for opposite user no show.
                    // and end meeting.
                    else {
                        resetTimerForOppositeUserNoShowNotification(currentServerTimeStampLog);
                    }
                }
            }
        }
    });
}

function removeEntery(uid, pid) {
    $.ajax({
        url: $('#absPath').val() + "mentor/removeEntery",
        type: 'POST',
        data: {
            uid: uid,
            pid: pid
        },
        success: function (response) {
            return true;
        }
    });
}

function noShowUserPopUpShouldNotGetDisplayed() {
    $('#hidOtherParticipantConnected').val("Y");
    clearTimeout(timerShowUserOppositeUserNoShow);
}

function resetTimerForOppositeUserNoShowNotification(currentServerTimeStampLog) {

    var scheduledStartTimeUnixTimestamp = moment.tz(scheduledStartTime, 'UTC').format("x");
    var scheduledStartTimeUnixTimestampMilli = parseInt(parseInt(scheduledStartTimeUnixTimestamp) % parseInt(1000));
    var scheduledStartTimeUnixTimestampSeconds = parseInt(parseInt(scheduledStartTimeUnixTimestamp) / parseInt(1000));

    var timeToTriggerNoShow = moment.tz(scheduledStartTime, 'UTC').add(10, 'minutes');
    // var timeToTriggerNoShowTimeStamp = moment(scheduledStartTime).add(10, 'minutes').format('X');
    // var timeToTriggerNoShowTimeStampMilli = parseInt(parseInt(timeToTriggerNoShowTimeStamp) % parseInt(1000));
    // var timeToTriggerNoShowTimeStampSeconds = parseInt(parseInt(timeToTriggerNoShowTimeStamp) / parseInt(1000));


    //var currentTime = moment.tz('UTC').format('YYYY-MM-DD HH:mm:ss');
    //var UTCUnixTimestampValue = moment.tz(currentTime, userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');

    //var UTCUnixTimestampValue = moment.tz('UTC').format('YYYY-MM-DD HH:mm:ss');
    UTCUnixTimestamp = moment.tz(currentServerTimeStampLog, 'UTC');
    // //var UTCUnixTimestampValue = moment.tz(currentTime, userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
    // var UTCUnixTimestampMilli = parseInt(parseInt(UTCUnixTimestamp) % parseInt(1000));
    // var UTCUnixTimestampSeconds = parseInt(parseInt(UTCUnixTimestamp) / parseInt(1000));

    //var secondsRemaining = parseInt(parseInt(timeToTriggerNoShowTimeStamp) - parseInt(UTCUnixTimestamp));
    var secondsRemaining = timeToTriggerNoShow.diff(UTCUnixTimestamp, 'second');


    if (secondsRemaining > 0) {
        if (timerShowUserOppositeUserNoShow) {
            clearTimeout(timerShowUserOppositeUserNoShow);
        }
        timerShowUserOppositeUserNoShow = setTimeout(function () {
            session.disconnect();
            ///clearTimeout(startingTimer);
            clearInterval(runTimerInterval);
            $('#oppositeUserNoShowModal').modal('show');
        }, secondsRemaining * 1000);
    }
}

function getTimerInfo() {
    $.ajax({
        url: $('#absPath').val() + "meetinglog/getMeetingTimerInfo",
        type: 'GET',
        data: {
            meeting_key: $('#meeting_key').val()
        },
        success: function (response) {
            if (response.flagMsg == 'USRNLOGIN') {
                window.location.href = $('#absPath').val() + 'meetinglog/meetingDisconnect/' + $('#participantMeetingLogMasterId').val() + "/" + $('#participantMeetingLogId').val() + "/" + $('#meeting_key').val();
            } else {
                if (typeof response.data.duration != 'undefined') {

                    secondsElapsed = response.data.secondsElapsed;
                    startTimerAfterTheseSecond = response.data.startTimerAfterTheseSecond;
                    timerRunning = response.data.timerRunning;
                    duration = response.data.duration;
                    scheduledStartTime = response.data.scheduledStartTime;
                    scheduledEndTime = response.data.scheduledEndTime;
                    userTimeZone = response.data.userTimeZone;
                    currentServerTimeStamp = response.data.currentServerTimeStamp;

                    var durationInSeconds = parseInt(duration) * 60;
                    duration = durationInSeconds;
                    //scheduledStartTime = moment.tz(scheduledStartTime, userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                    //var scheduledStartTimeUnixTimestamp = moment.tz(scheduledStartTime, 'UTC').tz(userTimeZone).format("x");
                    var scheduledStartTimeUnixTimestamp = moment.tz(scheduledStartTime, 'UTC').format('x');
                    var scheduledStartTimeUnixTimestampMilli = parseInt(parseInt(scheduledStartTimeUnixTimestamp) % parseInt(1000));
                    var scheduledStartTimeUnixTimestampSeconds = parseInt(parseInt(scheduledStartTimeUnixTimestamp) / parseInt(1000));

                    //var scheduledEndTimeUnixTimestamp = moment.tz(scheduledEndTime, 'UTC').tz(userTimeZone).format("x");
                    var scheduledEndTimeUnixTimestamp = moment.tz(scheduledEndTime, 'UTC').format('x');
                    var scheduledEndTimeUnixTimestampMilli = parseInt(parseInt(scheduledEndTimeUnixTimestamp) % parseInt(1000));
                    var scheduledEndTimeUnixTimestampSeconds = parseInt(parseInt(scheduledEndTimeUnixTimestamp) / parseInt(1000));

                    //var currentTime = moment().format('YYYY-MM-DD HH:mm:ss');
                    //var UTCUnixTimestampValue = moment.tz(currentTime, userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                    //var UTCUnixTimestampValue = moment.tz(userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                    var UTCUnixTimestampValue = moment.tz(currentServerTimeStamp, 'UTC').format('YYYY-MM-DD HH:mm:ss');
                    UTCUnixTimestamp = moment.tz(UTCUnixTimestampValue, 'UTC').format('x');
                    //var UTCUnixTimestampValue = moment.tz(currentTime, userTimeZone).tz('UTC').format('YYYY-MM-DD HH:mm:ss');
                    var UTCUnixTimestampMilli = parseInt(parseInt(UTCUnixTimestamp) % parseInt(1000));
                    var UTCUnixTimestampSeconds = parseInt(parseInt(UTCUnixTimestamp) / parseInt(1000));


                    var endMeetingTimeInCurrentUserTimeZone = moment.tz(scheduledEndTime, 'UTC').tz(userTimeZone).format('YYYY-MM-DD HH:mm:ss');

                    var meetingEndTimeIn12HrFormat = moment.tz(endMeetingTimeInCurrentUserTimeZone, userTimeZone).add(15, 'minutes').format('hh:mm A');
                    //moment(endMeetingTimeInCurrentUserTimeZone).format('hh:mm A');

                    $('#meetingEndTime').html(meetingEndTimeIn12HrFormat);

                    // meeting will start in future, so as timer
                    if (scheduledStartTime > UTCUnixTimestampValue) {
                        var timeToStartTimer = parseInt(parseInt(scheduledStartTimeUnixTimestampSeconds) - parseInt(UTCUnixTimestampSeconds));
                        timeToStartTimer = parseInt((parseInt(timeToStartTimer) * parseInt(1000)) + parseInt(UTCUnixTimestampMilli));
                        startingTimer = setTimeout(function () {
                            runTimer();
                        }, timeToStartTimer);
                    }
                    // meetings has been started, timer needs to start
                    else if (UTCUnixTimestampValue > scheduledStartTime && UTCUnixTimestampValue < scheduledEndTime) {
                        var timePassed = parseInt(parseInt(UTCUnixTimestamp) - parseInt(scheduledStartTimeUnixTimestamp));
                        var timePassedMilli = parseInt(parseInt(timePassed) % parseInt(1000));
                        var timePassedSeconds = parseInt(parseInt(timePassed) / parseInt(1000));
                        duration = parseInt(parseInt(duration) - parseInt(timePassedSeconds));
                        var timerStartInterval = parseInt(parseInt(1000) - parseInt(UTCUnixTimestampMilli));
                        //var timerStartInterval = parseInt(parseInt(timePassedMilli) + parseInt(UTCUnixTimestampMilli));
                        timePassedForBtn = parseInt(timePassedSeconds / 60);
                        startingTimer = setTimeout(function () {
                            runTimer();
                        }, timerStartInterval);
                    } else {
                        var gracePeriodTime = moment.tz(scheduledEndTime, 'UTC').add(15, 'minutes');
                        var currentTimeMoment = moment.tz(currentServerTimeStamp, 'UTC');
                        var differenceTillMeetingEndSeconds = gracePeriodTime.diff(currentTimeMoment, 'second');

                        if (differenceTillMeetingEndSeconds > 0) {
                            var endMeetingAfterUserJoinAfterMeetingEndTime = setTimeout(function () {
                                endMeetingAjax(function () {
                                    window.onbeforeunload = null;
                                    session.disconnect();
                                    $('#EndMeet').modal('show');
                                });
                            }, differenceTillMeetingEndSeconds * 1000);
                        }
                    }

                    timerIntervalForCheckMeetingEnded = setInterval(function () {
                        checkMeetingEnded();
                    }, 5000);

                }
            }
        }
    });
}


function runTimer() {
    runTimerInterval = setInterval(function () {
        timer++;
        // var newStartTime = moment.tz(scheduledStartTime, "UTC").tz(userTimeZone);
        // var newEndTime = moment.tz(newStartTime, userTimeZone).add(duration, "second").format("YYYY-MM-DD HH:mm:ss");
        // timer = parseInt(moment.tz(userTimeZone).diff(moment.tz(scheduledStartTime, "UTC").tz(userTimeZone)) / 1000);

        renderTimer();
    }, 1000);
}

function renderTimer() {
    var diffTimer = (duration) - timer;
    if (diffTimer == 0) {
        clearInterval(runTimerInterval);
        showScheduledMeetingTimeEndNotiffication();
    }
    if (diffTimer <= 300) {
        if (diffTimer % 2 == 0) {
            $('#render_timer').css('color', 'red');
        } else {
            $('#render_timer').css('color', 'white');
        }
    }

    var hours = Math.floor(diffTimer / 3600);
    var hoursRemain = Math.floor(diffTimer % 3600);

    var minutes = Math.floor(hoursRemain / 60);
    var minutesRemain = Math.floor(hoursRemain % 60);

    var seconds = minutesRemain;
    $('#hours').text(str_pad_left(hours, '0', 2));
    $('#minutes').text(str_pad_left(minutes, '0', 2));
    $('#seconds').text(str_pad_left(seconds, '0', 2));

    var minuteElapsed = Math.floor(timer / 60);
    var meetingTime = Math.floor(timePassedForBtn + minuteElapsed);

    if (meetingTime >= '9') {
        $("#endMeetingDiv").css("display", "block");
        $("#dashboardDiv").css("display", "none");

        //resetTimerForOppositeUserNoShowNotification(currentServerTimeStampLog);

    } else {
        $("#endMeetingDiv").css("display", "none");
        $("#dashboardDiv").css("display", "block");
    }

}


function showScheduledMeetingTimeEndNotiffication() {
    $('#EndMeetNotification').modal('show');
    var finalMeetingEnd = setTimeout(function () {
        endMeetingAjax(function () {
            window.onbeforeunload = null;
            session.disconnect();
            $('#EndMeet').modal('show');
        });
    }, (15 * 60 * 1000));
}