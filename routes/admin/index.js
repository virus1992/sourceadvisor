var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var expressValidator = require('express-validator');
var stripe = require('stripe')('sk_test_FBjXXumZacoKFirICPewX50C');
var async = require("async");

// model
var userModel = require('./../../model/users_model');
var miscFunction = require('./../../model/misc_model');
var mentorModel = require('./../../model/mentor_model');
var cronModel = require('./../../model/cron_model');
var adminModel = require('./../../model/admin/login');
var miscAdminModel = require('./../../model/admin/misc');
var mailer = require('./../../modules/mailer');



// modules
var frontConstant = require('./../../modules/front_constant');
var constant = require('./../../modules/constants');
var db_constant = require('./../../modules/db_constants');
var sendResponse = require('./../../modules/sendresponse');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
var upload = multer({storage: storage});
var router = express.Router();


router.get('/', function (req, res, callback) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: 'admin/adminlogin_layout',
        frontConstant: frontConstant,
        constant: constant,
        errors: errors,
        messages: messages,
        greyBG: 1
    };

    res.render('admin/adminlogin', renderParams);
});

router.post('/validateadminuser', function (req, res, callback) {
    var reqBody = req.body;

    req.checkBody('txtUserName', 'Please enter user name.').notEmpty();
    req.checkBody('txtPassword', 'Please enter password.').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        req.flash('errors', errors);
        res.redirect('/admin');
    } else {
        //var flagPasswordCheck = bcrypt.compareSync(password, passwordFromDB);
        var userName = req.body.txtUserName;
        var password = req.body.txtPassword;

        var objToPass = {
            userName: userName
        };
        adminModel.checkAdminUserCredentials(req, res, objToPass, function (req, res, objDataReceive) {
            if (typeof objDataReceive.adminUserId != 'undefined') {
                var adminUserId = objDataReceive.adminUserId;
                var adminUserName = objDataReceive.adminUserName;
                var adminPassword = objDataReceive.adminPassword;
                var adminTimezoneId = objDataReceive.adminTimezoneId;
                var adminTimeZoneLabel = objDataReceive.adminTimeZoneLabel;

                var flagPasswordCheck = bcrypt.compareSync(password, adminPassword);
                if (flagPasswordCheck) {
                    var objToSaveAdminSession = {
                        adminUserId: adminUserId,
                        adminUserName: adminUserName,
                        adminTimezoneId: adminTimezoneId,
                        adminTimeZoneLabel: adminTimeZoneLabel
                    }

                    adminModel.saveAdminSession(req, res, objToSaveAdminSession, function (req, res, flagSaved) {
                        if (flagSaved) {
                            res.redirect('/admin/dashboard');
                        } else {
                            errors = ['Some error occured.'];
                            req.flash('errors', errors);
                            res.redirect('/admin');
                        }
                    });
                } else {
                    errors = ['Invalid Password.'];
                    req.flash('errors', errors);
                    res.redirect('/admin');
                }
            } else {
                errors = ['No User Found.'];
                req.flash('errors', errors);
                res.redirect('/admin');
            }
        });
    }
});


router.get('/logout', function (req, res, callback) {
    adminModel.logoutAdminSession(req, res, function (req, res) {
        req.flash('messages', 'Admin logged out successfully.')
        res.redirect('/admin');
    });
});


router.get('/dashboard', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        async.parallel([
            function (adminDataCallback) {
                miscAdminModel.getCurrentMonthMeetings(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getCurrentYearMeetings(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getAllMeetings(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getCurrentMonthEarning(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getCurrentYearEarning(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getAllEarning(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getRecentMentor(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            },
            function (adminDataCallback) {
                miscAdminModel.getRecentUser(req, res, function (req, res, objCountMeeting) {
                    adminDataCallback(null, objCountMeeting);
                });
            }
        ], function (error, resultSet) {

            var getCurrentMonthMeetings = resultSet[0];
            var getCurrentYearMeetings = resultSet[1];
            var getAllMeetings = resultSet[2];
            var getCurrentMonthEarning = resultSet[3];
            var getCurrentYearEarning = resultSet[4];
            var getAllEarning = resultSet[5];
            var getRecentMentor = resultSet[6];
            var getRecentUser = resultSet[7];


            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                getCurrentMonthMeetings: getCurrentMonthMeetings,
                getCurrentYearMeetings: getCurrentYearMeetings,
                getAllMeetings: getAllMeetings,
                getCurrentMonthEarning: getCurrentMonthEarning,
                getCurrentYearEarning: getCurrentYearEarning,
                getAllEarning: getAllEarning,
                getRecentMentor: getRecentMentor,
                getRecentUser: getRecentUser
            };

            res.render('admin/dashboard', renderParams);
        });
    });
});


router.get('/siteusers', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'admin/adminlogin_layout',
            frontConstant: frontConstant,
            constant: constant,
            errors: errors,
            messages: messages
        };

        res.render('admin/siteuserslisting', renderParams);
    });
});

router.get('/countries/:regionId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        var regionId = req.params.regionId;

        async.parallel([
            // function call to get countryList
            function (countryCallback) {
                // miscFunction.getCountryList(req, res, function (req, res, objCountryList) {
                //     countryCallback(null, objCountryList);
                // });
                countryCallback(null, {});
            },
            // function call to get all the region list
            function (countryCallback) {
                miscFunction.getRegionListFromDb(req, res, function (req, res, objMasterRegionList) {
                    countryCallback(null, objMasterRegionList);
                });
            }
        ], function (error, resultSet) {
            var objCountryList = resultSet[0];
            var objMasterRegionList = resultSet[1];

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objCountryList: objCountryList,
                regionId: regionId,
                objMasterRegionList: objMasterRegionList
            };

            res.render('admin/countries', renderParams);
        });

    });
});


router.get('/regions', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        miscAdminModel.getRegionList(req, res, {}, function (req, res, objRegionList) {

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objRegionList: objRegionList
            };
            res.render('admin/regions', renderParams);
        });
    });
});


router.get('/industries', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        miscAdminModel.getIndustriesList(req, res, {}, function (req, res, objIndustryList) {

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objIndustryList: objIndustryList
            };
            res.render('admin/industries', renderParams);
        });
    });
});


router.get('/subindustry/:industryId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var industryId = req.params.industryId;

        async.parallel([
            // function call to get list of all the sub industries
            function (subindustryCallback) {
                miscAdminModel.getIndustriesList(req, res, {}, function (req, res, objIndustryList) {
                    subindustryCallback(null, objIndustryList);
                });
            },
            // functio call to get information od industry of selected sub industry
            function (subindustryCallback) {
                var objToPass = {
                    industryId: industryId
                };
                miscAdminModel.getIndustriesList(req, res, objToPass, function (req, res, objSelectedIndustryInfo) {
                    if (objSelectedIndustryInfo.length > 0) {
                        subindustryCallback(null, objSelectedIndustryInfo[0]);
                    } else {
                        subindustryCallback(null, []);
                    }
                });
            },
            // function call to get list of subindusrties for the selected industri
            function (subindustryCallback) {
                var objDataToSend = {
                    industryId: industryId,
                };
                miscAdminModel.getSubIndustriesDetails(req, res, objDataToSend, function (req, res, objSubIndustriesList) {
                    subindustryCallback(null, objSubIndustriesList);
                });
            }
        ], function (error, resultSet) {
            var objIndustriesList = resultSet[0];
            var objSelectedIndustryInfo = resultSet[1];
            var objSubIndustriesList = resultSet[2];

            if (typeof objSelectedIndustryInfo.industryId != 'undefined') {
                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'admin/adminlogin_layout',
                    frontConstant: frontConstant,
                    constant: constant,
                    errors: errors,
                    messages: messages,
                    objIndustriesList: objIndustriesList,
                    objSelectedIndustryInfo: objSelectedIndustryInfo,
                    objSubIndustriesList: objSubIndustriesList,
                    industryId: industryId
                };
                res.render('admin/subindustry', renderParams);
            } else {
                res.redirect('/admin/industries');
            }
        });
    });
});


router.get('/subindustrieskeywords/:industryId/:subIndustryId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var industryId = req.params.industryId;
        var subIndustryId = req.params.subIndustryId;

        async.parallel([
            // functio call to get information od industry of selected sub industry
            function (subindustryCallback) {
                var objToPass = {
                    industryId: industryId
                };
                miscAdminModel.getIndustriesList(req, res, objToPass, function (req, res, objSelectedIndustryInfo) {
                    if (objSelectedIndustryInfo.length > 0) {
                        subindustryCallback(null, objSelectedIndustryInfo[0]);
                    } else {
                        subindustryCallback(null, []);
                    }
                });
            },
            // function call to get list of subindusrties for the selected industri
            function (subindustryCallback) {
                var objDataToSend = {
                    industryId: industryId,
                };
                miscAdminModel.getSubIndustriesDetails(req, res, objDataToSend, function (req, res, objSubIndustriesList) {
                    subindustryCallback(null, objSubIndustriesList);
                });
            },
            // function call to get sub industry detail
            function (subindustryCallback) {
                var objDataToSend = {
                    subIndustryId: subIndustryId,
                    industryId: industryId,
                };
                miscAdminModel.getSubIndustriesDetails(req, res, objDataToSend, function (req, res, objSubIndustriesList) {
                    if (objSubIndustriesList.length > 0) {
                        subindustryCallback(null, objSubIndustriesList[0]);
                    } else {
                        subindustryCallback(null, []);
                    }
                });
            },
            // function call to get the list of keywods for the selected sub industries
            function (getListOfKeywords) {
                var objDataToPass = {
                    subIndustryId: subIndustryId,
                }
                miscAdminModel.getSubIndustriesKeywords(req, res, objDataToPass, function (req, res, objSubIndustriesKeywords) {
                    getListOfKeywords(null, objSubIndustriesKeywords);
                });
            }
        ], function (error, resultSet) {

            var objSelectedIndustryInfo = resultSet[0];
            var objSubIndustriesList = resultSet[1];
            var objSelectedSubIndustryInfo = resultSet[2];
            var objSubIndustriesKeywords = resultSet[3];

            if (typeof objSelectedIndustryInfo.industryId != 'undefined') {
                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'admin/adminlogin_layout',
                    frontConstant: frontConstant,
                    constant: constant,
                    errors: errors,
                    messages: messages,
                    objSelectedIndustryInfo: objSelectedIndustryInfo,
                    objSubIndustriesList: objSubIndustriesList,
                    objSelectedSubIndustryInfo: objSelectedSubIndustryInfo,
                    objSubIndustriesKeywords: objSubIndustriesKeywords,
                    industryId: industryId,
                    subIndustryId: subIndustryId
                };

                res.render('admin/subindustrieskeywords', renderParams);
            } else {
                res.redirect('/admin/industries');
            }
        });
    });
});


router.get('/domain', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        miscAdminModel.getDomainList(req, res, {}, function (req, res, objDomainList) {

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objDomainList: objDomainList
            };
            res.render('admin/domain', renderParams);
        });
    });
});


router.get('/systemsettings', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        miscAdminModel.getSystemSettingVariables(req, res, {}, function (req, res, objSystemSettingList) {

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objSystemSettingList: objSystemSettingList
            };

            res.render('admin/systemsettings', renderParams);
        });
    });
});

router.get('/privacypolicy', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        async.parallel([
            // fucntion call to get users policy info
            function (policyCallback) {
                var objDataToSend = {
                    policyFor: "user"
                };
                miscAdminModel.getPolicyInfo(req, res, objDataToSend, function (req, res, objPolicyListingInfo) {
                    miscAdminModel.recursivePolicyBreak(req, res, objPolicyListingInfo, {}, function (req, res, objFinalPolicy) {
                        policyCallback(null, objFinalPolicy);
                    });
                });
            },
            // function call to get mentor policy data.
            function (policyCallback) {
                var objDataToSend = {
                    policyFor: "mentor"
                };
                miscAdminModel.getPolicyInfo(req, res, objDataToSend, function (req, res, objPolicyListingInfo) {
                    miscAdminModel.recursivePolicyBreak(req, res, objPolicyListingInfo, {}, function (req, res, objFinalPolicy) {
                        policyCallback(null, objFinalPolicy);
                    });
                });
            }
        ], function (error, resultSet) {

            var objFinalPolicy = resultSet[0];
            var objFinalMentorPolicy = resultSet[1];

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objFinalPolicy: objFinalPolicy,
                objFinalMentorPolicy: objFinalMentorPolicy
            };

            res.render('admin/privacypolicy', renderParams);
        });
    });
});


router.get('/userdetails/:userId', function (req, res, callback) {

    var userId = req.params.userId;

    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        async.parallel([
            function (mentorDetailCallback) {
                mentorModel.getMentorAllData(req, res, userId, function (req, res, arrMentorData) {
                    mentorDetailCallback(null, arrMentorData);
                });
            },
            function (mentorDetailCallback) {
//                miscAdminModel.getUserTransection(req, res, userId, function (req, res, arrMentorTrans) {
//                    mentorDetailCallback(null, arrMentorTrans);
//                });                

                 mentorModel.getTransectionDataAdmin_model(req, res, userId, function (req, res, arrMentorTrans) {
                    mentorDetailCallback(null, arrMentorTrans);
                });
            },
            function (mentorDetailCallback) {
                miscAdminModel.getUserEarning(req, res, userId, "total", function (req, res, arrMentorEarning) {
                    mentorDetailCallback(null, arrMentorEarning);
                });
            },
            function (mentorDetailCallback) {
                miscAdminModel.getUserEarning(req, res, userId, "current", function (req, res, arrCurrentEarning) {
                    mentorDetailCallback(null, arrCurrentEarning);
                });
            }

        ], function (error, resultSet) {
            var arrMentorData = resultSet[0];
            var arrMentorTrans = resultSet[1];
            var arrMentorEarning = resultSet[2];
            var arrCurrentEarning = resultSet[3];


            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                arrMentorData: arrMentorData,
                arrMentorTrans: arrMentorTrans,
                arrMentorEarning: arrMentorEarning,
                arrCurrentEarning: arrCurrentEarning
            };
            res.render('admin/userdetails', renderParams);
        });

    });
});


router.get('/mentordetails/:mentorId', function (req, res, callback) {

    var mentorId = req.params.mentorId;

    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        async.parallel([
            function (mentorDetailCallback) {
                mentorModel.getMentorAllData(req, res, mentorId, function (req, res, arrMentorData) {
                    mentorDetailCallback(null, arrMentorData);
                });
            },
            function (mentorDetailCallback) {
//                miscAdminModel.getMentorTransection(req, res, mentorId, function (req, res, arrMentorTrans) {
//                    mentorDetailCallback(null, arrMentorTrans);
//                });
                
                mentorModel.getTransectionDataAdmin_model(req, res, mentorId, function (req, res, arrMentorTrans) {
                    mentorDetailCallback(null, arrMentorTrans);
                });
                
            },
            function (mentorDetailCallback) {
                miscAdminModel.getMentorEarning(req, res, mentorId, "total", function (req, res, arrMentorEarning) {
                    mentorDetailCallback(null, arrMentorEarning);
                });
            },
            function (mentorDetailCallback) {
                miscAdminModel.getMentorEarning(req, res, mentorId, "current", function (req, res, arrCurrentEarning) {
                    mentorDetailCallback(null, arrCurrentEarning);
                });
            }

        ], function (error, resultSet) {
            var arrMentorData = resultSet[0];
            var arrMentorTrans = resultSet[1];
            var arrMentorEarning = resultSet[2];
            var arrCurrentEarning = resultSet[3];


            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                arrMentorData: arrMentorData,
                arrMentorTrans: arrMentorTrans,
                arrMentorEarning: arrMentorEarning,
                arrCurrentEarning: arrCurrentEarning
            };
            res.render('admin/mentordetails', renderParams);
        });

    });
});


router.get('/refundrequestlist', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        miscAdminModel.getRefundRequestListInfo(req, res, {}, function (req, res, objRefundRequestListInfo) {

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objRefundRequestListInfo: objRefundRequestListInfo
            };
            res.render('admin/refundrequestlist', renderParams);

        });
    });
});


router.get('/refundrequestdetail/:refundRequestId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        var refundRequestId = req.params.refundRequestId;

        var objDataToSend = {
            refundRequestId: refundRequestId
        }
        miscAdminModel.getRefundRequestListInfo(req, res, objDataToSend, function (req, res, objRefundRequest) {

            var objRefundRequest = objRefundRequest[0];

            async.parallel([
                function (getUsersImageCallback) {
                    var objDataToPass = [{
                            profileImage: objRefundRequest.userProfileImage
                        }];
                    userModel.setImagePathOfAllUsers(req, res, objDataToPass, 0, [], function (req, res, objUserProfileImage) {
                        getUsersImageCallback(null, objUserProfileImage);
                    });
                },
                function (getUsersImageCallback) {
                    var objDataToPass = [{
                            profileImage: objRefundRequest.mentorProfileImage
                        }];
                    userModel.setImagePathOfAllUsers(req, res, objDataToPass, 0, [], function (req, res, objMentorProfileImage) {
                        getUsersImageCallback(null, objMentorProfileImage);
                    });
                },
                // function call to get admin messages info
                function (messagesAllInfoCallback) {
                    var objDataToSend = {
                        refundRequestId: refundRequestId
                    };
                    miscAdminModel.getAdminMessagesInfoForReundRequest(req, res, objDataToSend, function (req, res, objDataResponse) {
                        messagesAllInfoCallback(null, objDataResponse);
                    });
                },
                // function call to get admin messages for user
                function (userMessagesCallback) {
                    var objDataToSend = {
                        refundRequestId: refundRequestId,
                        userType: "user"
                    };
                    miscAdminModel.getAdminMessagesForRefundRequest(req, res, objDataToSend, function (req, res, objDataResponse) {
                        userMessagesCallback(null, objDataResponse);
                    });
                },
                // function call to get admin messsages for mentor
                function (AdminMessagesCallBack) {
                    var objDataToSend = {
                        refundRequestId: refundRequestId,
                        userType: "mentor"
                    };
                    miscAdminModel.getAdminMessagesForRefundRequest(req, res, objDataToSend, function (req, res, objDataResponse) {
                        AdminMessagesCallBack(null, objDataResponse);
                    });
                }
            ], function (error, resultSet) {

                var userProfileImage = resultSet[0][0];
                var mentorProfileImage = resultSet[1][0];

                var objAdminMessageRefundRequestInfo = resultSet[2];

                var objAdminUserMessages = resultSet[3];

                var objAdminMentorMessages = resultSet[4];

                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'admin/adminlogin_layout',
                    frontConstant: frontConstant,
                    constant: constant,
                    errors: errors,
                    messages: messages,
                    objRefundRequest: objRefundRequest,
                    userProfileImage: userProfileImage,
                    mentorProfileImage: mentorProfileImage,
                    objAdminMessageRefundRequestInfo: objAdminMessageRefundRequestInfo,
                    objAdminUserMessages: objAdminUserMessages,
                    objAdminMentorMessages: objAdminMentorMessages,
                    objAdminSession: req.session.adminActiveSession
                };

                res.render('admin/refundrequestdetail', renderParams);
            });
        });
    });
});

router.get('/states/:countryId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        var countryId = req.params.countryId;

        async.parallel([
            // function call to get countryList
            function (countryCallback) {
                miscFunction.getCountryList(req, res, "1", function (req, res, objCountryList) {
                    countryCallback(null, objCountryList);
                });
                //countryCallback(null, {});
            },
            // function call to get all the region list
            function (countryCallback) {
//                miscFunction.getRegionListFromDb(req, res, function (req, res, objMasterRegionList) {
//                    countryCallback(null, objMasterRegionList);
//                });
                countryCallback(null, {});
            }
        ], function (error, resultSet) {
            var objCountryList = resultSet[0];
            var objMasterRegionList = resultSet[1];

            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                errors: errors,
                messages: messages,
                objCountryList: objCountryList,
                countryId: countryId,
                objMasterRegionList: objMasterRegionList
            };

            res.render('admin/states', renderParams);
        });

    });
});


router.get('/cities/:stateId/:countryId', function (req, res, callback) {
    var stateId = req.params.stateId;
    var countryId = req.params.countryId;

    var obj = {
        stateId: stateId,
        countryId: countryId
    }

    async.parallel([
        // function to verify url credentials
        function (urlVerifyCallback) {
            var objDataParams = {
                stateId: stateId,
                countryId: countryId
            }
            miscAdminModel.checkStatesUrl(req, res, objDataParams, function (req, res, flagResponse) {
                urlVerifyCallback(null, flagResponse);
            });
        },
        // function call to get countryInformation
        function (cityInfoCallBack) {
            var objDataParams = {
                countryId: countryId,
                paging: true
            }
            miscAdminModel.getCountryListForAdmin(req, res, objDataParams, function (req, res, flagResponse) {
                cityInfoCallBack(null, flagResponse);
            });
        },
        // function call to get state information
        function (stateInfoCallback) {
            var objDataParams = {
                stateid: stateId,
                countryId: countryId,
                paging: true
            }
            miscAdminModel.getStatesListForAdmin(req, res, objDataParams, function (req, res, flagResponse) {
                stateInfoCallback(null, flagResponse);
            });
        }
    ], function (error, arrResultSet) {

        var flagValidUrl = arrResultSet[0];
        var objCountry = arrResultSet[1];
        var objState = arrResultSet[2];

        if (objCountry.length == 0 || objState.length == 0) {
            res.redirect('/admin/dashboard/');
        } else {
            if (objCountry.length > 0) {
                objCountry = objCountry[0];
            }

            if (objState.length > 0) {
                objState = objState[0];
            }


            var renderParams = {
                layout: 'admin/adminlogin_layout',
                frontConstant: frontConstant,
                constant: constant,
                objCountry: objCountry,
                objState: objState,
                stateId: stateId,
                countryId: countryId
            };

            res.render('admin/cities', renderParams);

            //res.send(arrResultSet);
        }
    });

    //res.send(obj);
});

router.get('/reports', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'admin/adminlogin_layout',
            frontConstant: frontConstant,
            constant: constant,
            errors: errors,
            messages: messages
        };

        res.render('admin/reports', renderParams);
    });
});

router.get('/payouts', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'admin/adminlogin_layout',
            frontConstant: frontConstant,
            constant: constant,
            errors: errors,
            messages: messages
        };

        res.render('admin/payout', renderParams);
    });
});

router.get('/payoutDetails/:mentorId', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {

        var mentorId = req.params.mentorId;
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'admin/adminlogin_layout',
            frontConstant: frontConstant,
            constant: constant,
            errors: errors,
            messages: messages,
            mentorId: mentorId
        };

        res.render('admin/payout_details', renderParams);
    });
});

router.get('/broadcasts', function (req, res, callback) {
    adminModel.checkAdminLoggedInWeb(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'admin/adminlogin_layout',
            frontConstant: frontConstant,
            constant: constant,
            errors: errors,
            messages: messages
        };

        res.render('admin/broadcast_msg', renderParams);
    });
});

module.exports = router;