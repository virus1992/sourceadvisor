var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var expressValidator = require('express-validator');
var stripe = require('stripe')('sk_test_FBjXXumZacoKFirICPewX50C');
var async = require("async");

// model
var userModel = require('./../../model/users_model');
var miscFunction = require('./../../model/misc_model');
var mentorModel = require('./../../model/mentor_model');
var cronModel = require('./../../model/cron_model');
var adminModel = require('./../../model/admin/login');
var adminMisc = require('./../../model/admin/misc');
var mailer = require('./../../modules/mailer');



// modules
var frontConstant = require('./../../modules/front_constant');
var constant = require('./../../modules/constants');
var db_constant = require('./../../modules/db_constants');
var sendResponse = require('./../../modules/sendresponse');


var router = express.Router();

router.post('/getCountryList', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;
        var regionId = reqBody.regionId;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    regionId: regionId
                }

                adminMisc.getCountryListForAdmin(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {
                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    regionId: regionId
                }
                adminMisc.getCountryListForAdmin(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });
    });
});


router.post('/getCountryDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var countryId = reqBody.countryId;

        var objToSend = {
            countryId: countryId,
            paging: true
        }
        adminMisc.getCountryListForAdmin(req, res, objToSend, function (req, res, objCountryData) {
            sendResponse.sendJsonResponse(req, res, 200, objCountryData, "0", "FETCH");
        });
    });
});



router.post('/updateWissenxCharges', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            txtWissenxUserCharge: reqBody.txtWissenxUserCharge,
            txtWissenxeMentorCharge: reqBody.txtWissenxeMentorCharge
        };

        adminMisc.updateWissenxCharges(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});


router.post('/deleteCountry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            countryId: reqBody.countryId
        };

        adminMisc.deleteCountry(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});

router.post('/addCountry', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidCountryId = reqBody.hidCountryId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                lstRegion: reqBody.lstRegion,
                txtCountryCode: reqBody.txtCountryCode,
                txtCountryName: reqBody.txtCountryName,
                txtPhoneCode: reqBody.txtPhoneCode,
                txtWissenxMentorChargeAddEdit: reqBody.txtWissenxMentorChargeAddEdit,
                txtWissenxUserChargeAddEdit: reqBody.txtWissenxUserChargeAddEdit,
                minpayout: reqBody.minpayout,
                maxpayout: reqBody.maxpayout,
                maxpayoutperweek: reqBody.maxpayoutperweek
            };

            adminMisc.addCountry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                lstRegion: reqBody.lstRegion,
                txtCountryCode: reqBody.txtCountryCode,
                txtCountryName: reqBody.txtCountryName,
                txtPhoneCode: reqBody.txtPhoneCode,
                txtWissenxMentorChargeAddEdit: reqBody.txtWissenxMentorChargeAddEdit,
                txtWissenxUserChargeAddEdit: reqBody.txtWissenxUserChargeAddEdit,
                minpayout: reqBody.minpayout,
                maxpayout: reqBody.maxpayout,
                maxpayoutperweek: reqBody.maxpayoutperweek,
                countryId: hidCountryId
            };

            adminMisc.updateCountry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});


router.post('/getSiteUsersList', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var filterUserType = reqBody.filterUserType;
        var filterUserStatus = reqBody.filterUserStatus;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserType: filterUserType,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getSiteUsersList(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserType: filterUserType,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getSiteUsersList(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });
    });
});


router.post('/getCityList', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;
        //res.send(reqBody);

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var countryId = reqBody.countryId;
        var stateId = reqBody.stateId;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;


        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    countryId: countryId,
                    stateId: stateId
                }

                adminMisc.getCitiesList(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    countryId: countryId,
                    stateId: stateId
                }

                adminMisc.getCitiesList(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });

    });
});



router.post('/addCity', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidCityId = reqBody.hidCityId;
        var countryId = reqBody.countryId;
        var stateId = reqBody.stateId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtCityName: reqBody.txtCityName,
                countryId: countryId,
                stateId: stateId
            };

            adminMisc.addCity(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtCityName: reqBody.txtCityName,
                cityId: hidCityId,
                countryId: countryId,
                stateId: stateId
            };

            adminMisc.updateCity(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});

router.post('/deleteCity', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            cityId: reqBody.cityId
        };

        adminMisc.deleteCity(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});

router.post('/getCityDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var cityId = reqBody.cityId;

        var objDataToPass = {
            cityId: cityId
        };

        adminMisc.getCityDetailInfo(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn, "0", "FETCH");
        });
    });
});


router.post('/changeStatusForCity', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var cityId = reqBody.cityId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            cityId: cityId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForCity(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "UPDATED");
        });
    });
});



router.post('/addRegion', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidRegionId = reqBody.hidRegionId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtRegionName: reqBody.txtRegionName
            };

            adminMisc.addRegion(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtRegionName: reqBody.txtRegionName,
                regionId: hidRegionId
            };

            adminMisc.updateRegion(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});

router.post('/deleteRegion', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            regionId: reqBody.regionId
        };

        adminMisc.deleteRegion(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});

router.post('/getRegionDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var regionId = reqBody.regionId;

        var objDataToPass = {
            regionId: regionId
        };

        adminMisc.getRegionList(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});


router.post('/changeStatusForRegion', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var regionId = reqBody.regionId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            regionId: regionId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForRegion(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});


router.post('/addIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidIndustryId = reqBody.hidIndustryId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtIndustrynName: reqBody.txtIndustrynName
            };

            adminMisc.addIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtIndustrynName: reqBody.txtIndustrynName,
                industryId: hidIndustryId
            };

            adminMisc.updateIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});


router.post('/getIndustryDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var industryId = reqBody.industryId;

        var objDataToPass = {
            industryId: industryId
        };

        adminMisc.getIndustriesList(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});

router.post('/deleteIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            industryId: reqBody.industryId
        };

        adminMisc.deleteIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});


router.post('/changeStatusForIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var industryId = reqBody.industryId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            industryId: industryId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForIndustry(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});


router.post('/addSubIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidSubIndustryId = reqBody.hidSubIndustryId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtIndustrynName: reqBody.txtIndustrynName,
                taDescription: reqBody.taDescription,
                taKeywords: reqBody.taKeywords,
                industryId: reqBody.hidIndustryId
            };

            adminMisc.addSubIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtIndustrynName: reqBody.txtIndustrynName,
                taDescription: reqBody.taDescription,
                taKeywords: reqBody.taKeywords,
                hidSubIndustryId: hidSubIndustryId,
                industryId: reqBody.hidIndustryId
            };

            adminMisc.updateSubIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});


router.post('/getSubIndustryInfo', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var subIndustryId = reqBody.subIndustryId;
        var industryId = reqBody.industryId;


        var objDataToPass = {
            subIndustryId: subIndustryId,
            industryId: industryId
        };

        adminMisc.getSubIndustriesDetails(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});


router.post('/deleteSubIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            industryId: reqBody.industryId,
            subIndustryId: reqBody.subIndustryId
        };

        adminMisc.deleteSubIndustry(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});


router.post('/changeStatusForSubIndustry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var subIndustryId = reqBody.subIndustryId;
        var statusToChange = reqBody.statusToChange;
        var industryId = reqBody.industryId;

        var objDataToPass = {
            subIndustryId: subIndustryId,
            industryId: industryId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForSubIndustry(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});



router.post('/addSubIndustryKeyword', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var txtSubIndustryKeywordName = reqBody.txtSubIndustryKeywordName;
        var hidActionType = reqBody.hidActionType;
        var subIndustryKeywordId = reqBody.subIndustryKeywordId;
        var hidSubIndustryId = reqBody.hidSubIndustryId;
        var hidIndustryId = reqBody.hidIndustryId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtSubIndustryKeywordName: reqBody.txtSubIndustryKeywordName,
                subIndustryKeywordId: reqBody.subIndustryKeywordId,
                subIndustryId: reqBody.hidSubIndustryId,
                industryId: reqBody.hidIndustryId
            };

            adminMisc.addSubIndustryKeyword(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtSubIndustryKeywordName: reqBody.txtSubIndustryKeywordName,
                subIndustryKeywordId: reqBody.subIndustryKeywordId,
                subIndustryId: reqBody.hidSubIndustryId,
                industryId: reqBody.hidIndustryId
            };

            adminMisc.updateSubIndustryKeyword(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});


router.post('/getSubIndustryKeywordInfo', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var subIndustryKeywordId = reqBody.subIndustryKeywordId;
        var subIndustryId = reqBody.subIndustryId;

        var objDataToPass = {
            subIndustryKeywordId: subIndustryKeywordId,
            subIndustryId: subIndustryId
        };

        adminMisc.getSubIndustriesKeywords(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});


router.post('/deleteSubIndustryKeyword', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            subIndustryKeywordId: reqBody.subIndustryKeywordId
        };

        adminMisc.deleteSubIndustryKeyword(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});


router.post('/changeStatusForSubIndustryKeyword', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var subIndustryId = reqBody.subIndustryId;
        var subIndustryKeywordId = reqBody.subIndustryKeywordId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            subIndustryId: subIndustryId,
            subIndustryKeywordId: subIndustryKeywordId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForSubIndustryKeyword(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});



router.post('/addDomain', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidDomainId = reqBody.hidDomainId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtDomainName: reqBody.txtDomainName
            };

            adminMisc.addDomain(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtDomainName: reqBody.txtDomainName,
                domainId: hidDomainId
            };

            adminMisc.updateDomain(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});

router.post('/getDomainDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var domainId = reqBody.domainId;

        var objDataToPass = {
            domainId: domainId
        };

        adminMisc.getDomainList(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});

router.post('/deleteDomain', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            domainId: reqBody.domainId
        };

        adminMisc.deleteDomain(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
})

router.post('/changeStatusForDomain', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var domainId = reqBody.domainId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            domainId: domainId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForDomain(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});


router.post('/addSystemVariable', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidSystemSettingVariableId = reqBody.hidSystemSettingVariableId;

        var txtCode = reqBody.txtCode;
        var taDescription = reqBody.taDescription;
        var lstValueType = reqBody.lstValueType;
        var txtIntValue = "0";
        var txtFloatValue = "0.0";
        var txtCharValue = "";
        var txtTextValue = "";
        var taEditorValue = "";

        switch (lstValueType) {
            case "1":
                txtIntValue = reqBody.txtIntValue;
                break;
            case "2":
                txtFloatValue = reqBody.txtFloatValue;
                break;
            case "3":
                txtCharValue = reqBody.txtCharValue;
                break;
            case "4":
                txtTextValue = reqBody.txtTextValue;
                break;
            case "5":
                taEditorValue = reqBody.taEditorValue;
                break;
        }

        if (hidActionType == 'add') {
            var objDataToPass = {
                txtCode: txtCode,
                taDescription: taDescription,
                lstValueType: lstValueType,
                txtIntValue: txtIntValue,
                txtFloatValue: txtFloatValue,
                txtCharValue: txtCharValue,
                txtTextValue: txtTextValue,
                taEditorValue: taEditorValue,
            };

            adminMisc.addSystemVariable(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                txtCode: txtCode,
                taDescription: taDescription,
                lstValueType: lstValueType,
                txtIntValue: txtIntValue,
                txtFloatValue: txtFloatValue,
                txtCharValue: txtCharValue,
                txtTextValue: txtTextValue,
                taEditorValue: taEditorValue,
                systemVariableId: hidSystemSettingVariableId
            };

            adminMisc.updateSystemVariable(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});


router.post('/deleteSystemVariable', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            systemSettingVariableId: reqBody.systemSettingVariableId
        };

        adminMisc.deleteSystemVariable(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});

router.post('/getSystemVariableDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var systemSettingVariableId = reqBody.systemSettingVariableId;

        var objDataToPass = {
            systemSettingVariableId: systemSettingVariableId
        };
        adminMisc.getSystemSettingVariables(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "FETCH");
        });
    });
});


router.post('/updateMentorCancellationPolicyForUsers', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        /**
         * EASY POLICY UPDATE START
         */
        var txt_easy_first = reqBody.txt_easy_first;
        var hidCancellationPolicyId_easy_first = reqBody.hidCancellationPolicyId_easy_first;
        var hidCancellationPolicyChargesId_easy_first = reqBody.hidCancellationPolicyChargesId_easy_first;
        var hidCancellationPolicyTypeId_easy_first = reqBody.hidCancellationPolicyTypeId_easy_first;


        var txt_easy_second = reqBody.txt_easy_second;
        var hidCancellationPolicyId_easy_second = reqBody.hidCancellationPolicyId_easy_second;
        var hidCancellationPolicyChargesId_easy_second = reqBody.hidCancellationPolicyChargesId_easy_second;
        var hidCancellationPolicyTypeId_easy_second = reqBody.hidCancellationPolicyTypeId_easy_second;


        var txt_easy_third = reqBody.txt_easy_third;
        var hidCancellationPolicyId_easy_third = reqBody.hidCancellationPolicyId_easy_third;
        var hidCancellationPolicyChargesId_easy_third = reqBody.hidCancellationPolicyChargesId_easy_third;
        var hidCancellationPolicyTypeId_easy_third = reqBody.hidCancellationPolicyTypeId_easy_third;



        /**
         * MODERATE POLICY UPDATE START
         */
        var txt_moderate_first = reqBody.txt_moderate_first;
        var hidCancellationPolicyId_moderate_first = reqBody.hidCancellationPolicyId_moderate_first;
        var hidCancellationPolicyChargesId_moderate_first = reqBody.hidCancellationPolicyChargesId_moderate_first;
        var hidCancellationPolicyTypeId_moderate_first = reqBody.hidCancellationPolicyTypeId_moderate_first;


        var txt_moderate_second = reqBody.txt_moderate_second;
        var hidCancellationPolicyId_moderate_second = reqBody.hidCancellationPolicyId_moderate_second;
        var hidCancellationPolicyChargesId_moderate_second = reqBody.hidCancellationPolicyChargesId_moderate_second;
        var hidCancellationPolicyTypeId_moderate_second = reqBody.hidCancellationPolicyTypeId_moderate_second;


        var txt_moderate_third = reqBody.txt_moderate_third;
        var hidCancellationPolicyId_moderate_third = reqBody.hidCancellationPolicyId_moderate_third;
        var hidCancellationPolicyChargesId_moderate_third = reqBody.hidCancellationPolicyChargesId_moderate_third;
        var hidCancellationPolicyTypeId_moderate_third = reqBody.hidCancellationPolicyTypeId_moderate_third;




        /**
         * STRICT POLICY UPDATE START
         */
        var txt_strict_first = reqBody.txt_strict_first;
        var hidCancellationPolicyId_strict_first = reqBody.hidCancellationPolicyId_strict_first;
        var hidCancellationPolicyChargesId_strict_first = reqBody.hidCancellationPolicyChargesId_strict_first;
        var hidCancellationPolicyTypeId_strict_first = reqBody.hidCancellationPolicyTypeId_strict_first;


        var txt_strict_second = reqBody.txt_strict_second;
        var hidCancellationPolicyId_strict_second = reqBody.hidCancellationPolicyId_strict_second;
        var hidCancellationPolicyChargesId_strict_second = reqBody.hidCancellationPolicyChargesId_strict_second;
        var hidCancellationPolicyTypeId_strict_second = reqBody.hidCancellationPolicyTypeId_strict_second;


        var txt_strict_third = reqBody.txt_strict_third;
        var hidCancellationPolicyId_strict_third = reqBody.hidCancellationPolicyId_strict_third;
        var hidCancellationPolicyChargesId_strict_third = reqBody.hidCancellationPolicyChargesId_strict_third;
        var hidCancellationPolicyTypeId_strict_third = reqBody.hidCancellationPolicyTypeId_strict_third;


        /**
         * POLICY SLOT UPDATE START
         */
        var txt_easy_slot1 = reqBody.txt_easy_slot1;
        var txt_easy_slot2 = reqBody.txt_easy_slot2;
        var txt_moderate_slot1 = reqBody.txt_moderate_slot1;
        var txt_moderate_slot2 = reqBody.txt_moderate_slot2;
        var txt_strict_slot1 = reqBody.txt_strict_slot1;
        var txt_strict_slot2 = reqBody.txt_strict_slot2;


        async.parallel([
            // function call to slot1 updates for policy
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    policyTypeId: hidCancellationPolicyTypeId_strict_first,
                    slotToUpdate: txt_easy_slot1
                };
                adminMisc.updateCancellationPolicyTypeHour(req, res, objDataToSend, function (req, res, flagError) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // function call to slot2 updates for policy
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    policyTypeId: hidCancellationPolicyTypeId_strict_second,
                    slotToUpdate: txt_easy_slot1 + ',' + txt_easy_slot2
                };
                adminMisc.updateCancellationPolicyTypeHour(req, res, objDataToSend, function (req, res, flagError) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // function call to slot2 updates for policy
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    policyTypeId: hidCancellationPolicyTypeId_strict_third,
                    slotToUpdate: txt_easy_slot2
                };
                adminMisc.updateCancellationPolicyTypeHour(req, res, objDataToSend, function (req, res, flagError) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // easy first update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_easy_first,
                    cancellationPolicyId: hidCancellationPolicyId_easy_first,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_easy_first,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_easy_first,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // easy second update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_easy_second,
                    cancellationPolicyId: hidCancellationPolicyId_easy_second,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_easy_second,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_easy_second,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // easy third update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_easy_third,
                    cancellationPolicyId: hidCancellationPolicyId_easy_third,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_easy_third,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_easy_third,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // moderate first update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_moderate_first,
                    cancellationPolicyId: hidCancellationPolicyId_moderate_first,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_moderate_first,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_moderate_first,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // moderate second update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_moderate_second,
                    cancellationPolicyId: hidCancellationPolicyId_moderate_second,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_moderate_second,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_moderate_second,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // moderate third update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_moderate_third,
                    cancellationPolicyId: hidCancellationPolicyId_moderate_third,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_moderate_third,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_moderate_third,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // strict first update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_strict_first,
                    cancellationPolicyId: hidCancellationPolicyId_strict_first,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_strict_first,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_strict_first,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // strict second update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_strict_second,
                    cancellationPolicyId: hidCancellationPolicyId_strict_second,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_strict_second,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_strict_second,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // strict third update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_strict_third,
                    cancellationPolicyId: hidCancellationPolicyId_strict_third,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_strict_third,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_strict_third,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            }
        ], function (error, resultSet) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "UPDATED");
        });
    });
});



router.post('/updateMentorCancellationPolicyForMentor', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var txt_mentor_first = reqBody.txt_mentor_first;
        var hidCancellationPolicyId_mentor_first = reqBody.hidCancellationPolicyId_mentor_first;
        var hidCancellationPolicyChargesId_mentor_first = reqBody.hidCancellationPolicyChargesId_mentor_first;
        var hidCancellationPolicyTypeId_mentor_first = reqBody.hidCancellationPolicyTypeId_mentor_first;

        /**
         * POLICY SLOT UPDATE START
         */
        var txt_mentor_first_hour = reqBody.txt_mentor_first_hour;



        async.parallel([
            // function call to slot1 updates for policy
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    policyTypeId: hidCancellationPolicyTypeId_mentor_first,
                    slotToUpdate: txt_mentor_first_hour
                };
                adminMisc.updateCancellationPolicyTypeHour(req, res, objDataToSend, function (req, res, flagError) {
                    updateMentorPolicyCallback(null, true);
                });
            },
            // easy first update
            function (updateMentorPolicyCallback) {
                var objDataToSend = {
                    cancellationPercetage: txt_mentor_first,
                    cancellationPolicyId: hidCancellationPolicyId_mentor_first,
                    cancellationPolicyChargesId: hidCancellationPolicyChargesId_mentor_first,
                    cancellationPolicyTypeId: hidCancellationPolicyTypeId_mentor_first,
                };

                adminMisc.updateMentorCancellationPolicy(req, res, objDataToSend, function (req, res, flagUpdate) {
                    updateMentorPolicyCallback(null, true);
                });
            }
        ], function (error, resultSet) {
            sendResponse.sendJsonResponse(req, res, 200, {}, "0", "UPDATED");
        });
    });
});


router.post('/declineRefundRequest', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;
        //res.send(reqBody);

        var taRefundDeclineRequest = reqBody.taRefundDeclineRequest;
        var refundRequestId = reqBody.refundRequestId;
        var refundRequestUnqId = reqBody.refundRequestUnqId;
        var meetingDetailId = reqBody.meetingDetailId;
        var invitationId = reqBody.invitationId;
        var meetingUserId = reqBody.meetingUserId;
        var meetingMentorId = reqBody.meetingMentorId;
        var meetingDuration = reqBody.meetingDuration;

        var objDataToSend = {
            taRefundDeclineRequest: taRefundDeclineRequest,
            refundRequestId: refundRequestId,
            refundRequestUnqId: refundRequestUnqId,
            meetingDetailId: meetingDetailId,
            invitationId: invitationId,
            meetingUserId: meetingUserId,
            meetingMentorId: meetingMentorId,
            meetingDuration: meetingDuration
        }

        adminMisc.declineRefundRequest(req, res, objDataToSend, function (req, res, objDataResponse) {
            if (objDataResponse) {
                sendResponse.sendJsonResponse(req, res, 200, {}, "0", "SUCCESS");
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, "0", "FAIL");
            }
        });
    });
});


router.post('/partialRefundForRefundRequest', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var chkRefundType = "PARTIAL";
        var txtAmountToRefund = reqBody.meetingCost;

        if (typeof reqBody.chkRefundType != 'undefined') {
            if (reqBody.chkRefundType == "on") {
                chkRefundType = "FULLREF";
            }
        }

        if (typeof reqBody.txtAmountToRefund != 'undefined') {
            txtAmountToRefund = reqBody.txtAmountToRefund;
        }

        var invitationId = reqBody.invitationId;
        var meetingCost = reqBody.meetingCost;
        var meetingDetailId = reqBody.meetingDetailId;
        var meetingMentorId = reqBody.meetingMentorId;
        var meetingUserId = reqBody.meetingUserId;
        var refundRequestId = reqBody.refundRequestId;
        var refundRequestUnqId = reqBody.refundRequestUnqId;
        var taRefundMessage = reqBody.taRefundMessage;
        var meetingDuration = reqBody.meetingDuration;
        var meeting_url_key = reqBody.meeting_url_key;

        var objDataToSend = {
            chkRefundType: chkRefundType,
            invitationId: invitationId,
            meetingCost: meetingCost,
            meetingDetailId: meetingDetailId,
            meetingMentorId: meetingMentorId,
            meetingUserId: meetingUserId,
            refundRequestId: refundRequestId,
            refundRequestUnqId: refundRequestUnqId,
            taRefundMessage: taRefundMessage,
            txtAmountToRefund: txtAmountToRefund,
            meetingDuration: meetingDuration,
            meeting_url_key: meeting_url_key
        }

        adminMisc.processPartialRefund(req, res, objDataToSend, function (req, res, objDataResponse) {
            if (objDataResponse) {
                sendResponse.sendJsonResponse(req, res, 200, {}, "0", "SUCCESS");
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, "0", "FAIL");
            }
        });
    });
});


router.post('/suspendUser', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.suspendUser(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/continueUser', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.continueUser(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/makeMentorCelebrity', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.makeMentorCelebrity(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/removeMentorCelebrity', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.removeMentorCelebrity(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/hideReviewRating', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.hideReviewRating(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/showReviewRating', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        adminMisc.showReviewRating(req, res, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, flagSuccess, "0", "success");
        });
    });
});

router.post('/sendAdminMessagesToUsers', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var adminRefundMessageMasterId = reqBody.adminRefundMessageMasterId;
        var message = reqBody.message;
        var sendMessageFor = reqBody.sendMessageFor;
        var refundRequestId = reqBody.refundRequestId;
        var refundRequestUnqId = reqBody.refundRequestUnqId;
        var invitationId = reqBody.invitationId;
        var meetingUserId = reqBody.meetingUserId;
        var meetingMentorId = reqBody.meetingMentorId;

        message = message.replace(/\n/g, '<br />');

        var objDataToSend = {
            adminRefundMessageMasterId: adminRefundMessageMasterId,
            message: message,
            sendMessageFor: sendMessageFor,
            refundRequestId: refundRequestId,
            refundRequestUnqId: refundRequestUnqId,
            invitationId: invitationId,
            meetingUserId: meetingUserId,
            meetingMentorId: meetingMentorId
        };

        adminMisc.sendRefundRequestMessageToUsers(req, res, objDataToSend, function (req, res, objDataResponse) {
            sendResponse.sendJsonResponse(req, res, 200, objDataResponse, "0", "SUCCESS");
        });

        //res.send(reqBody);
    });
});

router.post('/changeStatusForCountry', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var countryId = reqBody.countryId;
        var regionId = reqBody.regionId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            countryId: countryId,
            regionId: regionId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForCountry(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});

router.post('/getStatesList', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;
        var countryId = reqBody.countryId;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    countryId: countryId
                }

                adminMisc.getStatesListForAdmin(req, res, objToSend, function (req, res, objStateData) {
                    countryCallback(null, objStateData);
                });
            },
            // function call to get total records
            function (countryCallback) {
                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    countryId: countryId
                }
                adminMisc.getStatesListForAdmin(req, res, objToSend, function (req, res, objStateData) {
                    countryCallback(null, objStateData);
                });
            }
        ], function (error, resultSet) {

            var objStateRecords = resultSet[0];
            var objStateRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objStateRecords,
                "draw": draw,
                "recordsTotal": objStateRecordsCount,
                "recordsFiltered": objStateRecordsCount
            }));
            res.end();
        });
    });
});

router.post('/deleteState', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var objDataToPass = {
            stateid: reqBody.stateid
        };

        adminMisc.deleteState(req, res, objDataToPass, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
        });
    });
});

router.post('/changeStatusForState', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var stateId = reqBody.stateId;
        var statusToChange = reqBody.statusToChange;

        var objDataToPass = {
            stateId: stateId,
            statusToChange: statusToChange
        };

        adminMisc.changeStatusForState(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn[0], "0", "UPDATED");
        });
    });
});

router.post('/addState', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        var hidActionType = reqBody.hidActionType;
        var hidStateId = reqBody.hidStateId;

        if (hidActionType == 'add') {
            var objDataToPass = {
                lstCountry: reqBody.lstCountry,
                txtStateName: reqBody.txtStateName
            };

            adminMisc.addState(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        } else {
            var objDataToPass = {
                lstCountry: reqBody.lstCountry,
                txtStateName: reqBody.txtStateName,
                hidStateId: hidStateId
            };

            adminMisc.updateState(req, res, objDataToPass, function (req, res, flagSuccess) {
                sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "UPDATED");
            });
        }
    });
});

router.post('/getStateDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var stateid = reqBody.stateid;

        var objToSend = {
            stateid: stateid,
            paging: true
        }

        adminMisc.getStatesListForAdmin(req, res, objToSend, function (req, res, objStateData) {
            sendResponse.sendJsonResponse(req, res, 200, objStateData, "0", "FETCH");
        });
    });
});

router.post('/getReportData', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;
        //res.send(reqBody);

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var user_type = reqBody.user_type;
        var transTypefilter = reqBody.transTypefilter;
        var filterSD = reqBody.filterSD;
        var filterED = reqBody.filterED;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;


        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    user_type: user_type,
                    transTypefilter: transTypefilter,
                    filterSD: filterSD,
                    filterED: filterED
                };

                adminMisc.getReportData(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    user_type: user_type,
                    transTypefilter: transTypefilter,
                    filterSD: filterSD,
                    filterED: filterED
                };

                adminMisc.getReportData(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });

    });
});

router.post('/getMentorList', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var filterUserStatus = reqBody.filterUserStatus;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getPayoutRequest(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getPayoutRequest(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });
    });
});

router.post('/payoutMentorWallet', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;

        console.log(reqBody);

        var id = reqBody.id;
        var user_id = reqBody.user_id;
        var payoutAmount = reqBody.payoutAmount;        
        var mentorEmail = reqBody.mentorEmail;        
        var mentorpaypalEmail = reqBody.mentorpaypalEmail;        
        var walBal = reqBody.walBal;        
        var mentorName = reqBody.mentorName;        

        var objDataToPass = {
            id: id,
            user_id: user_id,
            payoutAmount: payoutAmount,            
            mentorEmail: mentorEmail,
            mentorpaypalEmail: mentorpaypalEmail,
            walBal: walBal,
            mentorName: mentorName
        };

        adminMisc.payoutMentorWallet(req, res, objDataToPass, function (req, res, objDataReturn) {
            sendResponse.sendJsonResponse(req, res, 200, objDataReturn, "0", "UPDATED");
        });
    });
});


router.post('/getPayoutDetails', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var mentorId = reqBody.mentorId;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    mentorId: mentorId
                }

                adminMisc.getMentorPayoutDetails(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    mentorId: mentorId
                }

                adminMisc.getMentorPayoutDetails(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });
    });
});

router.post('/getBroadCastData', function (req, res, callback) {
    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {

        var reqBody = req.body;

        var start = reqBody.start;
        var length = reqBody.length;
        var draw = reqBody.draw;
        var order = reqBody.order;
        var search = reqBody.search;

        var filterUserStatus = reqBody.filterUserStatus;

        var orderByColumn = order[0].column;
        var orderByColumnType = order[0].dir;

        var searchKey = search.value;

        async.parallel([
            // function call to get actual recordsFiltered
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    paging: true,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getBroadCastData(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            },
            // function call to get total records
            function (countryCallback) {

                var objToSend = {
                    start: start,
                    length: length,
                    orderByColumn: orderByColumn,
                    orderByColumnType: orderByColumnType,
                    searchKey: searchKey,
                    filterUserStatus: filterUserStatus
                }

                adminMisc.getBroadCastData(req, res, objToSend, function (req, res, objCountryData) {
                    countryCallback(null, objCountryData);
                });
            }
        ], function (error, resultSet) {

            var objCountryRecords = resultSet[0];
            var objCountryRecordsCount = resultSet[1][0]['totalCount'];

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "data": objCountryRecords,
                "draw": draw,
                "recordsTotal": objCountryRecordsCount,
                "recordsFiltered": objCountryRecordsCount
            }));
            res.end();
        });
    });
});

router.post('/addBroadCastMsg', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;
        adminMisc.addBroadCastMsg(req, res, reqBody, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "success");
        });

    });
});

router.post('/delBroadCastMsg', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;        
        adminMisc.delBroadCastMsg(req, res, reqBody, function (req, res, flagSuccess) {
            sendResponse.sendJsonResponse(req, res, 200, {flagSuccess: flagSuccess}, "0", "success");
        });

    });
});

router.post('/getMsgData', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;        
        adminMisc.getMsgData(req, res, reqBody, function (req, res, data) {
            sendResponse.sendJsonResponse(req, res, 200, data, "0", "success");
        });

    });
});

router.post('/editBroadCastMsg', function (req, res, callback) {

    adminModel.checkAdminLoggedInAPI(req, res, function (req, res) {
        var reqBody = req.body;
        adminMisc.editBroadCastMsg(req, res, reqBody, function (req, res, data) {
            sendResponse.sendJsonResponse(req, res, 200, data, "0", "success");
        });

    });
});


module.exports = router;