var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');

var adminMisc = require('./../model/admin/misc');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var paypalFunction = require('./../model/paypal_model');
var paymentModel = require('./../model/payment_model');
var sendResponse = require('./../modules/sendresponse');
var frontConstant = require('./../modules/front_constant');
var miscFunction = require('./../model/misc_model');
var dbConstants = require('./../modules/db_constants');
var constants = require('./../modules/constants');
var encdec = require('./../modules/encdec');
var router = express.Router();
var moment = require('moment');
var async = require('async');

var dbconnect = require('./../modules/dbconnect');



router.get('/:mentorMail', function (req, res, callback) {
    var mentorMail = req.params.mentorMail;

    var queryParam = [mentorMail];

    var strQuery = "    SELECT md.meeting_id meetingDetailId " +
            "   FROM meeting_details md, meeting_invitations mi, users u " +
            "   WHERE md.invitation_id = mi.invitation_id " +
            "   AND IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) = u.user_id " +
            "   AND mi.status = 'accepted' " +
            "   AND u.email = ? " +
            "   ORDER BY md.meeting_id DESC LIMIT 1";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {

        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }

        if (results.length > 0) {
            results = results[0];
            var meetingDetailId = results.meetingDetailId;


            // updating meeting status to be accepted.
            var data = {
                end_time: constants.CURR_UTC_DATETIME(),
                updated_date: constants.CURR_UTC_DATETIME(),
                status: 'ended',
                elapsed_time: 50
            };
            var queryParam = [data, meetingDetailId];
            var strQuery = " UPDATE meeting_details SET ? WHERE meeting_id = ? ";

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                }

                var queryParamMeetingInvite = [meetingDetailId];
                var strQueryMeetingInvite = " UPDATE meeting_invitations mi, meeting_details md SET mi.status = 'completed', mi.flag_cron_no_show_check = 'Y', mi.flag_cron_meeting_end_check = 'Y' WHERE mi.invitation_id = md.invitation_id AND md.meeting_id = ? ";

                dbconnect.executeQuery(req, res, strQueryMeetingInvite, queryParamMeetingInvite, function (req, res, error, results) {
                    if (error) {
                        sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                    }
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                });
            });
        }
    });
});

router.get('noshowbyuserapi/:mentorMail', function (req, res, callback) {
    var email = req.params.mentorMail;

    var param = [email];
    var strQuery = 'SELECT mi.invitation_id,urs.user_id FROM users urs ' +
            ' LEFT JOIN meeting_invitations mi ON IF(mi.invitation_type = "0",urs.user_id = mi.requester_id,urs.user_id = mi.user_id) ' +
            ' WHERE urs.email = "?" AND mi.status = "accepted" ORDER BY mi.invitation_id DESC LIMIT 1';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log("error");
        } else {
            var params = ['no_show_user', 'ended', results[0].user_id, results[0].invitation_id];
            var sqlQuery = ' UPDATE meeting_invitations mi, meeting_details md SET mi.flag_cron_no_show_check = "Y", mi.status = ?, md.status = ?, mi.declined_by = ? WHERE mi.invitation_id = ? AND mi.invitation_id = md.invitation_id';
            dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                } else {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                }
            });
        }
    });
})

router.get('noshowbybothapi/:mentorMail', function (req, res, callback) {
    var email = req.params.mentorMail;
    var param = [email];
    var strQuery = 'SELECT mi.invitation_id,urs.user_id FROM users urs ' +
            ' LEFT JOIN meeting_invitations mi ON IF(mi.invitation_type = "0",urs.user_id = mi.requester_id,urs.user_id = mi.user_id) ' +
            ' WHERE urs.email = "?" AND mi.status = "accepted" ORDER BY mi.invitation_id DESC LIMIT 1';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log("error");
        } else {
            var params = ['no_show_both', 'ended', null, results[0].invitation_id];
            var sqlQuery = ' UPDATE meeting_invitations mi, meeting_details md SET mi.flag_cron_no_show_check = "Y", mi.status = ?, md.status = ?, mi.declined_by = ? WHERE mi.invitation_id = ? AND mi.invitation_id = md.invitation_id';
            dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                } else {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                }
            });
        }
    });
});

router.get('noshowbymentorapi/:mentorMail', function (req, res, callback) {
    var email = req.params.mentorMail;
    var param = [email];
    var strQuery = 'SELECT mi.invitation_id,urs.user_id,IF(mi.invitation_type = "1",mi.requester_id,mi.user_id) as mentor_id FROM users urs ' +
            ' LEFT JOIN meeting_invitations mi ON IF(mi.invitation_type = "0",urs.user_id = mi.requester_id,urs.user_id = mi.user_id) ' +
            ' WHERE urs.email = "?" AND mi.status = "accepted" ORDER BY mi.invitation_id DESC LIMIT 1';
    dbconnect.executeQuery(req, res, strQuery, param, function (req, res, error, results, fields) {
        if (error) {
            console.log("error");
        } else {
            var params = ['no_show_both', 'ended', results[0].mentor_id, results[0].invitation_id];
            var sqlQuery = ' UPDATE meeting_invitations mi, meeting_details md SET mi.flag_cron_no_show_check = "Y", mi.status = ?, md.status = ?, mi.declined_by = ? WHERE mi.invitation_id = ? AND mi.invitation_id = md.invitation_id';
            dbconnect.executeQuery(req, res, sqlQuery, params, function (req, res, error, results, fields) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                } else {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                }
            });
        }
    });
});

router.get('timoutapi/:mentorMail', function (req, res, callback) {
    var mentorMail = req.params.mentorMail;

    var queryParam = [mentorMail];

    var strQuery = "    SELECT md.invitation_id " +
            "   FROM meeting_details md, meeting_invitations mi, users u " +
            "   WHERE md.invitation_id = mi.invitation_id " +
            "   AND IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) = u.user_id " +
            "   AND mi.status = 'accepted' " +
            "   AND u.email = ? " +
            "   ORDER BY md.meeting_id DESC LIMIT 1";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {

        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }

        if (results.length > 0) {
            results = results[0];
            var invitation_id = results.invitation_id;


            var queryParams = [constants.CURR_UTC_DATETIME(), invitation_id];

            var strQuery = '    UPDATE meeting_invitations mi' +
                    '   SET mi.status = "timedout", ' +
                    '   mi.updated_date = ? ' +
                    '   WHERE mi.invitation_id = ? ';

            dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                }

                var queryParams = [invitation_id];

                var strQuery = '    UPDATE meeting_invitation_slot_mapping mism ' +
                        '   SET mism.status = "timedout" ' +
                        '   WHERE mism.invitation_id = ? ';

                dbconnect.executeQuery(req, res, strQuery, queryParams, function (req, res, error, results) {
                    if (error) {
                        sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                    }
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                });
            });
        }
    });
});

router.get('fullrefund/:mentorMail', function (req, res, callback) {
    var mentorMail = req.params.mentorMail;

    var queryParam = [mentorMail];

    var strQuery = "    SELECT md.meeting_id meetingDetailId,md.invitation_id,md.meeting_url_key,mi.duration,i.total_payable_amount " +
            "   IF(mi.invitation_type = '1,mi.requester_id,mi.user_id) as mentor_id ,IF(mi.invitation_type = '1,mi.user_id,mi.requester_id) as user_id  " +
            "   FROM meeting_details md, meeting_invitations mi, invoice i , users u " +
            "   WHERE md.invitation_id = mi.invitation_id AND mi.invitation_id = i.invitation_id" +
            "   AND IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) = u.user_id " +
            "   AND mi.status = 'accepted' " +
            "   AND u.email = ? " +
            "   ORDER BY md.meeting_id DESC LIMIT 1";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {

        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }

        if (results.length > 0) {
            results = results[0];
            var meetingDetailId = results.meetingDetailId;
            var invitation_id = results.invitation_id;
            var meeting_url_key = results.meeting_url_key;
            var mentor_id = results.mentor_id;
            var user_id = results.user_id;
            var duration = results.duration;
            var total_payable_amount = results.total_payable_amount;


            // updating meeting status to be accepted.
            var data = {
                end_time: constants.CURR_UTC_DATETIME(),
                updated_date: constants.CURR_UTC_DATETIME(),
                status: 'ended',
                elapsed_time: 50
            };
            var queryParam = [data, meetingDetailId];
            var strQuery = " UPDATE meeting_details SET ? WHERE meeting_id = ? ";

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                }

                var queryParamMeetingInvite = [meetingDetailId];
                var strQueryMeetingInvite = " UPDATE meeting_invitations mi, meeting_details md SET mi.status = 'completed', mi.flag_cron_no_show_check = 'Y', mi.flag_cron_meeting_end_check = 'Y' WHERE mi.invitation_id = md.invitation_id AND md.meeting_id = ? ";

                dbconnect.executeQuery(req, res, strQueryMeetingInvite, queryParamMeetingInvite, function (req, res, error, results) {

                    userModel.getUniqueRefundRequestNumber(req, res, {}, function (req, res, objResponse) {
                        var newRandomUnqRefundRequest = objResponse.newRandomUnqRefundRequest;
                        var params = {
                            refund_request_unq_id: newRandomUnqRefundRequest,
                            invitation_id: invitation_id,
                            meeting_detail_id: meetingDetailId,
                            requester_id: user_id,
                            user_id: mentor_id,
                            reason: "No Show by mentor",
                            reason_description: "",
                            created_date: constants.CURR_UTC_DATETIME()
                        };
                        var strQuery = 'INSERT INTO refund_request SET ? ';
                        dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            } else {
                                var refundRequestId = results.insertId;
                                var objDataToSendToCreateMessageEntries = {
                                    refundRequestId: refundRequestId,
                                    unqRefundRequest: newRandomUnqRefundRequest,
                                    meetingUserId: user_id,
                                    meetingMentorId: mentor_id
                                };
                                userModel.createMessageEntriesForRefundRequest(req, res, objDataToSendToCreateMessageEntries, function (req, res, objResponse) {

                                    /**********process refund *******************/

                                    var objDataToSend = {
                                        chkRefundType: "FULLREF",
                                        invitationId: invitation_id,
                                        meetingCost: total_payable_amount,
                                        meetingDetailId: meetingDetailId,
                                        meetingMentorId: mentor_id,
                                        meetingUserId: user_id,
                                        refundRequestId: refundRequestId,
                                        refundRequestUnqId: newRandomUnqRefundRequest,
                                        taRefundMessage: "No Show by mentor",
                                        txtAmountToRefund: total_payable_amount,
                                        meetingDuration: duration,
                                        meeting_url_key: meeting_url_key
                                    }

                                    adminMisc.processPartialRefund(req, res, objDataToSend, function (req, res, objDataResponse) {
                                        if (objDataResponse) {
                                            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                                        } else {
                                            sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                                        }
                                    });


                                    /**********process refund *******************/

                                });
                            }
                        });
                    });
                });
            });
        }
    });
});


router.get('partialrefund/:mentorMail/:amount', function (req, res, callback) {
    var mentorMail = req.params.mentorMail;
    var amount = req.params.amount;

    var queryParam = [mentorMail];

    var strQuery = "    SELECT md.meeting_id meetingDetailId,md.invitation_id,md.meeting_url_key,mi.duration,i.total_payable_amount " +
            "   IF(mi.invitation_type = '1,mi.requester_id,mi.user_id) as mentor_id ,IF(mi.invitation_type = '1,mi.user_id,mi.requester_id) as user_id  " +
            "   FROM meeting_details md, meeting_invitations mi, invoice i , users u " +
            "   WHERE md.invitation_id = mi.invitation_id AND mi.invitation_id = i.invitation_id" +
            "   AND IF(mi.invitation_type = 0, mi.requester_id, mi.user_id) = u.user_id " +
            "   AND mi.status = 'accepted' " +
            "   AND u.email = ? " +
            "   ORDER BY md.meeting_id DESC LIMIT 1";

    dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {

        if (error) {
            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
        }

        if (results.length > 0) {
            results = results[0];
            var meetingDetailId = results.meetingDetailId;
            var invitation_id = results.invitation_id;
            var meeting_url_key = results.meeting_url_key;
            var mentor_id = results.mentor_id;
            var user_id = results.user_id;
            var duration = results.duration;
            var total_payable_amount = results.total_payable_amount;


            // updating meeting status to be accepted.
            var data = {
                end_time: constants.CURR_UTC_DATETIME(),
                updated_date: constants.CURR_UTC_DATETIME(),
                status: 'ended',
                elapsed_time: 50
            };
            var queryParam = [data, meetingDetailId];
            var strQuery = " UPDATE meeting_details SET ? WHERE meeting_id = ? ";

            dbconnect.executeQuery(req, res, strQuery, queryParam, function (req, res, error, results) {
                if (error) {
                    sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                }

                var queryParamMeetingInvite = [meetingDetailId];
                var strQueryMeetingInvite = " UPDATE meeting_invitations mi, meeting_details md SET mi.status = 'completed', mi.flag_cron_no_show_check = 'Y', mi.flag_cron_meeting_end_check = 'Y' WHERE mi.invitation_id = md.invitation_id AND md.meeting_id = ? ";

                dbconnect.executeQuery(req, res, strQueryMeetingInvite, queryParamMeetingInvite, function (req, res, error, results) {

                    userModel.getUniqueRefundRequestNumber(req, res, {}, function (req, res, objResponse) {
                        var newRandomUnqRefundRequest = objResponse.newRandomUnqRefundRequest;
                        var params = {
                            refund_request_unq_id: newRandomUnqRefundRequest,
                            invitation_id: invitation_id,
                            meeting_detail_id: meetingDetailId,
                            requester_id: user_id,
                            user_id: mentor_id,
                            reason: "No Show by mentor",
                            reason_description: "",
                            created_date: constants.CURR_UTC_DATETIME()
                        };
                        var strQuery = 'INSERT INTO refund_request SET ? ';
                        dbconnect.executeQuery(req, res, strQuery, params, function (req, res, error, results, fields) {
                            if (error) {
                                throw error;
                            } else {
                                var refundRequestId = results.insertId;
                                var objDataToSendToCreateMessageEntries = {
                                    refundRequestId: refundRequestId,
                                    unqRefundRequest: newRandomUnqRefundRequest,
                                    meetingUserId: user_id,
                                    meetingMentorId: mentor_id
                                };
                                userModel.createMessageEntriesForRefundRequest(req, res, objDataToSendToCreateMessageEntries, function (req, res, objResponse) {

                                    /**********process refund *******************/

                                    var objDataToSend = {
                                        chkRefundType: "PARTIAL",
                                        invitationId: invitation_id,
                                        meetingCost: total_payable_amount,
                                        meetingDetailId: meetingDetailId,
                                        meetingMentorId: mentor_id,
                                        meetingUserId: user_id,
                                        refundRequestId: refundRequestId,
                                        refundRequestUnqId: newRandomUnqRefundRequest,
                                        taRefundMessage: "No Show by mentor",
                                        txtAmountToRefund: amount,
                                        meetingDuration: duration,
                                        meeting_url_key: meeting_url_key
                                    }

                                    adminMisc.processPartialRefund(req, res, objDataToSend, function (req, res, objDataResponse) {
                                        if (objDataResponse) {
                                            sendResponse.sendJsonResponse(req, res, 200, {message: 'Some Error Occured. Please try again.'}, {}, 'DBERROR');
                                        } else {
                                            sendResponse.sendJsonResponse(req, res, 200, {message: 'Meeting status updated successfully.'}, {}, 'DONE');
                                        }
                                    });


                                    /**********process refund *******************/

                                });
                            }
                        });
                    });
                });
            });
        }
    });
});

// exporting all the routes
module.exports = router;