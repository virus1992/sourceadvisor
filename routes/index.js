var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var stripe = require('stripe')('sk_test_FBjXXumZacoKFirICPewX50C');
var async = require("async");
var _ = require('lodash');

// model
var userModel = require('./../model/users_model');
var miscFunction = require('./../model/misc_model');
var mentorModel = require('./../model/mentor_model');
var cronModel = require('./../model/cron_model');
var paymentModel = require('./../model/payment_model');
var mailer = require('./../modules/mailer');
var encdec = require('./../modules/encdec');

// modules
var frontConstant = require('./../modules/front_constant');
var constant = require('./../modules/constants');
var db_constant = require('./../modules/db_constants');
var sendResponse = require('./../modules/sendresponse');

var dbconnect = require('./../modules/dbconnect');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})
var upload = multer({storage: storage});
var router = express.Router();


//router.get('/getPendingReviewReminder', function (req, res, callback) {
//    cronModel.getPenginReminder(req, res, {}, function (req, res, objTest) {
//        console.log("objData :: ", objTest);
//        res.send(objTest);
//    });
//});

router.get('/test', function (req, res, callback) {
    miscFunction.getAllData(req, res, function (req, res, result) {

    });
});

router.get('/location', function (req, res, callback) {

    var renderParams = {
        layout: 'blank'
    };

    res.render('test/location', renderParams);
});

router.post('/getDbConstants', function (req, res) {
    sendResponse.sendJsonResponse(req, res, 200, db_constant);
});


router.get('/jwttest', function (req, res, callback) {

    //import crypto module to generate random binary data 
    var crypto = require('crypto');

    // generate random passphrase binary data 
    var r_pass = crypto.randomBytes(128);

    // convert passphrase to base64 format 
    var r_pass_base64 = r_pass.toString("base64");

    console.log("passphrase base64 format: ");
    console.log(r_pass_base64);


    var renderParams = {
        layout: 'blank',
        r_pass_base64: r_pass_base64,
        r_pass: r_pass
    };

    res.render('test/jwttest', renderParams);
});

router.get('/privateinvitetest', function (req, res) {
    // fucntion call to expire normal meeting invites.
    var dataToPass = {
        flagNormalInvite: false
    }
    userModel.timedoutCronMeetingSelect(req, res, dataToPass);
});

router.get('/tagtest', function (req, res, callback) {
    var renderParams = {
        layout: 'blank',
        frontConstant: frontConstant,
    };
    res.render('test/tagtest', renderParams);
});


router.get('/capturementorpolicy', function (req, res, callback) {
    var objParams = {
        invitationId: 3
    }
    userModel.captureMeetingInvitationDetailsForBookiing(req, res, objParams, function (req, res, data) {
        res.send(data);
    });
});


router.get('/updatenoshowboth', function (req, res, callback) {
    var objParams = {
        invitationId: 40,
        meetingUserId: 257,
        meetingMentorId: 245
    };
    console.log("objParams :: ", objParams);
    paymentModel.updateInvoiceForNoShowBothParties(req, res, objParams, function (req, res, objResponse) {
        res.send(objResponse);
    });
});


router.get('/updateinvoicenoshowbymentor', function (req, res, callback) {
    var objParams = {
        invitationId: 40,
        meetingUserId: 257,
        meetingMentorId: 245
    };

    paymentModel.updateInvoiceForNoShowByMentor(req, res, objParams, function (req, res, objResponse) {
        res.send(objResponse);
    });
});


router.get('/updateinvoicenoshowbyuser', function (req, res, callback) {
    var objParams = {
        invitationId: 40,
        meetingUserId: 257,
        meetingMentorId: 245
    };

    paymentModel.updateInvoiceForNoShowByUser(req, res, objParams, function (req, res, objResponse) {
        res.send(objResponse);
    });
});

router.get('/updateInvoiceForMentorCancellAcceptedInvite', function (req, res, callback) {
    var objParams = {
        invitationId: 3,
        meetingUserId: 257,
        meetingMentorId: 245
    };

    paymentModel.updateInvoiceForMentorCancellAcceptedInvite(req, res, objParams, function (req, res, objResponse) {
        //res.send(objResponse);
    });
});

router.get('/updateInvoiceForUserCancellAcceptedInvite', function (req, res, callback) {
    var objParams = {
        invitationId: 13,
        meetingUserId: 257,
        meetingMentorId: 245
    };

    paymentModel.updateInvoiceForUserCancellAcceptedInvite(req, res, objParams, function (req, res, objResponse) {
        //res.send(objResponse);
    });
});

router.get('/getcountryStateCity', function (req, res, callbacks) {


//     miscFunction.getCountryListForAllDB(req, res, function(req, res, objResponse) {
// // console.log("objResponse :: ", objResponse);
//         _.map(objResponse, function(objRow) {
//             var countryId = objRow.country_id;
//             var countryName = objRow.country_name;

//             async.parallel([
//                 /// function call to fetch state
//                 function(callbackGeoData) {
//                     miscFunction.
//                 },
//                 // function call to fetch cityList
//                 function(callbackGeoData) {

//                 }
//             ], function(error, arrResult) {

//             });
//         });

//         sendResponse.sendJsonResponse(req, res, 200, objResponse, "1", "DONE");
//     });

    miscFunction.getCountryCityDataBase(req, res, 0, function (req, res, result) {

    });

    sendResponse.sendJsonResponse(req, res, 200, {}, "1", "DONE");
});


router.get('/getStateWithoutCountry', function (req, res, callbacks) {

    miscFunction.getStateWithoutCountry(req, res, function (req, res, result) {

    });

    sendResponse.sendJsonResponse(req, res, 200, {}, "1", "DONE");
});




// router.get('/timedoutcron', function(req, res, callback) {
//     var dataToPass = {
//         flagNormalInvite: true
//     }
//     userModel.timedoutCronMeetingSelect(req, res, dataToPass);
//     //res.send("asdfasdfasdf");
// });


// router.get('/searchplugin1', function(req, res, callback){
//     var renderParams = {
//             layout: 'blank',
//             frontConstant: frontConstant,
//     };
//     res.render('test/searchplugin1', renderParams);
// });

// router.get('/noshowupdate', function(req, res, callback){
//     cronModel.finalStatusUpdateOfMeeting(req, res, function(req, res, objData) {

//     });
// });



// router.get('/updateInvoiceForNormalInviteDeclineByMentor', function(req ,res, callback){


//     var objUpdateInvoiceForNormalInviteDeclineByMentor = {
//         invitationId: 1,
//         meetingUserId: 257,
//         meetingMentorId: 245
//     };

//     paymentModel.updateInvoiceForNormalInviteDeclineByMentor(req, res, objUpdateInvoiceForNormalInviteDeclineByMentor, function(req, res, objResponse){
// res.send(objResponse);
//     });

// console.log("objDataToResentInvoiceDetailForPrivateMeetingTimeout :: ", objDataToResentInvoiceDetailForPrivateMeetingTimeout);
//     paymentModel.updateInvoiceForAfterAcceptMeetingStatusChange(req, res, objDataToResentInvoiceDetailForPrivateMeetingTimeout, function(req, res, objResponse){
//         res.send(objResponse);
//     });
// });



// router.get('/finalmeetingend', function(req, res, callback){
//     cronModel.finalStatusMeetingEndStatusUpdateOfMeeting(req, res, function(req, res, objData) {

//     });
// });


router.post('/classmarkertest', function (req, res, callback) {
    var params = {
        calledFrom: "POST"
    };
    cronModel.classMarkerTestResponse(req, res, params, function (req, res, objResponse) {
        res.send(200);
    });
});


/* GET home page. */
router.get('/', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }

    async.parallel([
        function (homePageCallback) {
            mentorModel.featuredMentorProfile(req, res, loggedInUserId, 3, function (req, res, arrMentorData) {
                homePageCallback(null, arrMentorData)
            });
        },
        function (homePageCallback) {
            miscFunction.getHomePageMentorExpertiseTags(req, res, {}, function (req, res, arrRandMentorExpertise) {
                homePageCallback(null, arrRandMentorExpertise);
            });
        }
    ], function (error, resultSet) {
        var arrMentorData = resultSet[0];
        var arrRandMentorExpertise = resultSet[1];


        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            loggedInUserId: loggedInUserId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData,
            arrRandMentorExpertise: arrRandMentorExpertise,
            hidGlobalSearch: "1",
            onlyNonSecure: "1"
        };
        res.render('index', renderParams);
    });
});

router.post('/checkMailWebSitePassword', function (req, res, callback) {
    var reqBody = req.body;

    JSON.stringify(reqBody, null, 4);


    var siteUserName = constant.SITEBLOCKPOPUSERNAME;
    var sitePassword = constant.SITEBLOCKPOPPASSWORD;

    var userName = reqBody.userName;
    var password = reqBody.password;



    if (siteUserName != userName) {
        sendResponse.sendJsonResponse(req, res, 200, {}, "1", "INVUSRN");
    } else if (sitePassword != password) {
        sendResponse.sendJsonResponse(req, res, 200, {}, "1", "INVPASS");
    } else {
        sendResponse.sendJsonResponse(req, res, 200, {}, "0", "VLUSR");
    }


});

router.get('/login', function (req, res, callback) {

    userModel.userNotAuthentication(req, res, function (req, res) {

        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'loginmodule_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session,
            userIp: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        };

        res.render('login', renderParams);

    });

});

router.get('/register', function (req, res, callback) {
    userModel.userNotAuthentication(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'loginmodule_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session,
            userIp: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        };

        res.render('registeration', renderParams);
    });
});

router.get('/organisation_register', function (req, res, callback) {
    userModel.userNotAuthentication(req, res, function (req, res) {
        miscFunction.getIndustryListFromDb(req, res, function (req, res, industriesList) {
            miscFunction.getSubIndustryListFromDb(req, res, function (req, res, subIndustriesList) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'loginmodule_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    industriesList: industriesList,
                    subIndustriesList: subIndustriesList,
                    userIp: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                };

                res.render('orgregistration', renderParams);
            });
        });
    });
});

router.get('/verifyUserEmail/:verficiationCode', function (req, res, callback) {
    var verficationCode = encodeURIComponent(req.params.verficiationCode);
    userModel.verifyUserEmail(req, res, verficationCode, userModel.updateUserStatus);
});

router.get('/forgotPassword', function (req, res, callback) {

    userModel.userNotAuthentication(req, res, function (req, res) {

        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'loginmodule_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session
        };

        res.render('forgotpassword', renderParams);

    });

});

router.get('/activateAccount', function (req, res, callback) {

    userModel.userNotAuthentication(req, res, function (req, res) {

        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'loginmodule_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session
        };

        res.render('activateAccount', renderParams);

    });

});

router.get('/aboutus', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('aboutus', renderParams);
    });

});

router.get('/policies', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('policies', renderParams);
    });

});

router.get('/promos', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };
        res.render('promos', renderParams);
    });

});
router.get('/terms_of_service', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };
        res.render('terms_of_service', renderParams);
    });
});

router.get('/privacy', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };
        res.render('privacy', renderParams);
    });
});

router.get('/compliance', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('compliance', renderParams);
    });

});

router.get('/pricing_mentor_services', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('pricing_mentor_services', renderParams);
    });

});



router.get('/contact_us', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('contactus', renderParams);
    });

});

router.get('/why_be_a_mentor', function (req, res, callback) {
    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }
    mentorModel.featuredMentorProfile(req, res, loggedInUserId, 6, function (req, res, arrMentorData) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'homepage',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            constant: constant,
            flagUserLoggedIn: flagUserLoggedIn,
            userId: userId,
            arrSessionData: req.session,
            arrMentorData: arrMentorData
        };

        res.render('why_be_a_mentor', renderParams);
    });

});

router.get('/generateNewPassword/:forgotPasswordToken', function (req, res, callback) {
    var forgotPasswordToken = req.params.forgotPasswordToken;
    userModel.validateUserToken(req, res, forgotPasswordToken, 'forgot_password', processResponseFromValidTokenForForgotPassword);

});

var processResponseFromValidTokenForForgotPassword = function (req, res, flagValidToken, forgotPasswordToken, tokenType) {

    // if token is valid then render set new password page.
    if (flagValidToken) {

        req.flash('messages', 'Please set new password.');
        res.redirect('/setNewPassword/' + forgotPasswordToken);
    }
    // if token is not valid then than redirect to forgot password page with error.
    else {
        req.flash('errors', 'Invalid forgot password link. Please generate again');
        res.redirect('/forgotPassword');
    }
}

router.get('/setNewPassword/:forgotPasswordToken', function (req, res, callback) {
    var forgotPasswordToken = req.params.forgotPasswordToken;

    userModel.userNotAuthentication(req, res, function (req, res) {

        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'loginmodule_layout',
            errors: errors,
            messages: messages,
            forgotPasswordToken: forgotPasswordToken,
            frontConstant: frontConstant,
            arrSessionData: req.session
        };

        res.render('setnewpassword', renderParams);

    });

});

router.post('/setNewPassword', function (req, res, callback) {
    userModel.resetNewPasswordForUser(req, res, handleNewPasswordResponse);
});

var handleNewPasswordResponse = function (req, res, flagError) {

    if (flagError) {

        var reqBody = req.body;
        JSON.stringify(reqBody, null, 4);

        var forgotPasswordToken = reqBody.forgotPasswordToken;

        req.flash('errors', 'Error occured, Please try again.');
        res.redirect('/setNewPassword/' + forgotPasswordToken);
    } else {
        req.flash('messages', 'Your password has been reset. Please login to continue.');
        res.redirect('/login');
    }
}

router.get('/dashboard', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        if (req.session.userType == 'user') {

            async.parallel([
                function (transectionDataCallback) {
                    mentorModel.getTransectionData_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.getTotalMoneySpent_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.mentorContacted_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    var conType = "week";
                    mentorModel.upComingMeeting_model(req, res, conType, function (req, res, meetingDataRes) {
                        transectionDataCallback(null, meetingDataRes);
                    });
                },
                function (transectionDataCallback) {
                    var conType = "month";
                    mentorModel.upComingMeeting_model(req, res, conType, function (req, res, meetingDataRes) {
                        transectionDataCallback(null, meetingDataRes);
                    });
                }
            ], function (err, results) {
                var transectionData = results[0];
                var spentAmount = results[1];
                var mentorContacted = results[2];
                var meetingDataResWeek = results[3];
                var meetingDataResMonth = results[3];

                var renderParams = {
                    title: 'DASHBOARD',
                    layout: 'dashboard_layout',
                    arrSessionData: req.session,
                    frontConstant: frontConstant,
                    bodyClass: 'showNavRed',
                    transectionData: transectionData,
                    spentAmount: spentAmount,
                    mentorContacted: mentorContacted,
                    meetingDataResWeek: meetingDataResWeek,
                    meetingDataResMonth: meetingDataResMonth
                };
                res.render('user_dashboard', renderParams);
            });
        } else {

            async.parallel([
                function (transectionDataCallback) {
                    mentorModel.getTransectionData_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.getWalletAmount_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.getYearlyIncome_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.getMonthIncome_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.getTotalIncome_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.paypalEmail_model(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
                function (transectionDataCallback) {
                    var objParam = {
                        user_id: req.session.userId
                    };
                    encdec.genrateKey(req, res, objParam, function (req, res, result) {
                        transectionDataCallback(null, result);
                    });
                },
                function (transectionDataCallback) {
                    mentorModel.mentorDetailsforPayout(req, res, function (req, res, arrData) {
                        transectionDataCallback(null, arrData);
                    });
                },
            ], function (err, results) {
                var transectionData = results[0];
                var walletAmount = results[1];
                var earlyAmount = results[2];
                var monthAmount = results[3];
                var totalAmount = results[4];
                var paypalEmail = results[5];
                var encInfo = results[6];
                var mentordata = results[7];

                var renderParams = {
                    title: 'DASHBOARD',
                    layout: 'dashboard_layout',
                    arrSessionData: req.session,
                    frontConstant: frontConstant,
                    bodyClass: 'showNavRed',
                    transectionData: transectionData,
                    walletAmount: walletAmount,
                    earlyAmount: earlyAmount,
                    monthAmount: monthAmount,
                    totalAmount: totalAmount,
                    paypalEmail: paypalEmail,
                    encId: encInfo.encId,
                    encKey: encInfo.encKey,
                    mentordata: mentordata
                };
                res.render('mentor_dashboard', renderParams);
            });
        }
    });
});

router.get('/logout', function (req, res, callback) {
    req.session.regenerate(function (err) {
        req.flash('messages', 'User logged out successfully.')
        res.redirect('/login');
    });
});

router.post('/register', function (req, res, callback) {
    userModel.checkUserByUserName(req, res, userModel.createNewUser);
});

router.post('/loginchk', function (req, res, callback) {
    userModel.checkUserLoginChk(req, res, userModel.setUserSessionChk);
});

router.post('/login', function (req, res, callback) {
    userModel.checkUserLogin(req, res, userModel.setUserSession);
});

router.post('/loginCheckPopUp', function (req, res, callback) {
    userModel.checkUserLoginPopUp(req, res, userModel.setUserSessionPopUp);
});

router.post('/forgotPassword', function (req, res, callback) {
    userModel.userNotAuthentication(req, res, function (req, res) {
        userModel.checkUserEmailValid(req, res);
    });
});

router.post('/newlinereplace', function (req, res) {
    res.send("MILIND");
});

router.post('/activateAcc', function (req, res, callback) {
    userModel.userNotAuthentication(req, res, function (req, res) {
        userModel.checkUserEmailValidForAccActivation(req, res);
    });
});

router.post('/socialLoginCheckUser', function (req, res) {
    userModel.checkUserIsDeleted(req, res, userModel.checkUserSocialLogin);
    //userModel.checkUserSocialLogin(req, res);
});

router.get('/redirectToEmailActivePage', function (req, res) {
    req.flash('errors', 'Account is currently Inactive. To Activate enter the e-mail associated with the Acct.');
    res.redirect('/activateAccount');
});

router.get('/mentorprofile/:mentorSlug', function (req, res, callback) {
    var mentorSlug = req.params.mentorSlug;
    mentorModel.verifyMentorSlug(req, res, mentorSlug, renderMentorDetailPage, mentorModel.getMentorAllData);
});

var renderMentorDetailPage = function (req, res, arrMentorData) {
    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var flagUserLoggedIn = false;
    var userId = "0";

    var reqBody = req.body;

    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }

    var renderParams = {
        layout: 'mentor_detail_layout',
        errors: errors,
        messages: messages,
        arrMentorData: arrMentorData,
        frontConstant: frontConstant,
        flagUserLoggedIn: flagUserLoggedIn,
        userId: userId,
        loggedInUserId: loggedInUserId,
        arrSessionData: req.session,
        mentorSlug: req.params.mentorSlug
    };
    res.render('mentors/detail', renderParams);
};

router.get('/stripetest', function (req, res, callback) {


    /*/* /*stripe.customers.create({ email: 'customer@example.com' }, function(err, customer) {
     console.log(err);
     console.log(customer);
     err; // null if no error occurred 
     customer; // the created customer object 
     }
     );
     
     // Create a new customer and then a new charge for that customer: 
     stripe.customers.create({
     email: 'milpas999@gmail.com'
     }).then(function(customer) {
     console.log("customer created");
     console.log(customer);
     
     stripe.recipients.
     /*return stripe.charges.create({
     amount: 1600,
     currency: 'usd',
     customer: customer.id
     });
     }).then(function(charge) {
     console.log('charge');
     console.log(charge);
     // New charge created on a new customer 
     }).catch(function(err) {
     // Deal with an error 
     });*/

    var renderParams = {
        layout: 'homepage',
        frontConstant: frontConstant,
        constant: constant
    };

    res.render('stripetest', renderParams);
});

router.post('/stripetest2', function (req, res, callback) {


    var email = req.body.email;
    var cardNumber = req.body.cardNumber;
    var exp_month = req.body.exp_month;
    var exp_year = req.body.exp_year;
    var cvc = req.body.cvc;
    var address_zip = req.body.address_zip;
    var stripeToken = req.body.stripeToken;


    stripe.customers.create({email: 'customer@example.com'}, function (err, customer) {
        console.log(err);
        console.log(customer);

        var customerId = customer.id;
        console.log(customerId);

        /*stripe.customers.createSource(customerId,{source: stripeToken},function(err, card) {
         console.log(err);                
         console.log(card);
         });*/

        // Create a charge: this will charge the user's card
        var charge = stripe.charges.create({
            amount: 51, // Amount in cents
            currency: "usd",
            source: stripeToken,
            description: "Example charge"
        }, function (err, charge) {
            console.log(err);
            if (err && err.type === 'StripeCardError') {
                // The card has been declined
            }
            console.log(charge);
        });

    });

});

router.post('/checkUserBasicProfileComplete', function (req, res, callback) {
    var emptyFields = [];
    //userModel.userAPIAuthentication(req, res, function (req, res) {
    userModel.checkUserBasicProfileComplete_model(req, res, function (req, res, userBasicInfo) {
        for (var key in userBasicInfo) {
            if (userBasicInfo[key] == '') {
                emptyFields.push(key);
            }
        }
        if (emptyFields.length > 0) {
            sendResponse.sendJsonResponse(req, res, 200, emptyFields, 0, 'MISSING');
        } else {
            sendResponse.sendJsonResponse(req, res, 200, [], 0, 'VALID');
        }
    });
    //});
});



router.get('/feedback/:feedbackKey', function (req, res, callback) {
    var feedbackKey = req.params.feedbackKey;
    var objParams = {
        feedbackKey: feedbackKey
    };
    var msg = "";
    miscFunction.getFeedbackData_model(req, res, objParams, function (req, res, feedbackData) {
        if (feedbackData != "") {
            msg = "";
        } else {
            msg = "Invalid URL";
        }

        var renderParams = {
            layout: 'independent_layout',
            frontConstant: frontConstant,
            constant: constant,
            feedbackKey: feedbackKey,
            feedbackData: feedbackData,
            msg: msg
        };
        res.render('independent/leave_feedback', renderParams);
    });
});

router.post('/updateKey', function (req, res) {
    miscFunction.updateKey_model(req, res);
});

router.post('/getBroadCastMsg', function (req, res, callback) {
    miscFunction.getBroadCastMsg(req, res, function (req, res, arrBCMsg) {
        sendResponse.sendJsonResponse(req, res, 200, arrBCMsg, "0", "FETCH");
    });
});

router.post('/chkMeeting', function (req, res, callback) {
    cronModel.chkMeeting(req, res, function (req, res, arrMeetingChk) {
        sendResponse.sendJsonResponse(req, res, 200, arrMeetingChk, "0", "FETCH");
    });
});

module.exports = router;
