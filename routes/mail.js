var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var Busboy = require('busboy');
var os = require('os');
var inspect = require('util').inspect;
var async = require("async");
var async2 = require("async");
var md5 = require('md5');

var app = express();
var OpenTok = require('opentok');

// model
var mailer = require('./../modules/mailer.js');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var miscFunction = require('./../model/misc_model');
var meetingLogModel = require('./../model/meetinglog_model');

// modules
var frontConstant = require('./../modules/front_constant');
var constant = require('./../modules/constants');
var multerSetting = require('./../modules/multerSetting');
var handlers = require('./../modules/handlers');
var sendResponse = require('./../modules/sendresponse');
var opentok = new OpenTok(constant.TOKBOX_API_KEY, constant.TOKBOX_SECRET_KEY);

var upload = multer({storage: multerSetting.defaultStorage});
var keynoteUpload = multer({storage: multerSetting.keynoteStorage});
var workfolioStorage = multer({storage: multerSetting.workfolioStorage});

var router = express.Router();

var moment = require('moment');


router.get('/acceptedInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedInvitationMailMentor', renderParams);
});

router.get('/acceptedInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedInvitationMailUser', renderParams);
});
router.get('/acceptedInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedInvitationMessageMentor', renderParams);
});
router.get('/acceptedInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedInvitationMessageUser', renderParams);
});
router.get('/acceptedPrivateInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedPrivateInvitationMailMentor', renderParams);
});
router.get('/acceptedPrivateInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedPrivateInvitationMailUser', renderParams);
});
router.get('/acceptedPrivateInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedPrivateInvitationMessageMentor', renderParams);
});
router.get('/acceptedPrivateInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        }
    };
    res.render('mailtemplate/acceptedPrivateInvitationMessageUser', renderParams);
});
router.get('/blanktemplate', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            dateDisplay: "sdfasdfasdfasdf",
            timeDisplay: "asdsa"
        },
        blankData: "test"
    };
    res.render('mailtemplate/blanktemplate', renderParams);
});
router.get('/cancelacceptedInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/cancelacceptedInvitationMailMentor', renderParams);
});
router.get('/cancelacceptedInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/cancelacceptedInvitationMailUser', renderParams);
});
router.get('/cancelacceptedInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/cancelacceptedInvitationMessageMentor', renderParams);
});
router.get('/cancelacceptedInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/cancelacceptedInvitationMessageUser', renderParams);
});
router.get('/declinedInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedInvitationMailMentor', renderParams);
});
router.get('/declinedInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedInvitationMailUser', renderParams);
});
router.get('/declinedInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedInvitationMessageMentor', renderParams);
});
router.get('/declinedInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedInvitationMessageUser', renderParams);
});
router.get('/declinedPrivateInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedPrivateInvitationMailMentor', renderParams);
});
router.get('/declinedPrivateInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedPrivateInvitationMailUser', renderParams);
});
router.get('/declinedPrivateInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedPrivateInvitationMessageMentor', renderParams);
});
router.get('/declinedPrivateInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {dateDisplay: "13-04-2017",
                        timeDisplay: "15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/declinedPrivateInvitationMessageUser', renderParams);
});
router.get('/meetingrequestmentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/meetingrequestmentor', renderParams);
});
router.get('/meetingrequestmentorbefore30minutes', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        userFullName: "Virendra",

        slot: {
            date: "14-04-2017",
            startTime: "13:00",
            endTime: "13:45"
        },
        blankData: "test"
    };
    res.render('mailtemplate/meetingrequestmentorbefore30minutes', renderParams);
});
router.get('/meetingrequestuser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/meetingrequestuser', renderParams);
});
router.get('/meetingrequestuserbefore30minutes', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorFullName: "Virendra",
        userName: "Milind",
        slot: {
            date: "14-04-2017",
            startTime: "13:00",
            endTime: "13:45"
        },
        blankData: "test"
    };
    res.render('mailtemplate/meetingrequestuserbefore30minutes', renderParams);
});
router.get('/msgreviecedmail', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorFullName: "Virendra",
        userName: "Milind",
        sendername: "Virendra",
        userSlug: "virendra",
        msgBody: "This is Test",
        userBreif: "This is user Brif",
        slot: {
            date: "14-04-2017",
            startTime: "13:00",
            endTime: "13:45"
        },
        blankData: "test"
    };
    res.render('mailtemplate/msgreviecedmail', renderParams);
});
router.get('/privateinvitedeletementor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinvitedeletementor', renderParams);
});
router.get('/privateinvitedeleteuser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinvitedeleteuser', renderParams);
});
router.get('/privateinvitedeletementormessage', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinvitedeletementormessage', renderParams);
});
router.get('/privateinvitedeleteusermessage', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinvitedeleteusermessage', renderParams);
});
router.get('/privateinvitementor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinvitementor', renderParams);
});
router.get('/privateinviteupdatementor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinviteupdatementor', renderParams);
});
router.get('/privateinviteupdatementormessage', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinviteupdatementormessage', renderParams);
});
router.get('/privateinviteupdateuser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinviteupdateuser', renderParams);
});
router.get('/privateinviteupdateusermessage', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinviteupdateusermessage', renderParams);
});
router.get('/privateinviteuser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        blankData: "test"
    };
    res.render('mailtemplate/privateinviteuser', renderParams);
});
router.get('/promocodemailtouser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        promocode: "WX20",
        discount: "20",
        blankData: "test"
    };
    res.render('mailtemplate/promocodemailtouser', renderParams);
});
router.get('/registration', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        userName: "Milind",
        verifyURL: "http://example.com"
    };
    res.render('mailtemplate/registration', renderParams);
});
router.get('/reviewmail', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        communication: 'GOOD',
        call_quality: 'GOOD',
        system_stability: 'GOOD',
        dexpertise_or_pre_call: 'GOOD',
        review_desc: 'GOOD'
    };
    res.render('mailtemplate/reviewmail', renderParams);
});
router.get('/socialregister', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        userName: "Milind",
        registeredEmail: "milind@solulab.com",
        systemGeneratedPassword: "milind123",
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        promocode: "WX20",
        discount: "20",
        blankData: "test"
    };
    res.render('mailtemplate/socialregister', renderParams);
});
router.get('/timeoutmailmentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        promocode: "WX20",
        discount: "20",
        blankData: "test"
    };
    res.render('mailtemplate/timeoutmailmentor', renderParams);
});
router.get('/timeoutmailuser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
        slotsInMail: {
            "slot1":
                    {index: "1",
                        displayFormat: "15-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "16-04-2017 15:00"}
        },
        promocode: "WX20",
        discount: "20",
        blankData: "test"
    };
    res.render('mailtemplate/timeoutmailuser', renderParams);
});
router.get('/timeoutmessagementor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        }
    };
    res.render('mailtemplate/timeoutmessagementor', renderParams);
});
router.get('/timeoutusermessage', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
    };
    res.render('mailtemplate/timeoutusermessage', renderParams);
});
router.get('/usercancelacceptedInvitationMailMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
    };
    res.render('mailtemplate/usercancelacceptedInvitationMailMentor', renderParams);
});
router.get('/usercancelacceptedInvitationMailUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
    };
    res.render('mailtemplate/usercancelacceptedInvitationMailUser', renderParams);
});
router.get('/usercancelacceptedInvitationMessageMentor', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
    };
    res.render('mailtemplate/usercancelacceptedInvitationMessageMentor', renderParams);
});
router.get('/usercancelacceptedInvitationMessageUser', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        reviewer: "Milind",
        verifyURL: "http://example.com",
        arrUserData: {
            firstName: "Milind",
            lastName: "pasi",
            slug: "ters",
            userBreif: 'This is test data'
        },
        arrMentorData: {
            firstName: "Virendra",
            lastName: "Sodha",
            slug: "asdasd"
        },
        slots: {
            "slot1":
                    {index: "1",
                        displayFormat: "13-04-2017 15:00"},
            "slot2":
                    {index: "2",
                        displayFormat: "14-04-2017 15:00"}
        },
    };
    res.render('mailtemplate/usercancelacceptedInvitationMessageUser', renderParams);
});

router.get('/changepassword', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        userName: "Virendra",
        changePasswordTimeStamp: "20-03-2017 12:45:00",
        operatingSystem: "Windows7",
        browser: "Chrome",
        ipAddress: "127.0.0.1"
    };
    res.render('mailtemplate/changepassword', renderParams);
});
router.get('/forgotpassword', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        meeting_url: "example.com",
        mentorCancellationPolicy: "Cancellation Policy",
        userName: "Virendra",
        verifyURL: "http://example.com"
    };
    res.render('mailtemplate/forgotpassword', renderParams);
});
router.get('/deleteaccount', function (req, res) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: '',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session,
        userName: "Virendra"
    };
    res.render('mailtemplate/deleteaccount', renderParams);
});
module.exports = router;