var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var Busboy = require('busboy');
var os = require('os');
var inspect = require('util').inspect;

var multerSetting = require('./../modules/multerSetting');
var uploadMessageDoc = multer({storage: multerSetting.uploadMessageDoc});

// model
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var messagesModel = require('./../model/messages_model');
var miscFunction = require('./../model/misc_model');
var meetingLogModel = require('./../model/meetinglog_model');

// modules
var frontConstant = require('./../modules/front_constant');
var sendResponse = require('./../modules/sendresponse');

var router = express.Router();


router.post('/otherParticipantDisconnected', function(req, res, callback) {
    userModel.userAPIAuthentication(req, res, function(req, res) {
        meetingLogModel.updateOarticipantDisconnectedLog(req, res, function(req, res) {
            sendResponse.sendJsonResponse(req, res, 200, {}, '', 'DONE');
        });
    });
});

router.post('/updateUserLeavingMeetingWindow', function(req, res, callback) {
    userModel.userAPIAuthentication(req, res, function(req, res) {
        var currentParticipant = req.session.userId;
        var meetingUrlKey = req.body.meeting_key;
        var participantMeetingLogMasterId = req.body.participantMeetingLogMasterId;
        var participantMeetingLogId = req.body.participantMeetingLogId;
        var disconnect_reason = req.body.disconnect_reason;

        var objParams = {
            meetingUrlKey: meetingUrlKey,
            participantMeetingLogMasterId: participantMeetingLogMasterId,
            participantMeetingLogId: participantMeetingLogId,
            disconnect_reason: disconnect_reason
        };
        meetingLogModel.updateCurrentUserLeavingMeetingWindow(req, res, objParams, function(req, res, objResponse){
            var dataToSend = {
                updated : objResponse
            }
            sendResponse.sendJsonResponse(req, res, 200, dataToSend, '', 'DONE');
        });
    });
});

router.post('/updateUserNotLeavingMeetingWindow', function(req, res, callback) {
    userModel.userAPIAuthentication(req, res, function(req, res) {
        var currentParticipant = req.session.userId;
        var meetingUrlKey = req.body.meeting_key;
        var participantMeetingLogMasterId = req.body.participantMeetingLogMasterId;
        var participantMeetingLogId = req.body.participantMeetingLogId;
        var disconnect_reason = req.body.disconnect_reason;

        var objParams = {
            meetingUrlKey: meetingUrlKey,
            participantMeetingLogMasterId: participantMeetingLogMasterId,
            participantMeetingLogId: participantMeetingLogId,
            disconnect_reason: disconnect_reason
        };
        meetingLogModel.updateCurrentUserNOTLeavingMeetingWindow(req, res, objParams, function(req, res, objResponse){
            var dataToSend = {
                updated : objResponse
            }
            sendResponse.sendJsonResponse(req, res, 200, dataToSend, '', 'DONE');
        });
    });
});

router.get('/getMeetingLog', function(req, res, callback) {
    var meetingUrlKey = req.query.meeting_key;

    userModel.userAPIAuthentication(req, res, function(req, res) {
        var objData = {
            meetingUrlKey: meetingUrlKey
        };
        meetingLogModel.getMeetingLogsDataForSessionFromMeetingUrlKey(req, res, objData, function(req, res, objMeetingLogData){
            sendResponse.sendJsonResponse(req, res, 200, objMeetingLogData, 0, 'DONE');
        });
    });
});

router.get('/getMeetingTimerInfo', function(req, res, callback) {
    var meetingUrlKey = req.query.meeting_key;

    var objParams = {
        meetingUrlKey: meetingUrlKey
    };

    meetingLogModel.getMeetingTimerInfo(req, res, objParams, function(req, res, objMeetingTimerInfo) {
        sendResponse.sendJsonResponse(req, res, 200, objMeetingTimerInfo, 0, 'DONE');
    });
});


router.post('/saveChatMessageToDB', function(req, res, callback) {

    userModel.userAPIAuthentication(req, res, function(req, res) {
        var reqBody = req.body;
        var message = reqBody.message;
        var dateTimeStamp = reqBody.dateTimeStamp;
        var chatMasterId = reqBody.chatMasterId;
        var meetingDetailId = reqBody.meetingDetailId;
        var meetingUrlKey = reqBody.meetingUrlKey;

        var objDataToSaveChatMessage = {
            message : message,
            dateTimeStamp : dateTimeStamp,
            chatMasterId : chatMasterId,
            meetingDetailId : meetingDetailId,
            meetingUrlKey: meetingUrlKey
        };

        meetingLogModel.saveChatMessageToDB(req, res, objDataToSaveChatMessage, function(req, res, objChatMessageInsertData) {
            sendResponse.sendJsonResponse(req, res, 200, objChatMessageInsertData, 0, 'DONE');
        });
    });

});

router.get('/meetingDisconnect/:participantMeetingLogMasterId/:participantMeetingLogId/:meetingUrlKey', function(req, res, callback){
    //res.send("asdfasdf");
    var participantMeetingLogMasterId = req.params.participantMeetingLogMasterId;
    var participantMeetingLogId = req.params.participantMeetingLogId;
    var meetingUrlKey = req.params.meetingUrlKey;

    var dataParams = {
        participantMeetingLogMasterId: participantMeetingLogMasterId,
        participantMeetingLogId: participantMeetingLogId,
        meetingUrlKey : meetingUrlKey,
        disconnect_reason: "wissenx_issue"
    }

    meetingLogModel.updateUsersMeetingDisconnectLog(req, res, dataParams, function(req, res, flagUpdated){
        res.redirect('/login');
    });
});

module.exports = router;
