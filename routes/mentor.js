var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var Busboy = require('busboy');
var os = require('os');
var inspect = require('util').inspect;
var async = require("async");
var async2 = require("async");
var md5 = require('md5');
var app = express();
var OpenTok = require('opentok');
var paypal = require('paypal-rest-sdk');
var constants = require('./../modules/constants');
// model
var mailer = require('./../modules/mailer.js');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var miscFunction = require('./../model/misc_model');
var meetingLogModel = require('./../model/meetinglog_model');
// modules
var encdec = require('./../modules/encdec');
var frontConstant = require('./../modules/front_constant');
var constant = require('./../modules/constants');
var multerSetting = require('./../modules/multerSetting');
var handlers = require('./../modules/handlers');
var sendResponse = require('./../modules/sendresponse');
var opentok = new OpenTok(constant.TOKBOX_API_KEY, constant.TOKBOX_SECRET_KEY);
var upload = multer({storage: multerSetting.defaultStorage});
var keynoteUpload = multer({storage: multerSetting.keynoteStorage});
var workfolioStorage = multer({storage: multerSetting.workfolioStorage});
var router = express.Router();
var moment = require('moment');
router.get('/startMeeting', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.checkUserAuthenticationForMeeting(req, res, {userId: req.session.userId, meetingUrlKey: req.query.meetingId}, function (req, res, flagUserAuthForMeeting) {
                if (flagUserAuthForMeeting) {
                    mentorModel.checkSessionForMeeting(req, res, req.query.meetingId, '', function (req, res, meetingSessionData) {
                        if (meetingSessionData.session_id == 0) {
                            opentok.createSession(function (err, session) {
                                if (err) {
                                    throw err;
                                }
                                var sessionId = session.sessionId;
                                mentorModel.insertSessionForMeeting(req, res, sessionId, function (req, res, meetingSessionData) {
                                    if (meetingSessionData.session_id != 0) {
                                        res.redirect('/mentor/startMeeting/' + meetingSessionData.meeting_url_key);
                                    }
                                });
                            });
                        } else {
                            res.redirect('/mentor/startMeeting/' + meetingSessionData.meeting_url_key);
                        }
                    });
                } else {
                    res.redirect('/');
                }
            });
        });
    });
});
router.get('/timertest', function (req, res, callback) {

});
// Create a session and store it in the express app
router.get('/startMeeting/:meeting_key', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {

        var dataParam = {
            userId: req.session.userId,
            meetingUrlKey: req.params.meeting_key
        };
        async.parallel([
            // function call to check meeting time
            function (parallelCallback) {
                meetingLogModel.checkMeetingTimeConstraintForMeeting(req, res, dataParam, function (req, res, arrResponseData) {
                    parallelCallback(null, arrResponseData);
                });
            },
            // function call to get loggedIn user data
            function (parallelCallback) {
                userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
                    parallelCallback(null, userData);
                });
            },
            // function call to check user is authorize for meeting.
            function (parallelCallback) {
                mentorModel.checkUserAuthenticationForMeeting(req, res, dataParam, function (req, res, flagUserAuthForMeeting) {
                    parallelCallback(null, flagUserAuthForMeeting);
                });
            },
            // function call to check multiple device check.
            function (parallelCallback) {
                meetingLogModel.authorizeFromMeetingLog(req, res, dataParam, function (req, res, objMeetingLogData) {
                    parallelCallback(null, objMeetingLogData);
                });
            },
            // function call to check both user and mentor got disconnected, 
            // due to technical issue with wissenX
            function (parallelCallback) {
                meetingLogModel.checkAnyTechnicalIssueForMeetingForSourceAdvisor(req, res, dataParam, function (req, res, objTechnicalIssueStatus) {
                    parallelCallback(null, objTechnicalIssueStatus);
                });
            },
            // function call to get user meeting details
            function (parallelCallback) {
                mentorModel.checkSessionForMeeting(req, res, '', req.params.meeting_key, function (req, res, meetingSessionData) {
                    parallelCallback(null, meetingSessionData);
                });
            },
            // function call to get meeting user details
            function (parallelCallback) {
                mentorModel.getMeetingUserDetails_model(req, res, req.params.meeting_key, function (req, res, meetingMentorData) {
                    parallelCallback(null, meetingMentorData);
                });
            },
            // function call to get opposite user data
            function (parallelCallback) {
                meetingLogModel.getOppositeUserIdForMeeting(req, res, dataParam, function (req, res, objReturnData) {
                    var oppositeUserId = objReturnData.userId;
                    var userTypeForLoggedInMeeting = objReturnData.userTypeForLoggedInMeeting;
                    if (userTypeForLoggedInMeeting == 'user') {
                        userModel.getUserInformation(req, res, oppositeUserId, {}, function (req, res, userId, userData, userParams) {
                            userData.userTypeForLoggedInMeeting = userTypeForLoggedInMeeting;
                            userData.finalProfileImageUrl = userData.fullProfileImageURL;
                            userData.first_name = userData.name;
                            userData.profession = '';
                            userData.country_name = userData.countryName;
                            userData.company_name = '';
                            parallelCallback(null, userData);
                        });
                    } else {
                        mentorModel.getMentorDetails(req, res, oppositeUserId, function (req, res, mentorId, objMentorData) {
                            objMentorData.userTypeForLoggedInMeeting = userTypeForLoggedInMeeting;
                            var arrMentorData = [objMentorData];
                            userModel.setImagePathOfAllUsers(req, res, arrMentorData, 0, [], function (req, res, objDataWithImage) {
                                parallelCallback(null, objDataWithImage[0]);
                            });
                        });
                    }
                });
            },
            // function call to get chat message records
            function (parallelCallback) {
                meetingLogModel.getChatMasterLogInfoForMeeting(req, res, dataParam, function (req, res, objChatMasterLogInfo) {
                    var objDataToFetchChatConversations = {
                        meetingChatMasterId: objChatMasterLogInfo.meetingChatMasterId
                    };
                    meetingLogModel.getChatConversations(req, res, objDataToFetchChatConversations, function (req, res, objChatConversations) {
                        var objResponseForChatConversations = {
                            chatMasterInfo: objChatMasterLogInfo,
                            chatConversations: objChatConversations
                        };
                        parallelCallback(null, objResponseForChatConversations);
                    });
                });
            }
        ],
                function (error, arrResultSet) {
                    var objMeetingTimeConstraintForMeeting = arrResultSet[0];
                    var objUserInformation = arrResultSet[1];
                    var objUserAuthenticationForMeeting = arrResultSet[2];
                    var objAuthorizeFromMeetingLog = arrResultSet[3];
                    var objAnyTechnicalIssueForMeetingForSourceAdvisor = arrResultSet[4];
                    var objSessionForMeeting = arrResultSet[5];
                    var objMeetingUserDetails_model = arrResultSet[6];
                    var objOppositeUserData = arrResultSet[7];
                    var objChatMasterLogInfo = arrResultSet[8];

                    var flagMeetingStart = false;
                    var errorMsg = [];
                    var flagMeetingCanStart = objMeetingTimeConstraintForMeeting[0].flagMeetingCanStart;
                    var flagMeetingTotalEnd = objMeetingTimeConstraintForMeeting[0].flagMeetingTotalEnd;
                    var participantMeetingLogMasterId = objAuthorizeFromMeetingLog.participantMeetingLogMasterId;
                    var participantMeetingLogId = objAuthorizeFromMeetingLog.participantMeetingLogId;
                    var userTypeForLoggedInMeeting = objOppositeUserData.userTypeForLoggedInMeeting;

                    // flagMeetingStart = true;
                    // var token = opentok.generateToken(objSessionForMeeting.session_id);
                    // var dataParamsToStartMeeting = {
                    //     flagMeetingStart: flagMeetingStart,
                    //     errorMsg: errorMsg,
                    //     userData: objUserInformation,
                    //     token: token,
                    //     sessionId: objSessionForMeeting.session_id,
                    //     mentorData: objMeetingUserDetails_model[0],
                    //     meetingKey: req.params.meeting_key,
                    //     participantMeetingLogMasterId: participantMeetingLogMasterId,
                    //     participantMeetingLogId: participantMeetingLogId,
                    //     objOppositeUserData: objOppositeUserData,
                    //     objChatMasterLogInfo: objChatMasterLogInfo
                    // };

                    // proceedToStartMeeting(req, res, dataParamsToStartMeeting);

                    // if valid user tries to join the meeting
                    if (objUserAuthenticationForMeeting) {
                        // check for meeting start time
                        if (flagMeetingCanStart == "1") {
                            // check if meeting time isn't over
                            if (flagMeetingTotalEnd == "0") {
                                //check for user/mentor no show
                                if (objAuthorizeFromMeetingLog.flagNoShow == false) {
                                    // check for multi device scenario.
                                    if (objAuthorizeFromMeetingLog.allowAccess == true) {

                                        if (objSessionForMeeting == "0") {
                                            errorMsg = [
                                                "The Meeting Key has changed, please click on the link received in your email."
                                            ];

                                            var dataParamsToStartMeeting = {
                                                flagMeetingStart: flagMeetingStart,
                                                errorMsg: errorMsg,
                                                userData: objUserInformation,
                                                meetingKey: req.params.meeting_key,
                                                participantMeetingLogMasterId: participantMeetingLogMasterId,
                                                participantMeetingLogId: participantMeetingLogId
                                            };
                                            proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                                        } else {
                                            flagMeetingStart = true;
                                            var token = opentok.generateToken(objSessionForMeeting.session_id);
                                            var dataParamsToStartMeeting = {
                                                flagMeetingStart: flagMeetingStart,
                                                errorMsg: errorMsg,
                                                userData: objUserInformation,
                                                token: token,
                                                sessionId: objSessionForMeeting.session_id,
                                                mentorData: objMeetingUserDetails_model[0],
                                                meetingKey: req.params.meeting_key,
                                                participantMeetingLogMasterId: participantMeetingLogMasterId,
                                                participantMeetingLogId: participantMeetingLogId,
                                                objOppositeUserData: objOppositeUserData,
                                                objChatMasterLogInfo: objChatMasterLogInfo
                                            };

                                            proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                                        }
                                    } else {
                                        errorMsg = [
                                            "You may have already entered the meeting room via another device.",
                                            "Currently SourceAdvisor is designed to handle only one-on-one meetings. Later versions will have capabilities of handling multiple parties entering a meeting room."
                                        ];
                                        var dataParamsToStartMeeting = {
                                            flagMeetingStart: flagMeetingStart,
                                            errorMsg: errorMsg,
                                            userData: objUserInformation,
                                            meetingKey: req.params.meeting_key
                                        };
                                        proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                                    }
                                } else {
                                    errorMsg = [
                                        "Sorry You are too late to join this meating as 10 minutes of grace period is over. You will be count as No Show for this meeting. Kindly contact administrator if have any queries."
                                    ];

                                    var dataParamsToStartMeeting = {
                                        flagMeetingStart: flagMeetingStart,
                                        errorMsg: errorMsg,
                                        userData: objUserInformation,
                                        meetingKey: req.params.meeting_key
                                    };
                                    proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                                }
                            } else {
                                errorMsg = [
                                    "The meeting is already Ended, either by you or the one you were supposed to meet.",
                                    "Meeting Time has expired."
                                ];
                                var dataParamsToStartMeeting = {
                                    flagMeetingStart: flagMeetingStart,
                                    errorMsg: errorMsg,
                                    userData: objUserInformation,
                                    meetingKey: req.params.meeting_key
                                };
                                proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                            }
                        } else {
                            errorMsg = [
                                "The Link provided permits you to enter the meeting room.   The Meeting room opens 15 minutes prior to scheduled start time and closes 15 after the scheduled end time. Please try to enter the meeting room with this time period.",
                                "Kindly note 'no show' policy  kicks-in if you do not enter the meeting room for 10 minutes from the scheduled start time of your meeting."
                            ];
                            var dataParamsToStartMeeting = {
                                flagMeetingStart: flagMeetingStart,
                                errorMsg: errorMsg,
                                userData: objUserInformation,
                                meetingKey: req.params.meeting_key
                            };
                            proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                        }
                    } else {
                        errorMsg = [
                            "You are not authorized to join this meeting.", "Once you end a meeting, you cannot re-enter the meeting room."
                        ];
                        var dataParamsToStartMeeting = {
                            flagMeetingStart: flagMeetingStart,
                            errorMsg: errorMsg,
                            userData: objUserInformation,
                            meetingKey: req.params.meeting_key
                        };
                        proceedToStartMeeting(req, res, dataParamsToStartMeeting);
                    }
                });
    });
});
var proceedToStartMeeting = function (req, res, objParams) {
    if (objParams.flagMeetingStart) {
        var renderParams = {
            layout: 'mentor_after_login_layout',
            frontConstant: frontConstant,
            apiKey: constant.TOKBOX_API_KEY,
            sessionId: objParams.sessionId,
            token: objParams.token,
            userData: objParams.userData,
            arrSessionData: req.session,
            mentorData: objParams.mentorData,
            meetingKey: objParams.meetingKey,
            bodyClass: 'chat',
            hidCommonFilter: 1,
            participantMeetingLogMasterId: objParams.participantMeetingLogMasterId,
            participantMeetingLogId: objParams.participantMeetingLogId,
            objOppositeUserData: objParams.objOppositeUserData,
            objChatMasterLogInfo: objParams.objChatMasterLogInfo
        };
        res.render('video_call', renderParams);
    } else {
        var renderParams = {
            layout: 'mentor_after_login_layout',
            frontConstant: frontConstant,
            userData: objParams.userData,
            arrSessionData: req.session,
            meetingKey: objParams.meetingKey,
            bodyClass: 'showNavRed',
            errorMsg: objParams.errorMsg
        };
        res.render('video_call_invalid', renderParams);
    }
};
router.get('/checkmeetinglog', function (req, res, callback) {
    var dataToSend = {
        meetingUrlKey: 'yZCpJiCMRFCs'
    };
    meetingLogModel.getMeetingLogFromMeetingUrlKey(req, res, dataToSend, function (req, res, objMeetingLogData) {
        res.send(objMeetingLogData);
    });
});
router.post('/checkMeetingSession', function (req, res, callback) {
    //    userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.checkMeetingSession_model(req, res, req.body.meeting_id, function (req, res, meetingData) {
        if (meetingData == 0) {
            sendResponse.sendJsonResponse(req, res, 200, {}, '', 'CONTINUE');
        } else {
            sendResponse.sendJsonResponse(req, res, 200, meetingData, '', 'END');
        }
        //        });
    });
});
router.post('/updateVideoSessionElapsedTime', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.updateVideoSessionElapsedTime_model(req, res, req.body.elapsed_time, req.body.meeting_key, {}, function (req, res) {

        });
    });
});
router.post('/endMeetingSession', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.endMeetingSession_model(req, res, function (req, res) {

        });
    });
});
router.post('/startMeetingSession', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.startMeetingSession_model(req, res, function (req, res) {

        });
    });
});
router.post('/getUserDetail', function (req, res, callback) {
    var data = {};
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.getMeetingInformation(req, res, req.body.meeting_key, function (req, res, meetinginfo) {
                data = {
                    userData: userData,
                    meetingInfo: meetinginfo
                };
                sendResponse.sendJsonResponse(req, res, 200, data, 0, 'success');
            });
        });
    });
});
router.get('/transactions', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed'
            };
            res.render('mentors/transactions', renderParams);
        });
    });
});
router.get('/meeting_invites', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            miscFunction.getTimeZoneList(req, res, '', function (req, res, timeZoneList) {
                miscFunction.chkComplainceTest(req, res, req.session.userId, function (req, res, complainceTest) {
                    var errors = req.flash('errors');
                    var messages = req.flash('messages');
                    var complainceTestPassed = 0;
                    if (typeof complainceTest.is_compliance_passed != "undefined") {
                        complainceTestPassed = complainceTest.is_compliance_passed;
                    }
                    var renderParams = {
                        layout: 'mentor_after_login_layout',
                        errors: errors,
                        messages: messages,
                        frontConstant: frontConstant,
                        userData: userData,
                        arrSessionData: req.session,
                        timeZoneList: timeZoneList,
                        complainceTest: complainceTestPassed
                    };
                    res.render('mentors/meeting_invites', renderParams);
                });
            });
        });
    });
});
router.post('/getMeetingPendingInvites', function (req, res, callback) {
    var data = {};
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.getMeetingPendingInvites_model(req, res, '', 'pending', function (req, res, pendingInvites) {
            sendResponse.sendJsonResponse(req, res, 200, pendingInvites, 0, 'SUCCESS');
        });
    });
});
/* GET become Mentor Step 1. */
router.get('/become_a_mentor', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed'
            };
            res.render('mentors/become_a_mentor', renderParams);
        });
    });
});
/* Update Become a Mentor */
router.post('/become_a_mentor_update', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.update_become_a_mentor(req, res, function (req, res) {
            if (req.body.bcm_mentor == 'mentor') {
                //req.session.userType = 'mentor';
                req.session.save(function (err) {
                    //res.redirect('/mentor/step1');
                    res.redirect('/mentor/become-a-mentor');
                });
            } else if (req.body.bcm_mentor == 'organisation') {
                //req.session.userType = 'organisation';
                req.session.save(function (err) {
                    res.redirect('/mentor/step2');
                });
            }
        });
    });
});

router.get('/become-a-mentor', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {

        async.parallel([
            // function calll to getUserInformation
            function (becomeAMentorCallback) {
                userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
                    becomeAMentorCallback(null, userData);
                });
            },
            // function call to get featured mentor list
            function (becomeAMentorCallback) {
                mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
                    becomeAMentorCallback(null, arrMentorData);
                });
            },
            // function call to get polici details
            function (becomeAMentorCallback) {
                miscFunction.getCancellationPolicies(req, res, function (req, res, arrCancellationPolicyData) {
                    becomeAMentorCallback(null, arrCancellationPolicyData);
                });
            },
            function (becomeAMentorCallback) {
                miscFunction.getIndustryListFromDb(req, res, function (req, res, industriesList) {
                    becomeAMentorCallback(null, industriesList);
                });
            },
            function (becomeAMentorCallback) {
                miscFunction.getSubIndustryListFromDb(req, res, function (req, res, subIndustriesList) {
                    becomeAMentorCallback(null, subIndustriesList);
                });
            },
            function (becomeAMentorCallback) {
                miscFunction.getDomainListFromDb(req, res, function (req, res, domainList) {
                    becomeAMentorCallback(null, domainList);
                });
            },
            function (becomeAMentorCallback) {
                miscFunction.getRegionListFromDb(req, res, function (req, res, regionList) {
                    becomeAMentorCallback(null, regionList);
                });
            },
            function (becomeAMentorCallback) {
                miscFunction.getCountryList(req, res, "0", function (req, res, countryList) {
                    becomeAMentorCallback(null, countryList);
                });
            }
        ], function (error, resultSet) {
            var userData = resultSet[0];
            var arrMentorData = resultSet[1];
            var arrCancellationPolicyData = resultSet[2];

            var industriesList = resultSet[3];
            var subIndustriesList = resultSet[4];
            var domainList = resultSet[5];
            var regionList = resultSet[6];
            var countryList = resultSet[7];

            var masterData = {
                industriesList: industriesList,
                subIndustriesList: subIndustriesList,
                domainList: domainList,
                regionList: regionList,
                countriesList: countryList
            };

            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                arrMentorData: arrMentorData,
                arrCancellationPolicyData: arrCancellationPolicyData,
                masterData: masterData
            };

            res.render('mentors/become_a_mentor_tabbed', renderParams);

        });

        // userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
        //     mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
        //         miscFunction.getCancellationPolicies(req, res, function (req, res, arrCancellationPolicyData) {
        //             var errors = req.flash('errors');
        //             var messages = req.flash('messages');
        //             var renderParams = {
        //                 layout: 'mentor_after_login_layout',
        //                 errors: errors,
        //                 messages: messages,
        //                 frontConstant: frontConstant,
        //                 userData: userData,
        //                 arrSessionData: req.session,
        //                 arrMentorData: arrMentorData,
        //                 arrCancellationPolicyData: arrCancellationPolicyData
        //             };
        //             res.render('mentors/become_mentor_1', renderParams);
        //         });
        //     });
        // });
    });
});
/* GET become Mentor Step 1. */
router.get('/step1', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
                miscFunction.getCancellationPolicies(req, res, function (req, res, arrCancellationPolicyData) {
                    var errors = req.flash('errors');
                    var messages = req.flash('messages');
                    var renderParams = {
                        layout: 'mentor_after_login_layout',
                        errors: errors,
                        messages: messages,
                        frontConstant: frontConstant,
                        userData: userData,
                        arrSessionData: req.session,
                        arrMentorData: arrMentorData,
                        arrCancellationPolicyData: arrCancellationPolicyData
                    };
                    res.render('mentors/become_mentor_1', renderParams);
                });
            });
        });
    });
});
/* Update Become Mentor Step 1*/
router.post('/step1_update', function (req, res, callback) {
    mentorModel.update_become_mentor_step_1(req, res, function (req, res) {
        if (req.xhr) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify({
                "status": "success",
                "message": "Commercial Data Updated Successfully"
            }));
            res.end();
        } else {
            res.redirect('/mentor/step2');
        }
    });
    return;
});
//Upload Recorded Keynote video to server
router.post('/mentorRecordKeynoteVideo', function (req, res, callback) {
    var form = new formidable.IncomingForm();
    form.multiples = true;
    form.uploadDir = path.join(__dirname, '../uploads');
    form.on('file', function (field, file) {
        fs.rename(file.path, path.join(form.uploadDir, file.name));
    });
    form.on('error', function (err) {
        console.log('An error has occured: \n' + err);
    });
    form.on('end', function () {
        res.end('success');
    });
    form.parse(req);
});
/* GET become Mentor Step 2. */
router.get('/step2', function (req, res, callback) {
    var masterData = '';
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            miscFunction.getIndustryListFromDb(req, res, function (req, res, industriesList) {
                miscFunction.getSubIndustryListFromDb(req, res, function (req, res, subIndustriesList) {
                    miscFunction.getDomainListFromDb(req, res, function (req, res, domainList) {
                        miscFunction.getRegionListFromDb(req, res, function (req, res, regionList) {
                            miscFunction.getCountryList(req, res, "0", function (req, res, countriesList) {
                                mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
                                    masterData = {
                                        industriesList: industriesList,
                                        subIndustriesList: subIndustriesList,
                                        domainList: domainList,
                                        regionList: regionList,
                                        countriesList: countriesList,
                                        arrMentorData: arrMentorData
                                    };
                                    var errors = req.flash('errors');
                                    var messages = req.flash('messages');
                                    var renderParams = {
                                        layout: 'mentor_after_login_layout',
                                        errors: errors,
                                        messages: messages,
                                        frontConstant: frontConstant,
                                        userData: userData,
                                        masterData: masterData,
                                        arrSessionData: req.session
                                    };
                                    res.render('mentors/become_mentor_2', renderParams);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
/* Update Become Mentor Step 2*/
router.post('/step2_update', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.update_become_mentor_step_2(req, res, function (req, res) {
            if (req.xhr) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "status": "success",
                    "message": "Professional Profile Data Updated Successfully"
                }));
                res.end();
            } else {
                res.redirect('/mentor/step3');
            }
        });
    });
    return;
});
router.get('/updateUserToMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.updateUserToMentor(req, res, function (req, res, flagResponse) {
            sendResponse.sendJsonResponse(req, res, 200, {flagResponse: flagResponse}, "0", "SUCCESS");
        });
    });
});
/* Update Organization Tag for Mentor */
router.post('/updateOrganizationTag', function (req, res, callback) {
    mentorModel.updateOrganizationTag_model(req, res, function (req, res) { });
    return;
});
/* GET become Mentor Step 3 */
router.get('/step3', function (req, res, callback) {

    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');
                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    userData: userData,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData
                };
                res.render('mentors/become_mentor_3', renderParams);
            });
        });
    });
});
/* GET become Mentor Step 4. */
router.get('/complianceTest', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.featuredMentorProfile(req, res, req.session.userId, 4, function (req, res, arrMentorData) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');
                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    userData: userData,
                    arrSessionData: req.session,
                    arrMentorData: arrMentorData
                };
                res.render('mentors/complianceTest', renderParams);
            });
        });
    });
});
/**
 * Mentor Messages Screen
 */
router.get('/messages', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed'
            };
            res.render('messages', renderParams);
        });
    });
});
router.post('/messages', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed',
                selectedConversationID: req.body.hdn_conversation_id
            };
            res.render('messages', renderParams);
        });
    });
});
/* Get Mentor Linkedin data from DB */
router.post('/get_mentor_linkedin_data', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.get_mentor_linkedin_data_model(req, res, function (req, res) {

        });
    });
});
/* DELETE Mentor Workfolio data from DB */
router.post('/delete_workfolio_items', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.delete_workfolio_items_model(req, res, function (req, res) { });
    });
});
/* SAVE Mentor Workfolio data to DB */
router.post('/add_workfolio_items', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.add_workfolio_items_model(req, res, function (req, res) {

        });
    });
});
/* SAVE Mentor Workfolio Uploaded video data to DB */
router.post('/add_workfolio_uploads', workfolioStorage.single('upload_video'), function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.add_workfolio_items_model(req, res, function (req, res) {

        });
    });
});
/* Save Employment History data to DB */
router.post('/save_employment_history', function (req, res, callback) {
    //    userModel.userAuthentication(req, res, function (req, res) {
    mentorModel.save_employment_history_model(req, res, function (req, res) {

    });
    //    });
});
/* Save Employment History data to DB */
router.post('/save_mentor_expertise', function (req, res, callback) {
    //    userModel.userAuthentication(req, res, function (req, res) {
    mentorModel.save_mentor_expertise_model(req, res, function (req, res) {

    });
    //    });
});
/* Save Youtube Keynote Video data to DB */
router.post('/addKeynoteYoutube', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.addKeynoteVideo_model(req, res);
    });
});
/* Save Recorded Keynote Video data to DB */
router.post('/addKeynoteRecord', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.addKeynoteVideo_model(req, res);
    });
});
/* Save Uploaded Keynote Video data to DB */
router.post('/addKeynoteUpload', keynoteUpload.single('upload_video'), function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.addKeynoteVideo_model(req, res);
    });
});
/* Remove Keynote Video data From DB */
router.post('/removeKeynoteVideo', function (req, res, callback) {
    mentorModel.removeKeynoteVideo_model(req, res);
});

/* GET mentor calendar. */
router.get('/calendar', function (req, res, callback) {

    // Mentor Timezone
    var mentorTimezone = req.session.timezone;
    if (mentorTimezone == '') {
        mentorTimezone = constants.DEFAULT_TIMEZONE;
    }
    userModel.userAuthentication(req, res, function (req, res) {
        miscFunction.getTimeZoneList(req, res, '', function (req, res, timeZoneList) {
            miscFunction.chkComplainceTest(req, res, req.session.userId, function (req, res, complainceTest) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');
                var errors = req.flash('errors');
                var messages = req.flash('messages');
                var complainceTestPassed = 0;
                if (typeof complainceTest.is_compliance_passed != "undefined") {
                    complainceTestPassed = complainceTest.is_compliance_passed;
                }

                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    timeZoneId: mentorTimezone,
                    timeZoneList: timeZoneList,
                    complainceTest: complainceTestPassed
                };
                res.render('mentors/mentorcalender', renderParams);
            });
        });
    });
});
/* Get High Profile Mentor*/
router.post('/high_mentor_profiles', function (req, res) {

});
/* Send private invitation to user */
router.post('/send_private_invitation', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.send_user_private_invite(req, res);
    });
});
/* Get user list for private invitation dropdown */
router.post('/userList', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.getUserListApi(req, res);
    });
});
/* Delete Mentor Expertise */
router.post('/deleteMentorExpertise', function (req, res) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.deleteMentorExpertise_model(req, res, req.body.expertise_id);
    //});
});
/* Delete Mentor Expertise */
router.post('/deleteMentorEmpHistory', function (req, res) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.deleteMentorEmpHistory_model(req, res, req.body.emp_history_id);
    //});
});
/* Get Employment History data from DB */
router.post('/getMentorExpertise', function (req, res, callback) {
    //    userModel.userAuthentication(req, res, function (req, res) {
    mentorModel.getMentorExpertise(req, res, req.body.userId, req.body.expertise_id, function (req, res, userId, mentorExpertise) {
        var data = {};
        data.mentorExpertise = mentorExpertise;
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": data,
            "error": "0",
            "message": "success"
        }));
        res.end();
    });
    //    });
});
/* Get Employment History data from DB */
router.post('/getMentorEmpHistory', function (req, res, callback) {
    //    userModel.userAuthentication(req, res, function (req, res) {
    mentorModel.getMentorEmploymenthistory(req, res, req.body.userId, req.body.emp_history_id, function (req, res, userId, mentorEmpHistory) {
        var data = {};
        data.mentorEmpHistory = mentorEmpHistory;
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": data,
            "error": "0",
            "message": "success"
        }));
        res.end();
    });
    //    });
});
/* Get Mentor Full data from DB*/
router.post('/getMentorWorkfolio', function (req, res) {
    var data = {};
    mentorModel.getMentorWorkfolioDetail(req, res, req.body.userId, req.body.industry_id, function (req, res, userId, mentorExpertiseWorkfolio) {
        data.mentorExpertiseWorkfolio = mentorExpertiseWorkfolio;
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "data": data,
            "error": "0",
            "message": "success"
        }));
        res.end();
    });
});
router.post('/getMentorFullData', function (req, res) {
    var data = {};
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.getMentorDetails(req, res, req.body.userId, function (req, res, userId, mentorDetails) {
                mentorModel.getMentorExpertise(req, res, req.body.userId, '', function (req, res, userId, mentorExpertise) {
                    mentorModel.getMentorExpertiseWorkfolio(req, res, req.body.userId, '', function (req, res, userId, mentorExpertiseWorkfolio) {
                        mentorModel.getMentorEmploymenthistory(req, res, req.body.userId, '', function (req, res, userId, mentorEmpHistory) {
                            mentorModel.getMentorOrganizationTag(req, res, req.body.userId, '', function (req, res, userId, expertiseId, mentorOrgTags) {
                                data.userData = userData;
                                data.mentor_details = mentorDetails;
                                data.mentorExpertise = mentorExpertise;
                                data.mentorExpertiseWorkfolio = mentorExpertiseWorkfolio;
                                data.mentorEmpHistory = mentorEmpHistory;
                                data.mentorOrgTags = mentorOrgTags;
                                res.writeHead(200, {'Content-Type': 'application/json'});
                                res.write(JSON.stringify({
                                    "data": data,
                                    "error": "0",
                                    "message": "success"
                                }));
                                res.end();
                            });
                        });
                    });
                });
            });
        });
    });
});
/* Get Mentor listing */
router.get('/listing', function (req, res, callback) {
    var errors = req.flash('errors');
    var messages = req.flash('messages');
    var flagUserLoggedIn = false;
    var userId = "0";
    var reqQuery = req.query;
    if (req.session.userId) {
        flagUserLoggedIn = true;
        userId = req.session.userId;
    }

    var strQueryParams = JSON.stringify(reqQuery);
    var renderParams = {
        layout: 'mentorlisting',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        constant: constant,
        flagUserLoggedIn: flagUserLoggedIn,
        userId: userId,
        dataParams: strQueryParams,
        arrSessionData: req.session,
        hidGlobalSearch: "1"
    };
    res.render('mentors/listing', renderParams);
    /*var hidSearchFrom = reqQuery.hidSearchFrom;
     var txtLocation = reqQuery.txtLocation;
     var hidLocationType = reqQuery.hidLocationType;
     var hidLocationId = reqQuery.hidLocationId;
     var txtQueryString = reqQuery.txtQueryString;
     var hidTagSelected = reqQuery.hidTagSelected;
     
     txtQueryString = decodeURI(txtQueryString);
     
     if (typeof txtLocation == 'undefined') {
     txtLocation = "";
     }
     if (typeof hidLocationType == 'undefined') {
     hidLocationType = "";
     }
     if (typeof hidLocationId == 'undefined') {
     hidLocationId = "0";
     }
     if (typeof txtQueryString == 'undefined') {
     txtQueryString = "";
     }
     if (typeof hidTagSelected == 'undefined') {
     hidTagSelected = "";
     }
     
     
     var dataParams = {
     txtLocation: txtLocation,
     hidLocationType: hidLocationType,
     hidLocationId: hidLocationId,
     txtQueryString: txtQueryString,
     hidTagSelected: hidTagSelected
     };
     
     mentorModel.getQueryStringJsonForTokenInput(req, res, dataParams, function (req, res, arrTokensString) {
     
     var locationTokenString = {};
     
     if (!isNaN(reqQuery.hidLocationId) && reqQuery.hidLocationId > 0) {
     locationTokenString = {
     id: "LOCATION_" + reqQuery.hidLocationType + "_" + reqQuery.hidLocationId,
     value: reqQuery.txtLocation,
     typeX: "LOCATION"
     };
     arrTokensString.push(locationTokenString);
     }
     
     var renderParams = {
     layout: 'mentorlisting',
     errors: errors,
     messages: messages,
     frontConstant: frontConstant,
     constant: constant,
     flagUserLoggedIn: flagUserLoggedIn,
     userId: userId,
     dataParams: dataParams,
     arrSessionData: req.session,
     arrTokensString: arrTokensString,
     hidGlobalSearch: "1"
     };
     
     res.render('mentors/listing', renderParams);
     });*/

});
router.post('/listingSearch', function (req, res, callback) {
    var reqBody = req.body;
    reqBodyP = JSON.parse(reqBody.data);
    var hidLocationType = "";
    var hidLocationId = "0";
    var industryId = "0";
    var subIndustryIds = [];
    var domain = [];
    var regionId = "0";
    var subRegion = [];
    var mentorExpertise = "";
    var countryId = "0";
    var cityId = "0";
    var designation = "";
    var company = "";
    var jobDescription = "";
    var firstName = "";
    var lastName = "";
    var fullName = "";
    var minPriceSelected = "";
    var maxPriceSelected = "";
    var pageNumber = "0";
    //location dataParam set
    if (typeof reqBodyP.location != 'undefined') {
        if ((typeof reqBodyP.location.locationType != 'undefined' && reqBodyP.location.locationType != "") &&
                (typeof reqBodyP.location.locationId != 'undefined' && !isNaN(reqBodyP.location.locationId) && reqBodyP.location.locationId > 0)) {

            hidLocationType = reqBodyP.location.locationType;
            hidLocationId = reqBodyP.location.locationId;
        }
    }

    // industryId Set
    if (typeof reqBodyP.industry != 'undefined') {

        // setting industry Id
        if (typeof reqBodyP.industry.industryId != 'undefined' && !isNaN(reqBodyP.industry.industryId) && reqBodyP.industry.industryId > 0) {
            industryId = reqBodyP.industry.industryId;
        }

        // setting array of subindustries
        if (typeof reqBodyP.industry.arrSubIndustries != 'undefined' && reqBodyP.industry.arrSubIndustries.length > 0) {
            subIndustryIds = reqBodyP.industry.arrSubIndustries;
        }
    }


    // setting domain 
    if (typeof reqBodyP.domain != 'undefined' && reqBodyP.domain.length > 0) {
        domain = reqBodyP.domain;
    }


    // region and sub region id set.
    if (typeof reqBodyP.region != 'undefined') {

        // setting region Id
        if (typeof reqBodyP.region.regionId != 'undefined' && !isNaN(reqBodyP.region.regionId) && reqBodyP.region.regionId > 0) {
            regionId = reqBodyP.region.regionId;
        }

        // setting array of subindustries
        if (typeof reqBodyP.region.arrSubRegionId != 'undefined' && reqBodyP.region.arrSubRegionId.length > 0) {
            subRegion = reqBodyP.region.arrSubRegionId;
        }
    }


    /// setting mentor profile params.
    if (typeof reqBodyP.mentoryProfile != 'undefined') {
        if (typeof reqBodyP.mentoryProfile.mentorFirstName != 'undefined' && reqBodyP.mentoryProfile.mentorFirstName != "") {
            firstName = reqBodyP.mentoryProfile.mentorFirstName;
        }

        if (typeof reqBodyP.mentoryProfile.mentorLastName != 'undefined' && reqBodyP.mentoryProfile.mentorLastName != "") {
            lastName = reqBodyP.mentoryProfile.mentorLastName;
        }

        if (typeof reqBodyP.mentoryProfile.mentorFullName != 'undefined' && reqBodyP.mentoryProfile.mentorFullName != "") {
            fullName = reqBodyP.mentoryProfile.mentorFullName;
        }

        if (typeof reqBodyP.mentoryProfile.mentorExpertise != 'undefined' && reqBodyP.mentoryProfile.mentorExpertise != "") {
            mentorExpertise = reqBodyP.mentoryProfile.mentorExpertise;
        }

        if (typeof reqBodyP.mentoryProfile.mentorDesignation != 'undefined' && reqBodyP.mentoryProfile.mentorDesignation != "") {
            designation = reqBodyP.mentoryProfile.mentorDesignation;
        }

        if (typeof reqBodyP.mentoryProfile.mentoryCompany != 'undefined' && reqBodyP.mentoryProfile.mentoryCompany != "") {
            company = reqBodyP.mentoryProfile.mentoryCompany;
        }

        if (typeof reqBodyP.mentoryProfile.mentorJobDescription != 'undefined' && reqBodyP.mentoryProfile.mentorJobDescription != "") {
            jobDescription = reqBodyP.mentoryProfile.mentorJobDescription;
        }

        if (typeof reqBodyP.mentoryProfile.mentorCountryId != 'undefined' && !isNaN(reqBodyP.mentoryProfile.mentorCountryId) && reqBodyP.mentoryProfile.mentorCountryId > 0) {
            countryId = reqBodyP.mentoryProfile.mentorCountryId;
        }

        if (typeof reqBodyP.mentoryProfile.mentorCityId != 'undefined' && !isNaN(reqBodyP.mentoryProfile.mentorCityId) && reqBodyP.mentoryProfile.mentorCityId > 0) {
            cityId = reqBodyP.mentoryProfile.mentorCityId;
        }
    }


    //price filter param set
    if (typeof reqBodyP.price != 'undefined') {
        if ((typeof reqBodyP.price.minPrice != 'undefined' && !isNaN(reqBodyP.price.minPrice) && reqBodyP.price.minPrice >= 0) &&
                (typeof reqBodyP.price.maxPrice != 'undefined' && !isNaN(reqBodyP.price.maxPrice) && reqBodyP.price.maxPrice >= 0)) {

            minPriceSelected = reqBodyP.price.minPrice;
            maxPriceSelected = reqBodyP.price.maxPrice;
        }
    }

    //page number
    if (typeof reqBodyP.pageNumber != 'undefined') {
        if (typeof reqBodyP.pageNumber != 'undefined' && !isNaN(reqBodyP.pageNumber) && reqBodyP.pageNumber > 0) {
            pageNumber = reqBodyP.pageNumber;
        }
    }

    var sortBy = "SORTBY_PHL";
    if (typeof reqBodyP.sortBy != 'undefined' && reqBodyP.sortBy != "") {
        sortBy = reqBodyP.sortBy;
    }


    var loggedInUserId = "";
    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
        loggedInUserId = req.session.userId;
    }


    var dataParams = {
        hidLocationType: hidLocationType,
        hidLocationId: hidLocationId,
        industryId: industryId,
        subIndustryIds: subIndustryIds,
        domain: domain,
        regionId: regionId,
        subRegion: subRegion,
        mentorExpertise: mentorExpertise,
        countryId: countryId,
        cityId: cityId,
        designation: designation,
        company: company,
        jobDescription: jobDescription,
        firstName: firstName,
        lastName: lastName,
        fullName: fullName,
        minPriceSelected: minPriceSelected,
        maxPriceSelected: maxPriceSelected,
        pageNumber: pageNumber,
        sortBy: sortBy,
        loggedInUserId: loggedInUserId
    };


    miscFunction.checkCountryOrCityActiveOrNot(req, res, dataParams, function (req, res, objResponseData) {

        var validCountryCity = objResponseData.valid;

        validCountryCity = true;


        async.parallel([
            // function call get mentor list to display.
            function (parallelCallback) {

                if (validCountryCity) {
                    mentorModel.getMentorSearhInformation(req, res, dataParams, function (req, res, arrMentorData) {
                        parallelCallback(null, arrMentorData);
                    });
                } else {
                    parallelCallback(null, []);
                }
            },
            // function call to get all data without limit
            function (parallelCallback) {
                if (validCountryCity) {
                    var tempDataParams = dataParams;
                    tempDataParams.queryForAllData = "1";
                    mentorModel.getMentorSearhInformation(req, res, tempDataParams, function (req, res, arrMentorDataWithoutLimit) {
                        parallelCallback(null, arrMentorDataWithoutLimit);
                    });
                } else {
                    parallelCallback(null, []);
                }
            },
            // function call to get industry sidebarfilter
            function (parallelCallback) {
                miscFunction.getIndustriesListForSideBar(req, res, dataParams, function (req, res, arrIndustryInfo) {
                    parallelCallback(null, arrIndustryInfo);
                });
            },
            // function call to get domain sidebarfilter
            function (parallelCallback) {
                miscFunction.getAllDomainInfo(req, res, function (req, res, arrDomainInfo) {
                    parallelCallback(null, arrDomainInfo);
                });
            },
            // function call to get region sidebarfilter
            function (parallelCallback) {
                miscFunction.getCompleteRegionInfo(req, res, dataParams, function (req, res, arrRegionData) {
                    parallelCallback(null, arrRegionData);
                });
            },
            // function call to get price sidebarfilter
            function (parallelCallback) {
                if (validCountryCity) {
                    var tempDataParams = dataParams;
                    tempDataParams.queryForAllData = "1";
                    tempDataParams.priceSideBar = "1";
                    mentorModel.getMentorSearhInformation(req, res, tempDataParams, function (req, res, arrMentorPriceData) {
                        parallelCallback(null, arrMentorPriceData);
                    });
                } else {
                    parallelCallback(null, []);
                }
            },
            // function call to All deleteable token tags.
            function (parallelCallback) {
                miscFunction.getAllTokenInputData(req, res, dataParams, function (req, res, arrTokenData) {
                    parallelCallback(null, arrTokenData);
                });
            }
        ],
                function (error, arrResultSet) {
                    var arrMentorDataToDisplay = arrResultSet[0];
                    var arrMentorAllDataWithoutLimit = arrResultSet[1];
                    var arrIndustrySideBarFilter = arrResultSet[2];
                    var arrDomainSidebarFilter = arrResultSet[3];
                    var arrRegionSidebarFilter = arrResultSet[4];
                    var arrSidebarPriceFilter = arrResultSet[5];
                    var arrTokenTagsToDisplay = arrResultSet[6];
                    var dataToReturn = {
                        arrMentorDataToDisplay: arrMentorDataToDisplay,
                        arrMentorAllDataWithoutLimit: arrMentorAllDataWithoutLimit,
                        objSideBarData: {
                            arrIndustrySideBarFilter: arrIndustrySideBarFilter,
                            arrDomainSidebarFilter: arrDomainSidebarFilter,
                            arrRegionSidebarFilter: arrRegionSidebarFilter,
                            arrSidebarPriceFilter: arrSidebarPriceFilter
                        },
                        arrTokenTagsToDisplay: arrTokenTagsToDisplay,
                        arrDataParams: dataParams,
                        loggedInUserId: loggedInUserId
                    };
                    if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
                        //dataToReturn.loggedInUserId = req. session.userId;
                    }

                    sendResponse.sendJsonResponse(req, res, 200, dataToReturn, "0", "MENTORLISTFETCH");
                });
    });


});
/* Sync google calendar data in the system database */
router.post('/gcalSync', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.gcalSyncApi(req, res);
    //});
});
/* Sync google calendar data in the system database */
router.post('/gcalSyncStep1', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.gcalSyncStep1Api(req, res);
    //});
});
/* Add availability data in the system database */
router.post('/addAvailability', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.addAvailabilityApi(req, res);
    //});
});
router.post('/updateAvailability', function (req, res) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.updateAvailabilityApi(req, res);
    //});
});
/* Get mentor events data from system database */
router.post('/eventList', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorEventData(req, res);
    //});
});
router.post('/insertNewMessage', function (req, res, callback) {
    if (req.body.message_id == "") {
        mentorModel.insertNewConversation_model(req, res, function (req, res) {

        });
    } else {
        mentorModel.insertNewMessage_model(req, res, function (req, res) {

        });
    }
});
/* Get availability data of mentor from the system database */
router.post('/availableData', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorAvailabilityData(req, res);
});
/* Check Slug for mentor */
router.post('/checkSlug', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.checkMentorSlug(req, res);
});
/* Download File */
router.post('/downloadFile', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorDownloadFile(req, res);
});
/* Save Uploaded Keynote Video data to DB */
router.post('/deleteCalEvent', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.deleteCalEvent(req, res);
    });
});
router.post('/deleteAllRecuring', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.deleteAllRecuringById(req, res);
    });
});
router.post('/acceptInvitation', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAPIAuthentication(req, res, function (req, res) {
        if (typeof req.body.transactionId == "undefined" || typeof req.body.invitation_id == "undefined" || typeof req.body.invitation_slot_id == "undefined" || req.body.invitation_id == "" || req.body.invitation_slot_id == "" || req.body.transactionId == "") {
            sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        } else {

            var invitation_id = req.body.invitation_id;
            var invitation_slot_id = req.body.invitation_slot_id;
            var transactionId = req.body.transactionId;
            var amount = req.body.amount;
            var invoiceid = req.body.invoiceid;
            var mentorId = req.body.mentorId;
            var inviteType = 0;

            mentorModel.acceptInvitation(req, res, invitation_id, invitation_slot_id, transactionId, amount, invoiceid, mentorId, inviteType, function (req, res, invitation_data, invitation_slot_data) {
                if (invitation_data != "error" && invitation_slot_data != "error") {

                    var dataToSend = {
                        invitation_data: invitation_data,
                        invitation_slot_data: invitation_slot_data,
                        amount: amount
                    };
                    mentorModel.createOpenTokTokenAndUpdate(req, res, dataToSend, function (req, res, flagStatus) {
                        sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
                    });
                } else {
                    sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
                }
            });
        }
    });
});
router.get('/thankyou/:invitation_id', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.getMeetingPendingInvites_model(req, res, req.params.invitation_id, 'accepted', function (req, res, arrFinalData) {
                mentorModel.getMeetingUrl(req, res, req.params.invitation_id, function (req, res, finalData) {
                    var slotsMentorObj = {};
                    arrFinalData.receivedData[0].invitation_slots.forEach(function (slot) {
                        if (slot.status == 'accepted') {
                            slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('MMMM DD, YYYY');
                            slotsMentorObj.startTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('hh:mm A - z');
                            slotsMentorObj.endTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).add(slot.duration, 'minutes').format('hh:mm A - z');
                            slotsMentorObj.atc_startTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('YYYY-MM-DD H:mm:ss');
                            slotsMentorObj.atc_endTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).add(slot.duration, 'minutes').format('YYYY-MM-DD H:mm:ss');
                        }
                    });
                    var renderParams = {
                        layout: 'mentor_after_login_layout',
                        frontConstant: frontConstant,
                        userData: userData,
                        arrSessionData: req.session,
                        invitationDetail: slotsMentorObj,
                        invitationData: arrFinalData.receivedData[0],
                        meetingKey: finalData.meeting_url_key
                    };
                    res.render('mentors/thankyou_page', renderParams);
                });
            });
        });
    });
});


router.get('/addMeetingToCalendar/:invitation_id', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            mentorModel.getMeetingPendingInvites_model(req, res, req.params.invitation_id, 'accepted', function (req, res, arrFinalData) {
                if (arrFinalData.receivedData.length > 0) {

                    var slotsMentorObj = {};
                    arrFinalData.receivedData[0].invitation_slots.forEach(function (slot) {
                        if (slot.status == 'accepted') {
                            slotsMentorObj.dateDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('MMMM DD, YYYY');
                            slotsMentorObj.startTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('hh:mm A - z');
                            slotsMentorObj.endTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).add(slot.duration, 'minutes').format('hh:mm A - z');
                            slotsMentorObj.atc_startTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).format('YYYY-MM-DD H:mm:ss');
                            slotsMentorObj.atc_endTimeDisplay = moment.tz(slot.date + " " + slot.time, "UTC").tz(userData.zone_name).add(slot.duration, 'minutes').format('YYYY-MM-DD H:mm:ss');
                        }
                    });
                    var renderParams = {
                        layout: 'mentor_after_login_layout',
                        frontConstant: frontConstant,
                        userData: userData,
                        arrSessionData: req.session,
                        invitationDetail: slotsMentorObj,
                        invitationData: arrFinalData.receivedData[0]
                    };
                    res.render('mentors/thankyou_page', renderParams);
                } else {
                    var renderParams = {
                        layout: 'mentor_after_login_layout',
                        frontConstant: frontConstant,
                        userData: userData,
                        arrSessionData: req.session,
                    };
                    res.render('mentors/error_msg', renderParams);
                }
            });
        });
    });
});


router.post('/declineInvitation', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.declineInvitation(req, res, function (req, res, invitation_data, invitation_slot_data) {
            if (invitation_data != "error" && invitation_slot_data != "error") {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
            }
        });
    });
});
router.post('/saveReview', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.saveReview(req, res, callback);
    // });
});
router.get('/getMentorReviewPagination', function (req, res, callback) {
    var pageNumber = req.query.pageNumber;
    var mentorId = req.query.mentorId;
    mentorModel.getMentorReview(req, res, mentorId, pageNumber, function (req, res, mentorId, arrReviewData) {
        sendResponse.sendJsonResponse(req, res, 200, arrReviewData, 0, 'REVFETCH');
    });
});
/* Get availability data of mentor from the system database */
router.post('/getPrivateInviteById', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    //mentorModel.mentorAvailabilityData(req, res);
    mentorModel.mentorPrivateInvitesData(req, res);
});
/* Send private invitation to user */
router.post('/update_private_invitation', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.update_private_invitation(req, res);
    });
});
/* Send private invitation to user */
router.post('/delete_private_invitation', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.delete_private_invitation(req, res);
    });
});
router.post('/inviteDataPaging', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.inviteDataPaging(req, res, function (req, res, meetingDataRes) {
            sendResponse.sendJsonResponse(req, res, 200, meetingDataRes, 0, 'SUCCESS');
        });
    });
});
/* GET mentor calendar. */
router.get('/promocode', function (req, res, callback) {

    userModel.userAuthentication(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');
        var renderParams = {
            layout: 'dashboard_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session,
            bodyClass: ''
        };
        res.render('mentors/promocode', renderParams);
    });
});

router.post('/genratePromoCode', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.genratePromoCode_model(req, res, function (req, res, dataToResponse) {
            sendResponse.sendJsonResponse(req, res, 200, dataToResponse, 0, 'SUCCESS');
        });
    });
});
router.post('/addPromoCode', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.addPromoCode_model(req, res, callback);
    });
});
router.post('/getPromoCodeData', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.getPromoCodeData_model(req, res, callback);
    });
});
router.post('/deletePromoCode', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.deletePromoCode_model(req, res, callback);
    });
});

router.post('/chkAcceptedSlots', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAPIAuthentication(req, res, function (req, res) {
        if (typeof req.body.invitation_id == "undefined" || typeof req.body.invitation_slot_id == "undefined" || req.body.invitation_id == "" || req.body.invitation_slot_id == "") {
            sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'USRNLOGIN');
        } else {

            var invitation_id = req.body.invitation_id;
            var invitation_slot_id = req.body.invitation_slot_id;
            var mentorId = req.body.mentorId;
            mentorModel.chkAcceptedSlots(req, res, invitation_id, invitation_slot_id, mentorId, function (req, res, invitation_data, invitation_slot_data, message) {
                if (message == "error") {
                    sendResponse.sendJsonResponse(req, res, 200, message, 0, 'error');
                } else {
                    sendResponse.sendJsonResponse(req, res, 200, message, 0, 'success');
                }
            });
        }
    });
});

router.post('/updatePaypalEmail', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {

        var reqBody = req.body;
        var encId = reqBody.encId;
        var encData = reqBody.encData;

        var objparam = {
            encId: encId,
            encData: encData
        };

        encdec.decData(req, res, objparam, function (req, res, decResult) {
            if (decResult != "") {
                var reqBodyP = decResult;
                var paypalEmail = reqBodyP.paypalEmail;
                var objParam = {
                    paypalEmail: paypalEmail
                };
                mentorModel.updatePaypalEmail_model(req, res, objParam, function (req, res, dataToResponse) {
                    sendResponse.sendJsonResponse(req, res, 200, dataToResponse, 0, 'SUCCESS');
                });
            }
        });
    });
});

router.post('/getPaypalEmail', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.paypalEmail_model(req, res, function (req, res, arrData) {
            sendResponse.sendJsonResponse(req, res, 200, arrData, 0, 'SUCCESS');
        });

    });
});

router.post('/paypalIdentityCheck', function (req, res, callback) {
    console.log("post paypal body :: ", JSON.stringify(req.body));
});

router.get('/paypalIdentityCheck', function (req, res, callback) {

    var openIdConnect = paypal.openIdConnect;
    paypal.configure({
        'openid_mode': constants.MODE,
        'openid_client_id': constants.CLIENT_ID,
        'openid_client_secret': constants.CLIENT_SECRET,
        'openid_redirect_uri': constants.RETURNURL
    });

    console.log("get paypal body :: ", JSON.stringify(req.query.code));
    console.log("openIdConnect.authorizeUrl :: ", openIdConnect.authorizeUrl({'scope': 'openid profile email phone address https://uri.paypal.com/services/paypalattributes https://uri.paypal.com/services/invoicing'}));

    openIdConnect.tokeninfo.create(req.query.code, function (error, tokeninfo) {
        if (error) {
            //console.log("error OUT :: ", error);
        } else {
            openIdConnect.userinfo.get(tokeninfo.access_token, function (error, userinfo) {
                if (error) {
                    //console.log("error IN :: ", error);
                } else {
                    //console.log("tokeninfo :: ",tokeninfo);
                    //console.log("userinfo :: ", userinfo);
                    // userModel.userAPIAuthentication(req, res, function (req, res) {
                    mentorModel.updatePaypalEmail_model(req, res, userinfo, function (req, res, info) {
                        res.send("<script> window.close(); </script>");
                        res.end();
                        //sendResponse.sendJsonResponse(req, res, 200, info, 0, 'SUCCESS');
                        //window.close();
                    });
                    // });
                    // Logout url
                    // console.log("openIdConnect.logoutUrl", openIdConnect.logoutUrl({ 'id_token': tokeninfo.id_token }));
                }
            });
        }
    });
});

router.post('/paypalPayout', function (req, res, callback) {

    var reqBody = req.body;
    var txtEmail = reqBody.txtEmail;
    var txtAmount = reqBody.txtAmount;

    paypal.configure({
        'mode': constants.MODE,
        'client_id': constants.CLIENT_ID,
        'client_secret': constants.CLIENT_SECRET,
    });

    var sender_batch_id = Math.random().toString(36).substring(9);

    var create_payout_json = {
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": txtAmount,
                    "currency": "USD"
                },
                "receiver": txtEmail,
                "note": "Thank you.",
                "sender_item_id": "item_1"
            },
        ]
    };

    paypal.payout.create(create_payout_json, function (error, payout) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
            console.log("Create Payout Response");
            console.log(payout);
        }
    });
});


router.post('/requestPayout', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.getMentorDetails(req, res, req.session.userId, function (req, res, mentorId, mentorDetails) {
            mentorModel.requestPayout_model(req, res, mentorDetails, function (req, res, dataToResponse) {
                sendResponse.sendJsonResponse(req, res, 200, dataToResponse, 0, 'SUCCESS');
            });
        });
    });
});

router.post('/removeEntery', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.removeEntery(req, res, function (req, res, data) {
            sendResponse.sendJsonResponse(req, res, 200, data, 0, 'SUCCESS');
        });
    });
});


module.exports = router;