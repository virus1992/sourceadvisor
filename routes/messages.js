var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var formidable = require('formidable');
var fs = require('fs');
var Busboy = require('busboy');
var os = require('os');
var inspect = require('util').inspect;
var async = require("async");

var multerSetting = require('./../modules/multerSetting');
var uploadMessageDoc = multer({storage: multerSetting.uploadMessageDoc});

// model
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var messagesModel = require('./../model/messages_model');
var miscFunction = require('./../model/misc_model');

// modules
var frontConstant = require('./../modules/front_constant');
var multerSetting = require('./../modules/multerSetting');
var handlers = require('./../modules/handlers');
var sendResponse = require('./../modules/sendresponse');

var router = express.Router();

router.post('/getUserConversations', function (req, res, callback) {
    
    userModel.userAuthentication(req, res, function (req, res) {
        async.parallel([
            // function call to get normal user messages
            function(messagesCallback){
                messagesModel.getUserConversations_model(req, res, function (req, res, conversations) {
                    messagesCallback(null, conversations);
                });
            },
            // function call to get admin messages
            function(messagesCallback){
                messagesModel.getAdminMessagesForLoggedInUser(req, res, function(req, res, adminMessages){
                    messagesCallback(null, adminMessages);
                });
            }
        ], function(error, resultSet){

            var conversation = resultSet[0];
            var adminMessages = resultSet[1];

            var objDataToResponse = {
                conversation: conversation,
                adminMessages: adminMessages
            }
            sendResponse.sendJsonResponse(req, res, 200, objDataToResponse, "0");
        });
    });
});

router.post('/getAdminMessagesForTheUser', function(req, res, callback){
    userModel.userAPIAuthentication(req, res, function(req, res){
        var reqBody = req.body;

        var adminRefundMessageId = reqBody.adminRefundMessageId;
        var adminRefundMessageMasterId = reqBody.adminRefundMessageMasterId;
        var adminRefundMessageThreadId = reqBody.adminRefundMessageThreadId;
        var userId = reqBody.userId;

        var objDataToSend = {
            adminRefundMessageId: adminRefundMessageId,
            adminRefundMessageMasterId: adminRefundMessageMasterId,
            adminRefundMessageThreadId: adminRefundMessageThreadId,
            userId: userId
        }

        messagesModel.updateMessageThreadCountToZero(req, res, objDataToSend, function(req, res, objData){

        });

        messagesModel.getUsersRequestRefundAdminMessages(req, res, objDataToSend, function(req, res, objRefundRequestAdminMessages){
            sendResponse.sendJsonResponse(req, res, 200, objRefundRequestAdminMessages, "FETCH");
        });

//res.send(reqBody);
    });
})

router.post('/getConversationMessages', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAuthentication(req, res, function (req, res) {
        messagesModel.getConversationMessages_model(req, res, function (req, res, conversationMessages) {
            sendResponse.sendJsonResponse(req, res, 200, conversationMessages, "0");
        });
    });
});

router.post('/sendUserMessages', uploadMessageDoc.array('msg_doc',5), function (req, res, callback) {
    //res.send(req.body);
    //req.session.userId = 2;
    //userModel.userAuthentication(req, res, function (req, res) {   
        var oppUserId = "0";
        if(typeof req.body.oppUserId != 'undefined') {
            oppUserId = req.body.oppUserId;
        } 

        if(oppUserId != "-1") {
            messagesModel.sendUserMessages(req, res, callback);
        }
        else {
            messagesModel.sendUserMessageToAdmin(req, res, function(req, res, objResponseData){
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(JSON.stringify({
                    "message": "success"}));
                res.end();
            });
        }
    //});
});

router.post('/archiveMsg', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAuthentication(req, res, function (req, res) {
        messagesModel.archiveMsg(req, res, function (req, res, conversationMessages) {
            sendResponse.sendJsonResponse(req, res, 200, conversationMessages, "0");
        });
    });
});

router.post('/blockUsers', function (req, res, callback) {
    messagesModel.blockUser(req, res, function (req, res, conversationMessages) {
        sendResponse.sendJsonResponse(req, res, 200, conversationMessages, "0");
    });
});

router.post('/deleteMsg', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAuthentication(req, res, function (req, res) {
        messagesModel.deleteMsg(req, res, function (req, res, conversationMessages) {
            sendResponse.sendJsonResponse(req, res, 200, conversationMessages, "0");
        });
    });
});

router.post('/sendContactMsg', function (req, res, callback) {
    //req.session.userId = 2;
    console.log('Helloo')
        messagesModel.sendContactMessages(req, res, function (req, res, conversationMessages) {
            sendResponse.sendJsonResponse(req, res, 200, conversationMessages, "0");
        });
});
module.exports = router;