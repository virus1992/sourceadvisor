var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');


// model
var userModel = require('./../model/users_model');
var miscFunction = require('./../model/misc_model');
var frontConstant = require('./../modules/front_constant');
var constant = require('./../modules/constants');
var sendResponse = require('./../modules/sendresponse');

//var arrSessionData = req.session;


var router = express.Router();


router.post('/getCancellationPriceForUser', function (req, res, callback) {
    var userId = req.body.userId;
    var mentorId = req.body.mentorId;
    var invitationId = req.body.invitationId;
    var cancellationFor = req.body.cancellationFor;

    var objDataParams = {
        userId: userId,
        mentorId: mentorId,
        invitationId: invitationId,
        cancellationFor: cancellationFor
    };
    miscFunction.getCancellationPriceForUser(req, res, objDataParams, function (req, res, objUsersCalcellationPrice) {
        sendResponse.sendJsonResponse(req, res, 200, objUsersCalcellationPrice, "0", "FETCH");
    });
});


router.get('/getAllLocationList', function (req, res, callback) {
    miscFunction.getAllLocationList(req, res, function (req, res, arrData, callback) {

        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(arrData));
        res.end();

        //sendResponse.sendJsonResponse(req, res, 200, arrData, "0", "FETCH");
    });
});

router.get('/getDataForSearchLocation', function (req, res, callback) {
    miscFunction.getLocationForSearch(req, res, function (req, res, arrData, callback) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(arrData));
        res.end();
    });
});


router.get('/getSearchQueryStringData', function (req, res, callback) {
    miscFunction.getSearchQueryStringData(req, res, function (req, res, arrData, callback) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(arrData));
        res.end();
    });
});


router.get('/fortest', function (req, res, callback) {

    var strTest = "";

    for (i = 0; i <= 3; i++) {
        strTest += i + ",";
    }

    var arrTest = strTest.split(",");

    //var arrTest = ['1','2'];


    arrTest = ['milind', 'pasi', 'virendra', 'ohhh'];


    miscFunction.printAllPermutation(arrTest, arrTest.length, function (arrPermutations) {
        console.log("arrPermutations :: ", arrPermutations);
        arrTest = JSON.stringify(arrPermutations);


        var renderParams = {
            layout: 'blank',
            arrTest: arrTest,
            strTest: strTest
        };

        res.render('test/fortest', renderParams);
    });
});


router.get('/getCityListForCountry', function (req, res, callback) {

    var countryId = req.query.countryId;

    miscFunction.getCityListCountFromCountryId(req, res, countryId, function (req, res, arrCityListCount) {
        sendResponse.sendJsonResponse(req, res, 200, arrCityListCount, "0", "FETCH");
    });
});

router.post('/getCostBreakUp', function (req, res, callback) {

    miscFunction.getCostBreakUp(req, res, function (req, res, costBreakUp) {
        sendResponse.sendJsonResponse(req, res, 200, costBreakUp, "0", "success");
    });
});




router.get('/mentorprofilecreation', function (req, res, callback) {

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    var renderParams = {
        layout: 'blank',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        constant: constant,
    };

    res.render('test/mentorprofilecreation', renderParams);
});

router.post('/processMentorCreate', function (req, res, callback) {
    miscFunction.uploadCSVForMentorCreate(req, res, function (req, res, objFileResponse) {
        miscFunction.processCsvForMentorCreate(req, res, objFileResponse, function (req, res, objResponseData) {

        });
    });
});

module.exports = router;