var express = require('express');
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
var multer = require('multer');
var path = require('path');
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var paypalFunction = require('./../model/paypal_model');
var paymentModel = require('./../model/payment_model');
var sendResponse = require('./../modules/sendresponse');
var frontConstant = require('./../modules/front_constant');
var miscFunction = require('./../model/misc_model');
var dbConstants = require('./../modules/db_constants');
var constants = require('./../modules/constants');
var encdec = require('./../modules/encdec');
var router = express.Router();
var moment = require('moment');
var async = require('async');
router.get('/callback', function (req, res, callback) {
    console.log(req.query);
});
router.get('/payouttomentor', function () {
    /*stripe.transfers.create(
     {
     amount: 1000,
     currency: "usd",
     destination: "usd"
     },
     {stripe_account: 'acct_19DXVNIvR1cvD8uz'}
     );*/

    stripe.transfers.create({
        amount: 60,
        currency: "usd",
        destination: "acct_19DXVNIvR1cvD8uz",
        description: "Transfer for test@example.com"
    }, function (err, transfer) {
        console.log(err);
        console.log(transfer);
    });
});
router.post('/chargeCustomerForCard', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        var reqBody = req.body;
        var encId = reqBody.encId;
        var encData = reqBody.encData;

        var objparam = {
            encId: encId,
            encData: encData
        };

        encdec.decData(req, res, objparam, function (req, res, decResult) {
            if (decResult != "") {
                var reqBodyP = decResult;
                var invoiceId = reqBodyP.invoiceId;
                var mentorSlug = reqBodyP.mentorSlug;
                var stripeToken = reqBodyP.id;
                var objSlotParams = reqBodyP.objSlotParams;
                var promocodeId = reqBodyP.promocode_id;
                var cardNumber = reqBodyP.cardNumber;
                var cardExpiryMM = reqBodyP.cardExpiryMM;
                var cardExpiryYY = reqBodyP.cardExpiryYY;
                var cardCVC = reqBodyP.cardCVC;
                var cardType = reqBodyP.cardType;
                var invitation_id = reqBodyP.invitation_id;
                var invitation_slot_id = reqBodyP.invitation_slot_id;
                var privateInvite = reqBodyP.privateInvite;
                var mentorId = objSlotParams.mentorId;
                var objPromoDataParams = {
                    promoCodeId: promocodeId
                };
                async.parallel([
                    function (paymentCallback) {
                        mentorModel.getMentorDetailsBySlug(req, res, mentorSlug, function (req, res, mentorDetails) {
                            paymentCallback(null, mentorDetails);
                        });
                    },
                    function (paymentCallback) {
                        mentorModel.getMeetingInvitationInfo(req, res, invoiceId, function (req, res, arrMeetingInvitationData) {
                            mentorModel.getMentorPendingInvitationsData(req, res, [arrMeetingInvitationData], 0, [], function (req, res, fullInvitationData) {
                                var dataReturn = {
                                    arrMeetingInvitationData: arrMeetingInvitationData,
                                    fullInvitationData: fullInvitationData
                                };
                                paymentCallback(null, dataReturn);
                            });
                            //paymentCallback(null, arrMeetingInvitationData);
                        });
                    },
                    function (paymentCallback) {
                        miscFunction.getPromoCodeInformation(req, res, objPromoDataParams, function (req, res, objPromoCodeInfo) {
                            paymentCallback(null, objPromoCodeInfo);
                        });
                    },
                    function (paymentCallback) {
                        var objDataParams = {
                            invoiceId: invoiceId
                        };
                        miscFunction.getMentorAndUserCountryPercentageForInvoice(req, res, objDataParams, function (req, res, objMentorAndUserCountryPercentageForInvoice) {
                            paymentCallback(null, objMentorAndUserCountryPercentageForInvoice);
                        });
                    },
                    function (paymentCallback) {
                        var objDataParams = {
                            systemSettingVariables: ['MINWCP', 'TAXCRG']
                        };
                        miscFunction.getSystemSettingVariable(req, res, objDataParams, function (req, res, objSystemSettingResponse) {
                            paymentCallback(null, objSystemSettingResponse);
                        });
                    }
                ], function (error, resultSet) {

                    var mentorDetails = resultSet[0];
                    var arrMeetingInvitationData = resultSet[1].arrMeetingInvitationData;
                    var fullInvitationData = resultSet[1].fullInvitationData;
                    var objPromoCodeInfo = resultSet[2];
                    var objMentorAndUserCountryPercentageForInvoice = resultSet[3];
                    var objSystemSettingResponse = resultSet[4];
                    var wissenxUserChargePercentage = objMentorAndUserCountryPercentageForInvoice.wissenxChargesUserPercentage;
                    var wissenxMentorChargePercentage = objMentorAndUserCountryPercentageForInvoice.wissenxChargesMentorPercentage;
                    var taxChargePercentage = 0;
                    var wissenx_min_user_charge = 0;
                    objSlotParams.slots = fullInvitationData[0].invitation_slots;
                    var mentor_real_rate = mentorDetails.hourlyRate;
                    var duration = arrMeetingInvitationData.duration;
                    var wissenx_charge_percentage = wissenxUserChargePercentage;
                    var taxes_percentage = taxChargePercentage;
                    var promocode_discount_percentage = 0;
                    if (typeof objPromoCodeInfo.promoCodeId != 'undefined') {
                        promocode_discount_percentage = objPromoCodeInfo.discountPercentage;
                    }

                    var total_payment = '';
                    for (var i = 0; i < objSystemSettingResponse.length; i++) {
                        if (objSystemSettingResponse[i]['systemVariableCode'] == 'MINWCP') {
                            wissenx_min_user_charge = objSystemSettingResponse[i]['settingValue'];
                        }

                        if (objSystemSettingResponse[i]['systemVariableCode'] == 'TAXCRG') {
                            taxChargePercentage = objSystemSettingResponse[i]['settingValue'];
                        }
                    }

                    var mentor_charge = mentor_real_rate;
                    var wissenx_user_percentage = wissenx_charge_percentage;
                    var wissenx_user_charge = 0;
                    // var promocode_id = 0;
                    // var promocode_discount = 0;
                    var wissenx_tax_percentage = taxChargePercentage;
                    var wissenx_tax_charge = 0;
                    var total_payable_amount = 0;
                    var pay_to_mentor = 0;
                    var pay_to_wissenx = 0;
                    var updated_time = constants.CURR_UTC_DATETIME();
                    var wissenx_mentor_charge = 0;
                    var timeConstraint = parseFloat(parseFloat(duration) / 60);
                    var mentorPayableCharge = parseFloat(parseFloat(mentor_charge) * parseFloat(timeConstraint)).toFixed(2);
                    var mentorPayableChargeWithoutDiscount = parseFloat(parseFloat(mentor_charge) * parseFloat(timeConstraint)).toFixed(2);
                    var promocode_discount_value = parseFloat(parseFloat(parseFloat(mentorPayableCharge) * parseFloat(promocode_discount_percentage)) / 100).toFixed(2);
                    mentorPayableCharge = parseFloat(parseFloat(mentorPayableCharge) - parseFloat(promocode_discount_value)).toFixed(2);
                    wissenx_user_charge = parseFloat(((parseFloat(mentorPayableCharge) * (parseFloat(wissenx_user_percentage) / 100)))).toFixed(2);
                    if (parseFloat(wissenx_user_charge) < parseFloat(wissenx_min_user_charge)) {
                        wissenx_user_charge = parseFloat(wissenx_min_user_charge).toFixed(2);
                    }

                    var taxableCharge = parseFloat(parseFloat(mentorPayableCharge) + parseInt(wissenx_user_charge) - parseFloat(promocode_discount_value));
                    wissenx_tax_charge = parseFloat(parseFloat(taxableCharge) * parseFloat(parseFloat(taxChargePercentage) / 100)).toFixed(2);
                    total_payable_amount = parseFloat(parseFloat(mentorPayableCharge) + parseFloat(wissenx_user_charge) + parseFloat(wissenx_tax_charge)).toFixed(2);
                    pay_to_mentor = parseFloat(parseFloat(mentorPayableCharge) - parseFloat(mentorPayableCharge) * (parseFloat(wissenxMentorChargePercentage) / 100)).toFixed(2);
                    pay_to_wissenx = parseFloat(parseFloat(total_payable_amount) - parseFloat(pay_to_mentor)).toFixed(2);
                    wissenx_mentor_charge = parseFloat(parseFloat(total_payable_amount) - (parseFloat(pay_to_mentor) + parseFloat(wissenx_user_charge))).toFixed(2);
                    var dataParamsInvoice = {
                        mentor_charge: mentorPayableCharge,
                        mentor_hourly_charge: mentorPayableChargeWithoutDiscount,
                        wissenx_user_percentage: wissenx_user_percentage,
                        wissenx_user_charge: wissenx_user_charge,
                        wissenx_mentor_percentage: wissenxMentorChargePercentage,
                        wissenx_mentor_charge: wissenx_mentor_charge,
                        promocode_id: promocodeId,
                        promocode_discount: promocode_discount_value,
                        wissenx_tax_percentage: wissenx_tax_percentage,
                        wissenx_tax_charge: wissenx_tax_charge,
                        total_payable_amount: total_payable_amount,
                        pay_to_mentor: pay_to_mentor,
                        pay_to_wissenx: pay_to_wissenx,
                        refund_type: "PAYNOR",
                        final_pay_to_wissenx: pay_to_wissenx,
                        final_pay_to_mentor: pay_to_mentor,
                        final_user_paid_amount: total_payable_amount,
                        updated_time: updated_time
                    };
                    var objDataParamsForPayment = {
                        requestBody: reqBodyP,
                        invoiceId: invoiceId,
                        mentorSlug: mentorSlug,
                        wissenxUserChargePercentage: wissenxUserChargePercentage,
                        wissenxMentorChargePercentage: wissenxMentorChargePercentage,
                        taxChargePercentage: taxChargePercentage,
                        arrMeetingInvitationData: arrMeetingInvitationData,
                        mentorDetails: mentorDetails,
                        dataParamsInvoice: dataParamsInvoice
                    }

                    paymentModel.getMeetingInviteInvoiceAndUserDetail(req, res, reqBodyP, function (req, res, arrInviteInvoiceAndUserData) {
                        // if invoice data found
                        if (typeof arrInviteInvoiceAndUserData.invoiceId != 'undefined' && !isNaN(arrInviteInvoiceAndUserData.invoiceId) && arrInviteInvoiceAndUserData.invoiceId > 0) {
                            /*
                             DONT DELETE THIS CODE
                             //if stripe customer id found in system for the user.
                             if(typeof arrInviteInvoiceAndUserData.userStripeCustomerNumber != 'undefined' && arrInviteInvoiceAndUserData.userStripeCustomerNumber != null) {
                             console.log("Customer created");
                             var userStripeCustomerNumber = arrInviteInvoiceAndUserData.userStripeCustomerNumber;
                             
                             var dataParamsForChargeCreate = {
                             stripeToken: stripeToken,
                             amount: total_payable_amount,
                             stripeCustomerNumber: userStripeCustomerNumber
                             };
                             
                             paymentModel.chargeCustomerCreditCard(req, res, dataParamsForChargeCreate, function(req, res, error, chargeData) {
                             console.log("error :: ", error);
                             console.log("chargeData :: ", chargeData);
                             });
                             
                             }
                             // if stripe customer not found in system, create new stripe customer
                             else {
                             console.log("Customer not created");            			
                             var userEmail = arrInviteInvoiceAndUserData.userEmail;
                             
                             var objDataParams = {
                             userEmail: userEmail,
                             stripeToken: stripeToken
                             };
                             console.log(objDataParams);
                             paymentModel.createStripeCustomer(req, res, objDataParams, function(req, res, arrStripeCustomerData) {
                             console.log(arrStripeCustomerData);
                             });
                             }*/

                            var reason = "";
                            var authuser_id = "";
                            if (privateInvite == 0) {
                                //reason = "Authorization for Normal Invite Meeting";
                                reason = "AUTHNORINVIMEET";
                                authuser_id = arrMeetingInvitationData.requester_id;
                            } else {
                                //reason = "Authorization for Private Invite Meeting";
                                reason = "AUTHPRIINVIMEET";
                                authuser_id = arrMeetingInvitationData.user_id;
                            }

                            var dataParamsForChargeCreate = {
                                stripeToken: stripeToken,
                                amount: total_payable_amount,
                                cardNumber: cardNumber,
                                cardExpiryMM: cardExpiryMM,
                                cardExpiryYY: cardExpiryYY,
                                cardCVC: cardCVC,
                                cardType: cardType,
                                orderId: invoiceId,
                                reason: reason,
                                user_id: authuser_id
                            };
                            //paymentModel.chargeCustomerCreditCard(req, res, dataParamsForChargeCreate, function (req, res, error, chargeData) {
                            paypalFunction.paypalAuthPayment(req, res, dataParamsForChargeCreate, function (req, res, error, chargeData) {
                                if (error) {
                                    sendResponse.sendJsonResponse(req, res, 200, error, "1", "CARDERROR");
                                } else {
                                    dataParamsForChargeCreate.chargeData = chargeData;
                                    objDataParamsForPayment.dataParamsForChargeCreate = dataParamsForChargeCreate;
                                    var transection_id = dataParamsForChargeCreate.chargeData.transactions[0].related_resources[0].authorization.id;
                                    var invoice_id = invoiceId;
                                    var amount = total_payable_amount;
                                    paymentModel.updateInvoiceAndInsertPayment(req, res, objDataParamsForPayment, function (req, res, flagSuccess) {
                                        var dataResponse = {
                                            invoiceId: arrInviteInvoiceAndUserData.invoiceId,
                                            mentorSlug: mentorSlug
                                        };
                                        if (!flagSuccess) {// error
                                            sendResponse.sendJsonResponse(req, res, 200, dataResponse, "1", "PAYMENTUPDATEFAIL");
                                        } else {//success

                                            if (privateInvite == 0) {
                                                // sending invitation to user
                                                objSlotParams.amount = total_payable_amount;
                                                userModel.getUserInformation(req, res, req.session.userId, objSlotParams, userModel.sendMeetingInvitationToUser);
                                                // sending invitation to mentor
                                                userModel.getUserInformation(req, res, req.session.userId, objSlotParams, userModel.sendMeetingInvitationToMentor);
                                                sendResponse.sendJsonResponse(req, res, 200, dataResponse, "0", "PAYMENTDONE");
                                            } else {
                                                var inviteType = 1;
                                                mentorModel.acceptInvitation(req, res, invitation_id, invitation_slot_id, transection_id, amount, invoice_id, authuser_id, inviteType, function (req, res, invitation_data, invitation_slot_data) {
                                                    if (invitation_data != "error" && invitation_slot_data != "error") {

                                                        var dataToSend = {
                                                            invitation_data: invitation_data,
                                                            invitation_slot_data: invitation_slot_data,
                                                            private_invite: 1,
                                                            mentorId: mentorId,
                                                            amount: total_payable_amount
                                                        };
                                                        var dataRes = {
                                                            invitation_id: invitation_id
                                                        };
                                                        mentorModel.createOpenTokTokenAndUpdate(req, res, dataToSend, function (req, res, flagStatus) {
                                                            //var win = window.open($('#absPath').val() + 'mentor/thankyou/' + invitation_id, '_blank');                                                            
                                                            if (flagStatus == true) {

                                                                sendResponse.sendJsonResponse(req, res, 200, dataRes, "0", "PAYMENTDONEI");
                                                            } else {
                                                                sendResponse.sendJsonResponse(req, res, 200, dataRes, "1", "PAYMENTDONEI");
                                                            }
                                                        });
                                                    } else {
                                                        sendResponse.sendJsonResponse(req, res, 200, dataRes, "1", "PAYMENTDONEI");
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            // Invoice data not found.
                            sendResponse.sendJsonResponse(req, res, 200, [], "1", "INVOICEDNF");
                        }
                    });
                });
            }
        });
    });
});
router.get('/payment_successfull', function (req, res, callback) {
    res.redirect('/dashboard');
});
router.post('/payment_successfull', function (req, res, callback) {
    var reqBody = req.body;
    userModel.userAuthentication(req, res, function (req, res) {
        var invoiceId = reqBody.invoiceId;
        var mentorSlug = reqBody.mentorSlug;
        mentorModel.verifyMentorSlug(req, res, mentorSlug, renderMentorPaymentSuccessPage, mentorModel.getMentorAllData);
    });
});
router.get('/pss', function (req, res, callback) {
    var reqBody = {
        "invoiceId": "15",
        "mentorSlug": "SanchitS"
    };
    var invoiceId = reqBody.invoiceId;
    var mentorSlug = reqBody.mentorSlug;
    req.body.invoiceId = invoiceId;
    req.body.mentorSlug = mentorSlug;
    mentorModel.verifyMentorSlug(req, res, mentorSlug, renderMentorPaymentSuccessPage, mentorModel.getMentorAllData);
});
var renderMentorPaymentSuccessPage = function (req, res, arrMentorData) {
    //console.log("=====================renderMentorPaymentSuccessPage=================");
    //console.log(arrMentorData);
    var reqBody = req.body;
    var invoiceId = reqBody.invoiceId;
    var mentorSlug = reqBody.mentorSlug;
    paymentModel.getInvoiceAndPaymentData(req, res, invoiceId, function (req, res, arrInvoicePaymentData) {
        var renderParams = {
            layout: 'dashboard_layout',
            frontConstant: frontConstant,
            arrSessionData: req.session,
            arrMentorData: arrMentorData,
            invoiceId: invoiceId,
            mentorSlug: mentorSlug,
            arrInvoicePaymentData: arrInvoicePaymentData.arrInvoiceAndPaymentData,
            paidAmount: arrInvoicePaymentData.arrInvoiceAndPaymentData.totalPayAbleAmount.toFixed(2),
            arrSlotsRequested: arrInvoicePaymentData.arrSlotsInformation
        };
        res.render('payment/payment_success', renderParams);
    });
};

// exporting all the routes
module.exports = router;