var express = require('express');
var multer = require('multer');
var path = require('path');
var router = express.Router();
var sendResponse = require('./../modules/sendresponse');
var encdec = require('./../modules/encdec');
var multerSetting = require('./../modules/multerSetting');
var uploadMeetingDoc = multer({
    storage: multerSetting.uploadMeetingDoc
});
// model
var userModel = require('./../model/users_model');
var mentorModel = require('./../model/mentor_model');
var miscFunction = require('./../model/misc_model');
var userSection = require('./../model/usersection_model');
var frontConstant = require('./../modules/front_constant');
var async = require('async');

router.get('/transactions', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed'
            };
            res.render('users/transactions', renderParams);
        });
    });
});

router.get('/meetings', function (req, res) {

    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            miscFunction.getTimeZoneList(req, res, '', function (req, res, timeZoneList) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    userData: userData,
                    timeZoneList: timeZoneList,
                    arrSessionData: req.session
                };
                res.render('users/meetings', renderParams);
            });
        });
    });
});

/* GET users listing. */
router.get('/profile', function (req, res, callback) {

    userModel.userAuthentication(req, res, function (req, res) {
        userSection.getUserProfileData(req, res, req.session.userId, function (req, res, userId, arrUserData) {
            userSection.getUserSocialLoginData(req, res, req.session.userId, function (req, res, userId, arrSocialLoginData) {
                miscFunction.getIndustryListFromDb(req, res, function (req, res, industriesList) {
                    miscFunction.getSubIndustryListFromDb(req, res, function (req, res, subIndustriesList) {
                        miscFunction.getDomainListFromDb(req, res, function (req, res, domainList) {
                            miscFunction.getRegionListFromDb(req, res, function (req, res, regionList) {
                                miscFunction.getCountryList(req, res, "1", function (req, res, countryListALL) {
                                    miscFunction.getCountryList(req, res, "0", function (req, res, countryList) {
                                        miscFunction.getTimeZoneList(req, res, '', function (req, res, timeZoneList) {
                                            miscFunction.getCancellationPolicies(req, res, function (req, res, policyData) {

                                                masterData = {
                                                    industriesList: industriesList,
                                                    subIndustriesList: subIndustriesList,
                                                    domainList: domainList,
                                                    regionList: regionList,
                                                    countriesList: countryList
                                                };
                                                var errors = req.flash('errors');
                                                var messages = req.flash('messages');
                                                var renderParams = {
                                                    layout: 'dashboard_layout',
                                                    errors: errors,
                                                    messages: messages,
                                                    frontConstant: frontConstant,
                                                    arrUserData: arrUserData,
                                                    masterData: masterData,
                                                    countryList: countryListALL,
                                                    timeZoneList: timeZoneList,
                                                    arrSocialLoginData: arrSocialLoginData,
                                                    arrSessionData: req.session,
                                                    policyData: policyData,
                                                    userIp: req.headers['x-forwarded-for'] || req.connection.remoteAddress
                                                };
                                                //console.log("renderParams.arrSessionData");
                                                //console.log(renderParams.arrSessionData);
                                                res.render('users/profile', renderParams);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});


router.get('/getCityList/', function (req, res, callback) {
    var countryId = req.query.country_id;
    var term = req.query.query;
    miscFunction.searchCityListFromCountryId(req, res, countryId, term, function (req, res, arrCityList) {
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify({
            "query": term,
            "suggestions": arrCityList
        }));
        res.end();
    });
});

router.post('/profileupdate', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userSection.updateUserProfile(req, res);
    });
});

router.post('/updateUserProfilePic', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userSection.uploadUserProfileImage);
});

router.post('/checkEmailAvailable', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userSection.checkDuplicateEmail);
});

router.get('/settings', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userSection.getUserSettingsInfo(req, res);
    });
});

router.get('/passwordChangeSuccessFully', function (req, res) {
    req.session.regenerate(function (err) {
        req.flash('messages', 'Your password changed successfully. Please login with new password');
        res.redirect('/login');
    });
});

router.post('/validateUserPassword', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userSection.validateUserPassword(req, res);
    });
});

router.post('/updateUserSettings', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userSection.updateUserSettings);
});

router.post('/updateUserPassword', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userSection.updateUserPassword);
});

router.post('/deleteUserAccount', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        userSection.deleteUserAccount(req, res);
    });
});

router.get('/calendar', function (req, res, callback) {
    //userModel.userAuthentication(req, res, function (req, res) {

    //userSection.getUserProfileData(req, res, function (req, res, userId, arrUserData) {
    var errors = req.flash('errors');
    var messages = req.flash('messages');
    var renderParams = {
        layout: 'mentor_after_login_layout',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session
                //arrUserData: arrUserData
    };
    res.render('users/usercalender', renderParams);
    //});
    //});
});

/* Send private invitation to user */
router.post('/send_message_mentor', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    userModel.send_message_to_mentor(req, res);
    //});
});

router.get('/slot_booking/:mentorSlug', function (req, res, callback) {
    var mentorSlug = req.params.mentorSlug;
    var errors = req.flash('errors');
    var messages = req.flash('messages');

    userModel.userAuthentication(req, res, function (req, res) {
        mentorModel.getMentorDetailsBySlug(req, res, mentorSlug, function (req, res, mentorDetails) {
            userModel.getUserInformation(req, res, mentorDetails.userId, {}, function (req, res, mentorId, userData, userParams) {
                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    arrSessionData: req.session,
                    mentorData: mentorDetails,
                    arrMentorData: userData,
                    mentorSlug: mentorSlug
                };
                res.render('users/slot_booking', renderParams);
            });
        });
    });
});

router.post('/slot_booking_payment', function (req, res, callback) {
    //console.log("req.body :: ", req.body);
    var invoiceId = req.body.invoiceId;
    var mentorSlug = req.body.mentorSlug;
    var objSlotParams = req.body.objParams;

    var invitation_id = 0;
    var invitation_slot_id = 0;
    var privateInvite = 0;

    if (typeof req.body.privateInvite != 'undefined' && req.body.privateInvite == "1") {
        invitation_id = req.body.invitation_id;
        invitation_slot_id = req.body.invitation_slot_id;
        privateInvite = req.body.privateInvite;
    }

    var errors = req.flash('errors');
    var messages = req.flash('messages');

    /*req.session.userId = 200;
     mentorSlug = "Sanchit";
     invoiceId = "88";*/

    userModel.userAuthentication(req, res, function (req, res) {

        async.parallel([
            function (slotBookingCallback) {
                mentorModel.getMentorDetailsBySlug(req, res, mentorSlug, function (req, res, mentorDetails) {
                    slotBookingCallback(null, mentorDetails);
                });
            },
            function (slotBookingCallback) {
                mentorModel.getMeetingInvitationInfo(req, res, invoiceId, function (req, res, arrMeetingInvitationData) {
                    slotBookingCallback(null, arrMeetingInvitationData);
                });
            },
            function (slotBookingCallback) {
                var objDataParams = {
                    invoiceId: invoiceId
                };
                miscFunction.getMentorAndUserCountryPercentageForInvoice(req, res, objDataParams, function (req, res, objMentorAndUserCountryPercentageForInvoice) {
                    slotBookingCallback(null, objMentorAndUserCountryPercentageForInvoice);
                });
            },
            function (slotBookingCallback) {
                var objDataParams = {
                    systemSettingVariables: ['MINWCP', 'TAXCRG']
                }
                miscFunction.getSystemSettingVariable(req, res, objDataParams, function (req, res, objSystemSettingResponse) {
                    slotBookingCallback(null, objSystemSettingResponse);
                });
            },
            function (slotBookingCallback) {
                var objParam = {
                    user_id: req.session.userId
                };
                encdec.genrateKey(req, res, objParam, function (req, res, result) {
                    slotBookingCallback(null, result);
                });
            }
        ], function (error, resultSet) {
            //console.log("resultSet :: ", resultSet);
            var mentorDetails = resultSet[0];
            var arrMeetingInvitationData = resultSet[1];
            var meetingCostData = resultSet[2];
            var systemVariableInfo = resultSet[3];
            var encInfo = resultSet[4];


            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                arrSessionData: req.session,
                mentorData: mentorDetails,
                arrMeetingInvitationData: arrMeetingInvitationData,
                invoiceId: invoiceId,
                mentorSlug: mentorSlug,
                objSlotParams: objSlotParams,
                invitation_id: invitation_id,
                invitation_slot_id: invitation_slot_id,
                privateInvite: privateInvite,
                meetingCostData: meetingCostData,
                systemVariableInfo: systemVariableInfo,
                encId: encInfo.encId,
                encKey: encInfo.encKey
            };
            res.render('users/slot_booking_payment', renderParams);

        });
    });
});

router.get('/slot_booking_payment', function (req, res) {
    res.redirect('/');
});

router.post('/bookSlot', uploadMeetingDoc.single('meeting_doc'), function (req, res, callback) {
    userModel.slotBooking(req, res);
});

router.post('/getTimeZone', function (req, res, callback) {
    miscFunction.getTimeZoneInfo(req, res, function (req, res, arrTimeZone) {
        sendResponse.sendJsonResponse(req, res, 200, arrTimeZone, "0", "FETCH");
    });
});

/* Get mentor events data from system database */
router.post('/availableDate', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorAvailableDate(req, res);
    //});
});
router.post('/getDeletedDates', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorDeletedDates(req, res);
    //});
});

/* Get mentor events data from system database */
router.post('/availableTime', function (req, res, callback) {
    // userModel.userAPIAuthentication(req, res, function (req, res) {
    mentorModel.mentorAvailableTime(req, res);
    //});
});

router.post('/messages', function (req, res, callback) {
    console.log('POST MESSAGES');
    // console.log(req.body);
    //req.session.userId = 2;
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');

            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed',
                selectedConversationID: req.body.hdn_conversation_id
            };
            res.render('messages', renderParams);
        });
    });
});

router.get('/messages', function (req, res, callback) {
    //req.session.userId = 103;
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            var errors = req.flash('errors');
            var messages = req.flash('messages');
            var renderParams = {
                layout: 'mentor_after_login_layout',
                errors: errors,
                messages: messages,
                frontConstant: frontConstant,
                userData: userData,
                arrSessionData: req.session,
                bodyClass: 'showNavRed'
            };
            // console.log(renderParams);
            res.render('messages', renderParams);
        });
    });
});

router.post('/updateUserSlug', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userSection.updateUserSlug);
});

router.post('/bookmarkMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.bookmarkMentor);
});

router.post('/chkbookmarkMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.chkbookmarkMentor);
});

router.get('/bookmarked_mentors', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        var errors = req.flash('errors');
        var messages = req.flash('messages');

        var renderParams = {
            layout: 'mentor_after_login_layout',
            errors: errors,
            messages: messages,
            frontConstant: frontConstant,
            arrSessionData: req.session
        };
        res.render('users/favoriteMentors', renderParams);
    });
});

router.post('/getUserBookmarkedMentors', function (req, res) {

    userModel.userAPIAuthentication(req, res, function (req, res) {
        var reqBody = req.body;
        var pageNumber = "0";

        //page number
        if (typeof reqBody.pageNumber != 'undefined') {
            if (typeof reqBody.pageNumber != 'undefined' && !isNaN(reqBody.pageNumber) && reqBody.pageNumber > 0) {
                pageNumber = reqBody.pageNumber;
            }
        }

        var loggedInUserId = "";
        if (typeof req.session.userId != 'undefined' && !isNaN(req.session.userId) && req.session.userId > 0) {
            loggedInUserId = req.session.userId;
        }

        var dataParams = {
            pageNumber: pageNumber,
            flagBookmarkedMentor: 1,
            loggedInUserId: loggedInUserId
        };

        async.parallel([
            // function call get mentor list to display.
            function (parallelCallback) {
                mentorModel.getMentorSearhInformation(req, res, dataParams, function (req, res, arrMentorData) {
                    parallelCallback(null, arrMentorData);
                });
            },
            // function call to get all data without limit
            function (parallelCallback) {
                var tempDataParams = dataParams;
                tempDataParams.queryForAllData = "1";
                mentorModel.getMentorSearhInformation(req, res, tempDataParams, function (req, res, arrMentorDataWithoutLimit) {
                    parallelCallback(null, arrMentorDataWithoutLimit);
                });
            }
        ],
                function (error, arrResultSet) {
                    var arrMentorDataToDisplay = arrResultSet[0];
                    var arrMentorAllDataWithoutLimit = arrResultSet[1];

                    var dataToReturn = {
                        arrMentorDataToDisplay: arrMentorDataToDisplay,
                        arrMentorAllDataWithoutLimit: arrMentorAllDataWithoutLimit,
                        loggedInUserId: loggedInUserId
                    };

                    sendResponse.sendJsonResponse(req, res, 200, dataToReturn, "0", "BOOKAMRKMENTORFETCH");
                });

    });
});

router.get('/bookingDetails', function (req, res, callback) {
    //userModel.userAuthentication(req, res, function (req, res) {

    //userModel.getUserMeetingData(req, res, function (req, res, arrUserData) {
    var errors = req.flash('errors');
    var messages = req.flash('messages');
    var renderParams = {
        layout: 'mentor_after_login_layout',
        errors: errors,
        messages: messages,
        frontConstant: frontConstant,
        arrSessionData: req.session
                //arrUserData: arrUserData
    };
    res.render('users/bookingDetails', renderParams);
    //});
    //});
});
router.post('/getUserMeetingData', function (req, res, callback) {
    //userModel.userAuthentication(req, res, function (req, res) {

    userModel.getUserMeetingData(req, res, function (req, res, arrUserData) {
        sendResponse.sendJsonResponse(req, res, 200, arrUserData, "0", "FETCH");
    });
    //});
});

router.post('/likeMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.likeMentor);
});

router.post('/chklikeMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.chklikeMentor);
});

router.post('/uploadfile', function (req, res) {
    userModel.uploadBookingDocument(req, res);
});

router.post('/chklikeMentorFavPage', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.chklikeMentorFavPage);
});

router.post('/likeThisMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.likeThisMentor);
});

router.post('/unLikeThisMentor', function (req, res, callback) {
    //req.session.userId = 200;
    userModel.userAPIAuthentication(req, res, userModel.unLikeThisMentor);
});

router.post('/bookmarkThisMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.bookmarkThisMentor);
});

router.post('/removeBookmarkedMentor', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, userModel.removeBookmarkedMentor);
});

router.get('/privateInvite', function (req, res) {
    userModel.userAuthentication(req, res, function (req, res) {
        userModel.getUserInformation(req, res, req.session.userId, {}, function (req, res, userId, userData, userParams) {
            miscFunction.getTimeZoneList(req, res, '', function (req, res, timeZoneList) {
                var errors = req.flash('errors');
                var messages = req.flash('messages');

                var renderParams = {
                    layout: 'mentor_after_login_layout',
                    errors: errors,
                    messages: messages,
                    frontConstant: frontConstant,
                    userData: userData,
                    arrSessionData: req.session,
                    timeZoneList: timeZoneList
                };
                res.render('users/privateinvite', renderParams);
            });
        });
    });
});

router.post('/getPrivateInvites', function (req, res, callback) {
    var data = {};
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.getPrivateInvites_model(req, res, function (req, res, pendingInvites) {
            sendResponse.sendJsonResponse(req, res, 200, pendingInvites, 0, 'SUCCESS');
        });
    });
});

router.post('/acceptInvitation', function (req, res, callback) {
    //req.session.userId = 2;
    userModel.userAPIAuthentication(req, res, function (req, res) {
        mentorModel.acceptInvitation(req, res, function (req, res, invitation_data, invitation_slot_data) {
            if (invitation_data != "error" && invitation_slot_data != "error") {
                var meeting_key = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                for (var i = 0; i < 12; i++) {
                    meeting_key += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                opentok.createSession(function (err, session) {
                    if (err) {
                        throw err;
                    }
                    var data = {
                        meeting_key: meeting_key,
                        sessionId: session.sessionId,
                        invitation_data: invitation_data,
                        invitation_slot_data: invitation_slot_data[0]
                    };
                    mentorModel.insertSessionForMeeting(req, res, data, function (req, res, response) {
                        if (response == 'error') {
                            sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
                        } else if ('success') {
                            var invitationData = {
                                invitation_id: data.invitation_data[0].invitation_id,
                                invitation_slot_id: data.invitation_slot_data[2],
                                duration: data.invitation_data[0].duration,
                                meeting_key: meeting_key
                            };

                            userModel.getUserInformation(req, res, data.invitation_data[0].user_id, invitationData, function (req, res, mentorId, arrMentorData, objParameterData) {
                                userModel.getUserInformation(req, res, data.invitation_data[0].requester_id, invitationData, function (req, res, userId, arrUserData, objParameterData) {
                                    mentorModel.sendAcceptedInvitationMail(req, res, arrMentorData, arrUserData, invitationData);
                                    sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
                                });
                            });
                        }
                    });
                });
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
            }
        });
    });
});

router.get('/userprofile/:userSlug', function (req, res, callback) {
    var userSlug = req.params.userSlug;

    var dataToVerify = {
        userSlug: userSlug
    };
    userModel.checkUserProfileSlug(req, res, dataToVerify, function (req, res, flagMsg, arrUserData) {

        var userType = arrUserData.userType;
        var slugFromDb = arrUserData.slug;
        var profileUserId = arrUserData.userId;

        if (flagMsg == "NOUSERFOUND") {
            //redirect to dashboard, no user found
            res.redirect('/');
        } else {

            // redirect to mentor detail page
            if (userType == "mentor") {
                res.redirect('/mentorprofile/' + slugFromDb);
            }
            // load user profile page
            else if (userType == "user") {

                userSection.getUserProfileData(req, res, profileUserId, function (req, res, userId, arrUserProfileData) {
                    var errors = req.flash('errors');
                    var messages = req.flash('messages');

                    var flagUserLoggedIn = false;
                    var userId = "0";

                    var reqBody = req.body;

                    if (req.session.userId) {
                        flagUserLoggedIn = true;
                        userId = req.session.userId;
                    }

                    var renderParams = {
                        layout: 'mentor_detail_layout',
                        errors: errors,
                        messages: messages,
                        arrSessionData: req.session,
                        flagUserLoggedIn: flagUserLoggedIn,
                        userId: userId,
                        frontConstant: frontConstant,
                        userSlug: req.params.userSlug,
                        arrUserProfileData: arrUserProfileData
                    };
                    res.render('users/userprofile', renderParams);
                });
            }
        }
    });
});

router.post('/declineInvitation', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.declineInvitation(req, res, function (req, res, invitation_data, invitation_slot_data) {
            if (invitation_data != "error") {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
            }
        });
    });
});

router.post('/meetingsData', function (req, res, callback) {
    var data = {};
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.meetingsData(req, res, function (req, res, meetingDataRes) {
            sendResponse.sendJsonResponse(req, res, 200, meetingDataRes, 0, 'SUCCESS');
        });
    });
});

router.post('/meetingsDataPaging', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.meetingsDataPaging(req, res, function (req, res, meetingDataRes) {
            sendResponse.sendJsonResponse(req, res, 200, meetingDataRes, 0, 'SUCCESS');
        });
    });
});

router.post('/cancel', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.cancelInvitation(req, res, function (req, res, invitation_data, invitation_slot_data) {
            if (invitation_data != "error" && invitation_slot_data != "error") {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'success');
            } else {
                sendResponse.sendJsonResponse(req, res, 200, {}, 0, 'error');
            }
        });
    });
});

router.post('/chkPromoCode', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.chkPromoCode_model(req, res, function (req, res, promocodeDataRes) {
            sendResponse.sendJsonResponse(req, res, 200, promocodeDataRes, 0, 'SUCCESS');
        });
    });
});

router.post('/mentorCancelRequest', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.mentorCancelRequest_model(req, res, function (req, res, mentorCancelRequestData) {
            sendResponse.sendJsonResponse(req, res, 200, mentorCancelRequestData, 0, 'SUCCESS');
        });
    });
});

router.post('/userCancelRequest', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.userCancelRequest_model(req, res, function (req, res, userCancelRequestData) {
            sendResponse.sendJsonResponse(req, res, 200, userCancelRequestData, 0, 'SUCCESS');
        });
    });
});


router.post('/checkMentorAnyFutureMeeting', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {
        userModel.checkMentorAnyFutureMeeting(req, res, function (req, res, flagIsAcceptedMeetingsPresent) {
            sendResponse.sendJsonResponse(req, res, 200, flagIsAcceptedMeetingsPresent, 0, 'SUCCESS');
        });
    });
});


router.post('/updateAccountSettings', function (req, res, callback) {
    userModel.userAPIAuthentication(req, res, function (req, res) {

        var stopBeingAMentor = "0";
        var hideProfileSearch = "0";
        var hideProfileCalendar = "0";

        var reqBody = req.body;

        if (typeof reqBody.stop_mentorship != 'undefined') {
            stopBeingAMentor = reqBody.stop_mentorship;
        }
        if (typeof reqBody.hide_profile_search != 'undefined') {
            hideProfileSearch = reqBody.hide_profile_search;
        }
        if (typeof reqBody.hide_profile_calendar != 'undefined') {
            hideProfileCalendar = reqBody.hide_profile_calendar;
        }

        var objDataParams = {
            stopBeingAMentor: stopBeingAMentor,
            hideProfileSearch: hideProfileSearch,
            hideProfileCalendar: hideProfileCalendar
        };

        userModel.updateAccountSettings(req, res, objDataParams, function (req, res, flagUpdated) {
            sendResponse.sendJsonResponse(req, res, 200, flagUpdated, 0, 'SUCCESS');
        });
    });
});



router.get('/changeUserAccountTypeStatus/:statusKey', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        var statusKey = req.params.statusKey;
        var objDataToSend = {
            statusKey: statusKey
        }
        userModel.changeUsersAccountTypeStatus(req, res, objDataToSend, function (req, res) {
            res.redirect('/');
        });
    });
});

router.get('/changeUserTypeAndRedirectToComplaice/:statusKey', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        var statusKey = req.params.statusKey;
        var objDataToSend = {
            statusKey: statusKey
        }
        userModel.changeUsersAccountTypeStatus(req, res, objDataToSend, function (req, res) {
            res.redirect('/mentor/complianceTest');
        });
    });
});

router.get('/changeUserTypeAndRedirectToHome/:statusKey', function (req, res, callback) {
    userModel.userAuthentication(req, res, function (req, res) {
        var statusKey = req.params.statusKey;
        var objDataToSend = {
            statusKey: statusKey
        }
        userModel.changeUsersAccountTypeStatus(req, res, objDataToSend, function (req, res) {
            res.redirect('/');
        });
    });
});

router.post('/raiseRefundRequest', function (req, res, callback) {
    userModel.raiseRefundRequest(req, res, function (req, res, response) {
        sendResponse.sendJsonResponse(req, res, 200, response, "0", "FETCH");
    });
});

module.exports = router;